# update ids
#install_name_tool -id @executable_path/lib/QtCore lib/QtCore
#install_name_tool -id @executable_path/lib/QtGui lib/QtGui
#install_name_tool -id @executable_path/lib/QtWidgets lib/QtWidgets
#install_name_tool -id @executable_path/lib/QtDBus lib/QtDBus
#install_name_tool -id @executable_path/lib/QtPrintSupport lib/QtPrintSupport

# XPIWIT
install_name_tool -change @rpath/QtCore.framework/Versions/5/QtCore @executable_path/lib/QtCore.framework/QtCore XPIWIT

install_name_tool -change @rpath/libc10.dylib @executable_path/lib/libc10.dylib XPIWIT
install_name_tool -change @rpath/libtorch.dylib @executable_path/lib/libtorch.dylib XPIWIT
install_name_tool -change @rpath/libtorch_cpu.dylib @executable_path/lib/libtorch_cpu.dylib XPIWIT

#install_name_tool -change @rpath/QtDBus.framework/Versions/5/QtDBus @executable_path/lib/QtDBus XPIWIT
#install_name_tool -change @rpath/QtGui.framework/Versions/5/QtGui @executable_path/lib/QtGui XPIWIT
#install_name_tool -change @rpath/QtWidgets.framework/Versions/5/QtWidgets @executable_path/lib/QtWidgets XPIWIT
#install_name_tool -change @rpath/QtPrintSupport.framework/Versions/5/QtPrintSupport @executable_path/lib/QtPrintSupport XPIWIT

# XPIWITGUI
install_name_tool -change @rpath/QtCore.framework/Versions/5/QtCore @executable_path/lib/QtCore.framework/QtCore XPIWITGUI
install_name_tool -change @rpath/QtGui.framework/Versions/5/QtGui @executable_path/lib/QtGui.framework/QtGui XPIWITGUI
install_name_tool -change @rpath/QtWidgets.framework/Versions/5/QtWidgets @executable_path/lib/QtWidgets.framework/QtWidgets XPIWITGUI

#install_name_tool -change @executable_path/lib/QtCore @rpath/QtCore.framework/Versions/5/QtCore XPIWITGUI
install_name_tool -change @rpath/QtDBus.framework/Versions/5/QtDBus @executable_path/lib/QtDBus.framework/QtDBus XPIWITGUI
install_name_tool -change @rpath/QtGui.framework/Versions/5/QtGui @executable_path/lib/QtGui.framework/QtGui XPIWITGUI
install_name_tool -change @rpath/QtWidgets.framework/Versions/5/QtWidgets @executable_path/lib/QtWidgets.framework/QtWidgets XPIWITGUI
install_name_tool -change @rpath/QtPrintSupport.framework/Versions/5/QtPrintSupport @executable_path/lib/QtPrintSupport.framework/QtPrintSupport XPIWITGUI

install_name_tool -change @rpath/libtorch.dylib @executable_path/lib/libtorch.dylib lib/libtorch.dylib
install_name_tool -change @rpath/libtorch_cpu.dylib @executable_path/lib/libtorch_cpu.dylib lib/libtorch.dylib
install_name_tool -change @rpath/libc10.dylib @executable_path/lib/libc10.dylib lib/libtorch.dylib
install_name_tool -change @rpath/libiomp5.dylib @executable_path/lib/libiomp5.dylib lib/libtorch.dylib

install_name_tool -change @rpath/libtorch.dylib @executable_path/lib/libtorch.dylib lib/libtorch_cpu.dylib
install_name_tool -change @rpath/libtorch_cpu.dylib @executable_path/lib/libtorch_cpu.dylib lib/libtorch_cpu.dylib
install_name_tool -change @rpath/libc10.dylib @executable_path/lib/libc10.dylib lib/libtorch_cpu.dylib
install_name_tool -change @rpath/libiomp5.dylib @executable_path/lib/libiomp5.dylib lib/libtorch_cpu.dylib


# QtCore
#install_name_tool -change @rpath/QtCore.framework/Versions/5/QtCore @executable_path/lib/QtCore lib/QtCore
#install_name_tool -change @rpath/QtDBus.framework/Versions/5/QtDBus @executable_path/lib/QtDBus lib/QtCore
#install_name_tool -change @rpath/QtGui.framework/Versions/5/QtGui @executable_path/lib/QtGui lib/QtCore
#install_name_tool -change @rpath/QtWidgets.framework/Versions/5/QtWidgets @executable_path/lib/QtWidgets lib/QtCore
#install_name_tool -change @rpath/QtPrintSupport.framework/Versions/5/QtPrintSupport @executable_path/lib/QtPrintSupport lib/QtCore

# QtDBus
#install_name_tool -change @rpath/QtCore.framework/Versions/5/QtCore @executable_path/lib/QtCore lib/QtDBus
#install_name_tool -change @rpath/QtDBus.framework/Versions/5/QtDBus @executable_path/lib/QtDBus lib/QtDBus
#install_name_tool -change @rpath/QtGui.framework/Versions/5/QtGui @executable_path/lib/QtGui lib/QtDBus
#install_name_tool -change @rpath/QtWidgets.framework/Versions/5/QtWidgets @executable_path/lib/QtWidgets lib/QtDBus
#install_name_tool -change @rpath/QtPrintSupport.framework/Versions/5/QtPrintSupport @executable_path/lib/QtPrintSupport lib/QtDBus

# QtGui
#install_name_tool -change @rpath/QtCore.framework/Versions/5/QtCore @executable_path/lib/QtCore lib/QtGui
#install_name_tool -change @rpath/QtDBus.framework/Versions/5/QtDBus @executable_path/lib/QtDBus lib/QtGui
#install_name_tool -change @rpath/QtGui.framework/Versions/5/QtGui @executable_path/lib/QtGui lib/QtGui
#install_name_tool -change @rpath/QtWidgets.framework/Versions/5/QtWidgets @executable_path/lib/QtWidgets lib/QtGui
#install_name_tool -change @rpath/QtPrintSupport.framework/Versions/5/QtPrintSupport @executable_path/lib/QtPrintSupport lib/QtGui

# QtPrintSupport
#install_name_tool -change @rpath/QtCore.framework/Versions/5/QtCore @executable_path/lib/QtCore lib/QtPrintSupport
#install_name_tool -change @rpath/QtDBus.framework/Versions/5/QtDBus @executable_path/lib/QtDBus lib/QtPrintSupport
#install_name_tool -change @rpath/QtGui.framework/Versions/5/QtGui @executable_path/lib/QtGui lib/QtPrintSupport
#install_name_tool -change @rpath/QtWidgets.framework/Versions/5/QtWidgets @executable_path/lib/QtWidgets lib/QtPrintSupport
#install_name_tool -change @rpath/QtPrintSupport.framework/Versions/5/QtPrintSupport @executable_path/lib/QtPrintSupport lib/QtPrintSupport

# QtWidgets
#install_name_tool -change @rpath/QtCore.framework/Versions/5/QtCore @executable_path/lib/QtCore lib/QtWidgets
#install_name_tool -change @rpath/QtDBus.framework/Versions/5/QtDBus @executable_path/lib/QtDBus lib/QtWidgets
#install_name_tool -change @rpath/QtGui.framework/Versions/5/QtGui @executable_path/lib/QtGui lib/QtWidgets
#install_name_tool -change @rpath/QtWidgets.framework/Versions/5/QtWidgets @executable_path/lib/QtWidgets lib/QtWidgets
#install_name_tool -change @rpath/QtPrintSupport.framework/Versions/5/QtPrintSupport @executable_path/lib/QtPrintSupport lib/QtWidgets

# libqcocoa.dylib
#install_name_tool -change @rpath/QtCore.framework/Versions/5/QtCore @executable_path/lib/QtCore platforms/libqcocoa.dylib
#install_name_tool -change @rpath/QtDBus.framework/Versions/5/QtDBus @executable_path/lib/QtDBus platforms/libqcocoa.dylib
#install_name_tool -change @rpath/QtGui.framework/Versions/5/QtGui @executable_path/lib/QtGui platforms/libqcocoa.dylib
#install_name_tool -change @rpath/QtWidgets.framework/Versions/5/QtWidgets @executable_path/lib/QtWidgets platforms/libqcocoa.dylib
#install_name_tool -change @rpath/QtPrintSupport.framework/Versions/5/QtPrintSupport @executable_path/lib/QtPrintSupport platforms/libqcocoa.dylib