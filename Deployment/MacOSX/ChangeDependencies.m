
subfolder = '';
inputPaths{1} = ['/Users/jstegmaier/Programming/XPIWIT/Release/Bin/lib/QtCore.framework/' subfolder];
inputPaths{2} = ['/Users/jstegmaier/Programming/XPIWIT/Release/Bin/lib/QtGui.framework/' subfolder];
inputPaths{3} = ['/Users/jstegmaier/Programming/XPIWIT/Release/Bin/lib/QtWidgets.framework/' subfolder];
inputPaths{4} = ['/Users/jstegmaier/Programming/XPIWIT/Release/Bin/lib/QtPrintSupport.framework/' subfolder];
inputPaths{5} = ['/Users/jstegmaier/Programming/XPIWIT/Release/Bin/lib/QtDBus.framework/' subfolder];
inputPaths{6} = ['/Users/jstegmaier/Programming/XPIWIT/Release/Bin/lib/QtSvg.framework/' subfolder];
inputPaths{7} = ['/Users/jstegmaier/Programming/XPIWIT/Release/Bin/platforms'];


for p=1:7
    inputFiles = dir([inputPaths{p} '/*']);

    cd(inputPaths{p});

    %% create dependency lists
    for i=1:length(inputFiles)

        if (~isfile([inputFiles(i).name]) || inputFiles(i).name(1) == '.')
            continue;
        end    

        system(['otool -L ' inputFiles(i).name ' > ' inputFiles(i).name '.dependencies']);
    end

    for i=1:length(inputFiles)

        if (~isfile([inputFiles(i).name]) || inputFiles(i).name(1) == '.')
            continue;
        end

        fileID = fopen([inputFiles(i).name '.dependencies'], 'rb');

        system(['install_name_tool -id @executable_path/lib/' inputFiles(i).name '.framework/Versions/5/' inputFiles(i).name ' ' inputFiles(i).name]);

        currentLine = fgetl(fileID);

        while (currentLine > 0)

            splitString = strsplit(currentLine, ' ');

            if (contains(splitString{1}, '@rpath'))

                oldString = strrep(strrep(splitString{1}, '→', ''), '	', '');
                currentString = oldString;
                currentString = strrep(currentString, '@rpath/QtCore.framework/Versions/5/QtCore', '@executable_path/lib/QtCore.framework/Versions/5/QtCore');
                currentString = strrep(currentString, '@rpath/QtDBus.framework/Versions/5/QtDBus', '@executable_path/lib/QtDBus.framework/Versions/5/QtDBus');
                currentString = strrep(currentString, '@rpath/QtSvg.framework/Versions/5/QtSvg', '@executable_path/lib/QtSvg.framework/Versions/5/QtSvg');
                currentString = strrep(currentString, '@rpath/QtGui.framework/Versions/5/QtGui', '@executable_path/lib/QtGui.framework/Versions/5/QtGui');
                currentString = strrep(currentString, '@rpath/QtWidgets.framework/Versions/5/QtWidgets', '@executable_path/lib/QtWidgets.framework/Versions/5/QtWidgets');
                currentString = strrep(currentString, '@rpath/QtPrintSupport.framework/Versions/5/QtPrintSupport', '@executable_path/lib/QtPrintSupport.framework/Versions/5/QtPrintSupport');
                system(['install_name_tool -change ' oldString ' ' currentString ' ' inputFiles(i).name]);
            end

            currentLine = fgetl(fileID);
        end

        fclose(fileID);
        delete([inputFiles(i).name '.dependencies']);
    end
end

% fileID = fopen('/Users/jstegmaier/ScieboDrive/Projects/2020/FilamentTracing_WindofferUKA/Software/dependenciesGUI.txt', 'rb');
% outputFile = fopen('/Users/jstegmaier/ScieboDrive/Projects/2020/FilamentTracing_WindofferUKA/Software/copyDependencies.sh', 'wb');
% outputPath = 'lib';
% 
% fprintf(outputFile, ' #!/bin/sh\n');
% fprintf(outputFile, 'mkdir %s\n', outputPath);
% 
% currentLine = fgetl(fileID);
% 
% while (currentLine > 0)
%    
%     splitString = strsplit(currentLine, ' ');
%     
%     if (length(splitString) > 3)
%         fprintf(outputFile, 'cp %s lib/\n', splitString{3});
%     end
%     
%     currentLine = fgetl(fileID);
% end
% 
% test = 1;
% 
% fclose(outputFile);
% fclose(fileID);