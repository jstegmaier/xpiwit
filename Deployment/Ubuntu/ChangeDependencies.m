fileID = fopen('XPIWITDependencies.txt', 'rb');
outputFile = fopen('copyDependencies.sh', 'wb');
outputPath = 'lib';

fprintf(outputFile, ' #!/bin/sh\n');
fprintf(outputFile, 'mkdir %s\n', outputPath);

currentLine = fgetl(fileID);

while (currentLine > 0)
   
    splitString = strsplit(currentLine, ' ');
    
    if (length(splitString) > 3)
        fprintf(outputFile, 'cp %s lib/\n', splitString{3});
    end
    
    currentLine = fgetl(fileID);
end

test = 1;

fclose(outputFile);
fclose(fileID);