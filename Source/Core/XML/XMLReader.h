/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef XMLREADER_H
#define XMLREADER_H

// namespace header
#include "AbstractPipeline.h"
#include "AbstractFilter.h"
#include "AbstractInput.h"

// qt header
#include <QtCore/QFile>
#include <QtCore/QXmlStreamReader>
#include <QtCore/QStringList>


namespace XPIWIT
{

/**
 * @class XMLReader
 * Reads the content of a XML file and creates an SimplifiedPipeline with all filter settings.
 */
class XMLReader
{
public:
    XMLReader();
	~XMLReader();

    void readXML( QString file, QStringList disabledFilters = QStringList() );

    AbstractPipeline getAbstractPipeline() { return mAbstractPipeline; }
	QList<AbstractFilter*> getFilterList() { return mFilterList; }
	unsigned int getMaximumFilterId() { return mMaximumFilterId; }

private:
    QXmlStreamReader xml;
    AbstractPipeline mAbstractPipeline;
	QList<AbstractFilter*> mFilterList;

    // xml reader sub methods
    AbstractFilter* readItem();

    QString ErrorMessage( QString methodName = "" );

    QStringList mIdCheckList;
    bool CheckIdAndAddToList( QString );
    QString GetAttribute( QString, bool throwException = false );
	unsigned int mMaximumFilterId;
};

} // namespace XPIWIT

#endif // XMLREADER_H
