/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// file header
#include "AbstractPipelineXMLCreator.h"

// project header
#include "../XML/AbstractFilter.h"
#include "../XML/AbstractInput.h"
#include "../XML/AbstractPipeline.h"
#include "../XML/AbstractPipelineTools.h"

// qt header
#include <QXmlStreamWriter>
#include <QFile>
#include <QBuffer>


namespace XPIWIT
{

// the default constructor
AbstractPipelineXMLCreator::AbstractPipelineXMLCreator()
{
    mWriteDescription = true;
	mAbstractPipeline = NULL;
}


// function to get the XML
QByteArray AbstractPipelineXMLCreator::GetXML()
{
    QBuffer xmlBuffer;
	CreateXMLFromAbstractPipeline( &xmlBuffer, mAbstractPipeline );

    return xmlBuffer.buffer();
}


// function to write the xml
void AbstractPipelineXMLCreator::WriteXML( QString file )
{
    QFile xmlFile(file);

    if( xmlFile.open(QIODevice::WriteOnly) )
	{
		CreateXMLFromAbstractPipeline( &xmlFile, mAbstractPipeline );
        xmlFile.close();
    }
	else
	{
        // error
    }
}


// function to create the whole filter list of all available filters
void AbstractPipelineXMLCreator::CreateXMLFromAbstractPipeline(QIODevice* device, AbstractPipeline* abstractPipeline)
{
	QHash< QString, AbstractFilter* >* pipelineItemSet = abstractPipeline->GetPipelineItemSet();

	QList<QString> abstractFilterKeys = pipelineItemSet->keys();
	
    QXmlStreamWriter xmlWriter;
    xmlWriter.setAutoFormatting( true );
    xmlWriter.setDevice( device );
    xmlWriter.writeStartDocument();

	xmlWriter.writeStartElement("xpiwit");
    xmlWriter.writeStartElement("pipeline");

	int readerId = -1;

    // every filter
    //int k = 0;
    QString itemId;
	foreach(QString currentKey, abstractFilterKeys )
	{
		AbstractFilter* currentFilter = pipelineItemSet->value(currentKey);

		int filterId = QString(currentFilter->GetId()[currentFilter->GetId().length()-1]).toInt();
		itemId.sprintf("%04d", filterId);

        xmlWriter.writeStartElement("item");
       // xmlWriter.writeAttribute( "item_id", "item_" + itemId );
		xmlWriter.writeAttribute( "item_id", currentFilter->GetId() );
		
        xmlWriter.writeStartElement("name");
		QString name = currentFilter->GetName();
        xmlWriter.writeCharacters( name );
        xmlWriter.writeEndElement();

		bool isReader = false;
		if (name == QString("ImageReader") || name == QString("MetaReader"))
		{
			isReader = true;
		}

        if( mWriteDescription )
		{
            xmlWriter.writeStartElement("description");
            xmlWriter.writeCharacters(currentFilter->GetDesription());
            xmlWriter.writeEndElement();
        }
		
        //inputs
        xmlWriter.writeStartElement("input");
		xmlWriter.writeAttribute( "number_images", QString().number( currentFilter->GetNumberImageIn() ) );
		xmlWriter.writeAttribute( "number_meta", QString::number( currentFilter->GetNumberMetaIn() ) );
        for(int i = 0; i < currentFilter->GetNumberImageIn(); i++)
		{
            xmlWriter.writeEmptyElement( "image" );

			if (isReader == true)
			{
				xmlWriter.writeAttribute( "item_id_ref", "cmd" );
				xmlWriter.writeAttribute( "number_of_output", QString::number(currentFilter->GetImageInputs()[i].mNumberRef));
				readerId++;
			}
			else
			{
				//itemId.sprintf("item_%04d", currentFilter->GetImageInputs()[i].mIdRef.toInt() );
				xmlWriter.writeAttribute( "item_id_ref", currentFilter->GetImageInputs()[i].mIdRef );
				xmlWriter.writeAttribute( "number_of_output", QString::number(currentFilter->GetImageInputs()[i].mNumberRef ) );
				xmlWriter.writeAttribute( "type_number", QString::number(currentFilter->GetImageInputs()[i].mDataType) );
			}
        }
        for(int i = 0; i < currentFilter->GetNumberMetaIn(); i++)
		{
			xmlWriter.writeEmptyElement("meta");

			if (isReader == true)
			{
				xmlWriter.writeAttribute("item_id_ref", "cmd");
				xmlWriter.writeAttribute("number_of_output", QString::number(currentFilter->GetMetaInputs()[i].mNumberRef));
				readerId++;
			}
			else
			{
				//itemId.sprintf("item_%04d", currentFilter->GetMetaInputs()[i].mIdRef.toInt() );
				xmlWriter.writeAttribute("item_id_ref", currentFilter->GetMetaInputs()[i].mIdRef);
				xmlWriter.writeAttribute("number_of_output", QString::number(currentFilter->GetMetaInputs()[i].mNumberRef));
				xmlWriter.writeAttribute("type_number", QString::number(currentFilter->GetMetaInputs()[i].mDataType));
			}
        }
        xmlWriter.writeEndElement();    // close inputs

        //outputs
        xmlWriter.writeStartElement( "output" );
		xmlWriter.writeAttribute( "number_images", QString::number( currentFilter->GetNumberImageOut() ) );
		xmlWriter.writeAttribute( "number_meta", QString::number( currentFilter->GetNumberMetaOut() ) );
		for(int i = 0; i < currentFilter->GetNumberImageOut(); i++)
		{
            xmlWriter.writeEmptyElement( "image" );
            xmlWriter.writeAttribute( "number", QString::number( i + 1 ) );
			//xmlWriter.writeAttribute( "type_number", QString::number( currentFilter->GetGetImageOutputTypes().at(i) ));
        }
        for(int i = 0; i < currentFilter->GetNumberMetaOut(); i++)
		{
            xmlWriter.writeEmptyElement("meta");
            xmlWriter.writeAttribute( "number", QString::number( i + 1 ) );
			//xmlWriter.writeAttribute( "type_number", processObjectType->GetMetaOutputTypes().at(i) );
        }
        xmlWriter.writeEndElement();

        // arguments
        xmlWriter.writeStartElement( "arguments" );
		for(int i = 0; i < currentFilter->GetNumberParameter(); i++)
		{
            xmlWriter.writeEmptyElement( "parameter" );
			xmlWriter.writeAttribute( "key", currentFilter->GetParameter(i).at(0) );
			xmlWriter.writeAttribute( "value", currentFilter->GetParameter(i).at(1) );
			xmlWriter.writeAttribute( "type", QString::number(currentFilter->GetParameterTypes().at(i)) );
			int debug = currentFilter->GetParameterDescriptions().size();
			if ( currentFilter->GetParameterDescriptions().size() != 0 && currentFilter->GetParameterDescriptions().size() > i)
			{
				if (!currentFilter->GetParameterDescriptions().at(i).isNull() && !currentFilter->GetParameterDescriptions().at(i).isEmpty())
				{
					xmlWriter.writeAttribute( "description", currentFilter->GetParameterDescriptions().at(i) );
				}
			}
        }
        xmlWriter.writeEndElement(); // close arguments

        xmlWriter.writeEndElement(); // close item
		
    }
    xmlWriter.writeEndElement();
	xmlWriter.writeEndElement();
    xmlWriter.writeEndDocument();
}

} //namespace XPIWIT
