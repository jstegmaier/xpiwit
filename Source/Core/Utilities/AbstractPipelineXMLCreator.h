/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef ABSTRACTPIPELINEXMLCREATOR_H
#define ABSTRACTPIPELINEXMLCREATOR_H

// qt header
#include <QtCore/QByteArray>
#include <QtCore/QIODevice>
#include "../XML/AbstractPipeline.h"

namespace XPIWIT
{

/**
 * @class XMLCreator
 * Produces a list of all available Filter and its default values
 */
class AbstractPipelineXMLCreator
{
public:
    /**
     * The constructor.
     */
    AbstractPipelineXMLCreator();

    /**
      * return the xml
      */
    QByteArray GetXML();

    /**
      * writes the xml
      */
    void WriteXML( QString file );

	void SetAbstractPipeline(AbstractPipeline* abstractPipeline) { mAbstractPipeline = abstractPipeline; }

    /**
     * set or unset the write description flag
     */
    void SetWriteDescription( bool writeDescription ) { mWriteDescription = writeDescription; }

private:

    /**
     * create the XML Document
     */
    void CreateXMLFromAbstractPipeline(QIODevice* device, AbstractPipeline* abstractPipeline);

    /**
     * defines if description is written or not
     */
    bool mWriteDescription;

	AbstractPipeline* mAbstractPipeline;
};

} //namespace XPIWIT

#endif // XMLCREATOR_H
