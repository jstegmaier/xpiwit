/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef PROCESSOBJECTLIST_H
#define PROCESSOBJECTLIST_H

// namespace header
#include "ProcessObjectBase.h"

// project header

// itk header

// qt header
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QList>

// system header

namespace XPIWITFilter
{

/**
 * stores one specific settings.
 */
class ProcessObjectList
{

public:

    ProcessObjectList() {}

	inline QList< ProcessObjectBase *> GetProcessObjectList();

	inline ProcessObjectBase *GetProcessObject( QString name, QStringList types = QStringList(), QList<int> dimensions = QList<int>() );

private:
	template< class TTypeOne >
    ProcessObjectBase *GetProcessObjectFirstType( QString name, QStringList types, QList<int> dimensions );

	template< class TTypeOne, class TTypeTwo >
	ProcessObjectBase *GetProcessObjectSecondType( QString name, QStringList types, QList<int> dimensions );

	template< class TTypeOne, class TTypeTwo, class TTypeThree >
	ProcessObjectBase *GetProcessObjectThirdType( QString name, QStringList types, QList<int> dimensions );

	template< class TTypeOne, class TTypeTwo, class TTypeThree >
	ProcessObjectBase *GetProcessObjectInstanciation( QString name, QStringList types, QList<int> dimensions );

};

} // namespace XPIWITFilter

#include "ProcessObjectList.txx"

#endif // PROCESSOBJECTLIST_H
