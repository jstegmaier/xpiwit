/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// namespace header
// ------------------------------------------------- //
// List of all Filters (TODO: extend for new filter) //
// ------------------------------------------------- //

// -------------------------------------------------- //
// ------------- XpWrapper -------------------------- //
// -------------------------------------------------- //

// ------------- Blur ------------------------------- //
#include "../../XpWrapper/Blur/DiscreteGaussianImageFilterWrapper.h"
#include "../../XpWrapper/Blur/LaplacianRecursiveGaussianImageFilterWrapper.h"
#include "../../XpWrapper/Blur/MedianImageFilterWrapper.h"

// ------------- Segmentation ----------------------- //
#include "../../XpWrapper/Segmentation/BinaryThresholdImageFilterWrapper.h"

// -------------------------------------------------- //
// ------------- XpExtended ------------------------- //
// -------------------------------------------------- //

// ------------- ACME ------------------------------- //
#include "../../XpExtended/ACME/ACMEMultiScalePlateMeasureFilterWidget.h"
#include "../../XpExtended/ACME/ACMEWatershedSegmentationFilterWidget.h"

// ------------- Blur ------------------------------- //
#include "../../XpExtended/Blur/DogFilterWidget.h"
#include "../../XpExtended/Blur/DoMFilterWidget.h"
#include "../../XpExtended/Blur/HotspotFilterWidget.h"

// ------------------------------------------------- //
// ------------- ImageTransformation --------------- //
#include "../../XpExtended/ImageTrasformation/CropRegionFilterWidget.h"
#include "../../XpExtended/ImageTrasformation/Euler3DTransformFilterWidget.h"
#include "../../XpExtended/ImageTrasformation/MaximumProjectionFilterWidget.h"
#include "../../XpExtended/ImageTrasformation/ResizeFilterWidget.h"

// ------------------------------------------------- //
// ------------- InformationExtraction-------------- //
#include "../../XpExtended/InformationExtraction/ExtractBoundingBoxFilterWidget.h"
#include "../../XpExtended/InformationExtraction/ExtractInfoFilterWidget.h"
#include "../../XpExtended/InformationExtraction/ExtractLocalExtremaFilterWidget.h"
#include "../../XpExtended/InformationExtraction/ExtractRegionPropsFilterWidget.h"
#include "../../XpExtended/InformationExtraction/LoGScaleSpaceMaximumProjectionFilterWidget.h"

// ------------------------------------------------- //
// ---------------- Segmentation ------------------- //
#include "../../XpExtended/Segmentation/EdgeMapFilterWidget.h"
#include "../../XpExtended/Segmentation/LabelImageFilterWidget.h"
//#include "../../XpExtended/Segmentation/LevelSetSegmentationWidget.h"
#include "../../XpExtended/Segmentation/SplitConnectedBlobsImageFilterWidget.h"
#include "../../XpExtended/Segmentation/ThresholdFilterWidget.h"
#include "../../XpExtended/Segmentation/TwangSegmentationWidget.h"
#include "../../XpExtended/Segmentation/WatershedImageFilterWidget.h"

// ------------------------------------------------- //
// ------------------ Workflow --------------------- //
#include "../../XpExtended/WorkflowFilter/ImageReaderWidget.h"
#include "../../XpExtended/WorkflowFilter/MetaReaderWidget.h"
#include "../../XpExtended/WorkflowFilter/CastImageFilterWidget.h"

// ------------------------------------------------- //

#include "ITKDefinitions.h"
#include "ProcessObjectType.h"

// project header

// itk header

// qt header
#include <QtCore/QList>

// system header


namespace XPIWITFilter
{

QList< ProcessObjectBase *> ProcessObjectList::GetProcessObjectList(){

	QList< ProcessObjectBase *> filterList;
	ProcessObjectBase *temp = NULL;


	// -------------------------------------------------- //
	// ------------- XpWrapper -------------------------- //
	// -------------------------------------------------- //

	// ------------- Blur ------------------------------- //
	filterList << GetProcessObject( "DiscreteGaussianImageFilter" );
	filterList << GetProcessObject( "LaplacianRecursiveGaussianImageFilter" );
	filterList << GetProcessObject( "MedianImageFilter" );

	// ------------- Segmentation ----------------------- //
	filterList << GetProcessObject( "BinaryThresholdImageFilter" );
	

	// -------------------------------------------------- //
	// ------------- XpExtended ------------------------- //
	// -------------------------------------------------- //

	// ------------- ACME ------------------------------- //
	filterList << GetProcessObject( "ACMEMultiScalePlateMeasureImageFilter", QStringList() << "float", QList<int>() << 3 );
	filterList << GetProcessObject( "ACMEWatershedSegmentationFilter", QStringList() << "float", QList<int>() << 3 );

	// ------------- Blur ------------------------------- //
	filterList << GetProcessObject( "DogFilter" );
	filterList << GetProcessObject( "DoMFilter" );
	filterList << GetProcessObject( "HotspotFilter" );

	// ------------------------------------------------- //
	// ------------- Image Transformation -------------- //
	filterList << GetProcessObject( "CropRegionFilter" );
	filterList << GetProcessObject( "Euler3DTransformFilter", QStringList() << "float", QList<int>() << 3 );
	filterList << GetProcessObject( "MaximumProjectionFilter", QStringList() << "float", QList<int>() << 3 );
	filterList << GetProcessObject( "ResizeFilter" );

	// ------------- Information Extraction ------------- //
	filterList << GetProcessObject( "ExtractBoundingBoxFilter" );
	filterList << GetProcessObject( "ExtractInfo" );
	filterList << GetProcessObject( "ExtractLocalExtremaFilter", QStringList() << "float" << "uchar", QList<int>() << 3 << 3 );
	filterList << GetProcessObject( "ExtractRegionPropsFilter" );
	filterList << GetProcessObject( "LoGScaleSpaceMaximumProjectionFilter", QStringList() << "float" << "uchar", QList<int>() << 3 << 3 );

	// ---------------- Segmentation ------------------- //
	filterList << GetProcessObject( "EdgeMapFilter" );
	filterList << GetProcessObject( "LabelImageFilter" );
	//filterList << GetProcessObject( "LevelSetSegmentationFilter",  QStringList() << "float", QList<int>() << 3 );
	filterList << GetProcessObject( "SplitConnectedBlobsImageFilter" );
	filterList << GetProcessObject( "ThresholdFilter" );
	filterList << GetProcessObject( "TwangSegmentation" );
	filterList << GetProcessObject( "WatershedImageFilter" );

	// ------------------ Workflow ---------------------- //
	filterList << GetProcessObject( "ImageReader" );
	filterList << GetProcessObject( "MetaReader" );
	filterList << GetProcessObject( "CastImageFilter" );

	return filterList;
}


template< class TTypeOne, class TTypeTwo, class TTypeThree >
ProcessObjectBase *ProcessObjectList::GetProcessObjectInstanciation( QString name, QStringList types, QList<int> dimensions ){

	ProcessObjectBase *processObjectBase = NULL;

	// -------------------------------------------------- //
	// ------------------ XpWrapper --------------------- //
	// -------------------------------------------------- //

	// ------------------ Blur -------------------------- //
	if( name.compare( "DiscreteGaussianImageFilter", Qt::CaseInsensitive ) == 0 ){
		processObjectBase = new DiscreteGaussianImageFilterWrapper< TTypeOne >();
	}
	if( name.compare( "LaplacianRecursiveGaussianImageFilter", Qt::CaseInsensitive ) == 0 ){
		processObjectBase = new LaplacianRecursiveGaussianImageFilterWrapper< TTypeOne >();
	}
	if( name.compare( "MedianImageFilter", Qt::CaseInsensitive ) == 0 ){
		processObjectBase = new MedianImageFilterWrapper< TTypeOne >();
	}
	
	// ------------- Segmentation ----------------------- //
	if( name.compare( "BinaryThresholdImageFilter", Qt::CaseInsensitive ) == 0 ){
		processObjectBase = new BinaryThresholdFilterWidget< TTypeOne >();
	}


	// -------------------------------------------------- //
	// ------------------ XpExtended -------------------- //
	// -------------------------------------------------- //

	// ------------- ACME ------------------------------- //
	if( name.compare( "ACMEMultiScalePlateMeasureImageFilter", Qt::CaseInsensitive ) == 0 ){
		if( TTypeOne::ImageDimension == 3)	// 3D only	- float only							//QString type = QString::fromStdString( typeid( TTypeOne::PixelType  ).name() );
			processObjectBase = new ACMEMultiScalePlateMeasureFilterWidget< Image3Float >();	// itk::Image< TTypeOne::PixelType, 3 >
	}
	if( name.compare( "ACMEWatershedSegmentationFilter", Qt::CaseInsensitive ) == 0 ){
		if( TTypeOne::ImageDimension == 3)	// 3D only	- float only	
			processObjectBase = new ACMEWatershedSegmentationFilterWidget< Image3Float >();
	}

	// ------------- Blur ------------------------------- //
	if( name.compare( "DoGFilter", Qt::CaseInsensitive ) == 0 ){
		processObjectBase = new DoGFilterWidget< TTypeOne >();
	}
	if( name.compare( "DoMFilter", Qt::CaseInsensitive ) == 0 ){
		processObjectBase = new DoMFilterWidget< TTypeOne >();
	}
	if( name.compare( "HotspotFilter", Qt::CaseInsensitive ) == 0 ){
		processObjectBase = new HotspotFilterWidget< TTypeOne >();
	}

	// ------------- Image Transformation -------------- //
	if( name.compare( "CropRegionFilter", Qt::CaseInsensitive ) == 0 ){
		processObjectBase = new CropRegionFilterWidget< TTypeOne >();
	}
	if( name.compare( "Euler3DTransformFilter", Qt::CaseInsensitive ) == 0 ){
		if( TTypeOne::ImageDimension == 3)	// 3D only
			processObjectBase = new Euler3DTransformFilterWidget< itk::Image< TTypeOne::PixelType, 3 > >();
	}
	if( name.compare( "MaximumProjectionFilter", Qt::CaseInsensitive ) == 0 ){
		if( TTypeOne::ImageDimension == 3)	// 3D only
			processObjectBase = new MaximumProjectionFilterWidget< itk::Image< TTypeOne::PixelType, 3 > >();
	}
	if( name.compare( "ResizeFilter", Qt::CaseInsensitive ) == 0 ){
		processObjectBase = new ResizeFilterWidget< TTypeOne >();
	}
	

	// ------------- Information Extraction ------------- //
	if( name.compare( "ExtractBoundingBoxFilter", Qt::CaseInsensitive ) == 0 ){
		processObjectBase = new ExtractBoundingBoxFilterWidget< TTypeOne >();
	}
	if( name.compare( "ExtractInfo", Qt::CaseInsensitive ) == 0 ){
		processObjectBase = new ExtractInfoFilterWidget< TTypeOne >();
	}
	if( name.compare( "ExtractLocalExtremaFilter", Qt::CaseInsensitive ) == 0 ){
		processObjectBase = new ExtractLocalExtremaFilterWidget< TTypeOne, itk::Image< TTypeTwo::PixelType, TTypeOne::ImageDimension > >();
	}
	if( name.compare( "ExtractRegionPropsFilter", Qt::CaseInsensitive ) == 0 ){
		processObjectBase = new ExtractRegionPropsFilterWidget< TTypeOne >();
	}
	if( name.compare( "LoGScaleSpaceMaximumProjectionFilter", Qt::CaseInsensitive ) == 0 ){
		processObjectBase = new LoGScaleSpaceMaximumProjectionFilterWidget< TTypeOne, itk::Image< TTypeTwo::PixelType, TTypeOne::ImageDimension > >();
	}

	// ---------------- Segmentation ------------------- //
	if( name.compare( "EdgeMapFilter", Qt::CaseInsensitive ) == 0 ){
		processObjectBase = new EdgeMapFilterWidget< TTypeOne >();
	}
	if( name.compare( "LabelImageFilter", Qt::CaseInsensitive ) == 0 ){
		processObjectBase = new LabelImageFilterWidget< TTypeOne >();
	}
	//if( name.compare( "LevelSetSegmentationFilter", Qt::CaseInsensitive ) == 0 ){
	//	processObjectBase = new LevelSetSegmentationWidget< TTypeOne >();
	//}
	if( name.compare( "SplitConnectedBlobsImageFilter", Qt::CaseInsensitive ) == 0 ){
		processObjectBase = new SplitConnectedBlobsImageFilterWidget< TTypeOne >();
	}
	if( name.compare( "ThresholdFilter", Qt::CaseInsensitive ) == 0 ){
		processObjectBase = new ThresholdFilterWidget< TTypeOne >();
	}
	if( name.compare( "TwangSegmentation", Qt::CaseInsensitive ) == 0 ){
		processObjectBase = new TwangSegmentationWidget< TTypeOne >();
	}
	if( name.compare( "WatershedImageFilter", Qt::CaseInsensitive ) == 0 ){
		processObjectBase = new WatershedImageFilterWidget< TTypeOne >();
	}

	// ------------------ Workflow ---------------------- //
	if( name.compare( "ImageReader", Qt::CaseInsensitive ) == 0 ){
		processObjectBase = new ImageReaderWidget< TTypeOne >();
	}

	if( name.compare( "MetaReader", Qt::CaseInsensitive ) == 0 ){
		processObjectBase = new MetaReaderWidget();
	}

	if( name.compare( "CastImageFilter", Qt::CaseInsensitive ) == 0 ){
		processObjectBase = new CastImageFilterWidget< TTypeOne, itk::Image< TTypeTwo::PixelType, TTypeOne::ImageDimension > >();
	}

	if( processObjectBase == NULL )
	{
		XPIWITCore::Logger::GetInstance()->WriteLine("! Filter: " + name + " can not be instantiated and will be skipped.\n");
		XPIWITCore::Logger::GetInstance()->WriteLine("- Update of ProcessObjectManager may solve this error.\n");
	}
	return processObjectBase;
}


ProcessObjectBase *ProcessObjectList::GetProcessObject( QString name, QStringList types, QList<int> dimensions ){

	const int stage = 0;

	if( stage == types.length() ){
		return GetProcessObjectInstanciation< Image2Float, Image2Float, Image2Float >( name, types, dimensions );
	}

	if( types.at( stage ).compare( "char", Qt::CaseInsensitive ) == 0 ){
		if( dimensions.at( stage ) == 2 )
			return GetProcessObjectFirstType< Image2Char >( name, types, dimensions );
		if( dimensions.at( stage ) == 3 )
			return GetProcessObjectFirstType< Image3Char >( name, types, dimensions );
	}
	if( types.at( stage ).compare( "short", Qt::CaseInsensitive ) == 0 ){
		if( dimensions.at( stage ) == 2 )
			return GetProcessObjectFirstType< Image2Short >( name, types, dimensions );
		if( dimensions.at( stage ) == 3 )
			return GetProcessObjectFirstType< Image3Short >( name, types, dimensions );
	}
	if( types.at( stage ).compare( "int", Qt::CaseInsensitive ) == 0 ){
		if( dimensions.at( stage ) == 2 )
			return GetProcessObjectFirstType< Image2Int >( name, types, dimensions );
		if( dimensions.at( stage ) == 3 )
			return GetProcessObjectFirstType< Image3Int >( name, types, dimensions );
	}

	if( types.at( stage ).compare( "uchar", Qt::CaseInsensitive ) == 0 ){
		if( dimensions.at( stage ) == 2 )
			return GetProcessObjectFirstType< Image2UChar >( name, types, dimensions );
		if( dimensions.at( stage ) == 3 )
			return GetProcessObjectFirstType< Image3UChar >( name, types, dimensions );
	}
	if( types.at( stage ).compare( "ushort", Qt::CaseInsensitive ) == 0 ){
		if( dimensions.at( stage ) == 2 )
			return GetProcessObjectFirstType< Image2UShort >( name, types, dimensions );
		if( dimensions.at( stage ) == 3 )
			return GetProcessObjectFirstType< Image3UShort >( name, types, dimensions );
	}
	if( types.at( stage ).compare( "uint", Qt::CaseInsensitive ) == 0 ){
		if( dimensions.at( stage ) == 2 )
			return GetProcessObjectFirstType< Image2UInt >( name, types, dimensions );
		if( dimensions.at( stage ) == 3 )
			return GetProcessObjectFirstType< Image3UInt >( name, types, dimensions );
	}

	if( types.at( stage ).compare( "float", Qt::CaseInsensitive ) == 0 ){
		if( dimensions.at( stage ) == 2 )
			return GetProcessObjectFirstType< Image2Float >( name, types, dimensions );
		if( dimensions.at( stage ) == 3 )
			return GetProcessObjectFirstType< Image3Float >( name, types, dimensions );
	}

	if( types.at( stage ).compare( "double", Qt::CaseInsensitive ) == 0 ){
		if( dimensions.at( stage ) == 2 )
			return GetProcessObjectFirstType< Image2Double >( name, types, dimensions );
		if( dimensions.at( stage ) == 3 )
			return GetProcessObjectFirstType< Image3Double >( name, types, dimensions );
	}

	return NULL;
}

template< class TTypeOne >
ProcessObjectBase *ProcessObjectList::GetProcessObjectFirstType( QString name, QStringList types, QList<int> dimensions ){
	const int stage = 1;

	if( stage == types.length() ){
		return GetProcessObjectInstanciation< TTypeOne, TTypeOne, TTypeOne >( name, types, dimensions );
	}


	if( types.at( stage ).compare( "char", Qt::CaseInsensitive ) == 0 ){
		if( dimensions.at( stage ) == 2 )
			return GetProcessObjectSecondType< TTypeOne, Image2Char >( name, types, dimensions );
		if( dimensions.at( stage ) == 3 )
			return GetProcessObjectSecondType< TTypeOne, Image3Char >( name, types, dimensions );
	}
	if( types.at( stage ).compare( "short", Qt::CaseInsensitive ) == 0 ){
		if( dimensions.at( stage ) == 2 )
			return GetProcessObjectSecondType< TTypeOne, Image2Short >( name, types, dimensions );
		if( dimensions.at( stage ) == 3 )
			return GetProcessObjectSecondType< TTypeOne, Image3Short >( name, types, dimensions );
	}
	if( types.at( stage ).compare( "int", Qt::CaseInsensitive ) == 0 ){
		if( dimensions.at( stage ) == 2 )
			return GetProcessObjectSecondType< TTypeOne, Image2Int >( name, types, dimensions );
		if( dimensions.at( stage ) == 3 )
			return GetProcessObjectSecondType< TTypeOne, Image3Int >( name, types, dimensions );
	}

	if( types.at( stage ).compare( "uchar", Qt::CaseInsensitive ) == 0 ){
		if( dimensions.at( stage ) == 2 )
			return GetProcessObjectSecondType< TTypeOne, Image2UChar >( name, types, dimensions );
		if( dimensions.at( stage ) == 3 )
			return GetProcessObjectSecondType< TTypeOne, Image3UChar >( name, types, dimensions );
	}
	if( types.at( stage ).compare( "ushort", Qt::CaseInsensitive ) == 0 ){
		if( dimensions.at( stage ) == 2 )
			return GetProcessObjectSecondType< TTypeOne, Image2UShort >( name, types, dimensions );
		if( dimensions.at( stage ) == 3 )
			return GetProcessObjectSecondType< TTypeOne, Image3UShort >( name, types, dimensions );
	}
	if( types.at( stage ).compare( "uint", Qt::CaseInsensitive ) == 0 ){
		if( dimensions.at( stage ) == 2 )
			return GetProcessObjectSecondType< TTypeOne, Image2UInt >( name, types, dimensions );
		if( dimensions.at( stage ) == 3 )
			return GetProcessObjectSecondType< TTypeOne, Image3UInt >( name, types, dimensions );
	}

	if( types.at( stage ).compare( "float", Qt::CaseInsensitive ) == 0 ){
		if( dimensions.at( stage ) == 2 )
			return GetProcessObjectSecondType< TTypeOne, Image2Float >( name, types, dimensions );
		if( dimensions.at( stage ) == 3 )
			return GetProcessObjectSecondType< TTypeOne, Image3Float >( name, types, dimensions );
	}

	if( types.at( stage ).compare( "double", Qt::CaseInsensitive ) == 0 ){
		if( dimensions.at( stage ) == 2 )
			return GetProcessObjectSecondType< TTypeOne, Image2Double >( name, types, dimensions );
		if( dimensions.at( stage ) == 3 )
			return GetProcessObjectSecondType< TTypeOne, Image3Double >( name, types, dimensions );
	}

	return NULL;
}

template< class TTypeOne, class TTypeTwo >
ProcessObjectBase *ProcessObjectList::GetProcessObjectSecondType( QString name, QStringList types, QList<int> dimensions ){
	const int stage = 2;

	if( stage == types.length() ){
		return GetProcessObjectInstanciation<  TTypeOne, TTypeTwo,  TTypeOne >( name, types, dimensions );
	}

	//if( types.at( stage ).compare( "char", Qt::CaseInsensitive ) == 0 ){
	//	if( dimensions.at( stage ) == 2 )
	//		return GetProcessObjectThirdType< TTypeOne, TTypeTwo, Image2Char >( name, types, dimensions );
	//	if( dimensions.at( stage ) == 3 )
	//		return GetProcessObjectThirdType< TTypeOne, TTypeTwo, Image3Char >( name, types, dimensions );
	//}
	//if( types.at( stage ).compare( "short", Qt::CaseInsensitive ) == 0 ){
	//	if( dimensions.at( stage ) == 2 )
	//		return GetProcessObjectThirdType< TTypeOne, TTypeTwo, Image2Short >( name, types, dimensions );
	//	if( dimensions.at( stage ) == 3 )
	//		return GetProcessObjectThirdType< TTypeOne, TTypeTwo, Image3Short >( name, types, dimensions );
	//}
	//if( types.at( stage ).compare( "int", Qt::CaseInsensitive ) == 0 ){
	//	if( dimensions.at( stage ) == 2 )
	//		return GetProcessObjectThirdType< TTypeOne, TTypeTwo, Image2Int >( name, types, dimensions );
	//	if( dimensions.at( stage ) == 3 )
	//		return GetProcessObjectThirdType< TTypeOne, TTypeTwo, Image3Int >( name, types, dimensions );
	//}

	//if( types.at( stage ).compare( "uchar", Qt::CaseInsensitive ) == 0 ){
	//	if( dimensions.at( stage ) == 2 )
	//		return GetProcessObjectThirdType< TTypeOne, TTypeTwo, Image2UChar >( name, types, dimensions );
	//	if( dimensions.at( stage ) == 3 )
	//		return GetProcessObjectThirdType< TTypeOne, TTypeTwo, Image3UChar >( name, types, dimensions );
	//}
	//if( types.at( stage ).compare( "ushort", Qt::CaseInsensitive ) == 0 ){
	//	if( dimensions.at( stage ) == 2 )
	//		return GetProcessObjectThirdType< TTypeOne, TTypeTwo, Image2UShort >( name, types, dimensions );
	//	if( dimensions.at( stage ) == 3 )
	//		return GetProcessObjectThirdType< TTypeOne, TTypeTwo, Image3UShort >( name, types, dimensions );
	//}
	//if( types.at( stage ).compare( "uint", Qt::CaseInsensitive ) == 0 ){
	//	if( dimensions.at( stage ) == 2 )
	//		return GetProcessObjectThirdType< TTypeOne, TTypeTwo, Image2UInt >( name, types, dimensions );
	//	if( dimensions.at( stage ) == 3 )
	//		return GetProcessObjectThirdType< TTypeOne, TTypeTwo, Image3UInt >( name, types, dimensions );
	//}

	//if( types.at( stage ).compare( "float", Qt::CaseInsensitive ) == 0 ){
	//	if( dimensions.at( stage ) == 2 )
	//		return GetProcessObjectThirdType< TTypeOne, TTypeTwo, Image2Float >( name, types, dimensions );
	//	if( dimensions.at( stage ) == 3 )
	//		return GetProcessObjectThirdType< TTypeOne, TTypeTwo, Image3Float >( name, types, dimensions );
	//}

	//if( types.at( stage ).compare( "double", Qt::CaseInsensitive ) == 0 ){
	//	if( dimensions.at( stage ) == 2 )
	//		return GetProcessObjectThirdType< TTypeOne, TTypeTwo, Image2Double >( name, types, dimensions );
	//	if( dimensions.at( stage ) == 3 )
	//		return GetProcessObjectThirdType< TTypeOne, TTypeTwo, Image3Double >( name, types, dimensions );
	//}
	return NULL;
}

template< class TTypeOne, class TTypeTwo, class TTypeThree >
ProcessObjectBase *ProcessObjectList::GetProcessObjectThirdType( QString name, QStringList types, QList<int> dimensions ){
	//const int stage = 3;

	//if( stage == types.length() ){
	//	return GetProcessObjectInstanciation< TTypeOne, TTypeTwo, TTypeThree >( name, types, dimensions );
	//}

	return NULL;
}


} // namespace XPIWITFilter
