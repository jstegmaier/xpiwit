/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef DROPLINEEDIT_H
#define DROPLINEEDIT_H

// namespace header
#include "../Core/Utilities/Logger.h"

// qt header
#include <QWidget>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>
#include <QDropEvent>
#include <QDragEnterEvent>


namespace XPIWIT
{

/**
 *	@class XPIWITGUIMainWindow
 *	Main class of the XPIWIT GUI project
 */
class DropLineEdit : public QWidget
{
    Q_OBJECT
    public:
		DropLineEdit(QWidget *parent = 0);
		DropLineEdit(const QString& label, const QString& text, QWidget *parent = 0);
        ~DropLineEdit();
		void SetLabel(const QString& label) { mLabel->setText(label); };
		void SetText(const QString& text) { mLineEdit->setText(text); };

		QString GetText() { CreateValidPath(); return mLineEdit->text(); }
		QString GetLabel() { return mLabel->text(); }
		
    signals:
		void textChanged(QString);

	private slots:
        void dropEvent(QDropEvent* event) override;
        void dragEnterEvent(QDragEnterEvent* event) override;
        void dragMoveEvent(QDragMoveEvent* event) override;
		void slotOpenFile();
		void slotOpenFolder();
		void slotTextChanged(QString);
		void CreateValidPath();

	private:
		QHBoxLayout* mLayout;
		QLineEdit* mLineEdit;
		QLabel* mLabel;
		QPushButton* mOpenFileButton;
		QPushButton* mOpenFolderButton;
};

} // namespace XPIWIT

#endif  // XPIWITGUIMainWindow_H
