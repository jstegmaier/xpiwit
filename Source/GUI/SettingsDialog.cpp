#include "SettingsDialog.h"
#include "qgroupbox.h"

namespace XPIWIT{

// Constructoe
SettingsDialog::SettingsDialog(void)
{
	this->init();
	// disable '?' button at top of the window
	this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
	// Delete the widget on close
	// this->setAttribute(Qt::WA_DeleteOnClose, true);

	// Connections
	connect(mCategoriesListWidget, SIGNAL(currentRowChanged(int)), mOptionsStack, SLOT(setCurrentIndex(int)));
	connect(mCancelButton, SIGNAL(clicked()), this, SLOT(close()));
	// Connections to apply changes
	connect(mOKButton, SIGNAL(clicked()), this, SLOT(slotOKButton()));
	connect(mApplyButton, SIGNAL(clicked()), this, SLOT(slotApplyButton()));
}


// Destructor
SettingsDialog::~SettingsDialog(void)
{
}


// Initializes the Widget. Adds Child Widgets and Layouts
void SettingsDialog::init()
{
	// Title and window options
	this->setWindowTitle(tr("Options"));
	//this->setSizeGripEnabled(true);

	// Generate the List and Stacked Layouts
	this->gernerateCategoriesList();
	this->gernerateOptionsStack();

	// Add general layout
	QGridLayout *mainLayout = new QGridLayout(this);
    mainLayout->setMargin(5);
    mainLayout->setSpacing(5);
	// mainLayout->addWidget(mCategoriesListWidget, 0, 0, Qt::AlignLeft);
    mainLayout->addWidget(mOptionsStack, 0, 0, Qt::AlignHCenter);
	mainLayout->setColumnStretch(0,1);
	mainLayout->setColumnStretch(1,3);
	mainLayout->setRowStretch(0,3);
	mainLayout->setRowStretch(1,0);
    // Add Buttons
	mOKButton = new QPushButton("OK", this);
	mApplyButton = new QPushButton("Apply", this);
	mCancelButton = new QPushButton("Cancel", this);
	QHBoxLayout* buttonLayout = new QHBoxLayout();
	buttonLayout->addWidget(mOKButton);
	buttonLayout->addWidget(mApplyButton);
	buttonLayout->addWidget(mCancelButton);
	mainLayout->addLayout(buttonLayout, 1, 0, Qt::AlignRight);
}


// Funtion to set up the list of categories, if you want to add categories, add them here
void SettingsDialog::gernerateCategoriesList()
{
	mCategoriesListWidget = new QListWidget();
	// Create ListWidgetItems, give them StatusTips
	QListWidgetItem* allItem = new QListWidgetItem(tr("All"));
	allItem->setStatusTip("All Options are Contained in this Category for now");
	// Add them to the listwidget
	mCategoriesListWidget->addItem(allItem);
}


// Funtion to set up the stacks of options, one page per category
void SettingsDialog::gernerateOptionsStack()
{
	mOptionsStack = new QStackedWidget(); // Only page one of the stack is currently in use
	// Page 1
	QGridLayout* optionsLayout1 = new QGridLayout();
	optionsLayout1->setColumnStretch(0,1);
	optionsLayout1->setAlignment(Qt::AlignRight);
	optionsLayout1->setColumnStretch(1,3);
	QWidget* optionsWidget1 = new QWidget();

	// I/O options
	QGroupBox *configGroup = new QGroupBox(tr("I/O"));
	// logging
	QLabel* loggingLabel = new QLabel("Turn logging on");
	QCheckBox* loggingCheck = new QCheckBox();
	loggingCheck->setChecked(false);
	optionsLayout1->addWidget(loggingLabel, 0, 0);
	optionsLayout1->addWidget(loggingCheck, 0, 1);
	
	// lockfile
	QLabel* lockfileLabel = new QLabel("Enable lockfile");
	QCheckBox* lockfileCheck = new QCheckBox();
	lockfileCheck->setChecked(false);
	optionsLayout1->addWidget(lockfileLabel, 1, 0);
	optionsLayout1->addWidget(lockfileCheck, 1, 1);

	// seed
	QLabel* seedLabel = new QLabel("Seed");
	QSpinBox* seedSpinBox = new QSpinBox();
	seedSpinBox->setValue(0);
	optionsLayout1->addWidget(seedLabel, 2, 0);
	optionsLayout1->addWidget(seedSpinBox, 2, 1);

	// subfolder

	// output format

	// meta data

	optionsWidget1->setLayout(optionsLayout1);
	mOptionsStack->addWidget(optionsWidget1);
}


// Slot for activation of the OK button
void SettingsDialog::slotOKButton()
{
	emit applySettings();
	this->close();
}


// Slot for activation of the Apply button
void SettingsDialog::slotApplyButton()
{
	emit applySettings();
}

} // namespace