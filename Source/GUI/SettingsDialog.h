/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

// Qtheader
#include <qdialog.h>
#include <QtWidgets/QWidget>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/qlineedit.h>
#include <qstackedwidget.h>
#include <qgridlayout.h>
#include <QHBoxLayout>
#include <QtWidgets/qpushbutton.h>

namespace XPIWIT
{

/**
 *	@class SettingsDialog 
 *	This class enables the user to set a few options in xpiwit.
 */

class SettingsDialog :
	public QDialog
{
	Q_OBJECT

	public:
		SettingsDialog(void);
		~SettingsDialog(void);

	private:
		void init();
		void gernerateCategoriesList();
		void gernerateOptionsStack();
		QListWidget* mCategoriesListWidget;
		QStackedWidget* mOptionsStack;
		QPushButton* mOKButton;
		QPushButton* mApplyButton;
		QPushButton* mCancelButton;

	signals:
		void applySettings();
	
	private slots:
		void slotOKButton();
		void slotApplyButton();
};

} // namespace XPIWIT

#endif // SETTINGSDIALOG_H