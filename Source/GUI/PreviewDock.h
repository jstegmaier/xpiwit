/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef PREVIEWDOCK_H
#define PREVIEWDOCK_H

// ProjectHeader
#include "PipelineController.h"
#include "../Core/CMD/CMDArguments.h"

// Qt header
#include <qdialog.h>
#include <QtWidgets/QWidget>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpinBox>
#include <qdockwidget.h>
#include <qgridlayout.h>
#include <QHBoxLayout>
#include <QtWidgets/qpushbutton.h>
#include <qgraphicsitem.h>
#include <qgraphicsview.h>
#include <qgraphicsscene.h>
#include <qmessagebox.h>
#include <qcheckbox.h>
#include <qspinbox.h>
#include <QFileDialog>
#include <QWheelEvent>
#include <QPoint>
#include <QDateTime>

// itk header
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkMaximumProjectionImageFilter.h"
#include "itkImageRegionIteratorWithIndex.h"
#include "itkRescaleIntensityImageFilter.h"

namespace XPIWIT
{

/**
 *	@class PreviewDock 
 *	The PrviewDock shows a preview of the selected filter.
 */
class PreviewDock :	public QDockWidget
{
	Q_OBJECT

	public:
		/**
		 * the default constructor.
		 */
		PreviewDock();

		/**
		 * the destructor.
		 */
		virtual ~PreviewDock();

        // typedefs of the filter and image types
        typedef itk::Image<unsigned char, 2> Image2UChar;
        typedef itk::Image<unsigned char, 3> Image3UChar;
        typedef itk::Image<float, 2> Image2Float;
        typedef itk::Image<float, 3> Image3Float;
        typedef itk::ImageFileReader<Image3Float> ImageReader;
        typedef itk::ImageFileWriter<Image2UChar> ImageWriter;
        typedef itk::MaximumProjectionImageFilter<Image3Float, Image2Float> MaximumProjectionFilterFloat;
        typedef itk::MaximumProjectionImageFilter<Image3UChar, Image2UChar> MaximumProjectionFilter;
        typedef itk::RescaleIntensityImageFilter<Image3Float, Image3UChar> RescaleIntensityFilter;
        typedef itk::ImageRegionIteratorWithIndex<Image2UChar> IteratorType2D;
        typedef itk::ImageRegionIteratorWithIndex<Image3UChar> IteratorType3D;
    

		CMDPipelineArguments* mCMDArguments;
		PipelineController mPipelineController;
		void setPreview(QGraphicsItem* item);
		
	public slots:
		/**
		 * slot to update the preview.
		 */
		void update();
        void slotResetCMDArguments(CMDPipelineArguments* cmdPipelineArguments, PipelineController pipelineController);
        void slotSliceChanged(int currentSlice);
        void slotCursorPositionChanged(QPointF currentPosition);
    
    signals:
        void sliceChanged(int currentSlice);
        void cursorPositionChanged(QPointF currentPosition);
    
	private:
		QGraphicsView* mPreviewView;	// the view for the preview window
		QGraphicsScene* mPreviewScene;  // the scene for the preview window

		AbstractFilter* mCurFilter; // pointer to the current filter
		void resizeEvent(QResizeEvent *event);
		void mouseDoubleClickEvent(QMouseEvent *event);
		void mousePressEvent(QMouseEvent *event);
		void wheelEvent(QWheelEvent* event);
		void keyReleaseEvent(QKeyEvent* event);

		void UpdatePreviewImages(const QString& fileName, const int projectionDimension);
		QString mInputFile;
		bool mLockSelection;
		bool mShowMaximumProjection;
		int mColorMap;
		int mCurrentSlice;
        float mCurrentValue;
        Image3Float::Pointer mOriginalImage;
        Image2Float::Pointer mOriginalMaxProjection;
		QList<QPixmap> mImageStack;
		QDateTime mLastModified;
		QPixmap mMaximumProjection;
		QGraphicsPixmapItem* mPreviewImage;
        QPointF mCursorPosition;
		
	private slots:
		void slotSetFilter(AbstractFilter* filter);
};

} // namespace XPIWIT

#endif // PreviewDock_H
