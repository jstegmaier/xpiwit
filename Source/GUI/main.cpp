/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#define _SCL_SECURE_NO_WARNINGS

// namespace header
#include "XPIWITGUIMainWindow.h"

// project header

// qt header
#include <QtWidgets/QApplication>
#include <QtCore/QTimer>
#include <QtCore/QFile>

// system header
#include <iostream>
#include <string>

// debug memory0
//#include "vld.h"

// the namespaces used
using namespace XPIWIT;

// the main function
int main(int argc, char *argv[])
{
    
    // create the application
    QApplication app(argc, argv);

	QFile File("DarkStyle.stylesheet");
	File.open(QFile::ReadOnly);
	QString StyleSheet = QLatin1String(File.readAll());
	app.setStyleSheet(StyleSheet);

    // create the XPIWITGUI main object
    XPIWITGUIMainWindow* xpiwitGUIMainWindow = new XPIWITGUIMainWindow();
	xpiwitGUIMainWindow->show();

    QObject::connect(xpiwitGUIMainWindow, SIGNAL(signalFinished()), QApplication::instance(), SLOT(quit()));
    
	// run the application
	return app.exec();
}
