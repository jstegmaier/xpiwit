/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef XPIWITGUIMAINWINDOW_H
#define XPIWITGUIMAINWINDOW_H

// namespace header
#include "../Core/XML/AbstractPipeline.h"
#include "../Core/XML/AbstractFilter.h"
#include "../Core/XML/XMLReader.h"
//#include "../Core/XML/XMLCreator.h"
#include "../Core/XML/AbstractPipelineTools.h"
#include "../Core/Utilities/Logger.h"
#include "../GUI/PreviewDock.h"

// project header
#include "QNodesEditor/qnodeseditor.h"
#include "QNodesEditor/qneblock.h"
#include "QNodesEditor/qneport.h"
#include "ParasLineEdit.h"
#include "ParasCheckBox.h"
#include "ParasDoubleSpinBox.h"
#include "ParasSpinBox.h"
#include "SettingsDialog.h"

// qt header
#include <QtCore/QObject>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QWidget>
#include <QtWidgets/qtoolbar.h>
#include <QtWidgets/QAction>
#include <QtWidgets/qpushbutton.h>
#include <QtCore/QDateTime>
#include <QtCore/QProcess>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QBoxLayout>
#include <QtWidgets/qgridlayout.h>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QGraphicsScene>
#include <QtWidgets/qmenu.h>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/qlineedit.h>
#include <QtWidgets/qtextedit.h>
#include <QtCore/qsignalmapper.h>
#include <QMessageBox>
#include <QDropEvent>
#include <QDragEnterEvent>
#include <QSettings>
#include <QFileSystemWatcher>
#include <QFileInfo>
#include <QProgressDialog>
#include "DroppableGraphicsView.h"
#include "DropLineEdit.h"
#include "XPIWITThread.h"
#include <../../Source/Core/CMD/CMDArguments.h>
#include "qtoolbutton.h"

namespace XPIWIT
{

/**
 *	@class XPIWITGUIMainWindow
 *	Main class of the XPIWIT GUI project
 */
class XPIWITGUIMainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        XPIWITGUIMainWindow( QWidget *parent = 0 );
        ~XPIWITGUIMainWindow();

        void Init();

        // return true for failure
        bool HasFailureOccured() { return !mNoFailureOccurred; }
		bool ValidateUserInputs();
		QGraphicsPixmapItem* CreateMaximumProjection(const QString& fileName, const int projectionDimension);
    
        void resizeEvent(QResizeEvent* event)
        {
            // save the geometry settings
            QSettings settings("KIT", "XPIWIT");
            settings.setValue("geometry", saveGeometry());
        }
    
        void moveEvent(QMoveEvent* event)
        {
            // save the geometry settings
            QSettings settings("KIT", "XPIWIT");
            settings.setValue("geometry", saveGeometry());
        }
	public slots:
		void slotDisplayWarning(QString title, QString warning);

    signals:
        void signalFinished();
		void displayFilterParameter( AbstractFilter* filter);
		void addFilterBlock(AbstractFilter* filter);
		void parameterEdited(int o);
        void statusChanged(QString status);
        void updatePreviewWindows();
        void newPipelineLoaded(CMDPipelineArguments* cmdPipelineArguments, PipelineController pipelineController);
        void sliceChanged(int currentSlice);
        void cursorPositionChanged(QPointF currentPosition);
    
	private slots:
		void slotDisplayParameter( AbstractFilter* filter );
		void slotListItemClicked( QListWidgetItem* item, QListWidgetItem* previousItem = NULL );
		void slotSaveFile();
		void slotSaveAs();
		void slotLoadFile();
		void slotAddFilterBlock(AbstractFilter* filter);
		void addFilterFromFilterListItem( QListWidgetItem * item );
		void slotDisplayFilterByID( int id );
		void slotDeleteFilterByID( int id );
		void slotSetParameter(AbstractFilter* filter, QString value, int idx);
		void slotConnectionAdded(QNEPort* port1, QNEPort* port2);
		void slotConnectionDeleted(QNEConnection* connection);
        void slotUpdateStatus(QString status);
		void slotUpdateConsoleWidget();
		void slotRun();
		void slotOutputPathChanged();
		//void slotRun(AbstractPipeline* pipeline, QString inputFile);
		void slotNewFile();
		void slotImport();
        void slotQuit();
        void dropEvent(QDropEvent* event);
        void dragEnterEvent(QDragEnterEvent* event);
        void dragMoveEvent(QDragMoveEvent* event);
		void slotShowSettings();
		void slotApplySettings();
		void slotDirectoryChanged(QString directory);
		void slotHideParameterDialog();
		void slotShowPreview();
        void slotSliceChanged(int currentSlice);
        void slotCursorPositionChanged(QPointF currentPosition);

    private:
		QString mCurFile;
		QString mCurXml;
		QPoint mCurMousePos;
		bool mNoFailureOccurred;

		XPIWITThread* mXPIWITThread;
		QFileSystemWatcher* mFileSystemWatcher;
		QProgressDialog* mProgressDialog;

		// list of disabled filters
		bool mDisableSelectedFilters;
		QStringList mDisabledFilters;

		// holds the dimension of the input images
		QCheckBox* mImageDimension3D;
		QCheckBox* mAutoClearResultFolder;

		// Methods
		void CreateFilterListDock();
		void CreateParametersDock();
		void CreatePreviewDock();
		void CreateConsoleDock();
		void CreateToolBars();
		void CreateStatusBar();
		void CreateDockWindows();
		void CreateActions();
		void CreateMenus();
		void CreateCMDArgs();
		void UpdateCMDArgs();
		void setGlobalMouseTracking(bool enable);
		void ParseXmlFilterList();
		void removeLayout(QLayout* layout);
		void reset();
		void writeArgsTxt(QString filename, int imageDimension);
		void writeArgsTxt(QString filename, int imageDimension, QStringList argNames, QStringList argValues);
		void writeArgsTxt(QString filename, CMDPipelineArguments cmdargs);
		// @override
		void mouseMoveEvent(QMouseEvent *event);
        bool fileExists(QString path, bool isFolder=false);
		void testFilters();

		// GUI elements
		QToolBar* mFileToolBar;
		QToolBar* mEditToolBar;
		QWidget * mParasWidget;
		QListWidget* mFilterListWidget;
		QGridLayout* mParasLayout;
		PipelineController mPipelineController;
		QList<AbstractFilter*> mAbstractReaders;
		QList<QNEBlock*> mInputPathBlocks;
		QList<DropLineEdit*> mInputPathEdits;
		DropLineEdit* mOutputPathEdit;
		QGridLayout* mParasEditLayout;
		QVBoxLayout* mInputEditsLayout;
		QLabel* mStatusLabel;

		// Data handling elements
		QSignalMapper* mSignalMapper;
		QList<AbstractFilter*> mFilterList;
        AbstractPipeline mAbstractPipeline;
		int mCurId; // ID counter
		CMDPipelineArguments mCMDPipelineArguments;

		// Actions
		QAction* mAddAction;
		QAction* mSaveAction;
		QAction* mSaveAsAction;
		QAction* mQuitAction;
		QAction* mLoadAction;
		QAction* mNewAction;
		QAction* mRunAction;
		QAction* mImportAction;
		QAction* mSettingsAction;
		QAction* mPreviewAction;
		QToolButton* mLoadButton;
		QToolButton* mNewButton;
		QToolButton* mSaveButton;
		QToolButton* mSaveAsButton;
		QToolButton* mRunButton;
		QToolButton* mPreviewButton;
		QToolButton* mSettingsButton;
		
		// For QNodesEditor
		QMenu* mFileMenu;
		QPushButton* mAddFilterButton;
		QNodesEditor* mNodesEditor;
		QGraphicsView* mDrawingView;
		QGraphicsScene* mDrawingScene;
		QDockWidget* mConsoleDockWidget;

		// watcher for the log file
		QFileSystemWatcher*	mLogFileWatcher;
		QTextEdit* mConsoleWidget;

};

} // namespace XPIWIT

#endif  // XPIWITGUIMainWindow_H
