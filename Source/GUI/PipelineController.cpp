#include "PipelineController.h"
#include <iostream>

namespace XPIWIT{

// Constructoe
PipelineController::PipelineController()
{

}
PipelineController::PipelineController(AbstractPipeline* pipeline)
{
	this->setPipeline(pipeline);
}


// Destructor
PipelineController::~PipelineController(void)
{
}

// Extract a Subpipeline containing all filters that are needed as input for a specified filter
AbstractPipeline PipelineController::extractSubPipeline(AbstractFilter* filter)
{
	// Update Pipeline
	updatePipeline();
	// Initialize filters used in subpipieline
	QList<AbstractFilter*> usedFilters;
	QStack<AbstractFilter*> inputsStack; // Stack to iterate through inputs
	inputsStack.push(filter);
	AbstractFilter* curFilter;
	QList<AbstractInput> imageInputs;
	QList<AbstractInput> metaInputs;
	// Get a list of used FilterIds to easier compare with pipelineSequence
	QStringList usedFilterIds;
	while (inputsStack.size() > 0)
	{
		// Iterate over the stack until it's empty
		curFilter = inputsStack.pop();
		imageInputs = curFilter->GetImageInputs();
		metaInputs = curFilter->GetMetaInputs();
		// Add inputs of the current filter to the stack and to the list aff all used filters
		QString filterId;
		for (int i = 0; i < imageInputs.length(); i++)
		{
			filterId = imageInputs.at(i).mIdRef;
			if (filterId.compare("cmd")!=0 && !filterId.isEmpty()) // changed !filterId.compare("cmd")==0 to filterId.compare("cmd")!=0
			{
			AbstractFilter* newInputFilter = mPipeline.GetFilter(filterId);
			// error message if circle detected
			if (usedFilterIds.contains(newInputFilter->GetId()))
			{
				// emit showWarning("Invalid Connection", QString("Filter ") + newInputFilter->GetName() + QString(" cannot rely on its own inputs! Please correct the connection and remove any circles in the pipeline."));
				return mPipeline; // return the whole pipeline, it will be validated later
			}
			usedFilterIds.append(newInputFilter->GetId());
			inputsStack.push(newInputFilter);
			usedFilters.append(newInputFilter);
			}
		}
		for (int i = 0; i < metaInputs.length(); i++)
		{
			if (!filterId.compare("cmd") && !filterId.isEmpty())
			{
			filterId = metaInputs.at(i).mIdRef;
			AbstractFilter* newInputFilter = mPipeline.GetFilter(filterId);
			// error message if circle detected
			if (usedFilterIds.contains(newInputFilter->GetId()))
			{
				// emit showWarning("Invalid Connection", QString("Filter ") + newInputFilter->GetName() + QString(" cannot rely on its own inputs! Please correct the connection and remove any circles in the pipeline."));
				return mPipeline; // return the whole pipeline, it will be validated later
			}
			usedFilterIds.append(newInputFilter->GetId());
			inputsStack.push(newInputFilter);
			usedFilters.append(newInputFilter);
			}
		}
	}	
	// Generate results pipeline by deleting all elements that are not necessary
	QList<QString> pipelineSequence = mPipeline.GetPipelineSequence();
	usedFilters.append(filter); // Add base filter
	usedFilterIds.append(filter->GetId());
	// update mPipeline
	int numFilters = mPipeline.GetNumFilter();
	for (int i = 0; i < numFilters; i++)
	{
		// remove the Filter if it is not in the List of used Filters
		if (!usedFilterIds.contains(pipelineSequence.at(i)))
		{
			mPipeline.RemoveItemById(pipelineSequence.at(i), false);
		}
	}
	return mPipeline;
}
// Updates mPipeline with the values mMainWindowPipeline
void PipelineController::updatePipeline()
{
	mPipeline = *mMainWindowPipeline;
}

//AbstractPipeline PipelineController::extractSubPipeline(QList<AbstractFilter*> Filters)
//{
//	// If several Filters have been edited the longest subPipeline has to be found to accomodate all of them
//	AbstractPipeline* resultSubPipeline = new AbstractPipeline();
//	QList<AbstractPipeline*> subPipelines;
//	// QList<int> ids;
//	//for (int i = 0; i< Filters.length(); i++)
//	//{
//	//	//ids.append(Filters.at(i)->GetId().remove("item_").toInt());
//	//	subPipelines.append(extractSubPipeline(Filters.at(i)));
//	//}
//	//// TODO: Find longest subPipeline and return as solution
//	//return resultSubPipeline;
//	return;
//}

QString PipelineController::getFilterOutput(AbstractFilter* filter, CMDPipelineArguments *cmdPipelineArguments, QString inputName)
{
	// QString path = ProcessObjectBase::CreateOutputPath(cmdPipelineArguments, filter);
	QString path = cmdPipelineArguments->mStdOutputPath;
	// add folder format
	if( cmdPipelineArguments->mUseSubFolder )
	{
		QString subfolder = "";
		QStringList folderFormat = cmdPipelineArguments->mSubFolderFormat;
		for( int i = 0; i < folderFormat.length(); i++ )
		{
			// add filter id to name
            if( folderFormat.at( i ).compare( "filterid" ) == 0 )
				subfolder += filter->GetId();
            // add filter name to name
            if( folderFormat.at( i ).compare( "filtername" ) == 0 )
				subfolder += filter->GetName();

            // seperate values
            if( subfolder.compare("") != 0 && (i+1) != folderFormat.length() )
                subfolder += "_";
		}
        
		// create path
		QDir outputDir( path );
		if( !( outputDir.exists( subfolder ) || outputDir.mkdir( subfolder ) ) )	// if folder doesn't exists and can not be created
            return QString();
            //throw QString( "Error while creating subfolder. Path: " + path + subfolder );
        

		path += subfolder + "/";
	}

	// add image name
	QStringList fileFormat = cmdPipelineArguments->mOutputFileFormat;
	QString fileName = "";
	for( int i = 0; i < fileFormat.length(); i++ )
	{
        // add filter id to name
        if( fileFormat.at( i ).compare( "prefix" ) == 0 )
            fileName += cmdPipelineArguments->mPrefix;
        // add imagename to name
        if( fileFormat.at( i ).compare( "imagename" ) == 0 )
		{
			QString temp;
			if (inputName.isEmpty())
			{
				temp = cmdPipelineArguments->mInputImagePaths.at( cmdPipelineArguments->mDefaultIndex );
			}
			else
			{
				temp = inputName;
			}
			temp = temp.split("/").last();
			QStringList temp2 = temp.split(".");
			temp2.removeLast();
			temp = temp2.join(".");
			fileName += temp;
		}

        // add filterid to name
        if( fileFormat.at( i ).compare( "filterid" ) == 0 )
            fileName += filter->GetId();
        // add filter name to name
        if( fileFormat.at( i ).compare( "filtername" ) == 0 )
            fileName += filter->GetName();

        // seperate values
        if( fileName.compare("") != 0 && (i+1) != fileFormat.length() )
            fileName += "_";
    }
	path += fileName;

	return path;
}

// Runs the Pipeline until the filter specified
void PipelineController::runSubPipeline(AbstractFilter* filter)
{
	extractSubPipeline(filter);
	run(mPipeline);
}
// Runs the current Pipeline specidfied in mPipeline, not functioning as of yet
void PipelineController::run(AbstractPipeline pipeline)
{

}

} // namespace
