/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef DROPPABLEGRAPHICSVIEW_H
#define DROPPABLEGRAPHICSVIEW_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QListWidget>
#include <QDropEvent>
#include <QDragEnterEvent>


namespace XPIWIT
{
/**
 * @class DroppableGraphicsView 
 * GraphicsView supporting Drag and Drop events
 */
class DroppableGraphicsView : public QGraphicsView
{
	Q_OBJECT
	public:
		DroppableGraphicsView(QWidget* parent = 0) : QGraphicsView(parent)
		{
			setAcceptDrops(true);
		}
        
	signals:
		void filterDropped(QListWidgetItem*);

	protected:
		void dropEvent(QDropEvent* event)
		{
			QListWidget* listWidget = (QListWidget*)event->source();
            
			if (listWidget != NULL)
			{
				emit filterDropped(listWidget->selectedItems()[0]);
				event->acceptProposedAction();
			}
		}

		void dragMoveEvent(QDragMoveEvent* event)
		{
			event->acceptProposedAction();
		}
        
		void dragEnterEvent(QDragEnterEvent* event)
		{
			event->acceptProposedAction();
		}
};

} // namespace XPIWIT

#endif // DROPPABLEGRAPHICSVIEW_H