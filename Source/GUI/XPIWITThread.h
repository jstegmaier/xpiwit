/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H¸bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H¸bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef XPIWITTHREAD_H
#define XPIWITTHREAD_H

// include required headers
#include <QThread>
#include <QProcess>

/**
 * class to start XPIWIT in a separate thread and keep the GUI responsive.
 */
class XPIWITThread : public QThread
{
    Q_OBJECT

public:
    XPIWITThread(QObject* parent = NULL) : QThread( parent )
    {
        setTerminationEnabled();
    }
    
    void run() 
	{
#ifdef _WIN32
        system( (QString("XPIWIT.exe < temp.txt")).toStdString().c_str() );
#else
        system( (QString("./XPIWIT < temp.txt")).toStdString().c_str() );
#endif
    }

public slots:
	void stop()
	{
        if (this->isFinished())
            return;
        
#ifdef _WIN32
        system( "taskkill /IM XPIWIT.exe /F" );
#else
        QProcess killProcess(this);
        killProcess.start("killall XPIWIT");
        system( "killall XPIWIT" );
#endif
	}

signals:
    void resultReady(const QString &s);
};

#endif
