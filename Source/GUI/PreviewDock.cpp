/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H¸bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H¸bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#include "PreviewDock.h"
#include "qgroupbox.h"
#include <QSizePolicy>


namespace XPIWIT {

// Constructor
PreviewDock::PreviewDock()
{
	// Init NULL pointers
	mCurFilter = NULL;
	mCMDArguments = NULL;
	// Title and window options
	this->setWindowTitle(tr("Preview"));
	this->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable | QDockWidget::DockWidgetClosable);
	QWidget* widget = new QWidget();
	this->setWidget(widget);
	this->setFloating(false);
    this->setMouseTracking(true);
    
	// Preview
	mPreviewScene = new QGraphicsScene();
        
	mPreviewView = new QGraphicsView();
	mPreviewView->setParent(this);
	mPreviewView->setScene(mPreviewScene);
	mPreviewView->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
    
	// Add general layout
	QGridLayout* mainLayout = new QGridLayout();
    mainLayout->setMargin(5);
    mainLayout->setSpacing(5);

	// Add the Preview widgets
	mainLayout->addWidget(mPreviewView, 0, 0, 5, 5);

	mColorMap = 2;
	mLockSelection = false;
	mShowMaximumProjection = true;
	mCurrentSlice = 0;
    mCursorPosition = QPointF(0,0);
    mCurrentValue = 0.0;
    
	// Show views
	mPreviewView->show();

	// Set mainlayout for dock
	widget->setLayout(mainLayout);
}

// Destructor
PreviewDock::~PreviewDock(void)
{
    mCMDArguments = NULL;
    mCurFilter = NULL;
	delete mPreviewView;
	delete mPreviewScene;
}


// handle resize events
void PreviewDock::resizeEvent(QResizeEvent* event)
{
	// Resize the scenes with the views
	mPreviewView->fitInView( mPreviewScene->sceneRect(), Qt::KeepAspectRatio );
}


// lock/unlock the selected filter with double click
void PreviewDock::mouseDoubleClickEvent(QMouseEvent* event)
{
	mLockSelection = !mLockSelection; 
	update();
}


// set the preview image
void PreviewDock::setPreview(QGraphicsItem* item)
{
    // set the pixmap for visualization
	mPreviewScene->clear();
	mPreviewScene->addItem(item);
    mPreviewScene->setSceneRect(item->boundingRect());
    mPreviewView->fitInView( mPreviewScene->sceneRect(), Qt::KeepAspectRatio );

    // draw crosshair at the cursor location
    int widgetOffset = (this->width() - mPreviewView->width()) / 2;
    float minSideLength = std::min<float>((float)this->width(), (float)this->height()) - 2*widgetOffset;
    QPointF localPos = mPreviewView->mapToScene(mCursorPosition.x()*minSideLength + this->width()/2 - widgetOffset, mCursorPosition.y()*minSideLength + this->height()/2 - widgetOffset);
    mPreviewScene->addLine(localPos.x()-10, localPos.y(), localPos.x() + 10 , localPos.y(), QColor(255, 255, 255));
    mPreviewScene->addLine(localPos.x(), localPos.y() - 10, localPos.x(), localPos.y() + 10, QColor(255, 255, 255));
    
    // show value under the cursor
    if (mShowMaximumProjection == true)
    {
        typename Image2Float::IndexType index;
        index[0] = std::max<int>(0, std::min<int>(int(localPos.x()), mOriginalMaxProjection->GetLargestPossibleRegion().GetSize()[0]-1));
        index[1] = std::max<int>(0, std::min<int>(int(localPos.y()), mOriginalMaxProjection->GetLargestPossibleRegion().GetSize()[1]-1));
        mCurrentValue = mOriginalMaxProjection->GetPixel(index);
    }
    else
    {
        typename Image3Float::IndexType index;
        index[0] = std::max<int>(0, std::min<int>(int(localPos.x()), mOriginalImage->GetLargestPossibleRegion().GetSize()[0]-1));
        index[1] = std::max<int>(0, std::min<int>(int(localPos.y()), mOriginalImage->GetLargestPossibleRegion().GetSize()[1]-1));
        index[2] = std::max<int>(0, std::min<int>(int(mCurrentSlice), mOriginalImage->GetLargestPossibleRegion().GetSize()[2]-1));
        mCurrentValue = mOriginalImage->GetPixel(index);
    }
}


// update the preview
void PreviewDock::update()
{
	// Abort if window has not yet been properly initialized, checked by looking at a pointer
	if (mCMDArguments == NULL)
		return;

	// if filter output file exists set preview to it
	QString previewState = "Preview (";
	if (mShowMaximumProjection == true)
		previewState = previewState + QString("Max. Proj.");
	else
		previewState = previewState + QString("Slice ") + QString::number(mCurrentSlice+1) + QString(" / " ) + QString::number(mImageStack.size());

	if (mLockSelection == true)
		previewState = previewState + QString(", Locked");
	else
		previewState = previewState + QString(", Live");
    
    previewState = previewState + QString().asprintf(", Value = %.2f): ", mCurrentValue);

	// try to update the preview
	bool previewFailed = true;
	if (mCurFilter != NULL)
	{
		QString filterOutPath = mPipelineController.getFilterOutput(mCurFilter, mCMDArguments, mInputFile);
		filterOutPath += "_Out" + QString::number(1) + ".tif";
        		
		// QFileInfo needed to check for file existence
		QFileInfo checkFile(filterOutPath);
		if (checkFile.exists() && checkFile.isFile())
		{
			// set the window title
			setWindowTitle(tr((previewState + mCurFilter->GetName()).toStdString().c_str()));

			// update the preview image if it was modified
			if (mLastModified != checkFile.lastModified())
			{
				UpdatePreviewImages(filterOutPath, 2);
				mLastModified = checkFile.lastModified();
			}

			// show the current slice or the maximum projection
			if (mShowMaximumProjection == true)
				mPreviewImage = new QGraphicsPixmapItem(mMaximumProjection);
			else
				mPreviewImage = new QGraphicsPixmapItem(mImageStack[mCurrentSlice]);

			setPreview(mPreviewImage);
			previewFailed = false;
		}
	}

	// display info message if no preview is available
	if (previewFailed == true)
	{
		mPreviewScene->clear();
		QGraphicsSimpleTextItem * simpleTextItem1 = mPreviewScene->addSimpleText(QString("No filter selected or no output generated for selected filter. \nEnable the write image flag of the desired filter and run the pipeline. \n\nHint:\n-Double-Click to lock/unlock preview for selected filter\n-Right click to toggle max. proj/slice view\n-Mouse-wheel to navigate in a stack"));
		mPreviewScene->setSceneRect(simpleTextItem1->boundingRect());
		mPreviewView->fitInView( mPreviewScene->sceneRect(), Qt::KeepAspectRatio );
		simpleTextItem1->setBrush(QColor(177, 177, 177));
	}
}


// function to set the current filter
void PreviewDock::slotSetFilter(AbstractFilter* filter)
{
	// check if the selection is locked
	if (mLockSelection == true)
		return;
	
	// if in live mode, set the preview to the current filter
	mCurFilter = filter;
	mLastModified = QDateTime();
	QString newPreviewLabelText = "Output preview for " + mCurFilter->GetName();
	if (this->isVisible())
		this->update();
}


void PreviewDock::slotResetCMDArguments(CMDPipelineArguments* cmdPipelineArguments, PipelineController pipelineController)
{
    mPipelineController = pipelineController;
    mCMDArguments = cmdPipelineArguments;
    update();
}


void PreviewDock::slotSliceChanged(int currentSlice)
{
    mCurrentSlice = std::max<int>(0, std::min<int>(currentSlice, mImageStack.size() - 1));;
    update();
}

////// TEMP COLORMAP

typedef struct {
	double r, g, b;
} COLOUR;

COLOUR GetColour(double v, double vmin, double vmax)
{
	COLOUR c = { 1.0,1.0,1.0 }; // white
	double dv;

	if (v < vmin)
		v = vmin;
	if (v > vmax)
		v = vmax;
	dv = vmax - vmin;

	if (v < (vmin + 0.25 * dv)) {
		c.r = 0;
		c.g = 4 * (v - vmin) / dv;
	}
	else if (v < (vmin + 0.5 * dv)) {
		c.r = 0;
		c.b = 1 + 4 * (vmin + 0.25 * dv - v) / dv;
	}
	else if (v < (vmin + 0.75 * dv)) {
		c.r = 4 * (v - vmin - 0.5 * dv) / dv;
		c.b = 0;
	}
	else {
		c.g = 1 + 4 * (vmin + 0.75 * dv - v) / dv;
		c.b = 0;
	}

	return(c);
}
//////// END TEMP


// update the cached preview images (TODO: potentially outsource this to a thread to improve the performance if images are large)
void PreviewDock::UpdatePreviewImages(const QString& fileName, const int projectionDimension)
{
	// clear all old images
	mImageStack.clear();

	// create a reader object and pass the file name to it
	ImageReader::Pointer reader = ImageReader::New();
	reader->SetFileName(fileName.toStdString());
    mOriginalImage = reader->GetOutput();
    
    // perform maximum projection of the original image
    MaximumProjectionFilterFloat::Pointer maxProjFilterFloat = MaximumProjectionFilterFloat::New();
    maxProjFilterFloat->SetInput(mOriginalImage);
    maxProjFilterFloat->SetProjectionDimension(projectionDimension);
    maxProjFilterFloat->Update();
    mOriginalMaxProjection = maxProjFilterFloat->GetOutput();
    
	// perform a contrast adjustment
	RescaleIntensityFilter::Pointer rescaleFilter = RescaleIntensityFilter::New();
	rescaleFilter->SetInput(mOriginalImage);
	rescaleFilter->Update();
    	
	// perform the maximum projection along the specified dimension
	MaximumProjectionFilter::Pointer maxProjFilter = MaximumProjectionFilter::New();
	maxProjFilter->SetInput(rescaleFilter->GetOutput());
	maxProjFilter->SetProjectionDimension(projectionDimension);
	maxProjFilter->Update();
	Image2UChar::Pointer xyProjection = maxProjFilter->GetOutput();
	
	// h and w are the sizes of the image
	const int width = xyProjection->GetLargestPossibleRegion().GetSize()[0];
	const int height = xyProjection->GetLargestPossibleRegion().GetSize()[1];
	const int depth = reader->GetOutput()->GetLargestPossibleRegion().GetSize()[2];
	QImage qtImage(width, height, QImage::Format_Indexed8);

	// create a gray colormap for the image
	for (unsigned int i=0;i<256;i++)
	{
		COLOUR mycolor = GetColour(i, 0, 256);
		if (mColorMap == 0)
		{
			qtImage.setColor(i, QColor(rand() % 256, rand() % 256, rand() % 256).rgb());
			if (i == 0)
				qtImage.setColor(i, QColor(0, 0, 0).rgb());
		}
		else if (mColorMap == 1)
			qtImage.setColor(i, QColor(255*mycolor.r, 255 * mycolor.g, 255 * mycolor.b).rgb());
		else
			qtImage.setColor(i, QColor(i,i,i).rgb());
	}

	// initialize a temporary stack as qt images
	QList<QImage> tmpImageStack;
	for (int i = 0; i < depth; ++i)
		tmpImageStack.append(qtImage);

	// define the stack iterator and fill the images
	IteratorType3D stackIterator(rescaleFilter->GetOutput(), rescaleFilter->GetOutput()->GetLargestPossibleRegion());
	stackIterator.GoToBegin();
	while (!stackIterator.IsAtEnd())
	{
		tmpImageStack[stackIterator.GetIndex()[2]].setPixel(stackIterator.GetIndex()[0], stackIterator.GetIndex()[1], stackIterator.Get());
		++stackIterator;
	}

	// convert the qt images to pixmaps
	for (int i = 0; i < depth; ++i)
		mImageStack.append(QPixmap::fromImage(tmpImageStack[i]));

	// initialize the image iterator
	IteratorType2D sliceIterator(xyProjection, xyProjection->GetLargestPossibleRegion().GetSize());
	sliceIterator.GoToBegin();

	// loop over the image and set the respective pixels
	for (int i=0; i<height; ++i)
	{
		for (int j=0; j<width; ++j)
		{
			qtImage.setPixel(j, i, sliceIterator.Get());
			++sliceIterator;
		}
	}
	mMaximumProjection = QPixmap::fromImage(qtImage);
}


// handle mouse clicks
void PreviewDock::mousePressEvent(QMouseEvent* event)
{
	if (event->button() == Qt::RightButton)
	{
		mShowMaximumProjection = !mShowMaximumProjection;
		update();
	}
    
    if (event->button() == Qt::LeftButton)
    {
        int widgetOffset = (this->width() - mPreviewView->width()) / 2;
        float minSideLength = std::min<float>((float)this->width(), (float)this->height()) - 2*widgetOffset;
        mCursorPosition = QPointF((event->pos().x() - this->width()/2) / minSideLength, (event->pos().y() - this->height()/2) / minSideLength);
        
        emit cursorPositionChanged(mCursorPosition);
        update();
    }
}


// handle key press events
void PreviewDock::keyReleaseEvent(QKeyEvent* event)
{
	std::cout << event->key() << std::endl;
	if (event->key() == Qt::Key_Right && mShowMaximumProjection == false)
		mCurrentSlice = std::max<int>(0, std::min<int>(mCurrentSlice + 1, mImageStack.size() - 1));
	else if (event->key() == Qt::Key_Left && mShowMaximumProjection == false)
		mCurrentSlice = std::max<int>(0, std::min<int>(mCurrentSlice - 1, mImageStack.size() - 1));
	else if (event->key() == Qt::Key_C)
	{
		mColorMap = (mColorMap + 1) % 3;
		mLastModified = QDateTime();
	}		

	update();
}


// handle mouse wheel events
void PreviewDock::wheelEvent(QWheelEvent* event)
{
	QPoint numPixels = event->pixelDelta();
	QPoint numDegrees = event->angleDelta() / 8;

	// identify scroll direction and update the current slice
	if (mShowMaximumProjection == false)
	{
		if (numDegrees.y() < 0)
			//parameters->SetCurrentSlice( std::max<int>(0, std::min<int>(parameters->GetCurrentSlice()+1, mSegmentationProject->GetNumSlices() - 1)) );
			mCurrentSlice = std::max<int>(0, std::min<int>(mCurrentSlice + 1, mImageStack.size() - 1));
		else
			mCurrentSlice = std::max<int>(0, std::min<int>(mCurrentSlice - 1, mImageStack.size() - 1));
		update();
	}

	// update the images
	event->accept();
    
    // emit slice change signal
    emit sliceChanged(mCurrentSlice);
}

void PreviewDock::slotCursorPositionChanged(QPointF currentPosition)
{
    mCursorPosition = currentPosition;
    update();
}

} // namespace
