/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// file header
#include "XPIWITGUIMainWindow.h"
#include "../Core/Utilities/AbstractPipelineXMLCreator.h"
#include "DropLineEdit.h"

// qt header
#include <iostream>
#include <QFile>
#include <QTextStream>
#include <QDropEvent>
#include <QDragEnterEvent>
#include <QApplication>
#include <QProgressDialog>
#include <QProgressBar>
#include <QSettings>
#include <QDir>
#include <QDirIterator>
#include <QScrollBar>
#include "XPIWITThread.h"
#include "QNodesEditor/qneconnection.h"
#include <QFileSystemWatcher>


namespace XPIWIT
{
// The contructor
XPIWITGUIMainWindow::XPIWITGUIMainWindow( QWidget* parent ) : QMainWindow( parent )
{
	// initialize the list of disabled filters
	mDisableSelectedFilters = true;
	mDisabledFilters << "VesselnessMesasureImageFilter" //<< "GPUHessianToObjectnessMeasureImageFilter"
					 //<< "SliceBySliceAdjustIntensityImageFilter" << "SliceBySliceAttenuateIntensityImageFilter" << "SliceBySliceExtractRegionPropsImageFilter" << "SliceBySliceIntersectionsImageFilter" << "SliceBySliceFusionFilter"
		//<< "ACMEMultiScalePlateMeasureFilter" << "ACMEWatershedSegmentationFilter"
		<< "FuseRotationImagesFilter" << "UncertaintyBasedMultiplicationFilter" << "UncertaintyGuidedWatershedFilter" << "ExtractIntensityProfileFilter" << "MorphologicalClosingFilter"
		<< "MorphologicalOpeningFilter" << "MARSWatershedFromMarkersImageFilter"
		<< "LabelImageFilter" << "RegionBasedWatershedImageFilter" << "SplitConnectedBlobsFilter" << "ThresholdFilter"
		<< "WatershedFilter" << "CastImageFilter" << "JunctionDetectionFilter" << "ImageWriter";
					//<< "GPUMedianImageFilter" << "GPUHMaximaImageFilter" << "GPUMorphologicalWatershedFilter" << "GPUFastMorphologicalOperatorsFilter" << "GPUHessianToObjectnessMeasureImageFilter";
	
	// call the init function
	Init();
	
	// Connections
	connect( this, SIGNAL( displayFilterParameter(AbstractFilter*) ), this, SLOT( slotDisplayParameter( AbstractFilter *) ) );
	connect( mFilterListWidget, SIGNAL( itemDoubleClicked( QListWidgetItem *) ), this, SLOT( addFilterFromFilterListItem( QListWidgetItem *) ) );
	connect( this, SIGNAL( addFilterBlock(AbstractFilter*) ), this, SLOT( slotAddFilterBlock( AbstractFilter* ) ) );
	connect( mNodesEditor, SIGNAL( backgroundClicked() ), this, SLOT( slotHideParameterDialog() ) );
	connect( mNodesEditor, SIGNAL( blockClicked( int ) ), this, SLOT( slotDisplayFilterByID( int ) ) );
	connect( mNodesEditor, SIGNAL( deleteBlock( int ) ), this, SLOT( slotDeleteFilterByID( int ) ) );
	connect( mNodesEditor, SIGNAL( connectionAdded(QNEPort*, QNEPort*) ), this, SLOT( slotConnectionAdded(QNEPort*, QNEPort*) ) );
	connect( mNodesEditor, SIGNAL( connectionDeleted( QNEConnection* ) ), this, SLOT( slotConnectionDeleted( QNEConnection* ) ) );

    // connect the status bar
    connect( this, SIGNAL(statusChanged(QString)), this, SLOT(slotUpdateStatus(QString)) );

	// Test Filterlist, only uncomment if you you want to test all filters in the Filterlist
	// testFilters();
}


// The destructor
XPIWITGUIMainWindow::~XPIWITGUIMainWindow()
{    
	// delete Pointers from heap
	delete mFileToolBar;
	delete mEditToolBar;
	delete mAddAction;
	delete mSaveAction;
	delete mQuitAction;
	delete mLoadAction;
	delete mLoadButton;
	delete mNewButton;
	delete mSaveButton;
	delete mSaveAsButton;
	delete mRunButton;
	delete mPreviewButton;
	delete mSettingsButton;
	delete mFileMenu;
	delete mAddFilterButton;
	delete mNodesEditor;
	delete mDrawingView;
	delete mDrawingScene;
    delete mXPIWITThread;
	removeLayout(mParasLayout);
}

    
// Init function to intitialize the basic layout of the GUI
void XPIWITGUIMainWindow::Init()
{
	// Instatiate member variables
	mCurId = 1;
	mCurFile = "";
	mAbstractPipeline = AbstractPipeline();
	mLogFileWatcher = NULL;
	mConsoleDockWidget = NULL;

	// Create Drawing Area
	mDrawingScene = new QGraphicsScene();
	mDrawingView = new DroppableGraphicsView();
	mDrawingView->setScene(mDrawingScene);
	mDrawingView->setRenderHint(QPainter::Antialiasing, true);
    mDrawingView->setAcceptDrops(true);
	mNodesEditor = new QNodesEditor(this);
	mNodesEditor->install(mDrawingScene);
    setCentralWidget( mDrawingView );

	// initialize the components
	CreateActions();
	CreateDockWindows();
	CreateToolBars();
	CreateMenus();
    CreateStatusBar();
	
    // maximize the window or load previous settings if they exist
    //showMaximized();
    QSettings settings("KIT", "XPIWIT");
    restoreGeometry(settings.value("geometry").toByteArray());

    // enable drag&drop
    setAcceptDrops( true );

	// enable mouse tracking
	// setGlobalMouseTracking( true );
    
    // connect signals and slots
    connect( mDrawingView, SIGNAL( filterDropped( QListWidgetItem* ) ), this, SLOT( addFilterFromFilterListItem( QListWidgetItem *) ) );

	// make sure the XPIWIT executable exists
	#ifdef _WIN32
		QFileInfo checkFile("XPIWIT.exe");
	#else
		QFileInfo checkFile("XPIWIT");
	#endif
	if (!checkFile.exists() || !checkFile.isFile() || !checkFile.isExecutable())
		QMessageBox::warning(this, "XPIWIT Executable Not Found", "The XPIWIT executable was not found. Make sure XPIWIT is located in the same folder as the GUI.");

	mXPIWITThread = new XPIWITThread(this);
	mFileSystemWatcher = new QFileSystemWatcher(this);
	connect( mFileSystemWatcher, SIGNAL(directoryChanged(QString)), this, SLOT(slotDirectoryChanged(QString)) );

	// create a new process dialog
	mProgressDialog = new QProgressDialog("Processing ...", "Cancel", 0, 0, this);
	mProgressDialog->setWindowModality(Qt::WindowModal);
	mProgressDialog->setHidden(true);
	mProgressDialog->close();

	connect(mXPIWITThread, SIGNAL(finished()), mProgressDialog, SLOT(close()));
	connect(mProgressDialog, SIGNAL(canceled()), mXPIWITThread, SLOT(stop()));

	// Set default CMDArgs
	CreateCMDArgs();
}


// Creates the dock windows
void XPIWITGUIMainWindow::CreateDockWindows()
{
	// Parse filter list
	ParseXmlFilterList();
    // Create dock containing filters
	CreateFilterListDock();
	// Create parameters dock
	CreateParametersDock();

	CreateConsoleDock();


}


void XPIWITGUIMainWindow::CreateConsoleDock()
{
	// create the log file watcher
	mLogFileWatcher = new QFileSystemWatcher();
	connect(mLogFileWatcher, SIGNAL(directoryChanged(QString)), this, SLOT(slotUpdateConsoleWidget()));
	connect(mOutputPathEdit, SIGNAL(textChanged(QString)), this, SLOT(slotOutputPathChanged()));

	mConsoleWidget = new QTextEdit(this);
	mConsoleWidget->setReadOnly(true);

	// create the dock widgets
	mConsoleDockWidget = new QDockWidget(tr("Console Output"), this);
	mConsoleDockWidget->setAllowedAreas(Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea);
	mConsoleDockWidget->setWidget(mConsoleWidget);
	mConsoleDockWidget->setFeatures(QDockWidget::DockWidgetFloatable | QDockWidget::DockWidgetMovable);
	addDockWidget(Qt::BottomDockWidgetArea, mConsoleDockWidget);
}

void XPIWITGUIMainWindow::slotOutputPathChanged()
{
	mLogFileWatcher->removePaths(mLogFileWatcher->directories());
	mLogFileWatcher->removePaths(mLogFileWatcher->files());
	mLogFileWatcher->addPath(mOutputPathEdit->GetText() + "result/");
}


// Initilaizesthe CMDPipelineArguments with default values
void XPIWITGUIMainWindow::CreateCMDArgs()
{
	UpdateCMDArgs();
	mCMDPipelineArguments.mXmlPath = QDir::currentPath() + QString("/temp.xml");
	mCMDPipelineArguments.mSeed = 0;
	mCMDPipelineArguments.mLockFileEnabled = false;
	mCMDPipelineArguments.mUseSubFolder = true;
	mCMDPipelineArguments.mSubFolderFormat.append("filterid");
	mCMDPipelineArguments.mSubFolderFormat.append("filtername");
	mCMDPipelineArguments.mOutputFileFormat.append("imagename");
	mCMDPipelineArguments.mOutputFileFormat.append("filtername");
}


// update the command line arguments variable
void XPIWITGUIMainWindow::UpdateCMDArgs()
{
	mCMDPipelineArguments.mStdOutputPath = mOutputPathEdit->GetText();
	mCMDPipelineArguments.mInputImagePaths.clear();
	QString inputFile = "";
	for (int i = 0; i < mInputPathEdits.length(); ++i)
	{
		QFileInfo inputInfo = QFileInfo(mInputPathEdits[i]->GetText());
		if (inputInfo.isDir())
		{
			QDir inputDir = QDir(mInputPathEdits[i]->GetText());
			inputFile = inputDir.path() + "/" + inputDir.entryList().at(2);
			mCMDPipelineArguments.mInputImagePaths.append(inputFile);
		}
		else if (!mInputPathEdits[i]->GetText().isEmpty()){
			mCMDPipelineArguments.mInputImagePaths.append(mInputPathEdits[i]->GetText());
		}
	}
    
    // inform preview windows about the changes
    emit(updatePreviewWindows());
}

// update the console widget if the log file changes
void XPIWITGUIMainWindow::slotUpdateConsoleWidget()
{
	// check the results folder for log files
	QDir dir(mOutputPathEdit->GetText() + "result/", "*PipelineLog.txt", QDir::SortFlags(QDir::Time), QDir::Files);
	
	// skip log file display if no files exist
	QDirIterator dirIterator(dir);
	if (dirIterator.hasNext() == false)
		return;

	// identify the latest file
	QString fileName = dirIterator.next();
	while (dirIterator.hasNext())
	{
		QString currentFile = dirIterator.next();
		if (!currentFile.contains("Overview_"))
			fileName = currentFile;
	}

	// check if console is currently scrolled to the bottom
	bool scrolledToBottom = false;
	QScrollBar* scrollBar = mConsoleWidget->verticalScrollBar();
	int position = scrollBar->value();

	if (position >= scrollBar->maximum() - 10)
		scrolledToBottom = true;

	// read the text
	QFile logFile(fileName);
	logFile.open(QIODevice::ReadOnly | QIODevice::Text);
	QTextStream in(&logFile);

	// set the text and scroll to the bottom
	mConsoleWidget->setPlainText(in.readAll());

	if (scrolledToBottom == true)
	{
		QTextCursor textCursor = mConsoleWidget->textCursor();
		textCursor.movePosition(QTextCursor::End);
		mConsoleWidget->setTextCursor(textCursor);
	}
	else
	{
		scrollBar->setValue(position);
	}
}



// Creates dock containing filters list
void XPIWITGUIMainWindow::CreateFilterListDock()
{
	QDockWidget *dock = new QDockWidget(tr("Filters"), this);
	dock->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable); // This dockwidget is not closeable
	mFilterListWidget = new QListWidget;
	// Add items with filter names
	for (int i = 0; i < mFilterList.length(); i++)
	{
		mFilterListWidget->addItem(mFilterList.value(i)->GetName());
		mFilterListWidget->item(i)->setToolTip(mFilterList.value(i)->GetDesription());
	}
    mFilterListWidget->sortItems();
	dock->setWidget(mFilterListWidget);
	dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
    mFilterListWidget->setDragEnabled(true);
	dock->geometry();
	addDockWidget(Qt::RightDockWidgetArea, dock);
}


// Creates the dock displaying parameters
void XPIWITGUIMainWindow::CreateParametersDock()
{
	QDockWidget* dock = new QDockWidget(tr("Parameters"), this);
	dock->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetFloatable);
	mParasLayout = new QGridLayout();
	mParasWidget = new QWidget();
	// Add input output paths
	mImageDimension3D = new QCheckBox( "3D Input Images" );
	mImageDimension3D->setChecked(true);
	mImageDimension3D->setToolTip( "If enabled, 3D input images are assumed. 3D images can also be composed of a series of 2D images as described in the quick start guide." );
	mParasLayout->addWidget(mImageDimension3D, 0, 0, 1, 1);

	mAutoClearResultFolder = new QCheckBox( "Clear Output Folder" );
	mAutoClearResultFolder->setToolTip( "If enabled, all contents of the current output folder are deleted before processing is started." );
	mParasLayout->addWidget(mAutoClearResultFolder, 0, 1, 1, 1);

	mInputEditsLayout = new QVBoxLayout();
	mParasLayout->addLayout(mInputEditsLayout, 1, 0, 1, 5);
		
	mOutputPathEdit = new DropLineEdit("Output path:", "Drag&Drop Output Path Here");
	connect(mOutputPathEdit, SIGNAL(textChanged(QString)), this, SLOT(slotUpdateStatus(QString)));
	mParasLayout->addWidget(mOutputPathEdit, 2, 0, 1, 5);

	// Set row stretches to control size (and resize behavior)
	mParasLayout->setRowStretch(0,0);
	mParasLayout->setRowStretch(1,0);
	mParasLayout->setRowStretch(2,0);
	mParasLayout->setRowStretch(3,0);

	// Add seperator
	int columnCount = mParasLayout->columnCount();
	QFrame* sepFrame = new QFrame();
	sepFrame->setFrameShape(QFrame::HLine);
	mParasLayout->addWidget(sepFrame, 4, 0, 1, columnCount);
	mParasLayout->setRowStretch(4,0);
	
	// Add layout displaying the parameters
	mParasEditLayout = new QGridLayout();
	mParasEditLayout->setColumnStretch(4,1);
	mParasLayout->addLayout(mParasEditLayout, 5, 0, 1, columnCount);

	// Add stretch (empty space for later paras)
	QVBoxLayout* stretch = new QVBoxLayout();
	stretch->addStretch();
	mParasLayout->addLayout(stretch, 6, 0, 1, columnCount);

	// Manage stretch factors
	mParasLayout->setRowStretch(6,1);

	// Display
	mParasWidget->setLayout(mParasLayout);
	dock->setWidget(mParasWidget);
	dock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	dock->show();
	addDockWidget(Qt::RightDockWidgetArea, dock);
}


// Creates the Menus, currently  not used
void XPIWITGUIMainWindow::CreateMenus()
{
	//fileMenu = menuBar()->addMenu(tr("&File"));
	//fileMenu->addAction(loadAction);
	//fileMenu->addAction(saveAction);
	//fileMenu->addSeparator();
	//fileMenu->addAction(quitAction);
}


// Creates the Toolbars
void XPIWITGUIMainWindow::CreateToolBars()
{
	// File Toolbar
	mFileToolBar = addToolBar(tr("&File"));
	mFileToolBar->addWidget(mNewButton);
	mFileToolBar->addWidget(mLoadButton);
	mFileToolBar->addWidget(mSaveButton);
	mFileToolBar->addWidget(mSaveAsButton);

	// Edit toolbar
	mEditToolBar = addToolBar(tr("&Edit"));
	mEditToolBar->addWidget(mRunButton);
	mEditToolBar->addWidget(mPreviewButton);
	// mEditToolBar->addWidget(mSettingsButton);
}


// Creates the actions used in menus/toolbars
void XPIWITGUIMainWindow::CreateActions()
{
	mQuitAction = new QAction(tr("&Quit"), this);
	mQuitAction->setShortcuts(QKeySequence::Quit);
	mQuitAction->setStatusTip(tr("Quit the application"));
	//connect(mQuitAction, SIGNAL(triggered()), this, SLOT(slotQuit()));

	mLoadAction = new QAction(tr("&Load"), this);
	mLoadAction->setShortcuts(QKeySequence::Open);
	mLoadAction->setStatusTip(tr("Load pipeline"));
	connect(mLoadAction, SIGNAL(triggered()), this, SLOT(slotLoadFile()));
	QIcon loadIcon = QIcon("./images/load_icon.png");
	mLoadAction->setIcon(loadIcon);
	mLoadButton = new QToolButton();
	mLoadButton->setDefaultAction(mLoadAction);
	mLoadButton->setText(tr("Load"));
	mLoadButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);

	mSaveAction = new QAction(tr("&Save"), this);
	mSaveAction->setShortcuts(QKeySequence::Save);
	mSaveAction->setStatusTip(tr("Save a file"));
	connect(mSaveAction, SIGNAL(triggered()), this, SLOT(slotSaveFile()));
	QIcon saveIcon = QIcon("./images/save_icon.png");
	mSaveAction->setIcon(saveIcon);
	mSaveButton = new QToolButton();
	mSaveButton->setDefaultAction(mSaveAction);
	mSaveButton->setText(tr("Save"));
	mSaveButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);

	mNewAction =  new QAction(tr("&New"), this);
	mNewAction->setShortcuts(QKeySequence::New);
	mNewAction->setStatusTip(tr("Open a new file"));
	connect(mNewAction, SIGNAL(triggered()), this, SLOT(slotNewFile()));
	QIcon newIcon = QIcon("./images/new_icon.png");
	mNewAction->setIcon(newIcon);
	mNewButton = new QToolButton();
	mNewButton->setDefaultAction(mNewAction);
	mNewButton->setText(tr("New"));
	mNewButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);

	mRunAction = new QAction(tr("&Run"), this);
	mRunAction->setShortcut(tr("CTRL+F5"));
	mRunAction->setStatusTip(tr("Run the current pipeline"));
	connect(mRunAction, SIGNAL(triggered()), this, SLOT(slotRun()));
	QIcon runIcon = QIcon("./images/run_icon.png");
	mRunAction->setIcon(runIcon);
	mRunButton = new QToolButton();
	mRunButton->setDefaultAction(mRunAction);
	mRunButton->setText(tr("Run"));
	mRunButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);

	/*
	mImportAction = new QAction(tr("&Import"), this);
	mImportAction->setStatusTip(tr("Import an existing XML pipeline"));
	connect(mImportAction, SIGNAL(triggered()), this, SLOT(slotImport()));*/

	mSettingsAction = new QAction(tr("&Settings"), this);
	mSettingsAction->setStatusTip(tr("Open Settings Window"));
	connect(mSettingsAction, SIGNAL(triggered()), this, SLOT(slotShowSettings()));
	QIcon settingsIcon = QIcon("./images/settings_icon.png");
	mSettingsAction->setIcon(settingsIcon);
	mSettingsButton = new QToolButton();
	mSettingsButton->setDefaultAction(mSettingsAction);
	mSettingsButton->setText(tr("Settings"));
	mSettingsButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);

	mSaveAsAction = new QAction(tr("&Save as..."), this);
	mSaveAsAction->setStatusTip(tr("Save file as..."));
	connect(mSaveAsAction, SIGNAL(triggered()), this, SLOT(slotSaveAs()));
	QIcon saveAsIcon = QIcon("./images/saveAs_icon.png");
	mSaveAsAction->setIcon(saveAsIcon);
	mSaveAsButton = new QToolButton();
	mSaveAsButton->setDefaultAction(mSaveAsAction);
	mSaveAsButton->setText(tr("Save as"));
	mSaveAsButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);

	mPreviewAction = new QAction(tr("Preview"), this);
	mPreviewAction->setStatusTip(tr("Preview Results for currently selecetd Filter"));
	connect(mPreviewAction, SIGNAL(triggered()), this, SLOT(slotShowPreview()));
	QIcon previewIcon = QIcon("./images/preview_icon.png");
	mPreviewAction->setIcon(previewIcon);
	mPreviewButton = new QToolButton();
	mPreviewButton->setDefaultAction(mPreviewAction);
	mPreviewButton->setText(tr("Preview"));
	mPreviewButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
}


// Creates statusbar in the bttom of the window, curerntly not used
void XPIWITGUIMainWindow::CreateStatusBar()
{
	mStatusLabel = new QLabel("Status updates and information is displayed here.");
	mStatusLabel->setAlignment(Qt::AlignLeft);
	mStatusLabel->setMinimumSize(mStatusLabel->sizeHint());

	this->statusBar()->addWidget(mStatusLabel);
	this->statusBar()->show();
	
	// TODO: Update statusbar
	//updateStatusBar();

	// TODO: Connect Statusbar to update it with info
}

void XPIWITGUIMainWindow::setGlobalMouseTracking(bool enable)
{
	QList<QWidget*> children = this->findChildren<QWidget*>();
	foreach (QWidget* child, children)
	{
			try{
				//QWidget* temp = (QWidget*) child;
				child->setMouseTracking(enable);
			}
			catch(...)
			{
			continue;
			}

	}
}


// Load Pipeline project from .txt
void XPIWITGUIMainWindow::slotLoadFile()
{
	// load graphical representation
	QString inputFile = QFileDialog::getOpenFileName(this, "Select save file", "../Saves/", "Save files (*.sav)");
	if (inputFile.isEmpty())
		return;

	// reset the GUI to load the new file	
	this->reset();
	mCurFile = inputFile;

	// get the file path
	QFileInfo fileInfo( mCurFile );
	QString projectPath = fileInfo.absolutePath();

	if (!projectPath.endsWith("/"))
		projectPath.append( "/" );

	// Load save file and read linked files
	QString graphicFileName;
	QString argFileName;
	QString xmlFileName;
	QFile saveFile(mCurFile);
	if(!saveFile.open(QIODevice::ReadOnly | QIODevice::Text))
        QMessageBox::information(0, "error", saveFile.errorString());

	QTextStream inFiles(&saveFile);
	while(!inFiles.atEnd()) {
		QString line = inFiles.readLine();
		if (line.endsWith("qne"))
		{
			graphicFileName = projectPath + line;
		} 
		else if (line.endsWith("txt"))
		{
			argFileName = projectPath + line;
		}
		else if (line.endsWith("xml"))
		{
			mCurXml = projectPath + line;
		}
		QStringList fields = line.split(",");    
	}
	saveFile.close();
	// Load nodes editor (graphical representation).
	QFile f(graphicFileName);
	f.open(QFile::ReadOnly);
	QDataStream ds(&f);
	mNodesEditor->load(ds);

	// XML
	   XMLReader xmlReader;
	try{
		xmlReader.readXML( mCurXml, mDisabledFilters );
	}catch(...){
		Logger::GetInstance()->WriteLine( "Error while parsing xml pipeline." );
		Logger::GetInstance()->WriteLine( "Critical Error - execution aborted" );
		mNoFailureOccurred = false;
	}
	mAbstractPipeline = xmlReader.getAbstractPipeline();


	mCurId = xmlReader.getMaximumFilterId()+1;

	// Sort Pipeline and set flags
	AbstractPipelineTools abstractPipelineTools( &mAbstractPipeline );
	try{
		abstractPipelineTools.OrderPipeline();
	}catch(...){
		Logger::GetInstance()->WriteLine( "Error while creating abstract pipeline." );
		Logger::GetInstance()->WriteLine( "Critical Error - execution aborted" );
		mNoFailureOccurred = false;
	}

	// extract the readers from the abstract pipeline
	for (int i = 0; i < mAbstractPipeline.GetNumFilter(); ++i)
	{
		if (mAbstractPipeline.GetFilter(i)->IsReader() == true)
			mAbstractReaders.append(mAbstractPipeline.GetFilter(i));
	}

	// extract the reader items from the graphicsscene
	for (int i = 0; i < mDrawingScene->items().length(); ++i)
	{
		if (mDrawingScene->items()[i]->type() == QNEBlock::Type)
		{
			QNEBlock* block = (QNEBlock*)mDrawingScene->items()[i];
			if (block->ports()[1]->portName().contains("Reader"))
			{
				// get the index of the current reader
				int readerIndex = QString(block->ports()[1]->portName()[block->ports()[1]->portName().size()-1]).toInt();

				DropLineEdit* inputPathLineEdit = new DropLineEdit("Input " + QString::number(mInputPathEdits.length()), "");
				connect(inputPathLineEdit, SIGNAL(textChanged(QString)), this, SLOT(slotUpdateStatus(QString)));
				mInputEditsLayout->addWidget(inputPathLineEdit);
				mInputPathEdits.append(inputPathLineEdit);
				mInputPathBlocks.append(block);
				std::cout << "Block: " << block->ports()[1]->portName().toStdString() << "(" << readerIndex << ") assigned to reader " << mInputPathEdits.length() << std::endl;
			}
		}
	}

	// Args
	QFile argFile(argFileName);
	if(!argFile.open(QIODevice::ReadOnly | QIODevice::Text))
		QMessageBox::information(0, "error", argFile.errorString());

	QTextStream in(&argFile);
	int readerIndex = 0;
	while(!in.atEnd())
	{
		QString line = in.readLine();
		QStringList fields = line.split(",");

		if (line.startsWith("--input"))
		{
			readerIndex = QString(fields[0][fields[0].size()-1]).toInt();
			if (mInputPathEdits.length() > readerIndex)
			{
				mInputPathEdits[readerIndex]->SetText(fields[1].trimmed());
				std::cout << "Setting reader " << readerIndex << "(" << fields[0].toStdString() << ")" << fields[1].trimmed().toStdString() << std::endl;
				//++readerIndex;
			}
			
			if (fields[2].contains('3') == true)
				mImageDimension3D->setChecked( true );
			else
				mImageDimension3D->setChecked( false );
		} 
		else if (line.startsWith("--output "))
		{
			mOutputPathEdit->SetText( fields[0].replace("--output ", "").trimmed() );
		}
	}
	saveFile.close();
    
    this->UpdateCMDArgs();
    mPipelineController.setPipeline(&mAbstractPipeline);
    emit(newPipelineLoaded(&mCMDPipelineArguments, mPipelineController));
}


// slot to show the Settings Dialog
void XPIWITGUIMainWindow::slotShowSettings()
{
	// Create new Settings Window and make it child widget
	SettingsDialog* settingsDialog = new SettingsDialog();
	connect(settingsDialog, SIGNAL(applySettings()), this, SLOT(slotApplySettings()));

	// settingsDialog.setParent(this);
	settingsDialog->exec();

}


//slot to apply the settings from the settings dialog
void XPIWITGUIMainWindow::slotApplySettings()
{
	// TODO
	QMessageBox::warning(this, "Not implemented yet", "This feature is not yet implemented. Check XPIWITGUIMainWindow.cpp slotApplySettings.");
}
    
bool XPIWITGUIMainWindow::fileExists(QString path, bool isFolder)
{
    QFileInfo checkFile(path);
	checkFile.makeAbsolute();

    // check if file exists and if yes: Is it really a file and no directory?
    if ((checkFile.exists() && checkFile.isFile() && isFolder == false) ||
        (checkFile.exists() && checkFile.isDir() && isFolder == true))
        return true;
    else
        return false;
}


bool XPIWITGUIMainWindow::ValidateUserInputs()
{
	// If paths are defined
	for (int i = 0; i < mInputPathEdits.length(); ++i)
	{
		if (fileExists(mInputPathEdits[i]->GetText(), false) == false && fileExists(mInputPathEdits[i]->GetText(), true) == false)
		{
			 QMessageBox::warning(this, "Invalid input path", "Invalid input path for reader " + QString::number(i) + "! Please check the input path of your pipeline. \nValid inputs are single channel 2D or 3D images or a folder containing 2D or 3D images.");
			 return false;
		}
	}
    
    // If paths are defined
    if (fileExists(mOutputPathEdit->GetText(), true) == false)
    {
        QMessageBox::warning(this, "Invalid output path", "Invalid output path! Please check the output path of your pipeline. \nThe output path has to exist and you need writing permissions.");
        return false;
    }

	// remove the output directory
	if (fileExists(mOutputPathEdit->GetText(), true) == true && mAutoClearResultFolder->isChecked())
	{
		QDir outputDir(mOutputPathEdit->GetText());
		QStringList outputEntries = outputDir.entryList();
		
		for (int i=0; i<outputEntries.length(); ++i)
		{
			if (outputEntries.at(i).compare(".") == 0 || outputEntries.at(i).compare("..") == 0)
				continue;

			QString currentFile = mOutputPathEdit->GetText() + outputEntries.at(i);
			QFileInfo checkFile(currentFile);
			if (checkFile.isDir())
			{
				QDir currentDir(currentFile);
				currentDir.removeRecursively();
			}
			else
			{
				QFile::remove(currentFile);
			}		
		}
	}

    // Check if pipeline is empty
	if (mAbstractPipeline.GetPipelineItemSet() == NULL || mAbstractPipeline.GetPipelineItemSet()->isEmpty())
	{
		QMessageBox::warning(this, "Pipeline is empty", "Your pipeline is empty. There is nothing to save/run.");
		return false;
	}
	
	// iterate over all items and perform naiive circle detection for each of the items
	int numFilters = mAbstractPipeline.GetNumFilter();
	for (int i=0; i<numFilters; ++i)
	{
		QStringList currentInputs;

		QList<AbstractFilter*> filterQueue;
		QList<AbstractFilter*> visitedFiltersQueue;
		AbstractFilter* currentFilter = mAbstractPipeline.GetFilter(i);
		filterQueue.append( currentFilter );
		//visitedFiltersQueue.append( currentFilter );

		while (filterQueue.length() > 0)
		{		
			AbstractFilter* nextFilter = filterQueue.first();
			filterQueue.pop_front();

			// check image inputs for cycles and existance
			for (int j=0; j<nextFilter->GetNumberImageIn(); ++j)
			{
				if (!nextFilter->GetImageInputs().at(j).mIdRef.isEmpty() && nextFilter->GetImageInputs().at(j).mIdRef.compare("cmd"))
				{
                    QList< AbstractInput > filterList = nextFilter->GetImageInputs();
					AbstractFilter* inputFilter = mAbstractPipeline.GetFilter(mAbstractPipeline.GetFilterNumberById(filterList.at(j).mIdRef));

					// check if filter already was visited before
					if (visitedFiltersQueue.contains(currentFilter))
					{
						QMessageBox::warning(this, "Invalid Connection", QString("Filter ") + inputFilter->GetName() + QString(" cannot rely on its own inputs! Please correct the connection and remove any circles in the pipeline."));
						return false;
					}

					filterQueue.append( inputFilter );
					visitedFiltersQueue.append( inputFilter );
				}
				else
				{
					if (nextFilter->IsReader() == false)
					{
						QMessageBox::warning(this, "Missing Connection", QString().sprintf("Please assign image input %i for filter %s and run the pipeline again.", j, nextFilter->GetName().toStdString().c_str()));
						return false;
					}
				}
			}
			
			// check meta inputs for cycles and existance
			for (int j=0; j<nextFilter->GetNumberMetaIn(); ++j)
			{
				if (!nextFilter->GetMetaInputs().at(j).mIdRef.isEmpty() && nextFilter->GetMetaInputs().at(j).mIdRef.compare("cmd"))
				{
					AbstractFilter* inputFilter = mAbstractPipeline.GetFilter(mAbstractPipeline.GetFilterNumberById(nextFilter->GetMetaInputs().at(j).mIdRef));

					// check if filter already was visited before
					if (visitedFiltersQueue.contains(currentFilter))
					{
						QMessageBox::warning(this, "Invalid Connection", QString("Filter ") + inputFilter->GetName() + QString(" cannot rely on its own inputs! Please correct the connection and remove any circles in the pipeline."));
						return false;
					}

					filterQueue.append( inputFilter );
					visitedFiltersQueue.append( inputFilter );
				}
				else
				{
					if (nextFilter->IsReader() == false)
					{
						QMessageBox::warning(this, "Missing Connection", QString().sprintf("Please assign meta input %i for filter %s and run the pipeline again.", j, currentFilter->GetName().toStdString().c_str()));
						return false;
					}
				}
			}
		}		
	}

	return true;
}

// slot to run the executable with the current parameters
void XPIWITGUIMainWindow::slotRun()
{
	// perform a validation of all user inputs
	if (!ValidateUserInputs())
		return;

	// create a temporary xml
	AbstractPipelineXMLCreator* xmlCreator = new AbstractPipelineXMLCreator();
	xmlCreator->SetAbstractPipeline( &mAbstractPipeline );
	QString tempXML = QDir::currentPath() + QString("/temp.xml");
    std::cout << tempXML.toStdString().c_str() << std::endl;
	xmlCreator->WriteXML(tempXML);

	// get the image dimension
	int imageDimension = 2;
	if (mImageDimension3D->isChecked() == true)
		imageDimension = 3;

	// create a temporary output test file
	QFile file(tempXML.replace("xml", "txt"));
	tempXML.replace("txt", "xml");
	file.open(QIODevice::WriteOnly | QIODevice::Text);
	QTextStream out(&file);
	out << "--output " << mOutputPathEdit->GetText() << endl;

	for (int i = 0; i < mInputPathEdits.length(); ++i)
	{
		QString inputPath = mInputPathEdits[i]->GetText();
		if (inputPath.contains("csv", Qt::CaseInsensitive))
			out << "--input " << i << ", " << inputPath << ", csv" << endl;
		else
			out << "--input " << i << ", " << inputPath << ", " << imageDimension << ", float" << endl;
	}
	
	out << "--xml " << tempXML << endl;
	out << "--seed 0" << endl;
	out << "--lockfile off" << endl;
	out << "--subfolder filterid, filtername" << endl;
	out << "--outputformat imagename, filtername" << endl;
	out << "--end" << endl;
	file.close();

	// start XPIWIT
	mXPIWITThread->start();
    
	// show a progress dialog and update it according to the log of XPIWIT
	mProgressDialog->exec();
}


// hide the parameter dialog if no filter is selected
void XPIWITGUIMainWindow::slotHideParameterDialog()
{
	removeLayout(mParasEditLayout);
    emit statusChanged("");
}

    
// Slot to update the status
void XPIWITGUIMainWindow::slotUpdateStatus(QString status)
{
    mStatusLabel->setText( status );
    UpdateCMDArgs();
}

// Display a warning containing the string given as parameter
void XPIWITGUIMainWindow::slotDisplayWarning(QString title, QString warning)
{
	QMessageBox::warning(this, title, warning);
}

// Slot to save
void XPIWITGUIMainWindow::slotSaveFile()
{
	// Check if necessary in/out paths set
	/*TODO
	if (mOutputPath.isEmpty() || mInputPath1.isEmpty())
	{
		 QMessageBox::warning(this, "Image input and output paths missing", "The saved input file will not contain any path information for input and output. Please adjust the corresponding locations manually.");
	}
	*/
    
	// Check if pipeline is empty
	if (mAbstractPipeline.GetPipelineItemSet() == NULL || mAbstractPipeline.GetPipelineItemSet()->isEmpty())
	{
		QMessageBox::warning(this, "Pipeline is empty", "Your pipeline is empty. There is nothing to save.");
		 return;
	}
    
	// Check if defaultdir exists, if not create
	/*
	if (!QDir("../Saves/").exists()) {
		QDir().mkdir("../Saves/");
	}*/
    
	// Get filename
	if ( mCurFile.isEmpty() )
	{
		 mCurFile = QFileDialog::getSaveFileName( this, "Select save file", "", "Save files (*.sav)");
	}
    
	if (mCurFile.isEmpty())
		return;
    
	if(!mCurFile.endsWith(".sav"))
        mCurFile.append(".sav");

	// get information about the file name
	QFileInfo fileInfo( mCurFile );
	QString fileName = fileInfo.fileName();
	QString filePath = fileInfo.absolutePath();

	if (!filePath.endsWith("/"))
		filePath.append("/");
    
	// Multiple files have to be created, their paths are stored in the save file (not dynamically generated on load)
	QString graphicFileName = fileName;
	graphicFileName.replace("sav", "qne");
	QString xmlFileName = fileName;
	xmlFileName.replace("sav", "xml");
	QString argsFileName = fileName;
	argsFileName.replace("sav", "txt");
    
	// Create save file
	QFile saveFile(mCurFile);
	saveFile.open(QIODevice::WriteOnly | QIODevice::Text);
	QTextStream savOut(&saveFile);
	savOut <<  graphicFileName << endl;
	savOut << xmlFileName << endl;
	savOut << argsFileName << endl;
	saveFile.close();
    
	// Create args file
	QFile argsFile(filePath + argsFileName);
	argsFile.open(QIODevice::WriteOnly | QIODevice::Text);
	QTextStream argsOut(&argsFile);

	int imageDimension = 2;
	if (mImageDimension3D->isChecked() == true)
		imageDimension = 3;
	
	if (!mOutputPathEdit->GetText().isEmpty())
		argsOut << "--output " << mOutputPathEdit->GetText() << endl;
	else
		argsOut << "--output " << "%OUTPUTPATH%" << endl;

	for (int i = 0; i < mInputPathEdits.length(); ++i)
	{
		QString inputPath = mInputPathEdits[i]->GetText();
		if (!inputPath.isEmpty())
			argsOut << "--input " << i << ", " << inputPath << ", " << imageDimension << ", float" << endl;
		else
			argsOut << "--input " << i << ", " << "%INPUTPATH1%" << ", " << imageDimension << ", float" << endl;
	}

	argsOut << "--xml " << filePath + xmlFileName << endl;
	argsOut << "--seed 0" << endl;
	argsOut << "--lockfile off" << endl;
	argsOut << "--subfolder filterid, filtername" << endl;
	argsOut << "--outputformat imagename, filtername" << endl;
	argsOut << "--end" << endl;
	argsFile.close();
    
	// Create xml
	AbstractPipelineXMLCreator* xmlCreator = new AbstractPipelineXMLCreator();
	xmlCreator->SetAbstractPipeline( &mAbstractPipeline );
	xmlCreator->WriteXML(filePath + xmlFileName);
	mCurXml = filePath + xmlFileName;
    
	// Save nodes editor (graphical representation)
	QFile graphicFile(filePath + graphicFileName);
	graphicFile.open(QFile::WriteOnly);
	QDataStream ds(&graphicFile);
	mNodesEditor->save(ds);
}


// Save as, enforces file selection
void XPIWITGUIMainWindow::slotSaveAs()
{
	QString tempFileName = QFileDialog::getSaveFileName( this, "Select save file", "", "Save files (*.sav)");
	if (tempFileName.isEmpty())
		return;
	mCurFile = tempFileName;
	slotSaveFile();
}
    

// start new pipeline generation
void XPIWITGUIMainWindow::slotNewFile()
{
	this->reset();
}

// show preview dock
void XPIWITGUIMainWindow::slotShowPreview()
{
	// Get the Input Image to use as selection guideline
	QDir curDir = QDir::currentPath();
	//QGraphicsPixmapItem* selectionItem1 = CreateMaximumProjection(curDir.path() + "/Test/in/test_image1.tif", 1);
	
	// Update and set the CmdArgs
	this->UpdateCMDArgs();

	// create a new PreviewDock
	PreviewDock* previewDock = new PreviewDock();
	connect( this, SIGNAL( displayFilterParameter(AbstractFilter*) ), previewDock, SLOT(slotSetFilter(AbstractFilter*)));
	connect(mXPIWITThread, SIGNAL(finished()), previewDock, SLOT(update())); // Update preview after execution
    connect( this, SIGNAL( newPipelineLoaded(CMDPipelineArguments*, PipelineController) ), previewDock, SLOT(slotResetCMDArguments(CMDPipelineArguments*, PipelineController)) );
    connect( this, SIGNAL(updatePreviewWindows()), previewDock, SLOT(update()) );
    connect( previewDock, SIGNAL(sliceChanged(int)), this, SLOT(slotSliceChanged(int)) );
    connect( this, SIGNAL(sliceChanged(int)), previewDock, SLOT(slotSliceChanged(int)) );
    
    connect( previewDock, SIGNAL(cursorPositionChanged(QPointF)), this, SLOT(slotCursorPositionChanged(QPointF)) );
    connect( this, SIGNAL(cursorPositionChanged(QPointF)), previewDock, SLOT(slotCursorPositionChanged(QPointF)) );
    
    
    previewDock->setAttribute(Qt::WA_DeleteOnClose);

	previewDock->mCMDArguments = &mCMDPipelineArguments;
	mPipelineController.setPipeline(&mAbstractPipeline);
	previewDock->mPipelineController = mPipelineController;

	// Set the selection viewports
	previewDock->update();
	
	// Show Preview
	this->addDockWidget(Qt::RightDockWidgetArea, previewDock);
	previewDock->setFloating(true);
	previewDock->setAllowedAreas(Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
	previewDock->geometry();
	previewDock->show();
}

void XPIWITGUIMainWindow::slotSliceChanged(int currentSlice)
{
    emit sliceChanged(currentSlice);
}

void XPIWITGUIMainWindow::slotCursorPositionChanged(QPointF currentPosition)
{
    emit cursorPositionChanged(currentPosition);
}

// import xml
void XPIWITGUIMainWindow::slotImport()
{
	this->reset();
    
	// Read XML
	XMLReader xmlReader;
	try
	{
		QFileInfo checkFile("myfilters.xml");
		if (!checkFile.exists() || !checkFile.isFile())
		{
			#ifdef _WIN32
				system( (QString("XPIWIT.exe --filterlist myfilters.xml")).toStdString().c_str() );
			#else
				system( (QString("./XPIWIT --filterlist myfilters.xml")).toStdString().c_str() );
			#endif
		}

		xmlReader.readXML( "myfilters.xml", mDisabledFilters );
	}
	catch(...)
	{
		Logger::GetInstance()->WriteLine( "Error while parsing xml filter list." );
		Logger::GetInstance()->WriteLine( "Critical Error - execution aborted" );
		mNoFailureOccurred = false;
	}
	mAbstractPipeline = xmlReader.getAbstractPipeline();
	// TODO:
	// Add graphical representation
}
    

// Reset the application
void XPIWITGUIMainWindow::reset()
{
	mAbstractPipeline.Clear();
	mDrawingScene->clear();

	for (int i = 0; i < mInputPathEdits.length(); ++i)
	{
		mInputEditsLayout->removeWidget(mInputPathEdits[i]);
		delete mInputPathEdits[i];
	}
		

	mInputPathEdits.clear();
	mAbstractReaders.clear();
	mInputPathBlocks.clear();

	mCurFile = "";
	mCurXml = "";
	mCurId = 0;
}

// Write the txt containing arguments to run xpiwit.exe with default values for the arguments.
void XPIWITGUIMainWindow::writeArgsTxt(QString filename, int imageDimension){
	try
	{
	QFile file(filename);
	file.open(QIODevice::WriteOnly | QIODevice::Text);
	QTextStream out(&file);
	out << "--output " << mOutputPathEdit->GetText() << endl;

	for (int i = 0; i < mInputPathEdits.length(); ++i)
		out << "--input " << i << ", " << mInputPathEdits[i]->GetText() << ", " << imageDimension << ", float" << endl;

	out << "--xml " << filename.remove(".txt") << ".xml" << endl;
	out << "--seed 0" << endl;
	out << "--lockfile off" << endl;
	out << "--subfolder filterid, filtername" << endl;
	out << "--outputformat imagename, filtername" << endl;
	out << "--end" << endl;
	file.close();
	} catch(...) {
		QMessageBox::warning(this, "Could not write " + filename, "An error occured while writing the file: " + filename + ". Make sure you have write permissions.");
	}
}

// Write the txt containing arguments to run "xpiwit.exe". For arguments you would like to set specify names(without "--") in argNames and values in "argValues". The order has to be the same.
void XPIWITGUIMainWindow::writeArgsTxt(QString filenameTxt, int imageDimension, QStringList argNames, QStringList argValues){
	try
	{
	// Autogenerate filename for xml file
	QString filenameXML = filenameTxt;
	filenameXML.replace("txt", "xml");
	// Open output filestream for args txt
	QFile file(filenameTxt);
	file.open(QIODevice::WriteOnly | QIODevice::Text);
	QTextStream out(&file);
	// Write common arguments
	out << "--output " << mOutputPathEdit->GetText() << endl;

	for (int i = 0; i < mInputPathEdits.length(); ++i)
		out << "--input " << i << ", " << mInputPathEdits[i]->GetText() << ", " << imageDimension << ", float" << endl;

	out << "--xml " << filenameXML << endl;
	// Write specified arguments
	// Write standard args if not mentioned before
	if (argNames.contains("seed"))
	{
		out << "--seed " + argValues.at(argNames.indexOf("seed")) << endl;
	}else{
		out << "--seed 0" << endl;
	}
	if (argNames.contains("lockfile"))
	{
		out << "--lockfile " + argValues.at(argNames.indexOf("lockfile")) << endl;
	}else{
		out << "--lockfile off" << endl;
	}
	if (argNames.contains("subfolder"))
	{
		out << "--subfolder " + argValues.at(argNames.indexOf("subfolder")) << endl;
	}else{
		out << "--subfolder filterid, filtername" << endl;
	}
	if (argNames.contains("outputformat"))
	{
		out << "--outputformat " + argValues.at(argNames.indexOf("outputformat")) << endl;
	}else{
		out << "--outputformat imagename, filtername" << endl;
	}
	// Finalize/Cleanup
	out << "--end" << endl;
	file.close();
	} catch(...) {
		QMessageBox::warning(this, "Could not write " + filenameTxt, "An error occured while writing the file: " + filenameTxt + ". Make sure you have write permissions.");
	}
}

void XPIWITGUIMainWindow::writeArgsTxt(QString filename, CMDPipelineArguments cmdargs)
{
	
}


void XPIWITGUIMainWindow::slotDirectoryChanged(QString directory)
{
	/*
	// find the latest logfile
	QDir myDir(directory);
	QStringList filesList = myDir.entryList(QStringList(QString("*PipelineLog.txt")), QDir::Files, QDir::Time);

	for (int i=0; i<filesList.length(); ++i)
	{
		if (filesList[i].contains("Overview"))
			filesList.removeAt(i);
	}

	while (mXPIWITThread->isRunning())
	{
		int numFiltersExecuted = 0;
		QFile file(directory + filesList[0]);
		QTextStream in(&file);
		QString line = in.readLine();
		while(!in.atEnd())
		{
			if (line.contains("Executing Process Object"))
				numFiltersExecuted++;
		}
		file.close();

		progressDialog.setValue(numFiltersExecuted-1);
	}

	progressDialog.close();
	mFileSystemWatcher->removePath( directory );
	*/
}


// displays parameters of specific filter
void XPIWITGUIMainWindow::slotDisplayParameter(AbstractFilter* filter)
{
	// Clear previous paras from view
	removeLayout(mParasEditLayout);
	int parCount = filter->GetNumberParameter();

	// Display paras
	QList<QString> descriptionList = filter->GetParameterDescriptions();
	QList<int> parameterTypes = filter->GetParameterTypes(); // 0=String, 1=Double, 2=int, 3=boolean
	int curType;
	for( int i = 0; i < parCount; i++)
	{
		QString name = filter->GetParameter(i).value(0);
		name.append(":");
		QString value = filter->GetParameter(i).value(1);
		QString description = descriptionList.value(i);
		// Label
		QLabel *nameLabel = new QLabel(name, mParasWidget);
		nameLabel->setToolTip(description);
		mParasEditLayout->addWidget(nameLabel, i, 1);
		curType = parameterTypes.value(i);
		// Edit field
		switch (curType)
		{
		case 0: // String
			{
			ParasLineEdit *valueEdit = new ParasLineEdit(value, mParasWidget, filter, i);
			valueEdit->setToolTip(description);
			mParasEditLayout->addWidget(valueEdit, i, 3);
			break;
			}
		case 1: // Double
			{
			ParasDoubleSpinBox *valueDoubleSpin = new ParasDoubleSpinBox(value.toDouble(), mParasWidget, filter, i);
			valueDoubleSpin->setRange(-100000000, 100000000);
			valueDoubleSpin->setValue(value.toDouble());
			valueDoubleSpin->setSingleStep(1);
			valueDoubleSpin->setEnabled(true);
			valueDoubleSpin->setAcceptDrops(true);
			valueDoubleSpin->setToolTip(description);
			mParasEditLayout->addWidget(valueDoubleSpin, i, 3);
			break;
			}
		case 2: // int
			{
			ParasSpinBox *valueSpin = new ParasSpinBox(value.toInt(), mParasWidget, filter, i);
			valueSpin->setRange(-100000000, 100000000);
			valueSpin->setValue(value.toInt());
			valueSpin->setEnabled(true);
			valueSpin->setAcceptDrops(true);
			valueSpin->setToolTip(description);
			mParasEditLayout->addWidget(valueSpin, i, 3);
			break;
			}
		case 3: // bool
			{
			ParasCheckBox *valueCheck = new ParasCheckBox(value.toInt(), mParasWidget, filter, i);
			valueCheck->setEnabled(true);
			valueCheck->setAcceptDrops(true);
			valueCheck->setToolTip(description);
			mParasEditLayout->addWidget(valueCheck, i, 3);
			break;
			}
		default: // default
			{
			ParasLineEdit *defaultValueEdit = new ParasLineEdit(value, mParasWidget, filter, i);
			defaultValueEdit->setToolTip(description);
			mParasEditLayout->addWidget(defaultValueEdit, i, 3);
			break;
			}
		}
		/*ParasLineEdit *valueEdit = new ParasLineEdit(value, mParasWidget, filter, i);
		nameLabel->setToolTip(description);
		valueEdit->setToolTip(description);
		mParasEditLayout->addWidget(nameLabel, i, 1);
		mParasEditLayout->addWidget(valueEdit, i, 3);*/
	}
}

// Set parameter at index i of filter* filter to value
void XPIWITGUIMainWindow::slotSetParameter(AbstractFilter* filter, QString value, int idx)
{
    /*
	//QHash<QString,AbstractFilter*>* temp = mAbstractPipeline.GetPipelineItemSet();
	for(int i = 0; i<mFilterList.length();i++)
	{
		if(mFilterList.at(i) == filter)
        {
			AbstractFilter * selectedFilter = mFilterList.value(i);
			selectedFilter->SetParameter(idx,value);
			emit displayFilterParameter(selectedFilter);
			break;
		}
	}
    */
}


// Proxy slot for listItemClicked -> displayFilterParameter
void XPIWITGUIMainWindow::slotListItemClicked( QListWidgetItem * item, QListWidgetItem* previousItem)
{
//	int idx = item->listWidget()->currentRow();
//	AbstractFilter * selectedFilter = mFilterList.value(idx);
//	emit displayFilterParameter(selectedFilter);
    
    // get the filter name of the currently selected filter item
    QString filterName = item->listWidget()->currentItem()->text();
    
    // iterate over the filter list and emit the abstract filter if the selected filter is found in the list
    for (int i=0; i<mFilterList.count(); ++i)
    {
        if (mFilterList.value(i)->GetName() == filterName)
        {
            AbstractFilter* selectedFilter = mFilterList.value(i);
            emit displayFilterParameter(selectedFilter);
            emit statusChanged( selectedFilter->GetDesription() );
            return;
        }
    }
}


// Proxy slot to add Filters directly from the List, sfinds the selected filter
void XPIWITGUIMainWindow::addFilterFromFilterListItem( QListWidgetItem * item )
{
    // get the filter name of the currently selected filter item
    QString filterName = item->listWidget()->currentItem()->text();
    
    // iterate over the filter list and emit the abstract filter if the selected filter is found in the list
    for (int i=0; i<mFilterList.count(); ++i)
    {
        if (mFilterList.value(i)->GetName() == filterName)
        {
            AbstractFilter* selectedFilter = mFilterList.value(i);
            emit addFilterBlock(selectedFilter);
            emit statusChanged( selectedFilter->GetDesription() );
            return;
        }
    }
}


// Slot to add a block to the filter representation. Also adds the filter to mAbstractPipeline
void XPIWITGUIMainWindow::slotAddFilterBlock(AbstractFilter* filter)
{
	// create a new filter and copy the contents from the filter templates
	AbstractFilter* newFilter = new AbstractFilter;
    newFilter->SetAbstractFilter(*filter);

	// add a new block for the filter and set the id of the filter
	QNEBlock* b = new QNEBlock(0);
	newFilter->SetId(QString().sprintf("item_%04d", mCurId)); // Filter and corresponding block have to have the same ID
	b->setID(mCurId);
	mCurId++;
	mAbstractPipeline.AddItem(newFilter);
	mDrawingScene->addItem(b);
	QString name = filter->GetName();

	// Add name tag
	if(filter->IsReader())
	{
		name.remove("Reader");
		b->addPort(name, 0, QNEPort::NamePort);
		b->addPort("Reader" + QString::number(mInputPathEdits.length()), 0, QNEPort::TypePort);

		if (name.contains("Image"))
			newFilter->SetImageInput(0, "cmd", mInputPathEdits.length(), 2);
		else
			newFilter->SetMetaInput(0, "cmd", mInputPathEdits.length(), 2);

		DropLineEdit* inputPathLineEdit = new DropLineEdit("Input " + QString::number(mInputPathEdits.length()), "Drag&Drop File Here");
		mInputEditsLayout->addWidget(inputPathLineEdit);
		mInputPathEdits.append(inputPathLineEdit);
		mInputPathBlocks.append(b);
		mAbstractReaders.append(newFilter);
		connect(inputPathLineEdit, SIGNAL(textChanged(QString)), this, SLOT(slotUpdateStatus(QString)));
	}
	else if(name.contains("ImageFilter"))
	{
		name.remove("ImageFilter");
		b->addPort(name, 0, QNEPort::NamePort);
		b->addPort("ImageFilter", 0, QNEPort::TypePort);
	}
	else 
	{
		b->addPort(name, 0, QNEPort::NamePort);
		b->addPort("Other", 0, QNEPort::TypePort);
	}

	// Input ports
	if (!filter->IsReader())
	{
		for (int i = 0; i < filter->GetNumberImageIn(); i++)
			b->addPort("image" + QString::number(i + 1), 0, 0, 0);

		for (int i = 0; i < filter->GetNumberMetaIn(); i++)
			b->addPort("meta" + QString::number(i + 1), 0, 0, 0);
	}

	// Ouput ports
	for( int i = 0; i < filter->GetNumberImageOut(); i++)
		b->addPort("image" + QString::number(i+1), 1, 0, 0);

	for( int i = 0; i < filter->GetNumberMetaOut(); i++)
		b->addPort("meta" + QString::number(i+1), 1, 0, 0);

	// set the position of the filter in the drawing area
    QPoint mousePos = mapFromGlobal(QCursor::pos());
	QPointF scenePos = mDrawingView->mapToScene(mousePos);
    b->setPos(scenePos);
}


// handle mouse move events
void XPIWITGUIMainWindow::mouseMoveEvent(QMouseEvent *event){
    mCurMousePos = event->pos();
}


// Deletes filter from pipeline data
void XPIWITGUIMainWindow::slotDeleteFilterByID( int id )
{
	// get the filter that should be deleted
	int filterId = mAbstractPipeline.GetFilterNumberById(QString().sprintf("item_%04d", id));
	AbstractFilter* filter = mAbstractPipeline.GetFilter( filterId );

	if (filter->IsReader() == true)
	{
		int readerIndex = -1;
		for (int i = 0; i < mAbstractReaders.length(); ++i)
		{
			if (mAbstractReaders[i] == filter && readerIndex < 0)
				readerIndex = i;

			if (readerIndex >= 0 && readerIndex != i)
			{
				mInputPathEdits[i]->SetLabel("Input " + QString::number(i - 1));
				mInputPathBlocks[i]->ports()[1]->setName("Reader" + QString::number(i - 1));
			}
		}

		mAbstractReaders.removeAt(readerIndex);
		mInputPathBlocks.removeAt(readerIndex);
		mInputEditsLayout->removeWidget(mInputPathEdits[readerIndex]);
		delete mInputPathEdits[readerIndex];
		mInputPathEdits.removeAt(readerIndex);
	}

	// reset all inputs that point to this filter
	for (int i=0; i<mAbstractPipeline.GetNumFilter(); ++i)
	{
		AbstractFilter* currentFilter = mAbstractPipeline.GetFilter(i);

		for (int j=0; j<currentFilter->GetNumberImageIn(); ++j)
		{
			AbstractInput currentInput = currentFilter->GetImageInputs().at(j);
			if (filter->GetId() == currentInput.mIdRef)
				currentFilter->SetImageInput(j, "", currentInput.mNumberRef, currentInput.mDataType);			
		}

		for (int j=0; j<currentFilter->GetNumberMetaIn(); ++j)
		{
			AbstractInput currentInput = currentFilter->GetMetaInputs().at(j);
			if (filter->GetId() == currentInput.mIdRef)
				currentFilter->SetMetaInput(j, "", currentInput.mNumberRef, currentInput.mDataType);
		}
	}

	mAbstractPipeline.RemoveItemById(id);
	removeLayout(mParasEditLayout);
}


// Deletes filter from pipeline data
void XPIWITGUIMainWindow::slotConnectionDeleted( QNEConnection* connection )
{
	// get the filter that requires an input adaption
	AbstractFilter* filter;
	QString portName;
	if (connection->port1()->isOutput() == true)
	{
		QString filterId = QString().sprintf("item_%04d", connection->port2()->block()->getID());
		filter = mAbstractPipeline.GetFilter(mAbstractPipeline.GetFilterNumberById(filterId));
		portName = connection->port2()->portName();
	}
	else
	{
		QString filterId = QString().sprintf("item_%04d", connection->port1()->block()->getID());
		filter = mAbstractPipeline.GetFilter(mAbstractPipeline.GetFilterNumberById(filterId));
		portName = connection->port1()->portName();
	}

	// skip readers as they have cmd dependent inputs
	if (filter->IsReader() == true)
		return;

	// get the port name
	int portId = QString(portName.at(portName.length()-1)).toInt()-1;
	AbstractInput currentInput = ((AbstractFilter*)filter)->GetImageInputs().at(portId);

	// reset the input to none
	if (portName.contains("image"))
		((AbstractFilter*)filter)->SetImageInput(portId, "", currentInput.mNumberRef, currentInput.mDataType);
	else if (portName.contains("meta"))
		((AbstractFilter*)filter)->SetMetaInput(portId, "", currentInput.mNumberRef, currentInput.mDataType);
}


// Display filter from current pipeline by ID
void XPIWITGUIMainWindow::slotDisplayFilterByID(int id)
{
    AbstractFilter* selectedFilter = mAbstractPipeline.GetPipelineItemSet()->value( QString().sprintf("item_%04d", id) );
	emit displayFilterParameter(mAbstractPipeline.GetPipelineItemSet()->value( QString().sprintf("item_%04d", id) ));
    emit statusChanged(selectedFilter->GetDesription());
}


// Add connection from graphical representation to the pipeline
void XPIWITGUIMainWindow::slotConnectionAdded(QNEPort* port1, QNEPort* port2)
{
	AbstractFilter* filter1 = mAbstractPipeline.GetPipelineItemSet()->value( QString().sprintf("item_%04d", port1->block()->getID()));
	AbstractFilter* filter2 = mAbstractPipeline.GetPipelineItemSet()->value( QString().sprintf("item_%04d", port2->block()->getID()));

	// switch filter 1 and two if the assignment was done from successor to predecessor
	if (!port1->isOutput() && port2->isOutput())
	{
		AbstractFilter* tmp = filter1;
		filter1 = filter2;
		filter2 = tmp;

		QNEPort* tmpPort = port1;
		port1 = port2;
		port2 = tmpPort;
	}

	QString id = ((AbstractFilter*)filter1)->GetId();
	QString outputName = port1->portName();
	QString inputName = port2->portName();
	int outputId = QString(outputName.at(outputName.length()-1)).toInt();
	int inputId = QString(inputName.at(inputName.length()-1)).toInt() - 1;
	
	bool isImageOutput = outputName.contains("image");
	bool isImageInput = inputName.contains("image");
	bool isMetaOutput = outputName.contains("meta");
	bool isMetaInput = inputName.contains("meta");

	// check if it is tried to set an input to a reader
	if (filter2->IsReader() == true)
	{
		QMessageBox::warning(this, "Invalid Connection", "You cannot assign an input to a reader!\n Use the image input edit fields to specify an input file name.");
		mNodesEditor->deleteLastAddedConnection();
		return;
	}

	// check if the port already has a connection
	if ((isImageInput == true && filter2->GetImageInputs().at(inputId).mIdRef.isEmpty() == false) || (isMetaInput == true && filter2->GetMetaInputs().at(inputId).mIdRef.isEmpty() == false))
	{
		QMessageBox::warning(this, "Invalid Connection", "You cannot assign multiple input images to a single port!");
		mNodesEditor->blockSignals(true);
		mNodesEditor->deleteLastAddedConnection();
		mNodesEditor->blockSignals(false);
		return;		
	}

	// check if only inputs/outputs of the same type are connected
	if (isImageOutput == true && isImageInput == true)
	{
		if (filter1->GetNumberImageOut() > 0)
		{
			int type = filter1->GetTypeIds()[0];
			((AbstractFilter*)filter2)->SetImageInput(inputId, id, outputId, type);
		}
	}
	else if (isMetaOutput == true && isMetaInput == true)
	{
		if (filter1->GetNumberMetaOut() > 0)
		{
			int type = filter1->GetTypeIds()[0];
			((AbstractFilter*)filter2)->SetMetaInput(inputId, id, outputId, type);
		}
	}
	else
	{
		QMessageBox::warning(this, "Invalid Connection", "You cannot mix image and meta ports! Please correct the connection.");
		mNodesEditor->deleteLastAddedConnection();
	}
}


// Parses the list of all filters and stores results in mFilterList
void XPIWITGUIMainWindow::ParseXmlFilterList()
{
	// XML
	XMLReader xmlReader;
	try
	{
		QFileInfo checkFile("myfilters.xml");
		if (!checkFile.exists() || !checkFile.isFile())
		{
			#ifdef _WIN32
				system( (QString("XPIWIT.exe --filterlist myfilters.xml")).toStdString().c_str() );
			#else
				system( (QString("./XPIWIT --filterlist myfilters.xml")).toStdString().c_str() );
			#endif
		}

		xmlReader.readXML( "myfilters.xml", mDisabledFilters );
	}
	catch(...)
	{
		Logger::GetInstance()->WriteLine( "Error while parsing xml filter list." );
		Logger::GetInstance()->WriteLine( "Critical Error - execution aborted" );
		mNoFailureOccurred = false;
	}
	mFilterList = xmlReader.getFilterList();
}


// Remove a QLAyout and delete all its children
void XPIWITGUIMainWindow::removeLayout(QLayout* layout)
{
	QLayoutItem* child;
	while(layout->count()!=0)
	{
		child = layout->takeAt(0);
		if(child->layout() != 0)
		{
			removeLayout(child->layout());
		}
		else if(child->widget() != 0)
		{
			delete child->widget();
		}

		delete child;
	}
}

// Test all filters in the filterlist. Results will be written into Bin directory
void XPIWITGUIMainWindow::testFilters()
{
	QDir curPath = QDir::currentPath();
	QString exePath = curPath.path();
	for (int i=0; i< mFilterList.length(); ++i)
	{
		AbstractFilter* filter = mFilterList.at(i);
		if (filter->IsReader())
		{
			//Skip Image and Meta Readers
			continue;
		}
		/*TODO
		mInputPath1 = exePath + "/Test/in/";
		mOutputPath = exePath + "/Test/out/";
		*/

		std::cout << "-----------Testing: " << filter->GetName().toStdString().c_str() << "-----------" << std::endl;
		AbstractFilter* imageReader = mFilterList.at(90); //TODO::Dynamically find Image reader!!
		mAbstractPipeline.AddItem(imageReader);
		QString ImageReaderId = ((AbstractFilter*)imageReader)->GetId();
		mAbstractPipeline.AddItem(filter);
		QFile file("/Test/filtertest_log.txt");
		file.open(QIODevice::WriteOnly | QIODevice::Text);
		QTextStream out(&file);
		int type = filter->GetTypeIds()[0];
		try {
			if (filter->GetNumberImageIn() > 0) // Statement before: !filter->GetNumberImageIn()==0 yielded compile errors under OSX! --> changed
			{
				filter->SetImageInput(0, ImageReaderId, 1, type);
			}
		} catch (std::exception e) {
			out << filter->GetName() << ": Invalid Connection: You cannot mix image and meta ports! Please correct the connection.\n";
			out << e.what();
		} catch (...) {
			// TODO: Identify exception/do something about it
			out << "An error occured creating the test pipeline for the following filter: " << filter->GetName();
		}
		try {
			this->slotRun();
		} catch (std::exception e) {
			//auto exPtr = std::current_exception();
			out << "An error occured running the test pipeline for the following filter: " << filter->GetName();
			out<< e.what();
		}
		// Check output folder
		QString outputName = "test_image1_" + filter->GetName() + "_Out1.tif";
		QString outputFullPath = exePath + "/Test/out/" + filter->GetId() + "_" + filter->GetName() + "/" + outputName;
		QFile outputFile(outputFullPath);
		bool fileExists = outputFile.exists();
		if (fileExists) {
			// File exists
		} else {
			out << "No output was detected for the following filter: " << filter->GetName();
		}   

		// Tidy up for the next run
		mAbstractPipeline = AbstractPipeline();
		file.close();
	}
}


void XPIWITGUIMainWindow::dragEnterEvent(QDragEnterEvent* event)
{
    event->acceptProposedAction();
}

void XPIWITGUIMainWindow::dragMoveEvent(QDragMoveEvent* event)
{
    event->acceptProposedAction();
}

void XPIWITGUIMainWindow::dropEvent(QDropEvent* event)
{
    //const QMimeData* mimeData = event->mimeData();
    
    event->acceptProposedAction();
}

void XPIWITGUIMainWindow::slotQuit()
{
    emit signalFinished();
}

} // namespace XPIWIT
