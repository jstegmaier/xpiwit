/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// namespace header
#include "DropLineEdit.h"
#include <qmimedata.h>
#include <qfileinfo.h>
#include <QHBoxLayout>
#include <QFileDialog>
#include <QErrorMessage>
#include <iostream>

namespace XPIWIT
{

// the default contstructor
DropLineEdit::DropLineEdit(QWidget *parent) : QWidget( parent )
{
	this->setAcceptDrops( true );
	connect(this, SIGNAL(editingFinished()), this, SLOT(createValidPath()));
}


// constructor that initializes the text of the edit field
DropLineEdit::DropLineEdit(const QString& label, const QString& text, QWidget *parent) : QWidget(parent)
{
	mLabel = new QLabel(label);
	mLineEdit = new QLineEdit(text, parent);
	mLineEdit->home(true);
	mOpenFileButton = new QPushButton("");
	mOpenFolderButton = new QPushButton("");

	QIcon fileIcon = QIcon("./images/new_icon.png");
	QIcon folderIcon = QIcon("./images/load_icon.png");

	bool outputFolder = false;
	if (label.contains("Output"))
		outputFolder = true;

	mOpenFileButton->setIcon(fileIcon);
	mOpenFolderButton->setIcon(folderIcon);

	mOpenFileButton->setToolTip("Select a single image file.");
	mOpenFolderButton->setToolTip("Select a folder containing image files.");

	QHBoxLayout* mLayout = new QHBoxLayout();
	mLayout->addWidget(mLabel);
	mLayout->addWidget(mLineEdit);
	if (outputFolder == false)
		mLayout->addWidget(mOpenFileButton);
	mLayout->addWidget(mOpenFolderButton);

	this->setLayout(mLayout);
	this->setAcceptDrops( true );
	mLineEdit->setAcceptDrops(false);
	connect(mLineEdit, SIGNAL(editingFinished()), this, SLOT(CreateValidPath()));
	connect(mLineEdit, SIGNAL(textChanged(QString)), this, SLOT(slotTextChanged(QString)));
	connect(mOpenFileButton, SIGNAL(clicked()), this, SLOT(slotOpenFile()));
	connect(mOpenFolderButton, SIGNAL(clicked()), this, SLOT(slotOpenFolder()));
}


// the destructor
DropLineEdit::~DropLineEdit()
{
	delete mLabel;
	delete mLineEdit;
	delete mOpenFileButton;
	delete mOpenFolderButton;
}

void DropLineEdit::slotOpenFile()
{
	QString selectedPath = QFileDialog::getOpenFileName(this, tr((QString("Select ") + mLabel->text()).toStdString().c_str()), mLineEdit->text());
	if (!selectedPath.isEmpty())
	{
		mLineEdit->setText(selectedPath);
		CreateValidPath();
	}
}


void DropLineEdit::slotOpenFolder()
{
	QString selectedPath = QFileDialog::getExistingDirectory(this, tr((QString("Select ") + mLabel->text()).toStdString().c_str()), mLineEdit->text());
	if (!selectedPath.isEmpty())
	{
		mLineEdit->setText(selectedPath);
		CreateValidPath();
	}
}

void DropLineEdit::slotTextChanged(QString text)
{
	emit textChanged(mLabel->text() + QString(" set to: ") + text);
}
   
// drag enter event
void DropLineEdit::dragEnterEvent(QDragEnterEvent* event)
{
    event->acceptProposedAction();
}


// drag move event
void DropLineEdit::dragMoveEvent(QDragMoveEvent* event)
{
    event->acceptProposedAction();
}


// drop move event
void DropLineEdit::dropEvent(QDropEvent* event)
{
    const QMimeData* mimeData = event->mimeData();
    
	if (mimeData->hasUrls() == true)
	{
         QList<QUrl> urlList = mimeData->urls();
         QString text;
         for (int i = 0; i < urlList.size() && i < 32; ++i)
		 {
			 QString url = urlList.at(i).path();
             text += url + QString("\n");
         }

		 #ifdef _WIN32
			text.remove(0,1);
		 #endif
		text.remove(text.length()-1,1);
		mLineEdit->setText( text );

		std::cout << text.toStdString() << std::endl;

		// convert the path to a valid XPIWIT representation
		CreateValidPath();
	}

    event->acceptProposedAction();
}


// function to generate a valid input path
void DropLineEdit::CreateValidPath()
{
	QString currentText = mLineEdit->text();
	currentText.replace( "\\", "/" );

	QFileInfo info1( currentText );
	if (info1.isDir() && currentText.at(currentText.length()-1) != '/')
		currentText.append("/");

	while (currentText.contains( "//" ))
		currentText.replace("//", "/");
		
	blockSignals(true);
	mLineEdit->setText(currentText);
	blockSignals(false);
}

} // namespace XPIWIT
