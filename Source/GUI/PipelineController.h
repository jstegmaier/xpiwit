/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef PIPELINECONTROLLER_H
#define PIPELINECONTROLLER_H

// Project header
#include "../Core/XML/AbstractPipeline.h"
#include "../Core/XML/AbstractInput.h"
// #include <../../Source/Filter/Base/Management/ProcessObjectBase.h>

// Qtheader
#include <qdir.h>
#include <qstack.h>
#include <qmessagebox.h>


namespace XPIWIT
{

/**
 *	@class A Controller for Pipelines
 *	The Pipelinecontroller gets a AbstractPipeline and provides some convenience funtions for handling it. it does not change the pipeline object it gets passed.
 */

class PipelineController // : QObject
{
	// Q_OBJECT

	public:
		PipelineController();
		PipelineController(AbstractPipeline*);
		~PipelineController();
		// Member variables
		QString getFilterOutput(AbstractFilter* filter, CMDPipelineArguments *cmdPipelineArguments, QString inputName = "");
		AbstractPipeline extractSubPipeline(AbstractFilter* Filter);
		AbstractPipeline* mMainWindowPipeline;
		// Methods
		void runSubPipeline(AbstractFilter* filter);
		void run(AbstractPipeline pipeline);
		void setPipeline(AbstractPipeline* pipeline) {mMainWindowPipeline = pipeline; mPipeline = *mMainWindowPipeline;};

	/*signals:
		void showWarning(QString title, QString warning);*/

	private:
		// Members
		AbstractPipeline mPipeline;
		// QFile getFilterOutput(AbstractFilter* filter, AbstractPipeline* pipeline) {this->setPipeline(pipeline); getFilterOutput(filter);};
		// AbstractPipeline extractSubPipeline(QList<AbstractFilter*> Filters);
		// void runPipeline(); TODO move this from Mainwindow
		void updatePipeline();
	//private slots:
};

} // namespace XPIWIT

#endif // PreviewDock_H