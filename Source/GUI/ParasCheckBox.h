/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef PARASCHECKBOX_H
#define PARASCHECKBOX_H

#include "qcheckbox.h"
#include "../Core/XML/AbstractFilter.h"
#include "qobject.h"

// Simple checkbox with reference to the filter and index of the parameter, passes changed values on to the Abstractfilter
namespace XPIWIT
{

class ParasCheckBox : public QCheckBox
{
	Q_OBJECT
	public:
		/**
		 * Constructor
		 */
		ParasCheckBox(bool value, QWidget* parent, AbstractFilter* filter, int index)
		{
			this->setChecked(value);
			this->setParent(parent);
			mIndex = index;
			mFilter = filter;
			connect(this, SIGNAL(stateChanged(int)), this, SLOT(slotParameterEdited()));
            //connect(this, ) TODO: let the edit field emit a signal upon mouse hover to update the status tooltips
		}

		/**
		 * Destructor
		 */
		~ParasCheckBox(void)
		{
		}

		/**
		 * function to get the index of the filter
		 */
		int GetIndex()                              { return mIndex; }
		AbstractFilter* GetFilter()                 { return mFilter; }
		void SetIndex(int index)                    { mIndex = index; }
		void SetFilter(AbstractFilter* filter)      { mFilter = filter;}

	signals:
		void parameterEdited(AbstractFilter* filter, QString value, int paraIndex);

	private slots:
		void slotParameterEdited()
		{
			QString value;
			if (this->isChecked()) 
				{
					value = "1";
			} else 
				{
					value = "0";
			}
			mFilter->SetParameter(mIndex, value);
			emit parameterEdited(mFilter, value, mIndex);
		}

	private:
		int mIndex;
		AbstractFilter* mFilter;
};
}

#endif