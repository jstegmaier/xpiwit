/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef PARASSPINBOX_H
#define PARASSPINBOX_H

#include "qspinbox.h"
#include "../Core/XML/AbstractFilter.h"
#include "qobject.h"

// Simple line edit with reference to the filter and index of the parameter
namespace XPIWIT
{

	class ParasSpinBox : public QSpinBox
{
	Q_OBJECT
	public:
		/**
		 * Constructor
		 */
		ParasSpinBox(int value, QWidget* parent, AbstractFilter* filter, int index, int rangeMin=-1000000, int rangeMax=1000000, int singleStep=1)
		{
			this->setRange(rangeMin, rangeMax);
			this->setSingleStep(singleStep);
			this->setValue(value);
			this->setParent(parent);
			mIndex = index;
			mFilter = filter;
			connect(this, SIGNAL(valueChanged(int)), this, SLOT(slotParameterEdited()));
            //connect(this, ) TODO: let the edit field emit a signal upon mouse hover to update the status tooltips
		}

		/**
		 * Destructor
		 */
		~ParasSpinBox(void)
		{
		}

		/**
		 * function to get the index of the filter
		 */
		int GetIndex()                              { return mIndex; }
		AbstractFilter* GetFilter()                 { return mFilter; }
		void SetIndex(int index)                    { mIndex = index; }
		void SetFilter(AbstractFilter* filter)      { mFilter = filter;}

	signals:
		void parameterEdited(AbstractFilter* filter, QString value, int paraIndex);

	private slots:
		void slotParameterEdited()
		{
			QString value = QString::number(this->value());
			mFilter->SetParameter(mIndex, value);
			emit parameterEdited(mFilter, value, mIndex);
		}

	private:
		int mIndex;
		AbstractFilter* mFilter;
};
}

#endif