/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef __XPIWITCORE_SLICEBYSLICEATTENUATEINTENSITYIMAGEFILTER_H
#define __XPIWITCORE_SLICEBYSLICEATTENUATEINTENSITYIMAGEFILTER_H

// include required headers
#include "../Base/Management/ITKDefinitions.h"

#include "itkExtractImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkMorphologicalWatershedImageFilter.h"
#include <string>

using namespace XPIWIT;

namespace itk {

template <class TImageType, class TOutputImage>
class ITK_EXPORT SliceBySliceAttenuateIntensityImageFilter : public ImageToImageFilter<TImageType, TOutputImage>
{
	public:
		/** Extract dimension from input and output image. */
		itkStaticConstMacro(ImageDimension, unsigned int, TImageType::ImageDimension);

		typedef SliceBySliceAttenuateIntensityImageFilter Self;
		typedef ImageToImageFilter<TImageType,TOutputImage> Superclass;
		typedef SmartPointer<Self> Pointer;
		typedef SmartPointer<const Self> ConstPointer;

		itkNewMacro(Self);
				
        itkSetMacro( Delta, float);
        itkGetMacro( Delta, float);
        itkSetMacro( MinSigma, float);
        itkGetMacro( MinSigma, float);
		itkSetMacro( MaxSigma, float);
        itkGetMacro( MaxSigma, float);
        itkSetMacro( MinAttenuation, float);
        itkGetMacro( MinAttenuation, float);
		itkSetMacro( MaxAttenuation, float);
        itkGetMacro( MaxAttenuation, float);
        itkSetMacro( UseImageSpacing, bool);
        itkGetMacro( UseImageSpacing, bool);
		itkSetMacro( ExponentialAttenuation, bool);
        itkGetMacro( ExponentialAttenuation, bool);
		itkSetMacro( InvertAttenuationDirection, bool);
        itkGetMacro( InvertAttenuationDirection, bool);
		itkSetMacro( Dimensionality, unsigned int);
        itkGetMacro( Dimensionality, unsigned int);

		typedef TImageType InputImageType;
		typedef typename TImageType::PixelType PixelType;

		typedef typename TImageType::RegionType InputImageRegionType;
		typedef typename TOutputImage::RegionType OutputImageRegionType;

	protected:
		SliceBySliceAttenuateIntensityImageFilter();
		virtual ~SliceBySliceAttenuateIntensityImageFilter();
		
		/**
		 * Functions for data generation.
		 */
		void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId) override;

		/** 
		 * functions for prepareing and post-processing the threaded generation.
		 */
		void BeforeThreadedGenerateData() override;
		void AfterThreadedGenerateData() override;

		float m_Delta; // attenuation factor
		float m_MinSigma; // attenuation factor
		float m_MaxSigma; // attenuation factor
		bool m_UseImageSpacing;
		unsigned int m_Dimensionality;
		float m_MinAttenuation;
		float m_MaxAttenuation;
		bool m_ExponentialAttenuation;
		bool m_InvertAttenuationDirection;
};

} /* namespace itk */

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkSliceBySliceAttenuateIntensityImageFilter.hxx"
#endif

#endif
