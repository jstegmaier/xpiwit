/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef __XPIWIT_GRADIENTVECTORFLOWTRACKINGIMAGEFILTER_H
#define __XPIWIT_GRADIENTVECTORFLOWTRACKINGIMAGEFILTER_H

// include required headers
#include "../Base/Management/ITKDefinitions.h"
#include "itkImageToImageFilter.h"
#include "itkImage.h"
#include "itkNumericTraits.h"

using namespace XPIWIT;

namespace itk
{

/**
 * @class GradientVectorFlowTrackingImageFilter extracts local maxima from a given image.
 */
template< class TInputImage, class TOutputImage >
class ITK_EXPORT GradientVectorFlowTrackingImageFilter : public ImageToImageFilter< TInputImage, TOutputImage >
{
public:
    // Extract dimension from input and output image.
    itkStaticConstMacro(InputImageDimension, unsigned int, TInputImage::ImageDimension);
    itkStaticConstMacro(OutputImageDimension, unsigned int, TOutputImage::ImageDimension);

    // Convenient typedefs for simplifying declarations.
    typedef TInputImage  InputImageType;
    typedef TOutputImage OutputImageType;

    // Standard class typedefs.
    typedef GradientVectorFlowTrackingImageFilter                   Self;
    typedef ImageToImageFilter< InputImageType, OutputImageType >   Superclass;
    typedef SmartPointer< Self >                                    Pointer;
    typedef SmartPointer< const Self >                              ConstPointer;

    // Method for creation through the object factory.
    itkNewMacro(Self);

    // Run-time type information (and related methods).
    itkTypeMacro(GradientVectorFlowTrackingImageFilter, ImageToImageFilter);
   
    // define additional image types
    typedef typename InputImageType::PixelType InputPixelType;
    typedef typename OutputImageType::PixelType OutputPixelType;
    typedef typename OutputImageType::RegionType OutputImageRegionType;
    typedef itk::Image< unsigned short, TInputImage::ImageDimension > LabelImageType;
    typedef itk::Image<typename TInputImage::IndexType, TInputImage::ImageDimension> IndexImageType;

    // image inputs
    void SetFlowXImage(typename TInputImage::Pointer flowXImage)    { mFlowXImage = flowXImage; }
    void SetFlowYImage(typename TInputImage::Pointer flowYImage)    { mFlowYImage = flowYImage; }
    void SetFlowZImage(typename TInputImage::Pointer flowZImage)    { mFlowZImage = flowZImage; }
    void SetMaskImage(typename TInputImage::Pointer maskImage)      { mMaskImage = maskImage; }
    
    // parameter set/get functions
    itkSetMacro(NumIterations, unsigned int)
    itkGetMacro(NumIterations, unsigned int)
    itkSetMacro(ClosingRadius, unsigned int)
    itkGetMacro(ClosingRadius, unsigned int)
    itkSetMacro(FullyConnected, bool)
    itkGetMacro(FullyConnected, bool)
    itkSetMacro(IgnoreBackground, bool)
    itkGetMacro(IgnoreBackground, bool)

#ifdef ITK_USE_CONCEPT_CHECKING
    /** Begin concept checking */
    itkConceptMacro( InputHasNumericTraitsCheck, ( Concept::HasNumericTraits< InputPixelType > ) );
    /** End concept checking */
#endif
protected:
    
    // constructor and destructor
    GradientVectorFlowTrackingImageFilter();
    virtual ~GradientVectorFlowTrackingImageFilter();

    // main computation method
    void ThreadedGenerateData(const OutputImageRegionType & outputRegionForThread, ThreadIdType threadId) override;

    // pre- and post processing functions
    void BeforeThreadedGenerateData() override;
    void AfterThreadedGenerateData() override;

private:
    GradientVectorFlowTrackingImageFilter(const Self &); //purposely not implemented
    void operator=(const Self &);  //purposely not implemented

    typename TInputImage::Pointer mFlowXImage;
    typename TInputImage::Pointer mFlowYImage;
    typename TInputImage::Pointer mFlowZImage;
    typename TInputImage::Pointer mMaskImage;
    
    typename LabelImageType::Pointer mSeedImage;
    typename IndexImageType::Pointer mIndexImage;
        
    unsigned int m_NumIterations;
    unsigned int m_ClosingRadius;
    bool m_FullyConnected;
    bool m_IgnoreBackground;
};

} // end namespace itk

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkGradientVectorFlowTrackingImageFilter.txx"
#endif

#endif
