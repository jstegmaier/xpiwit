/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef __XPIWIT_SEEDBASEDTWANGSEGMENTATIONFILTER_HXX
#define __XPIWIT_SEEDBASEDTWANGSEGMENTATIONFILTER_HXX

// include required headers
#include "itkSeedBasedTwangSegmentationFilter.h"
#include "itkImageRegionIterator.h"
#include "itkImageRegionConstIterator.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkExtractRegionPropsImageFilter.h"
#include "itkOtsuThresholdImageFilter.h"
#include "itkGradientImageFilter.h"
#include "itkStatisticsImageFilter.h"
#include <iostream>
#include <fstream>
#include <string>
#include <QString>
#include <QStringList>
#include <QDateTime>
#include "../../Core/Utilities/Logger.h"

// namespace itk
namespace itk
{

// the default constructor
template <class TImageType>
SeedBasedTwangSegmentationFilter<TImageType>::SeedBasedTwangSegmentationFilter()
{
    // set default values for the parameters
    m_WriteRegionProps = true;
    m_Segment3D = true;
    m_FuseSeedPoints = true;
    m_SeedPointMinDistance = 15.0;
    m_MinimumRegionSigma = 0.0;
    m_MinimumWeightedGradientNormalDotProduct = 0.6;
    m_WeightingKernelSizeMultiplicator = 1.0;
    m_WeightingKernelStdDev = 1.0;
    m_GradientImageStdDev = 1.5;
    m_LabelOutput = true;
	m_UseOriginalID = true;
    m_RandomLabels = false;
    m_InputMetaFilter = NULL;
    m_OutputMetaFilter = NULL;
	this->DynamicMultiThreadingOff();
}


// the destructor
template <class TImageType>
SeedBasedTwangSegmentationFilter<TImageType>::~SeedBasedTwangSegmentationFilter()
{
    delete[] m_SeedPoints;
    delete[] m_RegionProps;
    delete[] m_SeedPointCombinations;
}


// before threaded generate data
template <class TImageType>
void SeedBasedTwangSegmentationFilter<TImageType>::BeforeThreadedGenerateData()
{
    // Allocate output
    typename TImageType::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();
    output->FillBuffer( 0 );

    if( m_InputMetaFilter == NULL )
        Logger::GetInstance()->WriteLine( "- ERROR itkSeedBasedTwangSegmentation: no input meta available." );

    // get the z spacing
    typename TImageType::SpacingType spacing = input->GetSpacing();
    
    // get the number of threads
    int numThreads = ProcessObject::GetNumberOfWorkUnits();
    typename TImageType::RegionType splitRegion;
    unsigned int numSplitRegions = this->SplitRequestedRegion(0, this->GetNumberOfWorkUnits(), splitRegion);
    
    if (numSplitRegions < numThreads)
    {
        numThreads = numSplitRegions;
        std::cout << "Setting number of threads to maximum number of feasible split regions: " << numSplitRegions << std::endl;
    }
    
    this->SetNumberOfWorkUnits(numThreads);
    m_SeedPoints = new std::vector<SeedPointType>[numThreads];
    m_RegionProps = new std::vector<RegionPropsType>[numThreads];
    m_SeedPointCombinations = new std::vector<int>[numThreads];

    m_NumKeyPoints = m_InputMetaFilter->mData.length();
    std::vector<SeedPointType> originalSeedPoints;
    std::vector<int> originalSeedPointCombinations;

    int currentSeedId = 1;
    for(int i = 0; i < m_NumKeyPoints; i++){
        QList<float> line = m_InputMetaFilter->mData.at(i);

        typename TImageType::IndexType index;
        for ( int j = 0; j < TImageType::ImageDimension; ++j )
            index[j] = int( 0.5+line.at(j+2)/spacing[j] );

        // write keypoint
		if (m_UseOriginalID == true)
			originalSeedPoints.push_back( SeedPointType(index, line.at(1), int(line.at(0)), line.at(5)) ); // third parameter was set to currentSeedId
		else
			originalSeedPoints.push_back(SeedPointType(index, line.at(1), currentSeedId, line.at(5))); // third parameter was set to currentSeedId
        originalSeedPointCombinations.push_back(1);
        currentSeedId++;
    }


    // print the spacing of the investigated image
    Logger::GetInstance()->WriteLine( "+ Seed locations were transformed to image space with the following spacing: [" +
        QString::number(spacing[0]) + ", " +  QString::number(spacing[1]) + ", " +  QString::number(spacing[2]) + "]" );

    // distribute seed points across available threads
    for (int i=0; i<originalSeedPoints.size(); ++i)
    {
        int currentThread = i % numThreads;
        m_SeedPoints[currentThread].push_back( originalSeedPoints[i] );
    }
    
    int totalSeeds = 0;
    for (int i=0; i<numThreads; ++i)
    {
        std::cout << "Seeds for thread " << i << ": "<< m_SeedPoints[i].size() << std::endl;
        totalSeeds += m_SeedPoints[i].size();
    }
    std::cout << "Total Seeds: " << totalSeeds << std::endl;
    
    std::cout << "Maximum number of threads: " << itk::MultiThreaderBase::GetGlobalDefaultNumberOfThreads() << std::endl;
    std::cout << "Current number of threads: " << this->GetNumberOfWorkUnits() << std::endl;
}


// the thread generate data
template <class TImageType>
void SeedBasedTwangSegmentationFilter<TImageType>::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId)
{
    // Allocate output
    typename TImageType::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();
    typename TImageType::RegionType largestPossibleRegion = input->GetLargestPossibleRegion();
    typename TImageType::SpacingType spacing = input->GetSpacing();

    // get the number of threads
    int numThreads = this->GetNumberOfWorkUnits();
    
    // declare region variables for the region containing the seed point and the image dimension
    typename TImageType::IndexType cropIndex;
    typename TImageType::SizeType cropSize;
    typename TImageType::RegionType cropRegion;
    const int numDimensions = TImageType::ImageDimension;

    // iterate over all seeds and extract the ellipsoid from the gradient image
    const int numSeedsPerThread = m_SeedPoints[threadId].size();
	unsigned int currentObjectIndex = 1;
    for (int i=0; i<numSeedsPerThread; ++i)
    {
        // get the current seed index and scale
        typename TImageType::IndexType seedPosition = m_SeedPoints[threadId][i].GetIndex();

        /*
		if( !output->GetBufferedRegion().IsInside(seedPosition) )
			continue;

        // check if seed position is already occupied by another region. Skip if true.
        if (output->GetPixel(seedPosition) > 0)
            continue;
        */

        typename TImageType::IndexType relativeSeedPosition;
        const int seedScale = (int)m_SeedPoints[threadId][i].GetScale();
        const float seedRadius = 1.41*seedScale;
        const int seedID = (int)m_SeedPoints[threadId][i].GetID();

        // calculate the mask size
        const int regionRadius = 3*seedRadius;
		
        // specify the current seed region
        for (int j=0; j<numDimensions; ++j)
        {
            cropIndex[j] = std::max<int>(0, seedPosition[j]-(regionRadius/spacing[j]));
            cropSize[j] = std::min<int>(largestPossibleRegion.GetSize(j)-cropIndex[j], seedPosition[j]-cropIndex[j]+(regionRadius/spacing[j]));
        }
        cropRegion.SetIndex( cropIndex );
        cropRegion.SetSize( cropSize );

        // cropped input image
        typename TImageType::Pointer inputImageCropped = TImageType::New();
        inputImageCropped->SetRegions( cropRegion );
        inputImageCropped->SetSpacing( input->GetSpacing() );
        inputImageCropped->Allocate();
        inputImageCropped->FillBuffer( 0 );

        // define input iterators
        ImageRegionConstIterator<TImageType> inputIterator( input, cropRegion );
        ImageRegionIterator<TImageType> inputImageCroppedIterator( inputImageCropped, cropRegion );
        inputIterator.GoToBegin();
        inputImageCroppedIterator.GoToBegin();

        // fill the cropped image
        float minimum = 65535;
        float maximum = 0;
        float currentValue = 0;
        while( inputIterator.IsAtEnd() == false)
        {
            currentValue = inputIterator.Value();
            inputImageCroppedIterator.Set( currentValue );

            if (currentValue > maximum)
                maximum = currentValue;

            if (currentValue < minimum)
                minimum = currentValue;

            ++inputIterator;
            ++inputImageCroppedIterator;
        }

        // temporary label image
        typename TImageType::Pointer labelImage = TImageType::New();
        labelImage->SetRegions( cropRegion );
        labelImage->SetSpacing( input->GetSpacing() );
        labelImage->Allocate();
        labelImage->FillBuffer( minimum ); // --> statisticsImageFilter->GetMinimum()

        // calculate the relative seed position in the cropped region
        for (int j=0; j<numDimensions; ++j)
            relativeSeedPosition[j] = seedPosition[j] - cropIndex[j];

        // smooth the input image
        typedef itk::RecursiveGaussianImageFilter<TImageType> RecursiveGaussianImageFilterType;
        typename RecursiveGaussianImageFilterType::Pointer gaussianImageFilter = RecursiveGaussianImageFilterType::New();
        gaussianImageFilter->SetInput( input );
        gaussianImageFilter->GetOutput()->SetRequestedRegion( cropRegion );
        gaussianImageFilter->SetNumberOfWorkUnits( 1 );
        gaussianImageFilter->SetNormalizeAcrossScale( true );
        gaussianImageFilter->SetSigma( m_GradientImageStdDev );

        // calculate the image gradient
        typedef itk::GradientImageFilter<TImageType> GradientImageFilterType;
        typename GradientImageFilterType::Pointer gradientImageFilter = GradientImageFilterType::New();
        gradientImageFilter->SetInput( gaussianImageFilter->GetOutput() );
        gradientImageFilter->SetUseImageSpacing( true );
        gradientImageFilter->SetNumberOfWorkUnits( 1 );
        gradientImageFilter->GetOutput()->SetRequestedRegion( cropRegion );
        itkTryCatch( gradientImageFilter->Update(), "EllipsoidFitImageFilter -> Gradient image filter" );

        // define output, label and gradient iterators
        ImageRegionIterator<TImageType> outputIterator( output, cropRegion );
        ImageRegionIterator<TImageType> labelImageIterator( labelImage, cropRegion );
        ImageRegionIterator<Image< CovariantVector< float, numDimensions>, numDimensions > > gradientIterator( gradientImageFilter->GetOutput(), cropRegion );

        // reset the iterators
        gradientIterator.GoToBegin();
        labelImageIterator.GoToBegin();
        inputImageCroppedIterator.GoToBegin();

        // declare variables needed in the loop
        typename TImageType::IndexType currentIndex;
        CovariantVector< float, numDimensions> currentNormal, currentGradient;
        float pixelDistanceFromSeed, dotProduct, weightingKernel, intensityValue;
        float kernelSize = seedRadius * m_WeightingKernelSizeMultiplicator;
        float kernelStdDev = (m_WeightingKernelStdDev < 0) ? kernelSize : m_WeightingKernelStdDev;

        // iterate trough the seed region and set voxels for the initial LSF
        while (inputImageCroppedIterator.IsAtEnd() == false)
        {
            // calculate the seed normal
            currentIndex = inputImageCroppedIterator.GetIndex();
            for (int j=0; j<numDimensions; ++j)
                currentNormal.SetElement( j, (seedPosition[j] - currentIndex[j])*spacing[j] );

            // calculate the distance of the current pixel from the seed location
            pixelDistanceFromSeed = currentNormal.GetNorm();
            currentNormal.Normalize();

            // calculate the normalized gradient vector
            currentGradient = gradientIterator.Get();
            currentGradient.Normalize();

            // calculate the dot product and the weighting kernel
            dotProduct = 0.0;
            for (int j=0; j<numDimensions; ++j)
                dotProduct += currentNormal.GetElement(j)*currentGradient.GetElement(j);

            // calculate the weighting kernel for the current pixel
            //weightingKernel = (float)exp(-(powf(pixelDistanceFromSeed-kernelSize, 2)) / (2*powf(kernelSize, 2)));
            weightingKernel = (float)exp(-(powf(pixelDistanceFromSeed-kernelSize, 2)) / (2*powf(kernelStdDev, 2)));

            // calculate the weighted intensity value
            intensityValue = 0.5*(1+dotProduct) * weightingKernel;

            // check if intensity value is higher than a threshold or distance is smaller than the seed scale
            if (pixelDistanceFromSeed < kernelSize)
                labelImageIterator.Set( inputImageCroppedIterator.Value() );
            else if (intensityValue > m_MinimumWeightedGradientNormalDotProduct) // was 0.6 before
                labelImageIterator.Set( weightingKernel*inputImageCroppedIterator.Value() );

            // increase the iterators
            ++gradientIterator;
            ++inputImageCroppedIterator;
            ++labelImageIterator;
        }

        // ATTENTION: Region of interest provided by otsu is 0 and not the high intensity region in this case!
        typedef typename itk::OtsuThresholdImageFilter<TImageType, itk::Image<unsigned short, TImageType::ImageDimension> > OtsuThresholdImageFilterType;
        typename OtsuThresholdImageFilterType::Pointer otsuThreshold = OtsuThresholdImageFilterType::New();
        otsuThreshold->SetReleaseDataFlag( false );
        otsuThreshold->SetInput( labelImage );
        otsuThreshold->SetNumberOfWorkUnits( 1 );
        otsuThreshold->SetInsideValue( 65535 );
        otsuThreshold->SetOutsideValue( 0 );

        // update the otsu filter
        itkTryCatchWithContinue( otsuThreshold->Update(), "SeedBasedEllipsodFitImageFilter --> Otsu Threshold." );

        // check if regionprops should be generated
        if (m_WriteRegionProps == false)
        {
            // define otsu iterator
            ImageRegionIterator<itk::Image<unsigned short, TImageType::ImageDimension> > otsuIterator( otsuThreshold->GetOutput(), cropRegion );

            // reset the iterators
            outputIterator.GoToBegin();
            otsuIterator.GoToBegin();
            inputImageCroppedIterator.GoToBegin();

            // region label
            int regionLabel = 0;
            if (m_RandomLabels == true)
			{
                regionLabel = 65535 * (float(rand() % 100)/100.0)*0.8 + 0.2;
			}
            else
			{
                regionLabel = m_SeedPoints[threadId][i].GetID(); //(startIndex + currentObjectIndex); // * (1.0 / m_NumKeyPoints);
				//currentObjectIndex++;
			}

            // fill the extracted region with the corresponding label
            while (outputIterator.IsAtEnd() == false)
            {
                // set pixels that belong to the segmented region
                if (otsuIterator.Value() == 0)
                {
                    if (m_LabelOutput)
                        outputIterator.Set( regionLabel );
                    else
                        outputIterator.Set( inputImageCroppedIterator.Value() );
                }

                // increment the iterators
                ++outputIterator;
                ++inputImageCroppedIterator;
                ++otsuIterator;
            }
        }
        else
        {
            // initialize the geometry extraction filter
            typedef typename itk::LabelGeometryImageFilter<itk::Image<unsigned short, TImageType::ImageDimension>, TImageType> LabelGeometryImageFilterType;
            typename LabelGeometryImageFilterType::Pointer labelGeometryFilter =  LabelGeometryImageFilterType::New();
            //labelGeometryFilter->SetCalculateOrientedBoundingBox( true );
            //labelGeometryFilter->SetCalculateOrientedIntensityRegions( true );
            //labelGeometryFilter->SetCalculateOrientedLabelRegions( true );
            //labelGeometryFilter->SetCalculatePixelIndices( true );
            labelGeometryFilter->SetReleaseDataFlag( true );
            labelGeometryFilter->SetNumberOfWorkUnits( 1 );
            labelGeometryFilter->SetIntensityInput( inputImageCropped );
            labelGeometryFilter->SetInput( otsuThreshold->GetOutput() );
            labelGeometryFilter->GetOutput()->SetRequestedRegion( cropRegion );

            // update the geometry extraction filter
            itkTryCatch( labelGeometryFilter->Update(), "ExtractRecd /mediagionPropsImageFilter" );

            // get the labels
            typename LabelGeometryImageFilterType::LabelsType allLabels = labelGeometryFilter->GetLabels();
            typename LabelGeometryImageFilterType::LabelsType::iterator allLabelsIt;

            // iterate trough the labels and extract nucleus information
            int minLabel = 0;
            int minVolume = (int)floorf(0.5+labelGeometryFilter->GetVolume(minLabel));
            for( allLabelsIt = allLabels.begin(); allLabelsIt != allLabels.end(); allLabelsIt++ )
            {
                // extract infos of the region and store them in a temporary variable
                typename LabelGeometryImageFilterType::LabelPixelType labelValue = *allLabelsIt;

                // find smallest region and the associated label
                int currentVolume = (int)floorf(0.5+labelGeometryFilter->GetVolume(labelValue));
                if (minVolume > currentVolume)
                {
                    minVolume = currentVolume;
                    minLabel = labelValue;
                }
            }

            /*
            // check if seed location has the foreground label
            if (otsuThreshold->GetOutput()->GetPixel( seedPosition ) != minLabel)
                continue;

            if (labelGeometryFilter->GetVolume(minLabel)/((cropSize[0]*cropSize[1]*cropSize[2])-labelGeometryFilter->GetVolume(minLabel)) > 0.2)
               continue;
            */

            // define otsu iterator
            ImageRegionIterator<itk::Image<unsigned short, TImageType::ImageDimension> > otsuIterator( otsuThreshold->GetOutput(), cropRegion );

            // initialize variables for minimum and maximum intensity values
            float minIntensityForeground = 1.0;
            float maxIntensityForeground = 0.0;
            float minIntensityBackground = 1.0;
            float maxIntensityBackground = 0.0;
            int volumeForeground = 0;
            int volumeBackground = 0;
            float foregroundIntensity = 0.0;
            float backgroundIntensity = 0.0;
            float seedIntensity = input->GetPixel( seedPosition );
            float integratedGradientMagnitude = 0.0;

            // reset the iterators
            outputIterator.GoToBegin();
            otsuIterator.GoToBegin();
            //labelImageIterator.GoToBegin();
            inputImageCroppedIterator.GoToBegin();
            gradientIterator.GoToBegin();

            // region label
            //float regionLabel = (float(rand() % 100)/100.0)*0.8 + 0.2;
			int regionLabel = 0;
			if (m_RandomLabels == true)
				regionLabel = 65535 * (float(rand() % 100)/100.0)*0.8 + 0.2;
			else
				regionLabel = m_SeedPoints[threadId][i].GetID(); //(startIndex + currentObjectIndex); // * (1.0 / m_NumKeyPoints);
			//currentObjectIndex++;

            // fill the extracted region with the corresponding label
            while (outputIterator.IsAtEnd() == false)
            {
                // set pixels that belong to the segmented region
                if (otsuIterator.Value() == minLabel)
                {
                    integratedGradientMagnitude += gradientIterator.Get().GetNorm();
                    //outputIterator.Set( (startIndex + i) * (1.0 / m_NumKeyPoints) );
                    //outputIterator.Set( regionLabel );
                    if (m_LabelOutput)
                        outputIterator.Set( regionLabel );
                    else
                        outputIterator.Set( inputImageCroppedIterator.Value() );

                    if (inputImageCroppedIterator.Value() < minIntensityForeground)
                        minIntensityForeground = inputImageCroppedIterator.Value();

                    if (inputImageCroppedIterator.Value() > maxIntensityForeground)
                        maxIntensityForeground = inputImageCroppedIterator.Value();

                    // increase foreground intensity and number of foreground pixels
                    foregroundIntensity += inputImageCroppedIterator.Value();
                    ++volumeForeground;
                }
                else
                {
                    if (inputImageCroppedIterator.Value() < minIntensityBackground)
                        minIntensityBackground = inputImageCroppedIterator.Value();

                    if (inputImageCroppedIterator.Value() > maxIntensityBackground)
                        maxIntensityBackground = inputImageCroppedIterator.Value();

                    backgroundIntensity += inputImageCroppedIterator.Value();
                    ++volumeBackground;
                }

                // increment the iterators
                //++labelImageIterator;
                ++outputIterator;
                ++otsuIterator;
                ++inputImageCroppedIterator;
                ++gradientIterator;
            }

            foregroundIntensity /= (float)volumeForeground;
            backgroundIntensity /= (float)volumeBackground;

            // scheme: id, size, xpos, ypos, zpos, xsize, ysize, zsize, weightedx, weightedy, weightedz, minaxis, medaxis, majaxis, eccentricity, elongation, orientation, integratedintensity, ...
            //         sigma, seedIntensity, seedScale, minIntensityForeground, maxIntensityForeground, minIntensityBackground, maxIntensityBackground, volumeForeground, volumeBackground, foregroundIntensity, backgroundIntensity, fg/bg ratio,integratedGradientMagnitude
            if (numDimensions == 3)
            {
                RegionProps currentRegion(	regionLabel,
                                            (int)floorf(0.5+labelGeometryFilter->GetVolume(minLabel)),
                                            (int)floorf(0.5+labelGeometryFilter->GetCentroid(minLabel)[0]),
                                            (int)floorf(0.5+labelGeometryFilter->GetCentroid(minLabel)[1]),
                                            (int)floorf(0.5+labelGeometryFilter->GetCentroid(minLabel)[2]),
                                            labelGeometryFilter->GetBoundingBoxSize(minLabel)[0],
                                            labelGeometryFilter->GetBoundingBoxSize(minLabel)[1],
                                            labelGeometryFilter->GetBoundingBoxSize(minLabel)[2],
                                            (float)labelGeometryFilter->GetWeightedCentroid(minLabel)[0],
                                            (float)labelGeometryFilter->GetWeightedCentroid(minLabel)[1],
                                            (float)labelGeometryFilter->GetWeightedCentroid(minLabel)[2],
                                            labelGeometryFilter->GetAxesLength(minLabel)[0],
                                            labelGeometryFilter->GetAxesLength(minLabel)[1],
                                            labelGeometryFilter->GetAxesLength(minLabel)[2],
                                            labelGeometryFilter->GetEccentricity(minLabel),
                                            labelGeometryFilter->GetElongation(minLabel),
                                            labelGeometryFilter->GetOrientation(minLabel),
                                            labelGeometryFilter->GetIntegratedIntensity(minLabel),
                                            0,
                                            seedIntensity,
                                            seedScale,
                                            minIntensityForeground,
                                            maxIntensityForeground,
                                            minIntensityBackground,
                                            maxIntensityBackground,
                                            volumeForeground,
                                            volumeBackground,
                                            foregroundIntensity,
                                            backgroundIntensity,
                                            foregroundIntensity / backgroundIntensity,
                                            integratedGradientMagnitude);
				

                // add the current region
                m_RegionProps[threadId].push_back( currentRegion );
            }
            else
            {
                RegionProps currentRegion(	regionLabel,
                                            (int)floorf(0.5+labelGeometryFilter->GetVolume(minLabel)),
                                            (int)floorf(0.5+labelGeometryFilter->GetCentroid(minLabel)[0]),
                                            (int)floorf(0.5+labelGeometryFilter->GetCentroid(minLabel)[1]),
                                            0,
                                            labelGeometryFilter->GetBoundingBoxSize(minLabel)[0],
                                            labelGeometryFilter->GetBoundingBoxSize(minLabel)[1],
                                            0,
                                            (float)labelGeometryFilter->GetWeightedCentroid(minLabel)[0],
                                            (float)labelGeometryFilter->GetWeightedCentroid(minLabel)[1],
                                            0,
                                            labelGeometryFilter->GetAxesLength(minLabel)[0],
                                            labelGeometryFilter->GetAxesLength(minLabel)[1],
                                            0,
                                            labelGeometryFilter->GetEccentricity(minLabel),
                                            labelGeometryFilter->GetElongation(minLabel),
                                            labelGeometryFilter->GetOrientation(minLabel),
                                            labelGeometryFilter->GetIntegratedIntensity(minLabel),
                                            0,
                                            seedIntensity,
                                            seedScale,
                                            minIntensityForeground,
                                            maxIntensityForeground,
                                            minIntensityBackground,
                                            maxIntensityBackground,
                                            volumeForeground,
                                            volumeBackground,
                                            foregroundIntensity,
                                            backgroundIntensity,
                                            foregroundIntensity / backgroundIntensity,
                                            integratedGradientMagnitude);

                // add the current region
                m_RegionProps[threadId].push_back( currentRegion );
            }
        }
    }
}


// after threaded generate data
template <class TImageType>
void SeedBasedTwangSegmentationFilter<TImageType>::AfterThreadedGenerateData()
{
    if( m_OutputMetaFilter != NULL ){
        QStringList metaDescription;                    QStringList metaType;
        metaDescription << "id";                        metaType << "int";
        metaDescription << "size";                      metaType << "int";
        metaDescription << "xpos";                      metaType << "int";
        metaDescription << "ypos";                      metaType << "int";
        metaDescription << "zpos";                      metaType << "int";
        metaDescription << "xsize";                     metaType << "int";
        metaDescription << "ysize";                     metaType << "int";
        metaDescription << "zsize";                     metaType << "int";
        metaDescription << "weightedx";                 metaType << "float";
        metaDescription << "weightedy";                 metaType << "float";
        metaDescription << "weightedz";                 metaType << "float";
        metaDescription << "minaxis";                   metaType << "float";
        metaDescription << "medaxis";                   metaType << "float";
        metaDescription << "majaxis";                   metaType << "float";
        metaDescription << "eccentricity";              metaType << "float";
        metaDescription << "elongation";                metaType << "float";
        metaDescription << "orientation";               metaType << "float";
        metaDescription << "integratedintensty";        metaType << "float";
        metaDescription << "sigma";                     metaType << "float";
        metaDescription << "seedIntensity";             metaType << "float";
        metaDescription << "seedScale";                 metaType << "float";
        metaDescription << "minIntensityForeground";    metaType << "float";
        metaDescription << "maxIntensityForeground";    metaType << "float";
        metaDescription << "minIntensityBackground";    metaType << "float";
        metaDescription << "maxIntensityBackground";    metaType << "float";
        metaDescription << "volumeForeground";          metaType << "int";
        metaDescription << "volumeBackground";          metaType << "int";
        metaDescription << "foregroundIntensity";       metaType << "float";
        metaDescription << "backgroundIntensity";       metaType << "float";
        metaDescription << "fg/bg ratio";               metaType << "float";
		metaDescription << "integratedGradientMag";     metaType << "float";

        m_OutputMetaFilter->mTitle = metaDescription;
        m_OutputMetaFilter->mType = metaType;
    }

    m_OutputMetaFilter->mPostfix = "RegionProps";

    // get the number of threads
    int numThreads = this->GetNumberOfWorkUnits();

    // extract nucleus information
    unsigned int currentKeyPointID = 0;

    // iterate trough the labels and extract nucleus information
    for(int i=0; i<numThreads; ++i)
    {
        const unsigned int numRegionPropsPerThread = m_RegionProps[i].size();
        for (int j=0; j<numRegionPropsPerThread; ++j)
        {
            QList<float> line;
			line << m_RegionProps[i][j].GetID();
            line << m_RegionProps[i][j].GetVolume();
            line << m_RegionProps[i][j].GetX();
            line << m_RegionProps[i][j].GetY();
            line << m_RegionProps[i][j].GetZ();
            line << m_RegionProps[i][j].GetWidth();
            line << m_RegionProps[i][j].GetHeight();
            line << m_RegionProps[i][j].GetDepth();
            line << m_RegionProps[i][j].GetWeightedX();
            line << m_RegionProps[i][j].GetWeightedY();
            line << m_RegionProps[i][j].GetWeightedZ();
            line << m_RegionProps[i][j].GetMinorAxis();
            line << m_RegionProps[i][j].GetMediumAxis();
            line << m_RegionProps[i][j].GetMajorAxis();
            line << m_RegionProps[i][j].GetEccentricity();
            line << m_RegionProps[i][j].GetElongation();
            line << m_RegionProps[i][j].GetOrientation();
            line << m_RegionProps[i][j].GetIntegratedIntensity();
            line << m_RegionProps[i][j].GetRegionSigma();
            line << m_RegionProps[i][j].GetSeedIntensity();
            line << m_RegionProps[i][j].GetSeedScale();
            line << m_RegionProps[i][j].GetMinIntensityForeground();
            line << m_RegionProps[i][j].GetMaxIntensityForeground();
            line << m_RegionProps[i][j].GetMinIntensityBackground();
            line << m_RegionProps[i][j].GetMaxIntensityBackground();
            line << m_RegionProps[i][j].GetVolumeForeground();
            line << m_RegionProps[i][j].GetVolumeBackground();
            line << m_RegionProps[i][j].GetIntensityForeground();
            line << m_RegionProps[i][j].GetIntensityBackground();
            line << m_RegionProps[i][j].GetForegroundBackgroundRatio();
            line << m_RegionProps[i][j].GetIntegratedGradientMagnitude();

            m_OutputMetaFilter->mData.append( line );
            ++currentKeyPointID;
        }
    }

    // log number of extracted regions
    Logger::GetInstance()->WriteLine( "+ ExtractRegionPropsImageFilter: Found " + QString::number(currentKeyPointID) + " connected regions in the image." );

	m_OutputMetaFilter->mIsMultiDimensional = true;

    // check if regionprops should be written at all
    if (m_WriteRegionProps == false)
        return;
}

} // end namespace itk

#endif
