/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef __XPIWIT_SEEDBASEDWATERSHEDSEGMENTATIONFILTER_HXX
#define __XPIWIT_SEEDBASEDWATERSHEDSEGMENTATIONFILTER_HXX

// include required headers
#include "itkSeedBasedWatershedSegmentationFilter.h"
#include "itkMorphologicalWatershedImageFilter.h"
#include "itkMorphologicalWatershedFromMarkersImageFilter.h"
#include "itkInvertIntensityImageFilter.h"
#include "itkSignedMaurerDistanceMapImageFilter.h"
#include "itkDanielssonDistanceMapImageFilter.h"
#include "itkConfidenceConnectedImageFilter.h"
#include "itkConnectedThresholdImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkMultiplyImageFilter.h"
#include "itkImageRegionIterator.h"
#include "itkImageRegionConstIterator.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkExtractRegionPropsImageFilter.h"
#include "itkOtsuThresholdImageFilter.h"
#include "itkGradientImageFilter.h"
#include "itkStatisticsImageFilter.h"
#include "itkFlatStructuringElement.h"
#include "itkGrayscaleDilateImageFilter.h"
#include <iostream>
#include <fstream>
#include <string>
#include <limits>
#include <QString>
#include <QStringList>
#include <QDateTime>
#include <float.h>
#include "../../Core/Utilities/Logger.h"

// namespace itk
namespace itk
{

// the default constructor
template <class TImageType>
SeedBasedWatershedSegmentationFilter<TImageType>::SeedBasedWatershedSegmentationFilter()
{
    // set default values for the parameters
    m_WriteRegionProps = true;
    m_LabelOutput = true;
    m_RandomLabels = false;
	m_SeedRadiusMultiplier = 3.0;
	m_InvertIntensityImage = true;
    m_InputMetaFilter = NULL;
    m_OutputMetaFilter = NULL;
	m_UseOriginalID = false;
	m_UseRegionPropsAABB = false;
	m_UseRegionGrowing = false;
	m_MarkWatershedLine = false;
	m_SeedDilationRadius = 2;
	m_MaxSeedLabel = 0;
	m_NumSeedPoints = 0;
	this->DynamicMultiThreadingOff();
}


// the destructor
template <class TImageType>
SeedBasedWatershedSegmentationFilter<TImageType>::~SeedBasedWatershedSegmentationFilter()
{
    delete[] m_SeedPoints;
    delete[] m_RegionProps;
    delete[] m_SeedPointCombinations;
}


// before threaded generate data
template <class TImageType>
void SeedBasedWatershedSegmentationFilter<TImageType>::BeforeThreadedGenerateData()
{
    // Allocate output
    typename TImageType::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();
    output->FillBuffer( 0 );

	// create a seed image
	m_SeedImage = SeedImageType::New();
    m_SeedImage->SetRegions( input->GetLargestPossibleRegion() );
    m_SeedImage->SetSpacing( input->GetSpacing() );
	m_SeedImage->SetReleaseDataFlag( false );
    m_SeedImage->Allocate();
    m_SeedImage->FillBuffer( 0 );

	std::cout << "Hello? Is it mee you're looking for?" << std::endl;

    if( m_InputMetaFilter == NULL )
        Logger::GetInstance()->WriteLine( "- ERROR itkSeedBasedTwangSegmentation: no input meta available." );

    // get the z spacing
    typename TImageType::SpacingType spacing = input->GetSpacing();
    
    // get the number of threads
    int numThreads = ProcessObject::GetNumberOfWorkUnits();
    typename TImageType::RegionType splitRegion;
    unsigned int numSplitRegions = this->SplitRequestedRegion(0, this->GetNumberOfWorkUnits(), splitRegion);
    
    if (numSplitRegions < numThreads)
    {
        numThreads = numSplitRegions;
        std::cout << "Setting number of threads to maximum number of feasible split regions: " << numSplitRegions << std::endl;
    }
    
	if (m_UseRegionPropsAABB == true)
		std::cout << "Using AABB extents for ROI estimation ..." << std::endl;

    this->SetNumberOfWorkUnits(numThreads);
    m_SeedPoints = new std::vector<SeedPointType>[numThreads];
    m_RegionProps = new std::vector<RegionPropsType>[numThreads];
    m_SeedPointCombinations = new std::vector<int>[numThreads];

    m_NumSeedPoints = m_InputMetaFilter->mData.length();
	m_MaxSeedLabel = 0;
    std::vector<SeedPointType> originalSeedPoints;
    std::vector<int> originalSeedPointCombinations;

    int currentSeedId = 1;
    for(int i = 0; i < m_NumSeedPoints; i++){
        QList<float> line = m_InputMetaFilter->mData.at(i);

        typename TImageType::IndexType index;
        for ( int j = 0; j < TImageType::ImageDimension; ++j )
            index[j] = int( 0.5+(float)line.at(j+2)/(float)spacing[j] );

		if (m_UseOriginalID == true)
		{
			output->SetPixel(index, line.at(0));
			m_SeedImage->SetPixel(index, line.at(0));
		}
		else
		{
			output->SetPixel(index, currentSeedId);
			m_SeedImage->SetPixel(index, currentSeedId);
		}

		// identify the maximum seed label
		m_MaxSeedLabel = std::max<int>(m_MaxSeedLabel, line.at(0));

        // write keypoint
		float scale = line.at(1);
		if (m_UseRegionPropsAABB == true)
			scale = std::max<float>(line.at(5), line.at(6)) / 2;

		if (m_UseOriginalID == true)
			originalSeedPoints.push_back(SeedPointType(index, scale, line.at(0), line.at(5))); // third parameter was set to line.at(0)
		else
			originalSeedPoints.push_back(SeedPointType(index, scale, currentSeedId, line.at(5))); // third parameter was set to line.at(0)
        originalSeedPointCombinations.push_back(1);
        currentSeedId++;
    }

	// seed dilation
	if (m_SeedDilationRadius > 0)
	{
		// dilate the seed points to get more reliable region growing
		typedef itk::FlatStructuringElement< TImageType::ImageDimension > StructerElementType;
		typename StructerElementType::RadiusType structureElementRadius;
		structureElementRadius.Fill(m_SeedDilationRadius);
		StructerElementType structureElement;
		structureElement = StructerElementType::Ball(structureElementRadius);

		// setup the filter
		typedef itk::GrayscaleDilateImageFilter<SeedImageType, SeedImageType, StructerElementType> GrayscaleDilateImageFilterType;
		typename GrayscaleDilateImageFilterType::Pointer dilateFilter = GrayscaleDilateImageFilterType::New();
		dilateFilter->SetInput(m_SeedImage);
		dilateFilter->SetKernel(structureElement);
		dilateFilter->SetReleaseDataFlag(false);
		itkTryCatch(dilateFilter->Update(), "Error: GrayscaleDilateImageFilterWrapper Update Function.");

		m_SeedImage = dilateFilter->GetOutput();
	}

    // print the spacing of the investigated image
	if (TImageType::ImageDimension == 3)
    Logger::GetInstance()->WriteLine( "+ Seed locations were transformed to image space with the following spacing: [" +
        QString::number(spacing[0]) + ", " +  QString::number(spacing[1]) + ", " +  QString::number(spacing[2]) + "]" );
	else
		Logger::GetInstance()->WriteLine("+ Seed locations were transformed to image space with the following spacing: [" + 
		QString::number(spacing[0]) + ", " + QString::number(spacing[1]) + "]");

    // distribute seed points across available threads
    for (int i=0; i<originalSeedPoints.size(); ++i)
    {
        int currentThread = i % numThreads;
        m_SeedPoints[currentThread].push_back( originalSeedPoints[i] );
    }
    
    int totalSeeds = 0;
    for (int i=0; i<numThreads; ++i)
    {
        std::cout << "Seeds for thread " << i << ": "<< m_SeedPoints[i].size() << std::endl;
        totalSeeds += m_SeedPoints[i].size();
    }
    std::cout << "Total Seeds: " << totalSeeds << std::endl;
    
    std::cout << "Maximum number of threads: " << ITK_MAX_THREADS << std::endl;
    std::cout << "Current number of threads: " << this->GetNumberOfWorkUnits() << std::endl;
}


// the thread generate data
template <class TImageType>
void SeedBasedWatershedSegmentationFilter<TImageType>::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId)
{
    // Allocate output
    typename TImageType::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();
    typename TImageType::RegionType largestPossibleRegion = input->GetLargestPossibleRegion();
    typename TImageType::SpacingType spacing = input->GetSpacing();

    // get the number of threads
    int numThreads = this->GetNumberOfWorkUnits();
    
    // declare region variables for the region containing the seed point and the image dimension
    typename TImageType::IndexType cropIndex;
    typename TImageType::SizeType cropSize;
    typename TImageType::RegionType cropRegion;
    const int numDimensions = TImageType::ImageDimension;

    // iterate over all seeds and extract the ellipsoid from the gradient image
    const int numSeedsPerThread = m_SeedPoints[threadId].size();
	unsigned int currentObjectIndex = 1;
    for (int i=0; i<numSeedsPerThread; ++i)
    {
		// get the current seed index and scale
        typename TImageType::IndexType seedPosition = m_SeedPoints[threadId][i].GetIndex();
        typename TImageType::IndexType relativeSeedPosition;
        const int seedScale = (int)m_SeedPoints[threadId][i].GetScale();
        const float seedRadius = seedScale;
        const int seedID = (int)m_SeedPoints[threadId][i].GetID();
		const float seedIntensity = input->GetPixel( seedPosition ); // TODO: maybe compute seed intensity in a small window to be more robust

		//if (seedID != 603)
		//	continue;

        // calculate the mask size
        const int regionRadius = m_SeedRadiusMultiplier*seedRadius;
		
        // specify the current seed region
        for (int j=0; j<numDimensions; ++j)
        {
            cropIndex[j] = std::max<int>(0, seedPosition[j]-(regionRadius/spacing[j]));
            cropSize[j] = std::min<int>(largestPossibleRegion.GetSize(j)-cropIndex[j], seedPosition[j]-cropIndex[j]+(regionRadius/spacing[j]));
        }
        cropRegion.SetIndex( cropIndex );
        cropRegion.SetSize( cropSize );

        // cropped input image
        typename TImageType::Pointer inputImageCropped = TImageType::New();
        inputImageCropped->SetRegions( cropRegion );
        inputImageCropped->SetSpacing( input->GetSpacing() );
		inputImageCropped->SetReleaseDataFlag( false );
        inputImageCropped->Allocate();
        inputImageCropped->FillBuffer( 0 );

        typename SeedImageType::Pointer seedImageCropped = SeedImageType::New();
        seedImageCropped->SetRegions( cropRegion );
        seedImageCropped->SetSpacing( input->GetSpacing() );
		seedImageCropped->SetReleaseDataFlag( false );
        seedImageCropped->Allocate();
        seedImageCropped->FillBuffer( 0 );

        // define input iterators
        ImageRegionConstIterator<TImageType> inputIterator( input, cropRegion );
		ImageRegionConstIterator<SeedImageType> seedInputIterator( m_SeedImage, cropRegion );
        ImageRegionIteratorWithIndex<TImageType> inputImageCroppedIterator( inputImageCropped, cropRegion );
		ImageRegionIterator<SeedImageType> seedImageCroppedIterator( seedImageCropped, cropRegion );
        inputIterator.GoToBegin();
		seedInputIterator.GoToBegin();
        inputImageCroppedIterator.GoToBegin();
		seedImageCroppedIterator.GoToBegin();

		// setup the filter
		typedef itk::Image<unsigned short, TImageType::ImageDimension > SegmentationImageType;
		
        // fill the cropped image
		typename TImageType::IndexType minPosition;
		float maxIntensity = 0;
		float minIntensity = FLT_MAX;
		float centerMeanIntensity = 0;
		double meanIntensity = 0;
		int numVoxels = 0;
		int numSeedVoxels = 0;

        float currentValue = 0;
		float distance = 0;
        while( inputIterator.IsAtEnd() == false)
        {
			// copy the input intensity
            inputImageCroppedIterator.Set( std::min<float>(seedIntensity, inputIterator.Value()) );

			if (inputIterator.Value() > maxIntensity)
				maxIntensity = inputIterator.Value();

			if (inputIterator.Value() < minIntensity)
			{ 
				minIntensity = inputIterator.Value();
				minPosition = inputImageCroppedIterator.GetIndex();
			}

			meanIntensity += inputIterator.Value();
			numVoxels++;

			// set the seed intensity depending on the current seed id
			if (seedInputIterator.Value() > 0)
				seedImageCroppedIterator.Set(seedInputIterator.Value());
				//seedImageCroppedIterator.Set(m_NumSeedPoints+1);
				//seedImageCroppedIterator.Set(seedInputIterator.Value());
			
            // add background seed at the borders of the crop
			for (int j = 0; j < TImageType::ImageDimension; ++j)
				if ((inputImageCroppedIterator.GetIndex()[j] == cropRegion.GetIndex()[j]) ||
					(inputImageCroppedIterator.GetIndex()[j]) == (cropRegion.GetIndex()[j] + cropSize[j] - 1))
					seedImageCroppedIterator.Set(m_MaxSeedLabel + 1);
			
			// set pixels far from the centre to 1 for as the background label
			distance = 0;
			for (int j=0; j<TImageType::ImageDimension; ++j)
				distance += (seedPosition[j]-inputImageCroppedIterator.GetIndex()[j]) * (seedPosition[j]-inputImageCroppedIterator.GetIndex()[j]) * spacing[j] * spacing[j];
			distance = sqrtf(distance);

			if (distance <= m_SeedDilationRadius)
			{
				seedImageCroppedIterator.Set(seedID);
				centerMeanIntensity += inputIterator.Value();
				numSeedVoxels++;
			}
				
			// increase the iterators
            ++inputIterator;
			++seedInputIterator;
            ++inputImageCroppedIterator;
			++seedImageCroppedIterator;
        }

		//seedImageCropped->SetPixel(minPosition, 1);
		meanIntensity /= numVoxels;
		centerMeanIntensity /= numSeedVoxels;

        // calculate the relative seed position in the cropped region
        for (int j=0; j<numDimensions; ++j)
            relativeSeedPosition[j] = seedPosition[j] - cropIndex[j];

		// compute the Otsu theshold value for the current image
		typedef itk::OtsuThresholdImageFilter <TImageType, TImageType> FilterType;
		typename FilterType::Pointer filter = FilterType::New();
		filter->SetInput(inputImageCropped);
		filter->SetInsideValue(0);
		filter->SetOutsideValue(1);
		filter->SetReleaseDataFlag(true);
		itkTryCatch(filter->Update(), "Error: SeedBasedWatershedSegmentationFilter --> confidence connected filter Update Function.");

		// initialize the invert intensity filter
		typedef itk::InvertIntensityImageFilter<TImageType, TImageType> InvertIntensityFilterType;
		typename InvertIntensityFilterType::Pointer invertIntensity = InvertIntensityFilterType::New();
		invertIntensity->SetMaximum(1.0);
		invertIntensity->SetReleaseDataFlag(true);

		typename TImageType::Pointer binaryImage;

		// if enabled use a connected threshold filter to extract the center component. Otherwise, use conditional Otsu.
		if (m_UseRegionGrowing == true)
		{
			// initialize the region growing filter
			typedef itk::ConnectedThresholdImageFilter<TImageType, TImageType> ConnectedThresholdImageFilterType;
			typename ConnectedThresholdImageFilterType::Pointer regionGrowingFilter = ConnectedThresholdImageFilterType::New();
			regionGrowingFilter->SetSeed(seedPosition);
			regionGrowingFilter->SetLower(std::min<float>(centerMeanIntensity, filter->GetThreshold()));
			//regionGrowingFilter->SetLower(filter->GetThreshold());
			regionGrowingFilter->SetUpper(std::numeric_limits<typename TImageType::PixelType>::max());
			regionGrowingFilter->SetReplaceValue(1);
			inputImageCropped->SetReleaseDataFlag(true);
			regionGrowingFilter->SetInput(inputImageCropped);
			itkTryCatch(regionGrowingFilter->Update(), "Error: SeedBasedWatershedSegmentationFilter --> confidence connected filter Update Function.");

			// set the result image
			binaryImage = regionGrowingFilter->GetOutput();
		}
		else
		{
			// initialize a standard binary threshold filter
			typedef itk::BinaryThresholdImageFilter<TImageType, TImageType> ThresholdFilterType;
			typename ThresholdFilterType::Pointer thresholdFilter = ThresholdFilterType::New();
			thresholdFilter->SetInput(inputImageCropped);
			thresholdFilter->SetInsideValue(1.0);
			thresholdFilter->SetOutsideValue(0.0);
			thresholdFilter->SetLowerThreshold(std::min<float>(centerMeanIntensity, filter->GetThreshold()));
            //thresholdFilter->SetLowerThreshold(0.5 * (centerMeanIntensity + filter->GetThreshold()));
			thresholdFilter->SetUpperThreshold(std::numeric_limits<typename TImageType::PixelType>::max());
			itkTryCatch(thresholdFilter->Update(), "Error: SeedBasedWatershedSegmentationFilter --> conditional binary threshold filter Update Function.");
            
			// set the result image
			binaryImage = thresholdFilter->GetOutput();
		}

		// invert the intensity of the binarized image (cell interior becomes black)
		invertIntensity->SetInput(binaryImage);
		itkTryCatch(invertIntensity->Update(), "Error: InvertIntensityImageFilterWrapper Update Function.");
	
		// compute the distance map of the inverted binary binary image
		typedef itk::DanielssonDistanceMapImageFilter<TImageType, TImageType> DistanceMapFilterType;
		typename DistanceMapFilterType::Pointer distanceMap = DistanceMapFilterType::New();
		distanceMap->SetInput(invertIntensity->GetOutput());
		distanceMap->SetInputIsBinary(true);
		distanceMap->SetSquaredDistance(false);
		distanceMap->SetUseImageSpacing(true);
		distanceMap->SetReleaseDataFlag(true);
		itkTryCatch(distanceMap->Update(), "Error: SeedBasedWatershedSegmentationFilter --> Distance map Update Function.");

		// compute the minimum and maximum distances (required for valid image inversion of the distance map image)
		typedef itk::MinimumMaximumImageCalculator<TImageType> MinMaxCalculatorType;
		typename MinMaxCalculatorType::Pointer minMaxCalc = MinMaxCalculatorType::New();
		minMaxCalc->SetImage(distanceMap->GetOutput());
		minMaxCalc->Compute();

		typename TImageType::PixelType minimumDistance = minMaxCalc->GetMinimum();
		typename TImageType::PixelType maximumDistance = minMaxCalc->GetMaximum();

		// invert the distance map using the identified maximum distance
		typename InvertIntensityFilterType::Pointer invertIntensity2 = InvertIntensityFilterType::New();
		invertIntensity2 = InvertIntensityFilterType::New();
		invertIntensity2->SetInput(distanceMap->GetOutput());
		invertIntensity2->SetMaximum(maximumDistance+1);
		invertIntensity2->SetReleaseDataFlag(true);
		itkTryCatch(invertIntensity2->Update(), "Error: InvertIntensityImageFilterWrapper Update Function.");

		// constrain distance map to the binary foreground
		typedef itk::MultiplyImageFilter<TImageType, TImageType, TImageType> MultiplyImageFilterType;
		typename MultiplyImageFilterType::Pointer multiplyFilter = MultiplyImageFilterType::New();
		multiplyFilter->SetInput1(invertIntensity2->GetOutput());
		multiplyFilter->SetInput2(binaryImage);
		multiplyFilter->SetReleaseDataFlag(true);
		itkTryCatch(multiplyFilter->Update(), "Error: SeedBasedWatershedSegmentationFilter --> multiply seed image filter Update Function.");

		typedef itk::MultiplyImageFilter<TImageType, SegmentationImageType, SegmentationImageType> MultiplyImageFilterTypeSegmentation;
		typename MultiplyImageFilterTypeSegmentation::Pointer multiplyFilter3 = MultiplyImageFilterTypeSegmentation::New();
		multiplyFilter3->SetInput1(binaryImage);
		multiplyFilter3->SetInput2(seedImageCropped);
		multiplyFilter3->SetReleaseDataFlag(true);
		itkTryCatch(multiplyFilter3->Update(), "Error: SeedBasedWatershedSegmentationFilter --> multiply seed image filter Update Function.");
		
		// perform a seeded watershed segmentation using a background marker (outer border) and a seed marker (enlarged centroid)
		typedef itk::MorphologicalWatershedFromMarkersImageFilter<TImageType, SegmentationImageType> WatershedFromMarkersFilterType;
		typename WatershedFromMarkersFilterType::Pointer watershedFromMarkersFilter = WatershedFromMarkersFilterType::New();
		watershedFromMarkersFilter->SetInput1(multiplyFilter->GetOutput());
		watershedFromMarkersFilter->SetInput2(multiplyFilter3->GetOutput());
		watershedFromMarkersFilter->SetFullyConnected( true );
		watershedFromMarkersFilter->SetMarkWatershedLine( m_MarkWatershedLine );
		watershedFromMarkersFilter->SetReleaseDataFlag(true);
		watershedFromMarkersFilter->SetNumberOfWorkUnits( 1 );
		itkTryCatch(watershedFromMarkersFilter->Update(), "Error: SeedBasedWatershedSegmentationFilter --> watershed from markers Update Function.");

		typedef itk::MultiplyImageFilter<SegmentationImageType, TImageType, SegmentationImageType> MultiplyImageFilterType2;
		typename MultiplyImageFilterType2::Pointer multiplyFilter2 = MultiplyImageFilterType2::New();
		multiplyFilter2->SetInput1(watershedFromMarkersFilter->GetOutput());
		multiplyFilter2->SetInput2(binaryImage);
		multiplyFilter2->SetReleaseDataFlag(false);
		itkTryCatch(multiplyFilter2->Update(), "Error: SeedBasedWatershedSegmentationFilter --> multiply seed image filter Update Function.");

		/*
		typedef itk::BinaryThresholdImageFilter<SegmentationImageType, SegmentationImageType> ThresholdFilterSegmentationType;
		typename ThresholdFilterSegmentationType::Pointer thresholdFilter2 = ThresholdFilterSegmentationType::New();

		thresholdFilter2->SetInput(multiplyFilter2->GetOutput());
		thresholdFilter2->SetInsideValue(1.0);
		thresholdFilter2->SetOutsideValue(0.0);
		thresholdFilter2->SetLowerThreshold(0.0000001);
		thresholdFilter2->SetUpperThreshold(maxIntensity);
		itkTryCatch(thresholdFilter2->Update(), "Error: SeedBasedWatershedSegmentationFilter --> conditional binary threshold filter Update Function.");


		// determine the center label
		typedef itk::BinaryImageToLabelMapFilter<SegmentationImageType> BinaryImageToLabelMapFilterType;
		typename BinaryImageToLabelMapFilterType::Pointer binaryImageToLabelMapFilter = BinaryImageToLabelMapFilterType::New();
		binaryImageToLabelMapFilter->SetInput(thresholdFilter2->GetOutput());
		itkTryCatch(binaryImageToLabelMapFilter->Update(), "Error: SeedBasedWatershedSegmentationFilter --> multiply seed image filter Update Function.");
		
		typedef itk::LabelMapToLabelImageFilter< typename BinaryImageToLabelMapFilterType::OutputImageType, SegmentationImageType > LabelMapToLabelImageFilterType;
		typename LabelMapToLabelImageFilterType::Pointer labelMapToLabelImageFilter = LabelMapToLabelImageFilterType::New();
		labelMapToLabelImageFilter->SetInput(binaryImageToLabelMapFilter->GetOutput());
		labelMapToLabelImageFilter->SetReleaseDataFlag(false);
		itkTryCatch(labelMapToLabelImageFilter->Update(), "Error: LabelMapToLabelImageFilter update.");

		SegmentationImageType::PixelType centerLabel = labelMapToLabelImageFilter->GetOutput()->GetPixel(seedPosition);
		*/

		// define output iterators
		output->SetReleaseDataFlag(false);
        ImageRegionIterator<TImageType> outputIterator( output, cropRegion );
		ImageRegionIterator<SegmentationImageType> resultIterator(multiplyFilter2->GetOutput(), cropRegion ); // was multiplyFilter2 before...
		//ImageRegionIterator<SegmentationImageType> debugIterator(watershedFromMarkersFilter->GetOutput(), cropRegion); // was multiplyFilter2 before...
		//ImageRegionIterator<SegmentationImageType> seedIterator(multiplyFilter3->GetOutput(), cropRegion);
		bool touchesBorder = false;
        outputIterator.GoToBegin();
		resultIterator.GoToBegin();
		//seedIterator.GoToBegin();
		//debugIterator.GoToBegin();

        // fill the extracted region with the corresponding label
        while (outputIterator.IsAtEnd() == false)
        {


			//if (resultIterator.Value() > 0 && resultIterator.Value() <= m_MaxSeedLabel)
			//	outputIterator.Set(resultIterator.Value());

			if (resultIterator.Value() == seedID)
				outputIterator.Set(seedID);

			//outputIterator.Set(debugIterator.Value());

			//if (seedIterator.Value() > 0)
			//	outputIterator.Set(seedIterator.Value());

			//}

            // increment the iterators
            ++outputIterator;
            ++resultIterator;
			//++seedIterator;
			//++debugIterator;
        }
    }
}


// after threaded generate data
template <class TImageType>
void SeedBasedWatershedSegmentationFilter<TImageType>::AfterThreadedGenerateData()
{
    if( m_OutputMetaFilter != NULL ){
        QStringList metaDescription;                    QStringList metaType;
        metaDescription << "id";                        metaType << "int";
        metaDescription << "size";                      metaType << "int";
        metaDescription << "xpos";                      metaType << "int";
        metaDescription << "ypos";                      metaType << "int";
        metaDescription << "zpos";                      metaType << "int";
        metaDescription << "xsize";                     metaType << "int";
        metaDescription << "ysize";                     metaType << "int";
        metaDescription << "zsize";                     metaType << "int";
        metaDescription << "weightedx";                 metaType << "float";
        metaDescription << "weightedy";                 metaType << "float";
        metaDescription << "weightedz";                 metaType << "float";
        metaDescription << "minaxis";                   metaType << "float";
        metaDescription << "medaxis";                   metaType << "float";
        metaDescription << "majaxis";                   metaType << "float";
        metaDescription << "eccentricity";              metaType << "float";
        metaDescription << "elongation";                metaType << "float";
        metaDescription << "orientation";               metaType << "float";
        metaDescription << "integratedintensty";        metaType << "float";

        m_OutputMetaFilter->mTitle = metaDescription;
        m_OutputMetaFilter->mType = metaType;
    }

    m_OutputMetaFilter->mPostfix = "RegionProps";

    // get the number of threads
    int numThreads = this->GetNumberOfWorkUnits();

    // extract nucleus information
    unsigned int currentKeyPointID = 0;

    // iterate trough the labels and extract nucleus information
    for(int i=0; i<numThreads; ++i)
    {
        const unsigned int numRegionPropsPerThread = m_RegionProps[i].size();
        for (int j=0; j<numRegionPropsPerThread; ++j)
        {
            QList<float> line;
			line << m_RegionProps[i][j].GetID();
            line << m_RegionProps[i][j].GetVolume();
            line << m_RegionProps[i][j].GetX();
            line << m_RegionProps[i][j].GetY();
            line << m_RegionProps[i][j].GetZ();
            line << m_RegionProps[i][j].GetWidth();
            line << m_RegionProps[i][j].GetHeight();
            line << m_RegionProps[i][j].GetDepth();
            line << m_RegionProps[i][j].GetWeightedX();
            line << m_RegionProps[i][j].GetWeightedY();
            line << m_RegionProps[i][j].GetWeightedZ();
            line << m_RegionProps[i][j].GetMinorAxis();
            line << m_RegionProps[i][j].GetMediumAxis();
            line << m_RegionProps[i][j].GetMajorAxis();
            line << m_RegionProps[i][j].GetEccentricity();
            line << m_RegionProps[i][j].GetElongation();
            line << m_RegionProps[i][j].GetOrientation();
            line << m_RegionProps[i][j].GetIntegratedIntensity();

            m_OutputMetaFilter->mData.append( line );
            ++currentKeyPointID;
        }
    }

    // log number of extracted regions
    Logger::GetInstance()->WriteLine( "+ ExtractRegionPropsImageFilter: Found " + QString::number(currentKeyPointID) + " connected regions in the image." );

	m_OutputMetaFilter->mIsMultiDimensional = true;

    // check if regionprops should be written at all
    if (m_WriteRegionProps == false)
        return;
}

} // end namespace itk

#endif
