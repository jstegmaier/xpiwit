/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// include required headers
#include "itkSliceBySliceAttenuateIntensityImageFilter.h"
#include "itkImageRegionIterator.h"
#include "itkImageRegionConstIterator.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkDiscreteGaussianImageFilter.h"
#include <iostream>
#include <fstream>
#include <string>
#include <limits>
#include <QString>
#include <QStringList>
#include <QDateTime>
#include "../../Core/Utilities/Logger.h"

// namespace itk
namespace itk
{

// the default constructor
template <class TImageType, class TOutputImage>
SliceBySliceAttenuateIntensityImageFilter<TImageType, TOutputImage>::SliceBySliceAttenuateIntensityImageFilter()
{
	m_Delta = 0.01;
	m_MinSigma = 1.0;
	m_MaxSigma = 3.0;
	m_UseImageSpacing = 1;
	m_Dimensionality = 3;
	m_MinAttenuation = 0.0;
	m_MaxAttenuation = 0.8;
	m_ExponentialAttenuation = true;
	m_InvertAttenuationDirection = false;
	this->DynamicMultiThreadingOff();
}


// the destructor
template <class TImageType, class TOutputImage>
SliceBySliceAttenuateIntensityImageFilter<TImageType, TOutputImage>::~SliceBySliceAttenuateIntensityImageFilter()
{
}


// before threaded generate data
template <class TImageType, class TOutputImage>
void SliceBySliceAttenuateIntensityImageFilter<TImageType, TOutputImage>::BeforeThreadedGenerateData()
{
    // Allocate output
    typename TOutputImage::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();
}


// the thread generate data
template <class TImageType, class TOutputImage>
void SliceBySliceAttenuateIntensityImageFilter<TImageType, TOutputImage>::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId)
{
    // get input and output pointers
    typename TOutputImage::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();
	
	// initialize the regions to process
	typename TImageType::RegionType currentRegion;
	typename TImageType::IndexType currentIndex;
	typename TImageType::SizeType currentSize;

	for (int j=0; j<TImageType::ImageDimension; ++j)
	{
		currentIndex[j] = outputRegionForThread.GetIndex(j);
		currentSize[j] = outputRegionForThread.GetSize(j);
	}
	currentSize[2] = 1;
	currentRegion.SetIndex( currentIndex );
	currentRegion.SetSize( currentSize );
	
	// create a temporary image used to extract slices
	typename TImageType::Pointer tmpImage = TImageType::New();
	tmpImage->SetRegions(currentRegion);
	tmpImage->SetSpacing( input->GetSpacing() );
	tmpImage->Allocate();
	tmpImage->FillBuffer(0);

	// iterate over all slices and perform watershed separately for each slice
	for (int i=0; i<outputRegionForThread.GetSize(2); ++i)
	{
		// set the current index
		currentIndex[2] = outputRegionForThread.GetIndex(2)+i;
		currentRegion.SetIndex( currentIndex );
		currentRegion.SetSize( currentSize );
		
		// extract the slice to perform the watershed on
		ImageRegionConstIterator<TImageType> inputIterator( input, currentRegion );
		ImageRegionIterator<TOutputImage> outputIterator( output, currentRegion );
		ImageRegionIterator<TOutputImage> sliceIterator( tmpImage, tmpImage->GetLargestPossibleRegion() );
		inputIterator.GoToBegin();
		outputIterator.GoToBegin();
		sliceIterator.GoToBegin();

		// initialize the weight array
		float* probability = new float[currentSize[0]];
		std::fill_n(probability, currentSize[0], 1.0);
		float currentIntensity = 0.0f;
		float absorptionProbability = 0.0f;
		unsigned int currentPosition = 0;
		bool useUddinModel = false;
		float attenuation = 0.0;
		if (m_ExponentialAttenuation == true)
			attenuation = exp(((float)currentIndex[2]/(float)input->GetLargestPossibleRegion().GetSize()[2]) * log(m_MaxAttenuation));
		else
			attenuation = 1.0 - ((float)currentIndex[2]/(float)input->GetLargestPossibleRegion().GetSize()[2]) * m_MaxAttenuation;

		if (m_InvertAttenuationDirection == true)
			attenuation = 1.0 - attenuation;

		// initialize the histogram
		while(inputIterator.IsAtEnd() == false)
		{
			if (useUddinModel == true)
			{
				currentIntensity = inputIterator.Value();
				currentPosition = inputIterator.GetIndex()[0];
				absorptionProbability = currentIntensity * m_Delta * probability[currentPosition];
				probability[currentPosition] = std::max<float>( probability[currentPosition] - absorptionProbability, 0.0 );
				outputIterator.Set( currentIntensity * probability[currentPosition] );
				sliceIterator.Set( currentIntensity * probability[currentPosition] );
			}
			else
			{
				outputIterator.Set( inputIterator.Value() * attenuation );
				sliceIterator.Set( inputIterator.Value() * attenuation );
			}
			
			// increment the iterators
			++inputIterator;
			++outputIterator;
			++sliceIterator;
		}

		// 
		delete[] probability;

		// apply Gaussian blur to the slices with linearly increasing standard deviations
		if (m_MinSigma > 0 || m_MaxSigma > 0)
		{
			float sigma = m_MinSigma + ((float)currentIndex[2]/(float)input->GetLargestPossibleRegion().GetSize()[2]) * m_MaxSigma;
			typedef itk::DiscreteGaussianImageFilter<TImageType, TImageType> GaussianImageFilterType;
			typename GaussianImageFilterType::Pointer gaussianFilter = GaussianImageFilterType::New();
			gaussianFilter->SetInput( tmpImage );
			gaussianFilter->SetNumberOfWorkUnits(1);
			gaussianFilter->SetUseImageSpacing( m_UseImageSpacing );
			gaussianFilter->SetFilterDimensionality( m_Dimensionality );
			gaussianFilter->SetVariance( sigma );
			itkTryCatch( gaussianFilter->Update(), "Exception Caught: Updating the Gaussian filter in the attenuation filter." );

			ImageRegionIterator<TOutputImage> gaussianIterator( gaussianFilter->GetOutput(), gaussianFilter->GetOutput()->GetLargestPossibleRegion() );
			gaussianIterator.GoToBegin();
			outputIterator.GoToBegin();

			while(outputIterator.IsAtEnd() == false)
			{
				outputIterator.Set( gaussianIterator.Value() );
				++outputIterator;
				++gaussianIterator;
			}
		}
	}
}


// after threaded generate data
template <class TImageType, class TOutputImage>
void SliceBySliceAttenuateIntensityImageFilter<TImageType, TOutputImage>::AfterThreadedGenerateData()
{
    // maybe peform post processing here?
}

} // end namespace itk
