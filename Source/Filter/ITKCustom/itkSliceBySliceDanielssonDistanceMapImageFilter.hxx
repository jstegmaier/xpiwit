/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// include required headers
#include "itkSliceBySliceDanielssonDistanceMapImageFilter.h"
#include "itkImageRegionIterator.h"
#include "itkImageRegionConstIterator.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include <iostream>
#include <fstream>
#include <string>
#include <limits>
#include <QString>
#include <QStringList>
#include <QDateTime>
#include "../../Core/Utilities/Logger.h"

// namespace itk
namespace itk
{

// the default constructor
template <class TImageType, class TOutputImage>
SliceBySliceDanielssonDistanceMapImageFilter<TImageType, TOutputImage>::SliceBySliceDanielssonDistanceMapImageFilter()
{
	m_UseImageSpacing = 1;
	m_SquaredDistance = 0;
	m_InputIsBinary = 1;
	this->DynamicMultiThreadingOff();
}


// the destructor
template <class TImageType, class TOutputImage>
SliceBySliceDanielssonDistanceMapImageFilter<TImageType, TOutputImage>::~SliceBySliceDanielssonDistanceMapImageFilter()
{
}


// before threaded generate data
template <class TImageType, class TOutputImage>
void SliceBySliceDanielssonDistanceMapImageFilter<TImageType, TOutputImage>::BeforeThreadedGenerateData()
{
    // Allocate output
    typename TOutputImage::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();
}


// the thread generate data
template <class TImageType, class TOutputImage>
void SliceBySliceDanielssonDistanceMapImageFilter<TImageType, TOutputImage>::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId)
{
    // get input and output pointers
    typename TOutputImage::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();
	
	// initialize the regions to process
	typename TImageType::RegionType currentRegion;
	typename TImageType::IndexType currentIndex;
	typename TImageType::SizeType currentSize;

	for (int j=0; j<TImageType::ImageDimension; ++j)
	{
		currentIndex[j] = outputRegionForThread.GetIndex(j);
		currentSize[j] = outputRegionForThread.GetSize(j);
	}
	currentSize[2] = 1;
	currentRegion.SetIndex( currentIndex );
	currentRegion.SetSize( currentSize );
	
	// create a temporary image used to extract slices
	typename TImageType::Pointer tmpImage = TImageType::New();
	tmpImage->SetRegions(currentRegion);
	tmpImage->SetSpacing( input->GetSpacing() );
	tmpImage->Allocate();
	tmpImage->FillBuffer(0);

	// iterate over all slices and perform watershed separately for each slice
	for (int i=0; i<outputRegionForThread.GetSize(2); ++i)
	{
		// set the current index
		currentIndex[2] = outputRegionForThread.GetIndex(2)+i;
		currentRegion.SetIndex( currentIndex );
		currentRegion.SetSize( currentSize );
		
		// extract the slice to perform the distance map calculation on
		ImageRegionConstIterator<TImageType> inputIterator( input, currentRegion );
		ImageRegionIterator<TImageType> sliceIterator( tmpImage, tmpImage->GetLargestPossibleRegion() );
		inputIterator.GoToBegin();
		sliceIterator.GoToBegin();

		while(inputIterator.IsAtEnd() == false)
		{
			// copy the input value
			sliceIterator.Set( inputIterator.Value() );

			// increment the iterators
			++inputIterator;
			++sliceIterator;
		}

		// setup the distance map filter
		typedef itk::DanielssonDistanceMapImageFilter<TImageType, TImageType> DistanceMapFilterType;
		typename DistanceMapFilterType::Pointer distanceMapFilter = DistanceMapFilterType::New();
		distanceMapFilter->SetInput( tmpImage );
		distanceMapFilter->SetReleaseDataFlag( true );
		distanceMapFilter->SetSquaredDistance( m_SquaredDistance );
		distanceMapFilter->SetUseImageSpacing( m_UseImageSpacing );
		distanceMapFilter->SetInputIsBinary( m_InputIsBinary );
		distanceMapFilter->SetNumberOfWorkUnits( 1 );
		itkTryCatch( distanceMapFilter->Update(), "Exception Caught: Updating distance map calculation of the slice-based watershed filter." );

		// initialize the seed region iterator
		ImageRegionIterator<TOutputImage> distanceMapIterator( distanceMapFilter->GetOutput(), distanceMapFilter->GetOutput()->GetLargestPossibleRegion() );
		ImageRegionIterator<TOutputImage> outputIterator( output, currentRegion );

		distanceMapIterator.GoToBegin();
		outputIterator.GoToBegin();
		while (outputIterator.IsAtEnd() == false)
		{
			outputIterator.Set(distanceMapIterator.Value());

			++outputIterator;
			++distanceMapIterator;
		}
	}
}


// after threaded generate data
template <class TImageType, class TOutputImage>
void SliceBySliceDanielssonDistanceMapImageFilter<TImageType, TOutputImage>::AfterThreadedGenerateData()
{
    // maybe peform post processing here?
}

} // end namespace itk
