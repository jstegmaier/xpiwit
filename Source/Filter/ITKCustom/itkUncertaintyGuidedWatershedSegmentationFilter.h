/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef __XPIWIT_UNCERTAINTYGUIDEDWATERSHEDSEGMENTATIONFILTER_H
#define __XPIWIT_UNCERTAINTYGUIDEDWATERSHEDSEGMENTATIONFILTER_H

// include required headers
#include "../Base/Management/ITKDefinitions.h"
#include "itkExtractRegionPropsImageFilter.h"
#include "itkMacro.h"
#include <string>
#include "../Base/MetaData/MetaDataFilter.h"

using namespace XPIWIT;

namespace itk
{

/**
 * @class UncertaintyGuidedWatershedSegmentationFilter
 * Fast uncertainty based watershed for seed based splitting of connected objects.
 */
template <class TImageType>
class ITK_EXPORT UncertaintyGuidedWatershedSegmentationFilter : public ImageToImageFilter<TImageType, TImageType>
{
    public:
        // Extract dimension from input and output image.
        itkStaticConstMacro(ImageDimension, unsigned int, TImageType::ImageDimension);
        typedef UncertaintyGuidedWatershedSegmentationFilter Self;
        typedef ImageToImageFilter<TImageType,TImageType> Superclass;
        typedef SmartPointer<Self> Pointer;
        typedef SmartPointer<const Self> ConstPointer;
        itkNewMacro(Self);

        // definition of variable type abbreviations
        typedef typename TImageType::PixelType PixelType;
        typedef SeedPoint<TImageType> SeedPointType;
        typedef RegionProps RegionPropsType;
        typedef typename TImageType::RegionType InputImageRegionType;
        typedef typename TImageType::RegionType OutputImageRegionType;

        /**
         * The default constructor.
         */
        UncertaintyGuidedWatershedSegmentationFilter();

        /**
         * The destructor.
         */
        virtual ~UncertaintyGuidedWatershedSegmentationFilter();

        /**
         * Set and get methods for the member variables.
         */
        itkGetMacro( FullyConnected, bool);
        itkSetMacro( FullyConnected, bool);
        itkSetMacro( UncertaintyCombinationFunction, int );
		itkGetMacro( UncertaintyCombinationFunction, int );
        itkSetMacro( UncertaintyThreshold, float );
        itkGetMacro( UncertaintyThreshold, float );

        void SetRegionProps(MetaDataFilter* metaFilter) { m_RegionProps = metaFilter; }
		void SetFuzzySetParameters(MetaDataFilter* metaFilter) { m_FuzzySetParameters = metaFilter; }
		void SetSeedImage(const TImageType* image)               { m_SeedImage = image; }
		void SetLabelImage(const TImageType* image)               { m_LabelImage = image; }

    protected:

        /**
         * Functions for data generation.
         * @param outputRegionForThread the region to be processed.
         * @param threadId the id of the current thread.
         */
        void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId) override;

        /**
         * functions for prepareing and post-processing the threaded generation.
         */
        void BeforeThreadedGenerateData() override;
        void AfterThreadedGenerateData() override;

        float m_UncertaintyThreshold;						// minimum standard deviation of the intensity values of an extracted region
		bool m_FullyConnected;
		int m_UncertaintyCombinationFunction;

		MetaDataFilter* m_RegionProps;					// region props input
        MetaDataFilter* m_FuzzySetParameters;              // key points input meta data

		QList< QList< QList<float> > > m_RegionPropsPerThread;

		const TImageType* m_SeedImage;
		const TImageType* m_LabelImage;
};

} // namespace itk

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkUncertaintyGuidedWatershedSegmentationFilter.hxx"
#endif

#endif
