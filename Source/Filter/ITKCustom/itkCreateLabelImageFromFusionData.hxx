/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// include required headers
#include "itkCreateLabelImageFromFusionData.h"
#include "itkConstNeighborhoodIterator.h"
#include "itkConstantBoundaryCondition.h"
#include "itkImageRegionIterator.h"
#include "itkImageRegionConstIterator.h"
#include "itkIntensityWindowingImageFilter.h"
#include <iostream>
#include <fstream>
#include <math.h>
#include <string>
#include <QString>
#include <QStringList>
#include <QDateTime>
#include "../../Core/Utilities/Logger.h"

// namespace itk
namespace itk
{

// the default constructor
template <class TImageType, class TOutputImage>
CreateLabelImageFromFusionData<TImageType, TOutputImage>::CreateLabelImageFromFusionData()
{
	m_UseRandomLabels = 0;
	m_MaxSize = -1;
	m_MinSlices = 0;
	m_MinimumLabel = 0;
	m_LabelFeature = 14; // default to seed label
	m_ColorFeature = 14;
	this->DynamicMultiThreadingOff();
}


// the destructor
template <class TImageType, class TOutputImage>
CreateLabelImageFromFusionData<TImageType, TOutputImage>::~CreateLabelImageFromFusionData()
{
}


// before threaded generate data
template <class TImageType, class TOutputImage>
void CreateLabelImageFromFusionData<TImageType, TOutputImage>::BeforeThreadedGenerateData()
{
    // Allocate output
    typename TOutputImage::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();

	// compute the minimum value
	typename itk::MinimumMaximumImageCalculator<TImageType>::Pointer minMaxCalc = itk::MinimumMaximumImageCalculator<TImageType>::New();
    minMaxCalc->SetImage( input );
    minMaxCalc->Compute();
	m_MinimumLabel = minMaxCalc->GetMinimum();
	
	// create random labels if the flag is set. Otherwise the unique fusion labels are used
	if (m_UseRandomLabels == true)
	{
		// determine the maximum label from the provided fusion data
		unsigned int maxLabel = 0;

		for (int i=0; i<(*m_InputMetaFilter).length(); ++i)
			for (int j=0; j<(*m_InputMetaFilter)[i].length(); ++j)
				if ((*m_InputMetaFilter)[i][j].at(m_LabelFeature) > maxLabel)
					maxLabel = (unsigned int)(*m_InputMetaFilter)[i][j].at(m_LabelFeature);

		for (int i=0; i<=maxLabel; ++i)
			m_RandomLabels.append(65535.0 * ((float(rand() % 100)/100.0)*0.6 + 0.4));

		m_RandomLabels[0] = 0.0;
	}

	// initialize the output buffer to zero
	output->FillBuffer( 0.0 );
}


// the thread generate data
template <class TImageType, class TOutputImage>
void CreateLabelImageFromFusionData<TImageType, TOutputImage>::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId)
{
    // get input and output pointers
    typename TOutputImage::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();

	// get the feature indices from the meta data object
	const unsigned int sliceIndex = 4; //(*m_InputMetaFilter)->GetFeatureIndex( "zpos" );
	const unsigned int areaIndex = 1; //(*m_InputMetaFilter)->GetFeatureIndex( "volume" );

	// initialize the iterators
	ImageRegionConstIterator< TImageType > inputIterator( input, outputRegionForThread );
	ImageRegionIterator< TOutputImage > outputIterator( output, outputRegionForThread );
	inputIterator.GoToBegin();
	outputIterator.GoToBegin();

	// setup the intensity multiplier if float image is provided
	float intensityMultiplier = 1.0;
	if( typeid( typename TImageType::PixelType ) == typeid( float ) ||
		typeid( typename TImageType::PixelType ) == typeid( double ) ) {
		intensityMultiplier = 1;
	}

	// fill the output image with the assigned labels
	typename TImageType::IndexType currentIndex;
	unsigned int currentSegmentId = 0;
	while (inputIterator.IsAtEnd() == false)
	{
		currentIndex = inputIterator.GetIndex();
		currentSegmentId = (unsigned int)(floor(0.5+(inputIterator.Value() * intensityMultiplier)) - (*m_InputMetaFilter)[currentIndex[2]][0][0]);

		/*
		if (currentSegmentId > 370 && currentSegmentId < 380)
		{
			Logger::GetInstance()->WriteLine( QString("- Intensity value  ") + QString().number(inputIterator.Value()) + QString(", Offset ") + QString().number((*m_InputMetaFilter)[currentIndex[2]][0][0]) + QString(", resultValue ") + QString().number(currentSegmentId) + QString(", labelValue ") + QString().number((*m_InputMetaFilter)[currentIndex[2]][currentSegmentId][m_ColorFeature]) );
		}*/

		if (currentSegmentId >= (*m_InputMetaFilter)[currentIndex[2]].size() || currentSegmentId == 0)
		{
			//Logger::GetInstance()->WriteLine( QString("- Skipping element with label  ") + QString().number(currentSegmentId) + QString(", MinLabel ") + QString().number((*m_InputMetaFilter)[currentIndex[2]][0][0]) + QString(", inputValue ") + QString().number(inputIterator.Value()) );
			++inputIterator;
			++outputIterator;
			continue;
		}

		/*
		if ((*m_InputMetaFilter)[currentIndex[2]][currentSegmentId][m_ColorFeature] == 247)
		{
		   Logger::GetInstance()->WriteLine( "Current Segment: " + QString::number((*m_InputMetaFilter)[currentIndex[2]][currentSegmentId][m_ColorFeature]) );
		   Logger::GetInstance()->WriteLine( QString("Segment ") + QString::number((*m_InputMetaFilter)[currentIndex[2]][currentSegmentId][m_ColorFeature]) + QString(" has ") + QString::number((*m_LabelHashMap)[(*m_InputMetaFilter)[currentIndex[2]][currentSegmentId][m_ColorFeature]].size()) + QString(" slices --> remove?") );
		}
		*/

		if (((*m_InputMetaFilter)[currentIndex[2]][currentSegmentId][areaIndex] < m_MaxSize || m_MaxSize < 0) && 
			(*m_InputMetaFilter)[currentIndex[2]][currentSegmentId][m_LabelFeature] > 0 &&
			(*m_LabelHashMap)[(*m_InputMetaFilter)[currentIndex[2]][currentSegmentId][m_LabelFeature]].size() > m_MinSlices)
		{
			if (m_UseRandomLabels == true)
				outputIterator.Set( ((float)m_RandomLabels[(*m_InputMetaFilter)[currentIndex[2]][currentSegmentId][m_LabelFeature]]) / (float)intensityMultiplier );
			else
				outputIterator.Set( (*m_InputMetaFilter)[currentIndex[2]][currentSegmentId][m_ColorFeature] );
				//outputIterator.Set( (float((*m_InputMetaFilter)[currentIndex[2]][currentSegmentId][m_ColorFeature])) / intensityMultiplier ); // REMOVED "+1" for DEBUGGING of ANISOTROPY
			//
		}

		// increment the iterators
		++inputIterator;
		++outputIterator;
	}
}


// after threaded generate data
template <class TImageType, class TOutputImage>
void CreateLabelImageFromFusionData<TImageType, TOutputImage>::AfterThreadedGenerateData()
{
    // maybe peform post processing here?
}

} // end namespace itk
