/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef WATERSHEDBOUNDARIESMERGETREEUTILITIES_H
#define WATERSHEDBOUNDARIESMERGETREEUTILITIES_H

// include required headers
#include "../Base/Management/ITKDefinitions.h"
#include "itkBoxImageFilter.h"
#include "itkImage.h"
#include "itkNumericTraits.h"
#include "itkNeighborhoodIterator.h"
#include "itkConstNeighborhoodIterator.h"
#include "itkZeroFluxNeumannBoundaryCondition.h"
#include "itkNormalizeImageFilter.h"
#include "itkResampleImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkGrayscaleDilateImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkExtractImageFilter.h"
#include "itkMultiplyImageFilter.h"
#include "itkFlatStructuringElement.h"
#include "itkImageFileWriter.h"
#include "itkHistogram.h"
//#include "vnl_symmetric_eigensystem.h"
#include <vector>
#include <float.h>
#define _USE_MATH_DEFINES // required to enable math defines in visual studio
#include <math.h>
#include <QString>
#include <QHash>
#include <QFile>
#include <QDir>
#include <list>
#include <QCoreApplication>
#include <fstream>

// include qhull headers
#include "../ThirdParty/qhull/libqhullcpp/Qhull.h"
#include "../ThirdParty/qhull/libqhullcpp/RboxPoints.h"
#include "../ThirdParty/qhull/libqhullcpp/QhullFacetList.h"

#include "../Base/MetaData/MetaDataFilter.h"

#define XPIWIT_HISTOGRAMSIZE 10

using namespace XPIWIT;

namespace itk
{
    
    // forward declarations for SuperVoxel
    template<class TInputImage>
    class SuperVoxelProperties;
    
    /**
     * Class for representing the watershed boundary between two basins.
     */
    template<class TInputImage>
    class BasinEdge
    {
    public:
        
        // type defs for the templated index and pixel types
        typedef typename TInputImage::IndexType IndexType;
        typedef typename TInputImage::PixelType PixelType;
        
        /**
         * constructor to initialize a basin edge between two segments
         * @param The segment id of the first super voxel.
         * @param The segment id of the second super voxel.
         */
        BasinEdge(unsigned int segmentId1, unsigned int segmentId2, SuperVoxelProperties<TInputImage>* settings)
        {
            mSegmentId1 = segmentId1;
            mSegmentId2 = segmentId2;
            mMeanIntensityBorder = 0.0;
            mCentroid = vnl_vector<float>(3);
            mCentroid.fill(0);
            mNormal = vnl_vector<float>(3);
            mNormal.fill(0);
            mVolumeBorder = 0;
            mIsDirty = true;
            mSettings = settings;
            mSortFeature = 1;
        }
        
        /**
         * Return the sort feature.
         * @return the sort feature.
         */
        void SetSortFeature(const float sortFeature) { mSortFeature = sortFeature; }
        
        /**
         * Return the sort feature.
         * @return the sort feature.
         */
        float GetSortFeature() { return mSortFeature; }
        
        /**
         * Return the segment ids of the two separated super voxels.
         * @return the segment id of the first super voxel.
         */
        unsigned int GetSegmentId1() { return mSegmentId1; }
        
        /**
         * Return the segment ids of the two separated super voxels.
         * @return the segment id of the second super voxel.
         */
        unsigned int GetSegmentId2() { return mSegmentId2; }
        
        /**
         * Return the volume (number of voxels) of the current separator line.
         * @return The volume of the current separator line, i.e., the number of voxels.
         */
        unsigned int GetVolumeBorder()
        {
            if (mIsDirty == true)
                UpdateBasinEdgeFeatures();
            return mVolumeBorder;
        }
        
        /**
         * Return the mean intensity on the border between the two super voxels.
         * @return The mean intensity on the border between two super voxels.
         */
        PixelType GetMeanIntensityBorder()
        {
            if (mIsDirty == true)
                UpdateBasinEdgeFeatures();
            return mMeanIntensityBorder;
        }
        
        /**
         * Returns the centroid of the boundary.
         * @return The centroid of the current boundary.
         */
        vnl_vector<float> GetCentroid()
        {
            if (mIsDirty == true)
                UpdateBasinEdgeFeatures();
            return mCentroid;
        }
        
        /**
         * Returns the surface normal estimated as the normal of a least squares plane through all points.
         * @return The centroid of the current boundary.
         */
        vnl_vector<float> GetNormal()
        {
            if (mIsDirty == true)
                UpdateBasinEdgeFeatures();
            return mNormal;
        }
        
        /**
         * Function to update the properties of the separator line.
         */
        void UpdateBasinEdgeFeatures()
        {
            // get the number of pixels on the boundary.
            mVolumeBorder = mBorderIndices.size();
            
            if (mVolumeBorder == 0)
                std::cout << "DIVISION BY ZERO DETECTED!!!" << std::endl;
            
            // iterate over all boundary pixels and extract the mean intensity.
            mMeanIntensityBorder = 0.0;
            typename std::list<typename TInputImage::PixelType>::iterator intensityIterator;
            for (intensityIterator = mBorderIntensities.begin(); intensityIterator != mBorderIntensities.end(); ++intensityIterator)
                mMeanIntensityBorder += (*intensityIterator);
            mMeanIntensityBorder /= (typename TInputImage::PixelType)(mVolumeBorder);
            
            // iterate over all border indices to compute the centroid of the boundary.
            mCentroid.fill(0);
            typename std::list<typename TInputImage::IndexType>::iterator indexIterator;
            for (indexIterator = mBorderIndices.begin(); indexIterator != mBorderIndices.end(); ++indexIterator)
            {
                mCentroid[0] += (*indexIterator)[0];
                mCentroid[1] += (*indexIterator)[1];
                if (TInputImage::ImageDimension > 2)
                    mCentroid[2] += (*indexIterator)[2];
            }
            mCentroid /= (typename TInputImage::PixelType)(mVolumeBorder);
            
            // iterate over all border indices to compute the centroid of the boundary.
            itk::Matrix<float> covarianceMatrix;
            covarianceMatrix.Fill(0);
            
            for (indexIterator = mBorderIndices.begin(); indexIterator != mBorderIndices.end(); ++indexIterator)
            {
                for (int i = 0; i < TInputImage::ImageDimension; ++i)
                {
                    for (int j = 0; j<TInputImage::ImageDimension; ++j)
                    {
                        covarianceMatrix[i][j] = ((float)(*indexIterator)[i] - mCentroid[i]) * ((float)(*indexIterator)[j] - mCentroid[j]);
                    }
                }
            }
            
            itk::Vector<float> eigenValues;
            eigenValues.Fill(0);
            
            itk::Matrix<float> eigenVectors;
            eigenVectors.Fill(0);
            
            //typedef itk::SymmetricEigenAnalysis<itk::Matrix<float>, itk::Vector<float> > EigenType;
            typename itk::SymmetricEigenAnalysis<itk::Matrix<float>, itk::Vector<float> > eigenAnalysis;
            eigenAnalysis.SetDimension(3);
            eigenAnalysis.SetOrderEigenMagnitudes(true);
            eigenAnalysis.ComputeEigenValuesAndVectors(covarianceMatrix, eigenValues, eigenVectors);
            
            for (int i = 0; i < TInputImage::ImageDimension; ++i)
                mNormal[i] = eigenVectors[0][i];
            mNormal.normalize();
            
            // compute the structure tensor
            mStructureTensor = GetStructureTensor();
            
            // set the dirty flag to false after all features have been updated.
            mIsDirty = false;
        }
        
        
        /**
         * Compute the structure tensor of the basin edge
         * @return the structure tensor.
         */
        itk::Matrix<float> GetStructureTensor()
        {
            // simply return the precomputed strucutre tensor if nothing changed
            if (mIsDirty == false)
                return mStructureTensor;
            
            // iterate over all border indices to compute the centroid of the boundary.
            mStructureTensor.Fill(0);
            typename std::list<typename TInputImage::IndexType>::iterator indexIterator;
            for (indexIterator = mBorderIndices.begin(); indexIterator != mBorderIndices.end(); ++indexIterator)
            {
                for (int i = 0; i < 3; ++i)
                {
                    for (int j = 0; j < 3; ++j)
                    {
                        mStructureTensor[i][j] += mSettings->mMaskGradientImage->GetPixel(*indexIterator)[i] * mSettings->mMaskGradientImage->GetPixel(*indexIterator)[j];
                    }
                }
            }
            
            // return the new structure tensor
            return mStructureTensor;
        }
        
        
        /**
         * Add a voxel to the boundary.
         * @param borderIndex The index of the border voxel that should be added.
         * @param borderIntensity The intensity of the current border voxel.
         */
        void AddVoxel(const IndexType& borderIndex, const PixelType& borderIntensity)
        {
            mIsDirty = true;
            mBorderIndices.push_back(borderIndex);
            mBorderIntensities.push_back(borderIntensity);
        };
        
        /**
         * Returns a reference to the border indices.
         * @return Reference to the border indices.
         */
        std::list<typename TInputImage::IndexType>* GetBorderIndices()
        {
            return &mBorderIndices;
        }
        
        /**
         * Paint basin edge with the provided label.
         * @param outputImage Pointer to the output image.
         * @param label Label of the parent super voxel.
         */
        void PaintBasinEdge(TInputImage* outputImage, typename TInputImage::PixelType label, bool useIntensityImage = false)
        {
            if (mIsDirty == true)
                UpdateBasinEdgeFeatures();
            
            if (mBorderIndices.size() != mBorderIntensities.size())
                std::cout << "Border between " << mSegmentId1 << ", " << mSegmentId2 << ", Num border indices: " << mBorderIndices.size() << ", Num intensities: " << mBorderIntensities.size() << std::endl;
            
            typename std::list<typename TInputImage::IndexType>::iterator voxelIterator;
            //typename std::list<typename TInputImage::PixelType>::iterator intensityIterator = mBorderIntensities.begin();
            for (voxelIterator=mBorderIndices.begin(); voxelIterator!=mBorderIndices.end(); ++voxelIterator)
            {
                if (useIntensityImage == false)
                    outputImage->SetPixel(*voxelIterator, label);
                else
                    outputImage->SetPixel(*voxelIterator, mSettings->mIntensityImage->GetPixel(*voxelIterator));
                
                //++intensityIterator;
            }
        }
        
    private:
        float mSortFeature;                                                // sort feature only used for plotting
        unsigned int mSegmentId1;                                        // segment id of the first segment
        unsigned int mSegmentId2;                                        // segment id of the second segment
        unsigned int mVolumeBorder;                                        // volume (number of voxels) of the border
        bool mIsDirty;                                                    // flag that indicates if the border properties need to be updated
        typename TInputImage::PixelType mMeanIntensityBorder;            // the mean intensity of the border voxels
        SuperVoxelProperties<TInputImage>* mSettings;                    // pointer to the settings variable
        
        vnl_vector<float> mCentroid;                                    // centroid of the border
        vnl_vector<float> mNormal;                                        // approximated normal resulting from a least squares plane fit of the point cloud
        itk::Matrix<float> mStructureTensor;
        std::list<typename TInputImage::IndexType> mBorderIndices;    // list containing all border indices
        std::list<typename TInputImage::PixelType> mBorderIntensities;    // list containing all border intensities
    };
    
    
    // forward declarations for SuperVoxel
    template<class TInputImage>
    class SuperVoxel;
    
    // forward declarations for SuperVoxelEdge
    template<class TInputImage>
    class SuperVoxelEdge;
    
    
    /**
     * Class for sharing super voxel information among different objects.
     */
    template<class TInputImage>
    class SuperVoxelProperties
    {
    public:
        typedef typename itk::CovariantVector<float, TInputImage::ImageDimension> VectorType;
        typedef typename itk::Image<VectorType, TInputImage::ImageDimension> VectorImageType;
        
        SuperVoxelProperties()
        {
            for (int i = 0; i < XPIWIT_HISTOGRAMSIZE; ++i)
                mReferenceShapeHistogram[i] = 0.0;
            mReferenceShapeHistogram[XPIWIT_HISTOGRAMSIZE - 1] = 1.0;
            
            // initialize temp folder for the CNN intermediate results
            std::cout << "- Initializing temp folder for CNN results at: " << QDir::tempPath().toStdString().data() << "/RACE3DTemp/" << std::endl;
            QDir tempFolder = QDir::temp();
            QDir tempFolderRACE3D(tempFolder.absolutePath() + QString("/RACE3DTemp/"));
            QDir tempFolderImages(tempFolder.absolutePath() + QString("/RACE3DTemp/Images/"));
            QDir tempFolderProbabilities(tempFolder.absolutePath() + QString("/RACE3DTemp/Probabilities/"));
            
            // check if temp directory already exists and remove all previous files if yes
            if (tempFolderRACE3D.exists())
            {
                std::cout << "- Directory already existed -> removing previous results ... ";
                if (tempFolderImages.exists())
                {
                    tempFolderImages.setNameFilters(QStringList() << "*.*");
                    tempFolderImages.setFilter(QDir::Files);
                    foreach(QString dirFile, tempFolderImages.entryList())
                    tempFolderImages.remove(dirFile);
                }
                else
                {
                    tempFolderRACE3D.mkdir("Images");
                }
                
                if (tempFolderProbabilities.exists())
                {
                    tempFolderProbabilities.setNameFilters(QStringList() << "*.*");
                    tempFolderProbabilities.setFilter(QDir::Files);
                    foreach(QString dirFile, tempFolderProbabilities.entryList())
                    tempFolderProbabilities.remove(dirFile);
                }
                else
                {
                    tempFolderRACE3D.mkdir("Probabilities");
                }
                
                std::cout << "done" << std::endl;
            }
            else
            {
                tempFolder.mkdir("RACE3DTemp");
                tempFolder.mkdir("RACE3DTemp/Images/");
                tempFolder.mkdir("RACE3DTemp/Probabilities/");
            }
            
            // set the temp folder
            mTempFolder = tempFolder.absolutePath() + "/RACE3DTemp/";
            mModelFolder = QFileInfo(QCoreApplication::applicationFilePath()).absolutePath() + "/models/";
            if (!QDir(mModelFolder).exists("RACE3DSimpleSoftmaxInference.py"))
            {
                mCNNEnabled = false;
                std::cout << "ERROR: CNN processing module not found in: " << mModelFolder.toStdString().data() << std::endl;
            }
        }
        
        /**
         * Function to convert two segment ids to a single array index.
         * @param index1 The id of the first segment.
         * @param index2 The id of the second segment.
         * @return The single array index used as a hash key for two segment ids.
         */
        unsigned int ConvertSegmentIdsToArrayIndex(const unsigned int index1, const unsigned int index2) { return (std::min<unsigned int>(index1, index2)*(mMaximumLabel+1) + std::max<unsigned int>(index1, index2)); };
        
        /**
         * Function to convert a single array index to the two segment ids referring to it.
         * @param The array index that should be split to two segment ids.
         * @return The id of the first segment.
         */
        unsigned int ConvertArrayIndexToIndex1(const unsigned int arrayIndex) { return (unsigned int)std::floor(arrayIndex / (mMaximumLabel + 1)); };
        
        /**
         * Function to convert a single array index to the two segment ids referring to it.
         * @param The array index that should be split to two segment ids.
         * @return The id of the second segment.
         */
        unsigned int ConvertArrayIndexToIndex2(const unsigned int arrayIndex) { return arrayIndex % (mMaximumLabel + 1); };
        
        /**
         * Fuzzy membership function for a trapezoidal shape.
         * @param x The input value that should be assigned with a membership.
         * @param a The left minimum value (x values less than a will be 0).
         * @param b The left maximum value (x values between a and b are linearly interpolated between 0 and 1, values larger than b are 1).
         * @param c The right maximum value (x values between b and c are 1, values between c and d are linearly interpolated between 1 and 0).
         * @param d The right minimum value (x values between c and d are linearly interpolated between 1 and 0, values larger than d are 0).
         */
        float TrapMF(const float x, const float a, const float b, const float c, const float d)
        {
            return std::max<float>(std::min<float>(1.0, std::min<float>(((x-a)/(b-a+FLT_EPSILON)), ((d-x)/(d-c+FLT_EPSILON)))), 0);
        }
        
        bool mDebugOutput;                                                                // if true, debug output will be printed to the log file
        unsigned int mMaximumLabel;                                                        // the maximum segment id of the super voxel presegmentation
        unsigned int mIntensityFeatureIndex;                                            // the column index of the mean intensity feature
        unsigned int mVolumeFeatureIndex;                                                // the column index of the volume feature index
        unsigned int mXPosFeatureIndex;                                                    // the column index of the x position feature index
        unsigned int mYPosFeatureIndex;                                                    // the column index of the y position feature index
        unsigned int mZPosFeatureIndex;                                                    // the column index of the z position feature index
        unsigned int mMinimumVolume;                                                    // minimum volume of segments
        unsigned int mMaximumVolume;                                                    // maximum volume of segments
        float mMaximumAngle;                                                            // maximum angle for the structure tensor eigenvector vs. centroid distance vector
        bool mGenerateEdgeMap;                                                            // if enabled, an edge map of the merge feature is generated instead of the actual segmentation
        double mReferenceShapeHistogram[XPIWIT_HISTOGRAMSIZE];
        
        bool mUseMinimumVolumeCriterion;                                                // If enabled, segments smaller than the minimum volume will be merged to their most likely neighbor segment
        bool mUseSphericityCriterion;                                                    // If enabled, segments that increase the sphericity of a segment pair will be fused
        bool mUseMeanRatioCriterion;                                                    // If enabled, segments that are separated by a boundary whose intensity is smaller than the average intensity of the segments' Internal are fused
        bool mUseBoundaryCriterion;                                                        // If enabled, segments that are separated by a boundary that is more similar to the Internal of the regions are fused
        bool mUseProbabilityMapCriterion;                                                // If enabled, segments with a probability lower than 0.5 will be merged (encoded in the mean intensity of the provided intensity image).
        bool mUseStructureTensorCriterion;                                                // If enabled, the angle between the smallest eigenvector of the structure tensor and the centroid distance vector will be used for fusion
        bool mDisableMinimumVolumeCriterionOnBorder;                                    // If enabled, the minimum volume criterion is disabled for objects that touch the border
        bool mScaleByIntensity;                                                            // If enabled, valid merge candidates edges will additionally be scaled by the border average intensity
        
        XPIWIT::MetaDataFilter* mRegionProps;                                            // pointer to the region props, a structure containing all properties of the initial over segmentation
        TInputImage* mIntensityImage;                                                    // pointer to the intensity image
        VectorImageType* mMaskGradientImage;
        QList<unsigned int>** mNeighbors;                                                // neighbor relations of all initial super voxels
        QList<bool>* mIsBorderSegment;
        QList<bool>* mIsSurfaceSegment;
        QHash<unsigned int, SuperVoxel<TInputImage>* >* mSuperVoxels;                    // hash table containing all super voxels (initial super voxels and newly generated combinations of initial super voxels)
        QHash<unsigned int, SuperVoxelEdge<TInputImage>* >* mSuperVoxelEdges;            // super voxel edges that connect either initial or combined super voxels (may change during merge iterations)
        QHash<unsigned int, BasinEdge<TInputImage>* >* mBasinEdges;                        // initial basin edges between initial super voxels (not changed during merge iterations)
        
        bool mCNNEnabled;
        QString mTempFolder;
        QString mModelFolder;
    };
    
    
    /**
     * Class for representing a super voxel, i.e., either an initial super voxel
     * obtained by the watershed or a combination of multiple initial super voxels.
     */
    template<class TInputImage>
    class SuperVoxel
    {
    public:
        
        // typedefs for the templated index and pixel types
        typedef typename TInputImage::IndexType IndexType;
        typedef typename TInputImage::PixelType PixelType;
        
        /**
         * The constructor to create a super voxel based on an initial super voxel
         * @param id The id of the super voxel (basin if of the watershed oversegmentation)
         * @param settings Pointer to the settings structure that is shared among all objects
         */
        SuperVoxel(const unsigned int id, SuperVoxelProperties<TInputImage>* settings)
        {
            mInternalSuperVoxels.append(id);
            mVolumeInternal = settings->mRegionProps->mData[id][settings->mVolumeFeatureIndex];
            mCentroid = vnl_vector<float>(3);
            mCentroid.fill(0);
            mCentroid[0] = settings->mRegionProps->mData[id][settings->mXPosFeatureIndex];
            mCentroid[1] = settings->mRegionProps->mData[id][settings->mYPosFeatureIndex];
            if (TInputImage::ImageDimension > 2)
                mCentroid[2] = settings->mRegionProps->mData[id][settings->mZPosFeatureIndex];
            mMinimumLocation = vnl_vector<float>(3);
            mMinimumLocation.fill(FLT_MAX);
            mMaximumLocation = vnl_vector<float>(3);
            mMaximumLocation.fill(0);
            mNormal = vnl_vector<float>(3);
            mNormal.fill(0);
            mMeanIntensityInternal = settings->mRegionProps->mData[id][settings->mIntensityFeatureIndex];
            mMeanIntensityBorder = 0.0;
            mParent = NULL;
            mLeftSuperVoxel = NULL;
            mRightSuperVoxel = NULL;
            mSettings = settings;
            mIsBorderSegment = (*mSettings->mIsBorderSegment)[id];
            mIsSurfaceSegment = (*mSettings->mIsSurfaceSegment)[id];
            mId = id;
            mSelectionState = 0;
            mIsDirty = true;
            mIsDirtyConvexHull = true;
            mSuperVoxelImage = NULL;
        }
        
        /**
         * Constructor to initialize the super voxel based on pre existing super voxels.
         * @param leftSuperVoxel pointer to the left super voxel
         * @param rightSuperVoxel pointer to the right super voxel
         * @param settings Pointer to the settings structure that is shared among all objects
         */
        SuperVoxel(const unsigned int id, SuperVoxel<TInputImage>* leftSuperVoxel, SuperVoxel<TInputImage>* rightSuperVoxel, SuperVoxelProperties<TInputImage>* settings, bool setParent = true)
        {
            // set the pointers to the left and right super voxels
            mId = id;
            mLeftSuperVoxel = leftSuperVoxel;
            mRightSuperVoxel = rightSuperVoxel;
            mSettings = settings;
            mCentroid = vnl_vector<float>(3);
            mCentroid.fill(0);
            mMinimumLocation = vnl_vector<float>(3);
            mMinimumLocation.fill(FLT_MAX);
            mMaximumLocation = vnl_vector<float>(3);
            mMaximumLocation.fill(0);
            mNormal = vnl_vector<float>(3);
            mNormal.fill(0);
            mInternalSuperVoxels.clear();
            mMeanIntensityBorder = 0.0;
            mIsBorderSegment = false;
            mIsSurfaceSegment = false;
            mSelectionState = 0;
            mSuperVoxelImage = NULL;
            
            // iterate over all internal super voxels of the left super voxel
            QList<unsigned int>* InternalSuperVoxels1 = mLeftSuperVoxel->GetInternalSuperVoxels();
            for (int i = 0; i < InternalSuperVoxels1->size(); ++i)
            {
                if (!mInternalSuperVoxels.contains((*InternalSuperVoxels1)[i]))
                    mInternalSuperVoxels.append((*InternalSuperVoxels1)[i]);
                
                if ((*mSettings->mIsBorderSegment)[(*InternalSuperVoxels1)[i]] == true)
                    mIsBorderSegment = true;
                
                if ((*mSettings->mIsSurfaceSegment)[(*InternalSuperVoxels1)[i]] == true)
                    mIsSurfaceSegment = true;
            }
            
            // iterate over all internal super voxels of the right super voxel
            QList<unsigned int>* InternalSuperVoxels2 = mRightSuperVoxel->GetInternalSuperVoxels();
            for (int i = 0; i < InternalSuperVoxels2->size(); ++i)
            {
                if (!mInternalSuperVoxels.contains((*InternalSuperVoxels2)[i]))
                    mInternalSuperVoxels.append((*InternalSuperVoxels2)[i]);
                
                if ((*mSettings->mIsBorderSegment)[(*InternalSuperVoxels2)[i]] == true)
                    mIsBorderSegment = true;
                
                if ((*mSettings->mIsSurfaceSegment)[(*InternalSuperVoxels2)[i]] == true)
                    mIsSurfaceSegment = true;
            }
            
            // set the parents to obtain the merge tree
            mParent = NULL;
            if (setParent == true)
            {
                mLeftSuperVoxel->SetParent(this);
                mRightSuperVoxel->SetParent(this);
            }
            
            // update the minimum and maximum location
            for (int i = 0; i < TInputImage::ImageDimension; ++i)
            {
                mMinimumLocation[i] = std::min<double>(leftSuperVoxel->GetMinimumLocation()[i], rightSuperVoxel->GetMinimumLocation()[i]);
                mMaximumLocation[i] = std::max<double>(leftSuperVoxel->GetMaximumLocation()[i], rightSuperVoxel->GetMaximumLocation()[i]);
            }
            
            // set the dirty flag to ensure the features are updated before accessed
            mIsDirty = true;
            mIsDirtyConvexHull = true;
        }
        
        
        /**
         * Return the Internal volume of the super voxel
         * @return The Internal volume of the super voxel
         */
        unsigned int GetVolumeInternal()
        {
            if (mIsDirty == true)
                UpdateProperties();
            return mVolumeInternal;
        }
        
        
        /**
         * Return the border volume of the super voxel
         * @return The border volume of the super voxel
         */
        unsigned int GetVolumeBorder()
        {
            if (mIsDirty == true)
                UpdateProperties();
            return mVolumeBorder;
        }
        
        vnl_vector<float> GetNormal()
        {
            mStructureTensor = GetStructureTensor();
            
            // initialize the eigen values and vectors
            itk::Vector<float, TInputImage::ImageDimension> eigenValues;
            eigenValues.Fill(0);
            
            itk::Matrix<float, TInputImage::ImageDimension, TInputImage::ImageDimension> eigenVectors;
            eigenVectors.Fill(0);
            
            // compute the eigenvectors
            //typedef itk::SymmetricEigenAnalysis<itk::Matrix<float, TInputImage::ImageDimension, TInputImage::ImageDimension>, itk::Vector<float, TInputImage::ImageDimension> > EigenType;
            typename itk::SymmetricEigenAnalysis<itk::Matrix<float, TInputImage::ImageDimension, TInputImage::ImageDimension>, itk::Vector<float, TInputImage::ImageDimension> > eigenAnalysis;
            eigenAnalysis.SetDimension(TInputImage::ImageDimension);
            eigenAnalysis.SetOrderEigenMagnitudes(true);
            eigenAnalysis.ComputeEigenValuesAndVectors(mStructureTensor, eigenValues, eigenVectors);
            
            for (int i=0; i < TInputImage::ImageDimension; ++i)
                mNormal[i] = eigenVectors[0][i];
            mNormal.normalize();
            
            return mNormal;
        }
        
        
        /**
         * Compute the normal based on the
         */
        itk::Matrix<float, TInputImage::ImageDimension, TInputImage::ImageDimension> GetStructureTensor()
        {
            //if (mIsDirty == false)
            //    return mStructureTensor;
            
            if (mLeftSuperVoxel == NULL && mRightSuperVoxel == NULL)
            {
                // iterate over all border indices to compute the centroid of the boundary.
                itk::Matrix<float, TInputImage::ImageDimension, TInputImage::ImageDimension> structureTensor;
                structureTensor.Fill(0);
                
                typename std::list<typename TInputImage::IndexType>::iterator voxelIterator;
                for (voxelIterator = mInternalIndices.begin(); voxelIterator != mInternalIndices.end(); ++voxelIterator)
                {
                    for (int i = 0; i < TInputImage::ImageDimension; ++i)
                    {
                        for (int j = 0; j < TInputImage::ImageDimension; ++j)
                        {
                            structureTensor[i][j] += mSettings->mMaskGradientImage->GetPixel(*voxelIterator)[i] * mSettings->mMaskGradientImage->GetPixel(*voxelIterator)[j];
                        }
                    }
                }
                
                mStructureTensor = structureTensor;
            }
            else
            {
                mStructureTensor = mLeftSuperVoxel->GetStructureTensor() + mRightSuperVoxel->GetStructureTensor();
            }
            
            return mStructureTensor;
        }
        
        
        /**
         * Return the border intensity of the super voxel
         * @return The border intensity of the super voxel
         */
        float GetMeanIntensityBorder()
        {
            if (mIsDirty == true)
                UpdateProperties();
            return mMeanIntensityBorder;
        }
        
        /**
         * Return the Internal intensity of the super voxel
         * @return The Internal intensity of the super voxel
         */
        float GetMeanIntensityInternal()
        {
            if (mIsDirty == true)
                UpdateProperties();
            return mMeanIntensityInternal;
        }
        
        // loop through all contained super voxels and calculate the combined features
        void UpdateProperties()
        {
            // initialize the super voxel properties to zero
            mVolumeInternal = 0;
            mVolumeBorder = 0;
            mMeanIntensityInternal = 0.0;
            mMeanIntensityBorder = 0.0;
            mCentroid = vnl_vector<float>(3);
            mCentroid.fill(0);
            mNeighborSuperVoxels.clear();
            mInternalBasinEdges.clear();
            mNeighborBasinEdges.clear();
            
            // iterate over all internal super voxels
            for (int i = 0; i < mInternalSuperVoxels.size(); ++i)
            {
                /*
                 // update the minimum and maximum location based on the internal supervoxels
                 if (mSettings->mSuperVoxels->contains(mInternalSuperVoxels[i]) == true)
                 {
                 vnl_vector<double> currentMinimumLocation = (*mSettings->mSuperVoxels)[mInternalSuperVoxels[i]]->GetMinimumLocation();
                 vnl_vector<double> currentMaximumLocation = (*mSettings->mSuperVoxels)[mInternalSuperVoxels[i]]->GetMaximumLocation();
                 
                 for (int j = 0; j<TInputImage::ImageDimension; ++j)
                 {
                 mMinimumLocation[j] = std::min<double>(mMinimumLocation[j], currentMinimumLocation[j]);
                 mMaximumLocation[j] = std::max<double>(mMaximumLocation[j], currentMaximumLocation[j]);
                 }
                 }
                 */
                
                // get the id of the current super voxel
                unsigned int currentId = mInternalSuperVoxels[i];
                mVolumeInternal += mSettings->mRegionProps->mData[currentId][mSettings->mVolumeFeatureIndex];
                mMeanIntensityInternal += mSettings->mRegionProps->mData[currentId][mSettings->mVolumeFeatureIndex] * mSettings->mRegionProps->mData[currentId][mSettings->mIntensityFeatureIndex];
                
                // compute the centroid of the current super voxel
                mCentroid[0] += mSettings->mRegionProps->mData[currentId][mSettings->mVolumeFeatureIndex] * mSettings->mRegionProps->mData[currentId][mSettings->mXPosFeatureIndex];
                mCentroid[1] += mSettings->mRegionProps->mData[currentId][mSettings->mVolumeFeatureIndex] * mSettings->mRegionProps->mData[currentId][mSettings->mYPosFeatureIndex];
                if (TInputImage::ImageDimension > 2)
                    mCentroid[2] += mSettings->mRegionProps->mData[currentId][mSettings->mVolumeFeatureIndex] * mSettings->mRegionProps->mData[currentId][mSettings->mZPosFeatureIndex];
                else
                    mCentroid[2] += 0.0f;
                
                // identify the number of neighbors of the current segment
                unsigned int numCurrentNeighbors = (*mSettings->mNeighbors)[currentId].size();
                for (unsigned int j = 0; j < numCurrentNeighbors; ++j)
                {
                    // get the current neighbor and identify the basin edge between the current super voxel and its neighbor
                    unsigned int currentNeighbor = (*mSettings->mNeighbors)[currentId][j];
                    unsigned int basinEdge = mSettings->ConvertSegmentIdsToArrayIndex(currentId, currentNeighbor);
                    
                    // don't continue if the basin edge is not contained
                    if (!(*mSettings->mBasinEdges).contains(basinEdge))
                        continue;
                    
                    // if the neighbor is part of the current parent super voxel, add the boundary between both parts to the entire volume
                    if (mInternalSuperVoxels.contains(currentNeighbor))
                    {
                        // to avoid duplicate addition, only add it once
                        if (mInternalSuperVoxels[i] < currentNeighbor)
                        {
                            mVolumeInternal += (*mSettings->mBasinEdges)[basinEdge]->GetVolumeBorder();
                            mMeanIntensityInternal += (*mSettings->mBasinEdges)[basinEdge]->GetVolumeBorder() * (*mSettings->mBasinEdges)[basinEdge]->GetMeanIntensityBorder();
                            mInternalBasinEdges.append(basinEdge);
                            
                            // add internal basin edges for the centroid calculations
                            mCentroid += (float)(*mSettings->mBasinEdges)[basinEdge]->GetVolumeBorder() * (*mSettings->mBasinEdges)[basinEdge]->GetCentroid();
                        }
                    }
                    
                    // if the neighbor is not part of the current parent super voxel, add the boundary to the outer boundary of the super voxel
                    else
                    {
                        // add the current neighbor to the neighbor voxels
                        if (!mNeighborSuperVoxels.contains(currentNeighbor))
                            mNeighborSuperVoxels.append(currentNeighbor);
                        
                        mVolumeBorder += (*mSettings->mBasinEdges)[basinEdge]->GetVolumeBorder();
                        mMeanIntensityBorder += (*mSettings->mBasinEdges)[basinEdge]->GetVolumeBorder() * (*mSettings->mBasinEdges)[basinEdge]->GetMeanIntensityBorder();
                        mNeighborBasinEdges.append(basinEdge);
                    }
                }
            }
            
            // scale the centroid coordinates by the number of voxels
            mCentroid /= (float)mVolumeInternal;
            
            // check if one of the volumes is zero
            if (mVolumeInternal < 1 || mVolumeBorder < 1)
            {
                std::cout << "DIVISION BY ZERO DETECTED" << std::endl;
            }
            else
            {
                // scale the mean intensities based on the Internal and border volume
                mMeanIntensityInternal /= (PixelType)mVolumeInternal;
                mMeanIntensityBorder /= (PixelType)mVolumeBorder;
            }
            
            // compute the normal
            mNormal = GetNormal();
            
            // disable the dirty flag as the parameters have been updated
            mIsDirty = false;
            
            if (mVolumeInternal < 1 || mVolumeBorder < 1)
                PrintFeatures();
        }
        
        /**
         * Get the neighboring super voxels of the current super voxel.
         * @return Pointer to the list of neighboring super voxels.
         */
        QList<unsigned int>* GetNeighborSuperVoxels()
        {
            if (mIsDirty == true)
                UpdateProperties();
            return &mNeighborSuperVoxels;
        }
        
        /**
         * Return pointer to the internal super voxel list.
         * @return Pointer to the internal super voxel list
         */
        QList<unsigned int>* GetInternalSuperVoxels()
        {
            if (mIsDirty == true)
                UpdateProperties();
            return &mInternalSuperVoxels;
        }
        
        /**
         * Function to check if a super voxel is contained in the current super voxel
         * @param superVoxelId The id to be checked.
         * @param Returns true if the super voxel is part of the current super voxel
         */
        bool IsInternalSuperVoxel(unsigned int superVoxelId)
        {
            if (mIsDirty == true)
                UpdateProperties();
            return mInternalSuperVoxels.contains(superVoxelId);
        }
        
        /**
         * Function to check if a super voxel is a neighbor of the current super voxel
         * @param superVoxelId The id to be checked.
         * @param Returns true if the super voxel is a neighbor of the current super voxel
         */
        bool IsNeighborSuperVoxel(unsigned int superVoxelId)
        {
            if (mIsDirty == true)
                UpdateProperties();
            return mNeighborSuperVoxels.contains(superVoxelId);
        }
        
        /**
         * Function to check if a super voxel is contained in the current super voxel
         * @param superVoxelId The id to be checked.
         * @param Returns true if the super voxel is part of the current super voxel
         */
        bool IsInternalSuperVoxel(SuperVoxel<TInputImage>* superVoxel)
        {
            if (mIsDirty == true)
                UpdateProperties();
            
            for (int i = 0; i < superVoxel->GetInternalSuperVoxels()->size(); ++i)
                if (mInternalSuperVoxels.contains((*superVoxel->GetInternalSuperVoxels())[i]))
                    return true;
            
            return false;
        }
        
        /**
         * Function to check if a super voxel is a neighbor of the current super voxel
         * @param superVoxelId The id to be checked.
         * @param Returns true if the super voxel is a neighbor of the current super voxel
         */
        bool IsNeighborSuperVoxel(SuperVoxel<TInputImage>* superVoxel)
        {
            if (mIsDirty == true)
                UpdateProperties();
            
            for (int i = 0; i < superVoxel->GetInternalSuperVoxels()->size(); ++i)
                if (mNeighborSuperVoxels.contains((*superVoxel->GetInternalSuperVoxels())[i]))
                    return true;
            
            return false;
        }
        
        /**
         * Set the parent super voxel used to construct the final merge tree.
         * @param parent Pointer to the parent super voxel. If NULL, the current super voxel is a root.
         */
        void SetParent(SuperVoxel<TInputImage>* parent)
        {
            mParent = parent;
        }
        
        /**
         * Returns true if the current super voxel is a root node.
         * @return Boolean that is true if the current super voxel is a root node.
         */
        bool IsRootNode()
        {
            return mParent == NULL;
        }
        
        /**
         * Returns true if the current super voxel is a leaf node.
         * @return Boolean that is true if the current super voxel is a leaf node.
         */
        bool IsLeafNode()
        {
            return mInternalSuperVoxels.size() == 1;
        }
        
        /**
         * Returns true if the current super voxel needs to be updated.
         * @return Boolean that is true if the current super voxel is a root node.
         */
        bool IsDirty()
        {
            return mIsDirty;
        }
        
        /**
         * Returns true if the segment touches the border
         * @return Boolean that is true if the current super voxel touches the image border
         */
        bool IsBorderSegment()
        {
            return mIsBorderSegment;
        }
        
        /**
         * Returns true if the segment touches the background label
         * @return Boolean that is true if the current super voxel touches image background label
         */
        bool IsSurfaceSegment()
        {
            return mIsSurfaceSegment;
        }
        
        /**
         * Return the minimum label of the current super voxel.
         * @return The minimum label contained in the current super voxel.
         */
        
        unsigned int GetMinimumLabel()
        {
            unsigned int minimumLabel = mSettings->mMaximumLabel;
            for (int i = 0; i < mInternalSuperVoxels.size(); ++i)
            {
                if (mInternalSuperVoxels[i] < minimumLabel)
                    minimumLabel = mInternalSuperVoxels[i];
            }
            
            return minimumLabel;
        }
        
        /**
         * Add a voxel to the Internal.
         * @param InternalIndex The index of the Internal voxel that should be added.
         * @param InternalIntensity The intensity of the current Internal voxel.
         */
        void AddVoxel(const IndexType& internalIndex, const PixelType& internalIntensity)
        {
            mIsDirty = true;
            mInternalIndices.push_back(internalIndex);
            mInternalIntensities.push_back(internalIntensity);
            
            for (int i = 0; i < TInputImage::ImageDimension; ++i)
            {
                mMinimumLocation[i] = std::min<float>(mMinimumLocation[i], internalIndex[i]);
                mMaximumLocation[i] = std::max<float>(mMaximumLocation[i], internalIndex[i]);
            }
        };
        
        /**
         * Returns a reference to the Internal indices.
         * @return Reference to the Internal indices.
         */
        std::list<typename TInputImage::IndexType>* GetInternalIndices()
        {
            return &mInternalIndices;
        }
        
        /**
         * Compute the volume of the convex hull.
         */
        void ComputeConvexHullFeatures()
        {
            if (mIsDirty == true)
                UpdateProperties();
            
            // extract all border voxels (for the convex hull, the internal voxels are irrelevant and slow down the processing)
            unsigned int numBorderPixels = GetVolumeBorder();
            double* myarray = new double[numBorderPixels*TInputImage::ImageDimension];
            double* borderDistances = new double[numBorderPixels];
            double maxDistance = 0.0;
            unsigned int currentIndex = 0;
            unsigned int currentBorderIndex = 0;
            for (int i = 0; i < mNeighborBasinEdges.size(); ++i)
            {
                std::list<typename TInputImage::IndexType>* borderIndices = (*mSettings->mBasinEdges)[mNeighborBasinEdges[i]]->GetBorderIndices();
                typename std::list<typename TInputImage::IndexType>::iterator indexIterator;
                for (indexIterator = borderIndices->begin(); indexIterator != borderIndices->end(); ++indexIterator)
                {
                    for (int j = 0; j < TInputImage::ImageDimension; ++j)
                        myarray[currentIndex + j] = (*indexIterator)[j];
                    currentIndex += TInputImage::ImageDimension;
                    
                    double currentDistance = 0.0;
                    for (int j=0; j<3; ++j)
                        currentDistance +=  pow(mSettings->mIntensityImage->GetSpacing()[j] * ((*indexIterator)[j] - mCentroid[j]), 2.0);
                    currentDistance = sqrt(currentDistance);
                    
                    if (currentDistance > maxDistance)
                        maxDistance = currentDistance;
                    
                    // add border pixel distance
                    borderDistances[currentBorderIndex] = currentDistance;
                    currentBorderIndex++;
                }
            }
            
            // initialize histogram
            for (int i = 0; i < XPIWIT_HISTOGRAMSIZE; ++i)
                mShapeHistogram[i] = 0;
            
            // normalize the distances and fill the histogram
            for (int i = 0; i < numBorderPixels; ++i)
            {
                borderDistances[i] /= maxDistance;
                mShapeHistogram[(unsigned int)round(borderDistances[i]*(XPIWIT_HISTOGRAMSIZE-1))] += 1;
            }
            
            // normalize histogram
            for (int i = 0; i < XPIWIT_HISTOGRAMSIZE; ++i)
                mShapeHistogram[i] = mShapeHistogram[i] / numBorderPixels;
            
            // compute the Bhattacharyya coefficient for the current shape and the average shape
            mBhattacharyyaCoefficient = 0.0;
            for (int i = 0; i < XPIWIT_HISTOGRAMSIZE; ++i)
                mBhattacharyyaCoefficient += sqrt(mShapeHistogram[i] * mSettings->mReferenceShapeHistogram[i]);
            
            // compute the convex hull using qhull
            orgQhull::Qhull qhull("convexHull", TInputImage::ImageDimension, numBorderPixels, myarray, "");
            
            // set the convex volume
            mConvexVolume = qhull.volume();
            mConvexSurfaceArea = qhull.area();
            
            mIsDirtyConvexHull = false;
            delete[] myarray;
        }
        
        float GetConvexVolume()
        {
            if (mIsDirtyConvexHull == true)
                ComputeConvexHullFeatures();
            
            return mConvexVolume;
        }
        
        vnl_vector<float> GetCentroid()
        {
            return mCentroid;
        }
        
        vnl_vector<float> GetMinimumLocation()
        {
            return mMinimumLocation;
        }
        
        vnl_vector<float> GetMaximumLocation()
        {
            return mMaximumLocation;
        }
        
        float GetConvexSurfaceArea()
        {
            if (mIsDirtyConvexHull == true)
                ComputeConvexHullFeatures();
            
            return mConvexSurfaceArea;
        }
        
        double GetVolumeToConvexVolumeRatio()
        {
            return std::min<float>(1.0f, (0.0*GetVolumeBorder() + GetVolumeInternal()) / GetConvexVolume());
        }
        
        void WriteSuperVoxelCoordinates()
        {
            //return;
            
            std::ofstream myfile;
            myfile.open(QString().sprintf("E:\\Projects\\2017\\CNNTraining\\Processing\\Temp\\trainingData_SuperVoxel%i.txt", mId).toStdString().data());
            
            for (int i=0; i<mInternalSuperVoxels.size(); ++i)
            {
                myfile << mInternalSuperVoxels[i] << ", ";
            }
            myfile << std::endl;
            myfile.close();
            
            /*
             TInputImage::RegionType currentRegion = mSettings->mIntensityImage->GetLargestPossibleRegion();
             
             // normalize the distances and fill the histogram
             for (int i = 0; i < mNeighborBasinEdges.size(); ++i)
             {
             std::list<typename TInputImage::IndexType>* borderIndices = (*mSettings->mBasinEdges)[mNeighborBasinEdges[i]]->GetBorderIndices();
             typename std::list<typename TInputImage::IndexType>::iterator indexIterator;
             for (indexIterator = borderIndices->begin(); indexIterator != borderIndices->end(); ++indexIterator)
             {
             if (TInputImage::ImageDimension > 2)
             myfile << (*indexIterator)[0] << ", " << (*indexIterator)[1] << ", " << (*indexIterator)[2] << std::endl;
             else
             myfile << (*indexIterator)[0] << ", " << (*indexIterator)[1] << std::endl;
             }
             }*/
            
            //myfile.close();
        }
        
        void GenerateSuperVoxelImage()
        {
            
            //std::cout << "Supervoxel " << mId << " has extents " << GetMinimumLocation() << ", " << GetMaximumLocation() << std::endl;
            
            //return;
            
            if (mIsDirty == true)
                UpdateProperties();
            
            // specify the padding to use for the snippet
            unsigned int padding = 5;
            
            // compute the crop region using minimum and maximum extents in all dimensions
            typename TInputImage::RegionType cropRegion;
            typename TInputImage::IndexType lowerIndex;
            typename TInputImage::IndexType upperIndex;
            
            // check if it's a valid region
            for (int i = 0; i < TInputImage::ImageDimension; ++i)
            {
                if (mMinimumLocation[i] > mMaximumLocation[i])
                    return;
            }
            
            for (int i = 0; i < TInputImage::ImageDimension; ++i)
            {
                lowerIndex[i] = std::max<double>(0, mMinimumLocation[i] - padding);
                upperIndex[i] = std::min<double>(mMaximumLocation[i] + padding, mSettings->mIntensityImage->GetLargestPossibleRegion().GetSize()[i]-1);
            }
            
            cropRegion.SetIndex(lowerIndex);
            cropRegion.SetUpperIndex(upperIndex);
            
            // initialize the cropped image
            typename TInputImage::Pointer croppedImage = TInputImage::New();
            croppedImage->SetRegions(cropRegion);
            croppedImage->Allocate();
            croppedImage->FillBuffer(0);
            croppedImage->SetReleaseDataFlag(false);
            
            // paint the supervoxel using the original intensities
            PaintSuperVoxel(croppedImage, 1, false);
            
            // ball structuring element
            typedef itk::FlatStructuringElement< TInputImage::ImageDimension > StructerElementType;
            typename StructerElementType::RadiusType structureElementRadius;
            structureElementRadius.Fill(2);
            StructerElementType structureElement;
            structureElement = StructerElementType::Ball(structureElementRadius);
            
            // setup the filter
            typedef itk::GrayscaleDilateImageFilter<TInputImage, TInputImage, StructerElementType> DilateImageFilterType;
            typename DilateImageFilterType::Pointer dilateFilter = DilateImageFilterType::New();
            dilateFilter->SetInput(croppedImage);
            dilateFilter->SetKernel(structureElement);
            dilateFilter->SetReleaseDataFlag(true);
            itkTryCatch(dilateFilter->Update(), "Error: GrayscaleDilateImageFilterWrapper Update Function in CNN snippet preparation.");
            
            itk::ImageRegionIterator<TInputImage> rawImageIterator(mSettings->mIntensityImage, cropRegion);
            itk::ImageRegionIterator<TInputImage> binaryImageIterator(dilateFilter->GetOutput(), cropRegion);
            itk::ImageRegionIterator<TInputImage> outputIterator(croppedImage, cropRegion);
            rawImageIterator.GoToBegin();
            binaryImageIterator.GoToBegin();
            outputIterator.GoToBegin();
            while (!rawImageIterator.IsAtEnd())
            {
                if (binaryImageIterator.Value() > 0)
                    outputIterator.Set(rawImageIterator.Value());
                else
                    outputIterator.Set(0);
                
                ++rawImageIterator;
                ++binaryImageIterator;
                ++outputIterator;
            }
            
            /*
             typedef itk::NormalizeImageFilter<TInputImage, TInputImage> NormalizeFilterType;
             typename NormalizeFilterType::Pointer normalizeFilter = NormalizeFilterType::New();
             normalizeFilter->SetReleaseDataFlag(false);
             normalizeFilter->SetInput(croppedImage);
             itkTryCatch(normalizeFilter->Update(), "Error: GrayscaleDilateImageFilterWrapper Update Function in CNN snippet preparation.");
             */
            
            /*
             
             
             */
            // specify output size, output origin and spacing
            typename TInputImage::SizeType inputSize = cropRegion.GetSize();
            typename TInputImage::SpacingType inputSpacing = mSettings->mIntensityImage->GetSpacing();
            typename TInputImage::PointType inputOrigin;
            typename TInputImage::SizeType outputSize;
            typename TInputImage::PointType outputOrigin;
            typename TInputImage::SpacingType outputSpacing;
            
            outputSize.Fill(32);
            if (TInputImage::ImageDimension > 2 && inputSize[2] == 1)
                outputSize[2] = 1;
            
            //Logger::GetInstance()->WriteLine(QString("- Changed image spacing to ["));
            for (int i = 0; i < TInputImage::ImageDimension; ++i)
            {
                outputSpacing[i] = (double)inputSpacing[i] * (double)inputSize[i] / (double)outputSize[i];
                outputOrigin[i] = lowerIndex[i];
            }
            
            // initialize the transformation for rescaling to the desired size.
            typedef itk::IdentityTransform<double, TInputImage::ImageDimension> IdentityTransformType;
            typedef itk::ResampleImageFilter<TInputImage, TInputImage> ResampleFilterType;
            typename ResampleFilterType::Pointer resampleFilter = ResampleFilterType::New();
            resampleFilter->SetInput(croppedImage);
            resampleFilter->SetSize(outputSize);
            resampleFilter->SetOutputSpacing(outputSpacing);
            resampleFilter->SetOutputOrigin(outputOrigin);
            resampleFilter->SetTransform(IdentityTransformType::New());
            resampleFilter->SetReleaseDataFlag(false);
            itkTryCatch(resampleFilter->Update(), "ExceptionObject caught while updating the ResampleImageFilter!");
            
            typedef itk::MinimumMaximumImageCalculator<TInputImage> MinMaxCalculatorType;
            typename MinMaxCalculatorType::Pointer minMaxCalc = MinMaxCalculatorType::New();
            minMaxCalc->SetImage(resampleFilter->GetOutput());
            minMaxCalc->Compute();
            
            typename TInputImage::PixelType minimum = minMaxCalc->GetMinimum();
            typename TInputImage::PixelType maximum = minMaxCalc->GetMaximum();
            
            typedef itk::IntensityWindowingImageFilter<TInputImage, TInputImage> RescaleIntensityFilterType;
            typename RescaleIntensityFilterType::Pointer rescaleIntensityFilter = RescaleIntensityFilterType::New();
            rescaleIntensityFilter->SetInput(resampleFilter->GetOutput());
            rescaleIntensityFilter->SetWindowMinimum(minimum);
            rescaleIntensityFilter->SetWindowMaximum(maximum);
            rescaleIntensityFilter->SetOutputMinimum(0);
            rescaleIntensityFilter->SetOutputMaximum(1);
            rescaleIntensityFilter->SetReleaseDataFlag(false);
            itkTryCatch(rescaleIntensityFilter->Update(), "ExceptionObject caught while updating the ResampleImageFilter!");
            
            
            //typedef  itk::ImageFileWriter<typename itk::Image<unsigned int, TInputImage::ImageDimension> > WriterType;
            //typedef itk::ImageFileWriter<typename itk::Image<unsigned short, TInputImage::ImageDimension> > WriterType;
            typedef itk::ImageFileWriter<TInputImage> WriterType;
            //typedef itk::ImageFileWriter<TInputImage> WriterType;
            typename WriterType::Pointer writer = WriterType::New();
            //writer->SetFileName(QString().sprintf("E:\\Projects\\2017\\CNNTraining\\Processing\\trainingData_SuperVoxel%i.tif", mId).toStdString().data());
            
            writer->SetFileName((mSettings->mTempFolder + QString().sprintf("Images/trainingData_SuperVoxel%i.tif", mId)).toStdString().data());
            
            /*
             #ifdef Q_OS_UNIX
             writer->SetFileName(QString().sprintf("/tmp/trainingData_SuperVoxel%i.tif", mId).toStdString().data());
             #endif
             #ifdef Q_OS_WIN
             writer->SetFileName(QString().sprintf("C:\\Temp\\trainingData_SuperVoxel%i.tif", mId).toStdString().data());
             #endif
             */
            
            writer->SetInput(rescaleIntensityFilter->GetOutput());
            writer->Update();
            
            // resample the snippet to 32x32x32
        }
        
        float GetCellProbability()
        {
            //return 1;
            
            mCellProbability = 1;
            
            QFile csvFile(mSettings->mTempFolder + QString().sprintf("Probabilities/probabilities_SuperVoxel%i.csv", mId));
            if (csvFile.open(QIODevice::ReadOnly | QIODevice::Text))
            {
                while (!csvFile.atEnd())
                {
                    // read header line
                    QString line = csvFile.readLine();
                    std::cout << line.toStdString() << std::endl;
                    line.replace("\n", "");
                    QStringList probabilities = line.split(',');
                    mCellProbability = atof(probabilities[0].toStdString().data());
                    break;
                }
                
                csvFile.close();
            }
            
            return mCellProbability;
        }
        
        float GetOverSegmentationProbability()
        {
            // return 0;
            
            mOverSegmentationProbability = 0;
            
            QFile csvFile(mSettings->mTempFolder + QString().sprintf("Probabilities/probabilities_SuperVoxel%i.csv", mId));
            if (csvFile.open(QIODevice::ReadOnly | QIODevice::Text))
            {
                while (!csvFile.atEnd())
                {
                    // read header line
                    QString line = csvFile.readLine();
                    std::cout << line.toStdString() << std::endl;
                    line.replace("\n", "");
                    QStringList probabilities = line.split(',');
                    mOverSegmentationProbability = atof(probabilities[1].toStdString().data());
                    break;
                }
                
                csvFile.close();
            }
            
            return mOverSegmentationProbability;
        }
        
        float GetUnderSegmentationProbability()
        {
            //return 0;
            
            mUnderSegmentationProbability = 0;
            
            QFile csvFile(mSettings->mTempFolder + QString().sprintf("Probabilities/probabilities_SuperVoxel%i.csv", mId));
            if (csvFile.open(QIODevice::ReadOnly | QIODevice::Text))
            {
                while (!csvFile.atEnd())
                {
                    // read header line
                    QString line = csvFile.readLine();
                    std::cout << line.toStdString() << std::endl;
                    line.replace("\n", "");
                    QStringList probabilities = line.split(',');
                    mUnderSegmentationProbability = atof(probabilities[2].toStdString().data());
                    break;
                }
                
                csvFile.close();
            }
            
            return mUnderSegmentationProbability;
        }
        
        
        /**
         * Paint the current super voxel.
         */
        void PaintSuperVoxel(TInputImage* outputImage, typename TInputImage::PixelType label, bool useIntensityImage = false, bool markWatershedLines = true)
        {
            if (mIsDirty == true)
                UpdateProperties();
            
            // only the root node should erase the internal watershed boundaries
            if (mParent == NULL || mSelectionState == 1 || useIntensityImage == true)
            {
                for (int i=0; i<mInternalBasinEdges.size(); ++i)
                    (*mSettings->mBasinEdges)[mInternalBasinEdges[i]]->PaintBasinEdge(outputImage, label, useIntensityImage);
            }
            
            // draw watershed boundary using the segment label or zero
            if (mParent == NULL || mSelectionState == 1 || useIntensityImage == true)
            {
                typename TInputImage::PixelType edgeLabel = label;
                if (markWatershedLines == true)
                    edgeLabel = 0;
                
                for (int i = 0; i<mNeighborBasinEdges.size(); ++i)
                    (*mSettings->mBasinEdges)[mNeighborBasinEdges[i]]->PaintBasinEdge(outputImage, edgeLabel, useIntensityImage);
            }
            
            // only the leafs should paint their super voxel area
            if (mLeftSuperVoxel == NULL && mRightSuperVoxel == NULL)
            {
                typename std::list<typename TInputImage::IndexType>::iterator voxelIterator;
                for (voxelIterator = mInternalIndices.begin(); voxelIterator != mInternalIndices.end(); ++voxelIterator)
                {
                    if (useIntensityImage == false)
                        outputImage->SetPixel(*voxelIterator, label);
                    else
                        outputImage->SetPixel(*voxelIterator, mSettings->mIntensityImage->GetPixel(*voxelIterator));
                }
            }
            
            if (mLeftSuperVoxel != NULL)
                mLeftSuperVoxel->PaintSuperVoxel(outputImage, label, useIntensityImage);
            
            if (mRightSuperVoxel != NULL)
                mRightSuperVoxel->PaintSuperVoxel(outputImage, label, useIntensityImage);
        }
        
        
        void PrintTree()
        {
            if (mParent == NULL)
                std::cout << "<root>";
            
            if (mLeftSuperVoxel != NULL && mRightSuperVoxel != NULL)
            {
                std::cout << "<left>";
                mLeftSuperVoxel->PrintTree();
                std::cout << "</left><right>";
                mRightSuperVoxel->PrintTree();
                std::cout << "</right>";
            }
            else
                std::cout << mInternalSuperVoxels[0];
            
            if (mParent == NULL)
                std::cout << "</root>" << std::endl;
        }
        
        unsigned int GetId() { return mId; }
        SuperVoxel<TInputImage>* GetParent() { return mParent;  }
        bool IsValid() { return (mInternalSuperVoxels.size() > 0); }
        
        void SetLeftSuperVoxelParent(SuperVoxel<TInputImage>* parent) { mLeftSuperVoxel->SetParent(parent); }
        void SetRightSuperVoxelParent(SuperVoxel<TInputImage>* parent) { mRightSuperVoxel->SetParent(parent); }
        SuperVoxel<TInputImage>* GetLeftSuperVoxel() { return mLeftSuperVoxel; }
        SuperVoxel<TInputImage>* GetRightSuperVoxel() { return mRightSuperVoxel; }
        
        QList<SuperVoxel<TInputImage>*>* GetSuperVoxelList(QList<SuperVoxel<TInputImage>*>* superVoxelList)
        {
            if (mParent == NULL)
                superVoxelList = new QList<SuperVoxel<TInputImage>*>();
            superVoxelList->append(this);
            
            if (mLeftSuperVoxel != NULL)
                mLeftSuperVoxel->GetSuperVoxelList(superVoxelList);
            
            if (mRightSuperVoxel != NULL)
                mRightSuperVoxel->GetSuperVoxelList(superVoxelList);
            
            return superVoxelList;
        }
        
        void SetSelectionState(unsigned int state) { mSelectionState = state; }
        unsigned int GetSelectionState() { return mSelectionState; }
        
        void Select()
        {
            DeleteAncestors();
            DeleteChildren();
            SetSelectionState(1);
        }
        
        void DeleteAncestors()
        {
            if (mParent != NULL)
            {
                mParent->SetSelectionState(2);
                mParent->DeleteAncestors();
            }
        }
        
        void DeleteChildren()
        {
            if (mLeftSuperVoxel != NULL)
            {
                mLeftSuperVoxel->SetSelectionState(2);
                mLeftSuperVoxel->DeleteChildren();
            }
            
            if (mRightSuperVoxel != NULL)
            {
                mRightSuperVoxel->SetSelectionState(2);
                mRightSuperVoxel->DeleteChildren();
            }
        }
        
        /**
         * Prints debug output of the current super voxel.
         */
        void PrintFeatures()
        {
            std::cout << "+ Super voxel " << mId << " contains the following parts: ";
            for (int i=0; i<mInternalSuperVoxels.size(); ++i)
            {
                std::cout << mInternalSuperVoxels[i] << ", ";
            }
            std::cout << std::endl;
            
            std::cout << "+ Following neighbors are touched: ";
            for (int i = 0; i<mNeighborSuperVoxels.size(); ++i)
            {
                std::cout << mNeighborSuperVoxels[i] << ", ";
            }
            std::cout << std::endl;
            
            std::cout << "-- VolumeInternal: " << GetVolumeInternal() << std::endl;
            std::cout << "-- VolumeBorder: " << GetVolumeBorder() << std::endl;
            std::cout << "-- MeanIntensityInternal: " << GetMeanIntensityInternal() << std::endl;
            std::cout << "-- MeanIntensityBorder: " << GetMeanIntensityBorder() << std::endl;
            std::cout << "-- Centroid: " << GetCentroid() << std::endl;
            std::cout << "-- Probabilities: Cell: " << GetCellProbability() << ", OverSeg: " << GetOverSegmentationProbability() << ", UnderSeg: " << GetUnderSegmentationProbability() << std::endl;
            
            std::cout << "-- Shape Histogram: ";
            for (int i = 0; i < XPIWIT_HISTOGRAMSIZE; ++i)
                std::cout << mShapeHistogram[i] << ", ";
            std::cout << " BC = " << mBhattacharyyaCoefficient;
            std::cout << std::endl;
            
        }
        
    protected:
        unsigned int mId;                                        // id of the current super voxel
        itk::Matrix<float, TInputImage::ImageDimension, TInputImage::ImageDimension> mStructureTensor;                    // structure tensor the intensity gradient
        vnl_vector<float> mNormal;                                // smallest eigenvector of the intensity gradient structure tensor
        vnl_vector<float> mCentroid;                            // the centroid of the super voxel (TODO: including the boundaries between internal super voxels)
        vnl_vector<float> mMinimumLocation;                    // the minimum location of the current super voxel
        vnl_vector<float> mMaximumLocation;                    // the maximum location of the current super voxel
        float mMeanIntensityBorder;                                // the mean intensity along the border of the super voxel
        float mMeanIntensityInternal;                            // the mean intensity of the super voxels internal voxels (including the boundaries between internal super voxels)
        float mVolumeInternal;                                    // the Internal volume of the super voxel including internal boundaries
        float mVolumeBorder;                                    // the border volume
        float mConvexVolume;
        float mConvexSurfaceArea;
        float mShapeHistogram[XPIWIT_HISTOGRAMSIZE];
        float mBhattacharyyaCoefficient;
        bool mIsBorderSegment;                                    // flag to identify if it's a super voxel touching the border
        bool mIsSurfaceSegment;                                    // flag to identify if the super voxel touches the background label (i.e., if it's a surface super voxel)
        bool mIsDirty;                                            // if enabled, the features get updated before returning any values
        bool mIsDirtyConvexHull;
        unsigned int mSelectionState;                            // state used for the merge tree selection (0: no state, 1: selected, 2: deleted)
        float mUnderSegmentationProbability;
        float mOverSegmentationProbability;
        float mCellProbability;
        
        SuperVoxelProperties<TInputImage>* mSettings;            // pointer to the global settings variable
        QList<unsigned int> mInternalBasinEdges;                // list containing all current basin edges
        QList<unsigned int> mNeighborBasinEdges;                // list containing all current basin edges
        QList<unsigned int> mInternalSuperVoxels;                // list of Internal super voxel ids (correspond to initial watershed ids that form the super voxel)
        QList<unsigned int> mNeighborSuperVoxels;                // list of neighboring super voxel ids (correspond to initial watershed ids)
        std::list<typename TInputImage::IndexType> mInternalIndices;    // list containing all Internal indices of the super voxel
        std::list<typename TInputImage::PixelType> mInternalIntensities;    // list containing all Internal intensities of the super voxel
        
        SuperVoxel* mParent;                                    // pointer to the parent of this super voxel. If NULL, this is a root node
        SuperVoxel* mLeftSuperVoxel;                            // used to store a super voxel hierarchy based on a binary merge forest (left branch)
        SuperVoxel* mRightSuperVoxel;                            // used to store a super voxel hierarchy based on a binary merge forest (right branch)
        TInputImage* mSuperVoxelImage;                    // super voxel image used for classification by the CNN
    };
    
    
    /**
     * @class SeedPoint container class for seed points.
     * used to store informatino such as index, scale, intensity and an id.
     */
    template<class TInputImage>
    class SuperVoxelEdge
    {
    public:
        
        // typedefs for the templated index and pixel type
        typedef typename TInputImage::IndexType IndexType;
        typedef typename TInputImage::PixelType PixelType;
        
        /**
         * Constructor to create an edge between two neighboring super voxels.
         * Super voxels can either be initial segments of the watershed oversegmentation or combinations of initial segments.
         * @param segment1 Pointer to the first super voxel
         * @param segment2 Pointer to the second super voxel
         * @param settings Pointer to the global settings structure.
         */
        SuperVoxelEdge(SuperVoxel<TInputImage>* segment1, SuperVoxel<TInputImage>* segment2, SuperVoxelProperties<TInputImage>* settings)
        {
            mSegment1 = segment1;
            mSegment2 = segment2;
            mSettings = settings;
            mId = mSettings->ConvertSegmentIdsToArrayIndex(segment1->GetId(), segment2->GetId());
            mSortFeature = 1;
            mVolumeCombined = 0;
            mIsDirty = true;
        }
        
        /**
         * Destructor for the super voxel edge
         */
        ~SuperVoxelEdge()
        {
        }
        
        /**
         * Set the pointer to the first super voxel.
         * @param segment1 Pointer to the first super voxel
         */
        void SetSegment1(SuperVoxel<TInputImage>* segment1)
        {
            mSegment1 = segment1;
            mIsDirty = true;
        }
        
        /**
         * Set the pointer to the second super voxel.
         * @param segment2 Pointer to the second super voxel
         */
        void SetSegment2(SuperVoxel<TInputImage>* segment2)
        {
            mSegment2 = segment2;
            mIsDirty = true;
        }
        
        /**
         * Return the pointer to super voxel 1.
         * @return The pointer to super voxel 1.
         */
        SuperVoxel<TInputImage>* GetSegment1() { return mSegment1; }
        
        /**
         * Return the pointer to super voxel 2.
         * @return The pointer to super voxel 2.
         */
        SuperVoxel<TInputImage>* GetSegment2() { return mSegment2; }
        
        /**
         * Function to update the edge merge features based on the current super voxel pair.
         */
        void ComputeEdgeFeatures()
        {
            // get pointers to the Internal and the neighboring super voxel lists
            QList<unsigned int>* InternalVoxels1 = mSegment1->GetInternalSuperVoxels();
            QList<unsigned int>* InternalVoxels2 = mSegment2->GetInternalSuperVoxels();
            QList<unsigned int>* neighborVoxels1 = mSegment1->GetNeighborSuperVoxels();
            QList<unsigned int>* neighborVoxels2 = mSegment2->GetNeighborSuperVoxels();
            
            // initialize the basin edge volume and mean intensity
            float minEdgeVolume = 0.0;
            float basinEdgeVolume = 0.0;
            PixelType basinEdgeIntensity = 0.0;
            
            // clear the basin edges
            mBasinEdges.clear();
            for (int i=0; i<InternalVoxels1->size(); ++i)
            {
                for (int j=0; j<InternalVoxels2->size(); ++j)
                {
                    int currentInternalVoxel1 = (*InternalVoxels1)[i];
                    int currentInternalVoxel2 = (*InternalVoxels2)[j];
                    
                    if (neighborVoxels2->indexOf((*InternalVoxels1)[i]) >= 0 && neighborVoxels1->indexOf((*InternalVoxels2)[j]) >= 0)
                    {
                        unsigned int currentBasinEdgeId = mSettings->ConvertSegmentIdsToArrayIndex((*InternalVoxels1)[i], (*InternalVoxels2)[j]);
                        if (!mBasinEdges.contains(currentBasinEdgeId) && mSettings->mBasinEdges->contains(currentBasinEdgeId))
                        {
                            mBasinEdges.append(currentBasinEdgeId);
                            basinEdgeVolume += (*mSettings->mBasinEdges)[currentBasinEdgeId]->GetVolumeBorder();
                            basinEdgeIntensity += (*mSettings->mBasinEdges)[currentBasinEdgeId]->GetVolumeBorder() * (*mSettings->mBasinEdges)[currentBasinEdgeId]->GetMeanIntensityBorder();
                        }
                    }
                }
            }
            
            itk::Matrix<float> structureTensor;
            structureTensor.Fill(0);
            vnl_vector<float> normalVector(3);
            vnl_vector<float> centroid(3);
            normalVector.fill(0);
            centroid.fill(0);
            for (int i = 0; i < mBasinEdges.length(); ++i)
            {
                normalVector += (float)(*mSettings->mBasinEdges)[mBasinEdges[i]]->GetVolumeBorder() * (*mSettings->mBasinEdges)[mBasinEdges[i]]->GetNormal();
                centroid += (float)(*mSettings->mBasinEdges)[mBasinEdges[i]]->GetVolumeBorder() * (*mSettings->mBasinEdges)[mBasinEdges[i]]->GetCentroid();
                //std::cout << "Normal: " << normalVector << ", ZDir: " << zDirection << std::endl;
                
                structureTensor += (*mSettings->mBasinEdges)[mBasinEdges[i]]->GetStructureTensor();
            }
            centroid /= basinEdgeVolume;
            normalVector.normalize();
            
            ////////// COMPUTE STRUCTURE TENSOR EIGENVALUES ///////////
            /*
             itk::Vector<float> eigenValues;
             eigenValues.Fill(0);
             
             itk::Matrix<float> eigenVectors;
             eigenVectors.Fill(0);
             
             typedef itk::SymmetricEigenAnalysis<itk::Matrix<float>, itk::Vector<float> > EigenType;
             typename EigenType eigenAnalysis;
             eigenAnalysis.SetDimension(3);
             eigenAnalysis.SetOrderEigenMagnitudes(true);
             eigenAnalysis.ComputeEigenValuesAndVectors(structureTensor, eigenValues, eigenVectors);
             
             normalVector[0] = eigenVectors[0][0];
             normalVector[1] = eigenVectors[0][1];
             normalVector[2] = eigenVectors[0][2];
             normalVector.normalize();
             */
            //////////////////////////////////////////////////////////
            
            
            
            vnl_vector<float> centroidDistance(3);
            vnl_vector<float> centroidDistance1(3);
            vnl_vector<float> centroidDistance2(3);
            centroidDistance = mSegment1->GetCentroid() - mSegment2->GetCentroid();
            centroidDistance.normalize();
            centroidDistance1 = centroid - mSegment1->GetCentroid();
            centroidDistance1.normalize();
            centroidDistance2 = centroid - mSegment2->GetCentroid();
            centroidDistance2.normalize();
            
            
            if (basinEdgeVolume < 1)
                std::cout << "Division by zero " << std::endl;
            
            // scale the mean intensity based on the basin edge volume
            basinEdgeIntensity /= basinEdgeVolume;
            
            // temporary create a super voxel of both segments
            SuperVoxel<TInputImage> combinedSuperVoxel(-1, mSegment1, mSegment2, mSettings, false);
            mVolumeCombined = combinedSuperVoxel.GetVolumeInternal();
            
            // get the regional mean intensities
            PixelType meanIntensity1 = mSegment1->GetMeanIntensityInternal();
            PixelType meanIntensity2 = mSegment2->GetMeanIntensityInternal();
            
            typename TInputImage::IndexType centroidIndex;
            for (int i = 0; i < TInputImage::ImageDimension; ++i)
                centroidIndex[i] = std::round(centroid[i]);
            
            vnl_vector<float> gradient = mSettings->mMaskGradientImage->GetPixel(centroidIndex).GetVnlVector();
            gradient = -gradient;
            gradient.normalize();
            
            // compute the sort feature used for region merging
            float snrCriterion = std::min<float>(1.0f, 0.5 * basinEdgeIntensity * (1.0 / meanIntensity1 + 1.0 / meanIntensity2));
            
            
            float minimumVolumeCriterion = mSettings->TrapMF(std::min<unsigned int>((float)mSegment1->GetVolumeInternal(), (float)mSegment2->GetVolumeInternal()), 0, mSettings->mMinimumVolume, FLT_MAX, FLT_MAX);
            
            float angle = std::acos(dot_product(centroidDistance, normalVector)); // for deconvolved dataSet!
            float angle1 = std::acos(dot_product(centroidDistance, mSegment1->GetNormal())); // for mVenus and LSM880 dataSet!
            float angle2 = std::acos(dot_product(centroidDistance, mSegment2->GetNormal())); // for mVenus and LSM880 dataSet!
            //float angle = std::acos(dot_product(centroidDistance, gradient)); // not sure
            //gradient(0) = 0;
            //gradient(1) = 0;
            //gradient(2) = 1;
            //float angle = std::acos(dot_product(centroidDistance, gradient)); // not sure
            
            //angle = std::min<float>(angle, M_PI - angle);
            //float angleCriterion = mSettings->TrapMF(angle, 0, M_PI / 4, FLT_MAX, FLT_MAX);
            
            angle1 = std::min<float>(angle1, M_PI - angle1);
            angle2 = std::min<float>(angle2, M_PI - angle2);
            float angleCriterion = mSettings->TrapMF(std::max(angle1, angle2), 0, mSettings->mMaximumAngle, FLT_MAX, FLT_MAX);
            
            float probabilityCriterion = mSettings->TrapMF(basinEdgeIntensity, 0, 0.5, FLT_MAX, FLT_MAX);
            //if (minimumVolumeCriterion < 1.0)
            //    minimumVolumeCriterion = minimumVolumeCriterion * basinEdgeIntensity;
            
            float totalBorderIntensity = (mSegment1->GetVolumeBorder() * mSegment1->GetMeanIntensityBorder() + mSegment2->GetVolumeBorder() * mSegment2->GetMeanIntensityBorder()) / (mSegment1->GetVolumeBorder() + mSegment2->GetVolumeBorder());
            //float boundaryCriterion = std::min<float>(1.0f, std::abs<float>((basinEdgeIntensity - combinedSuperVoxel.GetMeanIntensityInternal())) / std::abs<float>((basinEdgeIntensity - totalBorderIntensity)));
            float boundaryCriterion = std::min<float>(1.0f, std::abs<float>((basinEdgeIntensity - (1.0/(mSegment1->GetVolumeInternal() + mSegment2->GetVolumeInternal()))*(mSegment1->GetVolumeInternal() * mSegment1->GetMeanIntensityInternal() + mSegment2->GetVolumeInternal() * mSegment2->GetMeanIntensityInternal()))) / std::abs<float>((basinEdgeIntensity - totalBorderIntensity)));
            
            unsigned int segment1 = (*mSegment1->GetInternalSuperVoxels())[0];
            unsigned int segment2 = (*mSegment2->GetInternalSuperVoxels())[0];
            //std::cout << "(NEW) Segments: " << segment1 << ", " << segment2 << ": " << basinEdgeIntensity << ", " << combinedSuperVoxel.GetMeanIntensityInternal() << ", " << totalBorderIntensity << ", " << boundaryCriterion << std::endl;
            
            
            //if ((mContainsBorderSegment == true && mDisableMinimumVolumeCriterionOnBorder == true) || (mVolumeCombined > mMaximumVolume))
            //    mMinimumVolumeCriterion = 1.0;
            
            //mSortFeature = boundaryCriterion; //sqrtf(snrCriterion*snrCriterion + minimumVolumeCriterion*minimumVolumeCriterion) / sqrtf(2.0);
            //mSortFeature = 1.0;
            
            //mSortFeature = sqrtf(1.0 + minVolumeCriterion*minVolumeCriterion);
            
            
            int numFeatures = 0;
            mSortFeature = 0.0;
            if (mSettings->mUseMinimumVolumeCriterion == true)
            {
                if (!mSettings->mDisableMinimumVolumeCriterionOnBorder || (!mSegment1->IsBorderSegment() && !mSegment2->IsBorderSegment() && mSettings->mDisableMinimumVolumeCriterionOnBorder))
                {
                    mSortFeature += minimumVolumeCriterion * minimumVolumeCriterion;
                    numFeatures++;
                }
            }
            
            if (mSettings->mUseMeanRatioCriterion == true)
            {
                mSortFeature += snrCriterion * snrCriterion;
                numFeatures++;
            }
            
            if (mSettings->mUseBoundaryCriterion == true)
            {
                mSortFeature += boundaryCriterion * boundaryCriterion;
                numFeatures++;
            }
            
            if (mSettings->mUseSphericityCriterion == true)
            {
                mSortFeature += basinEdgeIntensity * basinEdgeIntensity;
                numFeatures++;
                //std::cout << "Using sphericity feature..." << std::endl;
            }
            
            if (mSettings->mUseProbabilityMapCriterion == true)
            {
                mSortFeature = probabilityCriterion * probabilityCriterion;
                numFeatures++;
            }
            
            if (mSettings->mUseStructureTensorCriterion == true)
            {
                mSortFeature += angleCriterion * angleCriterion;
                numFeatures++;
            }
            
            /*
             if (isShortestEdge1 == true && isShortestEdge2 == true)
             {
             mSortFeature = 1.0;
             return;
             }
             */
            
            // normalize the sort feature, such that it is smaller than one of one of the criteria is violated and 1 otherwise
            /*        if (mUseSphericityCriterion == true && (mUseMinimumVolumeCriterion == false || (mUseMinimumVolumeCriterion == true && mMinimumVolumeCriterion >= 1.0)))
             mSortFeature = std::max<float>(sphericity, sqrtf(mSortFeature) / sqrtf(numFeatures));
             else
             */
            
            mSortFeature = sqrtf(mSortFeature) / sqrtf(numFeatures);
            
            //if (mSortFeature < (1.0 - FLT_EPSILON))
            //    mSortFeature -= 1.0 / float(mId);
            
            //if (basinEdgeVolume < (0.1 * std::min(mSegment1->GetVolumeBorder(), mSegment2->GetVolumeBorder())))
            //    mSortFeature = 1;
            
            if (mSortFeature < 1)
            {
                if (mSettings->mScaleByIntensity)
                    mSortFeature *= basinEdgeIntensity;
                else
                    mSortFeature = basinEdgeIntensity;
            }
            
//           if (mSettings->mUseProbabilityMapCriterion && probabilityCriterion >= 1.0)
//                mSortFeature = 1.0;
            
            for (int i = 0; i < mBasinEdges.length(); ++i)
                (*mSettings->mBasinEdges)[mBasinEdges[i]]->SetSortFeature(mSortFeature);
            
            mIsDirty = false;
        }
        
        
        unsigned int GetVolumeCombined()
        {
            if (mIsDirty == true)
                ComputeEdgeFeatures();
            return mVolumeCombined;
        }
        
        
        /**
         * Sets the sort feature
         * @param the current sort feature to set
         */
        void SetSortFeature(const float sortFeature)
        {
            mSortFeature = sortFeature;
        }
        
        /**
         * Returns the sort feature
         * @return the current sort feature
         */
        float GetSortFeature()
        {
            if (mIsDirty == true)
                ComputeEdgeFeatures();
            return mSortFeature;
        }
        
        /**
         * Return pointer to the basin edges list.
         * @return Pointer to the basin edges list.
         */
        QList<unsigned int>* GetBasinEdges()
        {
            return &mBasinEdges;
        }
        
        /**
         * Return the Id of the current super voxel edge.
         * @return The id of the current super voxel edge.
         */
        unsigned int GetId() { return mId; }
        
        
        /**
         * Print the edge properties if the debug output flag is enabled.
         */
        void PrintFeatures()
        {
            /*
             std::cout << "- NeighborPair " << GetSegmentId1() << ", " << GetSegmentId2() << ": "
             << "borderLength " << GetBorderVolume()
             << ", meanIntensity " << GetBorderMeanIntensity()
             << ", volume1 " << mVolume1
             << ", volume2 " << mVolume2
             << ", meanIntensity1 " << mMeanIntensity1
             << ", meanIntensity2 " << mMeanIntensity2
             << ", snrCriterion " << mSortFeature
             << ", sphericity " << mSphericityCriterion << std::endl;*/
            
            
            std::cout << "---------------------" << std::endl;
            mSegment1->PrintFeatures();
            mSegment2->PrintFeatures();
            
            std::cout << "- Contained basin edges are: " << std::endl;
            for (int i=0; i<mBasinEdges.size(); ++i)
            {
                std::cout << "-- Basin edge " << mBasinEdges[i] << " connecting " << mSettings->ConvertArrayIndexToIndex1(mBasinEdges[i]) << " and " << mSettings->ConvertArrayIndexToIndex2(mBasinEdges[i]) << " comprises " << (*mSettings->mBasinEdges)[mBasinEdges[i]]->GetVolumeBorder() << " with intensity " << (*mSettings->mBasinEdges)[mBasinEdges[i]]->GetMeanIntensityBorder() << std::endl;
            }
            std::cout << "---------------------" << std::endl;
        }
        
    protected:
        
        SuperVoxelProperties<TInputImage>* mSettings;                // Pointer to the global settings variable
        bool mIsDirty;                                                // If enabled, edge features are recomputed
        float mSortFeature;                                            // the current sort feature value used to merge neighboring super voxels
        unsigned int mVolumeCombined;
        QList<unsigned int> mBasinEdges;                            // list containing all current basin edges
        SuperVoxel<TInputImage>* mSegment1;                            // pointer to the left super voxel
        SuperVoxel<TInputImage>* mSegment2;                            // pointer to the right super voxel
        unsigned int mId;
    };
    
    // function to compare two super voxel edges based on the sort features.
    template<class TInputImage>
    bool EdgeLessThan(itk::SuperVoxelEdge<TInputImage>& edge1, itk::SuperVoxelEdge<TInputImage>& edge2)
    {
        return edge1.GetSortFeature() < edge2.GetSortFeature();
    }
    
} // end namespace itk

#endif
