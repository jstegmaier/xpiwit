/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// include required headers
#include "itkSplitConnectedBlobsImageFilter.h"
#include "itkImageRegionIterator.h"
#include "itkImageRegionConstIterator.h"
#include "itkRegionalMinimaImageFilter.h"
#include "itkSignedMaurerDistanceMapImageFilter.h"
#include "itkMorphologicalWatershedImageFilter.h"
#include "itkDanielssonDistanceMapImageFilter.h"
#include "itkInvertIntensityImageFilter.h"
#include "itkWatershedImageFilter.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkImageFileWriter.h"
#include <iostream>
#include <fstream>
#include <string>
#include <QString>

// namespace itk
namespace itk
{

// the default constructor
template <class TInputImageType, class TOutputImageType>
SplitConnectedBlobsImageFilter<TInputImageType, TOutputImageType>::SplitConnectedBlobsImageFilter()
{
	// set default values for the parameters
	m_Level = 0.0;
	m_BinaryOutput = true;

	// allocate the output
	this->SetNumberOfRequiredOutputs( 1 );
	this->SetNthOutput( 0, this->MakeOutput(0) );
}


// the destructor
template <class TInputImageType, class TOutputImageType>
SplitConnectedBlobsImageFilter<TInputImageType, TOutputImageType>::~SplitConnectedBlobsImageFilter()
{
}


// generate the data
template <class TInputImageType, class TOutputImageType>
void SplitConnectedBlobsImageFilter<TInputImageType, TOutputImageType>::GenerateData()
{
	// get input and allocate output
	typename TOutputImageType::Pointer output = this->GetOutput();
	typename TInputImageType::ConstPointer input  = this->GetInput();
	output->SetRegions( input->GetLargestPossibleRegion() );
	output->Allocate();

	typedef itk::Image<unsigned short, TInputImageType::ImageDimension> InternalImageType;

	// copy input to the output
	itk::ImageRegionConstIterator<TInputImageType> inputIterator( input, input->GetLargestPossibleRegion() );
	itk::ImageRegionIterator<TOutputImageType> outputIterator( output, output->GetLargestPossibleRegion() );
	
	// set iterators to the beginning
	inputIterator.GoToBegin();
	outputIterator.GoToBegin();

	// iterate trough the pixels and copy input to the output
	while (inputIterator.IsAtEnd() == false)
	{
		outputIterator.Set( inputIterator.Get() );

		++outputIterator;
		++inputIterator;
	}
	
	// define the filters
	typedef typename itk::IntensityWindowingImageFilter<TInputImageType, InternalImageType> IntensityWindowingImageFilterType;
	typedef typename itk::SignedMaurerDistanceMapImageFilter<InternalImageType, InternalImageType> SignedMaurerDistanceMapImageFilterType;
	typedef typename itk::DanielssonDistanceMapImageFilter<InternalImageType, InternalImageType> DanielssonDistanceMapImageFilterType;
	typedef typename itk::InvertIntensityImageFilter<InternalImageType> InvertIntensityImageFilterType;
	typedef typename itk::MorphologicalWatershedImageFilter<InternalImageType, InternalImageType> WatershedFilterType;
	typedef typename itk::ImageFileWriter<InternalImageType> WriterType;
	/*
	//typedef typename itk::WatershedImageFilter<TInputImageType> WatershedFilterType;

	typename SignedMaurerDistanceMapImageFilterType::Pointer distanceMapFilter = SignedMaurerDistanceMapImageFilterType::New();
	typename WatershedFilterType::Pointer watershedFilter = WatershedFilterType::New();

	// generate the distance map
	distanceMapFilter->SetInput( input );
	distanceMapFilter->SetBackgroundValue( 0 );
	distanceMapFilter->SetUseImageSpacing( true );
	distanceMapFilter->SetSquaredDistance( false );
	distanceMapFilter->SetInsideIsPositive( false );
	distanceMapFilter->SetReleaseDataFlag( this->GetReleaseDataFlag() );

	// h-minima filter to remove the smallest minima
 // typedef HMinimaImageFilter< InternalImageType, InternalImageType > HMinimaType;
 // HMinimaType::Pointer hmin = HMinimaType::New();

 // hmin->SetInput( distanceMapFilter->GetOutput() );
  itkTryCatch( distanceMapFilter->Update(), "H minimum supression" );

	
	// Delegate to a R-Min filter to find the regional minima
	//typedef RegionalMinimaImageFilter<InternalImageType, InternalImageType> RMinType;
	//typename RMinType::Pointer rmin = RMinType::New();
	//rmin->SetInput( distanceMapFilter->GetOutput() );
//	rmin->SetFullyConnected(true);
//	rmin->SetBackgroundValue(NumericTraits< OutputImagePixelType >::Zero);
//	rmin->SetForegroundValue( NumericTraits< OutputImagePixelType >::max() );

//	itkTryCatch( rmin->Update(), "SplitConnectedBlobsImageFilter -> Regional Minima Detection" );
	*/

	typename IntensityWindowingImageFilterType::Pointer convertIntensity = IntensityWindowingImageFilterType::New();
	convertIntensity->SetInput( input );
	convertIntensity->SetWindowMinimum(0.0);
	convertIntensity->SetWindowMaximum(1.0);
	convertIntensity->SetOutputMinimum(0.0);
	convertIntensity->SetOutputMaximum(65535);
		
	/*
	convertIntensity->SetReleaseDataFlag(0);
	convertIntensity->Update();
	WriterType::Pointer writer0 = WriterType::New();
	writer0->SetInput(convertIntensity->GetOutput());
	writer0->SetFileName( "D:/Programming/XPIVIT/XPIWIT/Bin/Test/Out/InputImage.tif" );
	writer0->Update();	
	*/
	typename InvertIntensityImageFilterType::Pointer invertIntensity1 = InvertIntensityImageFilterType::New();
	invertIntensity1->SetInput( convertIntensity->GetOutput() );
	
	/*
	invertIntensity1->SetReleaseDataFlag(0);
	invertIntensity1->Update();
	WriterType::Pointer writer1 = WriterType::New();
	writer1->SetInput(invertIntensity1->GetOutput());
	writer1->SetFileName( "D:/Programming/XPIVIT/XPIWIT/Bin/Test/Out/InvertedImage.tif" );
	writer1->Update();	
	*/
	typename DanielssonDistanceMapImageFilterType::Pointer distanceMap = DanielssonDistanceMapImageFilterType::New();
	distanceMap->SetInput( invertIntensity1->GetOutput() );
	/*
	distanceMap->SetReleaseDataFlag(0);
	distanceMap->Update();
	WriterType::Pointer writer2 = WriterType::New();
	writer2->SetInput(distanceMap->GetOutput());
	writer2->SetFileName( "D:/Programming/XPIVIT/XPIWIT/Bin/Test/Out/DistanceMap.tif" );
	writer2->Update();
	*/
	typename InvertIntensityImageFilterType::Pointer invertIntensity2 = InvertIntensityImageFilterType::New();
	invertIntensity2->SetInput( distanceMap->GetOutput() );
	/*
	invertIntensity2->SetReleaseDataFlag(0);
	invertIntensity2->Update();
	WriterType::Pointer writer3 = WriterType::New();
	writer3->SetInput(invertIntensity2->GetOutput());
	writer3->SetFileName( "D:/Programming/XPIVIT/XPIWIT/Bin/Test/Out/InvertedDistanceMap.tif" );
	writer3->Update();*/

	// perform the watershed segmentation on the distance map
	typename WatershedFilterType::Pointer watershedFilter = WatershedFilterType::New();
	watershedFilter->SetLevel( m_Level );
	//watershedFilter->SetThreshold( m_Threshold );
	watershedFilter->SetInput( invertIntensity2->GetOutput() );
	watershedFilter->SetReleaseDataFlag( this->GetReleaseDataFlag() );

	/*
	watershedFilter->SetReleaseDataFlag(0);
	watershedFilter->Update();
	WriterType::Pointer writer4 = WriterType::New();
	writer4->SetInput(watershedFilter->GetOutput());
	writer4->SetFileName( "D:/Programming/XPIVIT/XPIWIT/Bin/Test/Out/Watershed.tif" );
	writer4->Update();*/
	
	//itk::CastImageFilter<WatershedImageFilterType::OutputImageType, OutputImageType>::Pointer castFilter = itk::CastImageFilter<WatershedImageFilterType::OutputImageType, OutputImageType>::New();
	//castFilter->SetInput( watershedFilter->GetOutput() );

	// update the filter
	itkTryCatch( watershedFilter->Update(), "SplitConnectedBlobsImageFilter -> WatershedTransform" );

	
	
	// dichotomize the watershed output to get the split segmentation image
	//itk::ImageRegionConstIterator<OutputImageType> watershedIterator( watershedFilter->GetOutput(), watershedFilter->GetOutput()->GetLargestPossibleRegion() );

	//itk::CastImageFilter<InternalImageType, OutputImageType>::Pointer castFilter = itk::CastImageFilter<InternalImageType, OutputImageType>::New();
	//castFilter->SetInput( distanceMapFilter->GetOutput() );
	//castFilter->Update();

	//itk::ImageRegionIterator<itk::Image<IdentifierType, TInputImageType::ImageDimension > > watershedIterator( watershedFilter->GetOutput(), watershedFilter->GetOutput()->GetLargestPossibleRegion() );
	itk::ImageRegionIterator<InternalImageType> watershedIterator( watershedFilter->GetOutput(), watershedFilter->GetOutput()->GetLargestPossibleRegion() );
		
	// reset the iterators to the beginning
	watershedIterator.GoToBegin();
	outputIterator.GoToBegin();
	inputIterator.GoToBegin();

	// iterate trough the pixels and set values to 0 if on watershed border or outside a segmented region
	// set the values to one if within a detected region
	while (watershedIterator.IsAtEnd() == false)
	{
		//outputIterator.Set( watershedIterator.Get() );

		// set intermediate regions to zero
		if (watershedIterator.Get() == 0)
			outputIterator.Set( 0 );
		//else
		//	outputIterator.Set( (float)(watershedIterator.Get()) );
		
		// copy watershed basin id if output does not have to be binary
		if (m_BinaryOutput == false)
		{
			if (watershedIterator.Get() > 0 && outputIterator.Get() > 0)
				outputIterator.Set( (float)watershedIterator.Get()/65535 );
		}

		// increment the iterators
		++outputIterator;
		++watershedIterator;
	}
}

} // end namespace itk
