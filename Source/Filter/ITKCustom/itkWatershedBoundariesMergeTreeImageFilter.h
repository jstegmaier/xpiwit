/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef WatershedBoundariesMergeTreeImageFilter_H
#define WatershedBoundariesMergeTreeImageFilter_H

// include required headers
#include "../Base/Management/ITKDefinitions.h"
#include "itkBoxImageFilter.h"
#include "itkImage.h"
#include "itkNumericTraits.h"
#include "itkNeighborhoodIterator.h"
#include "itkConstNeighborhoodIterator.h"
#include "itkZeroFluxNeumannBoundaryCondition.h"
#include "itkWatershedBoundariesMergeTreeUtilities.h"
#include <vector>
#include <float.h>
#define _USE_MATH_DEFINES // required to enable math defines in visual studio
#include <math.h>
#include <QString>
#include <QHash>
#include <list>

#include "../Base/MetaData/MetaDataFilter.h"

using namespace XPIWIT;

namespace itk
{

/**
 * @class ExtractKeyPointsImageFilter extracts local maxima from a given image.
 */
template< class TInputImage, class TOutputImage >
class ITK_EXPORT WatershedBoundariesMergeTreeImageFilter : public BoxImageFilter< TInputImage, TOutputImage >
{
public:
    // Extract dimension from input and output image.
    itkStaticConstMacro(InputImageDimension, unsigned int, TInputImage::ImageDimension);
    itkStaticConstMacro(OutputImageDimension, unsigned int, TOutputImage::ImageDimension);

    // Convenient typedefs for simplifying declarations.
    typedef TInputImage  InputImageType;
    typedef TOutputImage OutputImageType;

    // Standard class typedefs.
    typedef WatershedBoundariesMergeTreeImageFilter           Self;
    typedef BoxImageFilter< InputImageType, OutputImageType > Superclass;
    typedef SmartPointer< Self >                              Pointer;
    typedef SmartPointer< const Self >                        ConstPointer;

	TOutputImage* GetOutput0();
	TOutputImage* GetOutput1();

	QList<unsigned int>* GetNeighbors() { return mNeighbors; }
	QHash<unsigned int, SuperVoxelEdge<TInputImage>*>* GetSuperVoxelEdges() { return &mSuperVoxelEdges; }
	unsigned int GetMaximumLabel() { return mSettings->mMaximumLabel; }
    
	void SetUseStructureTensorAngleCriterion(const bool useStructureTensorCriterion) { mSettings.mUseStructureTensorCriterion = useStructureTensorCriterion; }
	void SetUseMinimumVolumeCriterion(const bool useMinimumVolumeCriterion) { mSettings.mUseMinimumVolumeCriterion = useMinimumVolumeCriterion; }
	void SetUseSphericityCriterion(const bool useSphericityCriterion) { mSettings.mUseSphericityCriterion = useSphericityCriterion; }
	void SetUseMeanRatioCriterion(const bool useMeanRatioCriterion) { mSettings.mUseMeanRatioCriterion = useMeanRatioCriterion; }
	void SetUseBoundaryCriterion(const bool useBoundaryCriterion) { mSettings.mUseBoundaryCriterion = useBoundaryCriterion; }
	void SetDisableMinimumVolumeCriterionOnBorder(const bool disableMinimumVolumeCriterionOnBorder) { mSettings.mDisableMinimumVolumeCriterionOnBorder = disableMinimumVolumeCriterionOnBorder; }
    void SetUseCNNCorrection(const bool useCNNCorrection) { mSettings.mCNNEnabled = useCNNCorrection; }
	void SetScaleByIntensity(const bool scaleByIntensity) { mSettings.mScaleByIntensity = scaleByIntensity;  }
	void SetUseProbabilityMapCriterion(const bool useProbabilityMapCriterion) { mSettings.mUseProbabilityMapCriterion = useProbabilityMapCriterion; }

	
	void SetGenerateEdgeMap(const bool generateEdgeMap) { mSettings.mGenerateEdgeMap = generateEdgeMap; }
	void ComputeEdgeFeaturesThread(int threadId);
	void UpdateEdgeFeaturesAfterMergeThread(int threadId);

	void PerformRegionMerge(SuperVoxelEdge<TInputImage>* edge);
	void PickBestBranches(SuperVoxel<TInputImage>* currentSuperVoxel);
	void TopDownLabelAssignment(SuperVoxel<TInputImage>* currentSuperVoxel);
	void GenerateConnectivityInformation();
	void ComputeMaskGradientImage();

	void SetInputMetaFilter(XPIWIT::MetaDataFilter* metaFilter) 
	{ 
		mRegionProps = metaFilter;
		mSettings.mVolumeFeatureIndex = mRegionProps->GetFeatureIndex("volume");
		mSettings.mIntensityFeatureIndex = mRegionProps->GetFeatureIndex("meanIntensity");
		mSettings.mXPosFeatureIndex = mRegionProps->GetFeatureIndex("xpos");
		mSettings.mYPosFeatureIndex = mRegionProps->GetFeatureIndex("ypos");
		mSettings.mZPosFeatureIndex = mRegionProps->GetFeatureIndex("zpos");
	}
	void SetOutputMetaFilter(XPIWIT::MetaDataFilter *metaFilter) { mOutputMetaFilter = metaFilter; }
	void SetIntensityImage(TInputImage* intensityImage) { mIntensityImage = intensityImage; }
	void SetVolumeConstraints(const unsigned int minimumVolume = 4000, const unsigned int maximumVolume = 8000) { mSettings.mMinimumVolume = minimumVolume; mSettings.mMaximumVolume = maximumVolume; }
	void SetMaximumAngle(const float maximumAngle) { mSettings.mMaximumAngle = maximumAngle;  }

	void ProcessMergeQueue();

    // Method for creation through the object factory.
    itkNewMacro(Self);

    // Run-time type information (and related methods).
    itkTypeMacro(WatershedBoundariesMergeTreeImageFilter, BoxImageFilter);

    // Image typedef support.
    typedef typename InputImageType::PixelType                 InputPixelType;
	typedef typename itk::Index<InputImageType::ImageDimension> InputIndexType;
    typedef typename OutputImageType::PixelType                OutputPixelType;
    typedef typename NumericTraits< InputPixelType >::RealType InputRealType;
	typedef typename itk::CovariantVector<float, InputImageType::ImageDimension> VectorType;
	typedef typename itk::Image<VectorType, InputImageType::ImageDimension> VectorImageType;

    typedef typename InputImageType::RegionType  InputImageRegionType;
    typedef typename OutputImageType::RegionType OutputImageRegionType;

    typedef typename InputImageType::SizeType InputSizeType;
    typedef typename itk::ZeroFluxNeumannBoundaryCondition<InputImageType, InputImageType> ZeroFluxNeumannBoundaryConditionType;
    typedef itk::NeighborhoodIterator< InputImageType, ZeroFluxNeumannBoundaryConditionType> NeighborhoodIteratorType;
    typedef typename NeighborhoodIteratorType::IndexType NeighborhoodIteratorIndexType;

#ifdef ITK_USE_CONCEPT_CHECKING
    /** Begin concept checking */
    itkConceptMacro( InputHasNumericTraitsCheck,
                     ( Concept::HasNumericTraits< InputPixelType > ) );
    /** End concept checking */
#endif
protected:
	WatershedBoundariesMergeTreeImageFilter(); //5000000, 8000
    virtual ~WatershedBoundariesMergeTreeImageFilter();

    /** ExtractKeyPointsImageFilter can be implemented as a multithreaded filter.
   * Therefore, this implementation provides a ThreadedGenerateData()
   * routine which is called for each processing thread. The output
   * image data is allocated automatically by the superclass prior to
   * calling ThreadedGenerateData().  ThreadedGenerateData can only
   * write to the portion of the output image specified by the
   * parameter "outputRegionForThread"
   *
   * \sa BoxImageFilter::ThreadedGenerateData(),
   *     BoxImageFilter::GenerateData() */
    void ThreadedGenerateData(const OutputImageRegionType & outputRegionForThread,
                              ThreadIdType threadId) override;


    /** functions for prepareing and post-processing the threaded generation */
    void BeforeThreadedGenerateData() override;
    void AfterThreadedGenerateData() override;

private:
    WatershedBoundariesMergeTreeImageFilter(const Self &); //purposely not implemented
    void operator=(const Self &);  //purposely not implemented
    
    SuperVoxelProperties<TInputImage> mSettings;

	QList<unsigned int> mInvalidIndices;
	QList<unsigned int>** mNeighborsTemp;
	QList<unsigned int>* mNeighbors;
	QList<bool>* mIsBorderSegmentTemp;
	QList<bool> mIsBorderSegment;
	QList<bool>* mIsSurfaceSegmentTemp;
	QList<bool> mIsSurfaceSegment;
	QHash<unsigned int, std::list<InputIndexType> >* mBorderIndicesTemp;
	QHash<unsigned int, std::list<InputIndexType> >* mRegionIndicesTemp;
	QHash<unsigned int, std::list<QPair<InputIndexType, float> > > mRegionIndices;
	    
    QHash<unsigned int, SuperVoxel<TInputImage>* > mSuperVoxels;
    QHash<unsigned int, SuperVoxelEdge<TInputImage>* > mSuperVoxelEdges;
    QHash<unsigned int, BasinEdge<TInputImage>* > mBasinEdges;
    
    QList<SuperVoxelEdge<TInputImage>* > mSuperVoxelEdgesTemp;
	QMap<float, SuperVoxelEdge<TInputImage>* > mMergeQueue;
    XPIWIT::MetaDataFilter* mRegionProps;
	XPIWIT::MetaDataFilter* mOutputMetaFilter;             // connectivity output

	TInputImage* mIntensityImage;
	typename VectorImageType::Pointer mMaskGradientImage;
	typename itk::Image<unsigned short, TInputImage::ImageDimension>::Pointer mMergeOrderImage;
	bool mGenerateMergeOrderImage;
	unsigned int mNumPerformedMerges;
};

} // end namespace itk

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkWatershedBoundariesMergeTreeImageFilter.txx"
#endif

#endif
