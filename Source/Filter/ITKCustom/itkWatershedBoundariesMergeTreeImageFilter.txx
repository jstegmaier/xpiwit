/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef __XPIWIT_WATERSHEDBOUNDARIESMERGETREEIMAGEFILTER_HXX
#define __XPIWIT_WATERSHEDBOUNDARIESMERGETREEIMAGEFILTER_HXX

// include required headers
#include "itkWatershedBoundariesMergeTreeImageFilter.h"
#include "itkConstNeighborhoodIterator.h"
#include "itkNeighborhoodInnerProduct.h"
#include "itkImageRegionIterator.h"
#include "itkNeighborhoodAlgorithm.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkGradientRecursiveGaussianImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkDanielssonDistanceMapImageFilter.h"
#include "itkOffset.h"
#include "itkProgressReporter.h"
#include "itkImageFileWriter.h"
#include <fstream>
#include <iostream>
#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include "../../Core/Utilities/Logger.h"

#ifdef XPIWIT_USE_OPENMP
#include <omp.h>
#else
#include <thread>
#endif

using namespace XPIWIT;

// namespace itk
namespace itk
{

// the default constructor
template< class TInputImage, class TOutputImage >
WatershedBoundariesMergeTreeImageFilter< TInputImage, TOutputImage >::WatershedBoundariesMergeTreeImageFilter()
{
    mRegionProps = NULL;
	mIntensityImage = NULL;
	mNumPerformedMerges = 0;
	mMergeOrderImage = NULL;
	mGenerateMergeOrderImage = false;
	mSettings.mCNNEnabled = false;
	mSettings.mDebugOutput = false;
	mSettings.mMinimumVolume = 0;
	mSettings.mMaximumVolume = 0;
	mSettings.mGenerateEdgeMap = false;
	mSettings.mUseMinimumVolumeCriterion = true;
	mSettings.mUseSphericityCriterion = true;
	mSettings.mUseMeanRatioCriterion = true;
	mSettings.mUseBoundaryCriterion = true;
	mSettings.mDisableMinimumVolumeCriterionOnBorder = false;

	this->SetNumberOfRequiredOutputs(2);
	this->SetNthOutput(0, this->MakeOutput(0));
	this->SetNthOutput(1, this->MakeOutput(1));
	this->DynamicMultiThreadingOff();
}

template< class TInputImage, class TOutputImage >
void WatershedBoundariesMergeTreeImageFilter< TInputImage, TOutputImage >::ComputeMaskGradientImage()
{
	// compute the mask gradient image
	mIntensityImage->SetReleaseDataFlag(false);

	/*
	// convert input image to mask image by setting all non-zero elements to 1
	typedef itk::BinaryThresholdImageFilter<TInputImage, TInputImage> BinaryThresholdImageFilterType;
	BinaryThresholdImageFilterType::Pointer thresholdFilter = BinaryThresholdImageFilterType::New();
	thresholdFilter->SetInput(mIntensityImage);
	thresholdFilter->SetLowerThreshold(0.0000001);
	thresholdFilter->SetUpperThreshold(65535);
	thresholdFilter->SetInsideValue(0);
	thresholdFilter->SetOutsideValue(1);

	// compute the distance map for the binary mask
	typedef itk::DanielssonDistanceMapImageFilter<TInputImage, TInputImage> DanielssonDistanceMapImageFilterType;
	DanielssonDistanceMapImageFilterType::Pointer distanceMapImageFilter = DanielssonDistanceMapImageFilterType::New();
	distanceMapImageFilter->SetInput(thresholdFilter->GetOutput());

	//  rescale the intensity
	typedef itk::RescaleIntensityImageFilter<TInputImage, TInputImage> RescaleFilterType;
	RescaleFilterType::Pointer rescaleFilter = RescaleFilterType::New();
	rescaleFilter->SetInput(distanceMapImageFilter->GetOutput());
	//rescaleFilter->SetInput(mIntensityImage);
	rescaleFilter->SetOutputMinimum(0);
	rescaleFilter->SetOutputMaximum(1);
	*/
	// compute the vector image
	typedef itk::GradientRecursiveGaussianImageFilter<TInputImage, VectorImageType> GradientFilterType;
	typename GradientFilterType::Pointer gradientFilter = GradientFilterType::New();
	//gradientFilter->SetInput(rescaleFilter->GetOutput());
	gradientFilter->SetInput(mIntensityImage);
	gradientFilter->SetReleaseDataFlag(false);

	//sigma is specified in millimeters
	gradientFilter->SetSigma(1.5);
	itkTryCatch(gradientFilter->Update(), "GradientRecursiveGaussianImageFilter: Update of the filter failed!")
	mMaskGradientImage = gradientFilter->GetOutput();
	mMaskGradientImage->SetReleaseDataFlag(false);

	/*
	typedef itk::ImageFileWriter< VectorImageType > WriterType;
	WriterType::Pointer writer = WriterType::New();
	writer->SetInput(gradientFilter->GetOutput());
	writer->SetFileName("D:/myresult.mhd");

	itkTryCatch(writer->Update(), "GradientRecursiveGaussianImageFilter: Update of the filter failed!")
	*/
}


// the destructor
template< class TInputImage, class TOutputImage >
WatershedBoundariesMergeTreeImageFilter< TInputImage, TOutputImage >::~WatershedBoundariesMergeTreeImageFilter()
{
	itk::ThreadIdType numThreads = this->GetNumberOfWorkUnits();
	for (unsigned int i = 0; i<numThreads; ++i)
		delete[] mNeighborsTemp[i];
	
	// TODO: find out why delete is so slow...
	return;

	delete[] mNeighborsTemp;
	delete[] mBorderIndicesTemp;
	delete[] mRegionIndicesTemp;
	delete[] mIsBorderSegmentTemp;
	delete[] mIsSurfaceSegmentTemp;
	delete[] mNeighbors;
	qDeleteAll(mBasinEdges);
	qDeleteAll(mSuperVoxelEdges);
	qDeleteAll(mSuperVoxels);
}


// returns pointer to the first output image
template< class TInputImage, class TOutputImage>
TOutputImage* WatershedBoundariesMergeTreeImageFilter< TInputImage, TOutputImage>::GetOutput0()
{
	return dynamic_cast< TOutputImage * >(this->ProcessObject::GetOutput(0));
}


// returns pointer to the second output image
template< class TInputImage, class TOutputImage>
TOutputImage* WatershedBoundariesMergeTreeImageFilter< TInputImage, TOutputImage>::GetOutput1()
{
	return dynamic_cast< TOutputImage * >(this->ProcessObject::GetOutput(1));
}


// before threaded generate data
template< class TInputImage, class TOutputImage >
void WatershedBoundariesMergeTreeImageFilter< TInputImage, TOutputImage >::BeforeThreadedGenerateData()
{
    // get the input
    typename TInputImage::ConstPointer input  = this->GetInput();

	// compute the maximum label of the input image
    typedef itk::MinimumMaximumImageCalculator<TInputImage> ImageCalculatorFilterType;
    typename ImageCalculatorFilterType::Pointer minimumMaximumCalculator = ImageCalculatorFilterType::New();
    minimumMaximumCalculator->SetImage(input);
    minimumMaximumCalculator->Compute();
	mSettings.mMaximumLabel = minimumMaximumCalculator->GetMaximum();

	// extract volumes from the region props that exceed the maximum volume
	const unsigned int numRegions = mRegionProps->mData.size();
	for (unsigned int i = 0; i < numRegions; ++i)
	{
		if (mRegionProps->mData[i][1] > mSettings.mMaximumVolume && mRegionProps->mData[i][0] > 0)
		{
			mInvalidIndices.append(mRegionProps->mData[i][0]);
			std::cout << "adding segment " << mRegionProps->mData[i][0] << " with volume " << mRegionProps->mData[i][1] << " to the invalid list!" << std::endl;
		}
	}
	
	if (mSettings.mDebugOutput == true)
		std::cout << "- Maximum label is " << mSettings.mMaximumLabel << std::endl;
	
	if (mSettings.mDebugOutput == true)
		std::cout << "Number of Threads is set to " << this->GetNumberOfWorkUnits() << std::endl;

    // initialize the seed array for each thread
    itk::ThreadIdType numThreads = this->GetNumberOfWorkUnits();
    mNeighborsTemp = new QList<unsigned int>*[numThreads];
    for (unsigned int i=0; i<numThreads; ++i)
        mNeighborsTemp[i] = new QList<unsigned int>[mSettings.mMaximumLabel + 1];

	// initialize the border indicator
	mIsBorderSegmentTemp = new QList<bool>[numThreads];
	mIsSurfaceSegmentTemp = new QList<bool>[numThreads];
	for (unsigned int i = 0; i < numThreads; ++i)
	{
		mIsBorderSegmentTemp[i].reserve(mSettings.mMaximumLabel+1);
		mIsSurfaceSegmentTemp[i].reserve(mSettings.mMaximumLabel + 1);
		for (unsigned int j = 0; j <= mSettings.mMaximumLabel; ++j)
		{
			mIsBorderSegmentTemp[i].append(false);
			mIsSurfaceSegmentTemp[i].append(false);
		}
	}

	// initialize the seed array for each thread
	mBorderIndicesTemp = new QHash<unsigned int, std::list<typename TInputImage::IndexType> >[numThreads];
	mRegionIndicesTemp = new QHash<unsigned int, std::list<typename TInputImage::IndexType> >[numThreads];

    // fill the output buffer with zeros
    typename OutputImageType::Pointer output = this->GetOutput();
    output->FillBuffer(1);

	
	if (mSettings.mDebugOutput == true)
		std::cout << "Finished before threaded generate data  " << std::endl;
}


// the thread generate data
template< class TInputImage, class TOutputImage >
void WatershedBoundariesMergeTreeImageFilter< TInputImage, TOutputImage >::ThreadedGenerateData(const OutputImageRegionType & outputRegionForThread, ThreadIdType threadId)
{
    // define input and output image pointers
    typename InputImageType::ConstPointer input  = this->GetInput();
    //typename OutputImageType::Pointer output = this->GetOutput();

	if (mSettings.mDebugOutput == true)
		std::cout << "Threaded generate data with thread " << threadId << std::endl;

    // specify the neighborhood by the provided radius
    typename NeighborhoodIteratorType::RadiusType radius;
    radius.Fill( 1 );

    // remove 3rd dimension of the neighborhood iterator for 2d detection.
    if (TInputImage::ImageDimension > 2 && input->GetLargestPossibleRegion().GetSize()[2] == 1)
        radius[2] = 0;

    // specify input and output iterators
    ConstNeighborhoodIterator< InputImageType > inputIterator( radius, input, outputRegionForThread );
    //ImageRegionIterator< OutputImageType > outputIterator( output, outputRegionForThread );

    // get the number of neighbours and the center pixel index
    unsigned int numNeighbors = inputIterator.Size();

    // reset the iterators
    inputIterator.GoToBegin();
    //outputIterator.GoToBegin();

    // loop trough the region
    unsigned int currentValue = 0;
    while ( !inputIterator.IsAtEnd() )
    {
        // get current value
        currentValue = static_cast< unsigned int >(std::round(inputIterator.GetCenterPixel()));
		//outputIterator.Set((((float)currentValue)+0.5) / 65535.0);

        // if value is non-zero skip calculations, as it's not a boundary pixel
		// TODO: Double check why it gets sooo slow upon adding the background
        if (currentValue > 0 && !mInvalidIndices.contains(currentValue))
        {
			bool isInBounds = true;
			for (unsigned int i = 0; i < numNeighbors; ++i)
			{
				// potential race condition probably not causing problems as it's only setting to true 
				unsigned int neighborValue = inputIterator.GetPixel(i, isInBounds);
				if (isInBounds == false)
				{
					mIsBorderSegmentTemp[threadId][currentValue] = true;
					break;
				}
			}

			mRegionIndicesTemp[threadId][currentValue].push_back(inputIterator.GetIndex());
            ++inputIterator;
            //++outputIterator;
            continue;
        }

        // identify all unique values within the current image patch
		std::vector<unsigned int> uniqueValues;
		bool isSurfaceVoxel = false;
        for (unsigned int i=0; i<numNeighbors; ++i)
        {
			// get the current neighbor label
            unsigned int currentNeighbor = static_cast< unsigned int >(std::round(inputIterator.GetPixel(i)));

			// check if the current neighbor is contained in the background voxels
			if (mInvalidIndices.contains(currentNeighbor))
				isSurfaceVoxel = true;

			// add the unique neighbors
            if (currentNeighbor > 0 && currentNeighbor != currentValue)
            {
                bool elementExists = false;
                for (unsigned int j=0; j<uniqueValues.size(); ++j)
                {
                    if (uniqueValues[j] == currentNeighbor)
                    {
                        elementExists = true;
                        break;
                    }
                }

                if (elementExists == false)
                    uniqueValues.push_back(static_cast< unsigned int >(inputIterator.GetPixel(i)));
            }
        }

		// sort the indices
		std::sort(uniqueValues.begin(), uniqueValues.end());
        if (uniqueValues.size() >= 2)
        {
			for (int i = 0; i < uniqueValues.size(); ++i)
			{
				if (isSurfaceVoxel == true)
					mIsSurfaceSegmentTemp[threadId][uniqueValues[i]] = true;

				for (int j = i+1; j < uniqueValues.size(); ++j)
				{
					if (i == j)
						continue;

					// added loops and changed 0, 1 to i, j
					mNeighborsTemp[threadId][uniqueValues[i]].push_back(uniqueValues[j]);
					mNeighborsTemp[threadId][uniqueValues[j]].push_back(uniqueValues[i]);
					mBorderIndicesTemp[threadId][mSettings.ConvertSegmentIdsToArrayIndex(uniqueValues[i], uniqueValues[j])].push_back(inputIterator.GetIndex());
				}
			}
        }

        // increase the iterators
        ++inputIterator;
        //++outputIterator;
    }

	std::cout << "Done processing thread " << threadId << std::endl;
}


template< class TInputImage, class TOutputImage >
void WatershedBoundariesMergeTreeImageFilter< TInputImage, TOutputImage >::UpdateEdgeFeaturesAfterMergeThread(int threadId)
{
	itk::ThreadIdType numThreads = this->GetNumberOfWorkUnits();
	unsigned int stepSize = std::floor((float)mSuperVoxelEdgesTemp.size() / (float)numThreads);
	unsigned int startIndex = (threadId*stepSize);
	unsigned int endIndex = ((threadId + 1)*stepSize)-1;

	if (endIndex < 0 || startIndex >= mSuperVoxelEdgesTemp.size())
		return;

	if (threadId == (numThreads - 1))
		endIndex = mSuperVoxelEdgesTemp.size();

	if (mSettings.mDebugOutput == true)
		std::cout << "Thread " << threadId << " used to process range " << startIndex << " - " << endIndex << " with step size " << stepSize << "/" << mSuperVoxelEdgesTemp.size() << std::endl;
	
	// add all edges to the merge queue (implemented as a QMap)
	for (int i = startIndex; i < endIndex; ++i)
	{
		if (i < 0 || i >= mSuperVoxelEdgesTemp.size())
			std::cout << "Index out of bounds!!!" << std::endl;

		if (mSuperVoxelEdgesTemp[i] != NULL)
			mSuperVoxelEdgesTemp[i]->ComputeEdgeFeatures();
	}
}

template< class TInputImage, class TOutputImage >
void WatershedBoundariesMergeTreeImageFilter< TInputImage, TOutputImage >::ComputeEdgeFeaturesThread(int threadId)
{
	itk::ThreadIdType numThreads = this->GetNumberOfWorkUnits();
	int startIndex = (threadId) * mSuperVoxelEdges.size() / numThreads;
	int endIndex = (threadId + 1) * mSuperVoxelEdges.size() / numThreads;

	if (mSettings.mDebugOutput == true)
		std::cout << "Processing range " << startIndex << " to " << endIndex << " on thread " << threadId << std::endl;

	typename QHash<unsigned int, SuperVoxelEdge<TInputImage>* >::iterator i;
	for (i = mSuperVoxelEdges.begin() + startIndex; i != (mSuperVoxelEdges.begin() + endIndex); ++i)
		(*i)->ComputeEdgeFeatures();
}

template< class TInputImage, class TOutputImage >
void WatershedBoundariesMergeTreeImageFilter< TInputImage, TOutputImage >::PerformRegionMerge(SuperVoxelEdge<TInputImage>* edge)
{
	mNumPerformedMerges++;
	unsigned int newSuperVoxelId = mSuperVoxels.size()+1;
	mSuperVoxels[newSuperVoxelId] = new SuperVoxel<TInputImage>(newSuperVoxelId, edge->GetSegment1(), edge->GetSegment2(), &mSettings);
	mSuperVoxels[newSuperVoxelId]->UpdateProperties();

	//SuperVoxel<TInputImage>* currentSuperVoxel = mSuperVoxels[newSuperVoxelId];

	if (mSettings.mDebugOutput == true)
	{
		std::cout << "MERGING: " << std::endl;
		mSuperVoxels[newSuperVoxelId]->PrintFeatures();
	}

	QList<unsigned int>* currentBasinEdges = edge->GetBasinEdges();
	for (int i = 0; i < currentBasinEdges->size(); ++i)
	{
		if (!mBasinEdges.contains((*currentBasinEdges)[i]))
			continue;

		mBasinEdges[(*currentBasinEdges)[i]]->SetSortFeature(0);
	}

	// print the current edge to the merge order image
	if (mGenerateMergeOrderImage == true)
	{
		QList<unsigned int>* currentBasinEdges = edge->GetBasinEdges();
		for (int i = 0; i<currentBasinEdges->size(); ++i)
		{
			if (!mBasinEdges.contains((*currentBasinEdges)[i]))
				continue;

			std::list<typename TInputImage::IndexType>* borderIndices = mBasinEdges[(*currentBasinEdges)[i]]->GetBorderIndices();
			typename std::list<typename TInputImage::IndexType>::iterator j;
			for (j = borderIndices->begin(); j != borderIndices->end(); ++j)
				mMergeOrderImage->SetPixel(*j, mNumPerformedMerges);
		}
	}

	// 
	int removedItems = 0;
	typename QMap<float, SuperVoxelEdge<TInputImage>* >::iterator i = mMergeQueue.end();
	QList<SuperVoxelEdge<TInputImage>*> newSuperVoxelEdges;
	while (i != mMergeQueue.begin())
	{
		--i;
		if (mMergeQueue.size() == 0)
			break;

		if ((*i)->GetSegment1() == edge->GetSegment1() ||
			(*i)->GetSegment1() == edge->GetSegment2() ||
			(*i)->GetSegment2() == edge->GetSegment1() ||
			(*i)->GetSegment2() == edge->GetSegment2())
		{
			// increment the removed items counter
			removedItems++;
				
			// temporarily store the neighboring super voxels
			if ((*i)->GetSegment1() == edge->GetSegment1() ||
				(*i)->GetSegment1() == edge->GetSegment2())
			{
				if (!mSuperVoxels[newSuperVoxelId]->IsInternalSuperVoxel((*i)->GetSegment2()))
					newSuperVoxelEdges.append(new SuperVoxelEdge<TInputImage>(mSuperVoxels[newSuperVoxelId], (*i)->GetSegment2(), &mSettings));
			}
			else
			{
				if (!mSuperVoxels[newSuperVoxelId]->IsInternalSuperVoxel((*i)->GetSegment1()))
					newSuperVoxelEdges.append(new SuperVoxelEdge<TInputImage>(mSuperVoxels[newSuperVoxelId], (*i)->GetSegment1(), &mSettings));
			}

			// remove processed edge from the list
			i = mMergeQueue.erase(i);
		}
	}

	for (int i=0; i<newSuperVoxelEdges.size(); ++i)
	{
        float currentSortFeature = newSuperVoxelEdges[i]->GetSortFeature();
                
        if (mMergeQueue.contains(currentSortFeature) && currentSortFeature < 1.0)
        {
			if (mSettings.mDebugOutput == true)
			{
				std::cout << "in performregionmerge: trying to resort key: " << currentSortFeature << std::endl;
				newSuperVoxelEdges[i]->PrintFeatures();
			}

            QMap<unsigned int, SuperVoxelEdge<TInputImage>*> tempMap;
            typename QMap<float, SuperVoxelEdge<TInputImage>*>::iterator currentKeyIterator;
            
            tempMap.insert(newSuperVoxelEdges[i]->GetId(), newSuperVoxelEdges[i]);
            
            for (currentKeyIterator = mMergeQueue.lowerBound(currentSortFeature); currentKeyIterator != mMergeQueue.upperBound(currentSortFeature); ++currentKeyIterator)
                tempMap.insert((*currentKeyIterator)->GetId(), (*currentKeyIterator));
            
            mMergeQueue.remove(currentSortFeature);
            typename QMap<unsigned int, SuperVoxelEdge<TInputImage>*>::iterator tempMapIterator;
            for (tempMapIterator=tempMap.begin(); tempMapIterator != tempMap.end(); ++tempMapIterator)
                mMergeQueue.insertMulti(mMergeQueue.lowerBound(currentSortFeature), (*tempMapIterator)->GetSortFeature(), (*tempMapIterator));
        }
        else
        {
            mMergeQueue.insertMulti(currentSortFeature, newSuperVoxelEdges[i]);
        }

		if (mSettings.mDebugOutput == true)
			newSuperVoxelEdges[i]->PrintFeatures();
	}
}

template< class TInputImage, class TOutputImage >
void WatershedBoundariesMergeTreeImageFilter< TInputImage, TOutputImage >::PickBestBranches(SuperVoxel<TInputImage>* currentSuperVoxel)
{
	
	QList<SuperVoxel<TInputImage>*>* superVoxelList = currentSuperVoxel->GetSuperVoxelList(NULL);
	
	unsigned int numValidSuperVoxels = superVoxelList->size();

	while (numValidSuperVoxels > 0)
	{
		double maxProbability = -1;
		SuperVoxel<TInputImage>* maxScoringSuperVoxel = NULL;

		typename QList<SuperVoxel<TInputImage>*>::iterator superVoxelIterator;
		for (superVoxelIterator = superVoxelList->begin(); superVoxelIterator != superVoxelList->end(); ++superVoxelIterator)
		{
			if ((*superVoxelIterator)->GetSelectionState() > 0)
				continue;

			double currentProbability = (*superVoxelIterator)->GetSegmentPotential();

			if (currentProbability > maxProbability)
			{
				maxProbability = currentProbability;
				maxScoringSuperVoxel = (*superVoxelIterator);
			}

			std::cout << "SuperVoxel " << (*superVoxelIterator)->GetId() << " has a probability of " << currentProbability << " and is in state " << (*superVoxelIterator)->GetSelectionState() << "(" << (*superVoxelIterator)->GetVolumeBorder() << ", " << (*superVoxelIterator)->GetVolumeInternal() << ")" << std::endl;
		}

		std::cout << "Picking SuperVoxel " << maxScoringSuperVoxel->GetId() << " for the merge." << std::endl;
		maxScoringSuperVoxel->Select();
		numValidSuperVoxels = 0;
		for (superVoxelIterator = superVoxelList->begin(); superVoxelIterator != superVoxelList->end(); ++superVoxelIterator)
		{
			std::cout << "SuperVoxel " << (*superVoxelIterator)->GetId() << " is now in state " << (*superVoxelIterator)->GetSelectionState() << std::endl;

			if ((*superVoxelIterator)->GetSelectionState() == 0)
				numValidSuperVoxels++;
		}
	}
	
	std::cout << "----------------------" << std::endl;

	delete superVoxelList;
}

template< class TInputImage, class TOutputImage >
void WatershedBoundariesMergeTreeImageFilter< TInputImage, TOutputImage >::TopDownLabelAssignment(SuperVoxel<TInputImage>* currentSuperVoxel)
{
	//if (currentSuperVoxel->GetCellProbability() > currentSuperVoxel->GetUnderSegmentationProbability() || 
	//	currentSuperVoxel->GetOverSegmentationProbability() > currentSuperVoxel->GetUnderSegmentationProbability() ||
	//	((currentSuperVoxel->GetLeftSuperVoxel() != NULL && currentSuperVoxel->GetLeftSuperVoxel()->GetOverSegmentationProbability() > currentSuperVoxel->GetLeftSuperVoxel()->GetCellProbability()) ||
	//	(currentSuperVoxel->GetRightSuperVoxel() != NULL && currentSuperVoxel->GetRightSuperVoxel()->GetOverSegmentationProbability() > currentSuperVoxel->GetRightSuperVoxel()->GetCellProbability())))
	//	(currentSuperVoxel->GetRightSuperVoxel() != NULL && currentSuperVoxel->GetRightSuperVoxel()->GetVolumeInternal() < mSettings.mMinimumVolume))
	if (currentSuperVoxel->GetCellProbability() > currentSuperVoxel->GetUnderSegmentationProbability() ||
		currentSuperVoxel->GetOverSegmentationProbability() > currentSuperVoxel->GetUnderSegmentationProbability() ||
		currentSuperVoxel->IsLeafNode())
	{
		currentSuperVoxel->Select();
		if (currentSuperVoxel->IsLeafNode())
			std::cout << "Picking LeafNode SuperVoxel " << currentSuperVoxel->GetId() << " for the merge (cell: " << currentSuperVoxel->GetCellProbability() << ", overSeg: " << currentSuperVoxel->GetOverSegmentationProbability() << ", underSeg: " << currentSuperVoxel->GetUnderSegmentationProbability() << ")." << std::endl;
		else
			std::cout << "Picking SuperVoxel " << currentSuperVoxel->GetId() << " for the merge (cell: " << currentSuperVoxel->GetCellProbability() << ", overSeg: " << currentSuperVoxel->GetOverSegmentationProbability() << ", underSeg: " << currentSuperVoxel->GetUnderSegmentationProbability() << ")." << std::endl;
	}
	else
	{
		std::cout << "Not Picking SuperVoxel " << currentSuperVoxel->GetId() << " for the merge -> trying children (cell: " << currentSuperVoxel->GetCellProbability() << ", overSeg: " << currentSuperVoxel->GetOverSegmentationProbability() << ", underSeg: " << currentSuperVoxel->GetUnderSegmentationProbability() << ")." << std::endl;

		if (currentSuperVoxel->GetLeftSuperVoxel() != NULL)
			TopDownLabelAssignment(currentSuperVoxel->GetLeftSuperVoxel());
		
		if (currentSuperVoxel->GetRightSuperVoxel() != NULL)
			TopDownLabelAssignment(currentSuperVoxel->GetRightSuperVoxel());
	}
}


// process the merge queue
template< class TInputImage, class TOutputImage >
void WatershedBoundariesMergeTreeImageFilter< TInputImage, TOutputImage >::ProcessMergeQueue()
{
	// merge counter
	int mergeCounter = 0;
	bool debugMergeQueue = false;

	// get the first edge and process the merge queue
	SuperVoxelEdge<TInputImage>* currentEdge = mMergeQueue.first();
	while (mMergeQueue.size() > 0 && currentEdge->GetSortFeature() < 0.99)
	{
		if (mSettings.mDebugOutput == true)
		{
			std::cout << "----------------------------------------------------------------------------" << std::endl;
			std::cout << "New merge attempt started ..." << std::endl;
			std::cout << "Processed Edge: ";
			currentEdge->PrintFeatures();
		}
        
		// update region props and all other edges that are affected by the merge
		if (currentEdge->GetVolumeCombined() <= mSettings.mMaximumVolume)
            PerformRegionMerge(currentEdge);
		else
        	mMergeQueue.erase(mMergeQueue.begin());

		////////////////////
		if (debugMergeQueue == true)
		{
			typename OutputImageType::Pointer output0 = this->GetOutput0();

			// fill the output image with black values
			output0->FillBuffer(0);




			// add all edges to the merge queue (implemented as a QMap)
			typename QHash<unsigned int, SuperVoxelEdge<TInputImage>* >::iterator edgeIterator;
			for (edgeIterator = mSuperVoxelEdges.begin(); edgeIterator != (mSuperVoxelEdges.end()); ++edgeIterator)
			{
				if (mSettings.mDebugOutput == true)
					(*edgeIterator)->PrintFeatures();

				QList<unsigned int>* currentBasinEdges = (*edgeIterator)->GetBasinEdges();

				for (int i = 0; i < currentBasinEdges->size(); ++i)
				{
					if (!mBasinEdges.contains((*currentBasinEdges)[i]))
					{
						continue;
					}

					std::list<typename TInputImage::IndexType>* borderIndices = mBasinEdges[(*currentBasinEdges)[i]]->GetBorderIndices();
					typename std::list<typename TInputImage::IndexType>::iterator j;
					for (j = borderIndices->begin(); j != borderIndices->end(); ++j)
						output0->SetPixel(*j, std::min<float>(1.0f, mBasinEdges[(*currentBasinEdges)[i]]->GetSortFeature()));
				}
			}

			/*
			//typename QHash<unsigned int, SuperVoxel<TInputImage>* >::iterator superVoxelIterator;
			unsigned int currentId = 2;
			typename QHash<unsigned int, SuperVoxel<TInputImage>* >::iterator superVoxelIterator;
			for (superVoxelIterator = mSuperVoxels.begin(); superVoxelIterator != mSuperVoxels.end(); ++superVoxelIterator)
			{
				// print all super voxels if debug output is enabled
				if (mSettings.mDebugOutput == true)
					(*superVoxelIterator)->PrintFeatures();

				// skip all nodes that are not in the selection state
				if (!(*superVoxelIterator)->IsRootNode())
					continue;

				// paint the super voxel including the internal watershed lines to the final image
				//(*superVoxelIterator)->PaintSuperVoxel(output, std::min<float>(1.0f, (float)(*superVoxelIterator)->GetId() / 65535.0f));
				//(*superVoxelIterator)->PaintSuperVoxel(output, std::min<float>(1.0f, ((float)currentId)));

				if ((*superVoxelIterator)->GetVolumeInternal() <= mSettings.mMaximumVolume)
				{
					(*superVoxelIterator)->PaintSuperVoxel(output0, (float)(*superVoxelIterator)->GetId(), false, true);

					++currentId;
				}
			}
			*/

			typedef itk::ImageFileWriter< typename itk::Image< typename TInputImage::PixelType, TInputImage::ImageDimension> > WriterType;
			typename WriterType::Pointer writer = WriterType::New();
			//writer->SetFileName(QString().asprintf("D:\\MergeOrderImages\\MergeOrderImage_m=%04d.tif", mergeCounter).toStdString().c_str());
            writer->SetFileName(QString().asprintf("/Users/jstegmaier/Desktop/Processing/MergeOrderImages/MergeOrderImage_m=%04d.tif", mergeCounter).toStdString().c_str());
            
			writer->SetInput(output0);
			writer->Update();

			mergeCounter++;
		}
		////////////////////

		// get the next object from the merge queue
		if (mMergeQueue.size() > 0)
		{
			currentEdge = mMergeQueue.first();
			if (mSettings.mDebugOutput == true)
				currentEdge->PrintFeatures();
		}
    }
}

// after threaded generate data
template< class TInputImage, class TOutputImage >
void WatershedBoundariesMergeTreeImageFilter< TInputImage, TOutputImage >::AfterThreadedGenerateData()
{
    // get the image pointers to input and output
    typename InputImageType::ConstPointer input  = this->GetInput();
    typename OutputImageType::Pointer output0 = this->GetOutput0();
	typename OutputImageType::Pointer output1 = this->GetOutput1();
	
	// fill the output image with black values
	output0->FillBuffer(1);
	output1->FillBuffer(1);

	// get the number of threads and initialize the neighbors array
    itk::ThreadIdType numThreads = this->GetNumberOfWorkUnits();
    mNeighbors = new QList<unsigned int>[mSettings.mMaximumLabel + 1];

	// initialize the border segment variable, an indicator if a segment touches the border
	mIsBorderSegment.reserve(mSettings.mMaximumLabel+1);
	mIsSurfaceSegment.reserve(mSettings.mMaximumLabel + 1);
	for (unsigned int i = 0; i <= mSettings.mMaximumLabel; ++i)
	{
		mIsBorderSegment.append(false);
		mIsSurfaceSegment.append(false);
	}
	
	// combine the separately computed neighbors to a single neighbor variable
    for (unsigned int i=0; i<numThreads; ++i)
    {
        for (unsigned int j=0; j<(mSettings.mMaximumLabel + 1); ++j)
        {
			mIsBorderSegment[j] = mIsBorderSegment[j] || mIsBorderSegmentTemp[i][j];
			mIsSurfaceSegment[j] = mIsSurfaceSegment[j] || mIsSurfaceSegmentTemp[i][j];

            for (int k=0; k<mNeighborsTemp[i][j].size(); ++k)
            {
                if (mNeighbors[j].contains(mNeighborsTemp[i][j][k]) == true || j == mNeighborsTemp[i][j][k]) // || mRegionProps->mData[mNeighborsTemp[i][j][k]][mSettings.mVolumeFeatureIndex] > mSettings.mMaximumVolume || mRegionProps->mData[j][mSettings.mVolumeFeatureIndex] > mSettings.mMaximumVolume)
                    continue;
                
				mNeighbors[j].push_back(mNeighborsTemp[i][j][k]);
            }
        }
    }

	// compute mask gradient image
	ComputeMaskGradientImage();
	mSettings.mMaskGradientImage = mMaskGradientImage;

    // set the super voxel properties
    mSettings.mRegionProps = mRegionProps;
    mSettings.mIntensityImage = mIntensityImage;
    mSettings.mNeighbors = &mNeighbors;
	mSettings.mIsBorderSegment = &mIsBorderSegment;
	mSettings.mIsSurfaceSegment = &mIsSurfaceSegment;
    mSettings.mSuperVoxels = &mSuperVoxels;
    mSettings.mSuperVoxelEdges = &mSuperVoxelEdges;

    // add all initial super voxels
    mSuperVoxels.reserve(mSettings.mMaximumLabel+1);
    for (unsigned int i=1; i<(mSettings.mMaximumLabel+1); ++i)
        mSuperVoxels[i] = new SuperVoxel<TInputImage>(i, &mSettings);
    
	// combine all voxels that belong to a common edge
	for (unsigned int i = 0; i<numThreads; ++i)
	{
		typename QHash<unsigned int, std::list<InputIndexType> >::iterator j;
		for (j = mRegionIndicesTemp[i].begin(); j != mRegionIndicesTemp[i].end(); ++j)
		{
			typename std::list<typename TInputImage::IndexType>::iterator k;
			for (k = (*j).begin(); k != (*j).end(); ++k)
			{
				if (!mRegionIndices.contains(j.key()))
					mRegionIndices[j.key()] = std::list<QPair<InputIndexType, float> >();

				mRegionIndices[j.key()].push_back(QPair<InputIndexType, float>(*k, mIntensityImage->GetPixel(*k)));
				mSuperVoxels[j.key()]->AddVoxel(*k, mIntensityImage->GetPixel(*k));
			}
		}

		// iterate over the border pixels of the current thread
		for (j = mBorderIndicesTemp[i].begin(); j != mBorderIndicesTemp[i].end(); ++j)
		{
			typename std::list<typename TInputImage::IndexType>::iterator k;
			for (k = (*j).begin(); k != (*j).end(); ++k)
			{
				if (!mBasinEdges.contains(j.key()))
                {
                    mBasinEdges[j.key()] = new itk::BasinEdge<TInputImage>(mSettings.ConvertArrayIndexToIndex1(j.key()), mSettings.ConvertArrayIndexToIndex2(j.key()), &mSettings);
					if (mSettings.mDebugOutput == true)
						std::cout << "Adding basin edge " << j.key() << " between " << mSettings.ConvertArrayIndexToIndex1(j.key()) << " and " << mSettings.ConvertArrayIndexToIndex2(j.key()) << std::endl;
                }
                
				if ((mRegionProps->mData[mSettings.ConvertArrayIndexToIndex1(j.key())][mSettings.mVolumeFeatureIndex] + mRegionProps->mData[mSettings.ConvertArrayIndexToIndex2(j.key())][mSettings.mVolumeFeatureIndex]) <= mSettings.mMaximumVolume)
				{
					if (!mSuperVoxelEdges.contains(j.key()))
					{
						mSuperVoxelEdges[j.key()] = new itk::SuperVoxelEdge<TInputImage>(mSuperVoxels[mSettings.ConvertArrayIndexToIndex1(j.key())], mSuperVoxels[mSettings.ConvertArrayIndexToIndex2(j.key())], &mSettings);
						if (mSettings.mDebugOutput == true)
							std::cout << "Adding edge between " << mSettings.ConvertArrayIndexToIndex1(j.key()) << " and " << mSettings.ConvertArrayIndexToIndex2(j.key()) << std::endl;
					}
				}
				
				// add current voxel to the basin edges
				if (mIntensityImage != NULL)
					mBasinEdges[j.key()]->AddVoxel(*k, mIntensityImage->GetPixel(*k));
				else
					mBasinEdges[j.key()]->AddVoxel(*k, 0);
			}
		}
	}

	// set pointer to the basin edges
	mSettings.mBasinEdges = &mBasinEdges;
	if (mSettings.mDebugOutput == true)
		std::cout << "Num BasinEdges " << mSettings.mBasinEdges->size() << std::endl;
    
	// compute the edge features in parallel using std threads
	if (mSettings.mDebugOutput == true)
		std::cout << "Computing edge features using " << numThreads << " threads ..." << std::endl;

#ifdef XPIWIT_USE_OPENMP
	std::cout << "Using OpenMP for threading ..." << std::endl;
	
	// set the number of threads to the specified value
	if (numThreads > 0)
		omp_set_num_threads(numThreads);

	#pragma omp parallel
	{
		int currentThread = omp_get_thread_num();
		int numThreads = omp_get_num_threads();
		int startIndex = (currentThread)* mBasinEdges.size() / numThreads;
		int endIndex = (currentThread + 1) * mBasinEdges.size() / numThreads;

		typename QHash<unsigned int, BasinEdge<TInputImage>* >::iterator j;
		for (j = mBasinEdges.begin() + startIndex; j != (mBasinEdges.begin() + endIndex); ++j)
			(*j)->UpdateBasinEdgeFeatures();

		#pragma omp critical
		// Code inside this region runs in parallel.
		std::cout << "Processing range " << startIndex << " to " << endIndex << " on thread " << currentThread << std::endl;
	}

	#pragma omp parallel
	{
		int currentThread = omp_get_thread_num();
		int numThreads = omp_get_num_threads();
		int startIndex = (currentThread)* mSuperVoxels.size() / numThreads;
		int endIndex = (currentThread + 1) * mSuperVoxels.size() / numThreads;

		typename QHash<unsigned int, SuperVoxel<TInputImage>* >::iterator j;
		for (j = mSuperVoxels.begin() + startIndex; j != (mSuperVoxels.begin() + endIndex); ++j)
			(*j)->UpdateProperties();

		#pragma omp critical
		// Code inside this region runs in parallel.
		std::cout << "Processing range " << startIndex << " to " << endIndex << " on thread " << currentThread << std::endl;
	}

	#pragma omp parallel
	{
		int currentThread = omp_get_thread_num();
		int numThreads = omp_get_num_threads();
		int startIndex = (currentThread) * mSuperVoxelEdges.size() / numThreads;
		int endIndex = (currentThread + 1) * mSuperVoxelEdges.size() / numThreads;
	
		typename QHash<unsigned int, SuperVoxelEdge<TInputImage>* >::iterator j;
		for (j = mSuperVoxelEdges.begin() + startIndex; j != (mSuperVoxelEdges.begin()+endIndex); ++j)
			(*j)->ComputeEdgeFeatures();

		// Code inside this region runs in parallel.
		#pragma omp critical
		std::cout << "Processing range " << startIndex << " to " << endIndex <<" on thread " << currentThread << std::endl;
	}
    
    typename QHash<unsigned int, SuperVoxelEdge<TInputImage>* >::iterator superVoxelEdgeIterator;
#else
    std::cout << "Not using threads. Activate OpenMP in the CMake project to enable threading ..." << std::endl;

    // update the basin edges
    typename QHash<unsigned int, BasinEdge<TInputImage>* >::iterator basinEdgeIterator;
    for (basinEdgeIterator = mBasinEdges.begin(); basinEdgeIterator!=mBasinEdges.end(); ++basinEdgeIterator)
        (*basinEdgeIterator)->UpdateBasinEdgeFeatures();
    
    // update the super voxel properties
    typename QHash<unsigned int, SuperVoxel<TInputImage>* >::iterator superVoxelIterator;
    for (superVoxelIterator = mSuperVoxels.begin(); superVoxelIterator != mSuperVoxels.end(); ++superVoxelIterator)
        (*superVoxelIterator)->UpdateProperties();

    // update the super voxel edge features
    typename QHash<unsigned int, SuperVoxelEdge<TInputImage>* >::iterator superVoxelEdgeIterator;
    for (superVoxelEdgeIterator = mSuperVoxelEdges.begin(); superVoxelEdgeIterator != mSuperVoxelEdges.end(); ++superVoxelEdgeIterator)
        (*superVoxelEdgeIterator)->ComputeEdgeFeatures();
    
    std::cout << "Finished updating edge features without using threads ... " << std::endl;
#endif

	// add all edges to the merge queue (implemented as a QMap)
	for (superVoxelEdgeIterator = mSuperVoxelEdges.begin(); superVoxelEdgeIterator != mSuperVoxelEdges.end(); ++superVoxelEdgeIterator)
    {
        float currentSortFeature = (*superVoxelEdgeIterator)->GetSortFeature();
        
        if (mMergeQueue.contains(currentSortFeature) && currentSortFeature < 1.0)
        {
			if (mSettings.mDebugOutput == true)
			{
				std::cout << "trying to find location for supervoxeledge: " << (*superVoxelEdgeIterator)->GetId() << std::endl;
				(*superVoxelEdgeIterator)->PrintFeatures();
			}
            
            QMap<unsigned int, SuperVoxelEdge<TInputImage>*> tempMap;
            typename QMap<float, SuperVoxelEdge<TInputImage>*>::iterator currentKeyIterator;
            
            tempMap.insert((*superVoxelEdgeIterator)->GetId(), (*superVoxelEdgeIterator));
            
            for (currentKeyIterator = mMergeQueue.lowerBound(currentSortFeature); currentKeyIterator != mMergeQueue.upperBound(currentSortFeature); ++currentKeyIterator)
                tempMap.insert((*currentKeyIterator)->GetId(), (*currentKeyIterator));
            
            mMergeQueue.remove(currentSortFeature);
            typename QMap<unsigned int, SuperVoxelEdge<TInputImage>*>::iterator tempMapIterator;
            for (tempMapIterator=tempMap.begin(); tempMapIterator != tempMap.end(); ++tempMapIterator)
                mMergeQueue.insertMulti(mMergeQueue.lowerBound(currentSortFeature), (*tempMapIterator)->GetSortFeature(), (*tempMapIterator));
        }
        else
        {
            mMergeQueue.insertMulti(currentSortFeature, (*superVoxelEdgeIterator));
        }
    }
    
	// process the merge queue for the final segmentation
	if (mSettings.mGenerateEdgeMap == true)
	{
		// fill the output image with black values
		output0->FillBuffer(0);
        
        // add all edges to the merge queue (implemented as a QMap)
        typename QHash<unsigned int, SuperVoxelEdge<TInputImage>* >::iterator edgeIterator;
        for (edgeIterator = mSuperVoxelEdges.begin(); edgeIterator != (mSuperVoxelEdges.end()); ++edgeIterator)
        {
			if (mSettings.mDebugOutput == true)
				(*edgeIterator)->PrintFeatures();
            
            QList<unsigned int>* currentBasinEdges = (*edgeIterator)->GetBasinEdges();
            
            for (int i=0; i<currentBasinEdges->size(); ++i)
            {
                if (!mBasinEdges.contains((*currentBasinEdges)[i]))
                {
                    continue;
                }
                
                std::list<typename TInputImage::IndexType>* borderIndices = mBasinEdges[(*currentBasinEdges)[i]]->GetBorderIndices();
                typename std::list<typename TInputImage::IndexType>::iterator j;
                for (j = borderIndices->begin(); j != borderIndices->end(); ++j)
                    output0->SetPixel(*j, std::min<float>(1.0f, (*edgeIterator)->GetSortFeature()));
            }
        }
	}
	else
	{
		// if enabled, initialize the merge order image
		if (mGenerateMergeOrderImage == true)
		{
			mMergeOrderImage = itk::Image<unsigned short, TInputImage::ImageDimension>::New();
			mMergeOrderImage->SetRegions(output0->GetLargestPossibleRegion());
			mMergeOrderImage->Allocate();
			mMergeOrderImage->FillBuffer(0);
			mMergeOrderImage->SetReleaseDataFlag(true);
		}

		// add all edges to the merge queue (implemented as a QMap)
		typename QHash<unsigned int, SuperVoxelEdge<TInputImage>* >::iterator edgeIterator;
		for (edgeIterator = mSuperVoxelEdges.begin(); edgeIterator != (mSuperVoxelEdges.end()); ++edgeIterator)
		{
			QList<unsigned int>* currentBasinEdges = (*edgeIterator)->GetBasinEdges();

			for (int i = 0; i < currentBasinEdges->size(); ++i)
			{
				if (!mBasinEdges.contains((*currentBasinEdges)[i]))
					continue;

				if (mGenerateMergeOrderImage == true)
				{
					std::list<typename TInputImage::IndexType>* borderIndices = mBasinEdges[(*currentBasinEdges)[i]]->GetBorderIndices();
					typename std::list<typename TInputImage::IndexType>::iterator j;
					for (j = borderIndices->begin(); j != borderIndices->end(); ++j)
						mMergeOrderImage->SetPixel(*j, mSuperVoxelEdges.count());
				}
			}
		}

		// process the merge queue
		ProcessMergeQueue();

		// write the merge order image to disk if enabled
		if (mGenerateMergeOrderImage == true)
		{
			typedef  itk::ImageFileWriter< typename itk::Image<unsigned short, TInputImage::ImageDimension> > WriterType;
			typename WriterType::Pointer writer = WriterType::New();
			writer->SetFileName("E:\\Projects\\2017\\CNNTraining\\Processing\\item_0013_WatershedBoundariesMergeTreeFilter\\MergeOrderImage.tif");
			writer->SetInput(mMergeOrderImage);
			writer->Update();
		}

		// declare the super voxel iterator running over all existing super voxels
		typename QHash<unsigned int, SuperVoxel<TInputImage>* >::iterator superVoxelIterator;
		
		// if the CNN correction heuristic is enabled, write snippets to disk and classify
		if (mSettings.mCNNEnabled == true)
		{
			// run through all super voxels and write the snippets to the temp folder
			for (superVoxelIterator = mSuperVoxels.begin(); superVoxelIterator != mSuperVoxels.end(); ++superVoxelIterator)
				(*superVoxelIterator)->GenerateSuperVoxelImage();

			// run the python script for super voxel classification
			QString cnnCommand = "python ";
			cnnCommand += mSettings.mModelFolder;
			cnnCommand += "RACE3DSimpleSoftmaxInference.py ";
			cnnCommand += "\"" + mSettings.mTempFolder + "Images/\" ";
			cnnCommand += "\"" + mSettings.mModelFolder + "\" ";
			cnnCommand += "\"" + mSettings.mTempFolder + "Probabilities/" + "\"";
			
			std::cout << "- Applying CNN-based super voxel classification with the following command: " << std::endl;
			std::cout << cnnCommand.toStdString().data() << std::endl;

			system(cnnCommand.toStdString().data());
		}

		// perform the final node selection in a top-down fashion
		for (superVoxelIterator = mSuperVoxels.begin(); superVoxelIterator != mSuperVoxels.end(); ++superVoxelIterator)
		{
			// print the super voxel information if debug output is enabled
			//if (mSettings.mDebugOutput == true)
			//(*superVoxelIterator)->WriteSuperVoxelCoordinates();
			
			// skip all non-root nodes as these are already handled by the respective root node
			if ((*superVoxelIterator)->IsRootNode() == false)
				continue;

			//PickBestBranches((*superVoxelIterator)); // selects the highest scoring nodes of a tree
			TopDownLabelAssignment((*superVoxelIterator)); // selects the roots with the highest probabilities in a top-down fashion
			//(*superVoxelIterator)->Select(); // selects all roots
		}
		
		//typename QHash<unsigned int, SuperVoxel<TInputImage>* >::iterator superVoxelIterator;
		unsigned int currentId = 2;
		for (superVoxelIterator = mSuperVoxels.begin(); superVoxelIterator != mSuperVoxels.end(); ++superVoxelIterator)
		{
			// print all super voxels if debug output is enabled
			if (mSettings.mDebugOutput == true)
				(*superVoxelIterator)->PrintFeatures();

			// skip all nodes that are not in the selection state
			if ((*superVoxelIterator)->GetSelectionState() != 1)
				continue;

			// paint the super voxel including the internal watershed lines to the final image
			//(*superVoxelIterator)->PaintSuperVoxel(output, std::min<float>(1.0f, (float)(*superVoxelIterator)->GetId() / 65535.0f));
			//(*superVoxelIterator)->PaintSuperVoxel(output, std::min<float>(1.0f, ((float)currentId)));

			if ((*superVoxelIterator)->GetVolumeInternal() <= mSettings.mMaximumVolume)
			{
				(*superVoxelIterator)->PaintSuperVoxel(output0, (float)currentId, false, true);
				(*superVoxelIterator)->PaintSuperVoxel(output1, (float)currentId, false, false);
				
				++currentId;
			}
		}
	}

	// extract the connectivity information after the output images have been generated
	GenerateConnectivityInformation();
}

template< class TInputImage, class TOutputImage >
void WatershedBoundariesMergeTreeImageFilter< TInputImage, TOutputImage >::GenerateConnectivityInformation()
{
	// initialize the meta information
	if (mOutputMetaFilter == NULL)
		return;

	// get pointer to the first output
	typename OutputImageType::Pointer output1 = this->GetOutput1();
	
	// declare the super voxel iterator running over all existing super voxels
	typename QHash<unsigned int, SuperVoxel<TInputImage>* >::iterator superVoxelIterator;

	// identify the maximum number of neighbors as the number of columns
	int maxNeighbors = 0;
	for (superVoxelIterator = mSuperVoxels.begin(); superVoxelIterator != mSuperVoxels.end(); ++superVoxelIterator)
	{
		if ((*superVoxelIterator)->GetNeighborSuperVoxels()->size() > maxNeighbors &&
			(*superVoxelIterator)->GetSelectionState() == 1 &&
			(*superVoxelIterator)->GetVolumeInternal() <= mSettings.mMaximumVolume)
		{
			maxNeighbors = (*superVoxelIterator)->GetNeighborSuperVoxels()->size();
		}
	}

	// add column specifiers
	QStringList metaDescription;                    QStringList metaType;
	metaDescription << "cellIndex";                 metaType << "int";
	metaDescription << "isBorderSegment";           metaType << "int";
	metaDescription << "isSurfaceSegment";           metaType << "int";
	for (int i = 0; i<maxNeighbors; ++i)
	{
		metaDescription << QString("neighbor") + QString().number(i);
		metaType << "int";
	}
	mOutputMetaFilter->mTitle = metaDescription;
	mOutputMetaFilter->mType = metaType;
	mOutputMetaFilter->mIsMultiDimensional = true;
	mOutputMetaFilter->mPostfix = "Connectivity";
	
	// iterate over all super voxels and extract the ids of the current super voxel and its neighbors
	for (superVoxelIterator = mSuperVoxels.begin(); superVoxelIterator != mSuperVoxels.end(); ++superVoxelIterator)
	{
		// print all super voxels if debug output is enabled
		if (mSettings.mDebugOutput == true)
			(*superVoxelIterator)->PrintFeatures();

		// skip all nodes that are not in the selection state
		if ((*superVoxelIterator)->GetSelectionState() != 1)
			continue;

		// paint the super voxel including the internal watershed lines to the final image
		//(*superVoxelIterator)->PaintSuperVoxel(output, std::min<float>(1.0f, (float)(*superVoxelIterator)->GetId() / 65535.0f));
		//(*superVoxelIterator)->PaintSuperVoxel(output, std::min<float>(1.0f, ((float)currentId)));

		if ((*superVoxelIterator)->GetVolumeInternal() <= mSettings.mMaximumVolume)
		{
			// extract the label of the current super voxel
			vnl_vector<float> currentCentroid = (*superVoxelIterator)->GetCentroid();
			typename TOutputImage::IndexType currentIndex;
			currentIndex[0] = int(currentCentroid[0]);
			currentIndex[1] = int(currentCentroid[1]);
			currentIndex[2] = int(currentCentroid[2]);
			int currentLabel = output1->GetPixel(currentIndex);

			// add connectivity information to the meta data
			QList<unsigned int>* neighborSuperVoxels = (*superVoxelIterator)->GetNeighborSuperVoxels();
			QList<float> line;
			line << currentLabel;
			line << (*superVoxelIterator)->IsBorderSegment();
			line << (*superVoxelIterator)->IsSurfaceSegment();
			for (int i = 0; i < neighborSuperVoxels->size(); ++i)
			{
				vnl_vector<float> neighborCentroid = mSuperVoxels[neighborSuperVoxels->at(i)]->GetCentroid();
				typename TOutputImage::IndexType neighborIndex;
				neighborIndex[0] = int(neighborCentroid[0]);
				neighborIndex[1] = int(neighborCentroid[1]);
				neighborIndex[2] = int(neighborCentroid[2]);
				int neighborLabel = output1->GetPixel(neighborIndex);

				if (line.contains(neighborLabel) == false)
					line << neighborLabel;
			}

			mOutputMetaFilter->mData.append(line);
		}
	}
}

} // end namespace itk

#endif
