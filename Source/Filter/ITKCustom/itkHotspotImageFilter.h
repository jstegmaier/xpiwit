/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef __XPIWIT_HOTSPOTIMAGEFILTER_H
#define __XPIWIT_HOTSPOTIMAGEFILTER_H

// include required headers
#include "../Base/Management/ITKDefinitions.h"
#include "itkMacro.h"
#include "itkImageToImageFilter.h"

using namespace XPIWIT;

namespace itk
{

/**
 * @class HotpsotFilter
 * Parallelized hotspot detection filter.
 * TODO: Add automated transformation estimation.
 */
template <class TImageType>
class ITK_EXPORT HotspotImageFilter : public ImageToImageFilter<TImageType, TImageType>
{
    public:
        // Extract dimension from input and output image.
        itkStaticConstMacro(ImageDimension, unsigned int, TImageType::ImageDimension);
        typedef HotspotImageFilter Self;
        typedef ImageToImageFilter<TImageType,TImageType> Superclass;
        typedef SmartPointer<Self> Pointer;
        typedef SmartPointer<const Self> ConstPointer;
        itkNewMacro(Self);

        // definition of variable type abbreviations
        typedef TImageType ImageType;
        typedef typename ImageType::PixelType PixelType;
        typedef typename ImageType::RegionType InputImageRegionType;
        typedef typename ImageType::RegionType OutputImageRegionType;

        /**
         * The default constructor.
         */
        HotspotImageFilter();

        /**
         * The destructor.
         */
        virtual ~HotspotImageFilter();

        void SetRadii( std::vector<int> radii ){ m_Radii.clear(); m_Radii = radii; }
        void SetThreeD( bool in ){ threeD = in; }
        void SetMaxRadius( int maxRadius ){ m_MaxRadius = maxRadius; }

    protected:

        /**
         * Functions for data generation.
         * @param outputRegionForThread the region to be processed.
         * @param threadId the id of the current thread.
         */
        void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId) override;

        /**
         * functions for prepareing and post-processing the threaded generation.
         */
        void BeforeThreadedGenerateData() override;
        void AfterThreadedGenerateData() override;

        bool threeD;
        int m_MaxRadius;
        std::vector<int> m_Radii;

        //Manhattan Euclidean Chebyshev
        std::vector<std::vector<int> > getChebyshevRegionIndizes(int radius, bool threeD);

};

} // namespace itk

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkHotspotImageFilter.cpp"
#endif

#endif
