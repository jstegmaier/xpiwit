/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef __XPIWIT_SPLITCONNECTEDBLOBSIMAGEFILTER_H
#define __XPIWIT_SPLITCONNECTEDBLOBSIMAGEFILTER_H

// include required headers
#include "../Base/Management/ITKDefinitions.h"
#include "itkImageToImageFilter.h"
#include <string>

using namespace XPIWIT;

namespace itk {

template <class TInputImageType, class TOutputImageType=TInputImageType>
class ITK_EXPORT SplitConnectedBlobsImageFilter : public ImageToImageFilter<TInputImageType, TOutputImageType>
{
	public:
		/** Extract dimension from input and output image. */
		itkStaticConstMacro(ImageDimension, unsigned int, TInputImageType::ImageDimension);

		typedef SplitConnectedBlobsImageFilter Self;
		typedef ImageToImageFilter<TInputImageType, TOutputImageType> Superclass;
		typedef SmartPointer<Self> Pointer;
		typedef SmartPointer<const Self> ConstPointer;

		itkNewMacro(Self);

		typedef TInputImageType InputImageType;
		typedef typename InputImageType::PixelType     InputImagePixelType;
		typedef TOutputImageType						OutputImageType;
		typedef typename OutputImageType::PixelType    OutputImagePixelType;	  
  
		itkGetMacro( Level, float );
		itkSetMacro( Level, float );
		itkGetMacro( Threshold, float );
		itkSetMacro( Threshold, float );
		itkGetMacro( BinaryOutput, bool );
		itkSetMacro( BinaryOutput, bool );

	protected:
		SplitConnectedBlobsImageFilter();
		virtual ~SplitConnectedBlobsImageFilter();

		//typedef typename itk::SignedMaurerDistanceMapImageFilter<InputImageType, InternalImageType> SignedMaurerDistanceMapImageFilterType;
		//typedef typename itk::MorphologicalWatershedImageFilter<InternalImageType, OutputImageType> WatershedFilterType;

		/**
		 * Functions for data generation.
		 */
		void GenerateData() override;
		//void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId);

		/** 
		 * functions for prepareing and post-processing the threaded generation.
		 */
		//void BeforeThreadedGenerateData() override;
		//void AfterThreadedGenerateData() override;
		
		float m_Level;
		float m_Threshold;
		bool m_BinaryOutput;
};

} /* namespace itk */

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkSplitConnectedBlobsImageFilter.hxx"
#endif

#endif
