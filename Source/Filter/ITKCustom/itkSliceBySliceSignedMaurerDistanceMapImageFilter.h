/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef __XPIWITCORE_SLICEBYSLICESIGNEDMAURERDISTANCEMAPIMAGEFILTER_H
#define __XPIWITCORE_SLICEBYSLICESIGNEDMAURERDISTANCEMAPIMAGEFILTER_H

// include required headers
#include "../Base/Management/ITKDefinitions.h"

#include "itkExtractImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"

#include <string>

using namespace XPIWIT;

namespace itk {

template <class TImageType, class TOutputImage>
class ITK_EXPORT SliceBySliceSignedMaurerDistanceMapImageFilter : public ImageToImageFilter<TImageType, TOutputImage>
{
	public:
		/** Extract dimension from input and output image. */
		itkStaticConstMacro(ImageDimension, unsigned int, TImageType::ImageDimension);

		typedef SliceBySliceSignedMaurerDistanceMapImageFilter Self;
		typedef ImageToImageFilter<TImageType,TOutputImage> Superclass;
		typedef SmartPointer<Self> Pointer;
		typedef SmartPointer<const Self> ConstPointer;

		itkNewMacro(Self);

		itkGetMacro( UseImageSpacing, bool);
        itkSetMacro( UseImageSpacing, bool);
		itkSetMacro( UseSquaredDistance, bool);
		itkGetMacro( UseSquaredDistance, bool);
		itkSetMacro( InsideIsPositive, bool);
		itkGetMacro( InsideIsPositive, bool);
        itkSetMacro( BackgroundValue, float);
        itkGetMacro( BackgroundValue, float);
		
		typedef TImageType InputImageType;
		typedef typename TImageType::PixelType PixelType;

		typedef typename TImageType::RegionType InputImageRegionType;
		typedef typename TOutputImage::RegionType OutputImageRegionType;

	protected:
		SliceBySliceSignedMaurerDistanceMapImageFilter();
		virtual ~SliceBySliceSignedMaurerDistanceMapImageFilter();
		
		/**
		 * Functions for data generation.
		 */
		void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId) override;
		//unsigned int SplitRequestedRegion(unsigned int i, unsigned int num, OutputImageRegionType& splitRegion);

		/** 
		 * functions for prepareing and post-processing the threaded generation.
		 */
		void BeforeThreadedGenerateData() override;
		void AfterThreadedGenerateData() override;

		bool m_UseImageSpacing;
		bool m_UseSquaredDistance;
		bool m_InsideIsPositive;
		float m_BackgroundValue;
};

} /* namespace itk */

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkSliceBySliceSignedMaurerDistanceMapImageFilter.hxx"
#endif

#endif
