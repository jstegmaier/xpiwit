/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef __XPIWIT_SEEDBASEDWATERSHEDSEGMENTATIONFILTER_H
#define __XPIWIT_SEEDBASEDWATERSHEDSEGMENTATIONFILTER_H

// include required headers
#include "../Base/Management/ITKDefinitions.h"
#include "itkExtractKeyPointsImageFilter.h"
#include "itkExtractRegionPropsImageFilter.h"
#include "itkGradientMagnitudeRecursiveGaussianImageFilter.h"
#include "itkGradientMagnitudeImageFilter.h"
#include "itkBoundedReciprocalImageFilter.h"
//#include "itkMultiplyByConstantImageFilter.h"
#include "itkMultiplyImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkExtractImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkMacro.h"
#include <string>

#include "../Base/MetaData/MetaDataFilter.h"

using namespace XPIWIT;

namespace itk
{

/**
 * @class SeedBasedTwandSegmentationFilter
 * Fast seed based segmentation filter, e.g. to extract stained nuclei from microscopy images.
 * Yields a relatively good tradeoff between speed vs. processing time and is therefore suited for
 * processing large image stacks. The filter has to be provided with a seed point file.
 */
template <class TImageType>
class ITK_EXPORT SeedBasedWatershedSegmentationFilter : public ImageToImageFilter<TImageType, TImageType>
{
    public:
        // Extract dimension from input and output image.
        itkStaticConstMacro(ImageDimension, unsigned int, TImageType::ImageDimension);
        typedef SeedBasedWatershedSegmentationFilter Self;
        typedef ImageToImageFilter<TImageType,TImageType> Superclass;
        typedef SmartPointer<Self> Pointer;
        typedef SmartPointer<const Self> ConstPointer;
        itkNewMacro(Self);

        // definition of variable type abbreviations
        typedef typename TImageType::PixelType PixelType;
        typedef SeedPoint<TImageType> SeedPointType;
        typedef RegionProps RegionPropsType;
		typedef itk::Image<unsigned short, TImageType::ImageDimension> SeedImageType;
        typedef typename TImageType::RegionType InputImageRegionType;
        typedef typename TImageType::RegionType OutputImageRegionType;

        /**
         * The default constructor.
         */
        SeedBasedWatershedSegmentationFilter();

        /**
         * The destructor.
         */
        virtual ~SeedBasedWatershedSegmentationFilter();

        /**
         * Set and get methods for the member variables.
         */
        itkGetMacro( LabelOutput, bool);
        itkSetMacro( LabelOutput, bool);
        itkGetMacro( RandomLabels, bool);
        itkSetMacro( RandomLabels, bool);
        itkGetMacro( WriteRegionProps, bool);
        itkSetMacro( WriteRegionProps, bool);
		itkGetMacro( InvertIntensityImage, bool);
        itkSetMacro( InvertIntensityImage, bool);
		itkGetMacro( UseOriginalID, bool);
		itkSetMacro( UseOriginalID, bool);
		itkGetMacro( UseRegionPropsAABB, bool);
		itkSetMacro( UseRegionPropsAABB, bool);
		itkGetMacro( UseRegionGrowing, bool);
		itkSetMacro( UseRegionGrowing, bool);
		itkGetMacro( MarkWatershedLine, bool);
		itkSetMacro( MarkWatershedLine, bool);
		itkGetMacro( SeedRadiusMultiplier, float);
        itkSetMacro( SeedRadiusMultiplier, float);
		itkGetMacro( SeedDilationRadius, int);
		itkSetMacro( SeedDilationRadius, int);

        void SetInputMetaFilter( MetaDataFilter *metaFilter ) { m_InputMetaFilter = metaFilter; }
        void SetOutputMetaFilter( MetaDataFilter *metaFilter ) { m_OutputMetaFilter = metaFilter; }

    protected:

        /**
         * Functions for data generation.
         * @param outputRegionForThread the region to be processed.
         * @param threadId the id of the current thread.
         */
        void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId) override;

        /**
         * functions for prepareing and post-processing the threaded generation.
         */
        void BeforeThreadedGenerateData() override;
        void AfterThreadedGenerateData() override;

		typename SeedImageType::Pointer m_SeedImage;
		float m_SeedRadiusMultiplier;
		bool m_InvertIntensityImage;					// if set to true, the intensity image is inverted
        bool m_WriteRegionProps;						// if not checked region props are not stored
        bool m_LabelOutput;								// if set to true, the result image contains unique labels for each segment, otherwise the original intensity is used
        bool m_RandomLabels;							// if set to true, random intensities are used for each label, otherwise succesive integers are used
		bool m_UseOriginalID;
		bool m_UseRegionPropsAABB;
		bool m_MarkWatershedLine;
		bool m_UseRegionGrowing;
		int m_SeedDilationRadius;
		int m_MaxSeedLabel;
		unsigned int m_NumSeedPoints;
        std::vector<SeedPointType>* m_SeedPoints;		// the actual location info of the seeds
        std::vector<int>* m_SeedPointCombinations;		// the actual location info of the seeds
        std::vector<RegionPropsType>* m_RegionProps;	// the extracted region properties

        MetaDataFilter *m_InputMetaFilter;              // key points input meta data
        MetaDataFilter *m_OutputMetaFilter;             // region props output
};

} // namespace itk

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkSeedBasedWatershedSegmentationFilter.hxx"
#endif

#endif
