/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef __XPIWIT_HOTSPOTIMAGEFILTER_HXX
#define __XPIWIT_HOTSPOTIMAGEFILTER_HXX

// include required headers
#include <cfloat>
#include "itkHotspotImageFilter.h"
#include "itkImageRegionIterator.h"
#include "itkImageRegionConstIterator.h"
#include "itkNeighborhoodIterator.h"
#include "itkConstNeighborhoodIterator.h"
#include "itkZeroFluxNeumannBoundaryCondition.h"

// namespace itk
namespace itk
{

// the default constructor
template <class TImageType>
HotspotImageFilter<TImageType>::HotspotImageFilter()
{
    m_MaxRadius = 1;
    m_Radii.clear();
	this->DynamicMultiThreadingOff();
}

// the destructor
template <class TImageType>
HotspotImageFilter<TImageType>::~HotspotImageFilter()
{
    m_Radii.clear();
}

// before threaded generate data
template <class TImageType>
void HotspotImageFilter<TImageType>::BeforeThreadedGenerateData()
{
	// Allocate output
	typename ImageType::Pointer output = this->GetOutput();
	typename ImageType::ConstPointer input  = this->GetInput();
	output->FillBuffer( 0 );
}


// the thread generate data
template <class TImageType>
void HotspotImageFilter<TImageType>::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId)
{
	// Allocate output
    typename TImageType::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();
    typename TImageType::RegionType largestPossibleRegion = input->GetLargestPossibleRegion();
    typename TImageType::SpacingType spacing = input->GetSpacing();

	// iterate over the images and perform the fusion
    typename NeighborhoodIterator<TImageType>::RadiusType radius;
    radius.Fill(m_MaxRadius);
	
    //ConstNeighborhoodIterator<TImageType, ZeroFluxNeumannBoundaryCondition<TImageType> > inputIterator( radius, input, outputRegionForThread );
    ConstNeighborhoodIterator< TImageType > inputIterator( radius, input, outputRegionForThread );
    ImageRegionIterator< TImageType > outputIterator( output, outputRegionForThread );
	
	inputIterator.GoToBegin();
	outputIterator.GoToBegin();
	
	// perform the transformation
    float minimum = 1;
    float maximum = 0;
    float temp = 0;
	int curIndex = 0;
    std::vector< std::vector<int> > boundaryIndizes;
    std::vector<int>::iterator itr;

    // test
    boundaryIndizes.clear();
    boundaryIndizes = getChebyshevRegionIndizes(m_MaxRadius, threeD);

    while( !outputIterator.IsAtEnd() )
    {
        minimum = FLT_MAX;
        maximum = 0;

        // calc all maximas
        for(int i = 1; i <= m_MaxRadius; i++ ){
            maximum = 0;

            // maxima of one tile
            for( itr = boundaryIndizes.at(i-1).begin(); itr != boundaryIndizes.at(i-1).end(); ++itr ){
				curIndex = *itr;
                temp = inputIterator.GetPixel( curIndex );
                maximum = ( temp > maximum ) ? temp : maximum;
            }
            minimum = ( maximum < minimum ) ? maximum : minimum;
        }

        // calculate pixel value
        temp = inputIterator.GetCenterPixel();
        temp -= minimum;
        if(temp < 0)
            temp = 0;
        outputIterator.Set(temp);

        // prepare for next iteration
        ++outputIterator;
        ++inputIterator;
    }

    boundaryIndizes.clear();
}


// after threaded generate data
template <class TImageType>
void HotspotImageFilter<TImageType>::AfterThreadedGenerateData()
{
}

template <class TImageType>
std::vector<std::vector<int> > HotspotImageFilter<TImageType>::getChebyshevRegionIndizes( int radius, bool threeD )
{
    int i = 0;
    int absX, absY, absZ, max;
    std::vector< std::vector<int> > indizes;
    for(i = 0; i < radius; ++i){
		std::vector<int> a;
        indizes.push_back(a);
    }

    i = 0;
    if(threeD){
        for(int z = -radius; z <= radius; ++z){
            for(int y = -radius; y <= radius; ++y){
                for(int x = -radius; x <= radius; ++x){

                    absZ = z;
                    if(z < 0)
                        absZ *= -1;

                    absY = y;
                    if(y < 0)
                        absY *= -1;

                    absX = x;
                    if(x < 0)
                        absX *= -1;

                    if(absX > absY)
                        max = absX;
                    else
                        max = absY;

                    if(absZ > max)
                        max = absZ;

					if(max == 0)
						continue;

                    indizes.at(max - 1).push_back(i);
                    ++i;
                }
            }
        }
    }else{
        for(int y = -radius; y <= radius; ++y){
            for(int x = -radius; x <= radius; ++x){

                absY = y;
                if(y < 0)
                    absY *= -1;

                absX = x;
                if(x < 0)
                    absX *= -1;

                if(absX > absY)
                    max = absX;
                else
                    max = absY;

                if(max == 0)
                    continue;

                indizes.at(max - 1).push_back(i);
                ++i;
            }
        }
    }

	return indizes;
}

} // end namespace itk

#endif
