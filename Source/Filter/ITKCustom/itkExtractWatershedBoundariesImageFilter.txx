/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef __XPIWIT_EXTRACTWATERSHEDBOUNDARIESIMAGEFILTER_HXX
#define __XPIWIT_EXTRACTWATERSHEDBOUNDARIESIMAGEFILTER_HXX

// include required headers
#include "itkExtractWatershedBoundariesImageFilter.h"
#include "itkConstNeighborhoodIterator.h"
#include "itkNeighborhoodInnerProduct.h"
#include "itkImageRegionIterator.h"
#include "itkNeighborhoodAlgorithm.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkOffset.h"
#include "itkProgressReporter.h"
#include <fstream>
#include <iostream>
#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include "../../Core/Utilities/Logger.h"


#ifdef XPIWIT_USE_OPENMP
#include <omp.h>
#else
#include <thread>
#endif

using namespace XPIWIT;

// namespace itk
namespace itk
{

// the default constructor
template< class TInputImage, class TOutputImage >
ExtractWatershedBoundariesImageFilter< TInputImage, TOutputImage >::ExtractWatershedBoundariesImageFilter()
{
    mRegionProps = NULL;
	mIntensityImage = NULL;
	mDebugOutput = false;
	mMinimumVolume = 0;
	mMaximumVolume = 0;
	mGenerateEdgeMap = false;
	mUseMinimumVolumeCriterion = true;
	mUseSphericityCriterion = true;
	mUseMeanRatioCriterion = true;
	mUseBoundaryCriterion = true;
	mDisableMinimumVolumeCriterionOnBorder = false;
	this->DynamicMultiThreadingOff();
}


// the destructor
template< class TInputImage, class TOutputImage >
ExtractWatershedBoundariesImageFilter< TInputImage, TOutputImage >::~ExtractWatershedBoundariesImageFilter()
{
	// initialize the seed array for each thread
	itk::ThreadIdType numThreads = this->GetNumberOfWorkUnits();
	for (unsigned int i = 0; i<numThreads; ++i)
	{
		delete [] mNeighborsTemp[i];
	}

	delete mNeighborsTemp;
	delete [] mNeighbors;
	delete [] mBorderIndicesTemp;
	delete [] mIsBorderSegmentTemp;
}


// before threaded generate data
template< class TInputImage, class TOutputImage >
void ExtractWatershedBoundariesImageFilter< TInputImage, TOutputImage >::BeforeThreadedGenerateData()
{
    // get the input
    typename TInputImage::ConstPointer input  = this->GetInput();

    typedef itk::MinimumMaximumImageCalculator<TInputImage> ImageCalculatorFilterType;
    typename ImageCalculatorFilterType::Pointer minimumMaximumCalculator = ImageCalculatorFilterType::New();
    minimumMaximumCalculator->SetImage(input);
    minimumMaximumCalculator->Compute();
	mMaximumLabel = minimumMaximumCalculator->GetMaximum();
	
	if (mDebugOutput == true)
		std::cout << "- Maximum label is " << mMaximumLabel << std::endl;

	/*
	if (mDebugOutput == true)
	{
		std::cout << "Label 1: 123, Label 2: 121, Index: " << ConvertSegmentIdsToArrayIndex(121, 123) << std::endl;
		std::cout << ConvertSegmentIdsToArrayIndex(121, 123) << " Originates from labels " << ConvertArrayIndexToIndex1(ConvertSegmentIdsToArrayIndex(121, 123)) << " and " << ConvertArrayIndexToIndex2(ConvertSegmentIdsToArrayIndex(121, 123)) << std::endl;
	}*/

    // initialize the seed array for each thread
    itk::ThreadIdType numThreads = this->GetNumberOfWorkUnits();
    mNeighborsTemp = new QList<unsigned int>*[numThreads];
    for (unsigned int i=0; i<numThreads; ++i)
        mNeighborsTemp[i] = new QList<unsigned int>[mMaximumLabel + 1];

	// initialize the border indicator
	mIsBorderSegmentTemp = new QList<bool>[numThreads];
	for (unsigned int i = 0; i < numThreads; ++i)
	{
		mIsBorderSegmentTemp[i].reserve(mMaximumLabel+1);
		for (unsigned int j = 0; j <= mMaximumLabel; ++j)
			mIsBorderSegmentTemp[i].append(false);
	}

	// initialize the seed array for each thread
	mBorderIndicesTemp = new QHash<unsigned int, std::list<typename TInputImage::IndexType> >[numThreads];
	mRegionIndicesTemp = new QHash<unsigned int, std::list<typename TInputImage::IndexType> >[numThreads];

	/*for (unsigned int i = 0; i<numThreads; ++i)
	{
		mBorderIndicesTemp[i] = QHash<unsigned int, std::list<TInputImage::IndexType> >;
	}*/

    // fill the output buffer with zeros
    typename OutputImageType::Pointer output = this->GetOutput();
    output->FillBuffer(0);
}


// the thread generate data
template< class TInputImage, class TOutputImage >
void ExtractWatershedBoundariesImageFilter< TInputImage, TOutputImage >::ThreadedGenerateData(const OutputImageRegionType & outputRegionForThread, ThreadIdType threadId)
{
    // define input and output image pointers
    typename InputImageType::ConstPointer input  = this->GetInput();
    typename OutputImageType::Pointer output = this->GetOutput();

    // specify the neighborhood by the provided radius
    typename NeighborhoodIteratorType::RadiusType radius;
    radius.Fill( 1 );

    // remove 3rd dimension of the neighborhood iterator for 2d detection.
	if (TInputImage::ImageDimension > 2 && input->GetLargestPossibleRegion().GetSize()[2] == 1)
		radius[2] = 0;

    // specify input and output iterators
    ConstNeighborhoodIterator< InputImageType > inputIterator( radius, input, outputRegionForThread );
    ImageRegionIterator< OutputImageType > outputIterator( output, outputRegionForThread );

    // get the number of neighbours and the center pixel index
    unsigned int numNeighbors = inputIterator.Size();
    unsigned int centerIndex = (numNeighbors-1)/2;

    // reset the iterators
    inputIterator.GoToBegin();
    outputIterator.GoToBegin();

    // loop trough the region
    unsigned int currentValue = 0;
    while ( !inputIterator.IsAtEnd() )
    {
        // get current value
        currentValue = static_cast< unsigned int >(std::round(inputIterator.GetCenterPixel()));
		outputIterator.Set((((float)currentValue)+0.5) / 65535.0);

        // if value is non-zero skip calculations, as it's not a boundary pixel
        if (currentValue > 0)
        {
			bool isInBounds = true;
			for (unsigned int i = 0; i < numNeighbors; ++i)
			{
				// potential race condition probably not causing problems as it's only setting to true 
				inputIterator.GetPixel(i, isInBounds);
				if (isInBounds == false)
				{
					mIsBorderSegmentTemp[threadId][currentValue] = true;
					break;
				}
			}

			mRegionIndicesTemp[threadId][currentValue].push_back(inputIterator.GetIndex());
            ++inputIterator;
            ++outputIterator;
            continue;
        }

        // identify all unique values within the current image patch
		std::vector<unsigned int> uniqueValues;
        for (unsigned int i=0; i<numNeighbors; ++i)
        {
            unsigned int currentNeighbor = static_cast< unsigned int >(std::round(inputIterator.GetPixel(i)));
            if (currentNeighbor > 0 && currentNeighbor != currentValue)
            {
                bool elementExists = false;
                for (unsigned int j=0; j<uniqueValues.size(); ++j)
                {
                    if (uniqueValues[j] == currentNeighbor)
                    {
                        elementExists = true;
                        break;
                    }
                }

                if (elementExists == false)
                    uniqueValues.push_back(static_cast< unsigned int >(inputIterator.GetPixel(i)));
            }
        }

		// sort the indices
		std::sort(uniqueValues.begin(), uniqueValues.end());

        if (uniqueValues.size() >= 2)
        {
            //for (unsigned int i=0; i<uniqueValues.size(); ++i)
            //{
            //    for (unsigned int j=0; j<uniqueValues.size(); ++j)
            //    {
                    //if (uniqueValues[0] < uniqueValues[j] && uniqueValues[j] > 0)
                    //{
                        mNeighborsTemp[threadId][uniqueValues[0]].push_back(uniqueValues[1]);
						mNeighborsTemp[threadId][uniqueValues[1]].push_back(uniqueValues[0]);
						mBorderIndicesTemp[threadId][ConvertSegmentIdsToArrayIndex(uniqueValues[0], uniqueValues[1])].push_back(inputIterator.GetIndex());
                    //}
            //    }
            //}
        }

        // increase the iterators
        ++inputIterator;
        ++outputIterator;
    }
}


template< class TInputImage, class TOutputImage >
void ExtractWatershedBoundariesImageFilter< TInputImage, TOutputImage >::UpdateEdgeFeaturesAfterMergeThread(int threadId)
{
	itk::ThreadIdType numThreads = this->GetNumberOfWorkUnits();
	unsigned int stepSize = std::floor((float)mEdgesTemp.size() / (float)numThreads);
	unsigned int startIndex = (threadId*stepSize);
	unsigned int endIndex = ((threadId + 1)*stepSize)-1;

	if (endIndex < 0 || startIndex >= mEdgesTemp.size())
		return;

	if (threadId == (numThreads - 1))
		endIndex = mEdgesTemp.size();

	if (mDebugOutput == true)
		std::cout << "Thread " << threadId << " used to process range " << startIndex << " - " << endIndex << " with step size " << stepSize << "/" << mEdgesTemp.size() << std::endl;
	
	// add all edges to the merge queue (implemented as a QMap)
	for (int i = startIndex; i < endIndex; ++i)
	{
		if (i < 0 || i >= mEdgesTemp.size())
			std::cout << "Index out of bounds!!!" << std::endl;

		if (mEdgesTemp[i] != NULL)
			mEdgesTemp[i]->ComputeEdgeFeatures();
	}
}

template< class TInputImage, class TOutputImage >
void ExtractWatershedBoundariesImageFilter< TInputImage, TOutputImage >::ComputeEdgeFeaturesThread(int threadId)
{
	/*
	itk::ThreadIdType numThreads = this->GetNumberOfWorkUnits();
	unsigned int stepSize = std::floor((float)mEdges.size() / (float)numThreads);
	unsigned int startIndex = (threadId*stepSize);
	unsigned int endIndex = ((threadId + 1)*stepSize);

	if (endIndex < 0 || startIndex >= mEdges.size())
		return;

	if (threadId == (numThreads - 1))
		endIndex = mEdges.size();

	//if (mDebugOutput == true)
		std::cout << "Thread " << threadId << " used to process range " << startIndex << " - " << endIndex << " with step size " << stepSize << "/" << mEdges.size() << std::endl;

	if (startIndex < 0 || endIndex > mEdges.size())
		std::cout << "Index out of bounds!!!" << std::endl;

	// add all edges to the merge queue (implemented as a QMap)
	QHash<unsigned int, Edge<TInputImage>* >::iterator i;
	QHash<unsigned int, Edge<TInputImage>* >::iterator endPosition = (mEdges.begin() + endIndex);
	if (threadId == (numThreads - 1))
		endPosition = mEdges.end();

	for (i = mEdges.begin()+startIndex; i != endPosition; ++i)
	{
		(*i)->ComputeEdgeFeatures();
	}*/

	itk::ThreadIdType numThreads = this->GetNumberOfWorkUnits();
	int startIndex = (threadId) * mEdges.size() / numThreads;
	int endIndex = (threadId + 1) * mEdges.size() / numThreads;

	if (mDebugOutput == true)
		std::cout << "Processing range " << startIndex << " to " << endIndex << " on thread " << threadId << std::endl;

	typename QHash<unsigned int, Edge<TInputImage>* >::iterator i;
	for (i = mEdges.begin() + startIndex; i != (mEdges.begin() + endIndex); ++i)
		(*i)->ComputeEdgeFeatures();
}

template< class TInputImage, class TOutputImage >
void ExtractWatershedBoundariesImageFilter< TInputImage, TOutputImage >::PerformRegionMerge(Edge<TInputImage>* edge)
{
	if (mDebugOutput == true)
		std::cout << "Trying to perform the merge between " << edge->GetSegmentId1() << " and " << edge->GetSegmentId2() << std::endl;
    
	// get the input and output image
	typename InputImageType::ConstPointer input = this->GetInput();
	typename OutputImageType::Pointer output = this->GetOutput();

	// get the segment ids used for the merge
	const unsigned int segmentId1 = edge->GetSegmentId1();
	const unsigned int segmentId2 = edge->GetSegmentId2();
	const float imageLabel = (((float)segmentId1) + 0.5) / 65535.0;
    
	// remove the current edge from the image and set its label to the one of the first segment
    std::list<typename TInputImage::IndexType>* borderIndices = edge->GetBorderIndices();
	std::list<float>* borderIntensities = edge->GetBorderIntensities();
	typename std::list<typename TInputImage::IndexType>::iterator j;
	std::list<float>::iterator intensityIterator = borderIntensities->begin();
	
    // check if the indices are valid (should definitely be the case for valid edges!! bug somewhere else...)
	if (borderIndices->size() <= 0 || borderIntensities->size() <= 0)
		return;
    
	if (mDebugOutput == true)
		std::cout << "Removing watershed boundary between segments " << segmentId1 << " and " << segmentId2 << " ... ";
	for (j = borderIndices->begin(); j != borderIndices->end(); ++j)
	{
		output->SetPixel((*j), imageLabel);
        mRegionIndices[segmentId1].push_back(QPair<typename TInputImage::IndexType, float>(*j, *intensityIterator));
		++intensityIterator;
	}
	if (mDebugOutput == true) 
		std::cout << " SUCCESS" << std::endl;

	// set the label of the second segment to the label of the first segment
	if (mDebugOutput == true)
		std::cout << "Propagating label from " << segmentId1 << " and " << segmentId2 << " ... ";
	typename std::list<QPair<typename TInputImage::IndexType, float> >::iterator k;
	for (k = mRegionIndices[segmentId2].begin(); k != mRegionIndices[segmentId2].end(); ++k)
	{
		output->SetPixel((*k).first, imageLabel);
		mRegionIndices[segmentId1].push_back(QPair<typename TInputImage::IndexType, float>((*k).first, (*k).second));
	}
	if (mDebugOutput == true)
		std::cout << " SUCCESS" << std::endl;
    
    //////////
    // remove all edges from the merge queue, as they need to be updated.
	if (mDebugOutput == true)
	{
		std::cout << "Segment " << segmentId1 << " neighbors: ";
		for (int i = 0; i < mNeighbors[segmentId1].size(); ++i)
			std::cout << mNeighbors[segmentId1][i] << ", ";
		std::cout << std::endl;
 
		std::cout << "Segment " << segmentId2 << " neighbors: ";
		for (int i = 0; i < mNeighbors[segmentId2].size(); ++i)
			std::cout << mNeighbors[segmentId2][i] << ", ";
		std::cout << std::endl;
    
		std::cout << "Removing all occurrences of labels " << segmentId1 << " and " << segmentId2 << " from the merge list (expected: " << mNeighbors[segmentId1].size() + mNeighbors[segmentId2].size() << ")... " << std::endl;
	}

	int removedItems = 0;
    typename QMap<float, Edge<TInputImage>* >::iterator i = mMergeQueue.end();
    while (i != mMergeQueue.begin())
    {
		--i;
        if (mMergeQueue.size() == 0)
            break;
        
        if ((*i)->GetSegmentId1() == segmentId1 ||
            (*i)->GetSegmentId1() == segmentId2 ||
            (*i)->GetSegmentId2() == segmentId1 ||
            (*i)->GetSegmentId2() == segmentId2)
        {
            if (mDebugOutput == true)
            {
                std::cout << "Removing edge between segments " << (*i)->GetSegmentId1() << " and " << (*i)->GetSegmentId2() << " from  the merge queue" << std::endl;
                removedItems++;
            }
            
            i = mMergeQueue.erase(i);
        }
    }
    
	if (mDebugOutput == true)
		std::cout << "Removed " << removedItems << " items from the merge queue." << std::endl;
    
    //////////
    
	// calculate the updated features and store them for the first segment
	if (mDebugOutput == true)
		std::cout << "Updating region props of both segments ...";
	mRegionProps->mData[segmentId1][mVolumeFeatureIndex] = edge->GetVolumeCombined();
	mRegionProps->mData[segmentId1][mIntensityFeatureIndex] = edge->GetMeanIntensityCombined();
	mRegionProps->mData[segmentId1][mXPosFeatureIndex] = edge->GetCentroidCombined()[0];
	mRegionProps->mData[segmentId1][mYPosFeatureIndex] = edge->GetCentroidCombined()[1];
	mRegionProps->mData[segmentId1][mZPosFeatureIndex] = edge->GetCentroidCombined()[2];

	// set all features of the second segment to zero
	for (int i = 0; i < mRegionProps->mData[segmentId1].size(); ++i)
		mRegionProps->mData[segmentId2][i] = 0;
	if (mDebugOutput == true)
		std::cout << " SUCCESS!" << std::endl;

	// edges of neighbors present in both segments need to be combined to a single edge of segment1
	if (mDebugOutput == true)
		std::cout << "Processing neighbors of segment " << segmentId1 << " and identifying shared neighbors ..." << std::endl;
	QList<unsigned int> commonNeighbors;
	for (int i = 0; i<mNeighbors[segmentId1].size(); ++i)
	{
		const unsigned int currentNeighbor = mNeighbors[segmentId1][i];
		if (currentNeighbor != segmentId2 && !commonNeighbors.contains(currentNeighbor))
			commonNeighbors.append(currentNeighbor);

		if (mNeighbors[segmentId2].contains(currentNeighbor))
		{
			Edge<TInputImage>* removedEdge = mEdges[ConvertSegmentIdsToArrayIndex(segmentId2, currentNeighbor)];
			Edge<TInputImage>* updatedEdge = mEdges[ConvertSegmentIdsToArrayIndex(segmentId1, currentNeighbor)];

			if (removedEdge == NULL || updatedEdge == NULL)
			{
				std::cout << "Invalid edges selected between nodes " << segmentId1 << ", " << segmentId2 << ", " << currentNeighbor << std::endl;
				continue;
			}

			// remove the current edge and set its label to the one of the first segment
			std::list<typename TInputImage::IndexType>* borderIndices = removedEdge->GetBorderIndices();
			std::list<float>* borderIntensities = removedEdge->GetBorderIntensities();
			typename std::list<typename TInputImage::IndexType>::iterator j;
			std::list<float>::iterator intensityIterator = borderIntensities->begin();
			for (j = borderIndices->begin(); j != borderIndices->end(); ++j)
			{
				updatedEdge->AddVoxel((*j), (*intensityIterator));
				++intensityIterator;
			}
		}
	}
    
	if (mDebugOutput == true)
	{
		std::cout << "Shared neighbors are: ";
		for (int i = 0; i < commonNeighbors.size(); ++i)
			std::cout << commonNeighbors[i] << ", ";
		std::cout << std::endl;
	}

	// add  the neighbors of the second segment
	if (mDebugOutput == true)
		std::cout << "Processing neighbors of segment " << segmentId2 << " and identifying remaining neighbors ..." << std::endl;
	for (int i=0; i<mNeighbors[segmentId2].size(); ++i)
	{
		const unsigned int currentNeighbor = mNeighbors[segmentId2][i];
		Edge<TInputImage>* removedEdge = mEdges[ConvertSegmentIdsToArrayIndex(segmentId2, currentNeighbor)];
        
        if (currentNeighbor != segmentId1 && !commonNeighbors.contains(currentNeighbor))
		{
			commonNeighbors.append(currentNeighbor);

			// add the new edge that was previously only linked to segment2
			
			bool isBorderSegment = mIsBorderSegment[segmentId1] || mIsBorderSegment[currentNeighbor];
			mEdges[ConvertSegmentIdsToArrayIndex(segmentId1, currentNeighbor)] = new itk::Edge<TInputImage>(std::min<unsigned int>(segmentId1, currentNeighbor), std::max<unsigned int>(segmentId1, currentNeighbor), mRegionProps, isBorderSegment, mMinimumVolume, mMaximumVolume, mVolumeFeatureIndex, mIntensityFeatureIndex);
			//mEdges[ConvertSegmentIdsToArrayIndex(segmentId1, currentNeighbor)]->SetBorderVoxels(*removedEdge->GetBorderIndices(), *removedEdge->GetBorderIntensities());
			Edge<TInputImage>* updatedEdge = mEdges[ConvertSegmentIdsToArrayIndex(segmentId1, currentNeighbor)];
			updatedEdge->SetMergeCriteria(mUseMinimumVolumeCriterion, mUseSphericityCriterion, mUseMeanRatioCriterion, mUseBoundaryCriterion, mDisableMinimumVolumeCriterionOnBorder);

			// remove the current edge and set its label to the one of the first segment
			std::list<typename TInputImage::IndexType>* borderIndices = removedEdge->GetBorderIndices();
			std::list<float>* borderIntensities = removedEdge->GetBorderIntensities();
			typename std::list<typename TInputImage::IndexType>::iterator j;
			std::list<float>::iterator intensityIterator = borderIntensities->begin();
			for (j = borderIndices->begin(); j != borderIndices->end(); ++j)
			{
				updatedEdge->AddVoxel((*j), (*intensityIterator));
				++intensityIterator;
			}
            
            /*
             std::cout << "----- PRINTING ------" << removedEdge->GetBorderIndices()->size() << ", " << mEdges[ConvertSegmentIdsToArrayIndex(segmentId1, currentNeighbor)]->GetBorderIndices()->size() << std::endl;
             mEdges[ConvertSegmentIdsToArrayIndex(segmentId1, currentNeighbor)]->ComputeEdgeFeatures();
             mEdges[ConvertSegmentIdsToArrayIndex(segmentId1, currentNeighbor)]->PrintFeatures();
             std::cout << "--- END PRINTING ----" << std::endl;*/

			mEdges.remove(ConvertSegmentIdsToArrayIndex(segmentId2, currentNeighbor));
			if (mDebugOutput == true)
			{
				std::cout << "Trying to delete edge between " << segmentId2 << " and " << currentNeighbor << std::endl;
				removedEdge->PrintFeatures();
			}
			delete removedEdge;
        }

    }
    
	if (mDebugOutput == true)
	{
		std::cout << "Combined neighbors are: ";
		for (int i = 0; i < commonNeighbors.size(); ++i)
			std::cout << commonNeighbors[i] << ", ";
		std::cout << std::endl;
	}

	// set the new neighbor list
	mNeighbors[segmentId1] = commonNeighbors;
	mNeighbors[segmentId2].clear();

	// update the neighbors, such that the second segment is not contained anymore
	// and the id of the first segment is used instead
	if (mDebugOutput == true)
		std::cout << "Update all former neighbors of segment " << segmentId2 << " to  " << segmentId1 << std::endl;
	for (int i=0; i<commonNeighbors.size(); ++i)
	{
        const unsigned int currentNeighbor = commonNeighbors[i];
        
		if (mDebugOutput == true)
		{
			std::cout << "Image region " << currentNeighbor << " had neighbors ";
			for (int j = 0; j < mNeighbors[currentNeighbor].size(); ++j)
				std::cout << mNeighbors[currentNeighbor][j] << ", ";
			std::cout << std::endl;
		}
		
		if (!mNeighbors[currentNeighbor].contains(segmentId1))
			mNeighbors[currentNeighbor].append(segmentId1);

		int deprecatedIndex = mNeighbors[currentNeighbor].indexOf(segmentId2);
		if (mDebugOutput == true)
			std::cout << "Searching for " << segmentId2 << " yielded " << deprecatedIndex << std::endl;
		if (deprecatedIndex > -1)
		{
			if (mDebugOutput == true)
				std::cout << "Removing neighbor " <<  currentNeighbor << " sitting at index " << deprecatedIndex << std::endl;
			mNeighbors[currentNeighbor].removeAt(deprecatedIndex);
		}
        else
        {
            if (mDebugOutput == true)
                std::cout << segmentId2 << " is not a neighbor of " <<  currentNeighbor << std::endl;
        }
        
		if (mDebugOutput == true)
		{
			std::cout << "Image region " << currentNeighbor << " has neighbors ";
			for (int j = 0; j < mNeighbors[currentNeighbor].size(); ++j)
				std::cout << mNeighbors[currentNeighbor][j] << ", ";
			std::cout << std::endl;
		}
	}

    /*
	if (mDebugOutput == true)
	{
		for (unsigned int l = 0; l<=mMaximumLabel; ++l)
		{
			std::cout << "Image region " << l << " has neighbors ";
			for (int j = 0; j<mNeighbors[l].size(); ++j)
				std::cout << mNeighbors[l][j] << ", ";

			std::cout << std::endl;
		}
	}*/


    // clear the temporary new edges
	mEdgesTemp.clear();
	mEdgesTemp.reserve(commonNeighbors.size());
	for (int i = 0; i < commonNeighbors.size(); ++i)
		mEdgesTemp.append(mEdges[ConvertSegmentIdsToArrayIndex(segmentId1, commonNeighbors[i])]);

	if (mDebugOutput == true)
		std::cout << "Update needed for " << mEdgesTemp.size() << " edges ..." << std::endl;

	/*
	// update the edge features after the merge in parallel using std threads
	itk::ThreadIdType numThreads = this->GetNumberOfWorkUnits();

	if (mDebugOutput == true)
		std::cout << "Computing edge features using " << numThreads << " threads ..." << std::endl;

	
	std::thread* threads = new std::thread[numThreads];
	for (unsigned int threadId = 0; threadId < numThreads; ++threadId)
		threads[threadId] = std::thread([&] { this->UpdateEdgeFeaturesAfterMergeThread(threadId); });

	// join the threads and delete the array
	for (unsigned int threadId = 0; threadId < numThreads; ++threadId)
		threads[threadId].join();
	delete[] threads;
	*/

	// add all edges to the merge queue (implemented as a QMap)
	#pragma omp parallel for
	for (int i = 0; i < mEdgesTemp.size(); ++i)
	{
		if (i < 0 || i >= mEdgesTemp.size())
			std::cout << "Index out of bounds!!!" << std::endl;

		//std::cout << "Currently executing on thread " << omp_get_thread_num() << std::endl;

		if (mEdgesTemp[i] != NULL)
			mEdgesTemp[i]->ComputeEdgeFeatures();
	}
    
    #pragma omp parallel for
    for (int i = 0; i < mEdgesTemp.size(); ++i)
    {
        if (i < 0 || i >= mEdgesTemp.size())
            std::cout << "Index out of bounds!!!" << std::endl;
        
        //std::cout << "Currently executing on thread " << omp_get_thread_num() << std::endl;
        
        if (mEdgesTemp[i] != NULL)
            mEdgesTemp[i]->ComputeNeighborRelatedEdgeFeatures(mMaximumLabel, mNeighbors, &mEdges);
    }

	// add the updated edges to the merge queue again
	for (unsigned int i = 0; i < mEdgesTemp.size(); ++i)
	{
		if (mEdgesTemp[i] == NULL)
		{
			if (mDebugOutput == true)
				std::cout << "Skipping updated edge between segments " << i << std::endl;
			continue;
		}

        mMergeQueue.insertMulti(mEdgesTemp[i]->GetSortFeature(), mEdgesTemp[i]);
        
		if (mDebugOutput == true)
		{
			std::cout << "Adding updated edge between segments " << mEdgesTemp[i]->GetSegmentId1() << " and " << mEdgesTemp[i]->GetSegmentId2() << " to the merge queue" << std::endl;
			mEdgesTemp[i]->PrintFeatures();
		}

	}
}

// process the merge queue
template< class TInputImage, class TOutputImage >
void ExtractWatershedBoundariesImageFilter< TInputImage, TOutputImage >::ProcessMergeQueue()
{
	// get the first edge and process the merge queue
	Edge<TInputImage>* currentEdge = mMergeQueue.first();
	while (mMergeQueue.size() > 0 && currentEdge->GetSortFeature() < 0.99)
	{
		if (mDebugOutput == true)
		{
			std::cout << "----------------------------------------------------------------------------" << std::endl;
			std::cout << "New merge attempt started ..." << std::endl;
			std::cout << "Processed Edge: ";
			currentEdge->PrintFeatures();
		}
        
        // remove the first element from the map
		/*
        typename QMap<float, itk::Edge<TInputImage>*>::iterator i;
        for (i = mMergeQueue.end(); i != mMergeQueue.end(); ++i)
        {
            if (((*i)->GetSegmentId1() == currentEdge->GetSegmentId1() && (*i)->GetSegmentId2() == currentEdge->GetSegmentId2()) ||
                ((*i)->GetSegmentId1() == currentEdge->GetSegmentId2() && (*i)->GetSegmentId2() == currentEdge->GetSegmentId1()))
            {
                
            }
        }*/
		//std::cout << "Removing edge " << currentEdge->GetSegmentId1() << " and " << currentEdge->GetSegmentId2() << " from the merge queue." << std::endl;
		//mMergeQueue.erase(mMergeQueue.begin());
        
		// update region props and all other edges that are affected by the merge
		if (currentEdge->GetVolumeCombined() <= mMaximumVolume)
		{
			if (mDebugOutput == true)
				std::cout << "Performing the merge for edge between "<< currentEdge->GetSegmentId1() << " and " << currentEdge->GetSegmentId2() << " from the merge queue." << std::endl;
            PerformRegionMerge(currentEdge);
		}
		else
		{
			///// THIS ERASE DOES NOT UPDATE THE NEIGHBORS!!!
			int neighborIndex2 = mNeighbors[currentEdge->GetSegmentId1()].indexOf(currentEdge->GetSegmentId2());
			int neighborIndex1 = mNeighbors[currentEdge->GetSegmentId2()].indexOf(currentEdge->GetSegmentId1());
			mNeighbors[currentEdge->GetSegmentId1()].removeAt(neighborIndex2);
			mNeighbors[currentEdge->GetSegmentId2()].removeAt(neighborIndex1);

			mMergeQueue.erase(mMergeQueue.begin());
			if (mDebugOutput == true)
				std::cout << "Skipping edge " << currentEdge->GetSegmentId1() << " and " << currentEdge->GetSegmentId2() << " due to maximum volume constraint of " << mMaximumVolume << std::endl;
		}

        // delete top element from the queue and set
        //std::cout << "Removing the processed edge " << currentEdge->GetSegmentId1() << " and " << currentEdge->GetSegmentId2() << " from the edge list and deleting the object" << std::endl;
        mEdges.remove(ConvertSegmentIdsToArrayIndex(currentEdge->GetSegmentId1(), currentEdge->GetSegmentId2()));
		delete currentEdge;

		/*        
        typename QMap<float, itk::Edge<TInputImage>*>::iterator i;
        for (i = mMergeQueue.begin(); i != mMergeQueue.end(); ++i)
        {
            if (((*i)->GetSegmentId1() == currentEdge->GetSegmentId1() && (*i)->GetSegmentId2() == currentEdge->GetSegmentId2()) ||
                ((*i)->GetSegmentId1() == currentEdge->GetSegmentId2() && (*i)->GetSegmentId2() == currentEdge->GetSegmentId1()))
            {
                std::cout << "1 Oh oh, edge is still in the merge queue " << std::endl;
                (*i)->PrintFeatures();
            }
        }*/
        
        // get the next object from the merge queue
		if (mDebugOutput == true)
			std::cout << "Fetching the next edge from the merge queue" << std::endl;
		if (mMergeQueue.size() > 0)
		{
			currentEdge = mMergeQueue.first();
			if (mDebugOutput == true)
				currentEdge->PrintFeatures();
		}

        /*
		// for debug purposes, check if the iterator does the correct order
		if (mDebugOutput == true)
		{
			std::cout << "------ " << std::endl;
			std::cout << "Merge queue size is " << mMergeQueue.size() << " after merge" << std::endl;
			typename QMap<float, itk::Edge<TInputImage>*>::iterator i;
			for (i = mMergeQueue.begin(); i != mMergeQueue.end(); ++i)
			{
				//std::cout << "- NeighborPair " << (*i)->GetSegmentId1() << ", " << (*i)->GetSegmentId2() << " shares " << i.value()->GetBorderArea() << " voxels, with intensity " << i.value()->GetBorderMeanIntensity() << std::endl;
				(*i)->PrintFeatures();
			}
		}*/
    }
}

// after threaded generate data
template< class TInputImage, class TOutputImage >
void ExtractWatershedBoundariesImageFilter< TInputImage, TOutputImage >::AfterThreadedGenerateData()
{
    // get the output
    typename InputImageType::ConstPointer input  = this->GetInput();
    typename OutputImageType::Pointer output = this->GetOutput();

    itk::ThreadIdType numThreads = this->GetNumberOfWorkUnits();
    mNeighbors = new QList<unsigned int>[mMaximumLabel + 1];

	mIsBorderSegment.reserve(mMaximumLabel+1);
	for (unsigned int i = 0; i <= mMaximumLabel; ++i)
		mIsBorderSegment.append(false);
	
    for (unsigned int i=0; i<numThreads; ++i)
    {
        for (unsigned int j=0; j<(mMaximumLabel + 1); ++j)
        {
			mIsBorderSegment[j] = mIsBorderSegment[j] || mIsBorderSegmentTemp[i][j];

            for (unsigned int k=0; k<mNeighborsTemp[i][j].size(); ++k)
            {
                if (mNeighbors[j].contains(mNeighborsTemp[i][j][k]) == true || j == mNeighborsTemp[i][j][k] || mRegionProps->mData[mNeighborsTemp[i][j][k]][mVolumeFeatureIndex] > mMaximumVolume || mRegionProps->mData[j][mVolumeFeatureIndex] > mMaximumVolume)
                    continue;
                
				mNeighbors[j].push_back(mNeighborsTemp[i][j][k]);
            }
        }
    }

	

    /*
	if (mDebugOutput == true)
	{
		for (unsigned int l=0; l<(mMaximumLabel +1); ++l)
		{
			std::cout << "Image region " << l << " has neighbors ";
			for (int j=0; j<mNeighbors[l].size(); ++j)
				std::cout << mNeighbors[l][j] << ", ";

			std::cout << std::endl;
		}
	}*/

	// combine all voxels that belong to a common edge
	for (unsigned int i = 0; i<numThreads; ++i)
	{
		typename QHash<unsigned int, std::list<InputIndexType> >::iterator j;
		for (j = mRegionIndicesTemp[i].begin(); j != mRegionIndicesTemp[i].end(); ++j)
		{
			//if ((*j).size() > mMaxVolume)
			//	continue;

			typename std::list<typename TInputImage::IndexType>::iterator k;
			for (k = (*j).begin(); k != (*j).end(); ++k)
			{
				if (!mRegionIndices.contains(j.key()))
					mRegionIndices[j.key()] = std::list<QPair<InputIndexType, float> >();

				mRegionIndices[j.key()].push_back(QPair<InputIndexType, float>(*k, mIntensityImage->GetPixel(*k)));
			}
		}

		// iterate over the border pixels of the current thread
		//QHash<unsigned int, std::list<InputIndexType> >::iterator j;
		for (j = mBorderIndicesTemp[i].begin(); j != mBorderIndicesTemp[i].end(); ++j)
		{
			if ((mRegionProps->mData[ConvertArrayIndexToIndex1(j.key())][mVolumeFeatureIndex] > mMaximumVolume || mRegionProps->mData[ConvertArrayIndexToIndex2(j.key())][mVolumeFeatureIndex]) > mMaximumVolume)
			{
				//std::cout << "Skipping " << ConvertArrayIndexToIndex1(j.key()) << " and " << ConvertArrayIndexToIndex2(j.key()) << " with sizes " << mRegionProps->mData[ConvertArrayIndexToIndex1(j.key())][mVolumeFeatureIndex] << " and " << mRegionProps->mData[ConvertArrayIndexToIndex2(j.key())][mVolumeFeatureIndex] << std::endl;
				continue;
			}

			typename std::list<typename TInputImage::IndexType>::iterator k;
			for (k = (*j).begin(); k != (*j).end(); ++k)
			{
				if (!mEdges.contains(j.key()))
				{
					bool isBorderSegment = mIsBorderSegment[ConvertArrayIndexToIndex1(j.key())] || mIsBorderSegment[ConvertArrayIndexToIndex2(j.key())];
					mEdges[j.key()] = new itk::Edge<TInputImage>(ConvertArrayIndexToIndex1(j.key()), ConvertArrayIndexToIndex2(j.key()), mRegionProps, isBorderSegment, mMinimumVolume, mMaximumVolume, mVolumeFeatureIndex, mIntensityFeatureIndex);
					mEdges[j.key()]->SetMergeCriteria(mUseMinimumVolumeCriterion, mUseSphericityCriterion, mUseMeanRatioCriterion, mUseBoundaryCriterion, mDisableMinimumVolumeCriterionOnBorder);
				}

				if (mIntensityImage != NULL)
					mEdges[j.key()]->AddVoxel(*k, mIntensityImage->GetPixel(*k));
				else
					mEdges[j.key()]->AddVoxel(*k, 0);
			}

		}
	}

	// compute the edge features in parallel using std threads
	if (mDebugOutput == true)
		std::cout << "Computing edge features using " << numThreads << " threads ..." << std::endl;

#ifdef XPIWIT_USE_OPENMP
		std::cout << "Using OpenMP for threading ..." << std::endl;
		#pragma omp parallel
		{
			int currentThread = omp_get_thread_num();
			int numThreads = omp_get_num_threads();
			int startIndex = (currentThread) * mEdges.size() / numThreads;
			int endIndex = (currentThread + 1) * mEdges.size() / numThreads;
	
			typename QHash<unsigned int, Edge<TInputImage>* >::iterator j;
			for (j = mEdges.begin() + startIndex; j != (mEdges.begin()+endIndex); ++j)
				(*j)->ComputeEdgeFeatures();

			// Code inside this region runs in parallel.
			#pragma omp critical
			std::cout << "Processing range " << startIndex << " to " << endIndex <<" on thread " << currentThread << std::endl;
		}
    
    #pragma omp parallel
    {
        int currentThread = omp_get_thread_num();
        int numThreads = omp_get_num_threads();
        int startIndex = (currentThread) * mEdges.size() / numThreads;
        int endIndex = (currentThread + 1) * mEdges.size() / numThreads;
        
        typename QHash<unsigned int, Edge<TInputImage>* >::iterator j;
        for (j = mEdges.begin() + startIndex; j != (mEdges.begin()+endIndex); ++j)
            (*j)->ComputeNeighborRelatedEdgeFeatures(mMaximumLabel, mNeighbors, &mEdges);
        
        // Code inside this region runs in parallel.
		#pragma omp critical
        std::cout << "Processing range " << startIndex << " to " << endIndex <<" on thread " << currentThread << std::endl;
    }
    
    
#else
    std::cout << "Not using threads. Activate OpenMP in the CMake project to enable threading ..." << std::endl;

        
        typename QHash<unsigned int, Edge<TInputImage>* >::iterator i;
        for (i = mEdges.begin(); i != (mEdges.end()); ++i)
        {
            (*i)->ComputeEdgeFeatures();
            //std::cout << "Still running ..." << std::endl;
        }
        for (i = mEdges.begin(); i != (mEdges.end()); ++i)
            (*i)->ComputeNeighborRelatedEdgeFeatures(mMaximumLabel, mNeighbors, &mEdges);
    
        // Code inside this region runs in parallel.
        std::cout << "Finished updating edge features without using threads ... " << std::endl;
        
        
        /*
        std::cout << "Using std::threads for threading ..." << std::endl;
		std::thread* threads = new std::thread[numThreads];
		for (unsigned int threadId = 0; threadId < numThreads; ++threadId)
			threads[threadId] = std::thread([&] { this->ComputeEdgeFeaturesThread(threadId); } );

		// join the threads and delete the array
		for (unsigned int threadId = 0; threadId < numThreads; ++threadId)
			threads[threadId].join();
		delete [] threads;
    */
#endif

	// add all edges to the merge queue (implemented as a QMap)
	typename QHash<unsigned int, Edge<TInputImage>* >::iterator j;
	for (j = mEdges.begin(); j != mEdges.end(); ++j)
		mMergeQueue.insertMulti((*j)->GetSortFeature(), (*j));

    /*
	// for debug purposes, check if the iterator does the correct order
	if (mDebugOutput == true)
	{
		typename QMap<float, itk::Edge<TInputImage>*>::iterator i;
		for (i = mMergeQueue.begin(); i != mMergeQueue.end(); ++i)
		{
			//std::cout << "- NeighborPair " << (*i)->GetSegmentId1() << ", " << (*i)->GetSegmentId2() << " shares " << i.value()->GetBorderArea() << " voxels, with intensity " << i.value()->GetBorderMeanIntensity() << std::endl;
			(*i)->PrintFeatures();


		}
	}*/

	// process the merge queue for the final segmentation
	if (mGenerateEdgeMap == true)
	{
		// fill the image with black values
		output->FillBuffer(0);

		// add all edges to the merge queue (implemented as a QMap)
		typename QMap<float, Edge<TInputImage>* >::iterator i;
		for (i = mMergeQueue.begin(); i != mMergeQueue.end(); ++i)
		{
            std::list<typename TInputImage::IndexType>* borderIndices = (*i)->GetBorderIndices();
            typename std::list<typename TInputImage::IndexType>::iterator j;
			for (j = borderIndices->begin(); j != borderIndices->end(); ++j)
				output->SetPixel(*j, std::min<float>(1.0f, (*i)->GetSortFeature()));

			(*i)->PrintFeatures();
		}
	}
	else
	{
		ProcessMergeQueue();
	}
}

} // end namespace itk

#endif
