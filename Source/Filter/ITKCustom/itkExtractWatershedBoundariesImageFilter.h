/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef EXTRACTWATERSHEDBOUNDARIESIMAGEFILTER_H
#define EXTRACTWATERSHEDBOUNDARIESIMAGEFILTER_H

// include required headers
#include "../Base/Management/ITKDefinitions.h"
#include "itkBoxImageFilter.h"
#include "itkImage.h"
#include "itkNumericTraits.h"
#include "itkNeighborhoodIterator.h"
#include "itkConstNeighborhoodIterator.h"
#include "itkZeroFluxNeumannBoundaryCondition.h"
#include <vector>
#include <float.h>
#define _USE_MATH_DEFINES // required to enable math defines in visual studio
#include <math.h>
#include <QString>
#include <QHash>
#include <list>

#include "../Base/MetaData/MetaDataFilter.h"

using namespace XPIWIT;

namespace itk
{

/**
* @class SeedPoint container class for seed points.
* used to store informatino such as index, scale, intensity and an id.
*/
template<class TInputImage>
class Edge
{
public:
	typedef typename TInputImage::IndexType IndexType;
	typedef typename TInputImage::PixelType PixelType;
	Edge(const unsigned int segmentId1, const unsigned int segmentId2, XPIWIT::MetaDataFilter* regionProps, const bool containsBorderSegment, const unsigned int minimumVolume, const unsigned int maximumVolume, const unsigned int volumeFeatureIndex, const unsigned int intensityFeatureIndex, const unsigned int xposFeatureIndex = 2, const unsigned int yposFeatureIndex = 3, const unsigned int zposFeatureIndex = 4)
	{ 
		mSegmentId1 = segmentId1; mSegmentId2 = segmentId2; mBorderMeanIntensity = -1; mRegionProps = regionProps; 
		mIntensityFeatureIndex = intensityFeatureIndex; mVolumeFeatureIndex = volumeFeatureIndex;
		mXPosFeatureIndex = xposFeatureIndex; mYPosFeatureIndex = yposFeatureIndex; mZPosFeatureIndex = zposFeatureIndex;
		mMinimumVolume = minimumVolume;
		mMaximumVolume = maximumVolume;
		mVolume1 = 0;
		mVolume2 = 0;
		mVolumeCombined = 0;
		mVolumeBorder = 0;
		mContainsBorderSegment = containsBorderSegment;
		mBorderMeanIntensity = 0.0;
		mCentroid1 = vnl_vector<float>(3); mCentroid2 = vnl_vector<float>(3); mCentroidCombined = vnl_vector<float>(3);
		mDirtyFlag = true;
		mIsInteriorEdge = true;
		mUseBoundaryCriterion = true;
		mUseMinimumVolumeCriterion = true;
		mUseMeanRatioCriterion = true;
		mUseSphericityCriterion = true;		
	}
	~Edge()
    {
        //std::cout << "Edge " << mSegmentId1 << " and " << mSegmentId2 << " is deleted ..." << std::endl;
    }
	
    // static functions
    static unsigned int ConvertSegmentIdsToArrayIndex(const unsigned int index1, const unsigned int index2, const unsigned int maximumLabel) { return (std::min<unsigned int>(index1, index2)*(maximumLabel+1) + std::max<unsigned int>(index1, index2)); };
    static unsigned int ConvertArrayIndexToIndex1(const unsigned int arrayIndex, const unsigned int maximumLabel) { return (unsigned int)std::floor(arrayIndex / (maximumLabel + 1)); };
    static unsigned int ConvertArrayIndexToIndex2(const unsigned int arrayIndex, const unsigned int maximumLabel) { return arrayIndex % (maximumLabel + 1); };
    
    
	unsigned int GetSegmentId1() { return mSegmentId1; }
	unsigned int GetSegmentId2() { return mSegmentId2; }
	unsigned int GetVolume1() { return mVolume1; }
	unsigned int GetVolume2() { return mVolume2; }
	unsigned int GetVolumeCombined() { return mVolumeCombined; }
	bool GetDirtyFlag() { return mDirtyFlag; }
	bool GetIsInteriorEdge() { return mIsInteriorEdge;  }
	vnl_vector<float> GetCentroidCombined() { return mCentroidCombined; }

	float TrapMF(const float x, const float a, const float b, const float c, const float d) 
	{
		return std::max<float>(std::min<float>(1, std::min<float>(((x-a)/(b-a+FLT_EPSILON)), ((d-x)/(d-c+FLT_EPSILON)))), 0);
	}

	unsigned int GetBorderVolume() { return mBorderIndices.size(); }
	void AddVoxel(const IndexType& borderIndex, const float& borderIntensity) { mDirtyFlag = true;  mBorderIndices.push_back(borderIndex);  mBorderIntensities.push_back(borderIntensity); };
	//void SetBorderVoxels(const std::list<IndexType>& borderIndices, std::list<float> mBorderIntensities)
	//{
	//	mBorderIndices = borderIndices;
	//	mBorderIntensities = mBorderIntensities;
	//}
	
	void ComputeEdgeFeatures()
	{
		//std::cout << "- changed border intensity for " << mSegmentId1 << " -> " << mSegmentId2 << " with size " << mVolumeBorder << " from " << mBorderMeanIntensity << " to ";

		assert(mBorderIndices.size() == mBorderIntensities.size());

		// get the volume of each super voxel
		mVolume1 = mRegionProps->mData[mSegmentId1][mVolumeFeatureIndex];
		mVolume2 = mRegionProps->mData[mSegmentId2][mVolumeFeatureIndex];
		mVolumeBorder = mBorderIndices.size();
		mVolumeCombined = (mVolume1 + mVolume2 + mVolumeBorder);

		// compute the border mean intensity
		mBorderMeanIntensity = 0.0;
		std::list<float>::iterator i;
		for (i = mBorderIntensities.begin(); i != mBorderIntensities.end(); ++i)
			mBorderMeanIntensity += (*i);
		mBorderMeanIntensity /= (float)mBorderIntensities.size();

		//std::cout << mBorderMeanIntensity << " for " << mBorderIntensities.size() << " with dirtyFlag " << mDirtyFlag << std::endl;

		// get the regional mean intensities
		mMeanIntensity1 = mRegionProps->mData[mSegmentId1][mIntensityFeatureIndex];
		mMeanIntensity2 = mRegionProps->mData[mSegmentId2][mIntensityFeatureIndex];
		mMeanIntensityCombined = (((float)mVolume1)*mMeanIntensity1 + 
								  ((float)mVolume2)*mMeanIntensity2 + 
								  ((float)mBorderIndices.size())*mBorderMeanIntensity) / ((float)mVolumeCombined);

		// combined centroid
		mCentroid1[0] = mRegionProps->mData[mSegmentId1][mXPosFeatureIndex];
		mCentroid1[1] = mRegionProps->mData[mSegmentId1][mYPosFeatureIndex];
		mCentroid1[2] = ((TInputImage::ImageDimension == 2) ? 0 : mRegionProps->mData[mSegmentId1][mZPosFeatureIndex]);
		mCentroid2[0] = mRegionProps->mData[mSegmentId2][mXPosFeatureIndex];
		mCentroid2[1] = mRegionProps->mData[mSegmentId2][mYPosFeatureIndex];
		mCentroid2[2] = ((TInputImage::ImageDimension == 2) ? 0 : mRegionProps->mData[mSegmentId2][mZPosFeatureIndex]);
		mCentroidCombined = ((float)mVolume1 * mCentroid1 + (float)mVolume2 * mCentroid2);
        
        vnl_vector<float> currentPos(3);
        typename std::list<typename TInputImage::IndexType>::iterator j;
        for (j = mBorderIndices.begin(); j != mBorderIndices.end(); ++j)
        {
            currentPos[0] = (*j)[0];
            currentPos[1] = (*j)[1];
            currentPos[2] = ((TInputImage::ImageDimension == 2) ? 0 : (*j)[2]);
            mCentroidCombined = mCentroidCombined + currentPos;
        }
        mCentroidCombined = mCentroidCombined / mVolumeCombined;
        

		//std::cout << "- Computed edge features for NeighborPair " << GetSegmentId1() << ", " << GetSegmentId2() << std::endl;

		// compute the sort feature used for region merging
		mSNRCriterion = std::min<float>(1.0f, 0.5 * mBorderMeanIntensity * (1.0 / mMeanIntensity1 + 1.0 / mMeanIntensity2));
		
        //float minVolumeCriterion1 = TrapMF((float)mVolume1, 0, mMinimumVolume, FLT_MAX, FLT_MAX);
        //float minVolumeCriterion2 = TrapMF((float)mVolume2, 0, mMinimumVolume, FLT_MAX, FLT_MAX);
		mMinimumVolumeCriterion = TrapMF(std::min<unsigned int>((float)mVolume1, (float)mVolume2), 0, mMinimumVolume, FLT_MAX, FLT_MAX);

		if (mMinimumVolumeCriterion < 1.0)
			mMinimumVolumeCriterion = mMinimumVolumeCriterion * mBorderMeanIntensity;

		//if (mVolumeBorder < 20)
		//	snrCriterion = 1.0;

		if ((mContainsBorderSegment == true && mDisableMinimumVolumeCriterionOnBorder == true) || (mVolumeCombined > mMaximumVolume))
			mMinimumVolumeCriterion = 1.0;
		
		mSortFeature = sqrtf(mSNRCriterion*mSNRCriterion + mMinimumVolumeCriterion*mMinimumVolumeCriterion) / sqrtf(2.0);
		//mSortFeature = sqrtf(1.0 + minVolumeCriterion*minVolumeCriterion);

		mDirtyFlag = false;
	}

	void ComputeNeighborRelatedEdgeFeatures(const unsigned int maximumLabel, const QList<unsigned int>* neighbors, const QHash<unsigned int, Edge<TInputImage>* >* edges)
	{
		// get the number of neighbors for each of the segments
		const unsigned int numNeighbors1 = neighbors[mSegmentId1].size();
		const unsigned int numNeighbors2 = neighbors[mSegmentId2].size();

		// compute the boundary intensity for the first segment
		bool isWeakestEdge = true;
		bool isShortestEdge1 = true;
        bool isShortestEdge2 = true;
		float borderIntensities1 = 0.0;
		float averageDistance1 = 0.0;
		float averageDistance2 = 0.0;
		float averageDistanceCombined = 0.0;
		unsigned int borderPixels1 = 0;
		unsigned int borderPixelsCombined = 0;
		vnl_vector<float> currentPos(3);

		for (unsigned int i = 0; i < numNeighbors1; ++i)
		{
			// get the current neighbor id
			unsigned int currentNeighbor = neighbors[mSegmentId1][i];

			// get the border mean intensity for the current neighbor, if it's not the intersection neighbor
			if (currentNeighbor != mSegmentId2)
			{
				unsigned int currentEdgeId = ConvertSegmentIdsToArrayIndex(mSegmentId1, currentNeighbor, maximumLabel);
				borderIntensities1 += ((*edges)[currentEdgeId])->GetBorderVolume() * ((*edges)[currentEdgeId])->GetBorderMeanIntensity();
				borderPixels1 += ((*edges)[currentEdgeId])->GetBorderVolume();

				if (((*edges)[currentEdgeId])->GetBorderMeanIntensity() < mBorderMeanIntensity)
					isWeakestEdge = false;

				if (((*edges)[currentEdgeId])->GetBorderVolume() < mVolumeBorder)
					isShortestEdge1 = false;

				std::list<typename TInputImage::IndexType>* borderIndices = ((*edges)[currentEdgeId])->GetBorderIndices();
				typename std::list<typename TInputImage::IndexType>::iterator j;
				for (j = borderIndices->begin(); j != borderIndices->end(); ++j)
				{
					currentPos[0] = (*j)[0];
					currentPos[1] = (*j)[1];
					currentPos[2] = ((TInputImage::ImageDimension == 2) ? 0 : (*j)[2]);
					averageDistance1 = std::max<float>(averageDistance1, (currentPos - mCentroid1).magnitude());
					averageDistanceCombined = std::max<float>(averageDistance1, (currentPos - mCentroidCombined).magnitude());
					//averageDistance1 += (currentPos - mCentroid1).magnitude();
					//averageDistanceCombined += (currentPos - mCentroidCombined).magnitude();
					borderPixelsCombined++;
				}
			}
		}
		borderIntensities1 += GetBorderVolume() * GetBorderMeanIntensity();
		borderPixels1 += GetBorderVolume();

		// compute the boundary intensity for the second segment
		float borderIntensities2 = 0.0;
		unsigned int borderPixels2 = 0;
		for (unsigned int i = 0; i < numNeighbors2; ++i)
		{
			// get the current neighbor id
			unsigned int currentNeighbor = neighbors[mSegmentId2][i];

			// get the border mean intensity for the current neighbor, if it's not the intersection neighbor
			if (currentNeighbor != mSegmentId1)
			{
				unsigned int currentEdgeId = ConvertSegmentIdsToArrayIndex(currentNeighbor, mSegmentId2, maximumLabel);
				borderIntensities2 += ((*edges)[currentEdgeId])->GetBorderVolume() * ((*edges)[currentEdgeId])->GetBorderMeanIntensity();
				borderPixels2 += ((*edges)[currentEdgeId])->GetBorderVolume();

				if (((*edges)[currentEdgeId])->GetBorderMeanIntensity() < mBorderMeanIntensity)
					isWeakestEdge = false;

				if (((*edges)[currentEdgeId])->GetBorderVolume() < mVolumeBorder)
					isShortestEdge2 = false;

				std::list<typename TInputImage::IndexType>* borderIndices = ((*edges)[currentEdgeId])->GetBorderIndices();
				typename std::list<typename TInputImage::IndexType>::iterator j;
				for (j = borderIndices->begin(); j != borderIndices->end(); ++j)
				{
					currentPos[0] = (*j)[0];
					currentPos[1] = (*j)[1];
					currentPos[2] = ((TInputImage::ImageDimension == 2) ? 0 : (*j)[2]);
					averageDistance2 = std::max<float>(averageDistance2, (currentPos - mCentroid2).magnitude());
					averageDistanceCombined = std::max<float>(averageDistanceCombined, (currentPos - mCentroidCombined).magnitude());
					//averageDistance2 += (currentPos - mCentroid2).magnitude();
					//averageDistanceCombined += (currentPos - mCentroidCombined).magnitude();
					borderPixelsCombined++;
				}
			}
		}
		borderIntensities2 += GetBorderVolume() * GetBorderMeanIntensity();
		borderPixels2 += GetBorderVolume();
		//averageDistance1 /= (float)(borderPixels1);
		//averageDistance2 /= (float)(borderPixels2);
		//averageDistanceCombined /= (float)(borderPixelsCombined);

		float border1MeanIntensity = borderIntensities1 / borderPixels1;
		float border2MeanIntensity = borderIntensities2 / borderPixels2;
		float borderCombinedMeanIntensity = (borderIntensities1 + borderIntensities2 - 2 * GetBorderVolume() * GetBorderMeanIntensity()) / (float)(borderPixels1 + borderPixels2 - 2 * GetBorderVolume());
		//std::cout << border1MeanIntensity << " " << border2MeanIntensity << " " << borderCombinedMeanIntensity << std::endl;

		// intensity probability
		float totalBorderIntensity = ((borderIntensities1)+(borderIntensities2)) / (float)(borderPixels1 + borderPixels2);
		float probability1 = borderIntensities1 / (borderPixels1*mMeanIntensity1);
		float probability2 = borderIntensities2 / (borderPixels2*mMeanIntensity2);
		float probabilityCombined = totalBorderIntensity / mMeanIntensityCombined;

        if (std::abs<float>((GetBorderMeanIntensity() - totalBorderIntensity)) > 0)
		{
            mBoundaryCriterion = std::min<float>(1.0f, std::abs<float>((GetBorderMeanIntensity() - mMeanIntensityCombined)) / std::abs<float>((GetBorderMeanIntensity() - totalBorderIntensity))); //0.5*(probability1 + probability2) / probabilityCombined; //std::min<float>(1.0f, 0.5*(probability1+probability2) / probabilityCombined);
		}
		else
		{
			mBoundaryCriterion = 1.0f;
		}
        
		//std::cout << "Edge: " << mSegmentId1 << " <-> " << mSegmentId2 << ", BorderMeanIntensity: " << GetBorderMeanIntensity() << ", mMeanIntensityCombined: " << mMeanIntensityCombined << ", totalBorderIntensity: " << totalBorderIntensity << ", BoundaryCriterion: " << mBoundaryCriterion << std::endl;

		/*
		float surfaceArea1 = borderPixels1;
		float surfaceArea2 = borderPixels2;		
		float surfaceAreaCombined = borderPixels1 + borderPixels2 - 2 * GetBorderVolume();
		float volume1 = mVolume1 + borderPixels1;
		float volume2 = mVolume2 + borderPixels2;
		float volumeCombined = mVolume1 + mVolume2 + borderPixels1 + borderPixels2 - GetBorderVolume();

		float isoperimetricRatio1 = ((surfaceArea1*surfaceArea1) / volume1) / (4 * (M_PI));
		float isoperimetricRatio2 = ((surfaceArea2*surfaceArea2) / volume2) / (4 * (M_PI));
		float isoperimetricRatio3 = ((surfaceAreaCombined*surfaceAreaCombined) / volumeCombined) / (4 * (M_PI));


		//std::cout << "Edge: " << mSegmentId1 << " <-> " << mSegmentId2 << ", isoperimetricRatio1: " << isoperimetricRatio1 << ", isoperimetricRatio2: " << isoperimetricRatio2 << ", isoperimetricRatio3: " << isoperimetricRatio3 << ", RatioRatio: " << isoperimetricRatio3 / (0.5*(isoperimetricRatio1+isoperimetricRatio2)) << std::endl;
		//std::cout << "Edge: " << mSegmentId1 << " <-> " << mSegmentId2 << ", surfaceArea1: " << surfaceArea1 << ", surfaceArea2: " << surfaceArea2 << ", surfaceAreaCombined: " << surfaceAreaCombined << ", volume1: " << volume1 << ", volume2: " << volume2 << ", volume3: " << volumeCombined << std::endl;

		mSortFeature = std::min<float>(1.0, isoperimetricRatio3 / (0.5*(isoperimetricRatio1+isoperimetricRatio2)));
		return;*/

		//mBoundaryCriterion = GetBorderMeanIntensity();

        // sphericity probability
		float factor = (4.0*M_PI);
        float approxSphereRadius1 = std::pow(3.0*(float)(mVolume1 + borderPixels1) / factor, 1.0 / 3.0);
        float approxSphereRadius2 = std::pow(3.0*(float)(mVolume2 + borderPixels2) / factor, 1.0 / 3.0);
        float approxSphereRadiusCombined = std::pow(3.0*((float)mVolumeCombined + borderPixelsCombined) / factor, 1.0 / 3.0);
        
        //std::cout << "Approx sphere radii: " << approxSphereRadius1 << ", " << approxSphereRadius2 << ", " << approxSphereRadiusCombined << std::endl;

        //averageDistance1 /= (float)borderPixels1;
        //averageDistance2 /= (float)borderPixels2;
        //averageDistanceCombined /= (float)borderPixelsCombined;
        
        //std::cout << "Average distances: " << averageDistance1 << ", " << averageDistance2 << ", " << averageDistanceCombined << std::endl;
        
        float sphericity1 = approxSphereRadius1 / averageDistance1;
        float sphericity2 = approxSphereRadius2 / averageDistance2;
        float sphericityCombined = approxSphereRadiusCombined / averageDistanceCombined;
        
        float sphericity = std::min<float>(1.0, 0.5*(sphericity1+sphericity2) / sphericityCombined);
		mSphericityCriterion = sphericity1;

        //std::cout << "Sphericity of segment pair " << mSegmentId1 << " and " << mSegmentId2 << " is: " << sphericity1 << ", " << sphericity2 << ", " << sphericityCombined << ", " << sphericity << std::endl;
        
        // volume probability
        
        //if (isWeakestEdge == false ||
        //    ((0.5*(sphericity1+sphericity2) >= sphericityCombined) && std::min<unsigned int>((float)mVolume1, (float)mVolume2) > mMinimumVolume))
        //    mSortFeature = 1.0;

		//mSortFeature = mBorderMeanIntensity;
		//return;


		//if (isWeakestEdge == false || GetBorderVolume() < (0.05*std::min<float>(borderPixels1, borderPixels2)))
		//{
		//	mSortFeature = 1.0;
		//	return;
		//}

		int numFeatures = 0;
		mSortFeature = 0.0;
		if (mUseMinimumVolumeCriterion == true)
		{
			mSortFeature += mMinimumVolumeCriterion * mMinimumVolumeCriterion;
			numFeatures++;
		}

		if (mUseMeanRatioCriterion == true)
		{
			mSortFeature += mSNRCriterion * mSNRCriterion;
			numFeatures++;
		}

		if (mUseBoundaryCriterion == true)
		{
			mSortFeature += mBoundaryCriterion * mBoundaryCriterion;
			numFeatures++;
		}

		/*
		if (mUseSphericityCriterion == true)
		{
			mSortFeature += sphericity * sphericity;
			numFeatures++;
			//std::cout << "Using sphericity feature..." << std::endl;
		}*/
        
		/*
        if (isShortestEdge1 == true && isShortestEdge2 == true)
        {
            mSortFeature = 1.0;
            return;
        }
		*/
		
		// normalize the sort feature, such that it is smaller than one of one of the criteria is violated and 1 otherwise
		if (mUseSphericityCriterion == true && (mUseMinimumVolumeCriterion == false || (mUseMinimumVolumeCriterion == true && mMinimumVolumeCriterion >= 1.0)) )
			mSortFeature = std::max<float>(sphericity, sqrtf(mSortFeature) / sqrtf(numFeatures));
		else
			mSortFeature = sqrtf(mSortFeature) / sqrtf(numFeatures);
	}

	std::list<IndexType>* GetBorderIndices() { return &mBorderIndices; }
	std::list<float>* GetBorderIntensities() { return &mBorderIntensities; }
	float GetSortFeature() { return mSortFeature; }
	float GetBorderMeanIntensity() { return mBorderMeanIntensity; }
	float GetMeanIntensityCombined() { return mMeanIntensityCombined; }

	void SetMergeCriteria(const bool useMinimumVolumeCriterion, const bool useSphericityCriterion, const bool useMeanRatioCriterion, const bool useBoundaryCriterion, const bool disableMinimumVolumeCriterionOnBorder)
	{
		mUseBoundaryCriterion = useBoundaryCriterion;
		mUseMinimumVolumeCriterion = useMinimumVolumeCriterion;
		mUseMeanRatioCriterion = useMeanRatioCriterion;
		mUseSphericityCriterion = useSphericityCriterion;
		mDisableMinimumVolumeCriterionOnBorder = disableMinimumVolumeCriterionOnBorder;
	}

	void PrintFeatures()
	{
		std::cout << "- NeighborPair " << GetSegmentId1() << ", " << GetSegmentId2() << ": " 
					<< "borderLength " << GetBorderVolume() 
					<< ", meanIntensity " << GetBorderMeanIntensity()
					<< ", volume1 " << mVolume1
					<< ", volume2 " << mVolume2
					<< ", meanIntensity1 " << mMeanIntensity1
					<< ", meanIntensity2 " << mMeanIntensity2 
					<< ", snrCriterion " << mSortFeature
					<< ", sphericity " << mSphericityCriterion << std::endl;
	}

protected:
	unsigned int mIntensityFeatureIndex;
	unsigned int mVolumeFeatureIndex;
	unsigned int mXPosFeatureIndex;
	unsigned int mYPosFeatureIndex;
	unsigned int mZPosFeatureIndex;

	unsigned int mSegmentId1;
	unsigned int mSegmentId2;
	unsigned int mMinimumVolume;
	unsigned int mMaximumVolume;
	unsigned int mVolume1;
	unsigned int mVolume2;
	unsigned int mVolumeBorder;
	unsigned int mVolumeCombined;
	float mMeanIntensity1;
	float mMeanIntensity2;
	float mMeanIntensityCombined;
    float mBoundaryCriterion;
	float mSortFeature;
	float mBorderMeanIntensity;
	float mMinimumVolumeCriterion;
	float mSphericityCriterion;
	float mSNRCriterion;
	bool mContainsBorderSegment;
	bool mIsInteriorEdge;
	bool mDirtyFlag;

	bool mUseMinimumVolumeCriterion;
	bool mUseSphericityCriterion;
	bool mUseMeanRatioCriterion;
	bool mUseBoundaryCriterion;
	bool mDisableMinimumVolumeCriterionOnBorder;

	XPIWIT::MetaDataFilter* mRegionProps;
	vnl_vector<float> mCentroid1;
	vnl_vector<float> mCentroid2;
	vnl_vector<float> mCentroidCombined;
	std::list<IndexType> mBorderIndices;
	std::list<float> mBorderIntensities;
};

template<class TInputImage>
bool EdgeLessThan(itk::Edge<TInputImage>& edge1, itk::Edge<TInputImage>& edge2)
{
	return edge1.GetSortFeature() < edge2.GetSortFeature();
}

/**
 * @class ExtractKeyPointsImageFilter extracts local maxima from a given image.
 */
template< class TInputImage, class TOutputImage >
class ITK_EXPORT ExtractWatershedBoundariesImageFilter : public BoxImageFilter< TInputImage, TOutputImage >
{
public:
    // Extract dimension from input and output image.
    itkStaticConstMacro(InputImageDimension, unsigned int, TInputImage::ImageDimension);
    itkStaticConstMacro(OutputImageDimension, unsigned int, TOutputImage::ImageDimension);

    // Convenient typedefs for simplifying declarations.
    typedef TInputImage  InputImageType;
    typedef TOutputImage OutputImageType;

    // Standard class typedefs.
    typedef ExtractWatershedBoundariesImageFilter             Self;
    typedef BoxImageFilter< InputImageType, OutputImageType > Superclass;
    typedef SmartPointer< Self >                              Pointer;
    typedef SmartPointer< const Self >                        ConstPointer;

	QList<unsigned int>* GetNeighbors() { return mNeighbors; }
	QHash<unsigned int, Edge<TInputImage>*>* GetEdges() { return &mEdges; }
	unsigned int GetMaximumLabel() { return mMaximumLabel; }
	unsigned int ConvertSegmentIdsToArrayIndex(const unsigned int index1, const unsigned int index2) { return (std::min<unsigned int>(index1, index2)*(mMaximumLabel+1) + std::max<unsigned int>(index1, index2)); };
	unsigned int ConvertArrayIndexToIndex1(const unsigned int arrayIndex) { return (unsigned int)std::floor(arrayIndex / (mMaximumLabel + 1)); };
	unsigned int ConvertArrayIndexToIndex2(const unsigned int arrayIndex) { return arrayIndex % (mMaximumLabel + 1); };
    
    
	void SetUseMinimumVolumeCriterion(const bool useMinimumVolumeCriterion) { mUseMinimumVolumeCriterion = useMinimumVolumeCriterion; }
	void SetUseSphericityCriterion(const bool useSphericityCriterion) { mUseSphericityCriterion = useSphericityCriterion; }
	void SetUseMeanRatioCriterion(const bool useMeanRatioCriterion) { mUseMeanRatioCriterion = useMeanRatioCriterion; }
	void SetUseBoundaryCriterion(const bool useBoundaryCriterion) { mUseBoundaryCriterion = useBoundaryCriterion; }
	void SetDisableMinimumVolumeCriterionOnBorder(const bool disableMinimumVolumeCriterionOnBorder) { mDisableMinimumVolumeCriterionOnBorder = disableMinimumVolumeCriterionOnBorder; }

	void SetGenerateEdgeMap(const bool generateEdgeMap) { mGenerateEdgeMap = generateEdgeMap; }
	void ComputeEdgeFeaturesThread(int threadId);
	void UpdateEdgeFeaturesAfterMergeThread(int threadId);

	void PerformRegionMerge(Edge<TInputImage>* edge);

	void SetInputMetaFilter(XPIWIT::MetaDataFilter* metaFilter) 
	{ 
		mRegionProps = metaFilter;
		mVolumeFeatureIndex = mRegionProps->GetFeatureIndex("volume");
		mIntensityFeatureIndex = mRegionProps->GetFeatureIndex("meanIntensity");
		mXPosFeatureIndex = mRegionProps->GetFeatureIndex("xpos");
		mYPosFeatureIndex = mRegionProps->GetFeatureIndex("ypos");
		mZPosFeatureIndex = mRegionProps->GetFeatureIndex("zpos");
	}
	void SetIntensityImage(TInputImage* intensityImage) { mIntensityImage = intensityImage; }

	void SetVolumeConstraints(const unsigned int minimumVolume = 4000, const unsigned int maximumVolume = 8000) { mMinimumVolume = minimumVolume; mMaximumVolume = maximumVolume; }


	void ProcessMergeQueue();

    // Method for creation through the object factory.
    itkNewMacro(Self);

    // Run-time type information (and related methods).
    itkTypeMacro(ExtractWatershedBoundariesImageFilter, BoxImageFilter);

    // Image typedef support.
    typedef typename InputImageType::PixelType                 InputPixelType;
	typedef typename itk::Index<InputImageType::ImageDimension> InputIndexType;
    typedef typename OutputImageType::PixelType                OutputPixelType;
    typedef typename NumericTraits< InputPixelType >::RealType InputRealType;

    typedef typename InputImageType::RegionType  InputImageRegionType;
    typedef typename OutputImageType::RegionType OutputImageRegionType;

    typedef typename InputImageType::SizeType InputSizeType;
    typedef typename itk::ZeroFluxNeumannBoundaryCondition<InputImageType, InputImageType> ZeroFluxNeumannBoundaryConditionType;
    typedef itk::NeighborhoodIterator< InputImageType, ZeroFluxNeumannBoundaryConditionType> NeighborhoodIteratorType;
    typedef typename NeighborhoodIteratorType::IndexType NeighborhoodIteratorIndexType;

#ifdef ITK_USE_CONCEPT_CHECKING
    /** Begin concept checking */
    itkConceptMacro( InputHasNumericTraitsCheck,
                     ( Concept::HasNumericTraits< InputPixelType > ) );
    /** End concept checking */
#endif
protected:
	ExtractWatershedBoundariesImageFilter(); //5000000, 8000
    virtual ~ExtractWatershedBoundariesImageFilter();

    /** ExtractKeyPointsImageFilter can be implemented as a multithreaded filter.
   * Therefore, this implementation provides a ThreadedGenerateData()
   * routine which is called for each processing thread. The output
   * image data is allocated automatically by the superclass prior to
   * calling ThreadedGenerateData().  ThreadedGenerateData can only
   * write to the portion of the output image specified by the
   * parameter "outputRegionForThread"
   *
   * \sa BoxImageFilter::ThreadedGenerateData(),
   *     BoxImageFilter::GenerateData() */
    void ThreadedGenerateData(const OutputImageRegionType & outputRegionForThread,
                              ThreadIdType threadId) override;


    /** functions for prepareing and post-processing the threaded generation */
    void BeforeThreadedGenerateData() override;
    void AfterThreadedGenerateData() override;

private:
    ExtractWatershedBoundariesImageFilter(const Self &); //purposely not implemented
    void operator=(const Self &);  //purposely not implemented

	bool mDebugOutput;
    unsigned int mMaximumLabel;
	unsigned int mIntensityFeatureIndex;
	unsigned int mVolumeFeatureIndex;
	unsigned int mXPosFeatureIndex;
	unsigned int mYPosFeatureIndex;
	unsigned int mZPosFeatureIndex;
	unsigned int mMinimumVolume;
	unsigned int mMaximumVolume;
	bool mGenerateEdgeMap;

	bool mUseMinimumVolumeCriterion;
	bool mUseSphericityCriterion;
	bool mUseMeanRatioCriterion;
	bool mUseBoundaryCriterion;
	bool mDisableMinimumVolumeCriterionOnBorder;

	QList<unsigned int>** mNeighborsTemp;
	QList<unsigned int>* mNeighbors;
	QList<bool>* mIsBorderSegmentTemp;
	QList<bool> mIsBorderSegment;
	QHash<unsigned int, std::list<InputIndexType> >* mBorderIndicesTemp;
	QHash<unsigned int, std::list<InputIndexType> >* mRegionIndicesTemp;
	QHash<unsigned int, std::list<QPair<InputIndexType, float> > > mRegionIndices;
	
	QHash<unsigned int, Edge<TInputImage>* > mEdges;
	QList<Edge<TInputImage>* > mEdgesTemp;
	QMap<float, Edge<TInputImage>* > mMergeQueue;
    XPIWIT::MetaDataFilter* mRegionProps;
	TInputImage* mIntensityImage;
};

} // end namespace itk

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkExtractWatershedBoundariesImageFilter.txx"
#endif

#endif
