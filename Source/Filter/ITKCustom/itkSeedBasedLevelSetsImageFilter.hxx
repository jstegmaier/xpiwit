/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// include required headers
#include "itkSeedBasedLevelSetsImageFilter.h"
#include "itkImageRegionIterator.h"
#include "itkImageRegionConstIterator.h"
#include "itkIntensityWindowingImageFilter.h"
#include <iostream>
#include <fstream>
#include <string>
#include <QString>
#include <QStringList>
#include <QDateTime>
#include "../../Core/Utilities/Logger.h"

// namespace itk
namespace itk
{

// the default constructor
template <class TImageType>
SeedBasedLevelSetsImageFilter<TImageType>::SeedBasedLevelSetsImageFilter()
{
    // set default values for the parameters
    m_Sigma = 1.0;
    m_Segment3D = true;
    m_SplitConnectedBlobs = true;
    m_InitialLSFValue = 2.0;
    m_PropagationScaling = 1.0;
    m_CurvatureScaling = 1.0;
    m_AdvectionScaling = 1.0;
    m_NumIterations = 100;
    m_OverlapSize = 128;
	this->DynamicMultiThreadingOff();
}


// the destructor
template <class TImageType>
SeedBasedLevelSetsImageFilter<TImageType>::~SeedBasedLevelSetsImageFilter()
{
}


template <class TImageType>
unsigned int SeedBasedLevelSetsImageFilter<TImageType>::SplitRequestedRegion(unsigned int i, unsigned int num, OutputImageRegionType& splitRegion)
{
  // Get the output pointer
  TImageType* outputPtr = this->GetOutput();

  const typename TImageType::SizeType& requestedRegionSize = outputPtr->GetRequestedRegion().GetSize();

  int splitAxis;
  typename TImageType::IndexType splitIndex;
  typename TImageType::SizeType splitSize;

  // Initialize the splitRegion to the output requested region
  splitRegion = outputPtr->GetRequestedRegion();
  splitIndex = splitRegion.GetIndex();
  splitSize = splitRegion.GetSize();

  // split on the outermost dimension available
  splitAxis = outputPtr->GetImageDimension() - 2;
  while ( requestedRegionSize[splitAxis] == 1 )
    {
    --splitAxis;
    if ( splitAxis < 0 )
      { // cannot split
      itkDebugMacro("  Cannot Split");
      return 1;
      }
    }

  // determine the actual number of pieces that will be generated
  typename TImageType::SizeType::SizeValueType range = requestedRegionSize[splitAxis];
  unsigned int valuesPerThread = Math::Ceil< unsigned int >(range / (double)num);
  unsigned int maxThreadIdUsed = Math::Ceil< unsigned int >(range / (double)valuesPerThread) - 1;

  // Split the region
  if ( i < maxThreadIdUsed )
    {
    splitIndex[splitAxis] += i * valuesPerThread;
    splitSize[splitAxis] = valuesPerThread;
    }
  if ( i == maxThreadIdUsed )
    {
    splitIndex[splitAxis] += i * valuesPerThread;
    // last thread needs to process the "rest" dimension being split
    splitSize[splitAxis] = splitSize[splitAxis] - i * valuesPerThread;
    }

  // set the split region ivars
  splitRegion.SetIndex(splitIndex);
  splitRegion.SetSize(splitSize);

  itkDebugMacro("  Split Piece: " << splitRegion);

  return maxThreadIdUsed + 1;
}

// before threaded generate data
template <class TImageType>
void SeedBasedLevelSetsImageFilter<TImageType>::BeforeThreadedGenerateData()
{
    // Allocate output
    typename TImageType::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();
	typename TImageType::SpacingType spacing = input->GetSpacing();

	// create seed points
	int numKeyPoints = 0;
	for ( int i = 0; i < m_KeyPoints->mData.length(); ++i ){

		typename TImageType::IndexType index;
		for ( int j = 0; j < TImageType::ImageDimension; ++j )
			index[j] = int(0.5+m_KeyPoints->mData.at(i).at(j+2)/spacing[j]);

		// add the seed point
		m_SeedPoints.push_back( SeedPointType(index, m_KeyPoints->mData.at(i).at(1)) );
		numKeyPoints++;
	}

    //CurvatureAnisotropicDiffusionImageFilterType::Pointer anisotropicDiffusionFilter = CurvatureAnisotropicDiffusionImageFilterType::New();
    typename GradientMagnitudeRecursiveGaussianImageFilterType::Pointer gradientMagnitudeFilter = GradientMagnitudeRecursiveGaussianImageFilterType::New();
    //typename itk::MultiplyByConstantImageFilter<ImageType, float, ImageType>::Pointer constantMultiplyFilter = itk::MultiplyByConstantImageFilter<ImageType, float, ImageType>::New();
    typename itk::MultiplyImageFilter<TImageType, TImageType, TImageType>::Pointer constantMultiplyFilter = itk::MultiplyImageFilter<TImageType, TImageType, TImageType>::New();
    typename BoundedReciprocalImageFilterType::Pointer boundedReciprocalImageFilter = BoundedReciprocalImageFilterType::New();

    gradientMagnitudeFilter->SetInput( input );
    gradientMagnitudeFilter->SetSigma( m_Sigma );
    gradientMagnitudeFilter->SetInPlace( true );
    gradientMagnitudeFilter->SetReleaseDataFlag( true );
    constantMultiplyFilter->SetInput( gradientMagnitudeFilter->GetOutput() );
    constantMultiplyFilter->SetConstant( m_EdgeMapFactor );
    constantMultiplyFilter->SetInPlace( true );
    constantMultiplyFilter->SetReleaseDataFlag( true );
    boundedReciprocalImageFilter->SetInput( constantMultiplyFilter->GetOutput() );
    boundedReciprocalImageFilter->SetInPlace( true );
    boundedReciprocalImageFilter->SetReleaseDataFlag( false );

    try
    {
        boundedReciprocalImageFilter->Update();
    }
    catch( itk::ExceptionObject & err )
    {
        Logger::GetInstance()->WriteLine( "ExceptionObject caught !" );
        Logger::GetInstance()->WriteException( err );
        return;
    }

    mEdgeMap = boundedReciprocalImageFilter->GetOutput();

    // initialize the LSF
    /*
    typedef typename TImageType::RegionType InputImageRegionType;
    typedef TImageType InputImageType;
    InputImageType::Pointer initialLSF = InputImageType::New();

    mInitialLSF = initialLSF;

    InputImageRegionType myregion = ;*/
    typedef TImageType InputImageType;
    mInitialLSF = InputImageType::New();
    mInitialLSF->SetRegions( input->GetLargestPossibleRegion() );
    mInitialLSF->SetSpacing( input->GetSpacing() );
    mInitialLSF->Allocate();
    mInitialLSF->FillBuffer( m_InitialLSFValue );


    typename itk::IntensityWindowingImageFilter<TImageType, itk::Image< unsigned short, TImageType::ImageDimension > >::Pointer intensityConversion = itk::IntensityWindowingImageFilter<TImageType, itk::Image< unsigned short, TImageType::ImageDimension > >::New();
    intensityConversion->SetInput(mEdgeMap);
    intensityConversion->SetWindowMinimum(0);
    intensityConversion->SetWindowMaximum(1);
    intensityConversion->SetOutputMinimum(0);
    intensityConversion->SetOutputMaximum(65535);
    intensityConversion->Update();

    //typename itk::ImageFileWriter<itk::Image< unsigned short, TImageType::ImageDimension > >::Pointer mywriter = itk::ImageFileWriter<itk::Image< unsigned short, TImageType::ImageDimension > >::New();
    //mywriter->SetInput( intensityConversion->GetOutput() );
    //mywriter->SetFileName("D:/Programming/XPIWIT/XPIWIT/Bin/Test/Out/temp.tif");
    //mywriter->Update();
}


// the thread generate data
template <class TImageType>
void SeedBasedLevelSetsImageFilter<TImageType>::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId)
{
    // Allocate output
    typename TImageType::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();

    // allocate seed point info variables
    float radius = 0.0f;
    typename TImageType::IndexType pos;
    typename TImageType::IndexType index;
    typename TImageType::SizeType size;
    typename TImageType::RegionType region;

    // add seed points to the image region as initialization of the lsf
    const int numSeedPoints = m_SeedPoints.size();
    for (int i=0; i<numSeedPoints; ++i)
    {
        // check if keypoint is inside the region
        if (outputRegionForThread.IsInside( m_SeedPoints[i].GetIndex() ) == false)
            continue;

        // extract seed point information
        radius = m_SeedPoints[i].GetScale() / 2;
        for (int j=0; j<TImageType::ImageDimension; ++j)
            pos[j] = m_SeedPoints[i].GetIndex()[j];

        // initialize the region iterator arround the keypoint
        for (int j=0; j<TImageType::ImageDimension; ++j)
        {
            index[j] = std::max<int>(outputRegionForThread.GetIndex()[j], pos[j]-radius);
            size[j] = std::min<int>(m_SeedPoints[j].GetScale(), (outputRegionForThread.GetIndex()[j]+outputRegionForThread.GetSize()[j])-index[j]);
        }

        region.SetIndex( index );
        region.SetSize( size );

        // only process slice if 3d mode is disabled
        if (TImageType::ImageDimension == 3 && m_Segment3D == false)
        {
            region.SetIndex(2, m_SeedPoints[i].GetIndex()[2]);
            region.SetSize(2, 1);
        }

        // initialize the seed region iterator
        ImageRegionIterator<TImageType> seedRegionIterator( mInitialLSF, region );
        ImageRegionConstIterator<TImageType> edgeRegionIterator( input, region );
        seedRegionIterator.GoToBegin();
        edgeRegionIterator.GoToBegin();

        // iterate trough the seed region and set voxels for the initial LSF
        while (seedRegionIterator.IsAtEnd() == false)
        {
            // set value of the initial region
            if (radius > sqrt(pow(pos[0]-seedRegionIterator.GetIndex()[0], 2) + pow(pos[1]-seedRegionIterator.GetIndex()[1], 2)))
            {
                seedRegionIterator.Set( -m_InitialLSFValue );
            }

            // increment iterators
            ++seedRegionIterator;
            ++edgeRegionIterator;
        }
    }

    // perform the level set evolution
    //if (m_Segment3D == true)
    {
        // calculate LSE for all
        for (int i=0; i<2; ++i)
        {
            // define regions and the size of overlap parts
            InputImageRegionType inputRegion = outputRegionForThread;
            OutputImageRegionType outputRegion = outputRegionForThread;
            InputImageRegionType largestRegion = mInitialLSF->GetLargestPossibleRegion();
            int overlapHalf = m_OverlapSize / 2;
            int overlapFourth = m_OverlapSize / 4;

            // get the index of the input region
            typename TImageType::IndexType index = inputRegion.GetIndex();
            typename TImageType::SizeType size = inputRegion.GetSize();

            // handle the overlap case, i.e. regions between two larger regions
            if (i == 1)
            {
                // no overlap calculation needed for the first tile (1 if tiling y-axis, 2 if tiling z-axis)
                if (index[1] == 0)
                    continue;

                inputRegion.SetIndex( 1, index[1]-overlapHalf );
                inputRegion.SetSize( 1, m_OverlapSize );
                outputRegion.SetIndex( 1, index[1] - overlapFourth );
                outputRegion.SetSize( 1, overlapHalf );
            }

            // handle the actual image regions
            else
            {
                // the first piece of the image
                if (index[1] == 0)
                {
                    outputRegion.SetSize( 1, outputRegion.GetSize(1)-overlapFourth );
                }

                // the last piece of the image
                else if (index[1]+size[1] == largestRegion.GetSize(1))
                {
                    outputRegion.SetIndex( 1, outputRegion.GetIndex(1)+overlapFourth );
                    outputRegion.SetSize( 1, outputRegion.GetSize(1)-overlapFourth );
                }

                // intermediate pieces
                else
                {
                    outputRegion.SetIndex( 1, outputRegion.GetIndex(1)+overlapFourth );
                    outputRegion.SetSize( 1, outputRegion.GetSize(1)-overlapHalf );
                }

                //outputRegion.SetSize( 1, outputRegion.GetSize(1) );
            }

            // extract initial lsf from the image
            typename TImageType::Pointer image = TImageType::New();
            image->SetSpacing( mInitialLSF->GetSpacing() );
            image->SetRegions( inputRegion );
            image->Allocate();

            // generate the edge map
            typename TImageType::Pointer edgeMap = TImageType::New();
            edgeMap->SetSpacing( mInitialLSF->GetSpacing() );
            edgeMap->SetRegions( inputRegion );
            edgeMap->Allocate();

            // define and reset the image iterators
            ImageRegionIterator<TImageType> imageIterator( image, inputRegion );
            ImageRegionIterator<TImageType> initialLSFIterator( mInitialLSF, inputRegion );
            ImageRegionIterator<TImageType> edgeMapIterator( edgeMap, inputRegion );
            ImageRegionIterator<TImageType> edgeVolumeIterator( mEdgeMap, inputRegion );
            imageIterator.GoToBegin();
            initialLSFIterator.GoToBegin();
            edgeMapIterator.GoToBegin();
            edgeVolumeIterator.GoToBegin();

            // iterate trough the seed region and set voxels for the initial LSF
            while (initialLSFIterator.IsAtEnd() == false)
            {
                imageIterator.Set( initialLSFIterator.Value() );
                edgeMapIterator.Set( edgeVolumeIterator.Value() );

                ++initialLSFIterator;
                ++imageIterator;
                ++edgeMapIterator;
                ++edgeVolumeIterator;
            }

            // setup the level sets filter
            typename LevelSetImageFilterType::Pointer filter = LevelSetImageFilterType::New();
            filter->SetInput( image );
            filter->SetFeatureImage( edgeMap );
            filter->SetPropagationScaling( m_PropagationScaling );
            filter->SetCurvatureScaling( m_CurvatureScaling );
            filter->SetAdvectionScaling( m_AdvectionScaling );
            filter->SetInPlace( true );
            filter->SetReleaseDataFlag( true );
            filter->SetNumberOfIterations( m_NumIterations );
            filter->SetMaximumRMSError( 0.001 );
            filter->SetUseImageSpacing( 1 );

            // initialize the timer and update the pipeline
            qint64 startTime = QDateTime::currentMSecsSinceEpoch();
            itkTryCatch( filter->Update(), "Threaded LevelSet Segmentation in 3D" );

            // print information about the currently processed image region
            Logger::GetInstance()->WriteLine("- LevelSetSegmentation: Segmenting Region [index: (" +
                QString::number(index[0]) + "," + QString::number(index[1]) + "," + QString::number(index[2]) +
                "), size: (" + QString::number(size[0]) +
                "," + QString::number(size[1]) + "," + QString::number(size[2]) +
                ")] with Thread Nr: " + QString::number(threadId) +
                " took " + QString::number( (QDateTime::currentMSecsSinceEpoch() - startTime) / 1000.0 ) + " seconds." );

            // threshold the level sets result to get the final segmentation
            typename BinaryThresholdImageFilterType::Pointer thresholdFilter = BinaryThresholdImageFilterType::New();
            thresholdFilter->SetInput( filter->GetOutput() );
            thresholdFilter->SetLowerThreshold( -1000.0 );
            thresholdFilter->SetUpperThreshold( 0.0 );
            thresholdFilter->SetInsideValue( 1.0 );
            thresholdFilter->SetOutsideValue( 0.0 );
            thresholdFilter->SetNumberOfWorkUnits( 1 );
            thresholdFilter->SetInPlace( true );
            thresholdFilter->SetReleaseDataFlag( true );

            // update the threshold filter
            itkTryCatch( thresholdFilter->Update(), "LevelSet Thresholding for Segmentation" );

            // split connected regions using watershed
            typename TImageType::Pointer resultImage;
            if (m_SplitConnectedBlobs == true)
            {
                typename itk::RescaleIntensityImageFilter<TImageType, itk::Image<unsigned short, TImageType::ImageDimension> >::Pointer rescaleFilter = itk::RescaleIntensityImageFilter<TImageType, itk::Image<unsigned short, TImageType::ImageDimension> >::New();
                rescaleFilter->SetInput( thresholdFilter->GetOutput() );
                rescaleFilter->SetOutputMinimum( 0 );
                rescaleFilter->SetOutputMaximum( 65535 );
                rescaleFilter->SetInPlace( true );
                rescaleFilter->SetReleaseDataFlag( true );
                typename SplitConnectedBlobsImageFilterType::Pointer splitConnectedBlobsFilter = SplitConnectedBlobsImageFilterType::New();
                splitConnectedBlobsFilter->SetInput( rescaleFilter->GetOutput() );
                splitConnectedBlobsFilter->SetBinaryOutput( true );
                splitConnectedBlobsFilter->SetReleaseDataFlag( true );
                splitConnectedBlobsFilter->SetLevel( 0.9 );
                typename itk::RescaleIntensityImageFilter<itk::Image<unsigned short, TImageType::ImageDimension> , TImageType>::Pointer rescaleFilter2 = itk::RescaleIntensityImageFilter<itk::Image<unsigned short, TImageType::ImageDimension> , TImageType>::New();
                rescaleFilter2->SetInput( splitConnectedBlobsFilter->GetOutput() );
                rescaleFilter2->SetOutputMinimum( 0 );
                rescaleFilter2->SetOutputMaximum( 1 );
                rescaleFilter2->SetReleaseDataFlag( true );
                rescaleFilter2->SetInPlace( true );

                // reinit timer and update the pipeline
                startTime = QDateTime::currentMSecsSinceEpoch();
                itkTryCatch( rescaleFilter2->Update(), "LevelSet Splitting Connected Nuclei for Segmentation" );
                Logger::GetInstance()->WriteLine("- SplitConnectedBlobs: Splitting Region [index: (" +
                    QString::number(index[0]) + "," + QString::number(index[1]) + "," + QString::number(index[2]) +
                    "), size: (" + QString::number(size[0]) +
                    "," + QString::number(size[1]) + "," + QString::number(size[2]) +
                    ")] with Thread Nr: " + QString::number(threadId) +
                    " took " + QString::number( (QDateTime::currentMSecsSinceEpoch() - startTime) / 1000.0 ) + " seconds." );

                resultImage = rescaleFilter2->GetOutput();
            }
            else
            {
                resultImage = thresholdFilter->GetOutput();
            }

            // initialize the seed region iterator
            ImageRegionIterator<TImageType> lseIteratorIterator( resultImage, outputRegion );
            ImageRegionIterator<TImageType> outputIterator( output, outputRegion );

            lseIteratorIterator.GoToBegin();
            outputIterator.GoToBegin();

            // iterate trough the seed region and set voxels for the initial LSF
            while (outputIterator.IsAtEnd() == false)
            {
                outputIterator.Set( lseIteratorIterator.Value() );

                ++outputIterator;
                ++lseIteratorIterator;
            }
        }
    }
}


// after threaded generate data
template <class TImageType>
void SeedBasedLevelSetsImageFilter<TImageType>::AfterThreadedGenerateData()
{
    // maybe peform post processing here?
}

} // end namespace itk
