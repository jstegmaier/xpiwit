/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef __XPIWITCORE_SLICEBYSLICEINTERSECTIONSIMAGEFILTER_H
#define __XPIWITCORE_SLICEBYSLICEINTERSECTIONSIMAGEFILTER_H

// include required headers
#include "../Base/Management/ITKDefinitions.h"

#include "itkExtractImageFilter.h"
#include "itkBinaryImageToLabelMapFilter.h"
#include "itkLabelGeometryImageFilter.h"
#include "itkLabelMapToLabelImageFilter.h"
#include <string>

using namespace XPIWIT;

namespace itk {

template <class TImageType, class TOutputImage>
class ITK_EXPORT SliceBySliceIntersectionsImageFilter : public ImageToImageFilter<TImageType, TOutputImage>
{
	public:
		/** Extract dimension from input and output image. */
		itkStaticConstMacro(ImageDimension, unsigned int, TImageType::ImageDimension);

		typedef SliceBySliceIntersectionsImageFilter Self;
		typedef ImageToImageFilter<TImageType,TOutputImage> Superclass;
		typedef SmartPointer<Self> Pointer;
		typedef SmartPointer<const Self> ConstPointer;

		itkNewMacro(Self);

		itkGetMacro( DebugOutput, bool);
        itkSetMacro( DebugOutput, bool);
		itkGetMacro( MinSlice, unsigned int);
        itkSetMacro( MinSlice, unsigned int);
		itkGetMacro( MaxSlice, unsigned int);
        itkSetMacro( MaxSlice, unsigned int);

		void SetInputMetaFilter( MetaDataFilter *metaFilter ) { m_InputMetaFilter = metaFilter; }
        void SetOutputMetaFilter( MetaDataFilter *metaFilter ) { m_OutputMetaFilter = metaFilter; }
		
		typedef TImageType InputImageType;
		typedef typename TImageType::PixelType PixelType;
		typedef typename itk::Image< unsigned int, TImageType::ImageDimension >  LabelType;
		typedef typename itk::Image< unsigned int, 2 >  LabelHistogramType;
		typedef typename TImageType::RegionType InputImageRegionType;
		typedef typename TOutputImage::RegionType OutputImageRegionType;
		

	protected:
		SliceBySliceIntersectionsImageFilter();
		virtual ~SliceBySliceIntersectionsImageFilter();
		
		/**
		 * Functions for data generation.
		 */
		void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId) override;
		//unsigned int SplitRequestedRegion(unsigned int i, unsigned int num, OutputImageRegionType& splitRegion);

		/** 
		 * functions for prepareing and post-processing the threaded generation.
		 */
		void BeforeThreadedGenerateData() override;
		void AfterThreadedGenerateData() override;

		bool m_DebugOutput;
		unsigned int m_MinimumLabel;
		unsigned int m_MinSlice;
		unsigned int m_MaxSlice;
		
		QList< QList< QList<float> > >  m_SegmentFeatures;		// represent features as a 3D data structure
		QList< QList< QList<float> > >  m_IntersectionFeatures;	// represent intersections as 3D data structure (#threads as 3rd dimension)

		MetaDataFilter *m_InputMetaFilter;					// key points input meta data
        MetaDataFilter *m_OutputMetaFilter;					// region props output
};

} /* namespace itk */

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkSliceBySliceIntersectionsImageFilter.hxx"
#endif

#endif
