/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef __XPIWIT_GRADIENTVECTORFLOWTRACKINGIMAGEFILTER_HXX
#define __XPIWIT_GRADIENTVECTORFLOWTRACKINGIMAGEFILTER_HXX

// include required headers
#include "itkGradientVectorFlowTrackingImageFilter.h"
#include "itkImageRegionIterator.h"
#include "itkProgressReporter.h"
#include "../../Core/Utilities/Logger.h"

using namespace XPIWIT;

// namespace itk
namespace itk
{

// the default constructor
template< class TInputImage, class TOutputImage >
GradientVectorFlowTrackingImageFilter< TInputImage, TOutputImage >::GradientVectorFlowTrackingImageFilter()
{
	this->DynamicMultiThreadingOff();
}


// the destructor
template< class TInputImage, class TOutputImage >
GradientVectorFlowTrackingImageFilter< TInputImage, TOutputImage >::~GradientVectorFlowTrackingImageFilter()
{

}


// before threaded generate data
template< class TInputImage, class TOutputImage >
void GradientVectorFlowTrackingImageFilter< TInputImage, TOutputImage >::BeforeThreadedGenerateData()
{
    // get the input
    typename TInputImage::ConstPointer input  = this->GetInput();
    typename TInputImage::RegionType largestPossibleRegion = input->GetLargestPossibleRegion();
    
    // initialize the seed image for highlighting the sinks
    mSeedImage = LabelImageType::New();
    mSeedImage->SetBufferedRegion( largestPossibleRegion );
    mSeedImage->Allocate();
    mSeedImage->FillBuffer(0);
    mSeedImage->SetReleaseDataFlag( true );
        
    mIndexImage = IndexImageType::New();
    mIndexImage->SetLargestPossibleRegion( largestPossibleRegion );
    mIndexImage->SetBufferedRegion( largestPossibleRegion );
    mIndexImage->Allocate();
    mIndexImage->SetReleaseDataFlag( true );

    // fill the output buffer with zeros
    typename OutputImageType::Pointer output = this->GetOutput();
    output->FillBuffer(0);
}


// the thread generate data
template< class TInputImage, class TOutputImage >
void GradientVectorFlowTrackingImageFilter< TInputImage, TOutputImage >::ThreadedGenerateData(const OutputImageRegionType & outputRegionForThread, ThreadIdType threadId)
{
    // define input and output image pointers
    typename InputImageType::ConstPointer input  = this->GetInput();
    typename OutputImageType::Pointer output = this->GetOutput();
    typename TInputImage::RegionType largestPossibleRegion = input->GetLargestPossibleRegion();

    // initialize the index image with the actual voxel index
    itk::ImageRegionIterator<IndexImageType> indexImageIterator(mIndexImage, outputRegionForThread);
    indexImageIterator.GoToBegin();

    while (!indexImageIterator.IsAtEnd())
    {
        indexImageIterator.Set(indexImageIterator.GetIndex());
        ++indexImageIterator;
    }
    
    // create the mask image iterators
    itk::ImageRegionIterator<TInputImage> maskIterator(mMaskImage, outputRegionForThread);
    
    // auxiliary variables for computing the next indices
    typename TInputImage::IndexType currentIndex;
    typename TInputImage::IndexType nextIndex;
    float gradient[TInputImage::ImageDimension];
    typename TInputImage::Pointer flowImages[3];
    flowImages[0] = mFlowXImage;
    flowImages[1] = mFlowYImage;
    flowImages[2] = mFlowZImage;
    
    // set z-flow to zero for 2D images
    if (TInputImage::ImageDimension > 2 && largestPossibleRegion.GetSize(2) == 1)
        flowImages[2]->FillBuffer(0);
    
    // propagate indices along the gradient field for a predefined number of iterations
    // number of iterations should be slightly larger than the maximum expected cell radius
    for (int i=0; i<m_NumIterations; ++i)
    {
        // reset iterators
        maskIterator.GoToBegin();
        indexImageIterator.GoToBegin();
                
        // perform one step of index propagation
        while (!indexImageIterator.IsAtEnd())
        {
            // only process foreground voxels if enabled
            if (maskIterator.Value() > 0 || m_IgnoreBackground == false)
            {
                // get the moved index at the current iterator location
                currentIndex = indexImageIterator.Get();

                // check if proposed next pixel is valid
                if (largestPossibleRegion.IsInside(currentIndex))
                {
                    for (int j=0; j<TInputImage::ImageDimension; ++j)
                    {
                        // get the gradient at the moved location
                        gradient[j] = flowImages[j]->GetPixel(currentIndex);
                
                        // convert float gradient to sign (-1,0,1) for the step
                        gradient[j] = (gradient[j] > 0) ? 1 : ((gradient[j] < 0) ? -1 : 0);
                
                        // compute next moved index by going one step towards the sink
                        nextIndex[j] = std::max<int>(0, std::min<int>(currentIndex[j] - gradient[j], largestPossibleRegion.GetSize(j)));
                    }
                
                    // update the current iterator position to the new moved index
                    indexImageIterator.Set(nextIndex);

                    // set sink voxels in the last iteration
                    if (i >= (m_NumIterations-1) && largestPossibleRegion.IsInside(nextIndex))
                        mSeedImage->SetPixel(nextIndex, 1);
                }
            }
            
            // increment iterators
            ++maskIterator;
            ++indexImageIterator;
        }
    }
}


// after threaded generate data
template< class TInputImage, class TOutputImage >
void GradientVectorFlowTrackingImageFilter< TInputImage, TOutputImage >::AfterThreadedGenerateData()
{
    // get the output
    typename InputImageType::ConstPointer input  = this->GetInput();
    typename OutputImageType::Pointer output = this->GetOutput();
    typename TInputImage::RegionType largestPossibleRegion = input->GetLargestPossibleRegion();
    
    // create structuring element
    typedef itk::BinaryBallStructuringElement< typename TInputImage::PixelType, TInputImage::ImageDimension> StructuringElementType;
    StructuringElementType structuringElement;
    structuringElement.SetRadius( m_ClosingRadius );
    structuringElement.CreateStructuringElement();

    // setup the morphological closing filter
    typedef itk::BinaryMorphologicalClosingImageFilter<LabelImageType, LabelImageType, StructuringElementType> FilterType;
    typename FilterType::Pointer closingFilter = FilterType::New();
    closingFilter->SetInput( mSeedImage );
    closingFilter->SetKernel( structuringElement );
    closingFilter->SetForegroundValue( 1 );
    closingFilter->SetReleaseDataFlag( true );
    itkTryCatch(closingFilter->Update(), "Error: BinaryMorphologicalClosingImageFilterWrapper Update Function.");
    
    // setup the binary image to label map filter
    typedef itk::BinaryImageToLabelMapFilter< LabelImageType > BinaryImageToLabelMapFilterType;
    typename BinaryImageToLabelMapFilterType::Pointer binaryImageToLabelMapFilter = BinaryImageToLabelMapFilterType::New();
    binaryImageToLabelMapFilter->SetInput( closingFilter->GetOutput() );
    binaryImageToLabelMapFilter->SetReleaseDataFlag( true );
    binaryImageToLabelMapFilter->SetInputForegroundValue( 1 );
    binaryImageToLabelMapFilter->SetOutputBackgroundValue( 0 );
    binaryImageToLabelMapFilter->SetFullyConnected( m_FullyConnected );
    itkTryCatch( binaryImageToLabelMapFilter->Update(), "Error: BinaryImageToLabelMapFilter update.");
 
    // convert the label map into a label image
    typedef itk::LabelMapToLabelImageFilter< typename BinaryImageToLabelMapFilterType::OutputImageType, LabelImageType > LabelMapToLabelImageFilterType;
    typename LabelMapToLabelImageFilterType::Pointer labelMapToLabelImageFilter = LabelMapToLabelImageFilterType::New();
    labelMapToLabelImageFilter->SetInput( binaryImageToLabelMapFilter->GetOutput() );
    labelMapToLabelImageFilter->SetReleaseDataFlag( true );
    itkTryCatch( labelMapToLabelImageFilter->Update(), "Error: LabelMapToLabelImageFilter update.");
    typename LabelImageType::Pointer labelImage = labelMapToLabelImageFilter->GetOutput();
    
    // allocate result image
    typename TInputImage::Pointer resultImage = TInputImage::New();
    resultImage->SetBufferedRegion( largestPossibleRegion );
    resultImage->Allocate();
    resultImage->FillBuffer(0);
    resultImage->SetReleaseDataFlag( false );
    
    // initialize result image iterator
    itk::ImageRegionIterator<IndexImageType> indexImageIterator(mIndexImage, largestPossibleRegion);
    itk::ImageRegionIterator<TInputImage> resultImageIterator(output, largestPossibleRegion);
    itk::ImageRegionIterator<TInputImage> maskIterator(mMaskImage, largestPossibleRegion);
    indexImageIterator.GoToBegin();
    resultImageIterator.GoToBegin();
    maskIterator.GoToBegin();

    typename TInputImage::IndexType currentIndex;
    
    // create the final label image
    while (!resultImageIterator.IsAtEnd())
    {
        // get the current index
        currentIndex = indexImageIterator.Value();

        // propagate the seed label to all voxels that map to the same sink
        if ((maskIterator.Value() > 0 || !m_IgnoreBackground) && largestPossibleRegion.IsInside(currentIndex))
            resultImageIterator.Set(labelImage->GetPixel(currentIndex));
        
        // increment iterators
        ++maskIterator;
        ++indexImageIterator;
        ++resultImageIterator;
    }

    // print success message
    Logger::GetInstance()->WriteLine( "+ GradientVectorFlowTrackingImageFilter finished" );
}

} // end namespace itk

#endif
