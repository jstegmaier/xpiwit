/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef __XPIWIT_SEEDBASEDTWANGSEGMENTATIONFILTER_H
#define __XPIWIT_SEEDBASEDTWANGSEGMENTATIONFILTER_H

// include required headers
#include "../Base/Management/ITKDefinitions.h"
#include "itkExtractKeyPointsImageFilter.h"
#include "itkExtractRegionPropsImageFilter.h"
#include "itkGradientMagnitudeRecursiveGaussianImageFilter.h"
#include "itkGradientMagnitudeImageFilter.h"
#include "itkBoundedReciprocalImageFilter.h"
//#include "itkMultiplyByConstantImageFilter.h"
#include "itkMultiplyImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkExtractImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkMacro.h"
#include <string>

#include "../Base/MetaData/MetaDataFilter.h"

using namespace XPIWIT;

namespace itk
{

/**
 * @class SeedBasedTwandSegmentationFilter
 * Fast seed based segmentation filter, e.g. to extract stained nuclei from microscopy images.
 * Yields a relatively good tradeoff between speed vs. processing time and is therefore suited for
 * processing large image stacks. The filter has to be provided with a seed point file.
 */
template <class TImageType>
class ITK_EXPORT SeedBasedTwangSegmentationFilter : public ImageToImageFilter<TImageType, TImageType>
{
    public:
        // Extract dimension from input and output image.
        itkStaticConstMacro(ImageDimension, unsigned int, TImageType::ImageDimension);
        typedef SeedBasedTwangSegmentationFilter Self;
        typedef ImageToImageFilter<TImageType,TImageType> Superclass;
        typedef SmartPointer<Self> Pointer;
        typedef SmartPointer<const Self> ConstPointer;
        itkNewMacro(Self);

        // definition of variable type abbreviations
        typedef typename TImageType::PixelType PixelType;
        typedef SeedPoint<TImageType> SeedPointType;
        typedef RegionProps RegionPropsType;
        typedef typename TImageType::RegionType InputImageRegionType;
        typedef typename TImageType::RegionType OutputImageRegionType;

        /**
         * The default constructor.
         */
        SeedBasedTwangSegmentationFilter();

        /**
         * The destructor.
         */
        virtual ~SeedBasedTwangSegmentationFilter();

        /**
         * Set and get methods for the member variables.
         */
        itkGetMacro( Segment3D, bool);
        itkSetMacro( Segment3D, bool);
        itkGetMacro( FuseSeedPoints, bool);
        itkSetMacro( FuseSeedPoints, bool);
        itkGetMacro( LabelOutput, bool);
        itkSetMacro( LabelOutput, bool);
		itkGetMacro( UseOriginalID, bool);
		itkSetMacro( UseOriginalID, bool);
        itkGetMacro( RandomLabels, bool);
        itkSetMacro( RandomLabels, bool);
        itkGetMacro( WriteRegionProps, bool);
        itkSetMacro( WriteRegionProps, bool);
        itkSetMacro( NumKeyPoints, int );
        itkSetMacro( MinimumRegionSigma, float );
        itkGetMacro( MinimumRegionSigma, float );
        itkSetMacro( MinimumWeightedGradientNormalDotProduct, float );
        itkGetMacro( MinimumWeightedGradientNormalDotProduct, float );
        itkSetMacro( WeightingKernelSizeMultiplicator, float );
        itkGetMacro( WeightingKernelSizeMultiplicator, float );
        itkSetMacro( WeightingKernelStdDev, float );
        itkGetMacro( WeightingKernelStdDev, float );
        itkSetMacro( GradientImageStdDev, float );
        itkGetMacro( GradientImageStdDev, float );
        itkSetMacro( SeedPointMinDistance, float );
        itkGetMacro( SeedPointMinDistance, float );
        itkGetMacro( NumKeyPoints, int );

        void SetInputMetaFilter( MetaDataFilter *metaFilter ) { m_InputMetaFilter = metaFilter; }
        void SetOutputMetaFilter( MetaDataFilter *metaFilter ) { m_OutputMetaFilter = metaFilter; }

    protected:

        /**
         * Functions for data generation.
         * @param outputRegionForThread the region to be processed.
         * @param threadId the id of the current thread.
         */
        void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId) override;

        /**
         * functions for prepareing and post-processing the threaded generation.
         */
        void BeforeThreadedGenerateData() override;
        void AfterThreadedGenerateData() override;

        float m_MinimumRegionSigma;						// minimum standard deviation of the intensity values of an extracted region

        float m_MinimumWeightedGradientNormalDotProduct;		// specifies the minimum of the weighted dot product and gradient normal
        float m_WeightingKernelSizeMultiplicator;							// specifies the radius of the core of the weighting kernel
        float m_WeightingKernelStdDev;							// standard deviation of the gaussian of the weighting kernel
        float m_GradientImageStdDev;							// the standard deviation for gaussian blurring before gradient calculation

        bool m_WriteRegionProps;								// if not checked region props are not stored
        bool m_Segment3D;								// (NOT IMPLEMENTED YET) for 2D processing of the algorithm
        bool m_FuseSeedPoints;							// indicates if nearby seed points should be merged
        bool m_LabelOutput;								// if set to true, the result image contains unique labels for each segment, otherwise the original intensity is used
		bool m_UseOriginalID;							// if set to true, the same IDs as the provided seed points are used (default TRUE).
        bool m_RandomLabels;							// if set to true, random intensities are used for each label, otherwise succesive integers are used
        float m_SeedPointMinDistance;					// the minimum distance of seedpoints. close seed points will be merged.
        int   m_NumKeyPoints;							// number of seed points that should be processed
        std::vector<SeedPointType>* m_SeedPoints;		// the actual location info of the seeds
        std::vector<int>* m_SeedPointCombinations;		// the actual location info of the seeds
        std::vector<RegionPropsType>* m_RegionProps;	// the extracted region properties

        MetaDataFilter *m_InputMetaFilter;              // key points input meta data
        MetaDataFilter *m_OutputMetaFilter;             // region props output
};

} // namespace itk

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkSeedBasedTwangSegmentationFilter.hxx"
#endif

#endif
