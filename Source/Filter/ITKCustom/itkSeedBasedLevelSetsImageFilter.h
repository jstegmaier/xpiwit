/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef __XPIWIT_SEEDBASEDLEVELSETS2DIMAGEFILTER_H
#define __XPIWIT_SEEDBASEDLEVELSETS2DIMAGEFILTER_H

// include required headers
#include "../Base/Management/ITKDefinitions.h"
#include "itkExtractKeyPointsImageFilter.h"
#include "itkGeodesicActiveContourLevelSetImageFilter.h"
#include "itkLaplacianSegmentationLevelSetImageFilter.h"
#include "itkGradientMagnitudeRecursiveGaussianImageFilter.h"
#include "itkBoundedReciprocalImageFilter.h"
//#include "itkMultiplyByConstantImageFilter.h"
#include "itkMultiplyImageFilter.h"
#include "itkConfidenceConnectedImageFilter.h"
#include "itkIsolatedConnectedImageFilter.h"
#include "itkCurvatureAnisotropicDiffusionImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkSplitConnectedBlobsImageFilter.h"
#include "itkExtractImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include <string>
#include "../Base/MetaData/MetaDataFilter.h"

using namespace XPIWIT;

namespace itk {

template <class TImageType>
class ITK_EXPORT SeedBasedLevelSetsImageFilter : public ImageToImageFilter<TImageType, TImageType>
{
	public:
		/** Extract dimension from input and output image. */
		itkStaticConstMacro(ImageDimension, unsigned int, TImageType::ImageDimension);

		typedef SeedBasedLevelSetsImageFilter Self;
		typedef ImageToImageFilter<TImageType,TImageType> Superclass;
		typedef SmartPointer<Self> Pointer;
		typedef SmartPointer<const Self> ConstPointer;

		itkNewMacro(Self);

		typedef TImageType InputImageType;
		typedef typename TImageType::PixelType PixelType;
		typedef SeedPoint<TImageType> SeedPointType;

		typedef typename TImageType::RegionType InputImageRegionType;
		typedef typename TImageType::RegionType OutputImageRegionType;


		itkGetMacro( Sigma, float);
		itkSetMacro( Sigma, float);
		itkGetMacro( PropagationScaling, float);
		itkSetMacro( PropagationScaling, float);
		itkGetMacro( AdvectionScaling, float);
		itkSetMacro( AdvectionScaling, float);
		itkGetMacro( CurvatureScaling, float);
		itkSetMacro( CurvatureScaling, float);
		itkGetMacro( NumIterations, int);
		itkSetMacro( NumIterations, int);
		itkGetMacro( OverlapSize, int);
		itkSetMacro( OverlapSize, int);
		itkGetMacro( Segment3D, bool);
		itkSetMacro( Segment3D, bool);
		itkSetMacro( InitialLSFValue, PixelType );
		itkGetMacro( InitialLSFValue, PixelType );
		itkSetMacro( EdgeMapFactor, float );
		itkGetMacro( EdgeMapFactor, float );
		itkSetMacro( SplitConnectedBlobs, bool );
		itkGetMacro( SplitConnectedBlobs, bool );

		void SetKeyPoints( MetaDataFilter *metaFilter ) { m_KeyPoints = metaFilter; }

	protected:
		SeedBasedLevelSetsImageFilter();
		virtual ~SeedBasedLevelSetsImageFilter();

		typedef typename itk::GeodesicActiveContourLevelSetImageFilter<TImageType, TImageType>			LevelSetImageFilterType;
		typedef typename itk::GradientMagnitudeRecursiveGaussianImageFilter<TImageType, TImageType>		GradientMagnitudeRecursiveGaussianImageFilterType;
		typedef typename itk::BoundedReciprocalImageFilter<TImageType, TImageType>						BoundedReciprocalImageFilterType;
		typedef typename itk::ConfidenceConnectedImageFilter<TImageType, TImageType>					ConfidenceConnectedImageFilterType;
		typedef typename itk::IsolatedConnectedImageFilter<TImageType, TImageType>						IsolateConnectedFilterType;
		typedef typename itk::CurvatureAnisotropicDiffusionImageFilter<TImageType, TImageType>			CurvatureAnisotropicDiffusionImageFilterType;
		typedef typename itk::ExtractImageFilter<TImageType, TImageType>								ExtractImageFilterType;
		typedef typename itk::BinaryThresholdImageFilter<TImageType, TImageType>						BinaryThresholdImageFilterType;
		typedef typename itk::SplitConnectedBlobsImageFilter<itk::Image<unsigned short, TImageType::ImageDimension > >	 SplitConnectedBlobsImageFilterType;

		/**
		 * Functions for data generation.
		 */
		void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId);
		void GenerateInitialLSF();
		unsigned int SplitRequestedRegion(unsigned int i, unsigned int num, OutputImageRegionType& splitRegion);

		/** 
		 * functions for prepareing and post-processing the threaded generation.
		 */
		void BeforeThreadedGenerateData() override;
		void AfterThreadedGenerateData() override;
		
		float m_Sigma;
		float m_EdgeMapFactor;
		bool  m_Segment3D;
		bool  m_SplitConnectedBlobs;
		std::vector<SeedPointType> m_SeedPoints;
		PixelType m_InitialLSFValue;
		float m_PropagationScaling;
		float m_CurvatureScaling;
		float m_AdvectionScaling;
		int m_NumIterations;
		int m_OverlapSize;
		typename InputImageType::Pointer mEdgeMap;
		typename InputImageType::Pointer mInitialLSF;
		MetaDataFilter *m_KeyPoints;
};

} /* namespace itk */

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkSeedBasedLevelSetsImageFilter.hxx"
#endif

#endif
