/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef __XPIWIT_DOMIMAGEFILTER_HXX
#define __XPIWIT_DOMIMAGEFILTER_HXX

// include required headers
#include "itkNeighborhoodAlgorithm.h"
#include "itkProgressAccumulator.h"
#include "itkBoxUtilities.h"

#include "itkDoMImageFilter.h"

// namespace itk
namespace itk
{

// the default constructor
template <class TImageType>
DoMImageFilter<TImageType>::DoMImageFilter()
{
    for ( unsigned int i = 0; i < TImageType::ImageDimension; i++ )
    {
        minRadius[i] = 5;
        maxRadius[i] = 10;
        stepSize[i] = 1;
    }

	this->DynamicMultiThreadingOff();
}

// the thread generate data
template <class TImageType>
void DoMImageFilter<TImageType>::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId)
{


    typename TImageType::SizeType internalRadius;
    for ( unsigned int i = 0; i < TImageType::ImageDimension; i++ )
    {
        internalRadius[i] = maxRadius[i] + 1;
    }

    const InputImageType *inputImage = this->GetInput();
    OutputImageType *     outputImage = this->GetOutput();
    RegionType            accumRegion = outputRegionForThread;
    accumRegion.PadByRadius( internalRadius );
    accumRegion.Crop( inputImage->GetRequestedRegion() );

    ProgressReporter progress( this, threadId, 2 * accumRegion.GetNumberOfPixels() );

    typename AccumImageType::Pointer accImage = AccumImageType::New();
    accImage->SetRegions(accumRegion);
    accImage->Allocate();

     BoxAccumulateFunction< TImageType, AccumImageType >(inputImage, accImage,
                                                        accumRegion, accumRegion);

    BoxDoMCalculatorFunction(accImage.GetPointer(), outputImage,
                             accumRegion, outputRegionForThread,
                             minRadius, maxRadius,
                             progress);
     

    /*
    typename TImageType::Pointer smallMeanImage = TImageType::New();
    smallMeanImage->SetRegions(meanRegion);
    smallMeanImage->Allocate();

    typename TImageType::Pointer bigMeanImage = TImageType::New();
    bigMeanImage->SetRegions(meanRegion);
    bigMeanImage->Allocate();

    BoxMeanCalculatorFunction< AccumImageType, TImageType >(accImage.GetPointer(), smallMeanImage,
                                                            accumRegion,
                                                            outputRegionForThread,
                                                            minRadius,
                                                            progress);

    BoxMeanCalculatorFunction< AccumImageType, TImageType >(accImage.GetPointer(), bigMeanImage,
                                                            accumRegion,
                                                            outputRegionForThread,
                                                            maxRadius,
                                                            progress);

    BoxDifferenceFilter(smallMeanImage, bigMeanImage, outputImage,
                        outputRegionForThread, outputRegionForThread,
                        progress);
                        */
}

template <class TImageType>
void DoMImageFilter<TImageType>::BoxDoMCalculatorFunction(const AccumImageType *accImage,
                                                          TImageType *outputImage,
                                                          typename AccumImageType::RegionType inputRegion,
                                                          typename TImageType::RegionType outputRegion,
                                                          typename TImageType::SizeType RadiusSmall,
                                                          typename TImageType::SizeType Radius,
                                                          ProgressReporter & progress)
{
    // typedefs
    typedef AccumImageType                           InputImageType;
    typedef typename AccumImageType::RegionType      RegionType;
    typedef typename AccumImageType::SizeType        SizeType;
    typedef typename AccumImageType::IndexType       IndexType;
    typedef typename AccumImageType::PixelType       PixelType;
    typedef typename AccumImageType::OffsetType      OffsetType;
    typedef TImageType                               OutputImageType;
    typedef typename TImageType::PixelType           OutputPixelType;
    typedef typename AccumImageType::PixelType       InputPixelType;
    // use the face generator for speed
    typedef NeighborhoodAlgorithm::ImageBoundaryFacesCalculator< InputImageType > FaceCalculatorType;
    typedef typename FaceCalculatorType::FaceListType                             FaceListType;
    typedef typename FaceCalculatorType::FaceListType::iterator                   FaceListTypeIt;
    FaceCalculatorType faceCalculator;

    FaceListType                                    faceList;
    FaceListTypeIt                                  fit;
    ZeroFluxNeumannBoundaryCondition< InputImageType > nbc;

    // this process is actually slightly asymmetric because we need to
    // subtract rectangles that are next to our kernel, not overlapping it
    SizeType kernelSize;
    SizeType kernelSizeSmall;
    SizeType internalRadius;
    SizeType RegionLimit;

    IndexType RegionStart = inputRegion.GetIndex();
    for ( unsigned int i = 0; i < InputImageType::ImageDimension; i++ )
    {
        kernelSize[i] = Radius[i] * 2 + 1;
        kernelSizeSmall[i] = RadiusSmall[i] * 2 + 1;
        internalRadius[i] = Radius[i] + 1;
        RegionLimit[i] = inputRegion.GetSize()[i] + RegionStart[i] - 1;
    }

    typedef typename NumericTraits< OutputPixelType >::RealType AccPixType;
    // get a set of offsets to corners for a unit hypercube in this image
    std::vector< OffsetType > UnitCorners = CornerOffsets< InputImageType >(accImage);
    std::vector< OffsetType > RealCornersLarge;
    std::vector< OffsetType > RealCornersSmall;
    std::vector< AccPixType > Weights;
    // now compute the weights
    for ( unsigned int k = 0; k < UnitCorners.size(); k++ )
    {
        int        prod = 1;
        OffsetType ThisCornerLarge;
        OffsetType ThisCornerSmall;
        for ( unsigned int i = 0; i < InputImageType::ImageDimension; i++ )
        {
            prod *= UnitCorners[k][i];
            if ( UnitCorners[k][i] > 0 )
            {
                ThisCornerLarge[i] = Radius[i];
                ThisCornerSmall[i] = RadiusSmall[i];
            }
            else
            {
                ThisCornerLarge[i] = -( Radius[i] + 1 );
                ThisCornerSmall[i] = -( RadiusSmall[i] + 1 );
            }
        }
        Weights.push_back( (AccPixType)prod );
        RealCornersLarge.push_back( ThisCornerLarge );
        RealCornersSmall.push_back( ThisCornerSmall );
    }

    faceList = faceCalculator(accImage, outputRegion, internalRadius);
    // start with the body region
    for ( fit = faceList.begin(); fit != faceList.end(); ++fit )
    {
        if ( fit == faceList.begin() )
        {
            // this is the body region. This is meant to be an optimized
            // version that doesn't use neighborhood regions
            // compute the various offsets
            AccPixType pixelscount = 1;
            AccPixType pixelscountSmall = 1;
            for ( unsigned int i = 0; i < InputImageType::ImageDimension; i++ )
            {
                pixelscount *= (AccPixType)( 2 * Radius[i] + 1 );
                pixelscountSmall *= (AccPixType)( 2 * RadiusSmall[i] + 1 );
            }

            typedef ImageRegionIterator< OutputImageType >     OutputIteratorType;
            typedef ImageRegionConstIterator< InputImageType > InputIteratorType;

            typedef std::vector< InputIteratorType > CornerItVecType;
            CornerItVecType CornerItVec;
            CornerItVecType CornerItVecSmall;
            // set up the iterators for each corner
            for ( unsigned int k = 0; k < RealCornersLarge.size(); k++ )
            {
                typename InputImageType::RegionType tReg = ( *fit );
                tReg.SetIndex(tReg.GetIndex() + RealCornersLarge[k]);
                InputIteratorType tempIt(accImage, tReg);
                tempIt.GoToBegin();
                CornerItVec.push_back(tempIt);

                typename InputImageType::RegionType tRegSmall = ( *fit );
                tRegSmall.SetIndex(tRegSmall.GetIndex() + RealCornersSmall[k]);
                InputIteratorType tempItSmall(accImage, tRegSmall);
                tempItSmall.GoToBegin();
                CornerItVecSmall.push_back(tempItSmall);
            }
            // set up the output iterator
            OutputIteratorType oIt(outputImage, *fit);
            // now do the work
            for ( oIt.GoToBegin(); !oIt.IsAtEnd(); ++oIt )
            {
                AccPixType Sum = 0;
                AccPixType SumSmall = 0;
                // check each corner
                for ( unsigned int k = 0; k < CornerItVec.size(); k++ )
                {
                    Sum += Weights[k] * CornerItVec[k].Get();
                    SumSmall += Weights[k] * CornerItVecSmall[k].Get();

                    // increment each corner iterator
                    ++( CornerItVec[k] );
                    ++( CornerItVecSmall[k] );
                }
                oIt.Set( static_cast< OutputPixelType >( ( SumSmall / pixelscountSmall ) ) - static_cast< OutputPixelType >( ( Sum / pixelscount ) ) );
                progress.CompletedPixel();
            }
        }
        else
        {
            // now we need to deal with the border regions
            typedef ImageRegionIteratorWithIndex< OutputImageType > OutputIteratorType;
            OutputIteratorType oIt(outputImage, *fit);
            // now do the work
            for ( oIt.GoToBegin(); !oIt.IsAtEnd(); ++oIt )
            {
                // figure out the number of pixels in the box by creating an
                // equivalent region and cropping - this could probably be
                // included in the loop below.
                RegionType currentKernelRegion;
                RegionType currentKernelRegionSmall;
                currentKernelRegion.SetSize(kernelSize);
                currentKernelRegionSmall.SetSize(kernelSizeSmall);
                // compute the region's index
                IndexType kernelRegionIdx = oIt.GetIndex();
                IndexType CentIndex = kernelRegionIdx;
                for ( unsigned int i = 0; i < InputImageType::ImageDimension; i++ )
                {
                    kernelRegionIdx[i] -= Radius[i];
                }
                currentKernelRegion.SetIndex(kernelRegionIdx);
                currentKernelRegion.Crop(inputRegion);
                OffsetValueType edgepixelscount = currentKernelRegion.GetNumberOfPixels();

                currentKernelRegionSmall.SetIndex(kernelRegionIdx);
                currentKernelRegionSmall.Crop(inputRegion);
                OffsetValueType edgepixelscountSmall = currentKernelRegionSmall.GetNumberOfPixels();

                AccPixType Sum = 0;
                AccPixType SumSmall = 0;

                // rules are : for each corner,
                //               for each dimension
                //                  if dimension offset is positive -> this is
                //                  a leading edge. Crop if outside the input
                //                  region
                //                  if dimension offset is negative -> this is
                //                  a trailing edge. Ignore if it is outside
                //                  image region
                for ( unsigned int k = 0; k < RealCornersLarge.size(); k++ )
                {
                    IndexType ThisCorner = CentIndex + RealCornersLarge[k];
                    IndexType ThisCornerSmall = CentIndex + RealCornersSmall[k];
                    bool      IncludeCorner = true;
                    bool      IncludeCornerSmall = true;
                    for ( unsigned int j = 0; j < InputImageType::ImageDimension; j++ )
                    {
                        if ( UnitCorners[k][j] > 0 )
                        {
                            // leading edge - crop it
                            if ( ThisCorner[j] > static_cast< OffsetValueType >( RegionLimit[j] ) )
                            {
                                ThisCorner[j] = static_cast< OffsetValueType >( RegionLimit[j] );
                            }
                            // leading edge - crop it
                            if ( ThisCornerSmall[j] > static_cast< OffsetValueType >( RegionLimit[j] ) )
                            {
                                ThisCornerSmall[j] = static_cast< OffsetValueType >( RegionLimit[j] );
                            }
                        }
                        else
                        {
                            // trailing edge - check bounds
                            if ( ThisCorner[j] < RegionStart[j] )
                            {
                                IncludeCorner = false;
                                break;
                            }
                            // trailing edge - check bounds
                            if ( ThisCornerSmall[j] < RegionStart[j] )
                            {
                                IncludeCornerSmall = false;
                                //break;
                            }
                        }
                    }
                    if ( IncludeCorner )
                    {
                        Sum += accImage->GetPixel(ThisCorner) * Weights[k];
                    }
                    /*if ( IncludeCornerSmall )
                    {
                        SumSmall += accImage->GetPixel(ThisCornerSmall) * Weights[k];
                    }*/

                }

                //oIt.Set( static_cast< OutputPixelType >( SumSmall / (AccPixType)edgepixelscountSmall ) - static_cast< OutputPixelType >( Sum / (AccPixType)edgepixelscount ) );
                oIt.Set( static_cast< OutputPixelType >( Sum / (AccPixType)edgepixelscount ) );
                progress.CompletedPixel();
            }
        }
    }
}

// doesn't work
template <class TImageType>
void DoMImageFilter<TImageType>::BoxDifferenceFilter(const InputImageType *smallMeanImage,
                                                     const InputImageType *bigMeanImage,
                                                     OutputImageType *outputImage,
                                                     RegionType inputRegion, RegionType outputRegion,
                                                     ProgressReporter &progress)
{
    typedef ImageRegionConstIterator< InputImageType > InputIteratorType;
    typedef ImageRegionIterator< OutputImageType > OutputIteratorType;

    InputIteratorType smallInputIt(smallMeanImage, inputRegion);
    InputIteratorType bigInputIt(bigMeanImage, inputRegion);
    OutputIteratorType outputIt(outputImage, outputRegion);

    smallInputIt.GoToBegin();
    bigInputIt.GoToBegin();
    outputIt.GoToBegin();

    for( smallInputIt.GoToBegin(); smallInputIt.IsAtEnd(); ++smallInputIt, ++bigInputIt, ++outputIt ){
        outputIt.Set( smallInputIt.Get() - bigInputIt.Get() );
        progress.CompletedPixel();
    }

}


} // end namespace itk

#endif
