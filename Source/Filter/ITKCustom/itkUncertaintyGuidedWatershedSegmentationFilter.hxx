/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef __XPIWIT_UncertaintyGuidedWatershedSegmentationFilter_HXX
#define __XPIWIT_UncertaintyGuidedWatershedSegmentationFilter_HXX

// include required headers
#include "itkUncertaintyGuidedWatershedSegmentationFilter.h"
#include "itkMorphologicalWatershedFromMarkersImageFilter.h"
#include "itkImageRegionIterator.h"
#include "itkImageRegionConstIterator.h"
#include <iostream>
#include <fstream>
#include <string>
#include <QString>
#include <QList>
#include <QStringList>
#include <QDateTime>
#include "../../Core/Utilities/Logger.h"

// namespace itk
namespace itk
{

// the default constructor
template <class TImageType>
UncertaintyGuidedWatershedSegmentationFilter<TImageType>::UncertaintyGuidedWatershedSegmentationFilter()
{
    // set default values for the parameters
    m_FullyConnected = false;
	m_UncertaintyThreshold = 1.0;
	m_UncertaintyCombinationFunction = 0;

    m_RegionProps = NULL;
    m_FuzzySetParameters = NULL;
	m_SeedImage = NULL;
	m_LabelImage = NULL;
	this->DynamicMultiThreadingOff();
}


// the destructor
template <class TImageType>
UncertaintyGuidedWatershedSegmentationFilter<TImageType>::~UncertaintyGuidedWatershedSegmentationFilter()
{
}


// before threaded generate data
template <class TImageType>
void UncertaintyGuidedWatershedSegmentationFilter<TImageType>::BeforeThreadedGenerateData()
{
    // Allocate output
    typename TImageType::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();
    output->FillBuffer( 0 );

	// initialize the output
/*
	typename TInputImage::Pointer outputImage = TInputImage::New();
	outputImage->SetRegions( distanceMapImage->GetLargestPossibleRegion() );
	outputImage->SetSpacing( distanceMapImage->GetSpacing() );
	outputImage->SetOrigin( distanceMapImage->GetOrigin() );
	outputImage->Allocate();
	outputImage->FillBuffer(0);*/

	// copy the input label image
	typename itk::ImageRegionConstIterator<TImageType> labelImageIterator(m_LabelImage, m_LabelImage->GetLargestPossibleRegion() );
	typename itk::ImageRegionIterator<TImageType> outputIterator(output, output->GetLargestPossibleRegion() );
	labelImageIterator.GoToBegin();
	outputIterator.GoToBegin();
	while(labelImageIterator.IsAtEnd() == false)
	{
		outputIterator.Set(labelImageIterator.Value());
		++labelImageIterator;
		++outputIterator;
	}

	// check if the region props and fuzzy parameters have been set properly
    if( m_RegionProps == NULL || m_FuzzySetParameters == NULL)
        Logger::GetInstance()->WriteLine( "- ERROR itkUncertaintyGuidedWatershedSegmentationFilter: no input meta available." );

    // get the z spacing
    typename TImageType::SpacingType spacing = input->GetSpacing();

    // get the number of threads
    int numThreads = ProcessObject::GetNumberOfWorkUnits();
	for (int i=0; i<numThreads; ++i)
		m_RegionPropsPerThread.append(QList< QList<float> >());

    // distribute seed points across available threads
	const unsigned int numRegions = m_RegionProps->mData.length();
    for (int i=0; i<numRegions; ++i)
    {
        int currentThread = i % numThreads;
        m_RegionPropsPerThread[currentThread].push_back( m_RegionProps->mData[i] );
    }
}


// the thread generate data
template <class TImageType>
void UncertaintyGuidedWatershedSegmentationFilter<TImageType>::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId)
{
	typedef TImageType TInputImage;

    // Allocate output
    typename TImageType::Pointer outputImage = this->GetOutput();
    typename TImageType::ConstPointer inputImage  = this->GetInput();
    typename TImageType::RegionType largestPossibleRegion = inputImage->GetLargestPossibleRegion();
    typename TImageType::SpacingType spacing = inputImage->GetSpacing();

    // get the number of threads
	const unsigned int idIndex = 0;
	const unsigned int volumeIndex = 1;
	const unsigned int xposIndex = 8;
	const unsigned int yposIndex = 9;
	const unsigned int zposIndex = 10;
	const unsigned int xsizeIndex = 5;
	const unsigned int ysizeIndex = 6;
	const unsigned int zsizeIndex = 7;
	unsigned int currentLabel = 0;
	float a,b,c,d, featureValue;
	unsigned int featureIndex;

	const unsigned int numRegions = m_RegionPropsPerThread[threadId].length();
	const unsigned int numFuzzySets = m_FuzzySetParameters->mData.length();
	for (unsigned int i=0; i<numRegions; ++i)
	{
		// get the current label
		currentLabel = m_RegionPropsPerThread[threadId][i][idIndex];

		// skip the background region
		if (currentLabel == 0)
			continue;

		// calculate the current uncertainty value
		float uncertainty = 1.0;
		float currentUncertainty = 1.0;
		int featureLargerThanExpected = 0;
		for (int j=0; j<numFuzzySets; ++j)
		{
			a = m_FuzzySetParameters->mData[j][1];
			b = m_FuzzySetParameters->mData[j][2];
			c = m_FuzzySetParameters->mData[j][3];
			d = m_FuzzySetParameters->mData[j][4];
			featureIndex = (unsigned int)m_FuzzySetParameters->mData[j][0];
			featureValue = m_RegionPropsPerThread[threadId][i][featureIndex];
			currentUncertainty = std::max<float>(std::min<float>(std::min<float>(((featureValue-a)/(b-a)), ((d-featureValue)/(d-c))), 1), 0);

			if (featureLargerThanExpected == 0)
			{
				if (featureValue > c)
					featureLargerThanExpected = 1;
				else
					featureLargerThanExpected = -1;
			}

			if (m_UncertaintyCombinationFunction == 0)
				uncertainty *= currentUncertainty;
			if (m_UncertaintyCombinationFunction == 1)
				uncertainty = std::min<float>(currentUncertainty, uncertainty);
			if (m_UncertaintyCombinationFunction == 2)
				uncertainty = std::max<float>(currentUncertainty, uncertainty);
			if (m_UncertaintyCombinationFunction == 3)
				uncertainty += currentUncertainty;
		}

		if (m_UncertaintyCombinationFunction == 3 && numFuzzySets > 0)
			uncertainty /= numFuzzySets;


		if (uncertainty < m_UncertaintyThreshold && featureLargerThanExpected > 0) // if criterion is fullfilled and the considered segment is larger than the desired set
		{
			// define the current region based on the provided region properties
			typename TInputImage::IndexType tmpIndex;
 			typename TInputImage::SizeType tmpSize;
			typename TInputImage::RegionType tmpRegion;
			tmpIndex[0] = m_RegionPropsPerThread[threadId][i][xposIndex]; //int(0.5 + m_RegionPropsPerThread[threadId][i][xposIndex] - ((float)m_RegionPropsPerThread[threadId][i][xsizeIndex]/2.0));
			tmpIndex[1] = m_RegionPropsPerThread[threadId][i][yposIndex]; //int(0.5 + m_RegionPropsPerThread[threadId][i][yposIndex] - ((float)m_RegionPropsPerThread[threadId][i][ysizeIndex]/2.0));
			tmpIndex[2] = m_RegionPropsPerThread[threadId][i][zposIndex]; //int(0.5 + m_RegionPropsPerThread[threadId][i][zposIndex] - ((float)m_RegionPropsPerThread[threadId][i][zsizeIndex]/2.0));
			tmpSize[0] = m_RegionPropsPerThread[threadId][i][xsizeIndex];
			tmpSize[1] = m_RegionPropsPerThread[threadId][i][ysizeIndex];
			tmpSize[2] = m_RegionPropsPerThread[threadId][i][zsizeIndex];
			tmpRegion.SetIndex(tmpIndex);
			tmpRegion.SetSize(tmpSize);

			Logger::GetInstance()->WriteLine( QString("Processing label ") + QString::number(currentLabel) );

			// initialize the distance map for the region
			typename TInputImage::Pointer tmpDistanceMapImage = TInputImage::New();
			tmpDistanceMapImage->SetRegions(tmpRegion);
			tmpDistanceMapImage->SetSpacing(m_LabelImage->GetSpacing());
			tmpDistanceMapImage->SetOrigin(m_LabelImage->GetOrigin());
			tmpDistanceMapImage->Allocate();
			tmpDistanceMapImage->FillBuffer(0);

			// initialize the seed image for the region
			typename TInputImage::Pointer tmpSeedImage = TInputImage::New();
			tmpSeedImage->SetRegions(tmpRegion);
			tmpSeedImage->SetSpacing(m_LabelImage->GetSpacing());
			tmpSeedImage->SetOrigin(m_LabelImage->GetOrigin());
			tmpSeedImage->Allocate();
			tmpSeedImage->FillBuffer(0);

			// fill the distance map and the seed image
			typename itk::ImageRegionConstIterator<TInputImage> distanceMapIterator(inputImage, tmpRegion );
			typename itk::ImageRegionConstIterator<TInputImage> seedImageIterator(m_SeedImage, tmpRegion );
			typename itk::ImageRegionIterator<TInputImage> tmpDistanceMapIterator(tmpDistanceMapImage, tmpRegion );
			typename itk::ImageRegionIterator<TInputImage> tmpSeedImageIterator(tmpSeedImage, tmpRegion );
			distanceMapIterator.GoToBegin();
			seedImageIterator.GoToBegin();
			tmpDistanceMapIterator.GoToBegin();
			tmpSeedImageIterator.GoToBegin();
			while(tmpSeedImageIterator.IsAtEnd() == false)
			{
				tmpDistanceMapIterator.Set( distanceMapIterator.Value() );
				tmpSeedImageIterator.Set( seedImageIterator.Value() );

				++tmpSeedImageIterator;
				++tmpDistanceMapIterator;
				++distanceMapIterator;
				++seedImageIterator;
			}

			typedef itk::MinimumMaximumImageCalculator<TInputImage> MinMaxCalculatorType;
			typename MinMaxCalculatorType::Pointer minMaxCalc = MinMaxCalculatorType::New();
			minMaxCalc->SetImage( tmpSeedImage );
			minMaxCalc->Compute();
			if (minMaxCalc->GetMaximum() == 0)
				continue;

			// setup the filter
			typedef itk::MorphologicalWatershedFromMarkersImageFilter<TInputImage, TInputImage> WatershedFromMarkersFilterType;
			typename WatershedFromMarkersFilterType::Pointer watershedFromMarkersFilter = WatershedFromMarkersFilterType::New();
			watershedFromMarkersFilter->SetInput1( tmpDistanceMapImage );
			watershedFromMarkersFilter->SetInput2( tmpSeedImage );
			watershedFromMarkersFilter->SetFullyConnected( m_FullyConnected );
			watershedFromMarkersFilter->SetMarkWatershedLine( true );
			watershedFromMarkersFilter->SetReleaseDataFlag( true );

			// update the filter
			itkTryCatch(watershedFromMarkersFilter->Update(), "Error: MorphologicalWatershedFromMarkersImageWrapper Update Function.");

			typename itk::ImageRegionIterator<TInputImage> watershedIterator(watershedFromMarkersFilter->GetOutput(), tmpRegion );
			typename itk::ImageRegionConstIterator<TInputImage> labelImageIterator(m_LabelImage, tmpRegion );
			typename itk::ImageRegionIterator<TInputImage> outputIterator(outputImage, tmpRegion );
			watershedIterator.GoToBegin();
			labelImageIterator.GoToBegin();
			outputIterator.GoToBegin();

			while(watershedIterator.IsAtEnd() == false)
			{
				float currLabel = labelImageIterator.Value();
				float currWS = watershedIterator.Value();
				if (labelImageIterator.Value() == currentLabel && watershedIterator.Value() == 0)
					outputIterator.Set(labelImageIterator.Value() * watershedIterator.Value());

				++watershedIterator;
				++labelImageIterator;
				++outputIterator;
			}
		}
		else if (uncertainty < m_UncertaintyThreshold && featureLargerThanExpected < 0)
		{
			// define the current region based on the provided region properties
			typename TInputImage::IndexType tmpIndex;
 			typename TInputImage::SizeType tmpSize;
			typename TInputImage::RegionType tmpRegion;
			tmpIndex[0] = m_RegionPropsPerThread[threadId][i][xposIndex]; //int(0.5 + m_RegionPropsPerThread[threadId][i][xposIndex] - ((float)m_RegionPropsPerThread[threadId][i][xsizeIndex]/2.0));
			tmpIndex[1] = m_RegionPropsPerThread[threadId][i][yposIndex]; //int(0.5 + m_RegionPropsPerThread[threadId][i][yposIndex] - ((float)m_RegionPropsPerThread[threadId][i][ysizeIndex]/2.0));
			tmpIndex[2] = m_RegionPropsPerThread[threadId][i][zposIndex]; //int(0.5 + m_RegionPropsPerThread[threadId][i][zposIndex] - ((float)m_RegionPropsPerThread[threadId][i][zsizeIndex]/2.0));
			tmpSize[0] = m_RegionPropsPerThread[threadId][i][xsizeIndex];
			tmpSize[1] = m_RegionPropsPerThread[threadId][i][ysizeIndex];
			tmpSize[2] = m_RegionPropsPerThread[threadId][i][zsizeIndex];
			tmpRegion.SetIndex(tmpIndex);
			tmpRegion.SetSize(tmpSize);

			typename itk::ImageRegionConstIterator<TInputImage> labelImageIterator(m_LabelImage, tmpRegion );
			typename itk::ImageRegionIterator<TInputImage> outputIterator(outputImage, tmpRegion );
			labelImageIterator.GoToBegin();
			outputIterator.GoToBegin();
			while(outputIterator.IsAtEnd() == false)
			{
				if (labelImageIterator.Value() == currentLabel)
					outputIterator.Set(0);

				++outputIterator;
				++labelImageIterator;
			}
		}
	}
}


// after threaded generate data
template <class TImageType>
void UncertaintyGuidedWatershedSegmentationFilter<TImageType>::AfterThreadedGenerateData()
{
}

} // end namespace itk

#endif
