/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef __XPIWIT_DOMIMAGEFILTER_HC
#define __XPIWIT_DOMIMAGEFILTER_HC

// include required headers
#include "itkBoxImageFilter.h"

#include "../Base/Management/ITKDefinitions.h"
#include "itkMacro.h"

namespace itk
{

/**
 * @class DoM
 * Parallelized difference of mean filter.
 */
template <class TImageType>
class ITK_EXPORT DoMImageFilter :
        public BoxImageFilter<TImageType, TImageType>
{
public:
    /** Standard class typedefs. */
    typedef DoMImageFilter                              Self;
    typedef BoxImageFilter< TImageType, TImageType >    Superclass;
    typedef SmartPointer< Self >                        Pointer;
    typedef SmartPointer< const Self >                  ConstPointer;

    /** Standard New method. */
    itkNewMacro(Self);

    /** Runtime information support. */
    itkTypeMacro(DoMImageFilter,
                 BoxImageFilter);

    /** Image related typedefs. */
    typedef TImageType                                 InputImageType;
    typedef TImageType                                 OutputImageType;
    typedef typename TImageType::RegionType            RegionType;
    typedef typename TImageType::SizeType              SizeType;
    typedef typename TImageType::IndexType             IndexType;
    typedef typename TImageType::PixelType             PixelType;
    typedef typename TImageType::OffsetType            OffsetType;
    typedef typename Superclass::OutputImageRegionType OutputImageRegionType;
    typedef typename TImageType::PixelType             OutputPixelType;

    // Accumulate type is too small
    typedef typename NumericTraits< PixelType >::RealType    AccPixType;
    typedef Image< AccPixType, TImageType::ImageDimension > AccumImageType;

    /** Image related typedefs. */
    itkStaticConstMacro(OutputImageDimension, unsigned int,
                        TImageType::ImageDimension);
    itkStaticConstMacro(InputImageDimension, unsigned int,
                        TImageType::ImageDimension);

#ifdef ITK_USE_CONCEPT_CHECKING
    /** Begin concept checking */
    itkConceptMacro( SameDimension,
                     ( Concept::SameDimension< itkGetStaticConstMacro(InputImageDimension),
                       itkGetStaticConstMacro(OutputImageDimension) > ) );

    /** End concept checking */
#endif

    void SetMinRadius(SizeType radius){ minRadius = radius; }
    void SetMaxRadius(SizeType radius){ maxRadius = radius; }
    void SetStepSize(SizeType step){ stepSize = step; }

protected:
    DoMImageFilter();
    ~DoMImageFilter() {}

    /** Multi-thread version GenerateData. */
    void ThreadedGenerateData(const OutputImageRegionType & outputRegionForThread, ThreadIdType threadId) override;

private:
    DoMImageFilter(const Self &);     //purposely not implemented
    void operator=(const Self &);     //purposely not implemented

    SizeType minRadius;
    SizeType maxRadius;
    SizeType stepSize;

    void BoxDoMCalculatorFunction(const AccumImageType *accImage,
                                  TImageType *outputImage,
                                  typename AccumImageType::RegionType inputRegion,
                                  typename TImageType::RegionType outputRegion,
                                  typename TImageType::SizeType minRadius,
                                  typename TImageType::SizeType maxRadius,
                                  ProgressReporter & progress);

    void BoxDifferenceFilter(const InputImageType *smallMeanImage,
                             const InputImageType *bigMeanImage,
                             OutputImageType *outputImage,
                             RegionType inputRegion, RegionType outputRegion,
                             ProgressReporter &progress);

};                                    // end of class
} // end namespace itk

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkDoMImageFilter.cpp"
#endif

#endif
