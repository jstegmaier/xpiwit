/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef __XPIWIT_FUSEROTATIONIMAGESFILTER_HXX
#define __XPIWIT_FUSEROTATIONIMAGESFILTER_HXX

// include required headers
#include "itkFuseRotationImagesFilter.h"
#include "itkImageRegionIterator.h"
#include "itkImageRegionConstIterator.h"
#include "itkImageFileReader.h"
#include <iostream>
#include <fstream>
#include <string>
#include <QString>
#include <QStringList>
#include <QDateTime>
#include <vnl/vnl_matrix_fixed.h>
#include <vnl/vnl_vector_fixed.h>
#include "../../Core/Utilities/Logger.h"

// namespace itk
namespace itk
{

// the default constructor
template <class TImageType>
FuseRotationImagesFilter<TImageType>::FuseRotationImagesFilter()
{
    mRotationMatrix.fill(0);
    mRotationMatrix(0,0) = 1;
    mRotationMatrix(1,1) = -1;
    mRotationMatrix(2,2) = -1;
    mTranslation(0) = 0; mTranslation(1) = 0; mTranslation(2) = 0;
	this->DynamicMultiThreadingOff();
}


// the destructor
template <class TImageType>
FuseRotationImagesFilter<TImageType>::~FuseRotationImagesFilter()
{
}


// before threaded generate data
template <class TImageType>
void FuseRotationImagesFilter<TImageType>::BeforeThreadedGenerateData()
{
    // Allocate output
    typename ImageType::Pointer output = this->GetOutput();
    typename ImageType::ConstPointer input  = this->GetInput();
    output->FillBuffer( 0 );

//	Logger::GetInstance()->WriteLine( "+ TranslationVector " + QString::number(mTranslation(0)) + ", " + QString::number(mTranslation(1)) + ", " + QString::number(mTranslation(2)) );
//	Logger::GetInstance()->WriteLine( "+ RotationMatrix: " + QString::number(mRotationMatrix(0,0)) + ", " + QString::number(rotatedIndex[1]) + ", " + QString::number(rotatedIndex[2]) );


}


// the thread generate data
template <class TImageType>
void FuseRotationImagesFilter<TImageType>::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId)
{
    // Allocate output
    typename ImageType::Pointer output = this->GetOutput();
    typename ImageType::ConstPointer input  = this->GetInput();
    typename ImageType::RegionType largestPossibleRegion = input->GetLargestPossibleRegion();
    typename ImageType::SpacingType spacing = input->GetSpacing();

    // iterate over the images and perform the fusion
    ImageRegionConstIterator<ImageType> inputIterator( input, outputRegionForThread );
    ImageRegionIterator<ImageType> outputIterator( output, outputRegionForThread );
    inputIterator.GoToBegin();
    outputIterator.GoToBegin();

    // define the index vectors
    vnl_vector_fixed<double,3> x;
    vnl_vector_fixed<double,3> xrot;

    // perform the fusion
    typename TImageType::IndexType rotatedIndex;
    typename TImageType::RegionType rotatedRegion = mRotatedImage->GetLargestPossibleRegion();

    float fusionCenter = (float)rotatedRegion.GetSize()[2] / 2.0;
    float fusionWidth = (float)rotatedRegion.GetSize()[2] / 5.0;
    float fusionSlope = 5.0 / fusionWidth;
    float rotatedIntensityValue, unrotatedIntensityValue, weight1, weight2;
    int zpos, zposOld;
    zpos = 0;
    zposOld=0;
    while(inputIterator.IsAtEnd() == false)
    {
        // get the correspoinding value of the rotated image
        x(0) = inputIterator.GetIndex()[0];
        x(1) = inputIterator.GetIndex()[1];
        x(2) = inputIterator.GetIndex()[2];

        // calculate the transformed point
        xrot = mRotationMatrix * (x - mTranslation);
        rotatedIndex[0] = int(0.5+xrot(0));
        rotatedIndex[1] = int(0.5+xrot(1));
        rotatedIndex[2] = int(0.5+xrot(2));

		//Logger::GetInstance()->WriteLine( "+ ExtractRegionPropsImageFilter: Found " + QString::number(rotatedIndex[0]) + ", " + QString::number(rotatedIndex[1]) + ", " + QString::number(rotatedIndex[2]) );

        if (zposOld != zpos)
            zposOld = zpos;

        // check if rotated voxel is valid
        if (rotatedIndex[0] < rotatedRegion.GetSize()[0] && rotatedIndex[0] >= 0 &&
            rotatedIndex[1] < rotatedRegion.GetSize()[1] && rotatedIndex[1] >= 0 &&
            rotatedIndex[2] < rotatedRegion.GetSize()[2] && rotatedIndex[2] >= 0)
        {
            // get the intensity value of the rotated image
            unrotatedIntensityValue = inputIterator.Get();
            rotatedIntensityValue = mRotatedImage->GetPixel(rotatedIndex);

            // perform fusion of the two rotations
            zpos = outputIterator.GetIndex()[2];
            weight2 = 1.0 / (1.0 + exp(-(fusionSlope*(zpos-fusionCenter))));
            weight1 = 1.0 - weight2;

			outputIterator.Set( weight1 * unrotatedIntensityValue + weight2 * rotatedIntensityValue );
            //outputIterator.Set( rotatedIntensityValue );
            //outputIterator.Set( std::max<float>(rotatedIntensityValue, unrotatedIntensityValue) );
            /*if (zpos < fusionCenter-(fusionWidth/2.0))
                outputIterator.Set( unrotatedIntensityValue );
            else if (zpos > fusionCenter+(fusionWidth/2.0))
                outputIterator.Set( rotatedIntensityValue );
            else
                outputIterator.Set( std::max(unrotatedIntensityValue, rotatedIntensityValue) );
                */
            //outputIterator.Set( unrotatedIntensityValue * rotatedIntensityValue );
            //outputIterator.Set( rotatedIntensityValue );
        }

        // increment the iterator
        ++inputIterator;
        ++outputIterator;
    }
}


// after threaded generate data
template <class TImageType>
void FuseRotationImagesFilter<TImageType>::AfterThreadedGenerateData()
{

}

} // end namespace itk

#endif
