/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// include required headers
#include "itkExtractRegionPropsImageFilter.h"
#include <iostream>
#include <fstream>
#include <string>
#include "../../Core/Utilities/Logger.h"

using namespace XPIWIT;

// namespace itk
namespace itk
{

// the default constructor
template <class TInputImageType>
ExtractRegionPropsImageFilter<TInputImageType>::ExtractRegionPropsImageFilter()
{
    this->SetNumberOfRequiredInputs( 1 );
    this->SetNumberOfRequiredOutputs( 0 );

    m_BinaryInput = true;
    m_CentroidOffset = 1;
    m_FullyConnected = true;
    m_LogResults = false;
}


// the destructor
template <class TInputImageType>
ExtractRegionPropsImageFilter<TInputImageType>::~ExtractRegionPropsImageFilter()
{
}


// generate the data
template <class TInputImageType>
void ExtractRegionPropsImageFilter<TInputImageType>::GenerateData()
{
    // get the input pointer
    typename InputImageType::ConstPointer input  = this->GetInput();

    // initialize the geometry extraction filter
    typename LabelGeometryImageFilterType::Pointer labelGeometryFilter =  LabelGeometryImageFilterType::New();
    labelGeometryFilter->SetCalculateOrientedBoundingBox( false );
    labelGeometryFilter->SetCalculateOrientedIntensityRegions( false );
    labelGeometryFilter->SetCalculateOrientedLabelRegions( false );
    labelGeometryFilter->SetCalculatePixelIndices( false );
    labelGeometryFilter->SetReleaseDataFlag( true );
    labelGeometryFilter->SetIntensityInput( m_IntensityImage );

    if (m_BinaryInput == true)
    {
        // convert the binary image into a label map
        typename BinaryImageToLabelMapFilterType::Pointer binaryImageToLabelMapFilter = BinaryImageToLabelMapFilterType::New();
        binaryImageToLabelMapFilter->SetFullyConnected( m_FullyConnected );
        binaryImageToLabelMapFilter->SetInput( input );
        binaryImageToLabelMapFilter->SetReleaseDataFlag( true );
        itkTryCatch( binaryImageToLabelMapFilter->Update(), "ExtractRegionProps --> BinaryImageToLabelMapFilter" );

        // convert the label map into a label image
        typename LabelMapToLabelImageFilterType::Pointer labelMapToLabelImageFilter = LabelMapToLabelImageFilterType::New();
        labelMapToLabelImageFilter->SetInput(binaryImageToLabelMapFilter->GetOutput());
        labelMapToLabelImageFilter->SetReleaseDataFlag( true );
        itkTryCatch( labelMapToLabelImageFilter->Update(), "ExtractRegionProps --> LabelMapToLabelImage" );

        // set the extraction filter input
        labelGeometryFilter->SetInput( labelMapToLabelImageFilter->GetOutput() );
    }
    else
    {
        // directly get the information from the prelabeled input
        labelGeometryFilter->SetInput( input );
    }

    // update the geometry extraction filter
    itkTryCatch( labelGeometryFilter->Update(), "ExtractRegionPropsImageFilter" );
    Logger::GetInstance()->WriteLine( "+ ExtractRegionPropsImageFilter: Found " + QString::number(labelGeometryFilter->GetNumberOfLabels()) + " connected regions in the binary image." );

    // get the labels
    typename LabelGeometryImageFilterType::LabelsType allLabels = labelGeometryFilter->GetLabels();
    typename LabelGeometryImageFilterType::LabelsType::iterator allLabelsIt;

    // open the metadata results file
    std::ofstream keyPointsFile;
    QString fileName = m_FileName.c_str();
    //QString keyPointsFileName = fileName.replace(".tif", "_DetSeeds.csv");
    //keyPointsFileName = keyPointsFileName.replace(".btf", "_DetSeeds.csv");
    //keyPointsFile.open( keyPointsFileName.toLatin1().data() );

    // extract nucleus information
    unsigned int currentKeyPointID = 0;

    // write data file header
    m_MetaObject->mTitle << "id" << "size" << "centroid" <<
        "bounding box" <<
        "weighted centroid" <<
        "integrated intensity" <<
        "axes length" << "major axis length" << "minor axis length" << "eccentricity" << "elongation" << "orientation";

    m_MetaObject->mTitle << "id" << "size" << "xpos" << "ypos" << "zpos" <<
        "x size" << "y size" << "z size" <<
        "weighted x" << "weighted y" << "weighted z" <<
        "integrated intensity" <<
        "axes length" << "major axis length" << "minor axis length" << "eccentricity" << "elongation" << "orientation";

    // iterate trough the labels and extract nucleus information
    for( allLabelsIt = allLabels.begin(); allLabelsIt != allLabels.end(); allLabelsIt++ )
    {
        ++currentKeyPointID;
        typename LabelGeometryImageFilterType::LabelPixelType labelValue = *allLabelsIt;

        // write keypoint to file
        // scheme: id, size, xpos, ypos, zpos, xsize, ysize, zsize, sizeonborder, scale
        //keyPointsFile	<< currentKeyPointID << ","
        //                << (int)floorf(0.5+labelGeometryFilter->GetVolume(labelValue)) << ","
        //                << (int)floorf(0.5+labelGeometryFilter->GetCentroid(labelValue)[0]+m_CentroidOffset) << ","
        //                << (int)floorf(0.5+labelGeometryFilter->GetCentroid(labelValue)[1]+m_CentroidOffset) << ","
        //                << (int)floorf(0.5+labelGeometryFilter->GetCentroid(labelValue)[2]+m_CentroidOffset) << ","
        //                << (int)floorf(0.5+labelGeometryFilter->GetBoundingBoxSize(labelValue)[0]) << ","
        //                << (int)floorf(0.5+labelGeometryFilter->GetBoundingBoxSize(labelValue)[1]) << ","
        //                << (int)floorf(0.5+labelGeometryFilter->GetBoundingBoxSize(labelValue)[2]) << ","

        //                << labelGeometryFilter->GetIntegratedIntensity(labelValue) / labelGeometryFilter->GetVolume(labelValue) << ","
        //                << labelGeometryFilter->GetMajorAxisLength(labelValue) << ","
        //                << labelGeometryFilter->GetMinorAxisLength(labelValue) << ","
        //                << labelGeometryFilter->GetEccentricity(labelValue) << ","
        //                << 0 << std::endl;

        // only log the results if explicitly enabled
        if (true) //( m_LogResults == true )
        {
            std::cout << "- Object " << currentKeyPointID << std::endl;
            std::cout << "\tVolume: " << labelGeometryFilter->GetVolume(labelValue) << std::endl;
            std::cout << "\tIntegrated Intensity: " << labelGeometryFilter->GetIntegratedIntensity(labelValue) / labelGeometryFilter->GetVolume(labelValue) << std::endl;
            std::cout << "\tCentroid: " << labelGeometryFilter->GetCentroid(labelValue) << std::endl;
            std::cout << "\tWeighted Centroid: " << labelGeometryFilter->GetWeightedCentroid(labelValue) << std::endl;
            std::cout << "\tAxes Length: " << labelGeometryFilter->GetAxesLength(labelValue) << std::endl;
            std::cout << "\tMajorAxisLength: " << labelGeometryFilter->GetMajorAxisLength(labelValue) << std::endl;
            std::cout << "\tMinorAxisLength: " << labelGeometryFilter->GetMinorAxisLength(labelValue) << std::endl;
            std::cout << "\tEccentricity: " << labelGeometryFilter->GetEccentricity(labelValue) << std::endl;
            std::cout << "\tElongation: " << labelGeometryFilter->GetElongation(labelValue) << std::endl;
            std::cout << "\tOrientation: " << labelGeometryFilter->GetOrientation(labelValue) << std::endl;
            std::cout << "\tBounding box: " << labelGeometryFilter->GetBoundingBox(labelValue) << std::endl;

            std::cout << std::endl << std::endl;

   //         Logger::GetInstance()->WriteLine( "- Object " + QString::number(currentKeyPointID) );
   //         Logger::GetInstance()->WriteLine( "\tVolume: " + QString::number(labelGeometryFilter->GetVolume(labelValue)) );
   //         Logger::GetInstance()->WriteLine( "\tIntegrated Intensity: " + QString::number(labelGeometryFilter->GetIntegratedIntensity(labelValue) / labelGeometryFilter->GetVolume(labelValue)) );
   //         typename LabelGeometryImageFilterType::PointType p = labelGeometryFilter->GetCentroid(labelValue);
   //		  Logger::GetInstance()->WriteLine( "\tCentroid: " + QString::number(labelGeometryFilter->GetCentroid(labelValue)) );
   //         Logger::GetInstance()->WriteLine( "\tWeighted Centroid: " + QString::number(labelGeometryFilter->GetWeightedCentroid(labelValue)) );
   //         Logger::GetInstance()->WriteLine( "\tAxes Length: " + QString::number( labelGeometryFilter->GetAxesLength(labelValue) ) );
   //         Logger::GetInstance()->WriteLine( "\tMajorAxisLength: " + QString::number( labelGeometryFilter->GetMajorAxisLength(labelValue) ) );
   //         Logger::GetInstance()->WriteLine( "\tMinorAxisLength: " + QString::number( labelGeometryFilter->GetMinorAxisLength(labelValue) ) );
   //         Logger::GetInstance()->WriteLine( "\tEccentricity: " + QString::number( labelGeometryFilter->GetEccentricity(labelValue) ) );
   //         Logger::GetInstance()->WriteLine( "\tElongation: " + QString::number( labelGeometryFilter->GetElongation(labelValue) ) );
   //         Logger::GetInstance()->WriteLine( "\tOrientation: " + QString::number( labelGeometryFilter->GetOrientation(labelValue) ) );
   //         Logger::GetInstance()->WriteLine( "\tBounding box: " + QString::number( labelGeometryFilter->GetBoundingBox(labelValue) ) );

   //         Logger::GetInstance()->WriteLine( "" );
   //         Logger::GetInstance()->WriteLine( "" );
        }
    }

    // close the file handle
    //keyPointsFile.close();

    // release the inputs
    this->ReleaseInputs();
}

} // end namespace itk
