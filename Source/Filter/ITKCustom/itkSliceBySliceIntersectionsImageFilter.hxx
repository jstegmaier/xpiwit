/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// include required headers
#include "itkSliceBySliceIntersectionsImageFilter.h"
#include "itkImageRegionIterator.h"
#include "itkImageRegionConstIterator.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include <iostream>
#include <fstream>
#include <string>
#include <QString>
#include <QStringList>
#include <QDateTime>
#include "../../Core/Utilities/Logger.h"
#include "../Base/MetaData/MetaDataFilter.h"

// namespace itk
namespace itk
{

// the default constructor
template <class TImageType, class TOutputImage>
SliceBySliceIntersectionsImageFilter<TImageType, TOutputImage>::SliceBySliceIntersectionsImageFilter()
{
	m_MinSlice = 0;
	m_MaxSlice = 100000;
	m_DebugOutput = false;
	m_MinimumLabel = 0;
	this->DynamicMultiThreadingOff();
}


// the destructor
template <class TImageType, class TOutputImage>
SliceBySliceIntersectionsImageFilter<TImageType, TOutputImage>::~SliceBySliceIntersectionsImageFilter()
{
}


// before threaded generate data
template <class TImageType, class TOutputImage>
void SliceBySliceIntersectionsImageFilter<TImageType, TOutputImage>::BeforeThreadedGenerateData()
{
    // Allocate output
    typename TOutputImage::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();

	// get the slice index
	const unsigned int sliceIndex = m_InputMetaFilter->GetFeatureIndex( "zpos" );

	// append an empty list for each slice
	for (int i=0; i<input->GetLargestPossibleRegion().GetSize(2); ++i)
		m_SegmentFeatures.append( QList< QList<float> >() );

	// get the number of segments in all slices
	unsigned int numSegments = m_InputMetaFilter->mData.length();

	// fill the slice based lists. Minimum label is at the first position, as the region props are sorted according to the label id
	QList<float> line;
    for(int i=0; i<numSegments; i++)
	{
        line = m_InputMetaFilter->mData.at(i);
		m_SegmentFeatures[int(line.at(sliceIndex))].append( line );
    }

	// initialize the intersections for each of the threads
	for (int i=0; i<this->GetNumberOfWorkUnits(); ++i)
		m_IntersectionFeatures.append( QList< QList<float> >() );
}


// the thread generate data
template <class TImageType, class TOutputImage>
void SliceBySliceIntersectionsImageFilter<TImageType, TOutputImage>::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId)
{
    // get input and output pointers
    typename TOutputImage::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();
	
	// initialize the regions to process
	typename TImageType::RegionType currentRegion1;
	typename TImageType::IndexType currentIndex1;
	typename TImageType::SizeType currentSize1;
	typename TImageType::RegionType currentRegion2;
	typename TImageType::IndexType currentIndex2;
	typename TImageType::SizeType currentSize2;

	for (int j=0; j<TImageType::ImageDimension; ++j)
	{
		currentIndex1[j] = outputRegionForThread.GetIndex(j);
		currentSize1[j] = outputRegionForThread.GetSize(j);
		currentIndex2[j] = outputRegionForThread.GetIndex(j);
		currentSize2[j] = outputRegionForThread.GetSize(j);
	}
	currentSize1[2] = 1;
	currentSize2[2] = 1;
	currentRegion1.SetIndex( currentIndex1 );
	currentRegion1.SetSize( currentSize1 );
	currentRegion2.SetIndex( currentIndex2 );
	currentRegion2.SetSize( currentSize2 );
	
	// iterate over all slices and perform watershed separately for each slice
	for (int i=0; i<outputRegionForThread.GetSize(2); ++i)
	{
		// set the current index
		const unsigned int slice1 = outputRegionForThread.GetIndex(2)+i;
		const unsigned int slice2 = outputRegionForThread.GetIndex(2)+i+1;
		currentIndex1[2] = slice1;
		currentIndex2[2] = slice2;
		currentRegion1.SetIndex( currentIndex1 );
		currentRegion1.SetSize( currentSize1 );
		currentRegion2.SetIndex( currentIndex2 );
		currentRegion2.SetSize( currentSize2 );

		// only process the valid range of slices
		if (slice1 < m_MinSlice || slice2 > m_MaxSlice || slice2 >= input->GetLargestPossibleRegion().GetSize(2))
			continue;

		// the minimum labels should be at the first position, as the provided region props are sorted.
		unsigned int minLabel1 = m_SegmentFeatures[slice1][0][0];
		unsigned int minLabel2 = m_SegmentFeatures[slice2][0][0];
		
		/*
		for (int j=1; j<m_SegmentFeatures[slice1].length(); ++j)
		{
			if (m_SegmentFeatures[slice1][j][0] < minLabel1)
				minLabel1 = m_SegmentFeatures[slice1][j][0];
		}

		for (int j=1; j<m_SegmentFeatures[slice2].length(); ++j)
		{
			if (m_SegmentFeatures[slice2][j][0] < minLabel2)
				minLabel2 = m_SegmentFeatures[slice2][j][0];
		}*/

		// extract the slice to perform the watershed on
		ImageRegionIterator<TImageType> outputIterator( output, currentRegion1 );
		ImageRegionConstIterator<TImageType> input1Iterator( input, currentRegion1 );
		ImageRegionConstIterator<TImageType> input2Iterator( input, currentRegion2 );
		outputIterator.GoToBegin();
		input1Iterator.GoToBegin();
		input2Iterator.GoToBegin();

		// initialize the label histogram
		typename LabelHistogramType::RegionType labelRegion;
		typename LabelHistogramType::IndexType labelIndex;
		typename LabelHistogramType::IndexType currentIndex;
		typename LabelHistogramType::SizeType labelSize;
		LabelHistogramType::Pointer labelHistogram = LabelHistogramType::New();
		labelIndex[0] = 0; labelIndex[1] = 0;
		// V1: labelSize[0] = m_SegmentFeatures[slice1].length()-m_SegmentFeatures[slice1][0][0]+1; labelSize[1] = m_SegmentFeatures[slice2].length()-m_SegmentFeatures[slice2][0][0]+1;
		//labelSize[0] = m_SegmentFeatures[slice1].length()-minLabel1+1; labelSize[1] = m_SegmentFeatures[slice2].length()-minLabel2+1;
		labelSize[0] = m_SegmentFeatures[slice1].length(); labelSize[1] = m_SegmentFeatures[slice2].length();
		labelRegion.SetIndex( labelIndex );
		labelRegion.SetSize( labelSize );
		labelHistogram->SetRegions( labelRegion );
		labelHistogram->Allocate();
		labelHistogram->FillBuffer( 0 );

		// convert float labels to discrete values
		float intensityMultiplier = 1.0;
		if( typeid( typename TOutputImage::PixelType ) == typeid( float ) ||
			typeid( typename TOutputImage::PixelType ) == typeid( double ) ) {
			intensityMultiplier = 1.0;
		}

		// initialize the histogram
		unsigned int maxLabelSlice1 = 0;
		unsigned int maxLabelSlice2 = 0;
		float currentValue1 = 0.0;
		float currentValue2 = 0.0;

		while(input1Iterator.IsAtEnd() == false)
		{
			// copy the input value
			outputIterator.Set( input1Iterator.Value() );

			// increment the histogram bin that matches the label combination
			currentIndex[0] = (intensityMultiplier * input1Iterator.Value()) - minLabel1;
			currentIndex[1] = (intensityMultiplier * input2Iterator.Value()) - minLabel2;

			if (currentIndex[0] < 0 || currentIndex[1] < 0)
				Logger::GetInstance()->WriteToConsole( "ci0 " + QString::number(currentIndex[0]) + "ci1 " + QString::number(currentIndex[1]) + "ii1 " + QString::number(input1Iterator.Value())  + "ii2 " + QString::number(input2Iterator.Value()) + " ms1 " + QString::number(m_SegmentFeatures[slice1][0][0]) + " ms2 " + QString::number(m_SegmentFeatures[slice2][0][0]) );

			labelHistogram->SetPixel( currentIndex, labelHistogram->GetPixel(currentIndex)+1 );

			// increment the iterators
			++input1Iterator;
			++input2Iterator;
			++outputIterator;
		}

		if (m_DebugOutput == true)
			Logger::GetInstance()->WriteLine( "- SliceBySliceIntersectionsFilter: Found " + QString::number(maxLabelSlice1) + ", " + QString::number(maxLabelSlice2) + " labels in slices " + QString::number(slice1) + ", " + QString::number(slice2) );

		// extract the intersections from the histogram
		ImageRegionIterator<LabelHistogramType> histogramIterator( labelHistogram, labelHistogram->GetLargestPossibleRegion() );
		histogramIterator.GoToBegin();

		// declare variables for the intersection features
		float area1, area2, areaIntersection, areaUnion, jaccardIndex, minRelativeOverlap, symmetricDistance, distance;
		float xpos1, xpos2, ypos1, ypos2;
		float elongation1, elongation2, minorAxis1, minorAxis2, majorAxis1, majorAxis2, eccentricity1, eccentricity2, orientation1, orientation2, meanIntensity1, meanIntensity2;

		//  get the feature indices
		const int idIndex = m_InputMetaFilter->GetFeatureIndex( "id" );
		const int areaIndex = m_InputMetaFilter->GetFeatureIndex( "volume" );
		const int xposIndex = m_InputMetaFilter->GetFeatureIndex( "xpos" );
		const int yposIndex = m_InputMetaFilter->GetFeatureIndex( "ypos" );
		const int meanIntensityIndex = m_InputMetaFilter->GetFeatureIndex( "meanIntensity" );
		const int majorAxisIndex = m_InputMetaFilter->GetFeatureIndex( "majorAxisLength" );
		const int minorAxisIndex = m_InputMetaFilter->GetFeatureIndex( "minorAxisLength" );
		const int eccentricityIndex = m_InputMetaFilter->GetFeatureIndex( "eccentricity" );
		const int elongationIndex = m_InputMetaFilter->GetFeatureIndex( "elongation" );
		const int orientationIndex = m_InputMetaFilter->GetFeatureIndex( "orientation" );

		// declare the slice objects
		LabelHistogramType::IndexType histogramIndex;
		QList<float> slice1Object;
		QList<float> slice2Object;

		while (histogramIterator.IsAtEnd() == false)
		{
			// only calculate features if at least one pixel overlap is found (TODO: think about reasonable threshold)
			if (histogramIterator.Value() > 0)
			{
				// calculate the intersection features
				histogramIndex = histogramIterator.GetIndex();
				slice1Object = m_SegmentFeatures[slice1][histogramIndex[0]];
				slice2Object = m_SegmentFeatures[slice2][histogramIndex[1]];
				area1 = slice1Object.at(areaIndex);
				area2 = slice2Object.at(areaIndex);
				xpos1 = slice1Object.at(xposIndex);
				xpos2 = slice2Object.at(xposIndex);
				ypos1 = slice1Object.at(yposIndex);
				ypos2 = slice2Object.at(yposIndex);
				meanIntensity1 = slice1Object.at(meanIntensityIndex);
				meanIntensity2 = slice2Object.at(meanIntensityIndex);
				majorAxis1 = slice1Object.at(majorAxisIndex);
				majorAxis2 = slice2Object.at(majorAxisIndex);
				minorAxis1 = slice1Object.at(minorAxisIndex);
				minorAxis2 = slice2Object.at(minorAxisIndex);
				eccentricity1 = slice1Object.at(eccentricityIndex);
				eccentricity2 = slice2Object.at(eccentricityIndex);
				elongation1 = slice1Object.at(elongationIndex);
				elongation2 = slice2Object.at(elongationIndex);
				orientation1 = slice1Object.at(orientationIndex);
				orientation2 = slice2Object.at(orientationIndex);
				areaIntersection = histogramIterator.Value();
				areaUnion = area1+area2-areaIntersection;
				jaccardIndex = areaIntersection/areaUnion;
				minRelativeOverlap = vnl_math::min(areaIntersection/area1, areaIntersection/area2);
				symmetricDistance = areaUnion - areaIntersection;
				distance = sqrtf(vnl_math::sqr(xpos1-xpos2) + vnl_math::sqr(ypos1-ypos2));

				// add the values to the string list
				QList<float> currentIntersection;
				currentIntersection << slice1Object.at(idIndex); //histogramIterator.GetIndex()[0];
				currentIntersection << slice2Object.at(idIndex); //histogramIterator.GetIndex()[1];
				currentIntersection << slice1;
				currentIntersection << slice2;
				currentIntersection << areaIntersection;
				currentIntersection << areaUnion;
				currentIntersection << jaccardIndex;
				currentIntersection << minRelativeOverlap;
				currentIntersection << symmetricDistance;
				currentIntersection << distance;
				currentIntersection << meanIntensity1-meanIntensity2;
				currentIntersection << vnl_math::min(elongation1,elongation2) / vnl_math::max(elongation1,elongation2);
				currentIntersection << vnl_math::min(eccentricity1,eccentricity2) / vnl_math::max(eccentricity1,eccentricity2);
				currentIntersection << vnl_math::min(orientation1,orientation2) / vnl_math::max(orientation1,orientation2);
				currentIntersection << vnl_math::min(minorAxis1,minorAxis2) / vnl_math::max(minorAxis1,minorAxis2);
				currentIntersection << vnl_math::min(majorAxis1,majorAxis2) / vnl_math::max(majorAxis1,majorAxis2);
				m_IntersectionFeatures[threadId].append( currentIntersection );

				if (m_DebugOutput == true)
					Logger::GetInstance()->WriteLine( "- Found Overlap of Size " + QString::number(histogramIterator.Value()) + " between slice " + QString::number(slice1) + " and " + QString::number(slice2) );
			}

			++histogramIterator;
		}
	}
}


// after threaded generate data
template <class TImageType, class TOutputImage>
void SliceBySliceIntersectionsImageFilter<TImageType, TOutputImage>::AfterThreadedGenerateData()
{
    // setup the meta description
	QStringList metaDescription;                    
	QStringList metaType;
    metaDescription << "label1";					metaType << "int";
	metaDescription << "label2";					metaType << "int";
	metaDescription << "slice1";					metaType << "int";
	metaDescription << "slice2";					metaType << "int";
	metaDescription << "areaIntersection";			metaType << "int";
	metaDescription << "areaUnion";					metaType << "int";
	metaDescription << "jaccardIndex";				metaType << "float";
	metaDescription << "minRelativeOverlap";		metaType << "float";
	metaDescription << "symmetricDistance";			metaType << "float";
	metaDescription << "distance";					metaType << "float";
	metaDescription << "meanIntensityDiff";			metaType << "float";
	metaDescription << "elongationRatio";			metaType << "float";
	metaDescription << "eccentricityRatio";			metaType << "float";
	metaDescription << "orientationRatio";			metaType << "float";
	metaDescription << "minorAxisRatio";			metaType << "float";
	metaDescription << "majorAxisRatio";			metaType << "float";

	// write to meta object
	m_OutputMetaFilter->mTitle = metaDescription;
	m_OutputMetaFilter->mType = metaType;

	for (int i=0; i<m_IntersectionFeatures.length(); ++i)
	{
		for (int j=0; j<m_IntersectionFeatures[i].length(); ++j)
		{
			m_OutputMetaFilter->mData.append(m_IntersectionFeatures[i][j]);
		}
	}
}

} // end namespace itk
