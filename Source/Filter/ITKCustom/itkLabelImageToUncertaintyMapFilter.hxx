/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef __XPIWIT_LabelImageToUncertaintyMapFilter_HXX
#define __XPIWIT_LabelImageToUncertaintyMapFilter_HXX

// include required headers
#include "itkLabelImageToUncertaintyMapFilter.h"
#include "itkImageRegionIterator.h"
#include "itkImageRegionConstIterator.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkExtractRegionPropsImageFilter.h"
#include "itkOtsuThresholdImageFilter.h"
#include "itkGradientImageFilter.h"
#include "itkStatisticsImageFilter.h"
#include <iostream>
#include <fstream>
#include <string>
#include <QString>
#include <QStringList>
#include <QDateTime>
#include "../../Core/Utilities/Logger.h"

// namespace itk
namespace itk
{

// the default constructor
template <class TImageType>
LabelImageToUncertaintyMapFilter<TImageType>::LabelImageToUncertaintyMapFilter()
{
    m_InputMetaFilter = NULL;
    m_OutputMetaFilter = NULL;
	m_UncertaintyFeature = 0;
	m_LabelOffset = 0;
	this->DynamicMultiThreadingOff();
}


// the destructor
template <class TImageType>
LabelImageToUncertaintyMapFilter<TImageType>::~LabelImageToUncertaintyMapFilter()
{
}


// before threaded generate data
template <class TImageType>
void LabelImageToUncertaintyMapFilter<TImageType>::BeforeThreadedGenerateData()
{
    // Allocate output
    typename TImageType::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();
    output->FillBuffer( 0 );

	// convert the meta data into a hash table for faster access
	const unsigned int numObjects = m_InputMetaFilter->mData.length();
	m_LabelToUncertaintyHash.reserve( numObjects );
    
    std::cout << "Trying hard to process " << numObjects << " objects ..." << std::endl;
    
	for (unsigned int i=0; i<numObjects; ++i)
	{
        // Allocate output
        typename TImageType::Pointer output = this->GetOutput();
        typename TImageType::ConstPointer input  = this->GetInput();
        output->FillBuffer( 0 );

        // convert the meta data into a hash table for faster access
        const unsigned int numObjects = m_InputMetaFilter->mData.length();
        m_LabelToUncertaintyHash.reserve( numObjects );
        for (unsigned int i=0; i<numObjects; ++i)
        {
            typename TImageType::IndexType index;
            index[0] = m_InputMetaFilter->mData[i].at(2);
            index[1] = m_InputMetaFilter->mData[i].at(3);

            if (TImageType::ImageDimension > 2)
                index[2] = m_InputMetaFilter->mData[i].at(4);

            unsigned int currentValue = (unsigned int)(m_IntensityScale * input->GetPixel(index) + m_LabelOffset);

            if (currentValue > 0)
            {
                m_LabelToUncertaintyHash.insert(currentValue, m_InputMetaFilter->mData[i].at(m_UncertaintyFeature));
            }
            /*
            else
            {
                typename itk::ImageRegionIteratorWithIndex<TImageType> outputIterator(output, input->GetLargestPossibleRegion());
                outputIterator.GoToBegin();

                // iterate over the thread region and set the uncertainty value as new intensity
                int radius = 10;
                while(!outputIterator.IsAtEnd())
                {
                    typename TImageType::IndexType currentIndex = outputIterator.GetIndex();

                    float distance = 0;
                    for (int j=0; j<TImageType::ImageDimension; ++j)
                        distance += (currentIndex[j]-index[j]) * (currentIndex[j]-index[j]);
                    distance = sqrtf(distance);
                    
                    if (distance < radius)
                    {
                        outputIterator.Set(m_InputMetaFilter->mData[i].at(m_UncertaintyFeature));
                    }

                    ++outputIterator;
                }
            }
             */
        }
	}

	std::cout << "Hier noch kein fehler ..." << std::endl;
}


// the thread generate data
template <class TImageType>
void LabelImageToUncertaintyMapFilter<TImageType>::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId)
{
    // Allocate output
    typename TImageType::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();

	// initialize the image iterators
	typename itk::ImageRegionConstIterator<TImageType> inputIterator(input, outputRegionForThread);
	typename itk::ImageRegionIterator<TImageType> outputIterator(output, outputRegionForThread);
	inputIterator.GoToBegin();
	outputIterator.GoToBegin();

	// iterate over the thread region and set the uncertainty value as new intensity
	while(!inputIterator.IsAtEnd())
	{
		//unsigned int currentValue = (unsigned int)(m_IntensityScale*inputIterator.Value() + m_LabelOffset);
		//unsigned int currentValue = (unsigned int)(m_IntensityScale*inputIterator.Value() + m_LabelOffset);
		if (inputIterator.Value() > 0 && outputIterator.Value() <= 0)
		{
			//std::cout << "Using current value of " << currentValue << " for input iterator " << inputIterator.Value() << std::endl;
			outputIterator.Set(m_LabelToUncertaintyHash[inputIterator.Value()]);
		}

		++inputIterator;
		++outputIterator;
	}
}


// after threaded generate data
template <class TImageType>
void LabelImageToUncertaintyMapFilter<TImageType>::AfterThreadedGenerateData()
{
}

} // end namespace itk

#endif
