/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef __XPIWIT_FUSEROTATIONIMAGESFILTER_H
#define __XPIWIT_FUSEROTATIONIMAGESFILTER_H

// include required headers
#include "../Base/Management/ITKDefinitions.h"
#include "itkExtractKeyPointsImageFilter.h"
#include "itkExtractRegionPropsImageFilter.h"
#include "itkGradientMagnitudeRecursiveGaussianImageFilter.h"
#include "itkGradientMagnitudeImageFilter.h"
#include "itkBoundedReciprocalImageFilter.h"
//#include "itkMultiplyByConstantImageFilter.h"
#include "itkMultiplyImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkExtractImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkMacro.h"
#include <string>

using namespace XPIWIT;

namespace itk
{

/**
 * @class FuseRotationImagesFilter
 * Parallelized image fusion for images with known transformation.
 * TODO: Add automated transformation estimation.
 */
template <class TImageType>
class ITK_EXPORT FuseRotationImagesFilter : public ImageToImageFilter<TImageType, TImageType>
{
    public:
        // Extract dimension from input and output image.
        itkStaticConstMacro(ImageDimension, unsigned int, TImageType::ImageDimension);
        typedef FuseRotationImagesFilter Self;
        typedef ImageToImageFilter<TImageType,TImageType> Superclass;
        typedef SmartPointer<Self> Pointer;
        typedef SmartPointer<const Self> ConstPointer;
        itkNewMacro(Self);

        // definition of variable type abbreviations
        typedef TImageType ImageType;
        typedef typename ImageType::PixelType PixelType;
        typedef typename ImageType::RegionType InputImageRegionType;
        typedef typename ImageType::RegionType OutputImageRegionType;

        /**
         * The default constructor.
         */
        FuseRotationImagesFilter();

        /**
         * The destructor.
         */
        virtual ~FuseRotationImagesFilter();

        // get and set macros for input filename
        void SetTranslation(const vnl_vector_fixed<double,3>& translation) { mTranslation = translation; }
        void SetRotationMatrix(const vnl_matrix_fixed<double,3,3>& rotationMatrix) { mRotationMatrix = rotationMatrix; }

		void SetRotationImage(const TImageType* image)        { mRotatedImage = image; }

    protected:

        /**
         * Functions for data generation.
         * @param outputRegionForThread the region to be processed.
         * @param threadId the id of the current thread.
         */
        void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId) override;

        /**
         * functions for prepareing and post-processing the threaded generation.
         */
        void BeforeThreadedGenerateData() override;
        void AfterThreadedGenerateData() override;

        const TImageType* mRotatedImage;	// rotated image

        vnl_matrix_fixed<double,3,3> mRotationMatrix;
        vnl_vector_fixed<double,3> mTranslation;
};

} // namespace itk

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkFuseRotationImagesFilter.txx"
#endif

#endif
