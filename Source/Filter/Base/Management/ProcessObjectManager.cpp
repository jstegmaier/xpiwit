/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// include required headers
#include "ProcessObjectManager.h"
#include "ProcessObjectBase.h"
#include "ProcessObjectProxy.h"

namespace XPIWIT
{

// initialize the singleton instance to zero
ProcessObjectManager* ProcessObjectManager::mInstance = 0;


// returns a pointer to the singleton instance or creates it
ProcessObjectManager *ProcessObjectManager::GetInstance()
{
	static QMutex mutex;
	if (!mInstance)
	{
		mutex.lock();
		if (!mInstance)
			mInstance = new ProcessObjectManager();
		mutex.unlock();
	}

	return mInstance;
}


// deletes the singleton instance
void ProcessObjectManager::DeleteInstance()
{
	static QMutex mutex;
	mutex.lock();
	delete mInstance;
	mInstance = 0;
	mutex.unlock();
}


// the default constructor
ProcessObjectManager::ProcessObjectManager() : QObject()
{
	mProcessObjectWidgetList.clear();
	mProcessObjectList.clear();
}


// the destructor
ProcessObjectManager::~ProcessObjectManager()
{
	qDeleteAll( mProcessObjectWidgetList );
	mProcessObjectWidgetList.clear();
	qDeleteAll( mProcessObjectList );
	mProcessObjectList.clear();
}
   

// function that allows filters to register themselves
void ProcessObjectManager::Register(ProcessObjectProxyBase* filter)
{
    mProcessObjectList.append(filter);
}


// returns the process object list (used to print all contained filters)
QList<ProcessObjectBase*> ProcessObjectManager::GetProcessObjectList()
{
	if (mProcessObjectWidgetList.length() == 0)
	{
		const int numFilters = mProcessObjectList.length();
		for (int i=0; i<numFilters; ++i)
		{
			if ((StringCompare("float", mProcessObjectList.at(i)->GetType()) && mProcessObjectList.at(i)->GetDimension() == 3) ||
				StringCompare("MetaReader", mProcessObjectList.at(i)->GetName()))
				mProcessObjectWidgetList.append(mProcessObjectList.at(i)->CreateObject());
		}
	}

	// return the process object list
	return mProcessObjectWidgetList;
}


// case insensitive string compare
bool ProcessObjectManager::StringCompare( QString str1, QString str2 ){

	if( str1.toLower().compare( str2.toLower() ) == 0)
		return true;

	return false;
}


// get the process object instance based on filter name, type and image dimension
ProcessObjectBase *ProcessObjectManager::GetProcessObjectInstance( QString name, QStringList types, QList<int> dimensions )
{
	// initialize the object
	ProcessObjectBase* processObjectBase = NULL;

	// return if dimension is not set
	if( dimensions.isEmpty() )
	{
		Logger::GetInstance()->WriteToAll("! Filter: " + name + " can not be instantiated, dimension information is missing.\n");
		return processObjectBase;
	}

	// set default type to float and overwrite with desired type if necessary
	QString imageType = "float";
	if (types.length() > 0)
		imageType = types.at(0);

	//Logger::GetInstance()->WriteToAll( "Trying to instantiate " + name + " with type " + imageType + " and dimension " + QString::number(dimensions.at(0)) + "\n");
        
	// iterate over the filter list and return the appropriate filter
    const int numFilters = mProcessObjectList.length();
    for (int i=0; i<numFilters; ++i)
    {
		//Logger::GetInstance()->WriteToAll( "Trying to instantiate " + name + " with " + mProcessObjectList.at(i)->GetName() + " " + mProcessObjectList.at(i)->GetType() + " " + QString::number(mProcessObjectList.at(i)->GetDimension()) + "\n");
		
        if ( StringCompare(name, mProcessObjectList.at(i)->GetName()) &&
                StringCompare(imageType, mProcessObjectList.at(i)->GetType()) &&
                dimensions.at(0) == mProcessObjectList.at(i)->GetDimension())
        {
            return mProcessObjectList.at(i)->CreateObject();
        }
    }

	// display error message if filter was not found
	if( processObjectBase == NULL )
	{
		Logger::GetInstance()->WriteToAll("! Filter: " + name + " can not be instantiated and will be skipped.\n");
		Logger::GetInstance()->WriteToAll("- Update of ProcessObjectManager may solve this error.\n");
	}

	// return null pointer if filter was not found
	return processObjectBase;
}

} // namespace XPIWIT
