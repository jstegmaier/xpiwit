/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef PROCESSOBJECTSETTINGS_H
#define PROCESSOBJECTSETTINGS_H

// qt header
#include <QtCore/QSettings>
#include <QtCore/QList>
#include <QtCore/QString>


namespace XPIWIT
{

/**
 * stores one specific settings.
 */
class ProcessObjectSetting : public QObject
{
    public:
        enum SettingValueType
        {
            SETTINGVALUETYPE_STRING	= 0,
            SETTINGVALUETYPE_DOUBLE	= 1,
            SETTINGVALUETYPE_INT	= 2,
            SETTINGVALUETYPE_BOOLEAN= 3
        };


        /**
         * The default constructor.
         */
        ProcessObjectSetting() {}


        /**
         * The more advanced constructor.
         * @param name The name of the Ipopt setting.
         * @param value The initial value for the Ipopt setting.
         * @param type The value type of the ipopt setting.
         * @param activated Flag weather the setting is active or not.
         */
        ProcessObjectSetting(QString name, QString value, ProcessObjectSetting::SettingValueType type, QString description = "", bool activated = true)
        {
            mName		= name;
            mValue		= value;
            mType		= type;
            mActivated	= activated;
            mDescription = description;
        }


        /**
         * the destructor.
         */
        virtual ~ProcessObjectSetting() {}


        /**
         * functions to set the setting parameters.
         */
        void SetSettingName(QString name)					{ mName = name; }
        void SetSettingDescription(QString description)		{ mDescription = description; }
        void SetSettingValue(QString value)					{ mValue = value; }
        void SetSettingType(SettingValueType type)			{ mType = type; }
        void SetSettingActivated(bool activated = true)		{ mActivated = activated; }


        /**
         * functions to get the settings parameters.
         */
        QString GetSettingName()							{ return mName; }
        QString GetSettingDescription()						{ return mDescription; }
        QString GetSettingValue()							{ return mValue; }
        int GetSettingType()								{ return mType; }
        bool GetSettingActivated()							{ return mActivated; }

        void ValueChanged(const QString& value)				{ mValue = value; }
        void ValueChanged(const int& value)					{ mValue = QString::number( value ); }

    private:
        QString mName;
        QString mDescription;
        QString mValue;
        SettingValueType mType;
        bool	mActivated;
};

/**
 * class to store the ipopt options.
 */
class ProcessObjectSettings
{
    public:
        /**
         * the constructor.
         */
        ProcessObjectSettings()
        {
            mSettings = QList<ProcessObjectSetting*>();
        }


        /**
         * the destructor.
         */
        ~ProcessObjectSettings()
        {
            qDeleteAll( mSettings );
            mSettings.clear();
        }


        /**
         * returns list of default settings.
         */
        static ProcessObjectSetting* GetDefaultSetting(int group, int index)
        {
            /*
            // output
            if (group == 0)
            {
                switch (index)
                {
                    case 0: return new ProcessObjectSetting("print_level", "5", IpoptSetting::SETTINGVALUETYPE_INT, true); break;
                    case 1: return new ProcessObjectSetting("print_user_options", "no", IpoptSetting::SETTINGVALUETYPE_STRING, true); break;
                    case 2: return new ProcessObjectSetting("print_options_documentation", "no", IpoptSetting::SETTINGVALUETYPE_STRING, true); break;
                    case 3: return new ProcessObjectSetting("output_file", "", IpoptSetting::SETTINGVALUETYPE_STRING, true); break;
                    case 4: return new ProcessObjectSetting("file_print_level", "5", IpoptSetting::SETTINGVALUETYPE_INT, true); break;
                    case 5: return new ProcessObjectSetting("output_file_name", "", IpoptSetting::SETTINGVALUETYPE_STRING, true); break;
                    default: return NULL;
                }
            }
            */

            return NULL;
        }


        /**
         * returns the number of default settings.
         */
        static int GetNumDefaultSettings(int groupIndex)
        {
            /*
            switch (groupIndex) {
                case 0:  return 6;

                default: return -1;
            }
            */
            return -1;
        }


        /**
         * Returns the number of groups.
         * @return The number of groups.
         */
        static int GetNumGroups() { return 0; }



        /**
         * Returns a default setting for the given index.
         * @param index The index of the desired setting.
         * @return The setting for the index.
         */
        static ProcessObjectSetting* GetDefaultSetting(int index)
        {
            const int numGroups = GetNumGroups();
            for (int i=0; i<numGroups; ++i)
            {
                const int numDefaultSettings = GetNumDefaultSettings(i);
                for (int j=0; j<numDefaultSettings; ++j)
                {
                    if (index == 0)
                        return GetDefaultSetting( i, j );

                    index--;
                }

                index--;
            }

            return NULL;
        }


        /**
         * Returns the group name for a given index.
         * @return The group name for a given index.
         */
        static QString GetGroupName(int index)
        {
            //switch (index) {
            //    default:
                return QString( "" );
            //}
        }


        /**
         * Function to add a setting.
         * @param name The settings name.
         * @param value The value of the setting.
         * @param type The type of the setting.
         * @param activated Specifies if the setting should be applied.
         */
        void AddSetting(QString name, QString value, ProcessObjectSetting::SettingValueType type, QString description = "", bool activated = true)	{ mSettings.append( new ProcessObjectSetting(name, value, type, description, activated) ); }
        void AddSetting(ProcessObjectSetting* setting)																		{ mSettings.append( setting ); }


        /**
         * Function to completely remove a setting.
         * @param index The index of the setting to remove.
         */
        void RemoveSetting(int index) { mSettings.removeAt( index ); }


        /**
         * some setter functions.
         */
        void SetSettingName(int index, QString name)				{ mSettings[index]->SetSettingName( name ); }
        void SetSettingDescription(int index, QString description)	{ mSettings[index]->SetSettingDescription( description ); }
        void SetSettingValue(int index, QString value)				{ mSettings[index]->SetSettingValue( value ); }
        void SetSettingValue(const QString& name, QString value);
        void SetSettingType(int index, ProcessObjectSetting::SettingValueType value) { mSettings[index]->SetSettingType( value ); }
        void SetSettingActivated(int index, bool activated = true)	{ mSettings[index]->SetSettingActivated( activated ); }


        /**
         * functions to get the settings parameters.
         */
        QString GetSettingName(int index)							{ return mSettings[index]->GetSettingName(); }
        QString GetSettingDescription(int index)					{ return mSettings[index]->GetSettingDescription(); }
        QString GetSettingValue(int index)							{ return mSettings[index]->GetSettingValue(); }
        QString GetSettingValue(const QString& name);
        int GetSettingType(int index)								{ return mSettings[index]->GetSettingType(); }
        bool GetSettingActivated(int index)							{ return mSettings[index]->GetSettingActivated(); }
        int GetNumSettings()										{ return mSettings.size(); }
        ProcessObjectSetting* GetSetting(int index)					{ return mSettings[index]; }

    private:
        QList<ProcessObjectSetting*>	mSettings;
        static QList<ProcessObjectSetting*> mDefaultSettings;
};

} // namespace XPIWIT

#endif // PROCESSOBJECTSETTINGS_H
