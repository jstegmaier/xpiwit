/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef PROCESSOBJECTPROXY_H
#define PROCESSOBJECTPROXY_H

// include required headers
#include "ProcessObjectManager.h"

namespace XPIWIT
{

class ProcessObjectProxyBase
{
    public:
        ProcessObjectProxyBase();
        virtual ~ProcessObjectProxyBase() {}
        virtual ProcessObjectBase* CreateObject() const = 0;
        virtual QString GetName() const = 0;
        virtual QString GetType() const = 0;
        virtual int GetDimension() const = 0;
};

inline ProcessObjectProxyBase::ProcessObjectProxyBase()
{
	ProcessObjectManager::GetInstance()->Register(this);
}

/**
 *	@class ProcessObjectBase
 *	The base class for image to image filter wrapper.
 */
template <class T>
class ProcessObjectProxy : public ProcessObjectProxyBase
{
public:
    virtual ~ProcessObjectProxy() {}
    virtual ProcessObjectBase* CreateObject() const { return new T; }
    virtual QString GetName() const { return T::GetName(); }
    virtual QString GetType() const { return T::GetType(); }
    virtual int GetDimension() const { return T::GetDimension(); }
};

} // namespace XPIWIT

#endif // PROCESSOBJECTBASE_H
