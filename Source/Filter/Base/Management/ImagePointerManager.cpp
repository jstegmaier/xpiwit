/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// file header
#include "ImagePointerManager.h"


namespace XPIWIT
{

ImagePointerManager::ImagePointerManager()
{
    SetAllToNull();
}

ImagePointerManager::~ImagePointerManager()
{
	DeleteCurrentPointer();
	SetAllToNull();
}

void ImagePointerManager::SetReleaseDataFlag(bool releaseDataFlag)
{
	if( mImage2Char.IsNotNull() )
		mImage2Char->SetReleaseDataFlag( releaseDataFlag );
	if( mImage2Short.IsNotNull() )
		mImage2Short->SetReleaseDataFlag( releaseDataFlag );
	if( mImage2Int.IsNotNull() )
		mImage2Int->SetReleaseDataFlag( releaseDataFlag );
	if( mImage2Long.IsNotNull() )
		mImage2Long->SetReleaseDataFlag( releaseDataFlag );
	if( mImage2UChar.IsNotNull() )
		mImage2UChar->SetReleaseDataFlag( releaseDataFlag );
	if( mImage2UShort.IsNotNull() )
		mImage2UShort->SetReleaseDataFlag( releaseDataFlag );
	if( mImage2UInt.IsNotNull() )
		mImage2UInt->SetReleaseDataFlag( releaseDataFlag );
	if( mImage2ULong.IsNotNull() )
		mImage2ULong->SetReleaseDataFlag( releaseDataFlag );
	if( mImage2Float.IsNotNull() )
		mImage2Float->SetReleaseDataFlag( releaseDataFlag );
	if( mImage2Double.IsNotNull() )
		mImage2Double->SetReleaseDataFlag( releaseDataFlag );

	if( mImage3Char.IsNotNull() )
		mImage3Char->SetReleaseDataFlag( releaseDataFlag );
	if( mImage3Short.IsNotNull() )
		mImage3Short->SetReleaseDataFlag( releaseDataFlag );
	if( mImage3Int.IsNotNull() )
		mImage3Int->SetReleaseDataFlag( releaseDataFlag );
	if( mImage3Long.IsNotNull() )
		mImage3Long->SetReleaseDataFlag( releaseDataFlag );
	if( mImage3UChar.IsNotNull() )
		mImage3UChar->SetReleaseDataFlag( releaseDataFlag );
	if( mImage3UShort.IsNotNull() )
		mImage3UShort->SetReleaseDataFlag( releaseDataFlag );
	if( mImage3UInt.IsNotNull() )
		mImage3UInt->SetReleaseDataFlag( releaseDataFlag );
	if( mImage3ULong.IsNotNull() )
		mImage3ULong->SetReleaseDataFlag( releaseDataFlag );
	if( mImage3Float.IsNotNull() )
		mImage3Float->SetReleaseDataFlag( releaseDataFlag );
	if( mImage3Double.IsNotNull() )
		mImage3Double->SetReleaseDataFlag( releaseDataFlag );

	if( mImage4Char.IsNotNull() )
		mImage4Char->SetReleaseDataFlag( releaseDataFlag );
	if( mImage4Short.IsNotNull() )
		mImage4Short->SetReleaseDataFlag( releaseDataFlag );
	if( mImage4Int.IsNotNull() )
		mImage4Int->SetReleaseDataFlag( releaseDataFlag );
	if( mImage4Long.IsNotNull() )
		mImage4Long->SetReleaseDataFlag( releaseDataFlag );
	if( mImage4UChar.IsNotNull() )
		mImage4UChar->SetReleaseDataFlag( releaseDataFlag );
	if( mImage4UShort.IsNotNull() )
		mImage4UShort->SetReleaseDataFlag( releaseDataFlag );
	if( mImage4UInt.IsNotNull() )
		mImage4UInt->SetReleaseDataFlag( releaseDataFlag );
	if( mImage4ULong.IsNotNull() )
		mImage4ULong->SetReleaseDataFlag( releaseDataFlag );
	if( mImage4Float.IsNotNull() )
		mImage4Float->SetReleaseDataFlag( releaseDataFlag );
	if( mImage4Double.IsNotNull() )
		mImage4Double->SetReleaseDataFlag( releaseDataFlag );
}

void ImagePointerManager::DeleteCurrentPointer()
{	
	return;
	if( mImage2Char.IsNotNull() )
		mImage2Char->ReleaseData();
	if( mImage2Short.IsNotNull() )
		mImage2Short->ReleaseData();
	if( mImage2Int.IsNotNull() )
		mImage2Int->ReleaseData();
	if( mImage2Long.IsNotNull() )
		mImage2Long->ReleaseData();
	if( mImage2UChar.IsNotNull() )
		mImage2UChar->ReleaseData();
	if( mImage2UShort.IsNotNull() )
		mImage2UShort->ReleaseData();
	if( mImage2UInt.IsNotNull() )
		mImage2UInt->ReleaseData();
	if( mImage2ULong.IsNotNull() )
		mImage2ULong->ReleaseData();
	if( mImage2Float.IsNotNull() )
		mImage2Float->ReleaseData();
	if( mImage2Double.IsNotNull() )
		mImage2Double->ReleaseData();
	
	if( mImage3Char.IsNotNull() )
		mImage3Char->ReleaseData();
	if( mImage3Short.IsNotNull() )
		mImage3Short->ReleaseData();
	if( mImage3Int.IsNotNull() )
		mImage3Int->ReleaseData();
	if( mImage3Long.IsNotNull() )
		mImage3Long->ReleaseData();
	if( mImage3UChar.IsNotNull() )
		mImage3UChar->ReleaseData();
	if( mImage3UShort.IsNotNull() )
		mImage3UShort->ReleaseData();
	if( mImage3UInt.IsNotNull() )
		mImage3UInt->ReleaseData();
	if( mImage3ULong.IsNotNull() )
		mImage3ULong->ReleaseData();
	if( mImage3Float.IsNotNull() )
		mImage3Float->ReleaseData();
	if( mImage3Double.IsNotNull() )
		mImage3Double->ReleaseData();
		
	if( mImage4Char.IsNotNull() )
		mImage4Char->ReleaseData();
	if( mImage4Short.IsNotNull() )
		mImage4Short->ReleaseData();
	if( mImage4Int.IsNotNull() )
		mImage4Int->ReleaseData();
	if( mImage4Long.IsNotNull() )
		mImage4Long->ReleaseData();
	if( mImage4UChar.IsNotNull() )
		mImage4UChar->ReleaseData();
	if( mImage4UShort.IsNotNull() )
		mImage4UShort->ReleaseData();
	if( mImage4UInt.IsNotNull() )
		mImage4UInt->ReleaseData();
	if( mImage4ULong.IsNotNull() )
		mImage4ULong->ReleaseData();
	if( mImage4Float.IsNotNull() )
		mImage4Float->ReleaseData();
	if( mImage4Double.IsNotNull() )
		mImage4Double->ReleaseData();
}

void ImagePointerManager::SetAllToNull()
{
    mImageType.first = ProcessObjectType::DATATYPE_UNDEFINED;
    mImageType.second = 0;

    mImage2Char   = NULL;
    mImage2Short  = NULL;
    mImage2Int    = NULL;
    mImage2Long	  = NULL;
    mImage2UChar  = NULL;
	mImage2UShort = NULL;
    mImage2UInt   = NULL;
    mImage2ULong  = NULL;
    mImage2Float  = NULL;
    mImage2Double = NULL;

    mImage3Char   = NULL;
    mImage3Short  = NULL;
    mImage3Int    = NULL;
    mImage3Long	  = NULL;
    mImage3UChar  = NULL;
	mImage3UShort = NULL;
    mImage3UInt   = NULL;
    mImage3ULong  = NULL;	
	mImage3Float  = NULL;
    mImage3Double = NULL;
		
    mImage4Char   = NULL;
    mImage4Short  = NULL;
    mImage4Int    = NULL;
    mImage4Long   = NULL;
    mImage4UChar  = NULL;
	mImage4UShort = NULL;
    mImage4UInt   = NULL;
    mImage4ULong  = NULL;
    mImage4Float  = NULL;
    mImage4Double = NULL;
}

} // namespace XPIWIT
