/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef PROCESSOBJECTPIPELINE_H
#define PROCESSOBJECTPIPELINE_H

// namespace header
#include "ProcessObjectBase.h"
#include "ITKDefinitions.h"

// project header
#include "../../../Core/CMD/CMDPipelineArguments.h"
#include "../../../Core/XML/AbstractPipeline.h"

// qt header
#include <QtCore/QObject>
#include <QtCore/QSettings>
#include <QtCore/QDateTime>
#include <QtCore/QMutex>
#include <QtCore/QPair>


namespace XPIWIT
{

/**
 *	@class ProcessObjectPipeline
 *	Singleton
 *  Holds a list of all implemented Filters
 */
class ProcessObjectPipeline : public QObject
{
	public:
		/**
		 * The constructor.
		 */
		ProcessObjectPipeline();

		void run( CMDPipelineArguments *, AbstractPipeline );

	private:
	protected:
};

} // namespace XPIWIT

#endif // PROCESSOBJECTPIPELINE_H
