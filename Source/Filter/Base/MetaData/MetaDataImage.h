/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef METADATAIMAGE_H
#define METADATAIMAGE_H

// namespace header
#include "MetaDataFilter.h"

// qt header
#include <QtCore/QList>
#include <QtCore/QStringList>


namespace XPIWIT
{

/**
 * @class MetaDataBase
 * @brief The MetaDataBase class provides meta information and data output from filters
 */

class MetaDataImage
{
	public:
		MetaDataImage();
		~MetaDataImage();

		void SetMetaData( QString, MetaDataFilter * );
		MetaDataFilter *CreateMetaOutput( QString filterId );

		MetaDataFilter *GetMetaData( QString filterId );

		QList< MetaDataFilter *> GetMetaDataList() { return mMetaDataFilters; }

	private:
		QStringList mFilterIds;
		QList< MetaDataFilter *> mMetaDataFilters;
};

} //namespace XPIWIT

#endif // METADATAIMAGE_H

