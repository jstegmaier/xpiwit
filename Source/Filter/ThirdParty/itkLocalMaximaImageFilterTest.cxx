//  author: Bryn Lloyd, Computational Radiology Laboratory, 2005


#include "itkImage.h"
#include "itkImageRegionIterator.h"


#include "itkLocalMaximumImageFilter.h"

#include "itkPointSet.h"


/**
 * This code illustrates the usage of the itkLocalMaximaImageFilter, and tests  (in a simple way) if if worked.
 *
 */

//main
int itkLocalMaximaImageFilterTest( int argc, char * argv[] )
{


  /** standard typedefs for the image types */
  const unsigned int Dimension = 2;
  typedef   float  PixelType;

  typedef itk::Image< PixelType,   Dimension >        ImageType;
  
  typedef itk::PointSet< PixelType,  Dimension>   PointsetType;
  
  typedef itk::ImageRegionIterator<ImageType>  IteratorType;
  
  ImageType::Pointer  image = ImageType::New();
  ImageType::RegionType  region;
  ImageType::SizeType    size;
  size.Fill(10);
  ImageType::IndexType  index;
  index.Fill(0);
  region.SetIndex( index );
  region.SetSize( size );
  
  image->SetRegions( region );
  image->Allocate();
  image->FillBuffer( 0 );
  
  
  ImageType::IndexType  ind1;
  ind1[0] = 1;  
  ind1[1] = 1;  
  image->SetPixel( ind1 ,1);
    
  ImageType::IndexType  ind2;
  ind2[0] = 3;  
  ind2[1] = 3;  
  image->SetPixel( ind2 , 2);
    
  ImageType::IndexType  ind3;
  ind3[0] = 3;  
  ind3[1] = 4;  
  image->SetPixel( ind3 , 3);
  
  ImageType::IndexType  ind4;
  ind4[0] = 5;  
  ind4[1] = 4;  
  image->SetPixel( ind4 , 1);
  
  /** This filter extracts the local maxima in an image. It looks for points, where the intensity 
   *   is larger than the intensity at all neighboring points. The neighborhood is defined by a radius.
   *   The filter is derived from an image to mesh filter, so the GetOutput() method returns a point set. */
  typedef itk::LocalMaximumImageFilter <ImageType, PointsetType> LocalMaxFilterType;
  LocalMaxFilterType::Pointer localMaxfilter = LocalMaxFilterType::New();
   localMaxfilter->SetInput( image );
   itk::Size<Dimension> radius;
   for (unsigned int k=0; k<Dimension; k++)
     radius[k] = 1;
   localMaxfilter->SetRadius( radius );
   localMaxfilter->SetThreshold( 1 );
   localMaxfilter->Update();
   
  PointsetType::Pointer outputPointset = localMaxfilter->GetOutput();
  PointsetType::PointsContainer::Pointer points = outputPointset->GetPoints();
  
  PointsetType::PointsContainer::Iterator  pointIterator = points->Begin();
  PointsetType::PointsContainer::Iterator  end_point = points->End();

  if (outputPointset->GetNumberOfPoints()==1) {
    PointsetType::PointType p = pointIterator.Value();
    ImageType::IndexType ind;
    image->TransformPhysicalPointToIndex( p, ind );
    if (ind!=ind3) {
      std::cerr << "itkLocalMaximaImageFilterTest   failed! " << std::endl;
      return 1;
      }
  }
  else {
    std::cerr << "itkLocalMaximaImageFilterTest   failed! " << std::endl;
    return 1;
  }

   
  return 0;
}

