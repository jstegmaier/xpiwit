/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "ExtractSeedBasedIntensityWindowFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkBoxMeanImageFilter.h"


namespace XPIWIT
{

// the default constructor
template< class TInputImage >
ExtractSeedBasedIntensityWindowFilterWidget<TInputImage>::ExtractSeedBasedIntensityWindowFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = ExtractSeedBasedIntensityWindowFilterWidget<TInputImage>::GetName();
	this->mDescription = "Extracts the mean intensity values of an image from provided seed locations.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(1);
	this->mObjectType->AppendMetaInputType("KeyPoints");
	this->mObjectType->SetNumberMetaOutputs(1);
	this->mObjectType->AppendMetaOutputType("KeyPoints");
	
    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Radius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The radius of the window to use for the mean intensity extraction." );
    processObjectSettings->AddSetting( "UseImageSpacing", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, the radius is scaled with respect to the image spacing." );
	processObjectSettings->AddSetting( "UseImageSpacingForKeyPoints", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, the keypoints are scaled with respect to the image spacing.");
	processObjectSettings->AddSetting( "Threshold", "-1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "If larger than zero, used as a threshold, ie only seeds with larger values are saved." );
	processObjectSettings->AddSetting("SNRThreshold", "0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "If larger than zero, used as a threshold for the SNR criterion, ie only seeds with larger interior compared to exterior intensity are saved.");
	processObjectSettings->AddSetting("RadiusMultiplier", "2", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Multiplier for the radius to specify the exterior region for the SNR computation.");

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
ExtractSeedBasedIntensityWindowFilterWidget<TInputImage>::~ExtractSeedBasedIntensityWindowFilterWidget()
{
}


// the update function
template< class TInputImage >
void ExtractSeedBasedIntensityWindowFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	this->mMetaOutputs.at(0)->mIsMultiDimensional = true;

	// get the input image
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
    // get filter parameters
    const unsigned int windowRadius = processObjectSettings->GetSettingValue( "Radius" ).toInt();
	const float threshold = processObjectSettings->GetSettingValue( "Threshold" ).toFloat();
	const float snrRatioThreshold = processObjectSettings->GetSettingValue("SNRThreshold").toFloat();
	const float radiusMultiplier = processObjectSettings->GetSettingValue("RadiusMultiplier").toInt();
	const bool useImageSpacing = processObjectSettings->GetSettingValue( "UseImageSpacing" ).toInt() > 0;
	const bool useImageSpacingForKeyPoints = processObjectSettings->GetSettingValue( "UseImageSpacingForKeyPoints").toInt() > 0;
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	
	// iterate over all seed points and 
	typedef typename itk::BoxMeanImageFilter<TInputImage> BoxMeanFilterType;
	typedef typename BoxMeanFilterType::RadiusType RadiusType;

	// setup the radius for the box filter	
	RadiusType radius;
	RadiusType outerRadius;
	for (int i=0; i<TInputImage::ImageDimension; ++i)
	{
		if (useImageSpacingForKeyPoints == true)
			radius[i] = int(0.5 + (float)windowRadius / inputImage->GetSpacing()[i]);
		else
			radius[i] = windowRadius;

		// specify the outer radius
		outerRadius[i] = radiusMultiplier * radius[i];

		Logger::GetInstance()->WriteLine( "- Using radius " + QString().number(radius[i]) + QString(" for dimension ") + QString().number(i) );
	}

	// allocate the output image
	typename TInputImage::Pointer seedImage = TInputImage::New();
	seedImage->SetRegions(inputImage->GetLargestPossibleRegion());
	seedImage->SetOrigin(inputImage->GetOrigin());
	seedImage->SetSpacing(inputImage->GetSpacing());
	seedImage->Allocate();
	seedImage->FillBuffer(0);

	bool oldReleaseDataFlag = inputImage->GetReleaseDataFlag();
	inputImage->SetReleaseDataFlag(false);
	
	// setup the box filter
	typename BoxMeanFilterType::Pointer boxMeanFilterInterior = BoxMeanFilterType::New();
	boxMeanFilterInterior->SetInput( inputImage );
	boxMeanFilterInterior->SetNumberOfWorkUnits( maxThreads );
	boxMeanFilterInterior->SetRadius( radius );
	itkTryCatch(boxMeanFilterInterior->Update(), "ExtractSeedBasedIntensityWindowFilterWidget: Update box filter" );

	inputImage->SetReleaseDataFlag(oldReleaseDataFlag);
	typename BoxMeanFilterType::Pointer boxMeanFilterExterior = BoxMeanFilterType::New();
	boxMeanFilterExterior->SetInput(inputImage);
	boxMeanFilterExterior->SetNumberOfWorkUnits(maxThreads);
	boxMeanFilterExterior->SetRadius(outerRadius);
	itkTryCatch(boxMeanFilterExterior->Update(), "ExtractSeedBasedIntensityWindowFilterWidget: Update exterior box filter");
	

	// compute the number of voxels in each region
	const int numInteriorVoxels = std::pow((windowRadius * 2 + 1), TInputImage::ImageDimension);
	const int numExteriorVoxels = std::pow((radiusMultiplier * windowRadius * 2 + 1), TInputImage::ImageDimension);

	// set the output meta information
	this->mMetaOutputs.at(0)->mTitle = this->mMetaInputs.at(0)->mTitle;
	this->mMetaOutputs.at(0)->mTitle << "meanIntensity";
	this->mMetaOutputs.at(0)->mTitle << "snrCriterion";
	this->mMetaOutputs.at(0)->mType = this->mMetaInputs.at(0)->mType;
    this->mMetaOutputs.at(0)->mType << "float";
	this->mMetaOutputs.at(0)->mType << "float";

	// loop trough all seed points and extract the box filter value
	const int numKeyPoints = this->mMetaInputs.at(0)->mData.length();
	for(int i=0; i<numKeyPoints; i++)
	{
		QList<float> line = this->mMetaInputs.at(0)->mData.at(i);

		typename TInputImage::IndexType index;
		for (int j = 0; j < TInputImage::ImageDimension; ++j)
		{
			if (useImageSpacing == true)
				index[j] = int(0.5 + line.at(j + 2) / inputImage->GetSpacing()[j]);
			else
				index[j] = int(0.5 + line.at(j + 2));
		}

		// compute the current snrRatio
		float interiorMean = boxMeanFilterInterior->GetOutput()->GetPixel(index);
		float exteriorMean = boxMeanFilterExterior->GetOutput()->GetPixel(index);
		float snrRatio = interiorMean / ( (exteriorMean*numExteriorVoxels - interiorMean*numInteriorVoxels) / (numExteriorVoxels-numInteriorVoxels) );

		// add the mean intensity to the csv
		line.append(boxMeanFilterInterior->GetOutput()->GetPixel(index));
		line.append(snrRatio);

		// write keypoint to the output image and the data table
		//if (line.at(line.length()-1) >= threshold || (threshold < 0 && snrRatioThreshold < 0))
		if (interiorMean >= threshold && snrRatio >= snrRatioThreshold)
		{			
			seedImage->SetPixel(index, 1.0);
			this->mMetaOutputs.at(0)->mData.append( line );
		}
	}

	ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>(seedImage);
    mOutputImages.append( outputImage );

    // update the base class
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< ExtractSeedBasedIntensityWindowFilterWidget<Image2Float> > ExtractSeedBasedIntensityWindowFilterWidgetImage2Float;
static ProcessObjectProxy< ExtractSeedBasedIntensityWindowFilterWidget<Image3Float> > ExtractSeedBasedIntensityWindowFilterWidgetImage3Float;
static ProcessObjectProxy< ExtractSeedBasedIntensityWindowFilterWidget<Image2UShort> > ExtractSeedBasedIntensityWindowFilterWidgetImage2UShort;
static ProcessObjectProxy< ExtractSeedBasedIntensityWindowFilterWidget<Image3UShort> > ExtractSeedBasedIntensityWindowFilterWidgetImage3UShort;

} // namespace XPIWIT
