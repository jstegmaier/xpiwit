/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "WatershedBoundariesMergeTreeFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"


// qt header
#include <QFileInfo>


namespace XPIWIT
{

// the default constructor
template< class TInputImage0, class TInputImage1 >
WatershedBoundariesMergeTreeFilterWidget<TInputImage0, TInputImage1>::WatershedBoundariesMergeTreeFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = WatershedBoundariesMergeTreeFilterWidget<TInputImage0, TInputImage1>::GetName();
    this->mDescription = "Extracts watershed segment boundaries and merges supervoxels to complete objects.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(2);

    this->mObjectType->SetNumberImageInputs(2);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(2);

    this->mObjectType->SetNumberImageOutputs(2);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->AppendImageOutputType(2);

    this->mObjectType->SetNumberMetaInputs(1);
	this->mObjectType->AppendMetaInputType("RegionProps");
    this->mObjectType->SetNumberMetaOutputs(1);
	this->mObjectType->AppendMetaOutputType("WatershedBoundaries");

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting("MinimumVolume", "4000", ProcessObjectSetting::SETTINGVALUETYPE_INT, "All regions with smaller volume are merged.");
	processObjectSettings->AddSetting("MaximumVolume", "8000", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Stopping criterion for region merges, i.e., larger cells are not produced by the fusion.");
	processObjectSettings->AddSetting("MaximumAngle", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Stopping criterion for structure tensor vs. centroid distance angle. Larger angles will not be merged.");
	processObjectSettings->AddSetting("GenerateEdgeMap", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, the sort feature will be used as edge intensity. No segmentation merging is performed in this case.");
	processObjectSettings->AddSetting("UseBoundaryCriterion", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, the separating boundary rather belongs to background than foreground.");
	processObjectSettings->AddSetting("UseMinimumVolumeCriterion", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, objects smaller than the minimum volume will be fused (unless the maximum volume constraint is violated).");
	processObjectSettings->AddSetting("UseProbabilityMapCriterion", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, objects with an average edge probability smaller than 0.5 will be merged.");
	processObjectSettings->AddSetting("UseMeanRatioCriterion", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, objects are fused if the ratio of boundary intensity vs. interior intensity is below 1.");
	processObjectSettings->AddSetting("UseSphericityCriterion", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, objects are fused, if the sphericity after the merge is larger than before the merge (only useful for spherical objects).");
	processObjectSettings->AddSetting("DisableMVCOnBorder", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, objects are touching the border are not merged using the minimum volume criterion (to prevent false merges due to size).");
	processObjectSettings->AddSetting("UseStructureTensorAngle", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, the angle between the smallest structure tensor eigenvector and the centroid distance vector is considered.");
	processObjectSettings->AddSetting("ScaleByIntensity", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, valid merge candidate edges are additionally scaled by the separating boundary intensity.");
    processObjectSettings->AddSetting("UseCNNCorrection", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, super voxels are analyzed by a trained CNN to perform splitting operations.");

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage0, class TInputImage1>
WatershedBoundariesMergeTreeFilterWidget<TInputImage0, TInputImage1>::~WatershedBoundariesMergeTreeFilterWidget()
{
}


// the update function
template< class TInputImage0, class TInputImage1 >
void WatershedBoundariesMergeTreeFilterWidget<TInputImage0, TInputImage1>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	this->mMetaOutputs.at(0)->mIsMultiDimensional = true;

    // get filter parameters
    bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;
	bool generateEdgeMap = processObjectSettings->GetSettingValue("GenerateEdgeMap").toInt() > 0;
	const int numThreads = processObjectSettings->GetSettingValue("MaxThreads").toInt();
	const int minimumVolume = processObjectSettings->GetSettingValue("MinimumVolume").toInt();
	const int maximumVolume = processObjectSettings->GetSettingValue("MaximumVolume").toInt();
	const float maximumAngle = processObjectSettings->GetSettingValue("MaximumAngle").toFloat();
	
	bool useBoundaryCriterion = processObjectSettings->GetSettingValue("UseBoundaryCriterion").toInt() > 0;
	bool useMinimumVolumeCriterion = processObjectSettings->GetSettingValue("UseMinimumVolumeCriterion").toInt() > 0;
	bool useMeanRatioCriterion = processObjectSettings->GetSettingValue("UseMeanRatioCriterion").toInt() > 0;
	bool useProbabilityMapCriterion = processObjectSettings->GetSettingValue("UseProbabilityMapCriterion").toInt() > 0;
	bool useStructureTensorAngleCriterion = processObjectSettings->GetSettingValue("UseStructureTensorAngle").toInt() > 0;
	bool useSphericityCriterion = processObjectSettings->GetSettingValue("UseSphericityCriterion").toInt() > 0;
	bool scaleByIntensity = processObjectSettings->GetSettingValue("ScaleByIntensity").toInt() > 0;
	bool disableMVCOnBorder = processObjectSettings->GetSettingValue("DisableMVCOnBorder").toInt() > 0;
    bool useCNNCorrection = processObjectSettings->GetSettingValue("UseCNNCorrection").toInt() > 0;

	// get pointers to the input images
	typename TInputImage0::Pointer inputImage1 = mInputImages.at(0)->template GetImage<TInputImage0>();
	typename TInputImage1::Pointer inputImage2 = mInputImages.at(1)->template GetImage<TInputImage1>();

	// get pointers to the meta input and outputs
	MetaDataFilter* regionProps = this->mMetaInputs.at(0);
	MetaDataFilter* connectivity = this->mMetaOutputs.at(0);

    // initialize the extraction filter
	typename itk::WatershedBoundariesMergeTreeImageFilter<TInputImage0, TInputImage0>::Pointer extractWatershedBoundariesFilter = itk::WatershedBoundariesMergeTreeImageFilter<TInputImage0, TInputImage0>::New();
	extractWatershedBoundariesFilter->SetInput( inputImage1 );
	extractWatershedBoundariesFilter->SetNumberOfWorkUnits( numThreads );
	extractWatershedBoundariesFilter->SetInputMetaFilter( regionProps );
	extractWatershedBoundariesFilter->SetOutputMetaFilter(connectivity);
	extractWatershedBoundariesFilter->SetIntensityImage( inputImage2 );
	extractWatershedBoundariesFilter->SetVolumeConstraints( minimumVolume, maximumVolume );
	extractWatershedBoundariesFilter->SetMaximumAngle(maximumAngle);
	extractWatershedBoundariesFilter->SetGenerateEdgeMap(generateEdgeMap);
	extractWatershedBoundariesFilter->SetUseBoundaryCriterion(useBoundaryCriterion);
	extractWatershedBoundariesFilter->SetUseProbabilityMapCriterion(useProbabilityMapCriterion);
	extractWatershedBoundariesFilter->SetUseMinimumVolumeCriterion(useMinimumVolumeCriterion);
	extractWatershedBoundariesFilter->SetUseMeanRatioCriterion(useMeanRatioCriterion);
	extractWatershedBoundariesFilter->SetUseStructureTensorAngleCriterion(useStructureTensorAngleCriterion);
	extractWatershedBoundariesFilter->SetScaleByIntensity(scaleByIntensity);
	extractWatershedBoundariesFilter->SetUseSphericityCriterion(useSphericityCriterion);
	extractWatershedBoundariesFilter->SetDisableMinimumVolumeCriterionOnBorder(disableMVCOnBorder);
    extractWatershedBoundariesFilter->SetUseCNNCorrection(useCNNCorrection);

    // extract seed points
    extractWatershedBoundariesFilter->SetReleaseDataFlag( releaseDataFlag );
    itkTryCatch( extractWatershedBoundariesFilter->Update(), "WatershedBoundariesMergeTreeFilterWidget --> Update extraction filter!");
	
    ImageWrapper *outputImage0 = new ImageWrapper();
	outputImage0->SetRescaleFlag(false);
    outputImage0->SetImage<TInputImage0>( extractWatershedBoundariesFilter->GetOutput0() );
    mOutputImages.append( outputImage0 );

	ImageWrapper *outputImage1 = new ImageWrapper();
	outputImage1->SetRescaleFlag(false);
	outputImage1->SetImage<TInputImage0>(extractWatershedBoundariesFilter->GetOutput1());
	mOutputImages.append(outputImage1);

    // update the process object widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< WatershedBoundariesMergeTreeFilterWidget<Image2Float, Image2Float> > WatershedBoundariesMergeTreeFilterWidgetImage2Float;
static ProcessObjectProxy< WatershedBoundariesMergeTreeFilterWidget<Image3Float, Image3Float> > WatershedBoundariesMergeTreeFilterWidgetImage3Float;
static ProcessObjectProxy< WatershedBoundariesMergeTreeFilterWidget<Image2UShort, Image2UShort> > WatershedBoundariesMergeTreeFilterWidgetImage2UShort;
static ProcessObjectProxy< WatershedBoundariesMergeTreeFilterWidget<Image3UShort, Image3UShort> > WatershedBoundariesMergeTreeFilterWidgetImage3UShort;

} // namespace XPIWIT
