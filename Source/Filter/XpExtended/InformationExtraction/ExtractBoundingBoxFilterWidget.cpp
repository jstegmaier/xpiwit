/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "ExtractBoundingBoxFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkIntensityWindowingImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkImageRegionConstIterator.h"


// XPIWIT namespace
namespace XPIWIT
{

// the default constructor
template< class TInputImage>
ExtractBoundingBoxFilterWidget<TInputImage>::ExtractBoundingBoxFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = ExtractBoundingBoxFilterWidget<TInputImage>::GetName();
    this->mDescription = "Extract a bounding box of all values greater than zero";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(1);
	this->mObjectType->AppendMetaOutputType("BoundingBox");

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
ExtractBoundingBoxFilterWidget<TInputImage>::~ExtractBoundingBoxFilterWidget()
{
}


// the update function
template< class TInputImage >
void ExtractBoundingBoxFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    MetaDataFilter* metaOutput = this->mMetaOutputs.at(0);
    metaOutput->mIsMultiDimensional = false;

    if( TInputImage::ImageDimension == 2 ){
        metaOutput->mTitle << "minx"  << "miny"  << "maxx"  << "maxy";
        metaOutput->mType  << "float" << "float" << "float" << "float";
    }

    if( TInputImage::ImageDimension == 3 ){
        metaOutput->mTitle << "minx"  << "miny"  << "minz"  << "maxx"  << "maxy"  << "maxz";
        metaOutput->mType  << "float" << "float" << "float" << "float" << "float" << "float";
    }
    metaOutput->mPostfix = "BoundingBox";

    // get parameters
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

    // extract nucleus information
    unsigned int currentKeyPointID = 0;
    unsigned int centroidOffset = 1;

    typename itk::ImageRegionConstIterator<TInputImage> iterator( inputImage, inputImage->GetLargestPossibleRegion() );
    iterator.GoToBegin();

    int min[TInputImage::ImageDimension];
    int max[TInputImage::ImageDimension];

    for( int i = 0; i < TInputImage::ImageDimension; i++){
        min[i] = inputImage->GetLargestPossibleRegion().GetSize( i );
        max[i] = 0;
    }

    while (!iterator.IsAtEnd())
    {
        if (iterator.Value() > 0)
        {
            typename TInputImage::IndexType index = iterator.GetIndex();

            for( int i = 0; i < TInputImage::ImageDimension; i++){
                if (index[i] < min[i])
                    min[i] = index[i];
                if (index[i] > max[i])
                    max[i] = index[i];
            }

        }

        ++iterator;
    }

    QList<float> dataLine;

    // write to meta object
    for( int i = 0; i < TInputImage::ImageDimension; i++){
        dataLine << min[i];
    }
    for( int i = 0; i < TInputImage::ImageDimension; i++){
        dataLine << max[i];
    }
    metaOutput->mData.append( dataLine );

    typename itk::RescaleIntensityImageFilter<TInputImage, TInputImage>::Pointer filter = itk::RescaleIntensityImageFilter<TInputImage, TInputImage>::New();
    filter->SetInput( inputImage );
    filter->SetNumberOfWorkUnits( maxThreads );
    filter->SetReleaseDataFlag( releaseDataFlag );

    // update the filter
    itkTryCatch( filter->Update(), "Error: Updating ExtractBoundingBoxFilterWidget failed!" );

	ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( filter->GetOutput() );
    mOutputImages.append( outputImage );

    // update the parent
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< ExtractBoundingBoxFilterWidget<Image2Float> > ExtractBoundingBoxFilterWidgetImage2Float;
static ProcessObjectProxy< ExtractBoundingBoxFilterWidget<Image3Float> > ExtractBoundingBoxFilterWidgetImage3Float;
static ProcessObjectProxy< ExtractBoundingBoxFilterWidget<Image2UShort> > ExtractBoundingBoxFilterWidgetImage2UShort;
static ProcessObjectProxy< ExtractBoundingBoxFilterWidget<Image3UShort> > ExtractBoundingBoxFilterWidgetImage3UShort;

} // namespace XPIWIT
