/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "ExtractWatershedBoundariesFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"


// qt header
#include <QFileInfo>


namespace XPIWIT
{

// the default constructor
template< class TInputImage0, class TInputImage1 >
ExtractWatershedBoundariesFilterWidget<TInputImage0, TInputImage1>::ExtractWatershedBoundariesFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = ExtractWatershedBoundariesFilterWidget<TInputImage0, TInputImage1>::GetName();
    this->mDescription = "Extracts all local extrema in an image.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(2);

    this->mObjectType->SetNumberImageInputs(2);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(2);

    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);

    this->mObjectType->SetNumberMetaInputs(1);
	this->mObjectType->AppendMetaInputType("RegionProps");
	this->mObjectType->SetNumberMetaOutputs(1);
	this->mObjectType->AppendMetaOutputType("WatershedBoundaries");

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting("MinimumVolume", "4000", ProcessObjectSetting::SETTINGVALUETYPE_INT, "All regions with smaller volume are merged.");
	processObjectSettings->AddSetting("MaximumVolume", "8000", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Stopping criterion for region merges, i.e., larger cells are not produced by the fusion.");
	processObjectSettings->AddSetting("GenerateEdgeMap", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, the sort feature will be used as edge intensity. No segmentation merging is performed in this case.");
	processObjectSettings->AddSetting("UseBoundaryCriterion", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, the separating boundary rather belongs to background than foreground.");
	processObjectSettings->AddSetting("UseMinimumVolumeCriterion", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, objects smaller than the minimum volume will be fused (unless the maximum volume constraint is violated).");
	processObjectSettings->AddSetting("UseMeanRatioCriterion", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, objects are fused if the ratio of boundary intensity vs. interior intensity is below 1.");
	processObjectSettings->AddSetting("UseSphericityCriterion", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, objects are fused, if the sphericity after the merge is larger than before the merge (only useful for spherical objects).");
	processObjectSettings->AddSetting("DisableMVCOnBorder", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, objects are touching the border are not merged using the minimum volume criterion (to prevent false merges due to size).");


    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage0, class TInputImage1>
ExtractWatershedBoundariesFilterWidget<TInputImage0, TInputImage1>::~ExtractWatershedBoundariesFilterWidget()
{
}


// the update function
template< class TInputImage0, class TInputImage1 >
void ExtractWatershedBoundariesFilterWidget<TInputImage0, TInputImage1>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	this->mMetaOutputs.at(0)->mIsMultiDimensional = true;

    // get filter parameters
    bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;
	bool generateEdgeMap = processObjectSettings->GetSettingValue("GenerateEdgeMap").toInt() > 0;
	const int minimumVolume = processObjectSettings->GetSettingValue("MinimumVolume").toInt();
	const int maximumVolume = processObjectSettings->GetSettingValue("MaximumVolume").toInt();
	
	bool useBoundaryCriterion = processObjectSettings->GetSettingValue("UseBoundaryCriterion").toInt() > 0;
	bool useMinimumVolumeCriterion = processObjectSettings->GetSettingValue("UseMinimumVolumeCriterion").toInt() > 0;
	bool useMeanRatioCriterion = processObjectSettings->GetSettingValue("UseMeanRatioCriterion").toInt() > 0;
	bool useSphericityCriterion = processObjectSettings->GetSettingValue("UseSphericityCriterion").toInt() > 0;
	bool disableMVCOnBorder = processObjectSettings->GetSettingValue("DisableMVCOnBorder").toInt() > 0;
	
	typename TInputImage0::Pointer inputImage1 = mInputImages.at(0)->template GetImage<TInputImage0>();
	typename TInputImage1::Pointer inputImage2 = mInputImages.at(1)->template GetImage<TInputImage1>();


	MetaDataFilter* regionProps = this->mMetaInputs.at(0);
	//QList<float> line = m_InputMetaFilter->mData.at(i);

	std::cout << "I found " << regionProps->mData.size() << " regions " << std::endl;

    // initialize the extraction filter
    typename itk::ExtractWatershedBoundariesImageFilter<TInputImage0, TInputImage0>::Pointer extractWatershedBoundariesFilter = itk::ExtractWatershedBoundariesImageFilter<TInputImage0, TInputImage0>::New();
	extractWatershedBoundariesFilter->SetInput( inputImage1 );
	extractWatershedBoundariesFilter->SetInputMetaFilter( regionProps );
	extractWatershedBoundariesFilter->SetIntensityImage( inputImage2 );
	extractWatershedBoundariesFilter->SetVolumeConstraints( minimumVolume, maximumVolume );
	extractWatershedBoundariesFilter->SetGenerateEdgeMap(generateEdgeMap);
	extractWatershedBoundariesFilter->SetUseBoundaryCriterion(useBoundaryCriterion);
	extractWatershedBoundariesFilter->SetUseMinimumVolumeCriterion(useMinimumVolumeCriterion);
	extractWatershedBoundariesFilter->SetUseMeanRatioCriterion(useMeanRatioCriterion);
	extractWatershedBoundariesFilter->SetUseSphericityCriterion(useSphericityCriterion);
	extractWatershedBoundariesFilter->SetDisableMinimumVolumeCriterionOnBorder(disableMVCOnBorder);

    // extract seed points
    extractWatershedBoundariesFilter->SetReleaseDataFlag( releaseDataFlag );
    itkTryCatch( extractWatershedBoundariesFilter->Update(), "ExtractWatershedBoundariesFilterWidget --> Update extraction filter!");
	
    ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage0>( extractWatershedBoundariesFilter->GetOutput() );
    mOutputImages.append( outputImage );

    // update the process object widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< ExtractWatershedBoundariesFilterWidget<Image2Float, Image2Float> > ExtractWatershedBoundariesFilterWidgetImage2Float;
static ProcessObjectProxy< ExtractWatershedBoundariesFilterWidget<Image3Float, Image3Float> > ExtractWatershedBoundariesFilterWidgetImage3Float;
static ProcessObjectProxy< ExtractWatershedBoundariesFilterWidget<Image2UShort, Image2UShort> > ExtractWatershedBoundariesFilterWidgetImage2UShort;
static ProcessObjectProxy< ExtractWatershedBoundariesFilterWidget<Image3UShort, Image3UShort> > ExtractWatershedBoundariesFilterWidgetImage3UShort;

} // namespace XPIWIT
