/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "LoGScaleSpaceMaximumProjectionFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../Source/Filter/XpExtended/WorkflowFilter/ImageWriterWidget.h"
#include "../../../Core/Utilities/Logger.h"
#include "../../ITKCustom/itkExtractRegionPropsImageFilter.h"
#include "../../ITKCustom/itkExtractKeyPointsImageFilter.h"
#include "../../ITKCustom/itkScaleNormalizedLaplacianRecursiveGaussianImageFilter.h"
#include "../../ThirdParty/itkMaximumWithInfoImageFilter.h"
#include "../../ThirdParty/itkLocalMaximumImageFilter.h"


// itk header
#include "itkMaximumImageFilter.h"
#include "itkMinimumImageFilter.h"
#include "itkMesh.h"
#include "itkInvertIntensityImageFilter.h"
#include "itkNeighborhoodIterator.h"
#include "itkAddImageFilter.h"
#include "itkStreamingImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkImageFileWriter.h"

// qt header
#include <QtCore/QDir>


namespace XPIWIT
{

// the default constructor
template< class TImageType1, class TImageType2 >
LoGScaleSpaceMaximumProjectionFilterWidget<TImageType1, TImageType2>::LoGScaleSpaceMaximumProjectionFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = LoGScaleSpaceMaximumProjectionFilterWidget<TImageType1, TImageType2>::GetName();
    this->mDescription = "Creates the maximum projection of multiple laplacian of gaussian filter results.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(2);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(2);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->AppendImageOutputType(2);

    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Step", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Step width from min to max sigma." );
    processObjectSettings->AddSetting( "MinSigma", "8.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Minimum sigma value." );
    processObjectSettings->AddSetting( "MaxSigma", "12.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Maximum sigma value." );
    processObjectSettings->AddSetting( "NormalizeAcrossScales", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Normalize scales." );
	processObjectSettings->AddSetting( "NormalizationExponent", "2.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Normalization exponent used for the scale space normalization." );
	processObjectSettings->AddSetting( "WriteIntermediateResults", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, the intermediate LoG filtered images are also saved (32bit)." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TImageType1, class TImageType2 >
LoGScaleSpaceMaximumProjectionFilterWidget<TImageType1, TImageType2>::~LoGScaleSpaceMaximumProjectionFilterWidget()
{
}


// the update function
template< class TImageType1, class TImageType2 >
void LoGScaleSpaceMaximumProjectionFilterWidget<TImageType1, TImageType2>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // define internal integer data type
    typedef itk::Image<PixelUShort, TImageType1::ImageDimension> InternalImageTypeUShort;
    typedef itk::Image<PixelShort, TImageType1::ImageDimension> InternalImageTypeShort;
    typedef itk::Image<PixelUChar, TImageType1::ImageDimension> InternalImageTypeUChar;

    // typedefs for the filters
    typedef itk::ScaleNormalizedLaplacianRecursiveGaussianImageFilter<TImageType1, TImageType1> LoGFilter;
    typedef itk::RescaleIntensityImageFilter<TImageType1> RescaleIntensityFilter;
    typedef itk::IntensityWindowingImageFilter<InternalImageTypeUShort, TImageType1> IntensityExternalToInternalType;
    typedef itk::IntensityWindowingImageFilter<TImageType1, InternalImageTypeUShort> IntensityInternalToExternalType;

    // get the dialog settings for the scale space creation
    float minSigma	= processObjectSettings->GetSettingValue( "MinSigma" ).toDouble();
    float maxSigma	= processObjectSettings->GetSettingValue( "MaxSigma" ).toDouble();
    float step		= processObjectSettings->GetSettingValue( "Step" ).toDouble();
    bool normalizeAcrossScales = processObjectSettings->GetSettingValue( "NormalizeAcrossScales" ).toInt() > 0;
	float normalizationExponent = processObjectSettings->GetSettingValue("NormalizationExponent").toDouble();
    bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;
    unsigned int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    QString outPath = processObjectSettings->GetSettingValue( "OutPath" );
    bool writeResult = processObjectSettings->GetSettingValue( "WriteResult" ).toInt() > 0;
	bool writeIntermediateResults = processObjectSettings->GetSettingValue("WriteIntermediateResults").toInt() > 0;

	typename TImageType1::Pointer inputImage = mInputImages.at(0)->template GetImage<TImageType1>();
	typename TImageType1::SpacingType spacing = inputImage->GetSpacing();
//	bool oldReleaseDataFlag = inputImage->GetReleaseDataFlag();
//	inputImage->SetReleaseDataFlag( false );

    // initialize the maximum scale matrix
    typename TImageType2::Pointer scaleWithMaximumValue = TImageType2::New();
    scaleWithMaximumValue->SetRegions( inputImage->GetRequestedRegion() );
    scaleWithMaximumValue->SetSpacing( inputImage->GetSpacing() );
    scaleWithMaximumValue->Allocate();
    scaleWithMaximumValue->FillBuffer( minSigma );
    scaleWithMaximumValue->SetReleaseDataFlag( true );

    // invert the intensity of the scale space projection image
    typename itk::InvertIntensityImageFilter<TImageType1>::Pointer invertIntensity = itk::InvertIntensityImageFilter<TImageType1>::New();
    invertIntensity->SetInput( inputImage );
    invertIntensity->SetReleaseDataFlag( false );
    invertIntensity->SetNumberOfWorkUnits( maxThreads );
    invertIntensity->SetMaximum( 1.0 );
	itkTryCatch( invertIntensity->Update(), "LoGScaleSlaceMaximumProjectionImageFilterWidget --> Invert intensity filter." );
	
    // initialize the log scale space maximum projection image
    typename TImageType1::Pointer mLoGScaleSpaceMaximumProjection = TImageType1::New();
    mLoGScaleSpaceMaximumProjection->SetRegions( inputImage->GetLargestPossibleRegion() );
    mLoGScaleSpaceMaximumProjection->SetSpacing( inputImage->GetSpacing() );
    mLoGScaleSpaceMaximumProjection->Allocate();
    mLoGScaleSpaceMaximumProjection->FillBuffer( 0 );
    mLoGScaleSpaceMaximumProjection->SetReleaseDataFlag( true );

    // create scale space representations for the selected standard deviations and perform maximum projection
	float epsilon = 0.0001; // increase maximum scale by a small amount to avoid cutoff errors
	int currentScale = 1;
	for (float i = minSigma; i <= (maxSigma+epsilon); i += step)
	{
		// set release flag of the cached input if in the last loop
		if (i >= maxSigma)
			invertIntensity->SetReleaseDataFlag(true);

		// filter the input image with the given standard deviation
		typedef itk::ScaleNormalizedLaplacianRecursiveGaussianImageFilter<TImageType1, TImageType1> LoGFilterType;
		typename LoGFilterType::Pointer filter = LoGFilterType::New();
		filter->SetReleaseDataFlag(!writeResult);
		if (TImageType1::ImageDimension == 3)
			filter->SetNormalizeAcrossScale(normalizeAcrossScales, 2.5);
		else
			filter->SetNormalizeAcrossScale(normalizeAcrossScales, 2.0);
		filter->SetNumberOfWorkUnits(maxThreads);
		filter->SetSigma(i);
		filter->SetInput(invertIntensity->GetOutput());

		// update the LoG Filtering for the current scale
		itkTryCatch(filter->Update(), "LoGScaleSlaceMaximumProjectionImageFilterWidget --> updating log filter");

		// write the intermediate image
		if (writeIntermediateResults == true)
		{
			ImageWrapper *tmpImage = new ImageWrapper();
			tmpImage->SetImage<TImageType1>(filter->GetOutput());

			bool tempReleaseDataFlag = tmpImage->GetReleaseData();
			tmpImage->SetReleaseData(false);

			ImageWriterWidget imageWriter;
			imageWriter.SetInputImage(tmpImage, 0);
			imageWriter.SetSetting("Compression", "1");
			imageWriter.SetSetting("Precision", "32");

			QString path = ProcessObjectBase::CreateOutputPath(mCMDPipelineArguments, mAbstractFilter);
            path += "_Scale=" + QString().sprintf("%02d", currentScale) + ".tif";

			imageWriter.SetReaderPath(path);
			imageWriter.Update();

			tmpImage->SetReleaseData(tempReleaseDataFlag);
			++currentScale;
		}


        // initialize the scale space maximum projection and the corresponding scale with maximum image to the first filtered version
        typedef itk::MaximumWithInfoImageFilter< TImageType1, TImageType1, TImageType2, TImageType1, TImageType2 > MaximumWithInfoImageFilterType;
        typename MaximumWithInfoImageFilterType::Pointer maximumWithInfoImageFilter = MaximumWithInfoImageFilterType::New();

        // setup the minimum with info filter with additionally to the binary minimum operation also stores the pixel that contained the maximum
        maximumWithInfoImageFilter->SetInput1( mLoGScaleSpaceMaximumProjection );
        maximumWithInfoImageFilter->SetInput2( filter->GetOutput() );
        maximumWithInfoImageFilter->SetInput3( scaleWithMaximumValue );
        maximumWithInfoImageFilter->SetNumberOfWorkUnits( maxThreads );
        maximumWithInfoImageFilter->SetInfoValue( i );

        // try updating the filter
        itkTryCatch( maximumWithInfoImageFilter->Update(), "LoGScaleSlaceMaximumProjectionImageFilterWidget --> performing maximum with info image filter" );

        // set the filter result
        mLoGScaleSpaceMaximumProjection = maximumWithInfoImageFilter->GetOutput1();
        scaleWithMaximumValue = maximumWithInfoImageFilter->GetOutput2();
    }

    // convert the maximum projection to the internal image format
    mLoGScaleSpaceMaximumProjection->SetReleaseDataFlag( true );

    // update the intensity conversion
    //itkTryCatch( rescaleIntensity2->Update(), "LoGScaleSpaceMaximumProjection --> Final intensity conversion." );

	ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TImageType1>( mLoGScaleSpaceMaximumProjection );
    mOutputImages.append( outputImage );

	ImageWrapper *outputImage2 = new ImageWrapper();
    outputImage2->SetImage<TImageType2>( scaleWithMaximumValue );
	outputImage2->SetRescaleFlag( false );
    mOutputImages.append( outputImage2 );

    // update the process object
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< LoGScaleSpaceMaximumProjectionFilterWidget<Image2Float, Image2Float> > LoGScaleSpaceMaximumProjectionFilterWidgetImage2Float;
static ProcessObjectProxy< LoGScaleSpaceMaximumProjectionFilterWidget<Image3Float, Image3Float> > LoGScaleSpaceMaximumProjectionFilterWidgetImage3Float;
static ProcessObjectProxy< LoGScaleSpaceMaximumProjectionFilterWidget<Image2UShort, Image2UChar> > LoGScaleSpaceMaximumProjectionFilterWidgetImage2UShort;
static ProcessObjectProxy< LoGScaleSpaceMaximumProjectionFilterWidget<Image3UShort, Image3UChar> > LoGScaleSpaceMaximumProjectionFilterWidgetImage3UShort;

} // namespace XPIWIT

/*

        // write the intermediate scale space to disk if write flag is turned on
        if (false )  // (writeResult == true)
        {
            // set the filename
			QString outFileName = this->mCMDPipelineArguments->mStdOutputPath; 

			if( this->mCMDPipelineArguments->mUseSubFolder ){
				QString folder;
				folder = this->mAbstractFilter->GetId() + "_";
				folder += this->mAbstractFilter->GetName();

				QDir dir( outFileName );
				if( !dir.exists( folder ) )
					dir.mkdir( folder );

				outFileName += folder + "/";
			}


            outFileName += QString("LoGImage_Sigma") + QString::number(i) + QString(".tif");

            // rescale intensity range to the uint16 range (TODO: do this depending on the input image)
            typedef itk::RescaleIntensityImageFilter<InternalImageTypeShort, InternalImageTypeUShort> RescaleIntensityFilterType;
            typename RescaleIntensityFilterType::Pointer rescaleFilter = RescaleIntensityFilterType::New();
            filter->SetReleaseDataFlag( true );
            rescaleFilter->SetInput( filter->GetOutput() );
            rescaleFilter->SetNumberOfWorkUnits( maxThreads );
            rescaleFilter->SetOutputMinimum(0);
            rescaleFilter->SetOutputMaximum(65535);
            rescaleFilter->SetReleaseDataFlag( true );

            // setup writer properties
            typedef itk::ImageFileWriter<InternalImageTypeUShort> WriterType;
            typename WriterType::Pointer writer = WriterType::New();
            writer->SetInput( rescaleFilter->GetOutput() );
            writer->SetFileName( outFileName.toLatin1().data() );
            writer->SetNumberOfWorkUnits( maxThreads );
            writer->SetReleaseDataFlag( true );

            // try writing the file to the given destination
            itkTryCatch( writer->Update(), "Error: LoGScaleSpaceMaximumProjection writing single scale images." );
        }
*/
