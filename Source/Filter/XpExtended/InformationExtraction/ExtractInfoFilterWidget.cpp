/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "ExtractInfoFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkImageRegionConstIterator.h"
#include "itkIntensityWindowingImageFilter.h"

// system header
#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>

using namespace itk;


// ITKIT namespace
namespace XPIWIT
{

// the default constructor
template <class TInputImage>
ExtractInfoFilterWidget<TInputImage>::ExtractInfoFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = ExtractInfoFilterWidget<TInputImage>::GetName();
    this->mDescription = "Extract statistical values of the input image.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
    this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(0);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(1);
	this->mObjectType->AppendMetaOutputType("ImageInfo");

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "WriteHeader", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Write a header into csv output." );
    processObjectSettings->AddSetting( "Quantiles", "0.05", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Defines the upper and lower quantile." );

    // initialize the widget
    ProcessObjectBase::Init();
}

// the destructor
template <class TInputImage>
ExtractInfoFilterWidget<TInputImage>::~ExtractInfoFilterWidget()
{
}

// the update function
template <class TInputImage>
void ExtractInfoFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get settings
    const float quantile = processObjectSettings->GetSettingValue("Quantiles").toFloat();
    const int maxThreads = processObjectSettings->GetSettingValue("MaxThreads").toInt();
    bool writeHeader = processObjectSettings->GetSettingValue("WriteHeader").toInt() > 0;
    bool releaseDataFlag = processObjectSettings->GetSettingValue("ReleaseDataFlag").toInt() > 0;

    // initialize the needed variables
    float min = 1e19;
    float max = 0.0;
    float mean = 0.0;
    float median = -1;
    float stdDev = 0.0;
    float variance = 0.0;
    float sumOfIntensities = 0.0;
    float sumOfSquaredIntensities = 0.0;
    float uquantile = 0.0;
    float lquantile = 0.0;

	typename TInputImage::Pointer inputImage = mInputImages.at( 0 )->template GetImage<TInputImage>();

    // calculate histogram and scale to the selected quantiles
    ImageRegionConstIterator<TInputImage> inputIterator( inputImage, inputImage->GetLargestPossibleRegion() );
    inputIterator.GoToBegin();
    unsigned int overallNumberOfPixels = inputImage->GetLargestPossibleRegion().GetSize()[0]*
                                         inputImage->GetLargestPossibleRegion().GetSize()[1]*
                                         inputImage->GetLargestPossibleRegion().GetSize()[2];
    unsigned int numPixels = 0;
    unsigned int histogramSize = 65535;		// short?
    unsigned int* histogram = new unsigned int[histogramSize];
    for (int i=0; i<histogramSize; ++i)
        histogram[i] = 0;

    // fill the histogram
    while (inputIterator.IsAtEnd() == false)
    {
        //outputIterator.Set( inputIterator.Get() );
        histogram[std::min<int>(0, std::max<int>(65534, (int)(0.5+inputIterator.Get() * histogramSize)))]++;
        numPixels++;

        if (min > inputIterator.Get())
            min = inputIterator.Get();

        if (max < inputIterator.Get())
            max = inputIterator.Get();

        sumOfIntensities += inputIterator.Get();
        sumOfSquaredIntensities += inputIterator.Get()*inputIterator.Get();

        ++inputIterator;
    }

    // divide mean by the number of pixels
    mean = sumOfIntensities / numPixels;
    variance = (1.0/(float)(numPixels-1)) * (sumOfSquaredIntensities - (1.0/(float)numPixels)*(sumOfIntensities*sumOfIntensities));
    stdDev = sqrt( variance );

    // calculate the quantiles
    float numLower = 0.0;
    float numUpper = 0.0;
    float binWidth = 1.0 / (float)histogramSize;
    for (int i=0; i<histogramSize; ++i)
    {
        if (lquantile <= 0.0 && 100.0*(numLower/(float)overallNumberOfPixels) >= quantile)
        {
            lquantile = (float)i / (float)histogramSize;
            break;
        }

        numLower += histogram[i];
    }

    for (int i=0; i<histogramSize; ++i)
    {
        if (100.0*(numUpper/(float)overallNumberOfPixels) >= quantile)
        {
            uquantile = (float)(histogramSize-i-1) / (float)histogramSize;
            break;
        }

        numUpper += histogram[histogramSize-i-1];
    }

    numLower = 0.0;
    for (int i=0; i<histogramSize; ++i)
    {
        if (median <= 0.0 && 100.0*(numLower/(float)overallNumberOfPixels) >= 0.5)
        {
            median = (float)i / (float)histogramSize;
            break;
        }

        numLower += histogram[i];
    }

    // delete the histogram
    delete[] histogram;

    // create meta data object
    MetaDataFilter* metaOutput = this->mMetaOutputs.at(0);
    metaOutput->mIsMultiDimensional = false;                // one line per image

    metaOutput->mTitle << "mean"  << "stdDev" << "variance" << "min"   << "max"   << "median" << "lquantile" << "uquantile";    //     << "size"  << "spacing";
    metaOutput->mType  << "float" << "float"  << "float"    << "float" << "float" << "float"  << "float"     << "float";        //     << "float" << "float";
    QList<float> dataLine;
    dataLine << mean;
    dataLine << stdDev;
    dataLine << variance;
    dataLine << min;
    dataLine << max;
    dataLine << median;
    dataLine << quantile;
    dataLine << (1.0-quantile);
    metaOutput->mData.append( dataLine );
    metaOutput->mPostfix = "ImageInfo";


    // update the process object widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< ExtractInfoFilterWidget<Image2Float> > ExtractInfoFilterWidgetImage2Float;
static ProcessObjectProxy< ExtractInfoFilterWidget<Image3Float> > ExtractInfoFilterWidgetImage3Float;
static ProcessObjectProxy< ExtractInfoFilterWidget<Image2UShort> > ExtractInfoFilterWidgetImage2UShort;
static ProcessObjectProxy< ExtractInfoFilterWidget<Image3UShort> > ExtractInfoFilterWidgetImage3UShort;

}// XPIWIT namespace
