/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "FeatureBasedRegionRejectionFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkIntensityWindowingImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkLabelGeometryImageFilter.h"
#include "itkLabelStatisticsImageFilter.h"
#include "itkBinaryImageToLabelMapFilter.h"
#include "itkLabelMapToLabelImageFilter.h"
#include "itkImageRegionConstIterator.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"

namespace XPIWIT
{

// the default constructor
template <class TInputImage>
FeatureBasedRegionRejectionFilterWidget<TInputImage>::FeatureBasedRegionRejectionFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = FeatureBasedRegionRejectionFilterWidget<TInputImage>::GetName();
    this->mDescription = "Extracts the region properties of the image";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
    this->mObjectType->SetNumberMetaInputs(1);
    this->mObjectType->AppendMetaInputType("RegionProps");
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "LabelOutput", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Switch between feature map or label output");
    processObjectSettings->AddSetting( "Relabel", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Relabels the filtered objects such that all labels between 1 and N are present (only applied if LabelOutput is enabled)." );
    processObjectSettings->AddSetting( "FeatureIndex", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Index of the feature, i.e., the column id in the region props csv file starting with 0.");
    processObjectSettings->AddSetting( "MinimumValue", "0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "All regions with smaller feature valeus are suppressed. E.g. helpful for noise suppression in the EDM-based seed detection." );
	processObjectSettings->AddSetting( "MaximumValue", "-1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "All regions with larger feature valeus are suppressed. E.g. helpful for background suppression." );
    processObjectSettings->AddSetting( "LabelOffset", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The offset added to the image labels, if csv labels are shifted (e.g. C -> Matlab convention).");
    processObjectSettings->AddSetting( "IntensityScale", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Intensity scale: If set, the input intensities are multiplied by this factor.");

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template <class TInputImage>
FeatureBasedRegionRejectionFilterWidget<TInputImage>::~FeatureBasedRegionRejectionFilterWidget()
{
}

// the update function
template <class TInputImage>
void FeatureBasedRegionRejectionFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // prepare inputs
    typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
    MetaDataFilter* regionProps = this->mMetaInputs.at(0);

    // initialize output image
    typename TInputImage::Pointer outputImage;
    outputImage = TInputImage::New();
    outputImage->SetRegions(inputImage->GetLargestPossibleRegion());
    outputImage->Allocate();
    outputImage->SetSpacing(inputImage->GetSpacing());
    outputImage->FillBuffer(0);

    // get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;
	const bool relabel = processObjectSettings->GetSettingValue( "Relabel" ).toInt() > 0;
    const bool labelOutput = processObjectSettings->GetSettingValue("LabelOutput").toInt() > 0;
    const int featureIndex = processObjectSettings->GetSettingValue( "FeatureIndex" ).toInt();
	const float minimumValue = processObjectSettings->GetSettingValue( "MinimumValue" ).toFloat();
	const float maximumValue = processObjectSettings->GetSettingValue( "MaximumValue" ).toFloat();
    const int labelOffset = processObjectSettings->GetSettingValue("LabelOffset").toInt();
    const float intensityScale = processObjectSettings->GetSettingValue("IntensityScale").toFloat();

    // create the image iterators
    itk::ImageRegionIterator<TInputImage> inputIterator(inputImage, inputImage->GetLargestPossibleRegion());
    itk::ImageRegionIterator<TInputImage> outputIterator(outputImage, inputImage->GetLargestPossibleRegion());
    inputIterator.GoToBegin();
    outputIterator.GoToBegin();

    // initalize auxiliary variables
    unsigned int currentLabel = 1;
    unsigned int maxLabel = 0;
    unsigned int numLabels = 0;
    unsigned int regionLabel;
    unsigned int numObjects = regionProps->mData.length();
    float featureValue = 0.0;

    // identify the maximum label
    for (unsigned int i = 0; i < numObjects; ++i)
    {
        // get current region label and feature value
        regionLabel = regionProps->mData[i].at(0);

        // identify largest label
        if (regionLabel > maxLabel)
            maxLabel = regionLabel;
    }

    std::cout << "Identified maximum label: " << maxLabel << std::endl;
    numLabels = maxLabel + 1;

    // initialize the new labels and existing labels variables
    unsigned int* newLabels = new unsigned int[numLabels];
    unsigned int* existingLabels = new unsigned int[numLabels];
    unsigned int labelValue;
    for (unsigned int i=0; i<numLabels; ++i)
    {
        newLabels[i] = 0;
        existingLabels[i] = 0;
    }

    // find valid labels present in the image
    while (inputIterator.IsAtEnd() == false)
    {
        // get the current label
        labelValue = (unsigned int)(intensityScale * inputIterator.Value());
        existingLabels[labelValue] = 1;
        ++inputIterator;
    }

    // initialize the feature values array
    float* featureValues = new float[numLabels];
    for (unsigned int i = 0; i < numLabels; ++i)
        featureValues[i] = 0;

    // fill feature values array
    for (unsigned int i = 0; i < numObjects; ++i)
    {
        regionLabel = (unsigned int)(regionProps->mData[i].at(0));
        featureValue = regionProps->mData[i].at(featureIndex);
        featureValues[regionLabel] = featureValue;

        // print debug output
        std::cout << "Label " << regionLabel << " has feature value " << featureValue << ", label exists " << existingLabels[regionLabel];

        if (existingLabels[regionLabel] > 0 && featureValue >= minimumValue && (maximumValue < 0 || featureValue <= maximumValue))
        {
            std::cout << " -> including" << ", old label " << regionLabel << ", new label would be " << currentLabel << std::endl;

            newLabels[regionLabel] = currentLabel;
            ++currentLabel;
        }
        else
        {
            std::cout << " -> skipping" << std::endl;
        }
    }

    std::cout << "Maybe here? Num Objects is " << numObjects << std::endl;

    // fill the output pixels while skipping regions that are too small/large
    inputIterator.GoToBegin();
    while (inputIterator.IsAtEnd() == false)
    {
        // get the current label
        unsigned int labelValue = (unsigned int)(intensityScale * inputIterator.Value());
 
        // check if volume is larger than the specified minimum volume
        if (labelValue > 0 && labelValue <= maxLabel)
        {
            // get current feature value
            featureValue = featureValues[labelValue];

            // only include segment if it is in the range between [minimumValue, maximumValue]
            if (featureValue >= minimumValue && (maximumValue < 0 || featureValue <= maximumValue))
            {
                if (relabel == true)
                    labelValue = newLabels[labelValue];
                    
                if (labelOutput == true)
                    outputIterator.Set(labelValue);
                else
                    outputIterator.Set(featureValue);
            }
        }

        // increment iterators
        ++inputIterator;
        ++outputIterator;
    }

    // wrap up
    delete[] newLabels;
    delete[] featureValues;
    delete[] existingLabels;

    // set the output image
    ImageWrapper* outputWrapper = new ImageWrapper();
    outputWrapper->SetImage<TInputImage>(outputImage);
    outputWrapper->SetRescaleFlag(false);
    mOutputImages.append(outputWrapper);

	// update the process object widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< FeatureBasedRegionRejectionFilterWidget<Image2Float> > FeatureBasedRegionRejectionFilterWidgetImage2Float;
static ProcessObjectProxy< FeatureBasedRegionRejectionFilterWidget<Image3Float> > FeatureBasedRegionRejectionFilterWidgetImage3Float;
static ProcessObjectProxy< FeatureBasedRegionRejectionFilterWidget<Image2UShort> > FeatureBasedRegionRejectionFilterWidgetImage2UShort;
static ProcessObjectProxy< FeatureBasedRegionRejectionFilterWidget<Image3UShort> > FeatureBasedRegionRejectionFilterWidgetImage3UShort;

} // namespace XPIWIT