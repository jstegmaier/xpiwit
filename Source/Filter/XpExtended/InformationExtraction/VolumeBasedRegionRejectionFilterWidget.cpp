/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "VolumeBasedRegionRejectionFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkIntensityWindowingImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkLabelGeometryImageFilter.h"
#include "itkLabelStatisticsImageFilter.h"
#include "itkBinaryImageToLabelMapFilter.h"
#include "itkLabelMapToLabelImageFilter.h"
#include "itkImageRegionConstIterator.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"

namespace XPIWIT
{

// the default constructor
template <class TInputImage>
VolumeBasedRegionRejectionFilterWidget<TInputImage>::VolumeBasedRegionRejectionFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = VolumeBasedRegionRejectionFilterWidget<TInputImage>::GetName();
    this->mDescription = "Extracts the region properties of the image";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "BinaryInput", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If input is binary, a label image is generated before extracting the region properties." );
    processObjectSettings->AddSetting( "LabelOutput", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Switch between geometry and statistics mode");
    processObjectSettings->AddSetting( "FullyConnected", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Determines the connectivity model. Use FullyConnected = 1 for 8/26-neighborhood or to 0 for 4/6 neighborhood." );
	processObjectSettings->AddSetting( "MinimumVolume", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "All regions with smaller volume are suppressed. E.g. helpful for noise suppression in the EDM-based seed detection." );
	processObjectSettings->AddSetting( "MaximumVolume", "-1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "All regions with larger volume are suppressed. E.g. helpful for background suppression." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template <class TInputImage>
VolumeBasedRegionRejectionFilterWidget<TInputImage>::~VolumeBasedRegionRejectionFilterWidget()
{
}

// the update function
template <class TInputImage>
void VolumeBasedRegionRejectionFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;
	const bool binaryInput = processObjectSettings->GetSettingValue( "BinaryInput" ).toInt() > 0;
    const bool labelOutput = processObjectSettings->GetSettingValue("LabelOutput").toInt() > 0;
    const bool fullyConnected = processObjectSettings->GetSettingValue( "FullyConnected" ).toInt() > 0;
	const int minimumVolume = processObjectSettings->GetSettingValue( "MinimumVolume" ).toInt();
	const int maximumVolume = processObjectSettings->GetSettingValue( "MaximumVolume" ).toInt();

	// define region props type and get the input image
	typedef typename itk::Image< PixelUShort, TInputImage::ImageDimension > RegionPropsImageType;
	typename RegionPropsImageType::Pointer inputImage = mInputImages.at(0)->template GetImage< RegionPropsImageType >();

    inputImage->SetReleaseDataFlag(false);

    typename TInputImage::Pointer outputImage;
    outputImage = TInputImage::New();
    outputImage->SetRegions(inputImage->GetLargestPossibleRegion());
    outputImage->Allocate();
    outputImage->SetSpacing(inputImage->GetSpacing());
    outputImage->FillBuffer(0);

	// get image info
	typename RegionPropsImageType::PointType origin = inputImage->GetOrigin();
    typename RegionPropsImageType::SizeType inputSize = inputImage->GetLargestPossibleRegion().GetSize();
    typename RegionPropsImageType::SizeType outputSize = inputImage->GetLargestPossibleRegion().GetSize();
    typename RegionPropsImageType::SpacingType inputSpacing = inputImage->GetSpacing();
    typename RegionPropsImageType::SpacingType outputSpacing = inputImage->GetSpacing();

    // initialize the geometry extraction filter
	typedef typename itk::LabelGeometryImageFilter<RegionPropsImageType, TInputImage> LabelGeometryImageFilterType;
    typename LabelGeometryImageFilterType::Pointer labelGeometryFilter =  LabelGeometryImageFilterType::New();
    labelGeometryFilter->SetCalculateOrientedBoundingBox( false );
    labelGeometryFilter->SetCalculateOrientedIntensityRegions( false );
    labelGeometryFilter->SetCalculateOrientedLabelRegions( false );
    labelGeometryFilter->SetCalculatePixelIndices( false );
	labelGeometryFilter->SetReleaseDataFlag( true );

    typename RegionPropsImageType::Pointer labelImage;

    if ( binaryInput )
    {
        // adjust filter settings
        typedef itk::BinaryThresholdImageFilter<RegionPropsImageType, RegionPropsImageType> BinaryThresholdFilter;
        typename BinaryThresholdFilter::Pointer binaryThresholdFilter = BinaryThresholdFilter::New();
        binaryThresholdFilter->SetInput(inputImage);
        binaryThresholdFilter->SetNumberOfWorkUnits(maxThreads);
        binaryThresholdFilter->SetLowerThreshold(1);
        binaryThresholdFilter->SetUpperThreshold(65535);
        binaryThresholdFilter->SetOutsideValue(0.0);
        binaryThresholdFilter->SetInsideValue(1.0);
        binaryThresholdFilter->SetReleaseDataFlag( true );
        itkTryCatch(binaryThresholdFilter->Update(), "ExtractRegionProps --> BinaryThresholdFilter update.");
        
        // setup the filter
        typedef itk::BinaryImageToLabelMapFilter< RegionPropsImageType > BinaryImageToLabelMapFilterType;
        typename BinaryImageToLabelMapFilterType::Pointer binaryImageToLabelMapFilter = BinaryImageToLabelMapFilterType::New();
        binaryImageToLabelMapFilter->SetInput( binaryThresholdFilter->GetOutput() );
        binaryImageToLabelMapFilter->SetReleaseDataFlag( true );
        binaryImageToLabelMapFilter->SetInputForegroundValue(1.0);
        binaryImageToLabelMapFilter->SetOutputBackgroundValue(0.0);
        binaryImageToLabelMapFilter->SetFullyConnected(fullyConnected);
        itkTryCatch(binaryImageToLabelMapFilter->Update(), "ExtractRegionProps --> BinaryImageToLabelMapFilter update.");

        // convert the label mapt into a label image
        typedef itk::LabelMapToLabelImageFilter< typename BinaryImageToLabelMapFilterType::OutputImageType, RegionPropsImageType > LabelMapToLabelImageFilterType;
        typename LabelMapToLabelImageFilterType::Pointer labelMapToLabelImageFilter = LabelMapToLabelImageFilterType::New();
        labelMapToLabelImageFilter->SetInput(binaryImageToLabelMapFilter->GetOutput());
        labelMapToLabelImageFilter->SetReleaseDataFlag( false );
        itkTryCatch(labelMapToLabelImageFilter->Update(), "ExtractRegionProps --> LabelMapToLabelImage");

        // set the extraction filter input
        //labelGeometryFilter->SetInput( labelMapToLabelImageFilter->GetOutput() );
        labelImage = labelMapToLabelImageFilter->GetOutput();
    }
    else
    {
        // directly get the information from the prelabeled input
        //labelGeometryFilter->SetInput( inputImage );
        labelImage = inputImage;
    }

    // set the input image
    labelGeometryFilter->SetInput( labelImage );

    // update the geometry extraction filter
    itkTryCatch( labelGeometryFilter->Update(), "ExtractRegionPropsImageFilter, extract geometry" );
    Logger::GetInstance()->WriteLine( "+ ExtractRegionPropsImageFilter: Found " + QString::number(labelGeometryFilter->GetNumberOfLabels()) + " connected regions in the binary image." );

    // create the image iterators
    itk::ImageRegionIterator<RegionPropsImageType> inputIterator(inputImage, inputImage->GetLargestPossibleRegion());
    itk::ImageRegionIterator<RegionPropsImageType> labelIterator(labelImage, inputImage->GetLargestPossibleRegion());
    itk::ImageRegionIterator<TInputImage> outputIterator(outputImage, inputImage->GetLargestPossibleRegion());
    inputIterator.GoToBegin();
    labelIterator.GoToBegin();
    outputIterator.GoToBegin();

    // fill the output pixels while skipping regions that are too small/large
    while (inputIterator.IsAtEnd() == false)
    {
        // get the current label
        typename LabelGeometryImageFilterType::LabelPixelType labelValue = labelIterator.Value();

        // check if volume is larger than the specified minimum volume
        if (labelValue > 0 && labelGeometryFilter->GetVolume(labelValue) >= minimumVolume && (maximumVolume < 0 || labelGeometryFilter->GetVolume(labelValue) <= maximumVolume))
        {
            if (labelOutput == true)
                outputIterator.Set(labelIterator.Value());
            else
                outputIterator.Set(1);
        }

        // increment iterators
        ++inputIterator;
        ++labelIterator;
        ++outputIterator;
    }

    // set the output image
    ImageWrapper* outputWrapper = new ImageWrapper();
    outputWrapper->SetImage<TInputImage>(outputImage);
    outputWrapper->SetRescaleFlag(false);
    mOutputImages.append(outputWrapper);

	// update the process object widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< VolumeBasedRegionRejectionFilterWidget<Image2Float> > VolumeBasedRegionRejectionFilterWidgetImage2Float;
static ProcessObjectProxy< VolumeBasedRegionRejectionFilterWidget<Image3Float> > VolumeBasedRegionRejectionFilterWidgetImage3Float;
static ProcessObjectProxy< VolumeBasedRegionRejectionFilterWidget<Image2UShort> > VolumeBasedRegionRejectionFilterWidgetImage2UShort;
static ProcessObjectProxy< VolumeBasedRegionRejectionFilterWidget<Image3UShort> > VolumeBasedRegionRejectionFilterWidgetImage3UShort;

} // namespace XPIWIT