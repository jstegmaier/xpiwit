/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "ExtractLocalExtremaFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

#include "../../ITKCustom/itkExtractKeyPointsImageFilter.h"

// qt header
#include <QFileInfo>


namespace XPIWIT
{

// the default constructor
template< class TInputImage0, class TInputImage1 >
ExtractLocalExtremaFilterWidget<TInputImage0, TInputImage1>::ExtractLocalExtremaFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = ExtractLocalExtremaFilterWidget<TInputImage0, TInputImage1>::GetName();
    this->mDescription = "Extracts all local extrema in an image.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(2);

    this->mObjectType->SetNumberImageInputs(2);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(2);

    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);

    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(1);
	this->mObjectType->AppendMetaOutputType("KeyPoints");

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "KeyPointThreshold", "-1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Minium value for extrema. If set to -1 an automatic selection based on the mean intensity and the intensity standard deviation are used." );
    processObjectSettings->AddSetting( "StdDevMultiplicator", "2.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Multiplicator for the standard deviation. If automatic threshold selection is used, all seeds below (mu + StdDevMultiplicator * sigma) are rejected. " );
    processObjectSettings->AddSetting( "QuantileThreshold", "-1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "If set to a value between 0 and 1, values within the respective quantile are rejected. I.e. if set to 0.95 all seeds with intensity in the 95% quantile are rejected." );
    processObjectSettings->AddSetting( "KeyPointIndexOffset", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Adds an offset to the coordinates of each seed point. E.g. useful for MATLABs 1-based indexing." );
    processObjectSettings->AddSetting( "Neighborhood3D", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "0 to search in 2D only, 1 for searching in 3D only and 2 for searching in both 2D and 3D." );
    processObjectSettings->AddSetting( "NeighborhoodRadius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The neighborhood radius of the local extrema search." );
    processObjectSettings->AddSetting( "MinimumSeedCombinations", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "If detection in 2D and 3D is performed, seeds that were not multiply detected can be rejected." );
    processObjectSettings->AddSetting( "FuseSeedPoints", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Fuse extrema that lie within a small radius." );
    processObjectSettings->AddSetting( "Remove2DSeedsTouching3DSeeds", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Prefer 3D seeds over 2D seeds. I.e. all 2D seeds that lie within the seed radius of a 3D seed are rejected." );
    processObjectSettings->AddSetting( "AllowMaximumPlateaus", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Allow more than one point in a neighborhood to be an extremum." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage0, class TInputImage1>
ExtractLocalExtremaFilterWidget<TInputImage0, TInputImage1>::~ExtractLocalExtremaFilterWidget()
{
}


// the update function
template< class TInputImage0, class TInputImage1 >
void ExtractLocalExtremaFilterWidget<TInputImage0, TInputImage1>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	this->mMetaOutputs.at(0)->mIsMultiDimensional = true;

    // get filter parameters
    float keyPointThreshold = processObjectSettings->GetSettingValue( "KeyPointThreshold" ).toDouble();
    float quantileThreshold = processObjectSettings->GetSettingValue( "QuantileThreshold" ).toDouble();
    float stdDevMultiplicator = processObjectSettings->GetSettingValue( "StdDevMultiplicator" ).toDouble();
    unsigned int keyPointIndexOffset = processObjectSettings->GetSettingValue( "KeyPointIndexOffset" ).toInt();
    unsigned int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    unsigned int neighborhoodRadius = processObjectSettings->GetSettingValue( "NeighborhoodRadius" ).toInt();
    int neighborhood3D = processObjectSettings->GetSettingValue( "Neighborhood3D" ).toInt();
    int minimumSeedCombinations = processObjectSettings->GetSettingValue( "MinimumSeedCombinations" ).toInt();
    bool fuseSeedPoints = processObjectSettings->GetSettingValue( "FuseSeedPoints" ).toInt() > 0;
    bool remove2DSeedsTouching3DSeeds = processObjectSettings->GetSettingValue( "Remove2DSeedsTouching3DSeeds" ).toInt() > 0;
    bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;
    bool allowMaximumPlateaus = processObjectSettings->GetSettingValue( "AllowMaximumPlateaus" ).toInt() > 0;

	typename TInputImage0::Pointer inputImage1 = mInputImages.at(0)->template GetImage<TInputImage0>();
	typename TInputImage1::Pointer inputImage2 = mInputImages.at(1)->template GetImage<TInputImage1>();

    // initialize the extraction filter
    typename itk::ExtractKeyPointsImageFilter<TInputImage0, TInputImage0, TInputImage1>::Pointer extractKeyPointsFilter = itk::ExtractKeyPointsImageFilter<TInputImage0, TInputImage0, TInputImage1>::New();
    extractKeyPointsFilter->SetInput( inputImage1 );

    ////////// TODO ///////////
    extractKeyPointsFilter->SetMaximumScaleImage( inputImage2 );
    ////////// TODO ///////////

    extractKeyPointsFilter->SetKeyPointOffset( keyPointIndexOffset );
    extractKeyPointsFilter->SetKeyPointThreshold( keyPointThreshold );
    extractKeyPointsFilter->SetKeyPointQuantile( quantileThreshold );
    extractKeyPointsFilter->SetStdDevMultiplicator( stdDevMultiplicator );
    extractKeyPointsFilter->SetNeighborhood3D( neighborhood3D );
    extractKeyPointsFilter->SetNeighborhoodRadius( neighborhoodRadius );
    extractKeyPointsFilter->SetMinimumSeedCombinations( minimumSeedCombinations );
    extractKeyPointsFilter->SetNumberOfWorkUnits( maxThreads );
    extractKeyPointsFilter->SetFuseSeedPoints( fuseSeedPoints );
    extractKeyPointsFilter->SetAllowMaximumPlateaus( allowMaximumPlateaus );
    extractKeyPointsFilter->SetRemove2DSeedsTouching3DSeeds( remove2DSeedsTouching3DSeeds );
    extractKeyPointsFilter->SetMetaDataFilter( this->mMetaOutputs.at(0) );

    // extract seed points
    extractKeyPointsFilter->SetReleaseDataFlag( releaseDataFlag );
    itkTryCatch( extractKeyPointsFilter->Update(), "ExtractLocalExtremaFilterWidget --> Update extraction filter!");

    ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage0>( extractKeyPointsFilter->GetOutput() );
    mOutputImages.append( outputImage );

    // update the process object widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< ExtractLocalExtremaFilterWidget<Image2Float, Image2Float> > ExtractLocalExtremaFilterWidgetImage2Float;
static ProcessObjectProxy< ExtractLocalExtremaFilterWidget<Image3Float, Image3Float> > ExtractLocalExtremaFilterWidgetImage3Float;
static ProcessObjectProxy< ExtractLocalExtremaFilterWidget<Image2UShort, Image2UShort> > ExtractLocalExtremaFilterWidgetImage2UShort;
static ProcessObjectProxy< ExtractLocalExtremaFilterWidget<Image3UShort, Image3UShort> > ExtractLocalExtremaFilterWidgetImage3UShort;

} // namespace XPIWIT
