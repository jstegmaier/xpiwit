/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "ExtractIntensityProfileFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkIntensityWindowingImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkLabelGeometryImageFilter.h"
#include "itkLabelStatisticsImageFilter.h"
#include "itkBinaryImageToLabelMapFilter.h"
#include "itkLabelMapToLabelImageFilter.h"
#include "itkImageRegionConstIterator.h"


namespace XPIWIT
{

// the default constructor
template <class TInputImage>
ExtractIntensityProfileFilterWidget<TInputImage>::ExtractIntensityProfileFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = ExtractIntensityProfileFilterWidget<TInputImage>::GetName();
    this->mDescription = "my description goes here";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(1);
	this->mObjectType->AppendMetaOutputType("IntensityProfiles");

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "BinaryInput", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If input is binary, a label image is generated before extracting the region properties." );
	processObjectSettings->AddSetting( "Threshold", "-1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "If input is binary, recreate a binary image to get rid of casting failures (-1 no recalculation)." );
    processObjectSettings->AddSetting( "FullyConnected", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Determines the connectivity model. Use FullyConnected = 1 for 8/26-neighborhood or to 0 for 4/6 neighborhood." );
	processObjectSettings->AddSetting( "GeometryMode", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Switch between geometry and statistics mode" );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template <class TInputImage>
ExtractIntensityProfileFilterWidget<TInputImage>::~ExtractIntensityProfileFilterWidget()
{
}

// the update function
template <class TInputImage>
void ExtractIntensityProfileFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters from the xml file
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;
	bool binaryInput = processObjectSettings->GetSettingValue( "BinaryInput" ).toInt() > 0;
	double threshold = processObjectSettings->GetSettingValue( "Threshold" ).toFloat();
    bool fullyConnected = processObjectSettings->GetSettingValue( "FullyConnected" ).toInt() > 0;
	bool geometryMode = processObjectSettings->GetSettingValue( "GeometryMode" ).toInt() > 0;

	// get a pointer to the input image
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage< TInputImage >();


	// write the output image (here we simply copy the input image, feel free to add any filters in between :-) )
	ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( inputImage );
    mOutputImages.append( outputImage );
	
	// Create the meta data object. This is basically just a large table which is stored as a csv file
	MetaDataFilter* metaOutput = this->mMetaOutputs.at(0);
    metaOutput->mIsMultiDimensional = true;
	metaOutput->mPostfix = "IntensityProfiles";

	QStringList metaDescription;                    QStringList metaType;
    metaDescription << "myfirstcolumn"; 			metaType << "int";

	// write to meta object
	metaOutput->mTitle = metaDescription;
	metaOutput->mType = metaType;

	// write the actual data to the meta output
	for (int i=0; i<10; ++i)
	{
		QList<float> currentData;
		currentData << i;
		metaOutput->mData.append( currentData );
    }

	// update the process object widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< ExtractIntensityProfileFilterWidget<Image2Float> > ExtractIntensityProfileFilterWidgetImage2Float;
static ProcessObjectProxy< ExtractIntensityProfileFilterWidget<Image3Float> > ExtractIntensityProfileFilterWidgetImage3Float;
static ProcessObjectProxy< ExtractIntensityProfileFilterWidget<Image2UShort> > ExtractIntensityProfileFilterWidgetImage2UShort;
static ProcessObjectProxy< ExtractIntensityProfileFilterWidget<Image3UShort> > ExtractIntensityProfileFilterWidgetImage3UShort;

} // namespace XPIWIT

