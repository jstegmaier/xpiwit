/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "../../../Core/Utilities/Logger.h"

// ACME headers
#include "ACMEMultiScalePlateMeasureFilterWidget.h"
#include "../../../Filter/Base/Management/ImageWrapper.h"

// itk header
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkSymmetricSecondRankTensor.h"
#include "itkImageRegionIterator.h"
#include "itkVectorIndexSelectionCastImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "../../ThirdParty/ACME/TensorVoting/itkTensorVoting3D.h"
#include "../../ThirdParty/ACME/TensorVoting/itkTensorToSaliencyImageFilter.h"
#include "../../ThirdParty/ACME/PlanarityFilter/itkMultiscaleStructMeasureImageFilter.h"


namespace XPIWIT
{

// the default constructor
template <class TInputImage>
ACMEMultiScalePlateMeasureFilterWidget<TInputImage>::ACMEMultiScalePlateMeasureFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = ACMEMultiScalePlateMeasureFilterWidget<TInputImage>::GetName();
    this->mDescription = "MultiScalePlateMeasureImageFilter.";
    this->mDescription += "Enhances plane-like structures in the images using structure tensor information (See Mosaliganti et al. 2012).";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "NeighborhoodSizePM", "1.4", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The neighborhood size of the planarity measure (physical units)." );
	processObjectSettings->AddSetting( "NeighborhoodSizeTV", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The neighborhood size of the tensor voting (physical units)." );
	processObjectSettings->AddSetting( "PerformTensorVoting", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enable, tensor voting is performed to close gaps in membrane structures." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template <class TInputImage>
ACMEMultiScalePlateMeasureFilterWidget<TInputImage>::~ACMEMultiScalePlateMeasureFilterWidget()
{
}


// the update function
template <class TInputImage>
void ACMEMultiScalePlateMeasureFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const bool performTensorVoting = processObjectSettings->GetSettingValue( "PerformTensorVoting" ).toInt() > 0;
    const float neighborhoodSizePM = processObjectSettings->GetSettingValue( "NeighborhoodSizePM" ).toDouble();
	const float neighborhoodSizeTV = processObjectSettings->GetSettingValue( "NeighborhoodSizeTV" ).toDouble();

	// get the input image
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

	// Declare the type of multiscale vesselness filter
	typedef itk::MultiscaleStructMeasureImageFilter<TInputImage, TInputImage> MultiscalePlateFilterType;

	// rescale intensity to 0-255 range as required for the Mosaliganti et al. implementatiton
	typedef itk::RescaleIntensityImageFilter< TInputImage, TInputImage > RescaleInputFilterType;
	typename RescaleInputFilterType::Pointer rescaleInputImage = RescaleInputFilterType::New();
	rescaleInputImage->SetInput( inputImage );
	rescaleInputImage->SetOutputMinimum( 0 );
	rescaleInputImage->SetOutputMaximum( 255 );
	itkTryCatch( rescaleInputImage->Update(), "Exception Caught: rescaling input intensity range to 0 to 255 for Mosaliganti et al. plate detector." );
	
	// Create a vesselness Filter
	typename MultiscalePlateFilterType::Pointer multiscalePlateFilter = MultiscalePlateFilterType::New();
	multiscalePlateFilter->SetInput( rescaleInputImage->GetOutput() );
	multiscalePlateFilter->SetObjectType( 0 );
	multiscalePlateFilter->SetSigmaMin( neighborhoodSizePM );
	multiscalePlateFilter->SetSigmaMax( neighborhoodSizePM );
	multiscalePlateFilter->SetNumberOfSigmaSteps( 1 );

	// update the pipeline by calling update for the most downstream filter
	itkTryCatch( multiscalePlateFilter->Update(), "Exception Caught: MultiScalePlateFilter Update Function." );
	
	// set plate filter result as output or additionally perform tensor voting
	if (performTensorVoting == false)
	{
		typename RescaleInputFilterType::Pointer rescaleOutputImage = RescaleInputFilterType::New();
		rescaleOutputImage->SetInput( multiscalePlateFilter->GetOutput() );
		rescaleOutputImage->SetOutputMinimum( 0.0 );
		rescaleOutputImage->SetOutputMaximum( 1.0 );
		itkTryCatch( rescaleOutputImage->Update(), "Exception Caught: rescaling output intensity range from 0 to 255 by Mosaliganti et al. plate detector." );

		// set the output image
		ImageWrapper *outputImage = new ImageWrapper();
		outputImage->SetImage<TInputImage>( rescaleOutputImage->GetOutput() );
		mOutputImages.append( outputImage );
	}
	else
	{
		// Perform the tensor voting
		const unsigned int Dimension = TInputImage::ImageDimension;

		/*
		typedef itk::Matrix< double, Dimension, Dimension> MatrixType;
		typedef itk::Image< MatrixType, Dimension > EigenMatrixImageType;
		typedef itk::ImageFileWriter< EigenMatrixImageType > EigenMatrixWriterType;
		EigenMatrixWriterType::Pointer eigMatrixWriter = EigenMatrixWriterType::New();
		eigMatrixWriter->SetFileName( "D:/EigenMatrix.mha" );
		eigMatrixWriter->SetInput ( multiscalePlateFilter->GetEigenMatrix() );
		eigMatrixWriter->Update();
		*/
	
		typedef itk::Image< unsigned char, Dimension >   FeatureImageType;
		typedef itk::Image< float, Dimension > InputImageType;
		typedef itk::ImageRegionIterator< InputImageType > InputIteratorType;
	
		typedef itk::Matrix< double, Dimension, Dimension> MatrixPixelType;
		typedef itk::Image< MatrixPixelType, Dimension > MatrixImageType;
		typedef itk::TensorVoting3D< MatrixImageType > TensorVotingFilterType;
		typedef itk::ImageRegionIterator< MatrixImageType > IteratorType;
	
		typedef itk::Vector< double, Dimension > VectorType;
		typedef itk::Image< VectorType, Dimension > VectorImageType;
		typedef itk::TensorToSaliencyImageFilter< MatrixImageType, VectorImageType > SaliencyFilterType;
		typedef itk::VectorIndexSelectionCastImageFilter< VectorImageType, InputImageType > IndexFilterType;
		typedef itk::RescaleIntensityImageFilter< InputImageType, InputImageType > RescaleFilterType;
	
		// define eigen matrix pointer
		typename MatrixImageType::Pointer input = multiscalePlateFilter->GetEigenMatrix();
	
		// Rotate around a circle and fill the pixels
		InputIteratorType iIt( multiscalePlateFilter->GetOutput(), multiscalePlateFilter->GetOutput()->GetLargestPossibleRegion() );
		IteratorType It( input, input->GetLargestPossibleRegion() );
		iIt.GoToBegin();
		It.GoToBegin();
		MatrixPixelType p;
		VectorType u;
		double q;
		while( !It.IsAtEnd() )
		{
			q = iIt.Get();
			u = It.Get()[Dimension-1];
			//std::cout << "input value " << q << ", matrix entries " << It.Get()[0][0] << ", " << It.Get()[0][1] << ", " << It.Get()[0][2] << ", " << It.Get()[1][0] << ", " << It.Get()[1][1] << ", " << It.Get()[1][2] << ", " << It.Get()[2][0] << ", " << It.Get()[2][1] << ", " << It.Get()[2][2] << std::endl;

			for( unsigned int i = 0; i < Dimension; i++ )
			{
				for( unsigned int j = 0; j < Dimension; j++ )
				{
					p[i][j] = p[j][i] = u[i]*u[j]*q;
					//std::cout << p[i][j] << std::endl;
				}
			}
			It.Set( p );
			++iIt;
			++It;
		}
		std::cout << "Filled input image..." << std::endl;
		
		// Setup the tensor voting filter
		typename TensorVotingFilterType::Pointer tensorVote = TensorVotingFilterType::New();
		tensorVote->SetInput( input );
		tensorVote->SetSigma( neighborhoodSizeTV );
		tensorVote->SetUseSparseVoting( false );
		tensorVote->SetNumberOfWorkUnits( maxThreads );
	
		// Update the tensor voting filter
		itkTryCatch( tensorVote->Update(), "Exception Caught: Tensor Voting Filter Execution." );
	
		// setup and update the saliency filter
		typename SaliencyFilterType::Pointer saliency = SaliencyFilterType::New();
		saliency->SetInput( tensorVote->GetOutput() );
		itkTryCatch( saliency->Update(), "Exception Caught: Saliency Filter Execution." );
		std::cout << "Saliency complete..." << std::endl;
	
		// extract the third component of the vector image
		typename IndexFilterType::Pointer componentExtractor = IndexFilterType::New();
		componentExtractor->SetInput( saliency->GetOutput() );
		componentExtractor->SetIndex( 2 );
		itkTryCatch( componentExtractor->Update(), "Exception Caught: Component Extraction Update." );
		std::cout << "Extracted third component from voting vectors..." << std::endl;

		// rescale the intensity to match the desired output range
		typename RescaleFilterType::Pointer rescale = RescaleFilterType::New();
		rescale->SetInput( componentExtractor->GetOutput() );
		rescale->SetOutputMinimum( 0 );
		rescale->SetOutputMaximum( 1.0 );
		itkTryCatch( rescale->Update(), "Exception Caught: Rescale Intensity of the third vector component for output." );
	
		// set the output image
		ImageWrapper *outputImage = new ImageWrapper();
		outputImage->SetImage<TInputImage>( rescale->GetOutput() );
		mOutputImages.append( outputImage );
	}
	
	// update the base class
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< ACMEMultiScalePlateMeasureFilterWidget<Image3Float> > ACMEMultiScalePlateMeasureFilterWidgetImage3Float;

} // namespace XPIWIT
