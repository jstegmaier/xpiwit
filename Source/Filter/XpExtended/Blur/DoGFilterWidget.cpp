/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "DoGFilterWidget.h"
#include "../../../Core/Utilities/Logger.h"
#include "../../../Filter/Base/Management/ImageWrapper.h"

// itk header
#include "itkDiscreteGaussianImageFilter.h"
#include "itkRecursiveGaussianImageFilter.h"
#include "itkSubtractImageFilter.h"
#include "itkImageDuplicator.h"
#include "itkIntensityWindowingImageFilter.h"
using namespace itk;

// XPIWIT namespace
namespace XPIWIT 
{

// the default constructor
template <class TInputImage>
DoGFilterWidget<TInputImage>::DoGFilterWidget() : ProcessObjectBase()
{
    // set the widget type
	this->mName = DoGFilterWidget<TInputImage>::GetName();
    this->mDescription = "Difference of Gaussian Filter. ";
    this->mDescription += "The input image will be processed with the first gaussian kernel and subtracted by the result of the filtering with the second gaussian kernel.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Sigma1", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Sigma value of the smaller gaussian kernel" );
    processObjectSettings->AddSetting( "Sigma2", "5.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Sigma value of the bigger gaussian kernel");
    processObjectSettings->AddSetting( "MaximumError", "0.01", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Maximum error of the gaussian function approximation" );
    processObjectSettings->AddSetting( "MaximumKernelWidth", "32", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Maximum radius of the kernel in pixel" );
    processObjectSettings->AddSetting( "UseImageSpacing", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Use the real spacing for the gaussian kernel creation" );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template <class TInputImage>
DoGFilterWidget<TInputImage>::~DoGFilterWidget()
{
}


// the update function
template <class TInputImage>
void DoGFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    const bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;
    const float sigma1 = processObjectSettings->GetSettingValue( "Sigma1" ).toDouble();
    const float sigma2 = processObjectSettings->GetSettingValue( "Sigma2" ).toDouble();

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

    typedef itk::ImageDuplicator< TInputImage > DuplicatorType;
    typename DuplicatorType::Pointer duplicator = DuplicatorType::New();
    duplicator->SetInputImage( inputImage );
    duplicator->Update();

    // create the DoG filter and adjust settings
    typedef itk::RecursiveGaussianImageFilter<TInputImage, TInputImage> GaussianFilter;
    typename GaussianFilter::Pointer filterS1 = GaussianFilter::New();
    filterS1->SetInput( duplicator->GetOutput() );
    filterS1->SetReleaseDataFlag( releaseDataFlag );
    filterS1->SetSigma( sigma1 );
    filterS1->SetNormalizeAcrossScale(true);
    filterS1->SetDirection(0);
    filterS1->SetNumberOfWorkUnits( maxThreads );

    typename GaussianFilter::Pointer filterS2 = GaussianFilter::New();
    filterS2->SetInput( filterS1->GetOutput() );
    filterS2->SetReleaseDataFlag( releaseDataFlag );
    filterS2->SetSigma( sigma1 );
    filterS2->SetNormalizeAcrossScale(true);
    filterS2->SetDirection(1);
    filterS2->SetNumberOfWorkUnits( maxThreads );

	typename GaussianFilter::Pointer filterS3 = GaussianFilter::New();
	if( TInputImage::ImageDimension > 2 ){
		filterS3->SetInput( filterS2->GetOutput() );
		filterS3->SetReleaseDataFlag( releaseDataFlag );
		filterS3->SetSigma( sigma1 );
		filterS3->SetNormalizeAcrossScale(true);
		filterS3->SetDirection(2);
		filterS3->SetNumberOfWorkUnits( maxThreads );
		filterS3->Update();
	}

    typename GaussianFilter::Pointer filterL1 = GaussianFilter::New();
    filterL1->SetInput( duplicator->GetOutput() );
    filterL1->SetReleaseDataFlag( releaseDataFlag );
    filterL1->SetSigma( sigma2 );
    filterL1->SetNormalizeAcrossScale(true);
    filterL1->SetDirection(0);
    filterL1->SetNumberOfWorkUnits( maxThreads );

    typename GaussianFilter::Pointer filterL2 = GaussianFilter::New();
    filterL2->SetInput( filterL1->GetOutput() );
    filterL2->SetReleaseDataFlag( releaseDataFlag );
    filterL2->SetSigma( sigma2 );
    filterL2->SetNormalizeAcrossScale(true);
    filterL2->SetDirection(1);
    filterL2->SetNumberOfWorkUnits( maxThreads );

	typename GaussianFilter::Pointer filterL3 = GaussianFilter::New();
	if( TInputImage::ImageDimension > 2 ){
		filterL3->SetInput( filterL2->GetOutput() );
		filterL3->SetReleaseDataFlag( releaseDataFlag );
		filterL3->SetSigma( sigma2 );
		filterL3->SetNormalizeAcrossScale(true);
		filterL3->SetDirection(2);
		filterL3->SetNumberOfWorkUnits( maxThreads );
		filterL3->Update();
	}

    typedef itk::SubtractImageFilter< TInputImage, TInputImage, TInputImage> SubtractType;
	typename SubtractType::Pointer diffFilter = SubtractType::New ();
	if( TInputImage::ImageDimension > 2 ){
		diffFilter->SetReleaseDataFlag( releaseDataFlag );
		diffFilter->SetInput1( filterS3->GetOutput() );
		diffFilter->SetInput2( filterL3->GetOutput() );
		diffFilter->SetNumberOfWorkUnits( maxThreads );
	}else{

		diffFilter->SetReleaseDataFlag( releaseDataFlag );
		diffFilter->SetInput1( filterS2->GetOutput() );
		diffFilter->SetInput2( filterL2->GetOutput() );
		diffFilter->SetNumberOfWorkUnits( maxThreads );
	}


    // update the pipeline by calling update for the most downstream filter
    try
    {
        diffFilter->Update();
    }
    catch( itk::ExceptionObject & err )
    {
        Logger::GetInstance()->WriteLine("ExceptionObject caught !" );
        Logger::GetInstance()->WriteException( err );
        return;
    }

	// save output
	ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( diffFilter->GetOutput() );
    mOutputImages.append( outputImage );

    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< DoGFilterWidget<Image2Float> > DoGFilterWidgetImage2Float;
static ProcessObjectProxy< DoGFilterWidget<Image3Float> > DoGFilterWidgetImage3Float;
//static ProcessObjectProxy< DoGFilterWidget<Image2UShort> > DoGFilterWidgetImage2UShort;
//static ProcessObjectProxy< DoGFilterWidget<Image3UShort> > DoGFilterWidgetImage3UShort;

} // namespace XPIWIT
