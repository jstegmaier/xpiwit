/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "HotspotFilterWidget.h"
#include "../../../Core/Utilities/Logger.h"
#include "../../ITKCustom/itkHotspotImageFilter.h"
#include "../../../Filter/Base/Management/ImageWrapper.h"

// itk header
#include "itkSubtractImageFilter.h"
#include "itkImageDuplicator.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkConstShapedNeighborhoodIterator.h"
#include "itkShapedNeighborhoodIterator.h"


namespace XPIWIT
{

// the default constructor
template <class TInputImage>
HotspotFilterWidget<TInputImage>::HotspotFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = HotspotFilterWidget<TInputImage>::GetName();
    this->mDescription = "Hotspot Filter. ";
    this->mDescription += "calculates the maximum of the minimum with increasing radius to detekt small spots.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "FilterDimensionality", "2", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Dimensions being processed." );
    processObjectSettings->AddSetting( "Radius", "3", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Radius of the kernel." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template <class TInputImage>
HotspotFilterWidget<TInputImage>::~HotspotFilterWidget()
{
}


// the update function
template <class TInputImage>
void HotspotFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    const int filterDim = processObjectSettings->GetSettingValue( "FilterDimensionality" ).toInt();
    const int maxRadius = processObjectSettings->GetSettingValue( "Radius" ).toInt();
    const bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;
    
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

	int dimensionsToProcess = filterDim;
	if( TInputImage::ImageDimension < dimensionsToProcess )
		dimensionsToProcess = TInputImage::ImageDimension;

	typedef typename itk::HotspotImageFilter<TInputImage> HotspotFilter;
    typename HotspotFilter::Pointer hotspotFilter = HotspotFilter::New();
    hotspotFilter->SetInput( inputImage );
    hotspotFilter->SetReleaseDataFlag( releaseDataFlag );
    hotspotFilter->SetNumberOfWorkUnits( maxThreads ); //maxThreads
    hotspotFilter->SetMaxRadius( maxRadius );
	if( filterDim > 2)
		hotspotFilter->SetThreeD(true);
	else
		hotspotFilter->SetThreeD(false);

    // update the pipeline by calling update for the most downstream filter
    itkTryCatch( hotspotFilter->Update(), "Error: Updating HotSpotFilterWidget failed!" );
    ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( hotspotFilter->GetOutput() );
    mOutputImages.append( outputImage );

    // update the parent
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< HotspotFilterWidget<Image2Float> > HotspotFilterWidgetImage2Float;
static ProcessObjectProxy< HotspotFilterWidget<Image3Float> > HotspotFilterWidgetImage3Float;
static ProcessObjectProxy< HotspotFilterWidget<Image2UShort> > HotspotFilterWidgetImage2UShort;
static ProcessObjectProxy< HotspotFilterWidget<Image3UShort> > HotspotFilterWidgetImage3UShort;

} // namespace XPIWIT
