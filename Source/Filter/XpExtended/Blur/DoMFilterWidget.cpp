/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "DoMFilterWidget.h"
#include "../../../Core/Utilities/Logger.h"
#include "../../ITKCustom/itkDoMImageFilter.h"
#include "../../../Filter/Base/Management/ImageWrapper.h"

// itk header
#include "itkBoxMeanImageFilter.h"
#include "itkCropImageFilter.h"
#include "itkSubtractImageFilter.h"
#include "itkImageDuplicator.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkSubtractImageFilter.h"

namespace XPIWIT
{

// the default constructor
template <class TInputImage>
DoMFilterWidget<TInputImage>::DoMFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = DoMFilterWidget<TInputImage>::GetName();
    this->mDescription = "Difference of Mean Filter. ";
    this->mDescription += "The input image will be processed with the first mean kernel and subtracted by the result of the filtering with the second mean kernel.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "FilterDimensionality", "3", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Dimensions being processed." );
    processObjectSettings->AddSetting( "RadiusSmallXY", "5", ProcessObjectSetting::SETTINGVALUETYPE_INT, "x and y radius of the small kernel." );
    processObjectSettings->AddSetting( "RadiusLargeXY", "10", ProcessObjectSetting::SETTINGVALUETYPE_INT, "x and y radius of the big kernel." );
    processObjectSettings->AddSetting( "RadiusSmallZ", "2", ProcessObjectSetting::SETTINGVALUETYPE_INT, "z radius of the small kernel." );
    processObjectSettings->AddSetting( "RadiusLargeZ", "4", ProcessObjectSetting::SETTINGVALUETYPE_INT, "z radius of the big kernel.");

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template <class TInputImage>
DoMFilterWidget<TInputImage>::~DoMFilterWidget()
{
}


// update the widget
template <class TInputImage>
void DoMFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    const int filterDim = processObjectSettings->GetSettingValue( "FilterDimensionality" ).toInt();
    int radiusSmallXY = processObjectSettings->GetSettingValue( "RadiusSmallXY" ).toInt();
    int radiusLargeXY = processObjectSettings->GetSettingValue( "RadiusLargeXY" ).toInt();
    int radiusSmallZ = processObjectSettings->GetSettingValue( "RadiusSmallZ" ).toInt();
    int radiusLargeZ = processObjectSettings->GetSettingValue( "RadiusLargeZ" ).toInt();

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

    typename TInputImage::SizeType radiusSmall;
    typename TInputImage::SizeType radiusLarge;
    radiusSmall.Fill( radiusSmallXY );
    radiusLarge.Fill( radiusLargeXY );
    if( filterDim != 3 ){
        radiusSmall.SetElement(2, 0);
        radiusLarge.SetElement(2, 0);
    }
    else{
        radiusSmall.SetElement(2, radiusSmallZ);
        radiusLarge.SetElement(2, radiusLargeZ);
    }


    // mean filter
    typedef typename itk::DoMImageFilter<TInputImage> MeanFilterType;
    typename MeanFilterType::Pointer MeanFilter = MeanFilterType::New();
    MeanFilter->SetInput( inputImage );
    MeanFilter->SetNumberOfWorkUnits( maxThreads );
    MeanFilter->SetRadius(radiusLarge);
    MeanFilter->SetMinRadius(radiusSmall);
    MeanFilter->SetMaxRadius(radiusLarge);
    MeanFilter->Update();

    int offset = 1;

    typename TInputImage::PointType originPoint;
    originPoint.Fill( radiusLargeXY + offset );
    typename TInputImage::IndexType desiredStart;
    desiredStart.Fill( radiusLargeXY + offset );
    if( filterDim != 3 ){
        desiredStart.SetElement(2, 0);
        originPoint.SetElement(2, 0);
    }
    else{
        desiredStart.SetElement(2, radiusLargeZ + offset);
        originPoint.SetElement(2, radiusLargeZ + offset);
    }

    typename TInputImage::SizeType desiredSize;
    for( int i = 0; i < TInputImage::ImageDimension; ++i ){
        desiredSize.SetElement(i, MeanFilter->GetOutput()->GetLargestPossibleRegion().GetSize().GetElement(i) - (desiredStart.GetElement( i ) * 2) );
    }

    typename TInputImage::RegionType desiredRegion(desiredStart, desiredSize);

    //std::cout << "desiredRegion: " << desiredRegion << std::endl;

    typedef typename itk::ExtractImageFilter< TInputImage, TInputImage > FilterType;
    typename FilterType::Pointer cropFilter = FilterType::New();
    cropFilter->SetExtractionRegion(desiredRegion);
    cropFilter->SetInput( MeanFilter->GetOutput() );
#if ITK_VERSION_MAJOR >= 4
    cropFilter->SetDirectionCollapseToIdentity(); // This is required.
#endif


    // update the pipeline by calling update for the most downstream filter
    try
    {
        cropFilter->Update();
    }
    catch( itk::ExceptionObject & err )
    {
        Logger::GetInstance()->WriteLine("ExceptionObject caught !" );
        Logger::GetInstance()->WriteException( err );
        return;
    }

	typename TInputImage::Pointer outputImage = cropFilter->GetOutput();
	outputImage->SetLargestPossibleRegion( desiredRegion );

	ImageWrapper *outputImageWrapper = new ImageWrapper();
    outputImageWrapper->SetImage<TInputImage>( outputImage );
    mOutputImages.append( outputImageWrapper );

    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< DoMFilterWidget<Image2Float> > DoMFilterWidgetImage2Float;
static ProcessObjectProxy< DoMFilterWidget<Image3Float> > DoMFilterWidgetImage3Float;
static ProcessObjectProxy< DoMFilterWidget<Image2UShort> > DoMFilterWidgetImage2UShort;
static ProcessObjectProxy< DoMFilterWidget<Image3UShort> > DoMFilterWidgetImage3UShort;

} // namespace XPIWIT
