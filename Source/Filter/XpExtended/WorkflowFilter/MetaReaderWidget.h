/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef METAREADERWIDGET_H
#define METAREADERWIDGET_H

// namespace header
#include "../../Base/Management/ProcessObjectBase.h"
#include "../../Base/Management/ProcessObjectProxy.h"

// itk header
#include "itkImageFileReader.h"
#include "itkExtractImageFilter.h"

// qt header
#include <QtCore/QStringList>


namespace XPIWIT
{

/**
 *	@class MetaReaderWidget
 *	Reads meta inputs
 */
class MetaReaderWidget : public ProcessObjectBase
{
    public:

        /**
         * The constructor.
         * @param parent The parent of the main window. Usually should be NULL.
         */
        inline MetaReaderWidget();


        /**
         * The destructor.
         */
        inline virtual ~MetaReaderWidget();
        

        /**
         * Updates the reader process object
         */
        inline void Update();


		/**
		 * Static functions used for the filter factory.
		 */
		static QString GetName() { return "MetaReader"; }
		static QString GetType() { return "float"; }
		static int GetDimension() { return 2; }

};

} // namespace XPIWIT

//#include "MetaReaderWidget.txx"

#endif
