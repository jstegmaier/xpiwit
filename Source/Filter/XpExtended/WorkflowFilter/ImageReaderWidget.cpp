/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// namespace header
#include "ImageReaderWidget.h"
#include "CastImageFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../ITKCustom/itkFuseRotationImagesFilter.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkImageFileReader.h"

#include "itkImageSeriesReader.h"
#include "itkNumericSeriesFileNames.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkImageRegionIterator.h"
#include "itkExtractImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkMultiplyImageFilter.h"

// qt header
#include <QtCore/QUrl>
#include <QtCore/QStringList>

// system header
#include <math.h>
using namespace XPIWIT;


namespace XPIWIT
{

// the default constructor
template <class TInputImage>
ImageReaderWidget<TInputImage>::ImageReaderWidget() : ProcessObjectBase()
{
    // set type
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_READER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);
    this->mName = "ImageReader";
    this->mDescription = "Reads image from disk";

    // get process object settings pointer
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // add settings
    processObjectSettings->AddSetting( "UseSeriesReader", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Combine separate 2D images to one 3D stack. Drag&drop first file of the series and substitute the series index by %0nd, with n being the number of digits." );
    processObjectSettings->AddSetting( "SeriesMinIndex", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Start index for the series reader" );
    processObjectSettings->AddSetting( "SeriesMaxIndex", "499", ProcessObjectSetting::SETTINGVALUETYPE_INT, "End index for the series reader" );
	processObjectSettings->AddSetting( "SeriesIncrement", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Index increment for the series reader" );

    processObjectSettings->AddSetting( "SpacingX", "1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Original spacing in the first dimension" );
    processObjectSettings->AddSetting( "SpacingY", "1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Original spacing in the second dimension"  );
    processObjectSettings->AddSetting( "SpacingZ", "1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Original spacing in the third dimension"  );
    //processObjectSettings->AddSetting( "FuseRotations", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN );
    //processObjectSettings->AddSetting( "FuseTranslationX", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE );
    //processObjectSettings->AddSetting( "FuseTranslationY", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE );
    //processObjectSettings->AddSetting( "FuseTranslationZ", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE );
	processObjectSettings->AddSetting("InputMinimumValue", "0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Minimum intensity value of the input image. Set to -1 for auto detection");
	processObjectSettings->AddSetting("InputMaximumValue", "65535", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Maximum intensity value of the input image. Set to -1 for auto detection");
	processObjectSettings->AddSetting("NormalizeIntensity", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, rescales the input intensities to float range [0, 1]. Only disable for LabelImages.");

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template <class TInputImage>
ImageReaderWidget<TInputImage>::~ImageReaderWidget()
{
}



// update the reader
template <class TInputImage>
void ImageReaderWidget<TInputImage>::Update()
{
    // start timer and get the process object settings object
    ProcessObjectBase::StartTimer();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get the widget settings
    float spacingParameter[3];
    const int maxThreads		= processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    spacingParameter[0]			= processObjectSettings->GetSettingValue( "SpacingX" ).toFloat();
    spacingParameter[1]			= processObjectSettings->GetSettingValue( "SpacingY" ).toFloat();
    spacingParameter[2]			= processObjectSettings->GetSettingValue( "SpacingZ" ).toFloat();
	float inputMinimumValue = processObjectSettings->GetSettingValue( "InputMinimumValue" ).toFloat();
	float inputMaximumValue = processObjectSettings->GetSettingValue("InputMaximumValue").toFloat();
	bool useSeriesReader = processObjectSettings->GetSettingValue( "UseSeriesReader" ).toInt() > 0;
	bool normalizeIntensity = processObjectSettings->GetSettingValue("NormalizeIntensity").toInt() > 0;
	const int seriesMinIndex = processObjectSettings->GetSettingValue( "SeriesMinIndex" ).toInt();
	const int seriesMaxIndex = processObjectSettings->GetSettingValue( "SeriesMaxIndex" ).toInt();
	const int seriesIncrement = processObjectSettings->GetSettingValue( "SeriesIncrement" ).toInt();

	

    // declare the reader
	typedef itk::Image<float, TInputImage::ImageDimension> TReaderType;
	typename TReaderType::Pointer tempImage;
    typename itk::ImageSource<TReaderType>::Pointer fileReader;

	qint64 timer0 = QDateTime::currentMSecsSinceEpoch();

	// setup the reader
	if (useSeriesReader == false)
	{
		typedef itk::ImageFileReader<TReaderType>  ReaderType;
		fileReader = ReaderType::New();
		((ReaderType*)fileReader.GetPointer())->SetFileName( this->mPath.toStdString() );
		fileReader->SetReleaseDataFlag( true );
		fileReader->SetNumberOfWorkUnits( 1 );

		timer0 = QDateTime::currentMSecsSinceEpoch();
		itkTryCatch( fileReader->Update(), "Exception Caught: ImageReaderWidget -> Update function of the reader." );
		Logger::GetInstance()->WriteLine( QString("Updating reader took: ") + QString::number((float)(QDateTime::currentMSecsSinceEpoch() - timer0) / 1000.0) );
	}
	else
	{
		typedef itk::ImageSeriesReader<TReaderType>  ReaderType;
		fileReader = ReaderType::New();
		  
		typedef itk::NumericSeriesFileNames NameGeneratorType;
		NameGeneratorType::Pointer nameGenerator = NameGeneratorType::New();
		nameGenerator->SetSeriesFormat( this->mPath.toStdString() );
		nameGenerator->SetStartIndex( seriesMinIndex );
		nameGenerator->SetEndIndex( seriesMaxIndex );
		nameGenerator->SetIncrementIndex( seriesIncrement );
		((ReaderType*)fileReader.GetPointer())->SetFileNames( nameGenerator->GetFileNames()  );
		itkTryCatch( fileReader->Update(), "Exception Caught: ImageReaderWidget -> Update function of the series reader." );
	}

	
	timer0 = QDateTime::currentMSecsSinceEpoch();

	typedef itk::MinimumMaximumImageCalculator<TReaderType> MinMaxCalculatorType;
	typename MinMaxCalculatorType::Pointer minMaxCalculator = MinMaxCalculatorType::New();
	if (inputMinimumValue == -1 || inputMaximumValue == -1)
	{
		minMaxCalculator->SetImage(fileReader->GetOutput());
		minMaxCalculator->Compute();
		if (inputMinimumValue == -1)
			inputMinimumValue = minMaxCalculator->GetMinimum();

		if (inputMaximumValue == -1)
			inputMaximumValue = minMaxCalculator->GetMaximum();
	}

	// perform a windowing of the value range to match the 
	typedef itk::IntensityWindowingImageFilter<TReaderType, TInputImage> IntensityWindowingImageFilterType;
	typename IntensityWindowingImageFilterType::Pointer intensityWindowingFilter = IntensityWindowingImageFilterType::New();
	intensityWindowingFilter->SetInput( fileReader->GetOutput() );
	intensityWindowingFilter->SetWindowMinimum( inputMinimumValue );
	intensityWindowingFilter->SetWindowMaximum( inputMaximumValue );

    // set input range
	if (normalizeIntensity == true)
	{
		if( typeid( typename TInputImage::PixelType ) == typeid( float ) || typeid( typename TInputImage::PixelType ) == typeid( double ) )
		{
			// floating point types
			intensityWindowingFilter->SetOutputMinimum( itk::NumericTraits< typename TInputImage::PixelType >::ZeroValue() );
			intensityWindowingFilter->SetOutputMaximum( itk::NumericTraits< typename TInputImage::PixelType >::OneValue() );
		}
		else
		{
			// integer types
			intensityWindowingFilter->SetOutputMinimum( itk::NumericTraits< typename TInputImage::PixelType >::min() );
			intensityWindowingFilter->SetOutputMaximum( itk::NumericTraits< typename TInputImage::PixelType >::max() );
		}
	}
	else
	{
		intensityWindowingFilter->SetOutputMinimum(inputMinimumValue);
		intensityWindowingFilter->SetOutputMaximum(inputMaximumValue);
	}

	// update the intensity conversion
	itkTryCatch( intensityWindowingFilter->Update(), "Exception Caught: ImageReaderWidget -> Update function of the intensity conversion." );

	Logger::GetInstance()->WriteLine( QString("Updating intensity window filter took: ") + QString::number((float)(QDateTime::currentMSecsSinceEpoch() - timer0) / 1000.0) );
	timer0 = QDateTime::currentMSecsSinceEpoch();

    // set the spacing along x,y,z axes
    typename TInputImage::SpacingType spacing;
    for( int i=0; i<TInputImage::ImageDimension; i++ )
        spacing[i] = spacingParameter[i];
	typename TInputImage::Pointer resultImage = intensityWindowingFilter->GetOutput();
    resultImage->SetSpacing( spacingParameter ); // was resultImage before
	
    // store image in output list
    ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( resultImage );
	outputImage->SetRescaleFlag(normalizeIntensity);
    mOutputImages.append( outputImage );

	Logger::GetInstance()->WriteLine( QString("Updating image wrapper took: ") + QString::number((float)(QDateTime::currentMSecsSinceEpoch() - timer0) / 1000.0) );
	timer0 = QDateTime::currentMSecsSinceEpoch();

    // log the performance
    ProcessObjectBase::LogPerformance();

	// update the process object
    ProcessObjectBase::Update();
}

static ProcessObjectProxy< ImageReaderWidget<Image2Float> > ImageReaderWidgetImage2Float;
static ProcessObjectProxy< ImageReaderWidget<Image3Float> > ImageReaderWidgetImage3Float;
static ProcessObjectProxy< ImageReaderWidget<Image2UShort> > ImageReaderWidgetImage2UShort;
static ProcessObjectProxy< ImageReaderWidget<Image3UShort> > ImageReaderWidgetImage3UShort;

} // namespace XPIWIT
