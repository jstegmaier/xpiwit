/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// namespace header
#include "MARSWatershedFromMarkersImageFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkMorphologicalWatershedFromMarkersImageFilter.h"
#include "itkBinaryImageToLabelMapFilter.h"
#include "itkLabelMapToLabelImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkImageRegionIterator.h"
#include "itkLabelGeometryImageFilter.h"
#include "itkIntensityWindowingImageFilter.h"

namespace XPIWIT {

template < class TInputImage >
MARSWatershedFromMarkersImageFilterWidget< TInputImage >::MARSWatershedFromMarkersImageFilterWidget() : ProcessObjectBase()
{
	this->mName = MARSWatershedFromMarkersImageFilterWidget<TInputImage>::GetName();
	this->mDescription = "Calculates the iterative watershed transform from a marker image with volume constraints. The implementation is based on the description published by Fernandez et al. (2010), Nature Methods.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(2);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "MarkWatershedLine", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, watershed lines are highlighted by zero values." );
	processObjectSettings->AddSetting( "MinimumVolume", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "All objects with smaller volume will be iteratively removed and the watershed will be calculated again until convergence." );
	processObjectSettings->AddSetting( "BackgroundThreshold", "1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The largest connected component below this threshold will serve as background label." );
	processObjectSettings->AddSetting( "FullyConnected", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled 8-neighborhood (2D) or 27-neighborhood (3D) is used." );
	processObjectSettings->AddSetting( "UseSizeCriterion", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Use size criterion to suppress small regions." );

	// initialize the widget
	ProcessObjectBase::Init();
}


template < class TInputImage >
MARSWatershedFromMarkersImageFilterWidget< TInputImage >::~MARSWatershedFromMarkersImageFilterWidget()
{
}


template < class TInputImage >
void MARSWatershedFromMarkersImageFilterWidget< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// start processing
	ProcessObjectBase::StartTimer();

	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const int markWatershedLine = processObjectSettings->GetSettingValue( "MarkWatershedLine" ).toInt() > 0;
	const int fullyConnected = processObjectSettings->GetSettingValue( "FullyConnected" ).toInt() > 0;
	const int minimumVolume = processObjectSettings->GetSettingValue( "MinimumVolume" ).toInt();
	const float backgroundThreshold = processObjectSettings->GetSettingValue( "BackgroundThreshold" ).toFloat();
	const bool useSizeCriterion = processObjectSettings->GetSettingValue( "UseSizeCriterion" ).toInt() > 0;

	// get input image and allocate the marker image
	typedef TInputImage LabelImageType;
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	typename TInputImage::Pointer markerImage = mInputImages.at(1)->template GetImage<TInputImage>();
	typename TInputImage::SpacingType spacing = inputImage->GetSpacing();
	Logger::GetInstance()->WriteLine( "- Input images setup properly." );

	// convert the binary marker image to a label map
	typedef typename itk::BinaryImageToLabelMapFilter< TInputImage > BinaryImageToLabelMapFilterType;
	typename BinaryImageToLabelMapFilterType::Pointer binaryImageToLabelMapFilter = BinaryImageToLabelMapFilterType::New();
	binaryImageToLabelMapFilter->SetInput( markerImage );
	binaryImageToLabelMapFilter->SetReleaseDataFlag( true );
	binaryImageToLabelMapFilter->SetInputForegroundValue( 1 );
	binaryImageToLabelMapFilter->SetOutputBackgroundValue( 0 );
	binaryImageToLabelMapFilter->SetFullyConnected( fullyConnected );
	itkTryCatch( binaryImageToLabelMapFilter->Update(), "Error: BinaryImageToLabelMapFilter update.");
 
	// convert the label mapt into a label image
	typedef typename itk::Image< unsigned short, TInputImage::ImageDimension >  LabelType;
	typedef typename itk::LabelMapToLabelImageFilter< typename BinaryImageToLabelMapFilterType::OutputImageType, LabelType > LabelMapToLabelImageFilterType;
	typename LabelMapToLabelImageFilterType::Pointer labelMapToLabelImageFilter = LabelMapToLabelImageFilterType::New();
	labelMapToLabelImageFilter->SetInput( binaryImageToLabelMapFilter->GetOutput() );
	labelMapToLabelImageFilter->SetReleaseDataFlag( true );
	itkTryCatch( labelMapToLabelImageFilter->Update(), "Error: LabelMapToLabelImageFilter update.");
	Logger::GetInstance()->WriteLine( "- Converted binary image to label image." );

	// apply a threshold to the input image to attain the background label
    typedef typename itk::BinaryThresholdImageFilter<TInputImage, TInputImage> BinaryThresholdFilter;
    typename BinaryThresholdFilter::Pointer thresholdFilter = BinaryThresholdFilter::New();
	inputImage->SetReleaseDataFlag( false );
    thresholdFilter->SetInput( inputImage );
    thresholdFilter->SetNumberOfWorkUnits( maxThreads );
    thresholdFilter->SetLowerThreshold( backgroundThreshold );
    thresholdFilter->SetUpperThreshold( 65535 );
    thresholdFilter->SetOutsideValue( 1 );
    thresholdFilter->SetInsideValue( 0 );
    itkTryCatch( thresholdFilter->Update(), "Error: Updating BinaryThresholdImageFilter failed." );
	Logger::GetInstance()->WriteLine( "- Background threshold successfully applied." );
	
	// define the types
	typedef typename itk::BinaryImageToLabelMapFilter< TInputImage > BinaryImageToLabelMapFilterType;
	typedef typename itk::Image< unsigned short, TInputImage::ImageDimension >  LabelType;
	typedef typename itk::LabelMapToLabelImageFilter< typename BinaryImageToLabelMapFilterType::OutputImageType, LabelType > LabelMapToLabelImageFilterType;
	typedef typename itk::LabelGeometryImageFilter<LabelType> LabelGeometryImageFilterType;
	
	// add the background label to the provided seed image
	typename LabelType::Pointer currentMarkerImage = LabelType::New();
	currentMarkerImage->SetRegions( labelMapToLabelImageFilter->GetOutput()->GetLargestPossibleRegion() );
	currentMarkerImage->SetSpacing( labelMapToLabelImageFilter->GetOutput()->GetSpacing() );
	currentMarkerImage->Allocate();
	currentMarkerImage->FillBuffer( 0 );
	currentMarkerImage->SetReleaseDataFlag( false );

	// specify the iterators
	typename itk::ImageRegionIterator< LabelType > markerImageIterator( labelMapToLabelImageFilter->GetOutput(), labelMapToLabelImageFilter->GetOutput()->GetLargestPossibleRegion() );
	typename itk::ImageRegionIterator< LabelType > newMarkerImageIterator( currentMarkerImage, currentMarkerImage->GetLargestPossibleRegion() );
	typename itk::ImageRegionIterator< TInputImage > backgroundImageIterator( thresholdFilter->GetOutput(), thresholdFilter->GetOutput()->GetLargestPossibleRegion() );
	markerImageIterator.GoToBegin();
	backgroundImageIterator.GoToBegin();
	newMarkerImageIterator.GoToBegin();

	while (markerImageIterator.IsAtEnd() == false)
	{
		// set the background label if the value matches the maximum label. Otherwise the original label is used but incremented by 1
		if (backgroundImageIterator.Value() > 0)
		{
			newMarkerImageIterator.Set( 1 );
		}
		else if (markerImageIterator.Value() > 0)
		{
			newMarkerImageIterator.Set( markerImageIterator.Value() + 1 );
		}

		// increment the iterators
		++markerImageIterator;
		++newMarkerImageIterator;
		++backgroundImageIterator;
	}
	Logger::GetInstance()->WriteLine( "- Background marker successfully added to the marker image." );

	// setup the stopping criterion
	bool stoppingCriterion = false;
	typename LabelType::Pointer tmpImage;
	
	// perform the iterative watershed segmentation
	while (stoppingCriterion == false)
	{
		// perform watershed on the provided seed image
		typedef typename itk::MorphologicalWatershedFromMarkersImageFilter<TInputImage, LabelType> WatershedFromMarkersFilterType;
		typename WatershedFromMarkersFilterType::Pointer watershedFromMarkersFilter = WatershedFromMarkersFilterType::New();
		watershedFromMarkersFilter->SetInput1( inputImage );
		watershedFromMarkersFilter->SetInput2( currentMarkerImage );
		watershedFromMarkersFilter->SetFullyConnected( fullyConnected );
		watershedFromMarkersFilter->SetMarkWatershedLine( markWatershedLine );
		watershedFromMarkersFilter->SetReleaseDataFlag( false );
		itkTryCatch(watershedFromMarkersFilter->Update(), "Error: MARSWatershedFromMarkersImageFilterWidget Update Function.");
		Logger::GetInstance()->WriteLine( "- Successfully performed watershed segmentation for the current marker image." );

		if (useSizeCriterion == true)
		{
			// perform region props and check which of the extracted cells fall below the size criterion
			typename LabelGeometryImageFilterType::Pointer labelGeometryFilter2 = LabelGeometryImageFilterType::New();
			labelGeometryFilter2->SetInput( watershedFromMarkersFilter->GetOutput() );
			labelGeometryFilter2->SetCalculateOrientedBoundingBox( false );
			labelGeometryFilter2->SetCalculateOrientedIntensityRegions( false );
			labelGeometryFilter2->SetCalculateOrientedLabelRegions( false );
			labelGeometryFilter2->SetCalculatePixelIndices( false );
			labelGeometryFilter2->SetReleaseDataFlag( true );
			itkTryCatch(labelGeometryFilter2->Update(), "Error: MARSWatershedFromMarkersImageFilterWidget LabelGeometryImageFilter Update Function.");

			// get the labels
			typename LabelGeometryImageFilterType::LabelsType allLabels = labelGeometryFilter2->GetLabels();
			typename LabelGeometryImageFilterType::LabelsType::iterator allLabelsIt;
			QList<typename LabelGeometryImageFilterType::LabelPixelType> deletionList;
			const float currentVolume = 0.0f;

			// iterate trough the labels and extract nucleus information
			for( allLabelsIt = allLabels.begin(); allLabelsIt != allLabels.end(); allLabelsIt++ )
			{
				typename LabelGeometryImageFilterType::LabelPixelType labelValue = *allLabelsIt;
				if (labelGeometryFilter2->GetVolume(labelValue) < minimumVolume && labelGeometryFilter2->GetVolume(labelValue) > 0)
					deletionList.append( labelValue );

				Logger::GetInstance()->WriteLine( QString("- Found region with volume: ") + QString().number(labelGeometryFilter2->GetVolume(labelValue)) + QString( " and label " ) + QString().number( labelValue ) );
			}

			// loop through the seed image and remove the seeds that are too small
			if (deletionList.count() > 0)
			{
				Logger::GetInstance()->WriteLine( QString("- Removing ") + QString().number(deletionList.count()) + QString(" segments that were smaller than the minimum volume.") );

				// set the markers that were too small to zero
				typename itk::ImageRegionIterator< LabelType > markerImageIterator2( currentMarkerImage, currentMarkerImage->GetLargestPossibleRegion() );
				typename itk::ImageRegionIterator< LabelType > watershedImageIterator( watershedFromMarkersFilter->GetOutput(), watershedFromMarkersFilter->GetOutput()->GetLargestPossibleRegion() );
				markerImageIterator2.GoToBegin();
				watershedImageIterator.GoToBegin();
			
				while (markerImageIterator2.IsAtEnd() == false)
				{
					// set the background label if the value matches the maximum label. Otherwise the original label is used but incremented by 1
					if (deletionList.contains(watershedImageIterator.Value()) == true)
						markerImageIterator2.Set( 0 );

					++markerImageIterator2;
					++watershedImageIterator;
				}
			}
			else
			{
				// if no more cell falls below the volume criterion, the algorithm will be stopped
				stoppingCriterion = true;
				Logger::GetInstance()->WriteLine( QString("- Stopping criterion reached, no more segments are smaller than ") + QString().number(minimumVolume) + QString(".") );
			}
		}
		tmpImage = watershedFromMarkersFilter->GetOutput();
	}


	// rescale the watershed result to the internal format
	typedef itk::IntensityWindowingImageFilter< LabelType, TInputImage > RescaleIntensityFilterType;
	typename RescaleIntensityFilterType::Pointer rescaleIntensityFilter = RescaleIntensityFilterType::New();
	rescaleIntensityFilter->SetInput( tmpImage );
	rescaleIntensityFilter->SetWindowMinimum( 0 );
	rescaleIntensityFilter->SetWindowMaximum( 65535 );
	rescaleIntensityFilter->SetOutputMinimum( 0.0 );
	rescaleIntensityFilter->SetOutputMaximum( 1.0 );
	rescaleIntensityFilter->SetNumberOfWorkUnits( maxThreads );
	itkTryCatch( rescaleIntensityFilter->Update(), "Exception Caught: Rescale watershed intensity to [0, 1] range." );

	// set the output image
	ImageWrapper* outputImage = new ImageWrapper();
	outputImage->SetImage<TInputImage>( rescaleIntensityFilter->GetOutput() );
	outputImage->SetRescaleFlag( true );
	mOutputImages.append( outputImage );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< MARSWatershedFromMarkersImageFilterWidget<Image2Float> > MARSWatershedFromMarkersImageFilterWidgetImage2Float;
static ProcessObjectProxy< MARSWatershedFromMarkersImageFilterWidget<Image3Float> > MARSWatershedFromMarkersImageFilterWidgetImage3Float;
static ProcessObjectProxy< MARSWatershedFromMarkersImageFilterWidget<Image2UShort> > MARSWatershedFromMarkersImageFilterWidgetImage2UShort;
static ProcessObjectProxy< MARSWatershedFromMarkersImageFilterWidget<Image3UShort> > MARSWatershedFromMarkersImageFilterWidgetImage3UShort;
    
} // namespace XPIWIT

