/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "MorphologicalClosingFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkGrayscaleMorphologicalClosingImageFilter.h"
#include "itkConstNeighborhoodIterator.h"
#include "itkImageRegionIterator.h"
#include "itkBinaryBallStructuringElement.h"

// XPIWIT namespace
namespace XPIWIT
{

// the default constructor
template< class TInputImage >
MorphologicalClosingFilterWidget<TInputImage>::MorphologicalClosingFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = MorphologicalClosingFilterWidget<TInputImage>::GetName();
    this->mDescription = "Morphological Closing Filter. ";
    this->mDescription += "closes the image using erosion of the dilated input image.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings to the filter
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Radius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Radius of the filter kernel." );
	processObjectSettings->AddSetting( "MinRadius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Minimum radius of the filter kernel (UseRadiusRange has to be enabled for this parameter to work)." );
	processObjectSettings->AddSetting( "MaxRadius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Maximum radius of the filter kernel (UseRadiusRange has to be enabled for this parameter to work)." );
    processObjectSettings->AddSetting( "FilterMask3D", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Use a 3D kernel." );
	processObjectSettings->AddSetting( "UseRadiusRange", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Iteratively apply different radii to close the image (Used e.g. for viscous watershed)." );
	processObjectSettings->AddSetting( "SafeBorder", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Toggles border padding." );
	//processObjectSettings->AddSetting( "Algorithm", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The algorithm to use: BASIC = 0, HISTO = 1, ANCHOR = 2, VHGW = 3." );
	
    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
MorphologicalClosingFilterWidget<TInputImage>::~MorphologicalClosingFilterWidget()
{
}


// update the filter
template< class TInputImage >
void MorphologicalClosingFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int radius = processObjectSettings->GetSettingValue( "Radius" ).toInt();
    const int filterMask3D = processObjectSettings->GetSettingValue( "FilterMask3D" ).toInt() > 0;
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const bool useRadiusRange = processObjectSettings->GetSettingValue( "UseRadiusRange" ).toInt() > 0;
	const bool safeBorder = processObjectSettings->GetSettingValue( "SafeBorder" ).toInt() > 0;
	float minRadius = processObjectSettings->GetSettingValue( "MinRadius" ).toFloat();
	float maxRadius = processObjectSettings->GetSettingValue( "MaxRadius" ).toFloat();
	//const unsigned int algorithm = processObjectSettings->GetSettingValue( "Algorithm" ).toInt();

	// get images
    typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

	// if single radius should be used, set min and max accordingly
	if (useRadiusRange == false)
	{
		minRadius = radius;
		maxRadius = radius;
	}

	// typedefs for the filter and the structure element
    typedef itk::BinaryBallStructuringElement< typename TInputImage::PixelType, TInputImage::ImageDimension > StructuringElementType;
	typedef itk::GrayscaleMorphologicalClosingImageFilter<TInputImage, TInputImage, StructuringElementType> ClosingImageFilterType;
	
	typename TInputImage::SpacingType spacing = inputImage->GetSpacing();
	for (int j=minRadius; j<maxRadius; ++j)
	{
		// create the ball structuring element
		typename TInputImage::SizeType radius;
		radius[0] = int(0.5 + (float)j / spacing[0]);
		radius[1] = int(0.5 + (float)j / spacing[1]);
		radius[2] = int(0.5 + (float)j / spacing[2]);

		if (filterMask3D == false)
			radius[2] = 0;

		StructuringElementType structuringElement;
		structuringElement.SetRadius( radius );
		structuringElement.CreateStructuringElement();

		// create closing filter
		typename ClosingImageFilterType::Pointer closingImageFilter = ClosingImageFilterType::New();
		closingImageFilter->SetReleaseDataFlag( true );
		closingImageFilter->SetKernel( structuringElement );
		closingImageFilter->SetInput( inputImage );
		closingImageFilter->SetNumberOfWorkUnits( maxThreads );
		closingImageFilter->SetSafeBorder( safeBorder );
		//closingImageFilter->SetAlgorithm(algorithm);

		itkTryCatch( closingImageFilter->Update(), "Exception Caught: Updating the morphological closing operator." );
		inputImage = closingImageFilter->GetOutput();
	}
	
	inputImage->SetReleaseDataFlag( false );

    // set the output image
    ImageWrapper *outputWrapper = new ImageWrapper();
    outputWrapper->SetImage<TInputImage>( inputImage );
    mOutputImages.append( outputWrapper );

    // update the process update widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< MorphologicalClosingFilterWidget<Image2Float> > MorphologicalClosingFilterWidgetImage2Float;
static ProcessObjectProxy< MorphologicalClosingFilterWidget<Image3Float> > MorphologicalClosingFilterWidgetImage3Float;
static ProcessObjectProxy< MorphologicalClosingFilterWidget<Image2UShort> > MorphologicalClosingFilterWidgetImage2UShort;
static ProcessObjectProxy< MorphologicalClosingFilterWidget<Image3UShort> > MorphologicalClosingFilterWidgetImage3UShort;
    
} // namespace XPIWIT
