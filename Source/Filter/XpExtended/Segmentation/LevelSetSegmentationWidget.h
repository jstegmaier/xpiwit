/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifndef LEVELSETSEGMENTATIONWIDGET_H
#define LEVELSETSEGMENTATIONWIDGET_H

// namespace header
#include "../../Base/Management/ProcessObjectBase.h"
#include "../../Base/Management/ProcessObjectProxy.h"


namespace XPIWIT
{

/**
 *	@class LevelSetSegmentationWidget
 *	The base class for image to image filter widgets.
 */

template< class TInputImage >
class LevelSetSegmentationWidget : public ProcessObjectBase
{
    public:

        /**
         * The constructor.
         * @param parent The parent of the main window. Usually should be NULL.
         */
        LevelSetSegmentationWidget();


        /**
         * The destructor.
         */
        virtual ~LevelSetSegmentationWidget();


        /**
         * The update function.
         */
        void Update();


		/**
		 * Static functions used for the filter factory.
		 */
		static QString GetName() { return "LevelSetSegmentation"; }
		static QString GetType() { return (typeid(float) == typeid(typename TInputImage::PixelType)) ? "float" : "ushort"; }
		static int GetDimension() { return TInputImage::ImageDimension; }

};

} // namespace XPIWIT

//#include "LevelSetSegmentationWidget.txx"

#endif
