/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "TwangSegmentationWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"
#include "../../ITKCustom/itkSeedBasedTwangSegmentationFilter.h"


namespace XPIWIT
{

// the default constructor
template< class TInputImage >
TwangSegmentationWidget<TInputImage>::TwangSegmentationWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = TwangSegmentationWidget<TInputImage>::GetName();
	this->mDescription = "Applys the TWANG segmentation method on the supplied image as described by Stegmaier et al. Requires seed points as meta information.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(1);
	this->mObjectType->AppendMetaInputType("KeyPoints");
    this->mObjectType->SetNumberMetaOutputs(1);
	this->mObjectType->AppendMetaOutputType("RegionProps");
	
    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "MinimumRegionSigma", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The minimum std. dev. within a cropped image region. Ignored in the current implementation." );
    processObjectSettings->AddSetting( "Segment3D", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Used to perform the segmentation either directly in 3D or to merge 2D segmentation results instead." );
    processObjectSettings->AddSetting( "LabelOutput", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If true, the filter directly produces a labeled output image with a unique id for each detected blob." );
	processObjectSettings->AddSetting( "UseOriginalID", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If true, the filter uses the same labels as provided by the seed detection.");
    processObjectSettings->AddSetting( "RandomLabels", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If checked, random labels are used. Note that random labels might not be unique." );
    processObjectSettings->AddSetting( "WriteRegionProps", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If checked, the region props of extracted blobs are exported to a cvs file." );
    processObjectSettings->AddSetting( "MinimumWeightedGradientNormalDotProduct", "0.6", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Used to threshold the weighted dot product image." );
    processObjectSettings->AddSetting( "WeightingKernelSizeMultiplicator", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Used to scale the plateau region of the weighting kernel. If set to 1 the seed radius is used for the plateau radius." );
    processObjectSettings->AddSetting( "WeightingKernelStdDev", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Multiplier for the weighting kernel standard deviation." );
    processObjectSettings->AddSetting( "GradientImageStdDev", "1.5", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The standard deviation of the Gaussian smoothing for smoother gradient directions." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
TwangSegmentationWidget<TInputImage>::~TwangSegmentationWidget()
{
}


// the update function
template< class TInputImage >
void TwangSegmentationWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	this->mMetaOutputs.at(0)->mIsMultiDimensional = true;

    // get filter parameters
    float minimumRegionSigma = processObjectSettings->GetSettingValue( "MinimumRegionSigma" ).toDouble();
    float minimumWeightedGradientNormalDotProduct = processObjectSettings->GetSettingValue( "MinimumWeightedGradientNormalDotProduct" ).toDouble();
    float weightingKernelSizeMultiplicator = processObjectSettings->GetSettingValue( "WeightingKernelSizeMultiplicator" ).toDouble();
    float weightingKernelStdDev = processObjectSettings->GetSettingValue( "WeightingKernelStdDev" ).toDouble();
    float gradientImageStdDev = processObjectSettings->GetSettingValue( "GradientImageStdDev" ).toDouble();
    bool segment3D = processObjectSettings->GetSettingValue( "Segment3D" ).toInt() > 0;
    bool labelOutput = processObjectSettings->GetSettingValue( "LabelOutput" ).toInt() > 0;
	bool useOriginalID = processObjectSettings->GetSettingValue( "UseOriginalID" ).toInt() > 0;
    bool randomLabels = processObjectSettings->GetSettingValue( "RandomLabels" ).toInt() > 0;
    bool writeRegionProps = processObjectSettings->GetSettingValue( "WriteRegionProps" ).toInt() > 0;
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();

    // setup the filters
    typedef itk::SeedBasedTwangSegmentationFilter<TInputImage> SeedBasedTwangSegmentationFilterType;
    typename itk::SeedBasedTwangSegmentationFilter<TInputImage>::Pointer twangSegmentationFilter = itk::SeedBasedTwangSegmentationFilter<TInputImage>::New();

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

    // initialize level sets segmentation
    twangSegmentationFilter->SetInput( inputImage );
    twangSegmentationFilter->SetSegment3D( segment3D );
    twangSegmentationFilter->SetNumberOfWorkUnits( maxThreads );
    twangSegmentationFilter->SetLabelOutput( labelOutput );
    twangSegmentationFilter->SetMinimumRegionSigma( minimumRegionSigma );
    twangSegmentationFilter->SetMinimumWeightedGradientNormalDotProduct( minimumWeightedGradientNormalDotProduct );
    twangSegmentationFilter->SetWeightingKernelSizeMultiplicator( weightingKernelSizeMultiplicator );
    twangSegmentationFilter->SetWeightingKernelStdDev( weightingKernelStdDev );
    twangSegmentationFilter->SetGradientImageStdDev( gradientImageStdDev );
    twangSegmentationFilter->SetWriteRegionProps( writeRegionProps );
    twangSegmentationFilter->SetRandomLabels( randomLabels );
	twangSegmentationFilter->SetUseOriginalID( useOriginalID );
    twangSegmentationFilter->SetInputMetaFilter( this->mMetaInputs.at(0) );
    twangSegmentationFilter->SetOutputMetaFilter( this->mMetaOutputs.at(0) );

    // perform and set the output
    itkTryCatch( twangSegmentationFilter->Update(), "Twang Segmentation Called in TwangSegmentationWidget." );
    
	ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( twangSegmentationFilter->GetOutput() );
	outputImage->SetRescaleFlag( false );
    mOutputImages.append( outputImage );

    // update the base class
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< TwangSegmentationWidget<Image2Float> > TwangSegmentationWidgetImage2Float;
static ProcessObjectProxy< TwangSegmentationWidget<Image3Float> > TwangSegmentationWidgetImage3Float;
static ProcessObjectProxy< TwangSegmentationWidget<Image2UShort> > TwangSegmentationWidgetImage2UShort;
static ProcessObjectProxy< TwangSegmentationWidget<Image3UShort> > TwangSegmentationWidgetImage3UShort;

} // namespace XPIWIT
