/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "EdgeMapFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkGradientMagnitudeRecursiveGaussianImageFilter.h"
#include "itkBoundedReciprocalImageFilter.h"
#include "itkMultiplyImageFilter.h"
#include "itkAddImageFilter.h"
#include "itkGradientMagnitudeImageFilter.h"
#include "itkCurvatureAnisotropicDiffusionImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkRescaleIntensityImageFilter.h"


namespace XPIWIT
{

// the default constructor
template <class TInputImage>
EdgeMapFilterWidget<TInputImage>::EdgeMapFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = EdgeMapFilterWidget<TInputImage>::GetName();
    this->mDescription = "Transforms an intensity image into a edge map for level sets segmentation approaches. Edges are transformed to low intensity values, whereas plain areas appear bright.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Sigma", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Std. Dev. for the gradient magnitude filtering." );
    processObjectSettings->AddSetting( "EdgeMapFactor", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Gradient magnitude filter multiplicator to adjust the intensity range of the final edge map." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template <class TInputImage>
EdgeMapFilterWidget<TInputImage>::~EdgeMapFilterWidget()
{
}


// the update function
template <class TInputImage>
void EdgeMapFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    const float sigma = processObjectSettings->GetSettingValue( "Sigma" ).toDouble();
    const float edgeMapFactor = processObjectSettings->GetSettingValue( "EdgeMapFactor" ).toDouble();
    const bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    const bool inPlace = processObjectSettings->GetSettingValue( "InPlace" ).toInt() > 0;

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

    //typename itk::CurvatureAnisotropicDiffusionImageFilter<TInputImage, TInputImage>::Pointer anisotropicDiffusionFilter = itk::CurvatureAnisotropicDiffusionImageFilter<TInputImage, TInputImage>::New();
    typename itk::GradientMagnitudeRecursiveGaussianImageFilter<TInputImage>::Pointer gradientMagnitudeFilter = itk::GradientMagnitudeRecursiveGaussianImageFilter<TInputImage>::New();
    typename itk::MultiplyImageFilter<TInputImage, TInputImage, TInputImage>::Pointer constantMultiplyFilter = itk::MultiplyImageFilter<TInputImage, TInputImage, TInputImage>::New();
	typename itk::AddImageFilter<TInputImage, TInputImage, TInputImage>::Pointer constantAddFilter = itk::AddImageFilter<TInputImage, TInputImage, TInputImage>::New();
	typename itk::RescaleIntensityImageFilter<TInputImage, TInputImage>::Pointer rescaleIntensityFilter = itk::RescaleIntensityImageFilter<TInputImage, TInputImage>::New();
    typename itk::BoundedReciprocalImageFilter<TInputImage, TInputImage>::Pointer boundedReciprocalImageFilter = itk::BoundedReciprocalImageFilter<TInputImage, TInputImage>::New();

	/*
    anisotropicDiffusionFilter->SetUseImageSpacing( true );
    anisotropicDiffusionFilter->SetInput( inputImage );
    anisotropicDiffusionFilter->SetReleaseDataFlag( false );
    anisotropicDiffusionFilter->SetNumberOfWorkUnits( maxThreads );
	*/

    // set filter properties
    gradientMagnitudeFilter->SetInput( inputImage );
    gradientMagnitudeFilter->SetSigma( sigma );
    gradientMagnitudeFilter->SetInPlace( inPlace );
    gradientMagnitudeFilter->SetReleaseDataFlag( releaseDataFlag );
    gradientMagnitudeFilter->SetNumberOfWorkUnits( maxThreads );

    constantMultiplyFilter->SetInput( gradientMagnitudeFilter->GetOutput() );
    constantMultiplyFilter->SetConstant( edgeMapFactor );
    constantMultiplyFilter->SetInPlace( inPlace );
    constantMultiplyFilter->SetNumberOfWorkUnits( maxThreads );
    constantMultiplyFilter->SetReleaseDataFlag( releaseDataFlag );

	constantAddFilter->SetInput1( constantMultiplyFilter->GetOutput() );
	constantAddFilter->SetConstant2( 1.0 );
	constantAddFilter->SetInPlace( inPlace );
	constantAddFilter->SetReleaseDataFlag( true );

    boundedReciprocalImageFilter->SetInput( constantAddFilter->GetOutput() );
    boundedReciprocalImageFilter->SetInPlace( inPlace );
    boundedReciprocalImageFilter->SetNumberOfWorkUnits( maxThreads );
    boundedReciprocalImageFilter->SetReleaseDataFlag( releaseDataFlag );

	rescaleIntensityFilter->SetInput( boundedReciprocalImageFilter->GetOutput() );
	rescaleIntensityFilter->SetOutputMinimum( 0.0 );
	rescaleIntensityFilter->SetOutputMaximum( 1.0 );

    // update the filter
    itkTryCatch( rescaleIntensityFilter->Update(), "EdgeMapFilter --> Bounded Reciprocal Filter update." );

    // set the output
	ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( rescaleIntensityFilter->GetOutput() );
    mOutputImages.append( outputImage );

    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< EdgeMapFilterWidget<Image2Float> > EdgeMapFilterWidgetImage2Float;
static ProcessObjectProxy< EdgeMapFilterWidget<Image3Float> > EdgeMapFilterWidgetImage3Float;
static ProcessObjectProxy< EdgeMapFilterWidget<Image2UShort> > EdgeMapFilterWidgetImage2UShort;
static ProcessObjectProxy< EdgeMapFilterWidget<Image3UShort> > EdgeMapFilterWidgetImage3UShort;

} // namespace XPIWIT
