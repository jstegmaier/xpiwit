/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "WatershedImageFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkWatershedImageFilter.h"
#include "itkGradientMagnitudeRecursiveGaussianImageFilter.h"
#include "itkCastImageFilter.h"


namespace XPIWIT
{

// the default constructor
template <class TInputImage>
WatershedImageFilterWidget<TInputImage>::WatershedImageFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = WatershedImageFilterWidget<TInputImage>::GetName();
    this->mDescription = "Calculates the watershed segmentation over the input image.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "Level", "0.15", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Set the start level. Units are a percentage of the maximum depth in the image." );
    processObjectSettings->AddSetting( "Threshold", "0.03", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Set the threshold. Units are a percentage of the maximum depth in the image." );
	processObjectSettings->AddSetting( "Sigma", "1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Set the Sigma value for the Gradient Magnitude Gauss Filter." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template <class TInputImage>
WatershedImageFilterWidget<TInputImage>::~WatershedImageFilterWidget()
{
}


// the update function
template <class TInputImage>
void WatershedImageFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	float threshold = processObjectSettings->GetSettingValue( "Threshold" ).toFloat();
	float level = processObjectSettings->GetSettingValue( "Level" ).toFloat();
	float sigma = processObjectSettings->GetSettingValue( "Sigma" ).toFloat();

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

	// Instantiate the GradientMagnitude image filter
	typedef itk::GradientMagnitudeRecursiveGaussianImageFilter< TInputImage, TInputImage > GradientMagnitudeFilterType;
	typename GradientMagnitudeFilterType::Pointer gradienMagnitudeFilter = GradientMagnitudeFilterType::New();
	gradienMagnitudeFilter->SetInput( inputImage );
	gradienMagnitudeFilter->SetSigma( sigma );

	// Instantiate the Watershed filter
	typedef itk::WatershedImageFilter< TInputImage > WatershedFilterType;
	typename WatershedFilterType::Pointer watershedFilter = WatershedFilterType::New();
	watershedFilter->SetInput( gradienMagnitudeFilter->GetOutput() );
	watershedFilter->SetThreshold( threshold );
	watershedFilter->SetLevel( level );

	try
	{
		watershedFilter->Update();
	}
	catch( itk::ExceptionObject & excep )
	{
		std::cerr << "Exception caught !" << std::endl;
		std::cerr << excep << std::endl;
	}

	typedef itk::CastImageFilter< itk::Image< itk::IdentifierType, TInputImage::ImageDimension >, TInputImage > CastFilterType;
	typename CastFilterType::Pointer castFilter = CastFilterType::New();
	castFilter->SetInput(watershedFilter->GetOutput());

	ImageWrapper *outputImage = new ImageWrapper();
	outputImage->SetImage< TInputImage >( castFilter->GetOutput() );
    mOutputImages.append( outputImage );

    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< WatershedImageFilterWidget<Image2Float> > WatershedImageFilterWidgetImage2Float;
static ProcessObjectProxy< WatershedImageFilterWidget<Image3Float> > WatershedImageFilterWidgetImage3Float;
static ProcessObjectProxy< WatershedImageFilterWidget<Image2UShort> > WatershedImageFilterWidgetImage2UShort;
static ProcessObjectProxy< WatershedImageFilterWidget<Image3UShort> > WatershedImageFilterWidgetImage3UShort;

} // namespace XPIWIT
