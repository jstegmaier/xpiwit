/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "ParallelSeededWatershedSegmentationWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"
#include "../../ITKCustom/itkSeedBasedWatershedSegmentationFilter.h"


namespace XPIWIT
{

// the default constructor
template< class TInputImage >
ParallelSeededWatershedSegmentationWidget<TInputImage>::ParallelSeededWatershedSegmentationWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = ParallelSeededWatershedSegmentationWidget<TInputImage>::GetName();
	this->mDescription = "Applys the TWANG segmentation method on the supplied image as described by Stegmaier et al. Requires seed points as meta information.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(1);
	this->mObjectType->AppendMetaInputType("KeyPoints");
    this->mObjectType->SetNumberMetaOutputs(1);
	this->mObjectType->AppendMetaOutputType("RegionProps");
	
    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "LabelOutput", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If true, the filter directly produces a labeled output image with a unique id for each detected blob." );
	processObjectSettings->AddSetting( "UseOriginalID", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If true, the filter directly uses the seed ids as labels instead of sequential seed numbers.");
    processObjectSettings->AddSetting( "RandomLabels", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If checked, random labels are used. Note that random labels might not be unique." );
    processObjectSettings->AddSetting( "WriteRegionProps", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If checked, the region props of extracted blobs are exported to a cvs file." );
	processObjectSettings->AddSetting( "InvertIntensityImage", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If checked, the intensity image is inverted (e.g. set to 1 for nuclei and to 0 for membranes." );
	processObjectSettings->AddSetting( "UseRegionPropsAABB", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If checked, the aabb dimensions of the regionprops are used for size estimation. Note: this requires regionprops meta input.");
	processObjectSettings->AddSetting( "MarkWatershedLine", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If checked, watershed lines of split objects are enabled (enable for visualization, disable for exact region measurements).");
	processObjectSettings->AddSetting( "UseRegionGrowing", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If checked, region growing from the center seed is performed. Otherwise, a constrained Otsu threshold is used.");
	processObjectSettings->AddSetting( "SeedRadiusMultiplier", "3", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The seed radius is multiplied by this factor to determine the final ROI radius for the segmentation." );
	processObjectSettings->AddSetting( "SeedDilationRadius", "2", ProcessObjectSetting::SETTINGVALUETYPE_INT, "If larger than 0, seeds are dilated using a spherical structuring element of the specified radius. Use if cells appear erroneously split.");
	
    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
ParallelSeededWatershedSegmentationWidget<TInputImage>::~ParallelSeededWatershedSegmentationWidget()
{
}


// the update function
template< class TInputImage >
void ParallelSeededWatershedSegmentationWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	this->mMetaOutputs.at(0)->mIsMultiDimensional = true;

    // get filter parameters
    bool labelOutput = processObjectSettings->GetSettingValue( "LabelOutput" ).toInt() > 0;
	bool useOriginalIDs = processObjectSettings->GetSettingValue("UseOriginalID").toInt() > 0;
    bool randomLabels = processObjectSettings->GetSettingValue( "RandomLabels" ).toInt() > 0;
	bool invertIntensityImage = processObjectSettings->GetSettingValue( "InvertIntensityImage" ).toInt() > 0;
    bool writeRegionProps = processObjectSettings->GetSettingValue( "WriteRegionProps" ).toInt() > 0;
	bool useRegionPropsAABB = processObjectSettings->GetSettingValue("UseRegionPropsAABB").toInt() > 0;
	bool markWatershedLine = processObjectSettings->GetSettingValue("MarkWatershedLine").toInt() > 0;
	bool useRegionGrowing = processObjectSettings->GetSettingValue("UseRegionGrowing").toInt() > 0;
	const int seedDilationRadius = std::max<int>(0, processObjectSettings->GetSettingValue("SeedDilationRadius").toInt());
	const int maxThreads = processObjectSettings->GetSettingValue("MaxThreads").toInt();
	float seedRadiusMultiplier = processObjectSettings->GetSettingValue( "SeedRadiusMultiplier" ).toFloat();

    // setup the filters
    typedef itk::SeedBasedWatershedSegmentationFilter<TInputImage> SeedBasedWatershedSegmentationFilterType;
    typename SeedBasedWatershedSegmentationFilterType::Pointer watershedSegmentationFilter = SeedBasedWatershedSegmentationFilterType::New();

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

    // initialize level sets segmentation
    watershedSegmentationFilter->SetInput( inputImage );
    watershedSegmentationFilter->SetNumberOfWorkUnits( maxThreads );
    watershedSegmentationFilter->SetLabelOutput( labelOutput );
	watershedSegmentationFilter->SetUseOriginalID( useOriginalIDs );
    watershedSegmentationFilter->SetWriteRegionProps( writeRegionProps );
    watershedSegmentationFilter->SetRandomLabels( randomLabels );
	watershedSegmentationFilter->SetUseRegionPropsAABB( useRegionPropsAABB );
	watershedSegmentationFilter->SetSeedRadiusMultiplier( seedRadiusMultiplier );
	watershedSegmentationFilter->SetInvertIntensityImage( invertIntensityImage );
	watershedSegmentationFilter->SetMarkWatershedLine(markWatershedLine);
	watershedSegmentationFilter->SetUseRegionGrowing(useRegionGrowing);
	watershedSegmentationFilter->SetSeedDilationRadius(seedDilationRadius);
    watershedSegmentationFilter->SetInputMetaFilter( this->mMetaInputs.at(0) );
    watershedSegmentationFilter->SetOutputMetaFilter( this->mMetaOutputs.at(0) );
	watershedSegmentationFilter->SetReleaseDataFlag(false);

    // perform and set the output
    itkTryCatch( watershedSegmentationFilter->Update(), "Twang Segmentation Called in ParallelSeededWatershedSegmentationWidget." );
    
	ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( watershedSegmentationFilter->GetOutput() );
	outputImage->SetRescaleFlag( false );
    mOutputImages.append( outputImage );

    // update the base class
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< ParallelSeededWatershedSegmentationWidget<Image2Float> > ParallelSeededWatershedSegmentationWidgetImage2Float;
static ProcessObjectProxy< ParallelSeededWatershedSegmentationWidget<Image3Float> > ParallelSeededWatershedSegmentationWidgetImage3Float;
static ProcessObjectProxy< ParallelSeededWatershedSegmentationWidget<Image2UShort> > ParallelSeededWatershedSegmentationWidgetImage2UShort;
static ProcessObjectProxy< ParallelSeededWatershedSegmentationWidget<Image3UShort> > ParallelSeededWatershedSegmentationWidgetImage3UShort;

} // namespace XPIWIT
