/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. Hübner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. Hübner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "GradientVectorFlowTrackingImageFilterWidget.h"
#include "../../../Core/Utilities/Logger.h"
#include "../../../Filter/Base/Management/ImageWrapper.h"

// itk header
#include "itkBinaryBallStructuringElement.h"
#include "itkBinaryMorphologicalClosingImageFilter.h"
#include "itkBinaryImageToLabelMapFilter.h"
#include "itkLabelMapToLabelImageFilter.h"
#include "../../ITKCustom/itkGradientVectorFlowTrackingImageFilter.h"

// XPIWIT namespace
namespace XPIWIT 
{

// the default constructor
template <class TInputImage>
GradientVectorFlowTrackingImageFilterWidget<TInputImage>::GradientVectorFlowTrackingImageFilterWidget() : ProcessObjectBase()
{
    // set the widget type
	this->mName = GradientVectorFlowTrackingImageFilterWidget<TInputImage>::GetName();
    this->mDescription = "Two or three channel filter that traces the gradient flow.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(4);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);
    
    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "NumIterations", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The number of iterations (should be slightly larger than the radius of the biggest object)." );
	processObjectSettings->AddSetting( "IgnoreBackground", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, background pixels are ignored." );
    processObjectSettings->AddSetting( "FullyConnected", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, full neighborhood is used, otherwise only 4 / 18 connectivity." );
    processObjectSettings->AddSetting( "ClosingRadius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Radius of the structuring element to be used to fuse nearby sinks." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template <class TInputImage>
GradientVectorFlowTrackingImageFilterWidget<TInputImage>::~GradientVectorFlowTrackingImageFilterWidget()
{
}


// the update function
template <class TInputImage>
void GradientVectorFlowTrackingImageFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    const bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;
	const int numIterations = processObjectSettings->GetSettingValue( "NumIterations" ).toInt();
    const int closingRadius = processObjectSettings->GetSettingValue( "ClosingRadius" ).toInt();
    const bool fullyConnected = processObjectSettings->GetSettingValue( "FullyConnected" ).toInt() > 0;
    const bool ignoreBackground = processObjectSettings->GetSettingValue( "IgnoreBackground" ).toInt() > 0;
    
    // get the input images
	typename TInputImage::Pointer flowXImage = mInputImages.at(0)->template GetImage<TInputImage>();
	typename TInputImage::Pointer flowYImage = mInputImages.at(1)->template GetImage<TInputImage>();
	typename TInputImage::Pointer flowZImage = mInputImages.at(2)->template GetImage<TInputImage>();
	typename TInputImage::Pointer maskImage = mInputImages.at(3)->template GetImage<TInputImage>();
        
    typedef itk::GradientVectorFlowTrackingImageFilter<TInputImage, TInputImage> GradientVectorFlowTrackingImageFilterType;
    typename GradientVectorFlowTrackingImageFilterType::Pointer gradientVectorFlowTrackingFilter = GradientVectorFlowTrackingImageFilterType::New();
    gradientVectorFlowTrackingFilter->SetInput(maskImage);
    gradientVectorFlowTrackingFilter->SetFlowXImage(flowXImage);
    gradientVectorFlowTrackingFilter->SetFlowYImage(flowYImage);
    gradientVectorFlowTrackingFilter->SetFlowZImage(flowZImage);
    gradientVectorFlowTrackingFilter->SetMaskImage(maskImage);
    gradientVectorFlowTrackingFilter->SetNumIterations(numIterations);
    gradientVectorFlowTrackingFilter->SetClosingRadius(closingRadius);
    gradientVectorFlowTrackingFilter->SetFullyConnected(fullyConnected);
    gradientVectorFlowTrackingFilter->SetIgnoreBackground(ignoreBackground);
    itkTryCatch(gradientVectorFlowTrackingFilter->Update(), "ERROR: itkGradientVectorFlowTrackingImageFilter Update Function!");
    
    /*
    // define additional image types
    typedef itk::Image< unsigned short, TInputImage::ImageDimension >  LabelImageType;
    typedef itk::Image<typename TInputImage::IndexType, TInputImage::ImageDimension> IndexImageType;

    
    // initialize the seed image for highlighting the sinks
    typename LabelImageType::Pointer seedImage = LabelImageType::New();
    seedImage->SetBufferedRegion( largestPossibleRegion );
    seedImage->Allocate();
    seedImage->FillBuffer(0);
    seedImage->SetReleaseDataFlag( true );
        
    typename IndexImageType::Pointer indexImage = IndexImageType::New();
    indexImage->SetLargestPossibleRegion( largestPossibleRegion );
    indexImage->SetBufferedRegion( largestPossibleRegion );
    indexImage->Allocate();
    indexImage->SetReleaseDataFlag( true );
    
    // initialize the index image with the actual voxel index
    itk::ImageRegionIterator<IndexImageType> indexImageIterator(indexImage, largestPossibleRegion);
    indexImageIterator.GoToBegin();
    
    while (!indexImageIterator.IsAtEnd())
    {
        indexImageIterator.Set(indexImageIterator.GetIndex());
        ++indexImageIterator;
    }
    
    // create the mask image iterators
    itk::ImageRegionIterator<TInputImage> maskIterator(maskImage, largestPossibleRegion);
    
    // auxiliary variables for computing the next indices
    typename TInputImage::IndexType currentIndex;
    typename TInputImage::IndexType nextIndex;
    float gradientX = 0;
    float gradientY = 0;
    float gradientZ = 0;
    
    // propagate indices along the gradient field for a predefined number of iterations
    // number of iterations should be slightly larger than the maximum expected cell radius
    for (int i=0; i<numIterations; ++i)
    {
        // reset iterators
        maskIterator.GoToBegin();
        indexImageIterator.GoToBegin();
                
        // perform one step of index propagation
        while (!indexImageIterator.IsAtEnd())
        {
            // only process foreground voxels if enabled
            if (maskIterator.Value() > 0 || ignoreBackground == false)
            {
                // get the moved index at the current iterator location
                // as well as the gradient at the moved location
                currentIndex = indexImageIterator.Get();
                gradientX = flowXImage->GetPixel(currentIndex);
                gradientY = flowYImage->GetPixel(currentIndex);
                gradientZ = flowZImage->GetPixel(currentIndex);
                
                // convert float gradient to sign (-1,0,1) for the step
                gradientX = (gradientX > 0) ? 1 : ((gradientX < 0) ? -1 : 0);
                gradientY = (gradientY > 0) ? 1 : ((gradientY < 0) ? -1 : 0);
                gradientZ = (gradientZ > 0) ? 1 : ((gradientZ < 0) ? -1 : 0);
                
                // compute next moved index by going one step towards the sink
                nextIndex[0] = currentIndex[0] - gradientX;
                nextIndex[1] = currentIndex[1] - gradientY;
                if (TInputImage::ImageDimension > 2)
                    nextIndex[2] = currentIndex[2] - gradientZ;
                
                // update the current iterator position to the new moved index
                indexImageIterator.Set(nextIndex);

                // set sink voxels in the last iteration
                if (i >= (numIterations-1))
                    seedImage->SetPixel(nextIndex, 1);
            }
            
            // increment iterators
            ++maskIterator;
            ++indexImageIterator;
        }
    }
    
    // convert seed image to a label map
    // create kernel
    typedef itk::BinaryBallStructuringElement< typename TInputImage::PixelType, TInputImage::ImageDimension> StructuringElementType;
    StructuringElementType structuringElement;
    structuringElement.SetRadius( closingRadius );
    structuringElement.CreateStructuringElement();

    // setup the filter
    typedef itk::BinaryMorphologicalClosingImageFilter<LabelImageType, LabelImageType, StructuringElementType> FilterType;
    typename FilterType::Pointer closingFilter = FilterType::New();
    closingFilter->SetInput( seedImage );
    closingFilter->SetKernel( structuringElement );
    closingFilter->SetForegroundValue( 1 );
    closingFilter->SetReleaseDataFlag( true );
    
    itkTryCatch(closingFilter->Update(), "Error: BinaryMorphologicalClosingImageFilterWrapper Update Function.");
    
    // setup the filter
    typedef itk::BinaryImageToLabelMapFilter< LabelImageType > BinaryImageToLabelMapFilterType;
    typename BinaryImageToLabelMapFilterType::Pointer binaryImageToLabelMapFilter = BinaryImageToLabelMapFilterType::New();
    binaryImageToLabelMapFilter->SetInput( closingFilter->GetOutput() );
    binaryImageToLabelMapFilter->SetReleaseDataFlag( true );
    binaryImageToLabelMapFilter->SetInputForegroundValue( 1 );
    binaryImageToLabelMapFilter->SetOutputBackgroundValue( 0 );
    binaryImageToLabelMapFilter->SetFullyConnected( fullyConnected );
    itkTryCatch( binaryImageToLabelMapFilter->Update(), "Error: BinaryImageToLabelMapFilter update.");
 
    // convert the label map into a label image
    typedef itk::LabelMapToLabelImageFilter< typename BinaryImageToLabelMapFilterType::OutputImageType, LabelImageType > LabelMapToLabelImageFilterType;
    typename LabelMapToLabelImageFilterType::Pointer labelMapToLabelImageFilter = LabelMapToLabelImageFilterType::New();
    labelMapToLabelImageFilter->SetInput( binaryImageToLabelMapFilter->GetOutput() );
    labelMapToLabelImageFilter->SetReleaseDataFlag( true );
    itkTryCatch( labelMapToLabelImageFilter->Update(), "Error: LabelMapToLabelImageFilter update.");
    typename LabelImageType::Pointer labelImage = labelMapToLabelImageFilter->GetOutput();
    
    // allocate result image
    typename TInputImage::Pointer resultImage = TInputImage::New();
    resultImage->SetBufferedRegion( largestPossibleRegion );
    resultImage->Allocate();
    resultImage->FillBuffer(0);
    resultImage->SetReleaseDataFlag( false );
    
    // initialize result image iterator
    itk::ImageRegionIterator<TInputImage> resultImageIterator(resultImage, largestPossibleRegion);
    indexImageIterator.GoToBegin();
    resultImageIterator.GoToBegin();
    maskIterator.GoToBegin();
    
    // create the final label image
    while (!resultImageIterator.IsAtEnd())
    {
        // propagate the seed label to all voxels that map to the same sink
        if (maskIterator.Value() > 0 || !ignoreBackground)
            resultImageIterator.Set(labelImage->GetPixel(indexImageIterator.Value()));
        
        // increment iterators
        ++maskIterator;
        ++indexImageIterator;
        ++resultImageIterator;
    }
    */
    
	// save output
	ImageWrapper *outputWrapper = new ImageWrapper();
    outputWrapper->SetImage<TInputImage>( gradientVectorFlowTrackingFilter->GetOutput() );
    outputWrapper->SetRescaleFlag(false);
    mOutputImages.append( outputWrapper );

    // update the base class update function
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
// Note: If your filter only supports one or the other data type, remove the respective lines
static ProcessObjectProxy< GradientVectorFlowTrackingImageFilterWidget<Image2Float> > GradientVectorFlowTrackingImageFilterWidgetImage2Float;
static ProcessObjectProxy< GradientVectorFlowTrackingImageFilterWidget<Image3Float> > GradientVectorFlowTrackingImageFilterWidgetImage3Float;
static ProcessObjectProxy< GradientVectorFlowTrackingImageFilterWidget<Image2UShort> > GradientVectorFlowTrackingImageFilterWidgetImage2UShort;
static ProcessObjectProxy< GradientVectorFlowTrackingImageFilterWidget<Image3UShort> > GradientVectorFlowTrackingImageFilterWidgetImage3UShort;

} // namespace XPIWIT
