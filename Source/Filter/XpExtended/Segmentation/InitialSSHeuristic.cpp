	/*
	///////////////////// SINGLE SLICE FUSION HEURISTIC /////////////////////
	if (singleSliceHeuristic == true)
	{
		// determine segments with less than minSlices slices
		const unsigned int numSortedIntersections = sortedIntersections.length();
		QHash<unsigned int, QList< QList<float> > > labelHashMap;

		// iterate over all segments and count number of slices
		for (int i=0; i<numSlices; ++i)
		{
			const unsigned int numSegments = segmentFeatures[i].length();
			for (int j=0; j<numSegments; ++j)
			{
				if (!labelHashMap.contains(segmentFeatures[i][j][labelIndex]))
					labelHashMap[segmentFeatures[i][j][labelIndex]] = QList< QList<float> >();

				labelHashMap[segmentFeatures[i][j][labelIndex]].append( segmentFeatures[i][j] );
			}			
		}

		// iterate over all labels and merge segments that are smaller than minSlices
		QHash<unsigned int, QList< QList<float> > >::const_iterator labelIterator = labelHashMap.constBegin();
		while (labelIterator != labelHashMap.constEnd())
		{
			if (labelIterator.value().length() <= minSlices)
			{
				// get the current label
				unsigned int newLabel = 0;
				const unsigned int currentLabel = labelIterator.key();

				// find the start and end positions
				QList<float> startSegment = labelIterator.value().first();
				QList<float> endSegment = labelIterator.value().last();

				// declare the closest match variable
				QList<float> currentIntersection;
				QList<float> closestSegment;

				// find the most likely matches for the start segment and the end segment
				// as the list is sorted, if any match is found, this should be the most similar one
				for (int i=0; i<numSortedIntersections; ++i)
				{
					currentIntersection = sortedIntersections[i].first;
					unsigned int currentSlice1Index = currentIntersection[slice1Index];
					unsigned int currentSlice2Index = currentIntersection[slice2Index];
					unsigned int currentLabel1Index = currentIntersection[label1Index];
					unsigned int currentLabel2Index = currentIntersection[label2Index];
					unsigned int currentSegment1Label = segmentFeatures[currentSlice1Index][currentLabel1Index-minimumLabels[currentSlice1Index]][labelIndex];
					unsigned int currentSegment2Label = segmentFeatures[currentSlice2Index][currentLabel2Index-minimumLabels[currentSlice2Index]][labelIndex];

					// check if the first segment of the intersection matches
					if ((startSegment[idIndex] == currentLabel1Index && startSegment[sliceIndex] == currentSlice1Index && startSegment[labelIndex] != currentSegment2Label) ||
						(endSegment[idIndex] == currentLabel1Index && endSegment[sliceIndex] == currentSlice1Index && endSegment[labelIndex] != currentSegment2Label))
					{
						closestSegment = sortedIntersections[i].first;
						newLabel = currentSegment2Label;
						break;
					}

					// check if the second segment of the intersection matches
					if ((startSegment[idIndex] == currentLabel2Index && startSegment[sliceIndex] == currentSlice2Index && startSegment[labelIndex] != currentSegment1Label) ||
						(endSegment[idIndex] == currentLabel2Index && endSegment[sliceIndex] == currentSlice2Index && endSegment[labelIndex] != currentSegment1Label))
					{
						closestSegment = sortedIntersections[i].first;
						newLabel = currentSegment1Label;
						break;
					}
				}

				// if a valid intersection was found, propagate its label to the current segment
				QList< QList<float> > currentSegments = labelIterator.value();
				const unsigned int numCurrentSegments = currentSegments.length();
				for (int i=0; i<numCurrentSegments; ++i)
				{
					Logger::GetInstance()->WriteLine(QString("- Changing label: ") + QString::number(segmentFeatures[currentSegments[i][sliceIndex]][currentSegments[i][idIndex] - minimumLabels[currentSegments[i][sliceIndex]]][labelIndex]) + QString( " to " ) + QString().number(newLabel) );
					segmentFeatures[currentSegments[i][sliceIndex]][currentSegments[i][idIndex] - minimumLabels[currentSegments[i][sliceIndex]]][labelIndex] = newLabel;
				}

				if (debugOutput == true)
				{
					Logger::GetInstance()->WriteLine(QString("- Intersection: ") + QString::number(closestSegment[0]) + QString(", ") + QString::number(closestSegment[1]) + QString(", ") + QString::number(closestSegment[2]) + QString(", ") + QString::number(closestSegment[3]) + QString(", ") + QString::number(closestSegment[4]) + QString(", ") + QString::number(closestSegment[5]) + QString(", ") + QString::number(closestSegment[6]) + QString(", ") + QString::number(closestSegment[7]) + QString(", ") + QString::number(closestSegment[8]) );
					Logger::GetInstance()->WriteLine(QString("- Merging ") + QString::number(numCurrentSegments) + QString(" slices with label ") + QString::number(currentLabel) + QString(" to the segment with label ") + QString::number(newLabel) + ". Similarity: " + QString::number(closestSegment[similarityFeature]));
				}
			}

			// increment the label iterator
			++labelIterator;
		}
	}
	*/