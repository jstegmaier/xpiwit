/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "SplitConnectedBlobsImageFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"
#include "../../ITKCustom/itkSplitConnectedBlobsImageFilter.h"


// XPIWIT namespace
namespace XPIWIT
{

// the default constructor
template< class TInputImage >
SplitConnectedBlobsImageFilterWidget<TInputImage>::SplitConnectedBlobsImageFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = SplitConnectedBlobsImageFilterWidget<TInputImage>::GetName();
	this->mDescription = "Splits connected blobs based on an euclidean distance map and a watershed transform.";

	// set the filter type
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "WatershedLevel", "2.5", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The watershed level to start from. Lower levels tend to over segmentation, whereas higher levels tend to under segmentation." );
	processObjectSettings->AddSetting( "BinaryOutput", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "The watershed already performs a labeling of the regions. If binary output is desired instead, set this flag to 1." );
    
    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
SplitConnectedBlobsImageFilterWidget<TInputImage>::~SplitConnectedBlobsImageFilterWidget()
{
}


// the update function
template< class TInputImage >
void SplitConnectedBlobsImageFilterWidget<TInputImage>::Update()
{
	// start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const float watershedLevel = processObjectSettings->GetSettingValue( "WatershedLevel" ).toFloat();
	bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;
	bool inPlace = processObjectSettings->GetSettingValue( "InPlace" ).toInt() > 0;
	bool binaryOutput = processObjectSettings->GetSettingValue( "BinaryOutput" ).toInt() > 0;

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

	// define the splitting filter
	typedef typename itk::SplitConnectedBlobsImageFilter<TInputImage> SplitConnectedBlobsImageFilterType;
	typename SplitConnectedBlobsImageFilterType::Pointer splitConnectedBlobs = SplitConnectedBlobsImageFilterType::New();
	splitConnectedBlobs->SetBinaryOutput( binaryOutput );
	splitConnectedBlobs->SetInput( inputImage );
	splitConnectedBlobs->SetLevel( watershedLevel );
	splitConnectedBlobs->SetNumberOfWorkUnits( maxThreads );
	splitConnectedBlobs->SetReleaseDataFlag( releaseDataFlag );	

	// update the filter
	itkTryCatch( splitConnectedBlobs->Update(), "Error: Updating SplitConnectedBlobsImageFilterWidget failed." );
	
	ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( splitConnectedBlobs->GetOutput() );
    mOutputImages.append( outputImage );
	
	// update the process widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< SplitConnectedBlobsImageFilterWidget<Image2Float> > SplitConnectedBlobsImageFilterWidgetImage2Float;
static ProcessObjectProxy< SplitConnectedBlobsImageFilterWidget<Image3Float> > SplitConnectedBlobsImageFilterWidgetImage3Float;
static ProcessObjectProxy< SplitConnectedBlobsImageFilterWidget<Image2UShort> > SplitConnectedBlobsImageFilterWidgetImage2UShort;
static ProcessObjectProxy< SplitConnectedBlobsImageFilterWidget<Image3UShort> > SplitConnectedBlobsImageFilterWidgetImage3UShort;

} // namespace XPIWIT
