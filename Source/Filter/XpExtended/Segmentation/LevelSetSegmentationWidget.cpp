/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "LevelSetSegmentationWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

#include "../../ITKCustom/itkSeedBasedLevelSetsImageFilter.h"
#include "../../ITKCustom/itkExtractKeyPointsImageFilter.h"

// itk header
#include "itkGeodesicActiveContourLevelSetImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
/*
#include "itkSigmoidImageFilter.h"
#include "itkMultiplyImageFilter.h"
#include "itkBoundedReciprocalImageFilter.h"
#include "itkWatershedImageFilter.h"
#include "itkCurvatureAnisotropicDiffusionImageFilter.h"
#include "itkGradientMagnitudeRecursiveGaussianImageFilter.h"
#include "itkSigmoidImageFilter.h"
#include "itkInvertIntensityImageFilter.h"
#include "itkSobelEdgeDetectionImageFilter.h"
#include "itkConfidenceConnectedImageFilter.h"
#include "itkStreamingImageFilter.h"
*/


namespace XPIWIT
{

// the default constructor
template< class TInputImage>
LevelSetSegmentationWidget<TInputImage>::LevelSetSegmentationWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = LevelSetSegmentationWidget<TInputImage>::GetName();
	this->mDescription = "Perform geodesic active contours level sets segmentation as described by Caselles et al. Requires seed points as a meta input.";

	// set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(2);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    //processObjectSettings->AddSetting( "Sigma", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Std. Dev. for the gradient magnitude filtering." );
    //processObjectSettings->AddSetting( "EdgeMapFactor", "50.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Gradient magnitude filter multiplicator to adjust the intensity range of the final edge map.");
    processObjectSettings->AddSetting( "PropagationScaling", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The PropagationScaling parameter can be used to switch from propagation outwards (POSITIVE scaling parameter) versus propagating inwards (NEGATIVE scaling parameter)." );
    processObjectSettings->AddSetting( "CurvatureScaling", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "In general, the larger the CurvatureScaling, the smoother the resulting contour." );
    processObjectSettings->AddSetting( "AdvectionScaling", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Set the scaling of the advection field." );
    processObjectSettings->AddSetting( "NumIterations", "140", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The maximum number to perform the level sets extension." );
	processObjectSettings->AddSetting( "MaximumRMSError", "0.02", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The maximum rms error used as convergence threshold." );
    processObjectSettings->AddSetting( "Segment3D", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Perform segmentation in 3D or segment 2D slices with subsequent fusion of the slice segmentation results." );
	processObjectSettings->AddSetting( "InsideIsPositive", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Controls which side of 0 is chosen for the actual segmentation. Default: negative values." );
   // processObjectSettings->AddSetting( "SplitConnectedRegions", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Performs a splitting of connected blobs based on euclidean distance transform and watershed." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage>
LevelSetSegmentationWidget<TInputImage>::~LevelSetSegmentationWidget()
{
}


// the update function
template< class TInputImage >
void LevelSetSegmentationWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
	ProcessObjectBase::StartTimer();
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

	// get filter parameters
  //  const float sigma	= processObjectSettings->GetSettingValue( "Sigma" ).toDouble();
 //  const float edgeMapFactor = processObjectSettings->GetSettingValue( "EdgeMapFactor" ).toDouble();
	const float maximumRMSError = processObjectSettings->GetSettingValue( "MaximumRMSError" ).toFloat();
    const float propagationScaling = processObjectSettings->GetSettingValue( "PropagationScaling" ).toFloat();
    const float curvatureScaling = processObjectSettings->GetSettingValue( "CurvatureScaling" ).toFloat();
    const float advectionScaling = processObjectSettings->GetSettingValue( "AdvectionScaling" ).toFloat();
    const int numIterations = processObjectSettings->GetSettingValue( "NumIterations" ).toInt();
    const bool segment3D = processObjectSettings->GetSettingValue( "Segment3D" ).toInt() > 0;
	const bool insideIsPositive = processObjectSettings->GetSettingValue( "InsideIsPositive" ).toInt() > 0;
 //   const bool splitConnectedRegions = processObjectSettings->GetSettingValue( "SplitConnectedRegions" ).toInt();
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	typename TInputImage::Pointer edgeMapImage = mInputImages.at(1)->template GetImage<TInputImage>();
	
	typedef itk::GeodesicActiveContourLevelSetImageFilter<TInputImage, TInputImage> LevelSetsImageFilterType;
	typename LevelSetsImageFilterType::Pointer levelSetsFilter = LevelSetsImageFilterType::New();

	levelSetsFilter->SetInput( inputImage );
	levelSetsFilter->SetFeatureImage( edgeMapImage );
    levelSetsFilter->SetPropagationScaling( propagationScaling );
    levelSetsFilter->SetAdvectionScaling( advectionScaling );
    levelSetsFilter->SetCurvatureScaling( curvatureScaling );
	levelSetsFilter->SetMaximumRMSError( maximumRMSError );
    levelSetsFilter->SetNumberOfIterations( numIterations );

	typedef itk::BinaryThresholdImageFilter<TInputImage, TInputImage> ThresholdingFilterType;
	typename ThresholdingFilterType::Pointer thresholder = ThresholdingFilterType::New();
	thresholder->SetInput( levelSetsFilter->GetOutput() );
	thresholder->SetLowerThreshold( -65535 );
	thresholder->SetUpperThreshold( 0.0 );
	if (insideIsPositive == true)
	{	
		thresholder->SetLowerThreshold( 0.0 );
		thresholder->SetUpperThreshold( 65535 );
	}

	thresholder->SetOutsideValue( 0 );
	thresholder->SetInsideValue( 65535 );

	/*
    // setup the filters
    typedef itk::SeedBasedLevelSetsImageFilter<TInputImage> SeedBasedLevelSetsImageFilterType;
	typename SeedBasedLevelSetsImageFilterType::Pointer levelSetsFilter = SeedBasedLevelSetsImageFilterType::New();

    // initialize level sets segmentation
    levelSetsFilter->SetInput( inputImage );
    levelSetsFilter->SetSegment3D( segment3D );
    levelSetsFilter->SetSplitConnectedBlobs( splitConnectedRegions );
    levelSetsFilter->SetNumberOfWorkUnits( maxThreads );
    levelSetsFilter->SetOverlapSize( 64 );

	// adjust the level set segmentation parameters
	//levelSetsFilter->SetSeedFileName( QString(this->mCMDPipelineArguments->mStdOutputPath + "_KeyPoints.csv").toLatin1().data() );
	levelSetsFilter->SetKeyPoints( this->mMetaInputs.at(0) );
    levelSetsFilter->SetEdgeMapFactor( edgeMapFactor );
    levelSetsFilter->SetSigma( sigma );
    levelSetsFilter->SetPropagationScaling( propagationScaling );
    levelSetsFilter->SetAdvectionScaling( advectionScaling );
    levelSetsFilter->SetCurvatureScaling( curvatureScaling );
    levelSetsFilter->SetNumIterations( numIterations );
	*/




    // perform and set the output
    itkTryCatch( thresholder->Update(), "LevelSet Segmentation Called in LevelSetSegmentationWidget." );

    ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( thresholder->GetOutput() );
    mOutputImages.append( outputImage );

    // update the base class
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< LevelSetSegmentationWidget<Image2Float> > LevelSetSegmentationWidgetImage2Float;
static ProcessObjectProxy< LevelSetSegmentationWidget<Image3Float> > LevelSetSegmentationWidgetImage3Float;

} // namespace XPIWIT
