/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. Hübner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. Hübner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "LocalAdaptiveThresholdImageFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkBoxMeanImageFilter.h"
#include "itkSubtractImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"
#include "itkBoxSigmaImageFilter.h"
#include "itkMultiplyImageFilter.h"


namespace XPIWIT
{

// the default constructor
template< class TInputImage >
LocalAdaptiveThresholdImageFilterWidget<TInputImage>::LocalAdaptiveThresholdImageFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = LocalAdaptiveThresholdImageFilterWidget<TInputImage>::GetName();
	this->mDescription = "Performs a local adaptive thresholding of the input image.";

	// set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "OutsideValue", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Specifies the outside value, i.e., the background intensity." );
    processObjectSettings->AddSetting( "InsideValue", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Specifies the inside value, i.e., the foreground intensity." );
    processObjectSettings->AddSetting( "StdDevMultiplier", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Specifies the multiplier for the standard deviation, default is zero." );
    processObjectSettings->AddSetting( "Radius", "2", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The radius of the sampling region." );
	processObjectSettings->AddSetting( "Offset", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The offset, i.e., foreground regions have to be above this value.");

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
LocalAdaptiveThresholdImageFilterWidget<TInputImage>::~LocalAdaptiveThresholdImageFilterWidget()
{
}


// the update function
template< class TInputImage >
void LocalAdaptiveThresholdImageFilterWidget<TInputImage>::Update()
{
	// start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get the settings
    const float radius = processObjectSettings->GetSettingValue( "Radius" ).toInt();
    const float stdDevMultiplier = processObjectSettings->GetSettingValue( "StdDevMultiplier" ).toFloat();
	const float offset = processObjectSettings->GetSettingValue("Offset").toFloat();
    const float insideValue = processObjectSettings->GetSettingValue( "InsideValue" ).toFloat();
    const float outsideValue = processObjectSettings->GetSettingValue( "OutsideValue" ).toFloat();
	bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;
    
    typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
    inputImage->SetReleaseDataFlag( false );
    
	// calculate the mean image for the specified radius
	typename itk::BoxMeanImageFilter<TInputImage, TInputImage>::Pointer meanFilter = itk::BoxMeanImageFilter<TInputImage, TInputImage>::New();
	meanFilter->SetReleaseDataFlag( false );
    meanFilter->SetInput( inputImage );
    meanFilter->SetRadius( radius );
	itkTryCatch( meanFilter->Update(), "Error: Updating LocalAdaptiveThresholdingFilterWidget failed." );
    
	// calculate the difference image of the raw input image and the mean image
    typename itk::SubtractImageFilter<TInputImage, TInputImage, TInputImage>::Pointer subtractImageFilter = itk::SubtractImageFilter<TInputImage>::New();
    subtractImageFilter->SetReleaseDataFlag( false );
    subtractImageFilter->SetInput1( inputImage );
    subtractImageFilter->SetInput2( meanFilter->GetOutput() );
    itkTryCatch( subtractImageFilter->Update(), "Error: Updating LocalAdaptiveThresholdingFilterWidget failed." );

	// calculate the standard deviation image
	typename itk::BoxSigmaImageFilter<TInputImage, TInputImage>::Pointer sigmaFilter = itk::BoxSigmaImageFilter<TInputImage, TInputImage>::New();
	sigmaFilter->SetReleaseDataFlag(false);
	sigmaFilter->SetInput(inputImage);
	sigmaFilter->SetRadius(radius);
	itkTryCatch(sigmaFilter->Update(), "Error: Updating LocalAdaptiveThresholdingFilterWidget failed.");

	// multiply the standard deviation image by the multiplier
	typename itk::MultiplyImageFilter<TInputImage, TInputImage, TInputImage>::Pointer multiplyFilter = itk::MultiplyImageFilter<TInputImage, TInputImage, TInputImage>::New();
	multiplyFilter->SetInput(sigmaFilter->GetOutput());
	multiplyFilter->SetConstant(stdDevMultiplier);
	itkTryCatch(multiplyFilter->Update(), "Error: Updating LocalAdaptiveThresholdingFilterWidget failed.");

	// subtract the multiplied image from the mean subtracted image
	typename itk::SubtractImageFilter<TInputImage, TInputImage, TInputImage>::Pointer subtractImageFilter2 = itk::SubtractImageFilter<TInputImage>::New();
	subtractImageFilter2->SetReleaseDataFlag(false);
	subtractImageFilter2->SetInput1(subtractImageFilter->GetOutput());
	subtractImageFilter2->SetInput2(multiplyFilter->GetOutput());
	itkTryCatch(subtractImageFilter2->Update(), "Error: Updating LocalAdaptiveThresholdingFilterWidget failed.");
    
    // convert internal representation to external one
    typename itk::BinaryThresholdImageFilter<TInputImage, TInputImage>::Pointer binaryThresholdFilter = itk::BinaryThresholdImageFilter<TInputImage, TInputImage>::New();
	binaryThresholdFilter->SetInput(subtractImageFilter2->GetOutput());
	binaryThresholdFilter->SetReleaseDataFlag( releaseDataFlag );
    binaryThresholdFilter->SetInsideValue(insideValue);
    binaryThresholdFilter->SetOutsideValue(outsideValue);
    binaryThresholdFilter->SetLowerThreshold(offset);
    binaryThresholdFilter->SetUpperThreshold(1.0);

    // update the filter pipeline
	itkTryCatch( binaryThresholdFilter->Update(), "Error: Updating LocalAdaptiveThresholdingFilterWidget failed." );
    ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( binaryThresholdFilter->GetOutput() );
    outputImage->SetRescaleFlag( true );
    mOutputImages.append( outputImage );

    // update the base class
    ProcessObjectBase::Update();
    
    // log the performance
    ProcessObjectBase::LogPerformance();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< LocalAdaptiveThresholdImageFilterWidget<Image2Float> > LocalAdaptiveThresholdFilterWidgetImage2Float;
static ProcessObjectProxy< LocalAdaptiveThresholdImageFilterWidget<Image3Float> > LocalAdaptiveThresholdFilterWidgetImage3Float;
static ProcessObjectProxy< LocalAdaptiveThresholdImageFilterWidget<Image2UShort> > LocalAdaptiveThresholdFilterWidgetImage2UShort;
static ProcessObjectProxy< LocalAdaptiveThresholdImageFilterWidget<Image3UShort> > LocalAdaptiveThresholdFilterWidgetImage3UShort;

} // namespace XPIWIT