/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "SliceBySliceFusionFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"
#include "../../ITKCustom/itkCreateLabelImageFromFusionData.h"

// qt header
#include <list>

namespace XPIWIT
{

// the default constructor
template< class TInputImage >
SliceBySliceFusionFilterWidget<TInputImage>::SliceBySliceFusionFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = SliceBySliceFusionFilterWidget<TInputImage>::GetName();
	this->mDescription = "Apply the seed based segment fusion. Requires SliceBySliceRegionProps and Intersections as meta information.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(2);
	this->mObjectType->AppendMetaInputType("SliceBySliceRegionProps");
	this->mObjectType->AppendMetaInputType("Intersections");
    this->mObjectType->SetNumberMetaOutputs(1);
	this->mObjectType->AppendMetaOutputType("SliceBySliceRegionProps");

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "LinkThreshold", "0.4", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The minimum intersection similarity of two segments, 0.0 for no overlap, 1.0 for perfect overlap." );
    processObjectSettings->AddSetting( "DebugOutput", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, debug output will be written to the log file." );
	processObjectSettings->AddSetting( "SingleSliceHeuristic", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, the filter uses the slingle slice fusion heuristic, i.e., segments with less than minSlices slices are merged to their closest neighbour." );
    processObjectSettings->AddSetting( "JaccardIndexHeuristic", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If true, the filter uses the jaccard index based fusion heuristic with the specified maximum overlap size." );
    processObjectSettings->AddSetting( "MinSlices", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The minimum number of slices a segment should be build from. Only used in combination with the single slice heuristic." );
	processObjectSettings->AddSetting( "UseRandomLabels", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, final segmentation will be randomly labeled." );
	processObjectSettings->AddSetting( "SimilarityFeature", "6", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Similarity feature to use. 6 = JaccardIndex, 7=MinimumRelativeOverlap." );
	processObjectSettings->AddSetting( "MaxSize", "-1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "If larger than zero, segments 2D with more pixels than the specified size are discarded." );
	processObjectSettings->AddSetting( "MinimumVolume", "1500", ProcessObjectSetting::SETTINGVALUETYPE_INT, "If larger than zero, 3D segments with less pixels than the specified size are fused if the fusion does not violate the maximum volume." );
	processObjectSettings->AddSetting( "MaximumVolume", "4000", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Largest allowed volume. Used to limit the fusion of segments that fall below the minimum volume." );
	processObjectSettings->AddSetting( "MaxFusionOverlap", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The number of slices segments are allowed to overlap for the jaccard index based fusion heuristic. This can be used to avoid branchings." );
	processObjectSettings->AddSetting( "LabelFeature", "14", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The feature used to generate the label image. 14 = SeedLabel, 15 = JaccardIndexLabel." );
	processObjectSettings->AddSetting( "ColorFeature", "14", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The feature used to generate the label image. 14 = SeedLabel, 15 = JaccardIndexLabel, 16 = SizeRatio." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
SliceBySliceFusionFilterWidget<TInputImage>::~SliceBySliceFusionFilterWidget()
{
}


// function to determine the insertion position of a query to a sorted list
template< class TInputImage >
unsigned int SliceBySliceFusionFilterWidget<TInputImage>::FindInsertionLocation(const QList< QPair<QList<float>, float> >& sortedList, const QPair<QList<float>, float>& query)
{
	bool insertionFound = false;
	unsigned int insertionIndex = 0;

	unsigned int minIndex = 0;
	unsigned int maxIndex = sortedList.length()-1; 
	unsigned int searchLocation = 0;

	while (insertionFound == false)
	{
		// update the search location
		searchLocation = int(0.5+0.5*(minIndex+maxIndex));

		if (sortedList[searchLocation].second > query.second)
			maxIndex = searchLocation;
		else
			minIndex = searchLocation;

		if ((maxIndex - minIndex) <= 1)
		{
			insertionFound = true;
			insertionIndex = searchLocation;
		}
	}

	return insertionIndex;
}


// the update function
template< class TInputImage >
void SliceBySliceFusionFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get filter parameters
    const float linkThreshold = processObjectSettings->GetSettingValue( "LinkThreshold" ).toFloat();
    const bool singleSliceHeuristic = processObjectSettings->GetSettingValue( "SingleSliceHeuristic" ).toInt() > 0;
    const bool jaccardIndexHeuristic = processObjectSettings->GetSettingValue( "JaccardIndexHeuristic" ).toInt() > 0;
    const bool useRandomLabels = processObjectSettings->GetSettingValue( "UseRandomLabels" ).toInt() > 0;
	const bool debugOutput = processObjectSettings->GetSettingValue( "DebugOutput" ).toInt() > 0;
    const unsigned int minSlices = processObjectSettings->GetSettingValue( "MinSlices" ).toInt();
	const unsigned int minimumVolume = processObjectSettings->GetSettingValue( "MinimumVolume" ).toInt();
	const unsigned int maximumVolume = processObjectSettings->GetSettingValue( "MaximumVolume" ).toInt();
    const unsigned int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const unsigned int similarityFeature = processObjectSettings->GetSettingValue( "SimilarityFeature" ).toInt();
	const unsigned int labelFeature = processObjectSettings->GetSettingValue( "LabelFeature" ).toInt();
	const unsigned int colorFeature = processObjectSettings->GetSettingValue( "ColorFeature" ).toInt();
	const unsigned int maxFusionOverlap = processObjectSettings->GetSettingValue( "MaxFusionOverlap" ).toInt();
	const int maxSize = processObjectSettings->GetSettingValue( "MaxSize" ).toInt();

    // setup the filters
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

	// get the meta data
	QList< QList< QList<float> > > segmentFeatures;
	MetaDataFilter* segmentFeatures2D = this->mMetaInputs.at(0);
	MetaDataFilter* intersections = this->mMetaInputs.at(1);
	const unsigned int numSegments = segmentFeatures2D->mData.length();
	const unsigned int numIntersections = intersections->mData.length();

	// get the feature indices from the meta data objects
	const unsigned int idIndex = segmentFeatures2D->GetFeatureIndex("id");
	const unsigned int labelIndex = segmentFeatures2D->GetFeatureIndex("seedLabel");
	const unsigned int sizeRatioIndex = segmentFeatures2D->GetFeatureIndex("sizeRatio");
	const unsigned int jiLabelIndex = segmentFeatures2D->GetFeatureIndex("jiLabel");
	const unsigned int xposIndex = segmentFeatures2D->GetFeatureIndex("xpos");
	const unsigned int yposIndex = segmentFeatures2D->GetFeatureIndex("ypos");
	const unsigned int sliceIndex = segmentFeatures2D->GetFeatureIndex("zpos");
	const unsigned int xsizeIndex = segmentFeatures2D->GetFeatureIndex("xsize");
	const unsigned int ysizeIndex = segmentFeatures2D->GetFeatureIndex("ysize");
	const unsigned int zsizeIndex = segmentFeatures2D->GetFeatureIndex("zsize");
	const unsigned int areaIndex = segmentFeatures2D->GetFeatureIndex("volume");
	const unsigned int meanIntensityIndex = segmentFeatures2D->GetFeatureIndex("meanIntensity");
	const unsigned int areaUnionFeature = intersections->GetFeatureIndex("areaUnion");
	const unsigned int label1Index = intersections->GetFeatureIndex("label1");
	const unsigned int label2Index = intersections->GetFeatureIndex("label2");
	const unsigned int slice1Index = intersections->GetFeatureIndex("slice1");
	const unsigned int slice2Index = intersections->GetFeatureIndex("slice2");
		
	qint64 timer0 = QDateTime::currentMSecsSinceEpoch();

	// convert segment pairs to slice based representation
	const unsigned int numSlices = inputImage->GetLargestPossibleRegion().GetSize(2);
	segmentFeatures.reserve( numSlices );
	for (int i=0; i<numSlices; ++i)
		segmentFeatures.append( QList< QList<float> >() );

	Logger::GetInstance()->WriteLine( QString("Initializing segmentFeatures took: ") + QString::number((float)(QDateTime::currentMSecsSinceEpoch() - timer0) / 1000.0) );
	timer0 = QDateTime::currentMSecsSinceEpoch();

	// fill the initialized data structure
	QList<float> line;
	QList<unsigned int> minimumLabels;
	for (int i=0; i<numSlices; ++i)
		minimumLabels.append( 1000000 );

    for(int i=0; i<numSegments; i++)
	{
        line = segmentFeatures2D->mData.at(i);
		segmentFeatures[int(line.at(sliceIndex))].append( line );

		if (line[0] < minimumLabels[int(line.at(sliceIndex))])
			minimumLabels[int(line.at(sliceIndex))] = line[0];
    }

	Logger::GetInstance()->WriteLine( QString("Initializing minimumLabels took: ") + QString::number((float)(QDateTime::currentMSecsSinceEpoch() - timer0) / 1000.0) );
	timer0 = QDateTime::currentMSecsSinceEpoch();

	// sort the intersections based on the chosen similarity measure
	QList< QPair<QList<float>, float> > sortedIntersections;
	sortedIntersections.reserve(numIntersections);

	// add pairs for the intersections
	for (int i=0; i<numIntersections; ++i)
	{
		// check if overlap is larger than the specified intersection threshold
		if (intersections->mData[i].at(similarityFeature) > linkThreshold && (intersections->mData[i].at(areaUnionFeature) < maxSize && maxSize > 0))
			sortedIntersections.append( QPair<QList<float>, float>(intersections->mData[i], intersections->mData[i].at(similarityFeature)) );
	}

	// perform the sorting of all intersections
	const unsigned int numSortedIntersections = sortedIntersections.length();
	std::stable_sort(sortedIntersections.begin(), sortedIntersections.end(), QPairIntersectionComparer());

	Logger::GetInstance()->WriteLine( QString("Adding and sorting intersections took: ") + QString::number((float)(QDateTime::currentMSecsSinceEpoch() - timer0) / 1000.0) );
	timer0 = QDateTime::currentMSecsSinceEpoch();

	/////////////// TEMP /////////////////
	QHash<unsigned int, std::list< QList<float> > > intersectionHashMap;
		
	unsigned int slice1 = 0;
	unsigned int slice2 = 0;
	for (int i=0; i<numSortedIntersections; ++i)
	{
		slice1 = sortedIntersections[i].first[slice1Index];
		slice2 = sortedIntersections[i].first[slice2Index];
		if (!intersectionHashMap.contains(slice1))
			intersectionHashMap[slice1] = std::list< QList<float> >();

		if (!intersectionHashMap.contains(slice2))
			intersectionHashMap[slice2] = std::list< QList<float> >();
		
		intersectionHashMap[slice1].push_back( sortedIntersections[i].first );
		intersectionHashMap[slice2].push_back( sortedIntersections[i].first );
	}
	/////////////////////////////////////

	Logger::GetInstance()->WriteLine( QString("Generating hash table took: ") + QString::number((float)(QDateTime::currentMSecsSinceEpoch() - timer0) / 1000.0) );
	timer0 = QDateTime::currentMSecsSinceEpoch();

	// extract only the intersections that are already labeled
	std::list< QPair<QList<float>, float> > currentIntersections;
	//currentIntersections.reserve(sortedIntersections.length());

	// find all intersections that correspond to this label
	for (int k=0; k<numSortedIntersections; ++k)
	{
		// only add labeled intersections to initialize
		if (segmentFeatures[sortedIntersections[k].first.at(slice1Index)][sortedIntersections[k].first.at(label1Index)-minimumLabels[sortedIntersections[k].first.at(slice1Index)]][labelIndex] > 0  ||
			segmentFeatures[sortedIntersections[k].first.at(slice2Index)][sortedIntersections[k].first.at(label2Index)-minimumLabels[sortedIntersections[k].first.at(slice2Index)]][labelIndex] > 0)
			currentIntersections.push_back( sortedIntersections[k] );
	}

	Logger::GetInstance()->WriteLine( QString("Initializing currently valid intersection took: ") + QString::number((float)(QDateTime::currentMSecsSinceEpoch() - timer0) / 1000.0) );
	timer0 = QDateTime::currentMSecsSinceEpoch();

	// generate a hash map containing all segments that belong to a certain label


	// perform the sorting of all seeded intersections
	//qStableSort(currentIntersections.begin(), currentIntersections.end(), QPairIntersectionComparer());

	Logger::GetInstance()->WriteLine( QString("Sorting currently valid intersection took: ") + QString::number((float)(QDateTime::currentMSecsSinceEpoch() - timer0) / 1000.0) );
	timer0 = QDateTime::currentMSecsSinceEpoch();

	// initialize the hash map. The hash map is two dimensional, with the slice index in the first and the segment label in the second dimension
	QHash< unsigned int, QHash< unsigned int, std::list< QPair<QList<float>, float> > > > unlabeledIntersectionsHashMap;
	for (int i=0; i<numSlices; ++i)
		unlabeledIntersectionsHashMap[i] = QHash< unsigned int, std::list< QPair<QList<float>, float> > >();

	QList< QPair<QList<float>, float> >::iterator sortedIntersectionsIterator = sortedIntersections.begin();
	for (; sortedIntersectionsIterator != sortedIntersections.end(); ++sortedIntersectionsIterator)
	{
		const unsigned int label1 = (*sortedIntersectionsIterator).first.at(0);
		const unsigned int label2 = (*sortedIntersectionsIterator).first.at(1);
		const unsigned int slice1 = (*sortedIntersectionsIterator).first.at(2);
		const unsigned int slice2 = (*sortedIntersectionsIterator).first.at(3);

		if (!unlabeledIntersectionsHashMap[slice1].contains(label1))
			unlabeledIntersectionsHashMap[slice1][label1] = std::list< QPair<QList<float>, float> >();

		if (!unlabeledIntersectionsHashMap[slice2].contains(label2))
			unlabeledIntersectionsHashMap[slice2][label2] = std::list< QPair<QList<float>, float> >();

		unlabeledIntersectionsHashMap[slice1][label1].push_back((*sortedIntersectionsIterator));
		unlabeledIntersectionsHashMap[slice2][label2].push_back((*sortedIntersectionsIterator));
	}

	Logger::GetInstance()->WriteLine( QString("Unlabeled intersection hashmap creation took: ") + QString::number((float)(QDateTime::currentMSecsSinceEpoch() - timer0) / 1000.0) );
	timer0 = QDateTime::currentMSecsSinceEpoch();


	// process all intersections
	while (!currentIntersections.empty())
	{
		//QList<float> currentIntersection = currentIntersections.first().first;
		const unsigned int label1 = currentIntersections.front().first.at(0);
		const unsigned int label2 = currentIntersections.front().first.at(1);
		const unsigned int slice1 = currentIntersections.front().first.at(2);
		const unsigned int slice2 = currentIntersections.front().first.at(3);
		const unsigned int index1 = label1-minimumLabels[slice1];
		const unsigned int index2 = label2-minimumLabels[slice2];

		// remove the current intersection
		currentIntersections.pop_front();
		bool newSegmentAdded = false;

		// case 1: segment 1 has no label, segment 2 has a label
        // --> merge if no local maximum in the interior mean intensity feature is introduced by the merge
		if (segmentFeatures[slice1][index1].at(labelIndex) == 0 && segmentFeatures[slice2][index2].at(labelIndex) != 0)
		{
			segmentFeatures[slice1][index1][labelIndex] = segmentFeatures[slice2][index2][labelIndex];
			
			///////// NEW //////
			std::list< QPair<QList<float>, float> >::iterator intersectionIterator = unlabeledIntersectionsHashMap[slice1][label1].begin();
			for (; intersectionIterator != unlabeledIntersectionsHashMap[slice1][label1].end(); ++intersectionIterator)
			{
				if (segmentFeatures[(*intersectionIterator).first.at(3)][(*intersectionIterator).first.at(1)-minimumLabels[(*intersectionIterator).first.at(3)]][labelIndex] == 0 ||
					segmentFeatures[(*intersectionIterator).first.at(2)][(*intersectionIterator).first.at(0)-minimumLabels[(*intersectionIterator).first.at(2)]][labelIndex] == 0)
				{
					std::list< QPair<QList<float>, float> >::iterator currentIntersectionIterator = currentIntersections.begin();
					for (; currentIntersectionIterator != currentIntersections.end(); ++currentIntersectionIterator)
					{
						if ((*intersectionIterator).second > (*currentIntersectionIterator).second)
						{
							currentIntersections.insert(currentIntersectionIterator, (*intersectionIterator));
							break;
						}
					}

					// TODO: TRY IF REMOVING THE CURRENT INTERSECTION WORKS.
				}
			}
			///////// END NEW //////

			/*
			// find all intersections that correspond to this label
			for (int i=0; i<sortedIntersections.length(); ++i)
			{
				if ((sortedIntersections[i].first.at(0) == label1 && sortedIntersections[i].first.at(2) == slice1 && segmentFeatures[sortedIntersections[i].first.at(3)][sortedIntersections[i].first.at(1)-minimumLabels[sortedIntersections[i].first.at(3)]][labelIndex] == 0) ||
					(sortedIntersections[i].first.at(1) == label1 && sortedIntersections[i].first.at(3) == slice1 && segmentFeatures[sortedIntersections[i].first.at(2)][sortedIntersections[i].first.at(0)-minimumLabels[sortedIntersections[i].first.at(2)]][labelIndex] == 0))
				{
					//currentIntersections.insert( FindInsertionLocation(sortedIntersections, sortedIntersections[i]), sortedIntersections[i] );
					std::list< QPair<QList<float>, float> >::iterator currentIntersectionIterator = currentIntersections.begin();
					for (currentIntersectionIterator; currentIntersectionIterator != currentIntersections.end(); ++currentIntersectionIterator)
					{
						if (sortedIntersections[i].second > (*currentIntersectionIterator).second)
						{
							currentIntersections.insert(currentIntersectionIterator, sortedIntersections[i]);
							break;
						}
					}
				}
			}
			*/
		}
		else if (segmentFeatures[slice1][index1].at(labelIndex) != 0 && segmentFeatures[slice2][index2].at(labelIndex) == 0)
		{
			segmentFeatures[slice2][index2][labelIndex] = segmentFeatures[slice1][index1][labelIndex];

			///////// NEW //////
			std::list< QPair<QList<float>, float> >::iterator intersectionIterator = unlabeledIntersectionsHashMap[slice2][label2].begin();
			for (; intersectionIterator != unlabeledIntersectionsHashMap[slice2][label2].end(); ++intersectionIterator)
			{
				if (segmentFeatures[(*intersectionIterator).first.at(3)][(*intersectionIterator).first.at(1)-minimumLabels[(*intersectionIterator).first.at(3)]][labelIndex] == 0 ||
					segmentFeatures[(*intersectionIterator).first.at(2)][(*intersectionIterator).first.at(0)-minimumLabels[(*intersectionIterator).first.at(2)]][labelIndex] == 0)
				{
					std::list< QPair<QList<float>, float> >::iterator currentIntersectionIterator = currentIntersections.begin();
					for (; currentIntersectionIterator != currentIntersections.end(); ++currentIntersectionIterator)
					{
						if ((*intersectionIterator).second > (*currentIntersectionIterator).second)
						{
							currentIntersections.insert(currentIntersectionIterator, (*intersectionIterator));
							break;
						}
					}

					// TODO: TRY IF REMOVING THE CURRENT INTERSECTION WORKS.
				}
			}
			///////// END NEW //////

			/*
			// find all intersections that correspond to this label
			for (int i=0; i<sortedIntersections.length(); ++i)
			{
				if ((sortedIntersections[i].first.at(0) == label2 && sortedIntersections[i].first.at(2) == slice2 && segmentFeatures[sortedIntersections[i].first.at(3)][sortedIntersections[i].first.at(1)-minimumLabels[sortedIntersections[i].first.at(3)]][labelIndex] == 0) ||
					(sortedIntersections[i].first.at(1) == label2 && sortedIntersections[i].first.at(3) == slice2 && segmentFeatures[sortedIntersections[i].first.at(2)][sortedIntersections[i].first.at(0)-minimumLabels[sortedIntersections[i].first.at(2)]][labelIndex] == 0))
				{
					//currentIntersections.insert( FindInsertionLocation(sortedIntersections, sortedIntersections[i]), sortedIntersections[i] );
					std::list< QPair<QList<float>, float> >::iterator currentIntersectionIterator = currentIntersections.begin();
					for (currentIntersectionIterator; currentIntersectionIterator != currentIntersections.end(); ++currentIntersectionIterator)
					{
						if (sortedIntersections[i].second > (*currentIntersectionIterator).second)
						{
							currentIntersections.insert(currentIntersectionIterator, sortedIntersections[i]);
							break;
						}
					}
				}
			}*/
		}
	}

	Logger::GetInstance()->WriteLine( QString("Fusing all valid intersection took: ") + QString::number((float)(QDateTime::currentMSecsSinceEpoch() - timer0) / 1000.0) );
	timer0 = QDateTime::currentMSecsSinceEpoch();
	
	////////////////// JACCARD INDEX BASED FUSION HEURISTIC /////////////////
	if (jaccardIndexHeuristic == true)
	{
		// get the number of sorted intersections
		const unsigned int numSortedIntersections = sortedIntersections.length();
		QList<float> currentIntersection;
		unsigned int index1, index2, label1, label2, slice1, slice2;
		unsigned int currentLabel = 1;

		// iterate over the sorted itersections
		for (int i=0; i<numSortedIntersections; ++i)
		{
			// get the current intersection
			currentIntersection = sortedIntersections[i].first;
			label1 = currentIntersection.at(label1Index);
			label2 = currentIntersection.at(label2Index);
			slice1 = currentIntersection.at(slice1Index);
			slice2 = currentIntersection.at(slice2Index);
			index1 = label1 - minimumLabels[slice1];
			index2 = label2 - minimumLabels[slice2];

			// initialize the merge flag
			bool mergeSegments = true;

			// case 1: none of the segments has a label assigned
			// --> merge, as the decision is unambiguous
			if (segmentFeatures[slice1][index1][jiLabelIndex] == 0 && segmentFeatures[slice2][index2][jiLabelIndex] == 0)
			{
				segmentFeatures[slice1][index1][jiLabelIndex] = currentLabel;
				segmentFeatures[slice2][index2][jiLabelIndex] = currentLabel;

				// increment the current label
				currentLabel++;
			}

			// case 2: segment 1 has no label, segment 2 has a label 
			// --> merge if no local maximum in the interior mean intensity feature is introduced by the merge
			else if (segmentFeatures[slice1][index1][jiLabelIndex] == 0 && segmentFeatures[slice2][index2][jiLabelIndex] != 0)
			{
                unsigned int nextSlice = vnl_math::min((int)slice2+1, (int)segmentFeatures.length()-1);
				unsigned int area = 0;
				float accumulatedIntensity = 0.0;
				
				// find the segments in the next slice to check for local maxima
				for (int j=0; j<segmentFeatures[nextSlice].length(); ++j)
				{
					if (segmentFeatures[nextSlice][j][jiLabelIndex] == segmentFeatures[slice2][index2][jiLabelIndex])
					{
						area += segmentFeatures[nextSlice][j][areaIndex];
						accumulatedIntensity += segmentFeatures[nextSlice][j][areaIndex]*segmentFeatures[nextSlice][j][meanIntensityIndex];
					}
				}

				// check if a local maximum is introduced by the merge
				if (area > 0)
				{
					accumulatedIntensity /= area;
					if (accumulatedIntensity < segmentFeatures[slice2][index2][meanIntensityIndex] && segmentFeatures[slice1][index1][meanIntensityIndex] < segmentFeatures[slice2][index2][meanIntensityIndex])
						mergeSegments = false;
				}

				// merge segments only if no local maximum is introduced, otherwise assign new label to the single slice
				if (mergeSegments == true)
				{
					segmentFeatures[slice1][index1][jiLabelIndex] = segmentFeatures[slice2][index2][jiLabelIndex];
				}
				else
				{
					if (debugOutput == true)
						Logger::GetInstance()->WriteLine( "- Not Merging due to extremum heuristic... " );

					segmentFeatures[slice1][index1][jiLabelIndex] = currentLabel;
					currentLabel++;
				}
			}

			// case 3: segment 2 has no label, segment 1 has a label 
			// --> merge if no local maximum in the interior mean intensity feature is introduced by the merge
			else if (segmentFeatures[slice1][index1][jiLabelIndex] != 0 && segmentFeatures[slice2][index2][jiLabelIndex] == 0)
			{
				const unsigned int prevSlice = vnl_math::max((int)slice1-1, (int)0);
				unsigned int area = 0;
				float accumulatedIntensity = 0.0;
								
				// find the segments in the next slice to check for local maxima
				for (int j=0; j<segmentFeatures[prevSlice].length(); ++j)
				{
					if (segmentFeatures[prevSlice][j][jiLabelIndex] == segmentFeatures[slice1][index1][jiLabelIndex])
					{
						area += segmentFeatures[prevSlice][j][areaIndex];
						accumulatedIntensity += segmentFeatures[prevSlice][j][areaIndex]*segmentFeatures[prevSlice][j][meanIntensityIndex];
					}
				}

				// check if a local maximum is introduced by the merge
				if (area > 0)
				{
					accumulatedIntensity /= area;
					if (accumulatedIntensity < segmentFeatures[slice1][index1][meanIntensityIndex] && segmentFeatures[slice2][index2][meanIntensityIndex] < segmentFeatures[slice1][index1][meanIntensityIndex])
						mergeSegments = false;
				}

				// merge segments only if no local maximum is introduced, otherwise assign new label to the single slice
				if (mergeSegments == true)
				{
					segmentFeatures[slice2][index2][jiLabelIndex] = segmentFeatures[slice1][index1][jiLabelIndex];
				}
				else
				{
					if (debugOutput == true)
						Logger::GetInstance()->WriteLine( "- Not Merging due to extremum heuristic... " );

					segmentFeatures[slice1][index1][jiLabelIndex] = currentLabel;
					currentLabel++;
				}
			}

	        // case 4: both segments have a label assigned already (Skipped as under segmentation is desired for the correction heuristic)
			// --> merge if no local maximum in the interior mean intensity of the membrane gradient magnitude feature is introduced by the merge
			else
			{
				const unsigned int nextSlice = vnl_math::min((int)slice2+1, (int)segmentFeatures.length()-1);
				const unsigned int prevSlice = vnl_math::max((int)slice1-1, (int)0);
			}

			// perform the actual correction of the seeded fusion
			if (mergeSegments == true && segmentFeatures[slice1][index1][labelIndex] != segmentFeatures[slice2][index2][labelIndex])
			{
				// get the labels of the two segments that should be fused
				const unsigned int newLabel = segmentFeatures[slice1][index1][labelIndex];
				const unsigned int oldLabel = segmentFeatures[slice2][index2][labelIndex];

				unsigned int minZLabel1 = segmentFeatures.length(); 
				unsigned int maxZLabel1 = 0;
				unsigned int minZLabel2 = segmentFeatures.length();
				unsigned int maxZLabel2 = 0;

				for (int j=0; j<segmentFeatures.length(); ++j)
				{
					for (int k=0; k<segmentFeatures[j].length(); ++k)
					{
						if (segmentFeatures[j][k][labelIndex] == newLabel && segmentFeatures[j][k][sliceIndex] < minZLabel1)
							minZLabel1 = segmentFeatures[j][k][sliceIndex];

						if (segmentFeatures[j][k][labelIndex] == oldLabel && segmentFeatures[j][k][sliceIndex] < minZLabel2)
							minZLabel2 = segmentFeatures[j][k][sliceIndex];

						if (segmentFeatures[j][k][labelIndex] == newLabel && segmentFeatures[j][k][sliceIndex] > maxZLabel1)
							maxZLabel1 = segmentFeatures[j][k][sliceIndex];

						if (segmentFeatures[j][k][labelIndex] == oldLabel && segmentFeatures[j][k][sliceIndex] > maxZLabel2)
							maxZLabel2 = segmentFeatures[j][k][sliceIndex];					
					}
				}

				if (maxFusionOverlap >= vnl_math::min((maxZLabel1-minZLabel2), (maxZLabel2-minZLabel1)))
				{
					// propagate the label to the second segment
					for (int j=0; j<segmentFeatures.length(); ++j)
						for (int k=0; k<segmentFeatures[j].length(); ++k)
							if (segmentFeatures[j][k][labelIndex] == oldLabel)
								segmentFeatures[j][k][labelIndex] = newLabel;

					if (debugOutput == true)
						Logger::GetInstance()->WriteLine( "- Valid correction location found ... " );
				}
				else
				{
					if (debugOutput == true)
						Logger::GetInstance()->WriteLine( "- Branching avoided ... " );
				}
			}
		}

		Logger::GetInstance()->WriteLine( QString("Jaccard index fusion heuristic took: ") + QString::number((float)(QDateTime::currentMSecsSinceEpoch() - timer0) / 1000.0) );
		timer0 = QDateTime::currentMSecsSinceEpoch();
	}
	
	// determine segments with less than minSlices slices
	// const unsigned int numSortedIntersections = sortedIntersections.length(); 
	QHash<unsigned int, std::list< QList<float> > > labelHashMap;

	// iterate over all segments and count number of slices
	for (int i=0; i<numSlices; ++i)
	{
		const unsigned int numSegments = segmentFeatures[i].length();
		for (int j=0; j<numSegments; ++j)
		{
			if (!labelHashMap.contains(segmentFeatures[i][j][labelIndex]))
				labelHashMap[segmentFeatures[i][j][labelIndex]] = std::list< QList<float> >();

			labelHashMap[segmentFeatures[i][j][labelIndex]].push_back( segmentFeatures[i][j] );
		}			
	}

	Logger::GetInstance()->WriteLine( QString("Create label hash map for SSH took: ") + QString::number((float)(QDateTime::currentMSecsSinceEpoch() - timer0) / 1000.0) );
	timer0 = QDateTime::currentMSecsSinceEpoch();

	///////////////////// SINGLE SLICE FUSION HEURISTIC /////////////////////
	if (singleSliceHeuristic == true)
	{
		float timer1 = QDateTime::currentMSecsSinceEpoch();

		// iterate over all labels and merge segments that are smaller than minSlices
		QHash<unsigned int, std::list< QList<float> > >::const_iterator labelIterator = labelHashMap.constBegin();
		std::list< QPair<std::list< QList<float> >, unsigned int> > fusionSegmentQueue;

		while (labelIterator != labelHashMap.constEnd())
		{
			// calculate the volume of the segment
			unsigned int volume = 0;
			std::list< QList<float> >::const_iterator segmentIterator = labelIterator.value().cbegin();
			while (segmentIterator != labelIterator.value().cend())
			{
				volume += (*segmentIterator)[areaIndex];
				++segmentIterator;
			}

			// add the segment to the fusion queue if it is too small
			if (volume <= minimumVolume)
				fusionSegmentQueue.push_back( QPair<std::list< QList<float> >, unsigned int>(labelIterator.value(), volume) );

			// increment the label iterator
			++labelIterator;
		}

		if (debugOutput == true)
			Logger::GetInstance()->WriteLine( QString("Volume calculation took: ") + QString::number((QDateTime::currentMSecsSinceEpoch() - timer1) / 1000.0) );
		float timer2 = QDateTime::currentMSecsSinceEpoch();

		// initialize slice and label index variables
		unsigned int currentSlice1Index = 0;
		unsigned int currentSlice2Index = 0;
		unsigned int currentLabel1Index = 0;
		unsigned int currentLabel2Index = 0;
		unsigned int currentSegment1Label = 0;
		unsigned int currentSegment2Label = 0;

		// iterate over the queue and fuse the current segment
		while (!fusionSegmentQueue.empty())
		{
			// get the current label
			bool firstSegmentMatches = true;
			unsigned int newLabel = 0;
			float intersectionSimilarity = 0.0;
			const unsigned int currentLabel = labelIterator.key();

			// find the start and end positions
			std::list< QList<float> > currentSegment = fusionSegmentQueue.front().first;

			// get the border segments
			QList<float>* startSegment = &currentSegment.front();
			QList<float>* endSegment = &currentSegment.back();

			// find the most likely matches for the start segment and the end segment
			// as the list is sorted, if any match is found, this should be the most similar one
			std::list< QList<float> >::const_iterator startSliceIterator = intersectionHashMap.value((*startSegment)[sliceIndex]).cbegin();
			for (; startSliceIterator != intersectionHashMap.value((*startSegment)[sliceIndex]).cend(); ++startSliceIterator)
			{
				//currentIntersection = sortedIntersections[i].first;
				currentSlice1Index = startSliceIterator->at(slice1Index);
				currentSlice2Index = startSliceIterator->at(slice2Index);
				currentLabel1Index = startSliceIterator->at(label1Index);
				currentLabel2Index = startSliceIterator->at(label2Index);
				currentSegment1Label = segmentFeatures[currentSlice1Index][currentLabel1Index-minimumLabels[currentSlice1Index]][labelIndex];
				currentSegment2Label = segmentFeatures[currentSlice2Index][currentLabel2Index-minimumLabels[currentSlice2Index]][labelIndex];

				if (((*startSegment)[idIndex] == currentLabel1Index && (*startSegment)[sliceIndex] == currentSlice1Index && (*startSegment)[labelIndex] != currentSegment2Label))
				{
					newLabel = currentSegment2Label;
					firstSegmentMatches = true;
					intersectionSimilarity = startSliceIterator->at(similarityFeature);
					break;
				}

				if (((*startSegment)[idIndex] == currentLabel2Index && (*startSegment)[sliceIndex] == currentSlice2Index && (*startSegment)[labelIndex] != currentSegment1Label))
				{
					newLabel = currentSegment1Label;
					firstSegmentMatches = false;
					intersectionSimilarity = startSliceIterator->at(similarityFeature);
					break;
				}
			}


			std::list< QList<float> >::const_iterator endSliceIterator = intersectionHashMap.value((*endSegment)[sliceIndex]).cbegin();
			for (; endSliceIterator != intersectionHashMap.value((*endSegment)[sliceIndex]).cend(); ++endSliceIterator)
			{
				//currentIntersection = sortedIntersections[i].first;
				currentSlice1Index = endSliceIterator->at(slice1Index);
				currentSlice2Index = endSliceIterator->at(slice2Index);
				currentLabel1Index = endSliceIterator->at(label1Index);
				currentLabel2Index = endSliceIterator->at(label2Index);
				currentSegment1Label = segmentFeatures[currentSlice1Index][currentLabel1Index-minimumLabels[currentSlice1Index]][labelIndex];
				currentSegment2Label = segmentFeatures[currentSlice2Index][currentLabel2Index-minimumLabels[currentSlice2Index]][labelIndex];

				if (((*endSegment)[idIndex] == currentLabel1Index && (*endSegment)[sliceIndex] == currentSlice1Index && (*endSegment)[labelIndex] != currentSegment2Label))
				{
					if (endSliceIterator->at(similarityFeature) > intersectionSimilarity)
					{
						newLabel = currentSegment2Label;
						firstSegmentMatches = true;
						intersectionSimilarity = endSliceIterator->at(similarityFeature);
					}

					break;
				}

				if (((*endSegment)[idIndex] == currentLabel2Index && (*endSegment)[sliceIndex] == currentSlice2Index && (*endSegment)[labelIndex] != currentSegment1Label))
				{
					if (endSliceIterator->at(similarityFeature) > intersectionSimilarity)
					{
						newLabel = currentSegment1Label;
						firstSegmentMatches = false;
						intersectionSimilarity = endSliceIterator->at(similarityFeature);
					}

					break;
				}
			}

			if (debugOutput == true)
				Logger::GetInstance()->WriteLine( QString( "Fusing with : ") + QString::number(newLabel) );
			
			// calculate the fused volume
			unsigned int newVolume = fusionSegmentQueue.front().second;
			std::list< QList<float> > fusionSegment = labelHashMap.value( newLabel );
			std::list< QList<float> >::const_iterator fusionSegmentIterator = fusionSegment.cbegin();
			while (fusionSegmentIterator != fusionSegment.cend())
			{
				newVolume += (*fusionSegmentIterator)[areaIndex];
				++fusionSegmentIterator;
			}

			// check if maximum size constraint is violated by the merge
			if (newVolume <= maximumVolume && newLabel > 0)
			{
				// if a valid intersection was found, propagate its label to the current segment
				std::list< QList<float> >::iterator currentSegmentIterator = currentSegment.begin();

				while(currentSegmentIterator != currentSegment.end())
				{
					segmentFeatures[currentSegmentIterator->at(sliceIndex)][currentSegmentIterator->at(idIndex) - minimumLabels[currentSegmentIterator->at(sliceIndex)]][labelIndex] = newLabel;
					++currentSegmentIterator;
				}

				// if the size after the fusion is still smaller than the minimum size, add the segment to the queue again
				if (newVolume < minimumVolume)
				{
					// pop the fused segment and update the area of the 
					fusionSegmentQueue.pop_front();

					//const unsigned int numFusionSegments = fusionSegmentQueue.length();
					std::list< QPair<std::list< QList<float> >, unsigned int> >::iterator queueIterator = fusionSegmentQueue.begin();

					while (queueIterator != fusionSegmentQueue.end())
					{
						if (newLabel == queueIterator->first.front()[labelIndex])
						{
							// update the volume
							queueIterator->second = newVolume;

							if (firstSegmentMatches == true)
							{
								currentSegmentIterator = currentSegment.end();
								--currentSegmentIterator;
								for (; currentSegmentIterator != currentSegment.begin(); --currentSegmentIterator)
								{
									queueIterator->first.push_back(*currentSegmentIterator);
									labelHashMap[newLabel].push_back(*currentSegmentIterator);
								}
							}
							else
							{
								for (currentSegmentIterator = currentSegment.begin(); currentSegmentIterator != currentSegment.end(); ++currentSegmentIterator)
								{
									queueIterator->first.push_back(*currentSegmentIterator);
									labelHashMap[newLabel].push_back(*currentSegmentIterator);
								}
							}

							break;
						}

						++queueIterator;
					}
				}
				else if (newLabel == 0)
				{
					// remove objects whose closest match is the background and that are smaller than the minimum size
					std::list< QList<float> >::iterator currentSegmentIterator = currentSegment.begin();

					Logger::GetInstance()->WriteLine(QString("- Removing Unfusable Segment due to Size Criterion. Label index: ") + QString::number(segmentFeatures[currentSegmentIterator->at(sliceIndex)][currentSegmentIterator->at(idIndex) - minimumLabels[currentSegmentIterator->at(sliceIndex)]][labelIndex]) );

					while(currentSegmentIterator != currentSegment.end())
					{
						segmentFeatures[currentSegmentIterator->at(sliceIndex)][currentSegmentIterator->at(idIndex) - minimumLabels[currentSegmentIterator->at(sliceIndex)]][labelIndex] = newLabel;
						++currentSegmentIterator;
					}
					fusionSegmentQueue.pop_front();
				}
				else
				{
					fusionSegmentQueue.pop_front();
				}
			}
			else
			{
				fusionSegmentQueue.pop_front();
			}
		}

		if (debugOutput == true)
			Logger::GetInstance()->WriteLine( QString( "Processing the queue took: ") +QString::number((QDateTime::currentMSecsSinceEpoch() - timer2) / 1000.0) );
	}

	Logger::GetInstance()->WriteLine( QString("SSH took: ") + QString::number((float)(QDateTime::currentMSecsSinceEpoch() - timer0) / 1000.0) );
	timer0 = QDateTime::currentMSecsSinceEpoch();

	// check if there are remaining segments that are smaller than the minimum number of slices and remove them if necessary
	if (debugOutput == true)
	{
		for (int i=1; i<labelHashMap.size(); ++i)
		{
			if (labelHashMap[i].size() < minSlices)
				Logger::GetInstance()->WriteLine( QString("Segment ") + QString::number(i) + QString(" has only ") + QString::number(labelHashMap[i].size()) + QString(" slices --> remove.") );
		}
	}

	//////////////////////// LABEL IMAGE GENERATION /////////////////////////
	// generate the final label image
	typedef itk::CreateLabelImageFromFusionData<TInputImage, TInputImage> CreateLabelImageFromFusionDataFilter;
	typename CreateLabelImageFromFusionDataFilter::Pointer createLabelImageFilter = CreateLabelImageFromFusionDataFilter::New();
	createLabelImageFilter->SetInput( inputImage );
	createLabelImageFilter->SetInputMetaFilter( &segmentFeatures );
	createLabelImageFilter->SetLabelHashMap( &labelHashMap );
	createLabelImageFilter->SetUseRandomLabels( useRandomLabels );
	createLabelImageFilter->SetLabelFeature( labelFeature );
	createLabelImageFilter->SetColorFeature( colorFeature );
	createLabelImageFilter->SetMaxSize( maxSize );
	createLabelImageFilter->SetMinSlices( minSlices );
	itkTryCatch( createLabelImageFilter->Update(), "Exception Caught: Updating the creat label image from fusion data." );

	// set the result image
	ImageWrapper *outputImageWrapper = new ImageWrapper();
	outputImageWrapper->SetRescaleFlag( false );
    outputImageWrapper->SetImage<TInputImage>( createLabelImageFilter->GetOutput() );
    mOutputImages.append( outputImageWrapper );

	Logger::GetInstance()->WriteLine( QString("Label image generation took: ") + QString::number((float)(QDateTime::currentMSecsSinceEpoch() - timer0) / 1000.0) );
	timer0 = QDateTime::currentMSecsSinceEpoch();
	
	/////////////////////// CREATE META OUTPUT ////////////////////////
	// Create the meta data Object for output
	MetaDataFilter* metaOutput = this->mMetaOutputs.at(0);
    metaOutput->mIsMultiDimensional = true;
	metaOutput->mPostfix = "SliceBySliceRegionProps";
	
	// setup the meta description
	QStringList metaDescription;                    
	QStringList metaType;
    metaDescription << "id";						metaType << "int";
	metaDescription << "volume";					metaType << "int";
	metaDescription << "xpos";						metaType << "int";	// centroid
	metaDescription << "ypos";						metaType << "int";
	metaDescription << "zpos";						metaType << "int";
	metaDescription << "xsize";						metaType << "int";	// bounding box size
	metaDescription << "ysize";						metaType << "int";
	metaDescription << "zsize";						metaType << "int";
	metaDescription << "meanIntensity";				metaType << "float";
	metaDescription << "majorAxisLength";			metaType << "float";
	metaDescription << "minorAxisLength";			metaType << "float";
	metaDescription << "eccentricity";				metaType << "float";
	metaDescription << "elongation";				metaType << "float";
	metaDescription << "orientation";				metaType << "float";
	metaDescription << "seedLabel";					metaType << "int";
	metaDescription << "jiLabel";					metaType << "int";
	metaDescription << "sizeRatio";					metaType << "float";

	// write to meta object
	metaOutput->mTitle = metaDescription;
	metaOutput->mType = metaType;

	try
	{
		QList< QList< QList<float> > >::iterator sliceIterator = segmentFeatures.begin();
		for (; sliceIterator != segmentFeatures.end(); ++sliceIterator)
		{
			QList< QList<float> >::iterator segmentIterator = sliceIterator->begin();
			for (; segmentIterator != sliceIterator->end(); ++segmentIterator)
			{
				metaOutput->mData.append(*segmentIterator);
			}	
		}

		Logger::GetInstance()->WriteLine( QString("Meta output generation took: ") + QString::number((float)(QDateTime::currentMSecsSinceEpoch() - timer0) / 1000.0) );
		timer0 = QDateTime::currentMSecsSinceEpoch();
	}
	catch(itk::ExceptionObject& err)
	{
		std::cerr << "ExceptionObject caught !" << std::endl;
		std::cerr << err << std::endl;
		Logger::GetInstance()->WriteLine( QString("Meta output generation failed (Reason unknown so far :-) )!") );
	}

    // update the base class
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< SliceBySliceFusionFilterWidget<Image2Float> > SliceBySliceFusionFilterWidgetImage2Float;
static ProcessObjectProxy< SliceBySliceFusionFilterWidget<Image3Float> > SliceBySliceFusionFilterWidgetImage3Float;
static ProcessObjectProxy< SliceBySliceFusionFilterWidget<Image2UShort> > SliceBySliceFusionFilterWidgetImage2UShort;
static ProcessObjectProxy< SliceBySliceFusionFilterWidget<Image3UShort> > SliceBySliceFusionFilterWidgetImage3UShort;

} // namespace XPIWIT
