/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "RegionBasedWatershedImageFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkImageDuplicator.h"
#include "itkRegionOfInterestImageFilter.h"
#include "itkPasteImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkIntensityWindowingImageFilter.h"


namespace XPIWIT
{

template < class TInputImage >
RegionBasedWatershedImageFilterWidget< TInputImage >::RegionBasedWatershedImageFilterWidget() : ProcessObjectBase()
{
	this->mName = RegionBasedWatershedImageFilterWidget<TInputImage>::GetName();
	this->mDescription = "Performs a watershed segmentation on every cropped region.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(1);
	this->mObjectType->AppendMetaInputType("GeometryProperties");
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	//processObjectSettings->AddSetting( "Variance", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Variance of the gaussian kernel." );

	// initialize the widget
	ProcessObjectBase::Init();
}


template < class TInputImage >
RegionBasedWatershedImageFilterWidget< TInputImage >::~RegionBasedWatershedImageFilterWidget()
{
}


template < class TInputImage >
void RegionBasedWatershedImageFilterWidget< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	
	// get images
	const typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();

	typedef itk::ImageDuplicator< TInputImage > DuplicatorType;
	typename DuplicatorType::Pointer duplicator = DuplicatorType::New();
	duplicator->SetInputImage( inputImage );
	duplicator->Update();
	
	typename TInputImage::Pointer clonedImage = duplicator->GetOutput();
	clonedImage->FillBuffer( 0 );
	
	MetaDataFilter* geometryProps = this->mMetaInputs.at(0);

	for( int i = 0; i < geometryProps->mData.size(); i++ ){

		QList<float> line = geometryProps->mData.at( i );

		if( line.at( 1 ) > 5000 )
			continue;
 
		typename TInputImage::IndexType start;
		start[0] = line.at( 4 );
		start[1] = line.at( 5 );

		typename TInputImage::SizeType size;
		size[0] = line.at( 6 );
		size[1] = line.at( 7 );
 
		typename TInputImage::RegionType desiredRegion;
		desiredRegion.SetSize(size);
		desiredRegion.SetIndex(start);

		typedef itk::RegionOfInterestImageFilter< TInputImage, TInputImage > FilterType;
		typename FilterType::Pointer regionOfInterestFilter = FilterType::New();
		regionOfInterestFilter->SetRegionOfInterest( desiredRegion );
		regionOfInterestFilter->SetInput( inputImage );
		regionOfInterestFilter->Update();

		// filter
		//typedef itk::MinimumMaximumImageCalculator<TInputImage> MinMaxCalculatorType;
		//typename MinMaxCalculatorType::Pointer minMaxCalc = MinMaxCalculatorType::New();
		//minMaxCalc->SetImage( regionOfInterestFilter->GetOutput() );
		//minMaxCalc->Compute();

		//typename TInputImage::PixelType minimum = minMaxCalc->GetMinimum();
		//typename TInputImage::PixelType maximum = minMaxCalc->GetMaximum();

		//typedef itk::IntensityWindowingImageFilter<TInputImage, TInputImage> IntensityFilterType;
		//typename IntensityFilterType::Pointer intensityFilter = IntensityFilterType::New();
		//intensityFilter->SetInput( regionOfInterestFilter->GetOutput() );
		//intensityFilter->SetReleaseDataFlag( false );
		//intensityFilter->SetWindowMinimum( minimum );
		//intensityFilter->SetWindowMaximum( maximum );

		//if( typeid( TInputImage::PixelType ) == typeid( float ) ||
		//	typeid( TInputImage::PixelType ) == typeid( double ) )
		//{
		//	// float type
		//	intensityFilter->SetOutputMinimum( typename itk::NumericTraits< TInputImage::PixelType >::ZeroValue() );
		//	intensityFilter->SetOutputMaximum( typename itk::NumericTraits< TInputImage::PixelType >::OneValue() );
		//}else{
		//	// integral type
		//	intensityFilter->SetOutputMinimum( typename itk::NumericTraits< TInputImage::PixelType >::min() );
		//	intensityFilter->SetOutputMaximum( typename itk::NumericTraits< TInputImage::PixelType >::max() );
		//}
	
		//itkTryCatch(intensityFilter->Update(), "Error: IntensityWindowingImageFilter Update Function.");

		typedef itk::PasteImageFilter< TInputImage, TInputImage > PasteImageFilterType;
		typename PasteImageFilterType::Pointer pasteFilter = PasteImageFilterType::New ();
		pasteFilter->SetSourceImage( regionOfInterestFilter->GetOutput() );
		pasteFilter->SetDestinationImage( clonedImage );
		//pasteFilter->SetSourceRegion( desiredRegion );
		pasteFilter->SetDestinationIndex( start );

		itkTryCatch( pasteFilter->Update(), "Error: RegionBasedWatershedImageFilterWidget Update Function." );
		clonedImage = pasteFilter->GetOutput();
	}
	
	ImageWrapper *outputWrapper = new ImageWrapper();
	outputWrapper->SetImage<TInputImage>( clonedImage );
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< RegionBasedWatershedImageFilterWidget<Image2Float> > RegionBasedWatershedImageFilterWidgetImage2Float;
static ProcessObjectProxy< RegionBasedWatershedImageFilterWidget<Image3Float> > RegionBasedWatershedImageFilterWidgetImage3Float;
static ProcessObjectProxy< RegionBasedWatershedImageFilterWidget<Image2UShort> > RegionBasedWatershedImageFilterWidgetImage2UShort;
static ProcessObjectProxy< RegionBasedWatershedImageFilterWidget<Image3UShort> > RegionBasedWatershedImageFilterWidgetImage3UShort;

} // namespace XPIWIT

