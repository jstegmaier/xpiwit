/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "CropImageFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkExtractImageFilter.h"


// XPIWIT namespace
namespace XPIWIT
{

// the default constructor
template <class TInputImage>
CropImageFilterWidget<TInputImage>::CropImageFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = CropImageFilterWidget<TInputImage>::GetName();
	this->mDescription = "Crop a certain region from the input image.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "MinX", "-1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The minimum index in x-direction. Set to -1 to leave it unchanged." );
    processObjectSettings->AddSetting( "MinY", "-1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The minimum index in y-direction. Set to -1 to leave it unchanged." );
    processObjectSettings->AddSetting( "MinZ", "-1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The minimum index in z-direction. Set to -1 to leave it unchanged." );
    processObjectSettings->AddSetting( "MaxX", "-1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The maximum index in x-direction. Set to -1 to leave it unchanged." );
    processObjectSettings->AddSetting( "MaxY", "-1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The maximum index in y-direction. Set to -1 to leave it unchanged." );
    processObjectSettings->AddSetting( "MaxZ", "-1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The maximum index in z-direction. Set to -1 to leave it unchanged." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template <class TInputImage>
CropImageFilterWidget<TInputImage>::~CropImageFilterWidget()
{
}


// the update function
template <class TInputImage>
void CropImageFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    int min[TInputImage::ImageDimension];
    int max[TInputImage::ImageDimension];

	QStringList minDirections;
	minDirections << "MinX" << "MinY" << "MinZ";
	QStringList maxDirections;
	maxDirections << "MaxX" << "MaxY" << "MaxZ";

	for( int i = 0; i < TInputImage::ImageDimension; i++){
		min[i] = processObjectSettings->GetSettingValue( minDirections.at( i ) ).toInt();
		max[i] = processObjectSettings->GetSettingValue( maxDirections.at( i ) ).toInt();
	}

    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    bool inPlace = processObjectSettings->GetSettingValue( "InPlace" ).toInt() > 0;
    bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

    // extract sub region
    typedef itk::ExtractImageFilter<TInputImage, TInputImage> ExtractImageFilterType;
    typename ExtractImageFilterType::Pointer extractImageFilter = ExtractImageFilterType::New();
    typename TInputImage::RegionType largestPossibleRegion = inputImage->GetLargestPossibleRegion();
    typename TInputImage::RegionType desiredRegion;

    // set to old values if -1 is specified
    for( int i = 0; i < TInputImage::ImageDimension; i++){
        min[i] = (min[i] == -1) ? largestPossibleRegion.GetIndex(i) : min[i];
        max[i] = (max[i] == -1) ? largestPossibleRegion.GetSize(i) : max[i];

        // set the new region
        desiredRegion.SetIndex(i, min[i]);
        desiredRegion.SetSize(i, max[i] - min[i]);
    }

    // extract the desired region
    extractImageFilter->SetExtractionRegion( desiredRegion );
    extractImageFilter->SetNumberOfWorkUnits( maxThreads );
    extractImageFilter->SetInPlace( inPlace );
    extractImageFilter->SetReleaseDataFlag( releaseDataFlag );
    extractImageFilter->SetInput( inputImage );

    // update the extraction filter
    itkTryCatch( extractImageFilter->Update(), "CropImageFilterWidget --> Extract Image Filter." );

    // set the output
	ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( extractImageFilter->GetOutput() );
    mOutputImages.append( outputImage );

    // update the process widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< CropImageFilterWidget<Image2Float> > CropImageFilterWidgetImage2Float;
static ProcessObjectProxy< CropImageFilterWidget<Image3Float> > CropImageFilterWidgetImage3Float;
static ProcessObjectProxy< CropImageFilterWidget<Image2UShort> > CropImageFilterWidgetImage2UShort;
static ProcessObjectProxy< CropImageFilterWidget<Image3UShort> > CropImageFilterWidgetImage3UShort;

} // namespace XPIWIT
