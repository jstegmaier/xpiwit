/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "FuseRotationImagesFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"
#include "../../../Filter/ITKCustom/itkFuseRotationImagesFilter.h"

namespace XPIWIT
{

template < class TInputImage >
FuseRotationImagesFilterWidget< TInputImage >::FuseRotationImagesFilterWidget() : ProcessObjectBase()
{
	this->mName = FuseRotationImagesFilterWidget<TInputImage>::GetName();
	this->mDescription = "Pixel-wise subtraction of two images.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(2);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "TranslationX", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The x component of the translation vector." );
    processObjectSettings->AddSetting( "TranslationY", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The y component of the translation vector." );
    processObjectSettings->AddSetting( "TranslationZ", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The z component of the translation vector." );
    processObjectSettings->AddSetting( "RotationMatrix00", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The 00 component of the rotation matrix." );
    processObjectSettings->AddSetting( "RotationMatrix01", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The 01 component of the rotation matrix." );
    processObjectSettings->AddSetting( "RotationMatrix02", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The 01 component of the rotation matrix." );
    processObjectSettings->AddSetting( "RotationMatrix10", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The 10 component of the rotation matrix." );
    processObjectSettings->AddSetting( "RotationMatrix11", "-1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The 11 component of the rotation matrix." );
    processObjectSettings->AddSetting( "RotationMatrix12", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The 12 component of the rotation matrix." );
    processObjectSettings->AddSetting( "RotationMatrix20", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The 20 component of the rotation matrix." );
    processObjectSettings->AddSetting( "RotationMatrix21", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The 21 component of the rotation matrix." );
    processObjectSettings->AddSetting( "RotationMatrix22", "-1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The 22 component of the rotation matrix." );
	
	// initialize the widget
	ProcessObjectBase::Init();
}


template < class TInputImage >
FuseRotationImagesFilterWidget< TInputImage >::~FuseRotationImagesFilterWidget()
{
}


template < class TInputImage >
void FuseRotationImagesFilterWidget< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// initialize the transformation vector and matrix
	vnl_matrix_fixed<double,3,3> rotation;
    vnl_vector_fixed<double,3> translation;

	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	translation(0) = processObjectSettings->GetSettingValue( "TranslationX" ).toFloat();
	translation(1) = processObjectSettings->GetSettingValue( "TranslationY" ).toFloat();
	translation(2) = processObjectSettings->GetSettingValue( "TranslationZ" ).toFloat();
	rotation(0,0) = processObjectSettings->GetSettingValue( "RotationMatrix00" ).toFloat();
	rotation(0,1) = processObjectSettings->GetSettingValue( "RotationMatrix01" ).toFloat();
	rotation(0,2) = processObjectSettings->GetSettingValue( "RotationMatrix02" ).toFloat();
	rotation(1,0) = processObjectSettings->GetSettingValue( "RotationMatrix10" ).toFloat();
	rotation(1,1) = processObjectSettings->GetSettingValue( "RotationMatrix11" ).toFloat();
	rotation(1,2) = processObjectSettings->GetSettingValue( "RotationMatrix12" ).toFloat();
	rotation(2,0) = processObjectSettings->GetSettingValue( "RotationMatrix20" ).toFloat();
	rotation(2,1) = processObjectSettings->GetSettingValue( "RotationMatrix21" ).toFloat();
	rotation(2,2) = processObjectSettings->GetSettingValue( "RotationMatrix22" ).toFloat();

	// get images
	typename TInputImage::Pointer inputImage1 = mInputImages.at(0)->template GetImage<TInputImage>();
	typename TInputImage::Pointer inputImage2 = mInputImages.at(1)->template GetImage<TInputImage>();
		
	// start processing
	ProcessObjectBase::StartTimer();
	
	// setup the filter
	typedef itk::FuseRotationImagesFilter<TInputImage> FuseRotationImagesFilterType;
	typename FuseRotationImagesFilterType::Pointer fuseRotationsFilter = FuseRotationImagesFilterType::New();
	fuseRotationsFilter->SetInput( inputImage1 );
	fuseRotationsFilter->SetRotationImage( inputImage2 );
	fuseRotationsFilter->SetRotationMatrix( rotation );
	fuseRotationsFilter->SetTranslation( translation );
	fuseRotationsFilter->SetReleaseDataFlag( false );
	
	itkTryCatch(fuseRotationsFilter->Update(), "Error: FuseRotationImagesFilterWidget Update Function.");
	
	ImageWrapper *outputWrapper = new ImageWrapper();
	outputWrapper->SetImage<TInputImage>( fuseRotationsFilter->GetOutput() );
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< FuseRotationImagesFilterWidget<Image2Float> > FuseRotationImagesFilterWidgetImage2Float;
static ProcessObjectProxy< FuseRotationImagesFilterWidget<Image3Float> > FuseRotationImagesFilterWidgetImage3Float;
static ProcessObjectProxy< FuseRotationImagesFilterWidget<Image2UShort> > FuseRotationImagesFilterWidgetImage2UShort;
static ProcessObjectProxy< FuseRotationImagesFilterWidget<Image3UShort> > FuseRotationImagesFilterWidgetImage3UShort;

} // namespace XPIWIT

