/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "NormalizeIntensityImageFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkIntensityWindowingImageFilter.h"
#include "itkStatisticsImageFilter.h"
#include "itkMultiplyImageFilter.h"
#include "itkAddImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkNumericTraits.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
NormalizeIntensityImageFilterWidget< TInputImage >::NormalizeIntensityImageFilterWidget() : ProcessObjectBase()
{
	this->mName = NormalizeIntensityImageFilterWidget<TInputImage>::GetName();
	this->mDescription = "Rescales the image from min to max for integer types and from 0 to 1 for float types";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);
    
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "NormalizationMode", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Normalization mode. 0: Min-Max -> 0-1, 1: Mean 0, Std.Dev. 1, 2: max -> 1" );
    processObjectSettings->AddSetting( "AddConstant", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Optionally add a constant offset." );
    processObjectSettings->AddSetting( "MultiplyByConstant", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Optionally multiply a constant factor." );
    
	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
NormalizeIntensityImageFilterWidget< TInputImage >::~NormalizeIntensityImageFilterWidget()
{
}


// the update function
template < class TInputImage >
void NormalizeIntensityImageFilterWidget< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
    const int normalizationMode = processObjectSettings->GetSettingValue( "NormalizationMode" ).toInt();
    const float addConstant = processObjectSettings->GetSettingValue( "AddConstant" ).toFloat();
    const float multiplyByConstant = processObjectSettings->GetSettingValue( "MultiplyByConstant" ).toFloat();
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	
	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();
    
    // initialize result image pointer
    typename TInputImage::Pointer resultImage;
    
   // compute the image statistics
   typedef itk::StatisticsImageFilter< TInputImage > StatisticsImageFilterType;
   typename StatisticsImageFilterType::Pointer statisticsImageFilter = StatisticsImageFilterType::New();
   statisticsImageFilter->SetInput( inputImage );
   itkTryCatch( statisticsImageFilter->Update() , "Error: LabelStatisticsImageFilterWrapper Update Function.");
   
    std::cout << "- Image statistics before normalization. Min = " << statisticsImageFilter->GetMinimum() << ", Max = " << statisticsImageFilter->GetMaximum() << ", Mean = " << statisticsImageFilter->GetMean() << ", Std.Dev. = " << statisticsImageFilter->GetSigma() << std::endl;
    
    
    // Normalization mode 0: Min-Max -> 0-1
    if (normalizationMode == 0 || normalizationMode == 2)
    {
        // apply the linear transformation
        typedef itk::IntensityWindowingImageFilter<TInputImage, TInputImage> IntensityFilterType;
        typename IntensityFilterType::Pointer intensityFilter = IntensityFilterType::New();
        intensityFilter->SetInput( inputImage );
        intensityFilter->SetReleaseDataFlag( true );
        
        if (normalizationMode == 0)
            intensityFilter->SetWindowMinimum( statisticsImageFilter->GetMinimum()  );
        else
            intensityFilter->SetWindowMinimum( 0.0 );
        intensityFilter->SetWindowMaximum( statisticsImageFilter->GetMaximum()  );
        
        if( typeid( typename TInputImage::PixelType ) == typeid( float ) ||
            typeid( typename TInputImage::PixelType ) == typeid( double ) )
        {
            // float type
            intensityFilter->SetOutputMinimum( itk::NumericTraits< typename TInputImage::PixelType >::ZeroValue() );
            intensityFilter->SetOutputMaximum( itk::NumericTraits< typename TInputImage::PixelType >::OneValue() );
        }
        else
        {
            // int type
            intensityFilter->SetOutputMinimum( itk::NumericTraits< typename TInputImage::PixelType >::min() );
            intensityFilter->SetOutputMaximum( itk::NumericTraits< typename TInputImage::PixelType >::max() );
        }
        
        itkTryCatch(intensityFilter->Update(), "Error: NormalizeIntensityImageFilterWidget Update Function.");
        resultImage = intensityFilter->GetOutput();
    }
    
    // Normalization mode 1: Mean 0, Std.Dev. 1
    else if (normalizationMode == 1)
    {
        typedef itk::AddImageFilter< TInputImage > AddImageFilterType;
        typename AddImageFilterType::Pointer addImageFilter = AddImageFilterType::New();
        addImageFilter->SetInput1( inputImage );
        addImageFilter->SetConstant2( -statisticsImageFilter->GetMean() );
        itkTryCatch( addImageFilter->Update() , "Error: AddImageFilter Update Function.");
        
        typedef itk::MultiplyImageFilter< TInputImage > MultiplyImageFilterType;
        typename MultiplyImageFilterType::Pointer multiplyImageFilter = MultiplyImageFilterType::New();
        multiplyImageFilter->SetInput( addImageFilter->GetOutput() );
        multiplyImageFilter->SetConstant((float)1.0/statisticsImageFilter->GetSigma());
        itkTryCatch( multiplyImageFilter->Update() , "Error: MultiplyImageFilter Update Function.");
        resultImage = multiplyImageFilter->GetOutput();
    }
    else
    {
        resultImage = inputImage;
    }
        
    if (multiplyByConstant != 1.0)
    {
        typedef itk::MultiplyImageFilter< TInputImage > MultiplyImageFilterType;
        typename MultiplyImageFilterType::Pointer multiplyImageFilter = MultiplyImageFilterType::New();
        multiplyImageFilter->SetInput( resultImage );
        multiplyImageFilter->SetConstant(multiplyByConstant);
        itkTryCatch( multiplyImageFilter->Update() , "Error: MultiplyImageFilter Update Function.");
        
        resultImage = multiplyImageFilter->GetOutput();
    }

    if (addConstant != 0.0)
    {
        typedef itk::AddImageFilter< TInputImage > AddImageFilterType;
        typename AddImageFilterType::Pointer addImageFilter = AddImageFilterType::New();
        addImageFilter->SetInput1( resultImage );
        addImageFilter->SetConstant2(addConstant);
        itkTryCatch( addImageFilter->Update() , "Error: AddImageFilter Update Function.");
        
        resultImage = addImageFilter->GetOutput();
    }
    
    statisticsImageFilter->SetInput( resultImage );
    itkTryCatch( statisticsImageFilter->Update() , "Error: LabelStatisticsImageFilterWrapper Update Function.");
    
    std::cout << "- Image statistics after normalization. Min = " << statisticsImageFilter->GetMinimum() << ", Max = " << statisticsImageFilter->GetMaximum() << ", Mean = " << statisticsImageFilter->GetMean() << ", Std.Dev. = " << statisticsImageFilter->GetSigma() << std::endl;
    
    
	ImageWrapper *outputWrapper = new ImageWrapper();
    outputWrapper->SetRescaleFlag(false);
	outputWrapper->SetImage<TInputImage>( resultImage );
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< NormalizeIntensityImageFilterWidget<Image2Float> > NormalizeIntensityImageFilterWidgetImage2Float;
static ProcessObjectProxy< NormalizeIntensityImageFilterWidget<Image3Float> > NormalizeIntensityImageFilterWidgetImage3Float;
static ProcessObjectProxy< NormalizeIntensityImageFilterWidget<Image2UShort> > NormalizeIntensityImageFilterWidgetImage2UShort;
static ProcessObjectProxy< NormalizeIntensityImageFilterWidget<Image3UShort> > NormalizeIntensityImageFilterWidgetImage3UShort;

} // namespace XPIWIT

