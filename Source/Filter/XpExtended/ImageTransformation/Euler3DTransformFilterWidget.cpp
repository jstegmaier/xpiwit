/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "Euler3DTransformFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkResampleImageFilter.h"
#include "itkEuler3DTransform.h"
#include "itkEuler2DTransform.h"


namespace XPIWIT {

// the default constructor
template <class TInputImage>
Euler3DTransformFilterWidget<TInputImage>::Euler3DTransformFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = Euler3DTransformFilterWidget<TInputImage>::GetName();
	this->mDescription = "Performs a rigid transformation of the input image accordint to specified translation and rotation angles.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "PositionX", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The x component of the translation vector." );
    processObjectSettings->AddSetting( "PositionY", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The y component of the translation vector." );
    processObjectSettings->AddSetting( "PositionZ", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The z component of the translation vector." );
    processObjectSettings->AddSetting( "Roll", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The roll angle." );
    processObjectSettings->AddSetting( "Yaw", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The yaw angle." );
    processObjectSettings->AddSetting( "Pitch", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The pitch angle." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template <class TInputImage>
Euler3DTransformFilterWidget<TInputImage>::~Euler3DTransformFilterWidget()
{
}


// the update function
template <class TInputImage>
void Euler3DTransformFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();

    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get the parameter values
    float roll = processObjectSettings->GetSettingValue( "Roll" ).toDouble();
    float yaw = processObjectSettings->GetSettingValue( "Yaw" ).toDouble();
    float pitch = processObjectSettings->GetSettingValue( "Pitch" ).toDouble();
    float positionX = processObjectSettings->GetSettingValue( "PositionX" ).toDouble();
    float positionY = processObjectSettings->GetSettingValue( "PositionY" ).toDouble();
    float positionZ = processObjectSettings->GetSettingValue( "PositionZ" ).toDouble();
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();

	// get the input image
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

    // set image sizes
    typename TInputImage::PointType origin = inputImage->GetOrigin();
    typename TInputImage::SizeType inputSize = inputImage->GetLargestPossibleRegion().GetSize();
    typename TInputImage::SizeType outputSize = inputImage->GetLargestPossibleRegion().GetSize();
    typename TInputImage::SpacingType outputSpacing = inputImage->GetSpacing();

    // define the transformation type
    typedef itk::Euler3DTransform<double> Euler3DTransformType;
    Euler3DTransformType::Pointer euler3DTransform = Euler3DTransformType::New();

    // define the rotation center to lie in the middle of the image.
    // TODO: allow different rotation centers
    typename Euler3DTransformType::InputPointType center;
    center[0] = inputSize[0]*outputSpacing[0]/2.0;
    center[1] = inputSize[1]*outputSpacing[1]/2.0;
    center[2] = inputSize[2]*outputSpacing[2]/2.0;

    // set transformation parameters
    typename Euler3DTransformType::OutputVectorType translation;
    translation[0] = positionX;
    translation[1] = positionY;
    translation[2] = positionZ;
    euler3DTransform->SetCenter( center );
    euler3DTransform->SetRotation( roll, yaw, pitch );
    euler3DTransform->Translate( translation );

    // resample the image using the specified transformation
    typedef itk::ResampleImageFilter<TInputImage, TInputImage> ResampleImageFilterType;
    typename ResampleImageFilterType::Pointer resample = ResampleImageFilterType::New();
    resample->SetInput( inputImage );
    resample->SetNumberOfWorkUnits( maxThreads );
    resample->SetSize(outputSize);
    resample->SetOutputSpacing( outputSpacing );
    resample->SetOutputOrigin( origin );
    resample->SetTransform( euler3DTransform );
    resample->SetReleaseDataFlag( processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0 );

    itkTryCatch( resample->Update(), "Error: Updating Euler3DTransformFilterWidget failed!" );

    ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( resample->GetOutput() );
    mOutputImages.append( outputImage );

    // update the process widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< Euler3DTransformFilterWidget<Image3Float> > Euler3DTransformFilterWidgetImage3Float;

} // namespace XPIWIT
