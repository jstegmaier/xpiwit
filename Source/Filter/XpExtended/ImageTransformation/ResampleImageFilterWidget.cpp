/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "ResampleImageFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkResampleImageFilter.h"
#include "itkScaleTransform.h"
#include "itkIdentityTransform.h"
#include "itkNearestNeighborInterpolateImageFunction.h"
#include "itkMinimumMaximumImageCalculator.h"


namespace XPIWIT
{

// the default constructor
template< class TInputImage >
ResampleImageFilterWidget<TInputImage>::ResampleImageFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = ResampleImageFilterWidget<TInputImage>::GetName();
	this->mDescription = "Resize the filter according to the specified size or scaling factors.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Width", "-1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The new width for the image." );
    processObjectSettings->AddSetting( "Height", "-1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The new height for the image." );
    processObjectSettings->AddSetting( "Depth", "-1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The new depth for the image." );
    processObjectSettings->AddSetting( "ScaleX", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The scaling factor for the x dimension of the image." );
    processObjectSettings->AddSetting( "ScaleY", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The scaling factor for the y dimension of the image." );
    processObjectSettings->AddSetting( "ScaleZ", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The scaling factor for the z dimension of the image." );
    processObjectSettings->AddSetting( "UseScaleFactors", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, the scaling factors are used for resizing." );
	processObjectSettings->AddSetting( "InterpolationType", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "0: NearestNeighbor, 1: LinearInterpolation" );


    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage>
ResampleImageFilterWidget<TInputImage>::~ResampleImageFilterWidget()
{
}


// the update function
template< class TInputImage>
void ResampleImageFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get the parameter values
    int newWidth = processObjectSettings->GetSettingValue( "Width" ).toInt();
    int newHeight = processObjectSettings->GetSettingValue( "Height" ).toInt();
    int newDepth = processObjectSettings->GetSettingValue( "Depth" ).toInt();
    float scaleX = processObjectSettings->GetSettingValue( "ScaleX" ).toDouble();
    float scaleY = processObjectSettings->GetSettingValue( "ScaleY" ).toDouble();
    float scaleZ = processObjectSettings->GetSettingValue( "ScaleZ" ).toDouble();
    bool useScaleFactors = processObjectSettings->GetSettingValue( "UseScaleFactors" ).toInt() > 0;
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const int interpolationType = processObjectSettings->GetSettingValue( "InterpolationType" ).toInt();

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	inputImage->SetReleaseDataFlag( false );

	typename itk::MinimumMaximumImageCalculator< TInputImage >::Pointer minMaxCalculator = itk::MinimumMaximumImageCalculator< TInputImage >::New();
	minMaxCalculator->SetImage(inputImage);
	minMaxCalculator->ComputeMinimum();
	minMaxCalculator->ComputeMaximum();
	float minimum = minMaxCalculator->GetMinimum();
	float maximum = minMaxCalculator->GetMaximum();
	Logger::GetInstance()->WriteLine(QString("- Input Minimum Maximum Values are: ") + QString::number(minimum) + ", " + QString::number(maximum));

    // set image sizes
    typename TInputImage::PointType origin = inputImage->GetOrigin();
    typename TInputImage::SizeType inputSize = inputImage->GetLargestPossibleRegion().GetSize();
    typename TInputImage::SizeType outputSize = inputImage->GetLargestPossibleRegion().GetSize();
    typename TInputImage::SpacingType inputSpacing = inputImage->GetSpacing();
    typename TInputImage::SpacingType outputSpacing = inputImage->GetSpacing();

    // initialize the transformation for rescaling to the desired size.
	typedef itk::IdentityTransform<double, TInputImage::ImageDimension> IdentityTransformType;
	typename IdentityTransformType::Pointer scaleTransform = IdentityTransformType::New();
	scaleTransform->SetIdentity();

	itk::FixedArray<double, 3> scale = 1;
	typename TInputImage::RegionType::SizeType newSize;


    if (useScaleFactors == false)
    {
		if (newWidth > 0)
			scale[0] = (double)newWidth / (double)inputSize[0];

		if (newHeight > 0)
			scale[1] = (double)newHeight / (double)inputSize[1];
		
		if (newDepth > 0)
			scale[2] = (double)newDepth / (double)inputSize[2];

		newSize[0] = newWidth;
		newSize[1] = newHeight;
		newSize[2] = newDepth;
    }
    else
    {
		scale[0] = scaleX; scale[1] = scaleY; scale[2] = scaleZ;
    }

	for (int i=0; i<TInputImage::ImageDimension; ++i)
		outputSize[i] = (int)((double)inputSize[i] * scale[i]);

    // set output spacing and the origin
    Logger::GetInstance()->WriteLine( QString("- Changed image spacing to [" ));
	for (int i=0; i<TInputImage::ImageDimension; ++i)
    {
        outputSpacing[i] = (double)inputSpacing[i] * (double)inputSize[i] / (double)outputSize[i];
        origin[i] = origin[i] / outputSpacing[i];

		Logger::GetInstance()->WriteLine( QString().number(outputSpacing[i]) + QString(", ") );
    }
	Logger::GetInstance()->WriteLine( QString("]") );

    // resample the image using the specified transform
    typedef itk::ResampleImageFilter<TInputImage, TInputImage> ResampleImageFilterType;
    typename ResampleImageFilterType::Pointer resampleFilter = ResampleImageFilterType::New();
	resampleFilter->SetTransform(scaleTransform);
    resampleFilter->SetInput( inputImage );
	resampleFilter->SetSize(newSize);
    //resampleFilter->SetOutputSpacing( outputSpacing );
    //resampleFilter->SetOutputOrigin( origin );

	Logger::GetInstance()->WriteLine( QString("- Changed image origin to [" ));
	for (int i=0; i<TInputImage::ImageDimension; ++i)
    {
		Logger::GetInstance()->WriteLine( QString().number(origin[i]) + QString(", ") );
    }
	Logger::GetInstance()->WriteLine( QString("]") );

	Logger::GetInstance()->WriteLine( QString("- Changed output image size to [" ));
	for (int i=0; i<TInputImage::ImageDimension; ++i)
    {
		Logger::GetInstance()->WriteLine( QString().number(outputSize[i]) + QString(", ") );
    }
	Logger::GetInstance()->WriteLine( QString("]") );


    
	if (interpolationType == 0)
	{
		typedef itk::NearestNeighborInterpolateImageFunction<TInputImage, double > InterpolatorType;
		typename InterpolatorType::Pointer interpolator = InterpolatorType::New();
		resampleFilter->SetInterpolator( interpolator );
	}
	else if (interpolationType == 1)
	{
		typedef itk::LinearInterpolateImageFunction<TInputImage, double > InterpolatorType;
		typename InterpolatorType::Pointer interpolator = InterpolatorType::New();
		resampleFilter->SetInterpolator( interpolator );
	}

	resampleFilter->SetReleaseDataFlag(false);
	resampleFilter->SetSize( outputSize );
	resampleFilter->SetOutputSpacing(outputSpacing);
    itkTryCatch( resampleFilter->Update(), "ExceptionObject caught while updating the ResampleImageFilter!" );


	ImageWrapper *outputImage = new ImageWrapper();
	outputImage->SetRescaleFlag(false);
    outputImage->SetImage<TInputImage>( resampleFilter->GetOutput() );
    mOutputImages.append( outputImage );

    // update the process widget
    ProcessObjectBase::Update();
	//resampleFilter->SetReleaseDataFlag( true );

    // log the performance
    ProcessObjectBase::LogPerformance();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< ResampleImageFilterWidget<Image2Float> > ResampleImageFilterWidgetImage2Float;
static ProcessObjectProxy< ResampleImageFilterWidget<Image3Float> > ResampleImageFilterWidgetImage3Float;
static ProcessObjectProxy< ResampleImageFilterWidget<Image2UShort> > ResampleImageFilterWidgetImage2UShort;
static ProcessObjectProxy< ResampleImageFilterWidget<Image3UShort> > ResampleImageFilterWidgetImage3UShort;

} // namespace XPIWIT
