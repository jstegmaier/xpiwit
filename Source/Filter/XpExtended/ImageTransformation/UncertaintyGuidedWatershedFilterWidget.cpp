/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "UncertaintyGuidedWatershedFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"
#include "../../ITKCustom/itkLabelImageToUncertaintyMapFilter.h"
#include "itkImageRegionIterator.h"
#include "itkPasteImageFilter.h"
#include "itkMorphologicalWatershedFromMarkersImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "../../ITKCustom/itkUncertaintyGuidedWatershedSegmentationFilter.h"

namespace XPIWIT
{

// the default constructor
template< class TInputImage >
UncertaintyGuidedWatershedFilterWidget<TInputImage>::UncertaintyGuidedWatershedFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = UncertaintyGuidedWatershedFilterWidget<TInputImage>::GetName();
	this->mDescription = "Multiplies the two provided images using a third image as an uncertainty map.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(3);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(2);
	this->mObjectType->AppendMetaInputType("RegionProps");
	this->mObjectType->AppendMetaInputType("FuzzySetParameters");
    this->mObjectType->SetNumberMetaOutputs(0);
	
    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "FullyConnected", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Enabels/disables the fully connected option. I.e. defines connectivity by 4/6 vs. 8/26 neighborhood." );
	processObjectSettings->AddSetting( "UncertaintyCombinationFunction", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Multiplication: 0, Min: 1, Max: 2, Weighted Average: 3." );
	processObjectSettings->AddSetting( "UncertaintyThreshold", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Splits only segments points with uncertainty values below the threshold. RegionProps+FuzzySets required as inputs." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
UncertaintyGuidedWatershedFilterWidget<TInputImage>::~UncertaintyGuidedWatershedFilterWidget()
{
}


// the update function
template< class TInputImage >
void UncertaintyGuidedWatershedFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	const float uncertaintyThreshold = processObjectSettings->GetSettingValue( "UncertaintyThreshold" ).toFloat();
	const bool fullyConnected = processObjectSettings->GetSettingValue( "FullyConnected" ).toInt() > 0;
	const int uncertaintyCombinationFunction = processObjectSettings->GetSettingValue( "UncertaintyCombinationFunction" ).toInt();
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();

	// get the input images
	typename TInputImage::Pointer distanceMapImage = mInputImages.at(0)->template GetImage<TInputImage>();
	typename TInputImage::Pointer labelImage = mInputImages.at(1)->template GetImage<TInputImage>();
	typename TInputImage::Pointer seedImage = mInputImages.at(2)->template GetImage<TInputImage>();

	// get the meta data and specify indices
	MetaDataFilter* regionProps = this->mMetaInputs.at(0);
	MetaDataFilter* fuzzySetParameters = this->mMetaInputs.at(1);

	typename itk::UncertaintyGuidedWatershedSegmentationFilter<TInputImage>::Pointer watershedFilter = itk::UncertaintyGuidedWatershedSegmentationFilter<TInputImage>::New();
	watershedFilter->SetInput( distanceMapImage );
	watershedFilter->SetLabelImage( labelImage );
	watershedFilter->SetSeedImage( seedImage );
	watershedFilter->SetFullyConnected( fullyConnected );
	watershedFilter->SetUncertaintyCombinationFunction( uncertaintyCombinationFunction );
	watershedFilter->SetUncertaintyThreshold( uncertaintyThreshold );
	watershedFilter->SetRegionProps( regionProps );
	watershedFilter->SetFuzzySetParameters( fuzzySetParameters );
	watershedFilter->SetNumberOfWorkUnits( maxThreads );
	itkTryCatch( watershedFilter->Update(), "Exception Caught: Uncertainty Guided Watershed Filter Update!" );

	// set the output image   
	ImageWrapper *outputImageWrapper = new ImageWrapper();
    outputImageWrapper->SetImage<TInputImage>( watershedFilter->GetOutput() );
    mOutputImages.append( outputImageWrapper );

    // update the base class
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< UncertaintyGuidedWatershedFilterWidget<Image2Float> > UncertaintyGuidedWatershedFilterWidgetImage2Float;
static ProcessObjectProxy< UncertaintyGuidedWatershedFilterWidget<Image3Float> > UncertaintyGuidedWatershedFilterWidgetImage3Float;
static ProcessObjectProxy< UncertaintyGuidedWatershedFilterWidget<Image2UShort> > UncertaintyGuidedWatershedFilterWidgetImage2UShort;
static ProcessObjectProxy< UncertaintyGuidedWatershedFilterWidget<Image3UShort> > UncertaintyGuidedWatershedFilterWidgetImage3UShort;

} // namespace XPIWIT
