/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "MaximumProjectionImageFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkResampleImageFilter.h"
#include "itkEuler3DTransform.h"
#include "itkMaximumProjectionImageFilter.h"
#include "itkMedianProjectionImageFilter.h"
#include "itkPermuteAxesImageFilter.h"
#include "itkExtractImageFilter.h"


namespace XPIWIT
{

// the default constructor
template <class TInputImage>
MaximumProjectionImageFilterWidget<TInputImage>::MaximumProjectionImageFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = MaximumProjectionImageFilterWidget<TInputImage>::GetName();
    this->mDescription = "Maximum Projection Filter. ";
    this->mDescription += "Maps the input to an output with one dimension less by storing the biggest value of the dimension that gets lost.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings to the filter
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "ProjectionDimension", "2", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The dimension that will be projected." );
	processObjectSettings->AddSetting( "NumSlices", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "If larger than 1, the image will be divided by N parts in the projection dimension. Result image frames correspond to the maximum projection of the specified part." );
	processObjectSettings->AddSetting( "NumRows", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "If larger than 1, the image will have N rows containing the maximum projection slices." );
	processObjectSettings->AddSetting( "NumColumns", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "If larger than 1, the image will have N columns containing the maximum projection slices." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template <class TInputImage>
MaximumProjectionImageFilterWidget<TInputImage>::~MaximumProjectionImageFilterWidget()
{
}


// the update function
template <class TInputImage>
void MaximumProjectionImageFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    const int projectionDimension = processObjectSettings->GetSettingValue( "ProjectionDimension" ).toInt();
	const int numSlices = processObjectSettings->GetSettingValue( "NumSlices" ).toInt();
	const int numRows = processObjectSettings->GetSettingValue( "NumRows" ).toInt();
	const int numColumns = processObjectSettings->GetSettingValue( "NumColumns" ).toInt();

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	typename TInputImage::Pointer outputImage = TInputImage::New();
	
	bool oldReleaseDataFlag = inputImage->GetReleaseDataFlag();
	inputImage->SetReleaseDataFlag( false );

    typename TInputImage::RegionType outputRegion;

	// if slices is larger than one, a mosaic image is generated
	if (numSlices > 1)
	{
		// calculate the slice width
		const int sliceWidth = int(0.5+(float)inputImage->GetLargestPossibleRegion().GetSize(projectionDimension) / (float)numSlices);
	
		for (int i=0; i<numSlices; ++i)
		{
			// reset the release data flag in the last run
			if (i+1 == numSlices)
				inputImage->SetReleaseDataFlag( oldReleaseDataFlag );
		
			// specify the region for the current slice
			int min[TInputImage::ImageDimension];
			int max[TInputImage::ImageDimension];

			for(int j=0; j<TInputImage::ImageDimension; j++)
			{
				if (j == projectionDimension)
				{
					min[j] = i*sliceWidth;
					max[j] = (i+1)*sliceWidth;
				}
				else
				{
					min[j] = inputImage->GetLargestPossibleRegion().GetIndex(j);
					max[j] = inputImage->GetLargestPossibleRegion().GetSize(j);
				}
			}

			// extract sub region
			typedef itk::ExtractImageFilter<TInputImage, TInputImage> ExtractImageFilterType;
			typename ExtractImageFilterType::Pointer extractImageFilter = ExtractImageFilterType::New();
			typename TInputImage::RegionType largestPossibleRegion = inputImage->GetLargestPossibleRegion();
			typename TInputImage::RegionType desiredRegion;

			// set to old values if -1 is specified
			for( int j=0; j<TInputImage::ImageDimension; ++j)
			{
				max[j] = std::min<int>(largestPossibleRegion.GetSize(j), max[j]);

				// set the new region
				desiredRegion.SetIndex(j, min[j]);
				desiredRegion.SetSize(j, max[j] - min[j]);
			}

			// extract the desired region
			extractImageFilter->SetExtractionRegion( desiredRegion );
			extractImageFilter->SetNumberOfWorkUnits( maxThreads );
			extractImageFilter->SetInput( inputImage );

			// update the extraction filter
			itkTryCatch( extractImageFilter->Update(), "MaximumProjectionFilter --> Extract Image Filter." );

			// define the maximum projection filter
			typedef itk::MaximumProjectionImageFilter<TInputImage, TInputImage> MaximumProjectionFilterType;
			typename MaximumProjectionFilterType::Pointer maximumProjectionFilter = MaximumProjectionFilterType::New();
			maximumProjectionFilter->SetInput( extractImageFilter->GetOutput() );
			maximumProjectionFilter->SetNumberOfWorkUnits( maxThreads );
			maximumProjectionFilter->SetProjectionDimension(projectionDimension);
		
			// update the extraction filter
			itkTryCatch( maximumProjectionFilter->Update(), "MaximumProjectionFilter --> Perform Maximum Projection." );

			// permute the axes if required by the projection axes
			typedef typename itk::PermuteAxesImageFilter<TInputImage> PermuteAxesImageFilterType;
			typename itk::FixedArray<unsigned int, TInputImage::ImageDimension> order;

			if (projectionDimension == 0)
			{
				order[0] = 2; order[1] = 1; order[2] = 0;
			}
			else if (projectionDimension == 1)
			{
				order[0] = 0; order[1] = 2; order[2] = 1;
			}
			else
			{
				order[0] = 0; order[1] = 1; order[2] = 2;
			}

			// permute the axes
			typename PermuteAxesImageFilterType::Pointer permuteAxesFilter = PermuteAxesImageFilterType::New();
			permuteAxesFilter->SetInput(maximumProjectionFilter->GetOutput());
			permuteAxesFilter->SetReleaseDataFlag( processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0 );
			permuteAxesFilter->SetOrder(order);
			permuteAxesFilter->Update();

			// set the output
			if (i==0)
			{
				typename TInputImage::IndexType outputIndex;
				typename TInputImage::SizeType outputSize;
				typename TInputImage::RegionType outputRegion;

				for (int j=0; j<TInputImage::ImageDimension; ++j)
				{
					int sizeMultiplicator = 1;
					if (j==0)
						sizeMultiplicator = numRows;
					else if (j==1)
						sizeMultiplicator = numColumns;

					outputIndex[j] = permuteAxesFilter->GetOutput()->GetLargestPossibleRegion().GetIndex(j);
					outputSize[j] = permuteAxesFilter->GetOutput()->GetLargestPossibleRegion().GetSize(j) * sizeMultiplicator + 1;
				}

				outputSize[2] = (int)std::ceil((float)numSlices / (float)(numRows*numColumns));;
				outputIndex[2] = 0;
				outputRegion.SetIndex( outputIndex );
				outputRegion.SetSize( outputSize );

				outputImage = TInputImage::New();
				outputImage->SetRegions( outputRegion );
				outputImage->Allocate();
				outputImage->FillBuffer(0);

				Logger::GetInstance()->WriteLine( QString("- Allocated Output: [") + QString().number(outputIndex[0]) + QString(", ") +
																				 QString().number(outputIndex[1]) + QString(", ") +
																				 QString().number(outputIndex[2]) + QString(", ") +
																				 QString().number(outputSize[0]) + QString(", ") +
																				 QString().number(outputSize[1]) + QString(", ") +
																				 QString().number(outputSize[2]) + QString("]"));
			}

			typename TInputImage::IndexType outputIndex;
			typename TInputImage::SizeType outputSize;
			typename TInputImage::RegionType outputRegion;
		
			for (int j=0; j<TInputImage::ImageDimension; ++j)
			{
				if (j==2)
				{
					outputIndex[2] = std::floor((float)i / (float)(numRows*numColumns));
					outputSize[2] = 1;
				}
				else
				{
					if (j==0)
						outputIndex[j] = (i % numRows)*permuteAxesFilter->GetOutput()->GetLargestPossibleRegion().GetSize(j);
					else if (j==1)
						outputIndex[j] = ((int)std::floor((float)i / (float)(numRows)) % (numColumns))*permuteAxesFilter->GetOutput()->GetLargestPossibleRegion().GetSize(j);
				
					outputSize[j] = permuteAxesFilter->GetOutput()->GetLargestPossibleRegion().GetSize(j);
				}
			}
			outputRegion.SetIndex(outputIndex);
			outputRegion.SetSize(outputSize);

			typename itk::ImageRegionIterator<TInputImage> currentFrameIterator(permuteAxesFilter->GetOutput(), permuteAxesFilter->GetOutput()->GetLargestPossibleRegion());
			typename itk::ImageRegionIterator<TInputImage> outputIterator(outputImage, outputRegion);
			currentFrameIterator.GoToBegin();
			outputIterator.GoToBegin();

			Logger::GetInstance()->WriteLine( QString("- Current Region: [") + QString().number(outputIndex[0]) + QString(", ") +
																			  QString().number(outputIndex[1]) + QString(", ") +
																			  QString().number(outputIndex[2]) + QString(", ") +
																			  QString().number(outputSize[0]) + QString(", ") +
																			  QString().number(outputSize[1]) + QString(", ") +
																			  QString().number(outputSize[2]) + QString("]"));

			while (currentFrameIterator.IsAtEnd() == false)
			{
				outputIterator.Set( currentFrameIterator.Value() );
				++outputIterator;
				++currentFrameIterator;
			}
		}
	}
	else
	{
		// define the maximum projection filter
		typedef itk::MaximumProjectionImageFilter<TInputImage, TInputImage> MaximumProjectionFilterType;
		typename MaximumProjectionFilterType::Pointer maximumProjectionFilter = MaximumProjectionFilterType::New();
		maximumProjectionFilter->SetInput( inputImage );
		maximumProjectionFilter->SetNumberOfWorkUnits( maxThreads );
		maximumProjectionFilter->SetProjectionDimension(projectionDimension);

		// update the extraction filter
		itkTryCatch( maximumProjectionFilter->Update(), "MaximumProjectionFilter --> Perform Maximum Projection." );

		// log the status
		Logger::GetInstance()->WriteLine( QString("- Performing Maximum Projection along dimension ") + QString().number(projectionDimension) );

		// permute the axes if required by the projection axes
		typedef typename itk::PermuteAxesImageFilter<TInputImage> PermuteAxesImageFilterType;
		typename itk::FixedArray<unsigned int, TInputImage::ImageDimension> order;

		if (projectionDimension == 0)
		{
			order[0] = 2; order[1] = 1; order[2] = 0;
		}
		else if (projectionDimension == 1)
		{
			order[0] = 0; order[1] = 2; order[2] = 1;
		}
		else
		{
			order[0] = 0; order[1] = 1; order[2] = 2;
		}

		// permute the axes
		typename PermuteAxesImageFilterType::Pointer permuteAxesFilter = PermuteAxesImageFilterType::New();
		permuteAxesFilter->SetInput(maximumProjectionFilter->GetOutput());
		permuteAxesFilter->SetReleaseDataFlag( processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0 );
		permuteAxesFilter->SetOrder(order);
		itkTryCatch( permuteAxesFilter->Update(), "MaximumProjectionFilter -> Permute the axes of the output image." );

		// set the output image
		outputImage = permuteAxesFilter->GetOutput();
	}

	// set the output
	ImageWrapper *outputImageWrapper = new ImageWrapper();
	outputImageWrapper->SetRescaleFlag(false);
    outputImageWrapper->SetImage<TInputImage>( outputImage );
    mOutputImages.append( outputImageWrapper );

    // update the process widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< MaximumProjectionImageFilterWidget<Image2Float> > MaximumProjectionImageFilterWidgetImage2Float;
static ProcessObjectProxy< MaximumProjectionImageFilterWidget<Image3Float> > MaximumProjectionImageFilterWidgetImage3Float;
//static ProcessObjectProxy< MaximumProjectionImageFilterWidget<Image2UShort> > MaximumProjectionImageFilterWidgetImage2UShort;
//static ProcessObjectProxy< MaximumProjectionImageFilterWidget<Image3UShort> > MaximumProjectionImageFilterWidgetImage3UShort;


} // namespace XPIWIT
