/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifdef XPIWIT_USE_TORCH

#ifndef TORCHMODELFILTERWIDGET_H
#define TORCHMODELFILTERWIDGET_H

#include <torch/script.h>

#ifdef XPIWIT_USE_TORCH_GPU
#include <torch/cuda.h>
#endif

// namespace header
#include "../../Base/Management/ProcessObjectBase.h"
#include "../../Base/Management/ProcessObjectProxy.h"

namespace XPIWIT
{

/**
 *	@class TorchModelFilterWidget
 *	The base class for image to image filter widgets.
 */
template< class TInputImage>
class TorchModelFilterWidget : public ProcessObjectBase
{
    public:

        /**
         * The constructor.
         */
        TorchModelFilterWidget();


        /**
         * The destructor.
         */
        virtual ~TorchModelFilterWidget();


        /**
         * The update function.
         */
        void Update();


		/**
		 * Static functions used for the filter factory.
		 */
		static QString GetName() { return "TorchModel"; }
		static QString GetType() { return (typeid(float) == typeid(typename TInputImage::PixelType)) ? "float" : "ushort"; }
		static int GetDimension() { return TInputImage::ImageDimension; }
};

} // namespace XPIWIT

//#include "TorchModelFilterWidget.txx"

#endif
#endif
