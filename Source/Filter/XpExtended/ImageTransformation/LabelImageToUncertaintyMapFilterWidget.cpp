/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "LabelImageToUncertaintyMapFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"
#include "../../ITKCustom/itkLabelImageToUncertaintyMapFilter.h"


namespace XPIWIT
{

// the default constructor
template< class TInputImage >
LabelImageToUncertaintyMapFilterWidget<TInputImage>::LabelImageToUncertaintyMapFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = LabelImageToUncertaintyMapFilterWidget<TInputImage>::GetName();
	this->mDescription = "Converts the provided label image to an uncertainty map.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(2);
	this->mObjectType->AppendMetaInputType("RegionProps");
	this->mObjectType->AppendMetaInputType("FuzzySetParameters");
    this->mObjectType->SetNumberMetaOutputs(1);
	this->mObjectType->AppendMetaOutputType("RegionProps");
	
    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "MinimumIntensity", "0.05", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The minimum intensity to be produced by the uncertainty image. Used to distinguish uncertain objects from the background." );
	processObjectSettings->AddSetting( "UncertaintyCombinationFunction", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Multiplication: 0, Min: 1, Max: 2, Weighted Average: 3." );

	/*
    processObjectSettings->AddSetting( "MinimumRegionSigma", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The minimum std. dev. within a cropped image region. Ignored in the current implementation." );
    processObjectSettings->AddSetting( "Segment3D", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Used to perform the segmentation either directly in 3D or to merge 2D segmentation results instead." );
    processObjectSettings->AddSetting( "LabelOutput", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If true, the filter directly produces a labeled output image with a unique id for each detected blob." );
    processObjectSettings->AddSetting( "RandomLabels", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If checked, random labels are used. Note that random labels might not be unique." );
    processObjectSettings->AddSetting( "WriteRegionProps", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If checked, the region props of extracted blobs are exported to a cvs file." );
    processObjectSettings->AddSetting( "MinimumWeightedGradientNormalDotProduct", "0.6", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Used to threshold the weighted dot product image." );
    processObjectSettings->AddSetting( "WeightingKernelSizeMultiplicator", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Used to scale the plateau region of the weighting kernel. If set to 1 the seed radius is used for the plateau radius." );
    processObjectSettings->AddSetting( "WeightingKernelStdDev", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Multiplier for the weighting kernel standard deviation." );
    processObjectSettings->AddSetting( "GradientImageStdDev", "1.5", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The standard deviation of the Gaussian smoothing for smoother gradient directions." );
	*/

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
LabelImageToUncertaintyMapFilterWidget<TInputImage>::~LabelImageToUncertaintyMapFilterWidget()
{
}


// the update function
template< class TInputImage >
void LabelImageToUncertaintyMapFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	this->mMetaOutputs.at(0)->mIsMultiDimensional = true;

	MetaDataFilter* regionProps = this->mMetaInputs.at(0);
	MetaDataFilter* fuzzySetParameters = this->mMetaInputs.at(1);
	MetaDataFilter* outputFilter = this->mMetaOutputs.at(0);
	outputFilter->mData = regionProps->mData;
	outputFilter->mType = regionProps->mType;
	outputFilter->mTitle = regionProps->mTitle;

    // get filter parameters
    bool writeRegionProps = processObjectSettings->GetSettingValue( "WriteRegionProps" ).toInt() > 0;
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const float minimumIntensity = processObjectSettings->GetSettingValue( "MinimumIntensity" ).toFloat();
	const int uncertaintyCombinationFunction = processObjectSettings->GetSettingValue( "UncertaintyCombinationFunction" ).toInt();

	// calculate the fuzzy set membership values for each of the objects
	const unsigned int numFuzzySets = fuzzySetParameters->mData.length();
	const unsigned int numObjects = outputFilter->mData.length();
	outputFilter->mTitle << "uncertainty";
	outputFilter->mType << "float";
	float a,b,c,d, featureValue;
	unsigned int featureIndex;

	for (int i=0; i<numObjects; ++i)
	{
		float uncertainty = 1.0;
		float currentUncertainty = 1.0;
		for (int j=0; j<numFuzzySets; ++j)
		{
			a = fuzzySetParameters->mData[j][1];
			b = fuzzySetParameters->mData[j][2];
			c = fuzzySetParameters->mData[j][3];
			d = fuzzySetParameters->mData[j][4];
			featureIndex = (unsigned int)fuzzySetParameters->mData[j][0];
			featureValue = regionProps->mData[i][featureIndex];
			currentUncertainty = std::max<float>(std::min<float>(std::min<float>(((featureValue-a)/(b-a)), ((d-featureValue)/(d-c))), 1), 0);

			if (uncertaintyCombinationFunction == 0)
				uncertainty *= currentUncertainty;
			if (uncertaintyCombinationFunction == 1)
				uncertainty = std::min<float>(currentUncertainty, uncertainty);
			if (uncertaintyCombinationFunction == 2)
				uncertainty = std::max<float>(currentUncertainty, uncertainty);
			if (uncertaintyCombinationFunction == 3)
				uncertainty += currentUncertainty;
		}

		if (uncertaintyCombinationFunction == 3 && numFuzzySets > 0)
			uncertainty /= numFuzzySets;

		uncertainty = std::max<float>(uncertainty, minimumIntensity);
		outputFilter->mData[i].append(uncertainty);
	}

    // setup the filters
    typedef itk::LabelImageToUncertaintyMapFilter<TInputImage> LabelImageToUncertaintyMapFilterType;
    typename LabelImageToUncertaintyMapFilterType::Pointer labelToUncertaintyMapFilter = LabelImageToUncertaintyMapFilterType::New();
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
    // initialize level sets segmentation
    labelToUncertaintyMapFilter->SetInput( inputImage );
    labelToUncertaintyMapFilter->SetNumberOfWorkUnits( maxThreads );
    labelToUncertaintyMapFilter->SetInputMetaFilter( outputFilter );
	labelToUncertaintyMapFilter->SetUncertaintyFeature( outputFilter->mTitle.length()-1 );

    // perform and set the output
    itkTryCatch( labelToUncertaintyMapFilter->Update(), "Twang Segmentation Called in LabelImageToUncertaintyMapFilterWidget." );
    
	ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( labelToUncertaintyMapFilter->GetOutput() );
    mOutputImages.append( outputImage );

    // update the base class
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< LabelImageToUncertaintyMapFilterWidget<Image2Float> > LabelImageToUncertaintyMapFilterWidgetImage2Float;
static ProcessObjectProxy< LabelImageToUncertaintyMapFilterWidget<Image3Float> > LabelImageToUncertaintyMapFilterWidgetImage3Float;
static ProcessObjectProxy< LabelImageToUncertaintyMapFilterWidget<Image2UShort> > LabelImageToUncertaintyMapFilterWidgetImage2UShort;
static ProcessObjectProxy< LabelImageToUncertaintyMapFilterWidget<Image3UShort> > LabelImageToUncertaintyMapFilterWidgetImage3UShort;

} // namespace XPIWIT
