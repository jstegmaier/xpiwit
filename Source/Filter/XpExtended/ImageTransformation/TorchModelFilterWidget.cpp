/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifdef XPIWIT_USE_TORCH

// project header
#include "TorchModelFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkMinimumMaximumImageCalculator.h"
#include "ATen/Parallel.h"

namespace XPIWIT
{

// the default constructor
template< class TInputImage >
TorchModelFilterWidget<TInputImage>::TorchModelFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = TorchModelFilterWidget<TInputImage>::GetName();
	this->mDescription = "Apply specified Torch pipeline from a serialized model (*.pt).";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(4);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(4);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "NumInputChannels", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The number of input channels." );
    processObjectSettings->AddSetting( "UseGPU", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Uses the GPU if possible. Defaults to CPU.");
    processObjectSettings->AddSetting( "PatchWidth", "128", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Patch width used for processing. Set to -1 for complete image at once." );
    processObjectSettings->AddSetting( "PatchHeight", "128", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Patch height used for processing. Set to -1 for complete image at once." );
    processObjectSettings->AddSetting( "PatchDepth", "64", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Patch depth used for processing. Set to -1 for complete image at once." );
    processObjectSettings->AddSetting( "PatchStride", "0.5", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Absolute (values > 1) or patch-relative stride (values < 1)." );
	processObjectSettings->AddSetting( "ModelPath", "/Users/jstegmaier/Programming/TorchC++/Models/3ClassUNet.pt", ProcessObjectSetting::SETTINGVALUETYPE_STRING, "Absolute file path to the model file in *.pt format." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage>
TorchModelFilterWidget<TInputImage>::~TorchModelFilterWidget()
{
}


// the update function
template< class TInputImage>
void TorchModelFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get the parameter values
    const int patchWidth = processObjectSettings->GetSettingValue( "PatchWidth" ).toInt();
    const int patchHeight = processObjectSettings->GetSettingValue( "PatchHeight" ).toInt();
    const int patchDepth = processObjectSettings->GetSettingValue( "PatchDepth" ).toInt();
    const int numInputChannels = processObjectSettings->GetSettingValue( "NumInputChannels" ).toInt();
    const float patchStrideParameter = processObjectSettings->GetSettingValue( "PatchStride" ).toFloat();
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    QString modelPath = processObjectSettings->GetSettingValue( "ModelPath" );

    bool useGPU = processObjectSettings->GetSettingValue("UseGPU") > 0;

    #ifndef XPIWIT_USE_TORCH_GPU
        useGPU = false;
    #else
        std::cout << "- CUDA DEVICE COUNT: " << torch::cuda::device_count() << std::endl;
        if (torch::cuda::is_available() && useGPU == true) 
            std::cout << "- CUDA is available! Performing forward pass on GPU." << std::endl;
        else if (torch::cuda::is_available() && useGPU == false)
            std::cout << "- CUDA is available but not enabled for this filter. Performing forward pass on CPU." << std::endl;
        else
        {
            std::cout << "- CUDA is not available. Performing forward pass on CPU." << std::endl;
            useGPU = false;
        }
    #endif
    
    // load the torch model
    torch::jit::script::Module torchModel;
    try
    {
        // deserialize the ScriptModule from the provided model file
        if (useGPU == false)
        {
            torchModel = torch::jit::load(modelPath.toStdString().c_str());

            // set the number of threads to use for multi-core processing
            at::init_num_threads();
            at::set_num_threads(maxThreads);
            std::cout << "- Processing image with " << at::get_num_threads() << " threads." << std::endl;
        }
        else
        {
            torchModel = torch::jit::load(modelPath.toStdString().c_str(), torch::kCUDA);
            std::cout << "- Successfully loaded model in GPU memory." << std::endl;
        }
    }
    catch (const c10::Error& e)
    {
        std::cerr << "- Error loading the model!\n";
        return;
    }

    // get the input images
	typename TInputImage::Pointer inputImage1 = mInputImages.at(0)->template GetImage<TInputImage>();
    typename TInputImage::Pointer inputImage2 = mInputImages.at(1)->template GetImage<TInputImage>();
    typename TInputImage::Pointer inputImage3 = mInputImages.at(2)->template GetImage<TInputImage>();
    typename TInputImage::Pointer inputImage4 = mInputImages.at(3)->template GetImage<TInputImage>();
    
    // perform tile-based processing
    int imageDimension = TInputImage::ImageDimension;
    typename TInputImage::RegionType inputRegion = inputImage1->GetLargestPossibleRegion();
    typename TInputImage::SpacingType inputSpacing = inputImage1->GetSpacing();
    typename TInputImage::SizeType imageSize = inputRegion.GetSize();
    typename TInputImage::SizeType patchSize;
    typename TInputImage::SizeType patchStride;
    typename TInputImage::SizeType numSteps;

    // check if 2D or 3D was properly selected
    if (imageDimension > 2 && imageSize[2] == 1)
        std::cout << "- You're trying to process a 2D image in 3D mode. Switch to 2D processing for correct behavior (checkbox above the parameter dialog)." << std::endl;
    
    // set the patch size used for processing
    patchSize[0] = patchWidth;
    patchSize[1] = patchHeight;
    if (imageDimension > 2)
        patchSize[2] = patchDepth;
        
    // compute the strides and number of steps used for processing
    int numPatchVoxels = 1;
    for (int i=0; i<imageDimension; ++i)
    {
        numPatchVoxels *= imageSize[i];
        patchStride[i] = (patchStrideParameter >= 1) ? patchStrideParameter : int(patchStrideParameter * (float)patchSize[i]);
        numSteps[i] = floor((float)imageSize[i] / (float)patchStride[i]);
        
        if (patchSize[i] == imageSize[i])
            numSteps[i] = 1;
    }
    
    // print processing information for debugging
    std::cout << "- Image Dimension: " << imageDimension << std::endl;
    if (imageDimension > 2)
    {
        std::cout << "- Image Size: [" << imageSize[0] << ", " << imageSize[1] << ", " << imageSize[2] << "]" << std::endl;
        std::cout << "- Patch Size: [" << patchSize[0] << ", " << patchSize[1] << ", " << patchSize[2] << "]" << std::endl;
        std::cout << "- Patch Stride: [" << patchStride[0] << ", " << patchStride[1] << ", " << patchStride[2] << "]" << std::endl;
        std::cout << "- Num Steps: [" << numSteps[0] << ", " << numSteps[1] << ", " << numSteps[2] << "]" << std::endl;
    }
    else
    {
        std::cout << "- Image Size: [" << imageSize[0] << ", " << imageSize[1] << "]" << std::endl;
        std::cout << "- Patch Size: [" << patchSize[0] << ", " << patchSize[1] << "]" << std::endl;
        std::cout << "- Patch Stride: [" << patchStride[0] << ", " << patchStride[1] << "]" << std::endl;
        std::cout << "- Num Steps: [" << numSteps[0] << ", " << numSteps[1] << "]" << std::endl;
    }
   
    // initialize the patch region as well as the patch center for distance computations
    typename TInputImage::RegionType patchRegion;
    vnl_vector<float> patchCenter(imageDimension);
    for (int i=0; i<imageDimension; ++i)
    {
        patchRegion.SetSize(i, patchSize[i]);
        patchRegion.SetIndex(i, 0);
        patchCenter[i] = (float)(patchSize[i]/2.0f);
    }
    float patchCenterDistance = patchCenter.two_norm();
            
    // create output and weight images
    typename TInputImage::Pointer patchWeightMap = TInputImage::New();
    typename TInputImage::Pointer outputImage1 = TInputImage::New();
    typename TInputImage::Pointer outputImage2 = TInputImage::New();
    typename TInputImage::Pointer outputImage3 = TInputImage::New();
    typename TInputImage::Pointer outputImage4 = TInputImage::New();
    typename TInputImage::Pointer weightImage = TInputImage::New();
    outputImage1->SetRegions( inputRegion );
    outputImage1->SetSpacing( inputSpacing );
    outputImage1->Allocate();
    outputImage1->FillBuffer( 0 );
    outputImage2->SetRegions( inputRegion );
    outputImage2->SetSpacing( inputSpacing );
    outputImage2->Allocate();
    outputImage2->FillBuffer( 0 );
    outputImage3->SetRegions( inputRegion );
    outputImage3->SetSpacing( inputSpacing );
    outputImage3->Allocate();
    outputImage3->FillBuffer( 0 );
    outputImage4->SetRegions( inputRegion );
    outputImage4->SetSpacing( inputSpacing );
    outputImage4->Allocate();
    outputImage4->FillBuffer( 0 );
    weightImage->SetRegions( inputRegion );
    weightImage->SetSpacing( inputSpacing );
    weightImage->Allocate();
    weightImage->FillBuffer( 0 );
    patchWeightMap->SetRegions( patchRegion );
    patchWeightMap->SetSpacing( inputSpacing );
    patchWeightMap->Allocate();
    patchWeightMap->FillBuffer( 0 );

    // initialize the temporary patch
    typename TInputImage::IndexType currentIndex;
    typename TInputImage::SizeType currentSize;
    typename TInputImage::RegionType currentRegion;
    for (int i=0; i<imageDimension; ++i)
    {
        currentRegion.SetSize(i, patchSize[i]);
        currentRegion.SetIndex(i, 0);
    }
    
    // generate the weight map
    itk::ImageRegionIterator<TInputImage> patchWeightMapIterator( patchWeightMap, patchRegion );
    patchWeightMapIterator.GoToBegin();
    vnl_vector<float> currentPosition(imageDimension);
    while (patchWeightMapIterator.IsAtEnd() == false)
    {
        vnl_vector<float> currentPosition(imageDimension);
        for (int i=0; i<imageDimension; ++i)
            currentPosition[i] = (float)patchWeightMapIterator.GetIndex()[i];
         
        patchWeightMapIterator.Set((patchCenterDistance - (currentPosition - patchCenter).two_norm()) / patchCenterDistance);
        ++patchWeightMapIterator;
    }
    
    // initialize counters and temporary patch
    int counter = 0;
    int numStepsZ = (imageDimension > 2) ? std::max<int>(1, numSteps[2]) : 1;
    float* currentPatch = new float[numInputChannels * numPatchVoxels];
    
    // process all patches separately
    for (int i=0; i<numSteps[0]; ++i)
    {
        currentRegion.SetIndex(0, std::min<int>(imageSize[0]-patchSize[0], i*patchStride[0]));
        
        for (int j=0; j<numSteps[1]; ++j)
        {
            currentRegion.SetIndex(1, std::min<int>(imageSize[1]-patchSize[1], j*patchStride[1]));
            
            for (int k=0; k<numStepsZ; ++k)
            {
                if (imageDimension > 2)
                    currentRegion.SetIndex(2, std::min<int>(imageSize[2]-patchSize[2], k*patchStride[2]));
                
                // print information about the current patch to be processed
                if (imageDimension > 2)
                {
                    std::cout << "- Processing region with index [" << currentRegion.GetIndex(0) << ", "  << currentRegion.GetIndex(1) << ", "  << currentRegion.GetIndex(2) << "]";
                    std::cout << " and size [" << currentRegion.GetSize(0) << ", "  << currentRegion.GetSize(1) << ", "  << currentRegion.GetSize(2) << "]" << std::endl;
                }
                else
                {
                    std::cout << "- Processing region with index [" << currentRegion.GetIndex(0) << ", "  << currentRegion.GetIndex(1) << "]";
                    std::cout << " and size [" << currentRegion.GetSize(0) << ", "  << currentRegion.GetSize(1) << "]" << std::endl;
                }
                                
                // extract information from the inputs at the current patch location
                itk::ImageRegionIterator<TInputImage> inputIterator1( inputImage1, currentRegion );
                itk::ImageRegionIterator<TInputImage> inputIterator2( inputImage2, currentRegion );
                itk::ImageRegionIterator<TInputImage> inputIterator3( inputImage3, currentRegion );
                itk::ImageRegionIterator<TInputImage> inputIterator4( inputImage4, currentRegion );
                inputIterator1.GoToBegin();
                inputIterator2.GoToBegin();
                inputIterator3.GoToBegin();
                inputIterator4.GoToBegin();

                // initialize temporary input tensor
                torch::Tensor inputTensor;
                if (imageDimension > 2)
                    inputTensor = torch::zeros({1, numInputChannels, patchDepth, patchHeight, patchWidth});
                else
                    inputTensor = torch::zeros({1, numInputChannels, patchHeight, patchWidth});
                
                // fill the current patch input
                counter = 0;
                while (inputIterator1.IsAtEnd() == false)
                {
                    currentPatch[counter] = inputIterator1.Value();
                    
                    if (numInputChannels > 1)
                        currentPatch[counter+(numPatchVoxels)] = inputIterator2.Value();
                    
                    if (numInputChannels > 2)
                        currentPatch[counter+(2*numPatchVoxels)] = inputIterator3.Value();
                    
                    if (numInputChannels > 3)
                        currentPatch[counter+(3*numPatchVoxels)] = inputIterator4.Value();
                    
                    // increment iterators
                    ++counter;
                    ++inputIterator1;
                    ++inputIterator2;
                    ++inputIterator3;
                    ++inputIterator4;
                }

                // convert the float array to a torch tensor
                torch::Tensor tensorImage;
                if (imageDimension > 2)
                    tensorImage = torch::from_blob(currentPatch, {1, numInputChannels, patchDepth, patchHeight, patchWidth}, torch::kFloat).clone();
                else
                    tensorImage = torch::from_blob(currentPatch, {1, numInputChannels, patchHeight, patchWidth}, torch::kFloat).clone();
                
                tensorImage = tensorImage.toType(torch::kFloat);
                tensorImage.set_requires_grad(0);
                if (useGPU == true)
                {
                    tensorImage = tensorImage.to(at::kCUDA);
                    std::cout << "Transfering image to GPU memory was successful." << std::endl;
                }

                // Create a vector of inputs.
                std::vector<torch::jit::IValue> inputs;
                inputs.push_back(tensorImage);

                
                // Execute the model and turn its output into a tensor.
                torch::jit::IValue networkPrediction;
                torch::Tensor outputTensor;
                std::vector<torch::jit::IValue> outputTuple;
                                                
                int numOutputChannels = 0;
                try
                {
                    networkPrediction = torchModel.forward(inputs);
                    
                    if (networkPrediction.isTuple() == false)
                    {
                        outputTensor = networkPrediction.toTensor();
                        if (useGPU == true)
                            outputTensor = outputTensor.to(at::kCPU);
                        
                        numOutputChannels = outputTensor.size(1);
                        
                        // print result tensor size
                        std::cout << "Result tensor dtype: " << outputTensor.dtype() << ", size: " << outputTensor.sizes() << std::endl;
                    }
                    else
                    {
                        outputTuple = networkPrediction.toTuple()->elements();
                        numOutputChannels = outputTuple.size();
                        outputTensor = outputTuple[0].toTensor();
                        
                        std::cout << "Processing tuple output with " << numOutputChannels << " tuple elements." << outputTensor.dtype() << ", size: " << outputTensor.sizes() << std::endl;
                    }
                }
                catch (const c10::Error& e)
                {
                    std::cout << e.what() << std::endl;
                }
                

                // convert tensor output back to a float array
                outputTensor = outputTensor.toType(torch::kFloat);
                float* resultArray = outputTensor.data_ptr<float>();
                float currentValue = 0.0;
                
                // create output iterators
                itk::ImageRegionIterator<TInputImage> outputIterator1( outputImage1, currentRegion );
                itk::ImageRegionIterator<TInputImage> outputIterator2( outputImage2, currentRegion );
                itk::ImageRegionIterator<TInputImage> outputIterator3( outputImage3, currentRegion );
                itk::ImageRegionIterator<TInputImage> outputIterator4( outputImage4, currentRegion );
                itk::ImageRegionIterator<TInputImage> weightIterator( weightImage, currentRegion );

                // reset output iterators to beginning
                outputIterator1.GoToBegin();
                weightIterator.GoToBegin();
                patchWeightMapIterator.GoToBegin();
                
                // fill the output region of the current patch for the first channel
                while (outputIterator1.IsAtEnd() == false)
                {
                    // get the current value and set it to the respective output position
                    currentValue = *resultArray++;
                    outputIterator1.Set(outputIterator1.Value() + patchWeightMapIterator.Value() * currentValue);
                   
                    // increment the weight iterator. As weights for all channels are identical, this only has to be done once.
                    weightIterator.Set(weightIterator.Value() + patchWeightMapIterator.Value());
                    
                    // increment the iterators
                    ++outputIterator1;
                    ++weightIterator;
                    ++patchWeightMapIterator;
                }
                
                // fill the output region of the current patch for the second channel
                if (numOutputChannels > 1)
                {
                    if (networkPrediction.isTuple())
                    {
                        outputTensor = outputTuple[1].toTensor();
                        outputTensor = outputTensor.toType(torch::kFloat);
                        resultArray = outputTensor.data_ptr<float>();
                    }
                    
                    // reset iterators to beginning
                    outputIterator2.GoToBegin();
                    patchWeightMapIterator.GoToBegin();
                    
                    while (outputIterator2.IsAtEnd() == false)
                    {
                        // get the current value and set it to the respective output position
                        currentValue = *resultArray++;
                        outputIterator2.Set(outputIterator2.Value() + patchWeightMapIterator.Value() * currentValue);

                        // increment the iterators
                        ++outputIterator2;
                        ++patchWeightMapIterator;
                    }
                }
                
                // fill the output region of the current patch for the third channel
                if (numOutputChannels > 2)
                {
                    if (networkPrediction.isTuple())
                    {
                        outputTensor = outputTuple[2].toTensor();
                        outputTensor = outputTensor.toType(torch::kFloat);
                        resultArray = outputTensor.data_ptr<float>();
                    }
                    
                    // reset iterators to beginning
                    outputIterator3.GoToBegin();
                    patchWeightMapIterator.GoToBegin();
                    
                    while (outputIterator3.IsAtEnd() == false)
                    {
                        // get the current value and set it to the respective output position
                        currentValue = *resultArray++;
                        outputIterator3.Set(outputIterator3.Value() + patchWeightMapIterator.Value() * currentValue);
                       
                        // increment the iterators
                        ++outputIterator3;
                        ++patchWeightMapIterator;
                    }
                }
                
                // fill the output region of the current patch for the third channel
                if (numOutputChannels > 3)
                {
                    if (networkPrediction.isTuple())
                    {
                        outputTuple = networkPrediction.toTuple()->elements();
                        numOutputChannels = outputTuple.size();
                        outputTensor = outputTuple[3].toTensor();
                        outputTensor = outputTensor.toType(torch::kFloat);
                        resultArray = outputTensor.data_ptr<float>();
                    }
                    
                    // reset iterators to beginning
                    outputIterator4.GoToBegin();
                    patchWeightMapIterator.GoToBegin();
                    
                    while (outputIterator4.IsAtEnd() == false)
                    {
                        // get the current value and set it to the respective output position
                        currentValue = *resultArray++;
                        outputIterator4.Set(outputIterator4.Value() + patchWeightMapIterator.Value() * currentValue);
                       
                        // increment the iterators
                        ++outputIterator4;
                        ++patchWeightMapIterator;
                    }
                }
            }
        }
    }
    
    // remove temporary input image
    delete[] currentPatch;
    
    // perform weight normalization for correct outputs
    itk::ImageRegionIterator<TInputImage> outputIterator1( outputImage1, inputRegion );
    itk::ImageRegionIterator<TInputImage> outputIterator2( outputImage2, inputRegion );
    itk::ImageRegionIterator<TInputImage> outputIterator3( outputImage3, inputRegion );
    itk::ImageRegionIterator<TInputImage> outputIterator4( outputImage4, inputRegion );
    itk::ImageRegionIterator<TInputImage> weightIterator( weightImage, inputRegion );
    outputIterator1.GoToBegin();
    outputIterator2.GoToBegin();
    outputIterator3.GoToBegin();
    outputIterator4.GoToBegin();
    weightIterator.GoToBegin();

    while (outputIterator1.IsAtEnd() == false)
    {
        // normalize pixels with the associated weights
        outputIterator1.Set(outputIterator1.Value() / weightIterator.Value());
        outputIterator2.Set(outputIterator2.Value() / weightIterator.Value());
        outputIterator3.Set(outputIterator3.Value() / weightIterator.Value());
        outputIterator4.Set(outputIterator4.Value() / weightIterator.Value());
     
        // increment iterators
        ++outputIterator1;
        ++outputIterator2;
        ++outputIterator3;
        ++outputIterator4;
        ++weightIterator;
    }
    
    // set the outputs
	ImageWrapper* outputImageWrapper1 = new ImageWrapper();
    ImageWrapper* outputImageWrapper2 = new ImageWrapper();
    ImageWrapper* outputImageWrapper3 = new ImageWrapper();
    ImageWrapper* outputImageWrapper4 = new ImageWrapper();
    
    /*
    outputImageWrapper1->SetRescaleFlag(true); // TODO: make sure the output is scaled properly!
    outputImageWrapper2->SetRescaleFlag(true); // TODO: make sure the output is scaled properly!
    outputImageWrapper3->SetRescaleFlag(true); // TODO: make sure the output is scaled properly!
    outputImageWrapper4->SetRescaleFlag(true); // TODO: make sure the output is scaled properly!
    */
    
    outputImageWrapper1->SetImage<TInputImage>( outputImage1 );
    outputImageWrapper2->SetImage<TInputImage>( outputImage2 );
    outputImageWrapper3->SetImage<TInputImage>( outputImage3 );
    outputImageWrapper4->SetImage<TInputImage>( outputImage4 );
    
    mOutputImages.append( outputImageWrapper1 );
    mOutputImages.append( outputImageWrapper2 );
    mOutputImages.append( outputImageWrapper3 );
    mOutputImages.append( outputImageWrapper4 );

    // update the process widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< TorchModelFilterWidget<Image2Float> > TorchModelFilterWidgetImage2Float;
static ProcessObjectProxy< TorchModelFilterWidget<Image3Float> > TorchModelFilterWidgetImage3Float;
static ProcessObjectProxy< TorchModelFilterWidget<Image2UShort> > TorchModelFilterWidgetImage2UShort;
static ProcessObjectProxy< TorchModelFilterWidget<Image3UShort> > TorchModelFilterWidgetImage3UShort;

} // namespace XPIWIT

#endif
