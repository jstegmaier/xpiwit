/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "UncertaintyBasedMultiplicationFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"
#include "../../ITKCustom/itkLabelImageToUncertaintyMapFilter.h"
#include "itkImageRegionIterator.h"

namespace XPIWIT
{

// the default constructor
template< class TInputImage >
UncertaintyBasedMultiplicationFilterWidget<TInputImage>::UncertaintyBasedMultiplicationFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = UncertaintyBasedMultiplicationFilterWidget<TInputImage>::GetName();
	this->mDescription = "Multiplies the two provided images using a third image as an uncertainty map.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(3);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);
	
    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "UncertaintyThreshold", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Multiplies only voxels with a lower uncertainty than this threshold." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
UncertaintyBasedMultiplicationFilterWidget<TInputImage>::~UncertaintyBasedMultiplicationFilterWidget()
{
}


// the update function
template< class TInputImage >
void UncertaintyBasedMultiplicationFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	const float uncertaintyThreshold = processObjectSettings->GetSettingValue( "UncertaintyThreshold" ).toFloat();

	// initialize the output
	typename TInputImage::Pointer inputImage1 = mInputImages.at(0)->template GetImage<TInputImage>();
	typename TInputImage::Pointer inputImage2 = mInputImages.at(1)->template GetImage<TInputImage>();
	typename TInputImage::Pointer uncertaintyMap = mInputImages.at(2)->template GetImage<TInputImage>();
	typename TInputImage::Pointer outputImage = TInputImage::New();
	outputImage->SetRegions( inputImage1->GetLargestPossibleRegion() );
	outputImage->SetSpacing( inputImage1->GetSpacing() );
	outputImage->SetOrigin( inputImage1->GetOrigin() );
	outputImage->Allocate();
	outputImage->FillBuffer(0);

	itk::ImageRegionIterator<TInputImage> input1Iterator(inputImage1, inputImage1->GetLargestPossibleRegion() );
	itk::ImageRegionIterator<TInputImage> input2Iterator(inputImage2, inputImage1->GetLargestPossibleRegion() );
	itk::ImageRegionIterator<TInputImage> uncertaintyMapIterator(uncertaintyMap, inputImage1->GetLargestPossibleRegion() );
	itk::ImageRegionIterator<TInputImage> outputIterator(outputImage, inputImage1->GetLargestPossibleRegion() );
	input1Iterator.GoToBegin();
	input2Iterator.GoToBegin();
	uncertaintyMapIterator.GoToBegin();
	outputIterator.GoToBegin();

	while(input1Iterator.IsAtEnd() == false)
	{
		if (uncertaintyMapIterator.Value() <= uncertaintyThreshold)
			outputIterator.Set(input1Iterator.Value() * input2Iterator.Value());
		else
			outputIterator.Set(input1Iterator.Value());

		++input1Iterator;
		++input2Iterator;
		++uncertaintyMapIterator;
		++outputIterator;
	}

	// set the output image   
	ImageWrapper *outputImageWrapper = new ImageWrapper();
    outputImageWrapper->SetImage<TInputImage>( outputImage );
    mOutputImages.append( outputImageWrapper );

    // update the base class
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< UncertaintyBasedMultiplicationFilterWidget<Image2Float> > UncertaintyBasedMultiplicationFilterWidgetImage2Float;
static ProcessObjectProxy< UncertaintyBasedMultiplicationFilterWidget<Image3Float> > UncertaintyBasedMultiplicationFilterWidgetImage3Float;
static ProcessObjectProxy< UncertaintyBasedMultiplicationFilterWidget<Image2UShort> > UncertaintyBasedMultiplicationFilterWidgetImage2UShort;
static ProcessObjectProxy< UncertaintyBasedMultiplicationFilterWidget<Image3UShort> > UncertaintyBasedMultiplicationFilterWidgetImage3UShort;

} // namespace XPIWIT
