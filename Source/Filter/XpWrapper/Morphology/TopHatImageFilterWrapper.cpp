/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// namespace header
#include "TopHatImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkWhiteTopHatImageFilter.h"
#include "itkBlackTopHatImageFilter.h"
#include "itkBinaryBallStructuringElement.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
TopHatImageFilterWrapper< TInputImage >::TopHatImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = TopHatImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Morphological top hat filter to extract local extrema from an image.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "KernelRadius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Radius of the kernel." );
	processObjectSettings->AddSetting( "UseBlackTopHat", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Default is a white top hat which extracts local maxima. If this flag is enabled, local minima are extracted." );
	processObjectSettings->AddSetting( "SafeBorder", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Flag to enable/disable the safe border handling." );

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
TopHatImageFilterWrapper< TInputImage >::~TopHatImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void TopHatImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const int radius = processObjectSettings->GetSettingValue( "KernelRadius" ).toInt();
	const bool useBlackTopHat = processObjectSettings->GetSettingValue( "UseBlackTopHat" ).toInt() > 0;
	const bool safeBorder = processObjectSettings->GetSettingValue( "SafeBorder" ).toInt() > 0;
	
	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();
	
	// create kernel
	typedef itk::BinaryBallStructuringElement< typename TInputImage::PixelType, TInputImage::ImageDimension> StructuringElementType;
	StructuringElementType structuringElement;
	structuringElement.SetRadius( radius );
	structuringElement.CreateStructuringElement();

	// setup the filter
	typedef itk::WhiteTopHatImageFilter<TInputImage, TInputImage, StructuringElementType> FilterType;
	typename FilterType::Pointer filter = FilterType::New();
	filter->SetInput( inputImage );
	filter->SetKernel( structuringElement );
	filter->SetAlgorithm( itk::MathematicalMorphologyEnums::Algorithm::BASIC);
	filter->SetSafeBorder( safeBorder );
	filter->SetReleaseDataFlag( true );
	itkTryCatch(filter->Update(), "Error: TopHatImageFilterWrapper Update Function.");
	
	ImageWrapper *outputImage = new ImageWrapper();
	outputImage->SetImage<TInputImage>( filter->GetOutput() );
	outputImage->SetRescaleFlag( false );
	mOutputImages.append( outputImage );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< TopHatImageFilterWrapper<Image2Float> > TopHatImageFilterWrapperImage2Float;
static ProcessObjectProxy< TopHatImageFilterWrapper<Image3Float> > TopHatImageFilterWrapperImage3Float;
static ProcessObjectProxy< TopHatImageFilterWrapper<Image2UShort> > TopHatImageFilterWrapperImage2UShort;
static ProcessObjectProxy< TopHatImageFilterWrapper<Image3UShort> > TopHatImageFilterWrapperImage3UShort;

} // namespace XPIWIT

