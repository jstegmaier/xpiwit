/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "GrayscaleMorphologicalClosingImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkFlatStructuringElement.h"
#include "itkGrayscaleMorphologicalClosingImageFilter.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
GrayscaleMorphologicalClosingImageFilterWrapper< TInputImage >::GrayscaleMorphologicalClosingImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = GrayscaleMorphologicalClosingImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Dilation and erosion of a grayscale image";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "Type", "Ball", ProcessObjectSetting::SETTINGVALUETYPE_STRING, "Kernel type. (Annulus, Ball, Box, Cross)" );
	processObjectSettings->AddSetting( "Radius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Radius of the kernel." );
	processObjectSettings->AddSetting( "Thickness", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Thickness of annulus." );
	processObjectSettings->AddSetting( "Parametric", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Use parametric mode." );
	processObjectSettings->AddSetting( "SafeBorder", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Use safe border mode." );

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
GrayscaleMorphologicalClosingImageFilterWrapper< TInputImage >::~GrayscaleMorphologicalClosingImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void GrayscaleMorphologicalClosingImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const QString type = processObjectSettings->GetSettingValue( "Type" );
	const int radius = processObjectSettings->GetSettingValue( "Radius" ).toInt();
	const int thickness = processObjectSettings->GetSettingValue( "Thickness" ).toInt();
	const bool isParametric = processObjectSettings->GetSettingValue( "Parametric" ).toInt() > 0;
	const bool safeBorder = processObjectSettings->GetSettingValue( "SafeBorder" ).toInt() > 0;
	
	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();

	// structuring element
	typedef itk::FlatStructuringElement< TInputImage::ImageDimension > StructerElementType;
	typename StructerElementType::RadiusType structureElementRadius;
	structureElementRadius.Fill( radius );
	StructerElementType structureElement;

	// default = ball
	structureElement = StructerElementType::Ball( structureElementRadius );
	
	if( type.toLower().compare("annulus ") == 0 )
	{
		structureElement = StructerElementType::Annulus( structureElementRadius, thickness, false );
	}
	if( type.toLower().compare("box ") == 0 )
	{
		structureElement = StructerElementType::Box( structureElementRadius );
	}
	if( type.toLower().compare("cross ") == 0 )
	{
		structureElement = StructerElementType::Cross( structureElementRadius );
	}
	// polygon
	
	// setup the filter
	typedef itk::GrayscaleMorphologicalClosingImageFilter<TInputImage, TInputImage, StructerElementType> FilterType;
	typename FilterType::Pointer filter = FilterType::New();
	filter->SetInput( inputImage );
	filter->SetKernel( structureElement );
	//filter->SetAlgorithm();
	filter->SetSafeBorder( safeBorder );
	filter->SetReleaseDataFlag( true );
	
	itkTryCatch(filter->Update(), "Error: GrayscaleMorphologicalClosingImageFilterWrapper Update Function.");
	
	ImageWrapper *outputWrapper = new ImageWrapper();
	outputWrapper->SetImage<TInputImage>( filter->GetOutput() );
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< GrayscaleMorphologicalClosingImageFilterWrapper<Image2Float> > GrayscaleMorphologicalClosingImageFilterWrapperImage2Float;
static ProcessObjectProxy< GrayscaleMorphologicalClosingImageFilterWrapper<Image3Float> > GrayscaleMorphologicalClosingImageFilterWrapperImage3Float;
static ProcessObjectProxy< GrayscaleMorphologicalClosingImageFilterWrapper<Image2UShort> > GrayscaleMorphologicalClosingImageFilterWrapperImage2UShort;
static ProcessObjectProxy< GrayscaleMorphologicalClosingImageFilterWrapper<Image3UShort> > GrayscaleMorphologicalClosingImageFilterWrapperImage3UShort;

} // namespace XPIWIT

