/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "IntensityWindowingImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkIntensityWindowingImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkImageToHistogramFilter.h"
#include "itkNumericTraits.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
IntensityWindowingImageFilterWrapper< TInputImage >::IntensityWindowingImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = IntensityWindowingImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Rescales the image min to max to the specified range";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings to the filter
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "UseQuantiles", "-1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "If > 0, the value serves as quantile threshold.");
	processObjectSettings->AddSetting( "InputMinimum", "-1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The minimum value of the input image." );
	processObjectSettings->AddSetting( "InputMaximum", "-1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The maximum value of the input image." );
    processObjectSettings->AddSetting( "OutputMinimum", "0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The minimum value of the output image." );
	processObjectSettings->AddSetting( "OutputMaximum", "1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The maximum value of the output image." );

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
IntensityWindowingImageFilterWrapper< TInputImage >::~IntensityWindowingImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void IntensityWindowingImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	typename TInputImage::PixelType inputMinimum = processObjectSettings->GetSettingValue( "InputMinimum" ).toFloat();
	typename TInputImage::PixelType inputMaximum = processObjectSettings->GetSettingValue( "InputMaximum" ).toFloat();
	typename TInputImage::PixelType outputMinimum = processObjectSettings->GetSettingValue( "OutputMinimum" ).toFloat();
	typename TInputImage::PixelType outputMaximum = processObjectSettings->GetSettingValue( "OutputMaximum" ).toFloat();
	const float useQuantiles = processObjectSettings->GetSettingValue("UseQuantiles").toFloat();

	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();

	// automatically calculate the minimum and maximum values if they are not set
	if (inputMinimum < 0 || inputMaximum < 0 || useQuantiles > 0)
	{
		typedef itk::MinimumMaximumImageCalculator<TInputImage> MinMaxCalculatorType;
		typename MinMaxCalculatorType::Pointer minMaxCalc = MinMaxCalculatorType::New();
		minMaxCalc->SetImage( inputImage );
		minMaxCalc->Compute();

		if (inputMinimum < 0)
			inputMinimum = minMaxCalc->GetMinimum();

		if (inputMaximum < 0)
			inputMaximum = minMaxCalc->GetMaximum();

		// if enabled, apply a quantile-based intensity normalization
		if (useQuantiles > 0)
		{
			constexpr unsigned int MeasurementVectorSize = 1; // only for grayscale images right now
			const auto             binsPerDimension = 256; // potentially add as a parameter to the GUI

			// define the image to histogram filter and compute histogram bounds
			typedef itk::Statistics::ImageToHistogramFilter<TInputImage> ImageToHistogramFilterType;
			typename ImageToHistogramFilterType::HistogramType::MeasurementVectorType lowerBound(binsPerDimension);
			typename ImageToHistogramFilterType::HistogramType::MeasurementVectorType upperBound(binsPerDimension);
			typename ImageToHistogramFilterType::HistogramType::SizeType size(MeasurementVectorSize);
			lowerBound.Fill(minMaxCalc->GetMinimum());
			upperBound.Fill(minMaxCalc->GetMaximum());	
			size.Fill(binsPerDimension);

			// create histogram to image filter and set inputs
			typename ImageToHistogramFilterType::Pointer imageToHistogramFilter = ImageToHistogramFilterType::New();
			imageToHistogramFilter->SetInput(inputImage);
			imageToHistogramFilter->SetHistogramBinMinimum(lowerBound);
			imageToHistogramFilter->SetHistogramBinMaximum(upperBound);
			imageToHistogramFilter->SetHistogramSize(size);

			// update the filter
			itkTryCatch(imageToHistogramFilter->Update(), "Error: ImageToHistogramFilter");

			// extract quantiles to be used for intensity normalization
			typename ImageToHistogramFilterType::HistogramType* histogram = imageToHistogramFilter->GetOutput();
			inputMinimum = histogram->Quantile(0, useQuantiles);
			inputMaximum = histogram->Quantile(0, 1.0-useQuantiles);
			std::cout << "Using quantile-based intensity normalization with lower quantile " << inputMinimum << ", upper quantile " << inputMaximum << std::endl;
		}
	}

	// apply the intensity transformation
	typedef itk::IntensityWindowingImageFilter<TInputImage, TInputImage> IntensityFilterType;
	typename IntensityFilterType::Pointer intensityFilter = IntensityFilterType::New();
	intensityFilter->SetInput( inputImage );
	intensityFilter->SetReleaseDataFlag( true );
	intensityFilter->SetWindowMinimum( inputMinimum );
	intensityFilter->SetWindowMaximum( inputMaximum );
	intensityFilter->SetOutputMinimum( outputMinimum );
    intensityFilter->SetOutputMaximum( outputMaximum );
	intensityFilter->SetNumberOfWorkUnits( maxThreads );
	itkTryCatch(intensityFilter->Update(), "Error: IntensityWindowingImageFilterWrapper Update Function.");
	
	// set the output image
	ImageWrapper *outputWrapper = new ImageWrapper();
	outputWrapper->SetImage<TInputImage>( intensityFilter->GetOutput() );
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< IntensityWindowingImageFilterWrapper<Image2Float> > IntensityWindowingImageFilterWrapperImage2Float;
static ProcessObjectProxy< IntensityWindowingImageFilterWrapper<Image3Float> > IntensityWindowingImageFilterWrapperImage3Float;
static ProcessObjectProxy< IntensityWindowingImageFilterWrapper<Image2UShort> > IntensityWindowingImageFilterWrapperImage2UShort;
static ProcessObjectProxy< IntensityWindowingImageFilterWrapper<Image3UShort> > IntensityWindowingImageFilterWrapperImage3UShort;
    
} // namespace XPIWIT

