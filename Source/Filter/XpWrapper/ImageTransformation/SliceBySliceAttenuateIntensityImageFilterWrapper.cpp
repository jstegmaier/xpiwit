/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "SliceBySliceAttenuateIntensityImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "../../ITKCustom/itkSliceBySliceAttenuateIntensityImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkNumericTraits.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
SliceBySliceAttenuateIntensityImageFilterWrapper< TInputImage >::SliceBySliceAttenuateIntensityImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = SliceBySliceAttenuateIntensityImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Attenuates the intensity of the image according to its content along a specified direction.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "Delta", "0.01", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The attenuation factor. Higher values result in higher attenuation and vice versa." );
	processObjectSettings->AddSetting( "MinAttenuation", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The minimum attenuation level (used at the slice nearest to the detection objective)." );
	processObjectSettings->AddSetting( "MaxAttenuation", "0.8", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The maximum attenuation level (used at the slice farthest from the detection objective)." );	
	processObjectSettings->AddSetting( "MinSigma", "0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The minimum blur level (used at the slice nearest to the detection objective)." );
	processObjectSettings->AddSetting( "MaxSigma", "0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The minimum blur level (used at the slice farthest from the detection objective)." );
	processObjectSettings->AddSetting( "UseImageSpacing", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "The attenuation factor. Higher values result in higher attenuation and vice versa." );
	processObjectSettings->AddSetting( "ExponentialAttenuation", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "The attenuation function. 0 for linear and 1 for exponential." );
	processObjectSettings->AddSetting( "InvertAttenuationDirection", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, 1-attenuation is used, i.e. a rotation around the y axis is simulated." );
	processObjectSettings->AddSetting( "Dimensionality", "3", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The attenuation factor. Higher values result in higher attenuation and vice versa." );
	
	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
SliceBySliceAttenuateIntensityImageFilterWrapper< TInputImage >::~SliceBySliceAttenuateIntensityImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void SliceBySliceAttenuateIntensityImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const float delta = processObjectSettings->GetSettingValue( "Delta" ).toFloat();
	const float minSigma = processObjectSettings->GetSettingValue( "MinSigma" ).toFloat();
	const float maxSigma = processObjectSettings->GetSettingValue( "MaxSigma" ).toFloat();
	const float minAttenuation = processObjectSettings->GetSettingValue( "MinAttenuation" ).toFloat();
	const float maxAttenuation = processObjectSettings->GetSettingValue( "MaxAttenuation" ).toFloat();
	const bool useImageSpacing = processObjectSettings->GetSettingValue( "UseImageSpacing" ).toInt() > 0;
	const bool exponentialAttenuation = processObjectSettings->GetSettingValue( "ExponentialAttenuation" ).toInt() > 0;
	const bool invertAttenuationDirection = processObjectSettings->GetSettingValue( "InvertAttenuationDirection" ).toInt() > 0;
	const unsigned int dimensionality = processObjectSettings->GetSettingValue( "Dimensionality" ).toInt();
	
	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();

	typedef itk::SliceBySliceAttenuateIntensityImageFilter<TInputImage, TInputImage> IntensityFilterType;
	typename IntensityFilterType::Pointer intensityFilter = IntensityFilterType::New();
	intensityFilter->SetInput( inputImage );
	intensityFilter->SetNumberOfWorkUnits( maxThreads );
	intensityFilter->SetDelta( delta );
	intensityFilter->SetMinSigma( minSigma );
	intensityFilter->SetMaxSigma( maxSigma );
	intensityFilter->SetMinAttenuation( minAttenuation );
	intensityFilter->SetMaxAttenuation( maxAttenuation );
	intensityFilter->SetExponentialAttenuation( exponentialAttenuation );
	intensityFilter->SetUseImageSpacing( useImageSpacing );
	intensityFilter->SetDimensionality( dimensionality );
	intensityFilter->SetInvertAttenuationDirection( invertAttenuationDirection );
	intensityFilter->SetReleaseDataFlag( true );
	itkTryCatch(intensityFilter->Update(), "Error: SliceBySliceAttenuateIntensityImageFilterWrapper Update Function.");
	
	ImageWrapper *outputWrapper = new ImageWrapper();
	outputWrapper->SetImage<TInputImage>( intensityFilter->GetOutput() );
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< SliceBySliceAttenuateIntensityImageFilterWrapper<Image2Float> > SliceBySliceAttenuateIntensityImageFilterWrapperImage2Float;
static ProcessObjectProxy< SliceBySliceAttenuateIntensityImageFilterWrapper<Image3Float> > SliceBySliceAttenuateIntensityImageFilterWrapperImage3Float;
static ProcessObjectProxy< SliceBySliceAttenuateIntensityImageFilterWrapper<Image2UShort> > SliceBySliceAttenuateIntensityImageFilterWrapperImage2UShort;
static ProcessObjectProxy< SliceBySliceAttenuateIntensityImageFilterWrapper<Image3UShort> > SliceBySliceAttenuateIntensityImageFilterWrapperImage3UShort;

} // namespace XPIWIT

