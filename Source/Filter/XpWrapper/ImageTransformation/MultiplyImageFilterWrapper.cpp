/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "MultiplyImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkMultiplyImageFilter.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
MultiplyImageFilterWrapper< TInputImage >::MultiplyImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = MultiplyImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Pixel-wise multiplication of two images.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(2);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "Constant", "0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "If non-zero, this is used instead of the second input image for multiplication." );

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
MultiplyImageFilterWrapper< TInputImage >::~MultiplyImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void MultiplyImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const float constant = processObjectSettings->GetSettingValue( "Constant" ).toFloat();
	
	// get images
	typename TInputImage::Pointer inputImage1 = mInputImages.at(0)->template GetImage<TInputImage>();
	typename TInputImage::Pointer inputImage2 = mInputImages.at(1)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();
	
	// setup the filter
	typedef itk::MultiplyImageFilter<TInputImage, TInputImage> FilterType;
	typename FilterType::Pointer filter = FilterType::New();

	if (constant == 0)
	{
		filter->SetInput1( inputImage1 );
		filter->SetInput2( inputImage2 );
		filter->SetReleaseDataFlag( true );
	}
	else
	{
		filter->SetInput1( inputImage1 );
		filter->SetConstant( constant );
		filter->SetReleaseDataFlag( true );
	}

	itkTryCatch(filter->Update(), "Error: MultiplyImageFilterWrapper Update Function.");
	
	ImageWrapper *outputWrapper = new ImageWrapper();
	outputWrapper->SetImage<TInputImage>( filter->GetOutput() );
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< MultiplyImageFilterWrapper<Image2Float> > MultiplyImageFilterWrapperImage2Float;
static ProcessObjectProxy< MultiplyImageFilterWrapper<Image3Float> > MultiplyImageFilterWrapperImage3Float;
static ProcessObjectProxy< MultiplyImageFilterWrapper<Image2UShort> > MultiplyImageFilterWrapperImage2UShort;
static ProcessObjectProxy< MultiplyImageFilterWrapper<Image3UShort> > MultiplyImageFilterWrapperImage3UShort;

} // namespace XPIWIT

