/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "SliceBySliceAdjustIntensityImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "../../ITKCustom/itkSliceBySliceAdjustIntensityImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkNumericTraits.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
SliceBySliceAdjustIntensityImageFilterWrapper< TInputImage >::SliceBySliceAdjustIntensityImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = SliceBySliceAdjustIntensityImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Rescales the intensity of an image separately for each slice.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);

	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);

	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);

	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "DebugOutput", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, the debug output is written to the log file." );
	processObjectSettings->AddSetting( "ScaleToMinMax", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, the intensity range is linearly transformed to the minimum and maximum intensity." );
	processObjectSettings->AddSetting( "Quantile", "-1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "If positive, the intensity range is linearly transformed to the quantiles correcponding to quantile and 1-quantile." );
	processObjectSettings->AddSetting( "MinSlice", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "If positive, only the slices larger than this number are processed." );
	processObjectSettings->AddSetting( "MaxSlice", "100000", ProcessObjectSetting::SETTINGVALUETYPE_INT, "If positive, only the slices smaller than this number are processed." );

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
SliceBySliceAdjustIntensityImageFilterWrapper< TInputImage >::~SliceBySliceAdjustIntensityImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void SliceBySliceAdjustIntensityImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const float quantile = processObjectSettings->GetSettingValue( "Quantile" ).toFloat();
	const bool scaleToMinMax = processObjectSettings->GetSettingValue( "ScaleToMinMax" ).toInt() > 0;
	const bool debugOutput = processObjectSettings->GetSettingValue( "DebugOutput" ).toInt() > 0;
	const unsigned int minSlice = processObjectSettings->GetSettingValue( "MinSlice" ).toInt();
	const unsigned int maxSlice = processObjectSettings->GetSettingValue( "MaxSlice" ).toInt();
	
	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();

	typedef itk::SliceBySliceAdjustIntensityImageFilter<TInputImage, TInputImage> IntensityFilterType;
	typename IntensityFilterType::Pointer intensityFilter = IntensityFilterType::New();
	intensityFilter->SetScaleToMinMax( scaleToMinMax );
	intensityFilter->SetQuantile( quantile );
	intensityFilter->SetMinSlice( minSlice );
	intensityFilter->SetMaxSlice( maxSlice );
	intensityFilter->SetDebugOutput( debugOutput );
	intensityFilter->SetInput( inputImage );
	intensityFilter->SetReleaseDataFlag( true );

	itkTryCatch(intensityFilter->Update(), "Error: SliceBySliceAdjustIntensityImageFilterWrapper Update Function.");
	
	ImageWrapper *outputWrapper = new ImageWrapper();
	outputWrapper->SetImage<TInputImage>( intensityFilter->GetOutput() );
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< SliceBySliceAdjustIntensityImageFilterWrapper<Image2Float> > SliceBySliceAdjustIntensityImageFilterWrapperImage2Float;
static ProcessObjectProxy< SliceBySliceAdjustIntensityImageFilterWrapper<Image3Float> > SliceBySliceAdjustIntensityImageFilterWrapperImage3Float;
static ProcessObjectProxy< SliceBySliceAdjustIntensityImageFilterWrapper<Image2UShort> > SliceBySliceAdjustIntensityImageFilterWrapperImage2UShort;
static ProcessObjectProxy< SliceBySliceAdjustIntensityImageFilterWrapper<Image3UShort> > SliceBySliceAdjustIntensityImageFilterWrapperImage3UShort;


} // namespace XPIWIT

