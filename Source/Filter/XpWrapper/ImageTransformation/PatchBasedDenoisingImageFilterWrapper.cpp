/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "PatchBasedDenoisingImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkPatchBasedDenoisingImageFilter.h"
#include "itkNumericTraits.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
PatchBasedDenoisingImageFilterWrapper< TInputImage >::PatchBasedDenoisingImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = PatchBasedDenoisingImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Performs a patch based denoising such as non-local means filtering.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);
    
    // add settings to the filter
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "PatchRadius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The patch radius to use." );
    processObjectSettings->AddSetting( "NoiseSigma", "1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The standard deviation of the noise." );
    processObjectSettings->AddSetting( "UseSmoothDiscPatchWeights", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "The noise model to use. 0=NOMODEL, 1=GAUSSIAN, 2=RICIAN, 3=POISSON." );
    processObjectSettings->AddSetting( "KernelBandwidthSigma", "0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Set the ." );
    processObjectSettings->AddSetting( "KernelBandwidthFractionPixelsForEstimation", "1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "..." );
    processObjectSettings->AddSetting( "ComputeConditionalDerivatives", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "..." );
    processObjectSettings->AddSetting( "UseFastTensorComputations", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "..." );
    processObjectSettings->AddSetting( "KernelBandwidthMultiplicationFactor", "0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "..." );
    processObjectSettings->AddSetting( "NumberOfIterations", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Set the number of denoising iterations to perform. Must be a positive integer. Defaults to 1." );
    
    /*

    processObjectSettings->AddSetting( "NoiseModel", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The noise model to use. 0=NOMODEL, 1=GAUSSIAN, 2=RICIAN, 3=POISSON." );
    processObjectSettings->AddSetting( "SmoothingWeight", "0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Set the weight on the smoothing term. This option is used when a noise model is specified. This weight controls the balance between the smoothing and the closeness to the noisy data." );
    processObjectSettings->AddSetting( "NoiseModelFidelityWeight", "1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Set the weight on the fidelity term (penalizes deviations from the noisy data). This option is used when a noise model is specified. This weight controls the balance between the smoothing and the closeness to the noisy data." );
    processObjectSettings->AddSetting( "KernelBandwidthEstimation", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Set flag indicating whether kernel-bandwidth should be estimated automatically from the image data. Defaults to false." );
    processObjectSettings->AddSetting( "KernelBandwidthUpdateFrequency", "3", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Set the update frequency for the kernel bandwidth estimation. An optimal bandwidth will be re-estimated based on the denoised image after every 'n' iterations. Must be a positive integer. Defaults to 3, i.e. bandwidth updated after every 3 denoising iteration." );

    processObjectSettings->AddSetting( "AlwaysTreatComponentsAsEuclidean", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Set flag indicating whether all components should always be treated as if they are in euclidean space regardless of pixel type. Defaults to false." );
    */

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
PatchBasedDenoisingImageFilterWrapper< TInputImage >::~PatchBasedDenoisingImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void PatchBasedDenoisingImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
    const unsigned int patchRadius = processObjectSettings->GetSettingValue( "PatchRadius" ).toInt();
    const unsigned int numberOfIterations = processObjectSettings->GetSettingValue( "NumberOfIterations" ).toInt();
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    const float noiseSigma = processObjectSettings->GetSettingValue( "NoiseSigma" ).toFloat();
    const bool useSmoothDiscPatchWeights = processObjectSettings->GetSettingValue( "UseSmoothDiscPatchWeights" ).toInt() > 0;
    const int smoothingWeight = processObjectSettings->GetSettingValue( "SmoothingWeight" ).toFloat();
    const float noiseModelFidelityWeight = processObjectSettings->GetSettingValue( "NoiseModelFidelityWeight" ).toFloat();

    /*
    const unsigned int noiseModel = processObjectSettings->GetSettingValue( "NoiseModel" ).toInt();
    const int smoothingWeight = processObjectSettings->GetSettingValue( "SmoothingWeight" ).toFloat();
    const float noiseModelFidelityWeight = processObjectSettings->GetSettingValue( "NoiseModelFidelityWeight" ).toFloat();
    const bool kernelBandwidthEstimation = processObjectSettings->GetSettingValue( "KernelBandwidthEstimation" ).toInt() > 0;
    const unsigned int kernelBandwidthUpdateFrequency = processObjectSettings->GetSettingValue( "KernelBandwidthUpdateFrequency" ).toInt();
    const bool alwaysTreatComponentsAsEuclidean = processObjectSettings->GetSettingValue( "AlwaysTreatComponentsAsEuclidean" ).toInt() > 0;
     */
	
	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();

	typedef itk::PatchBasedDenoisingImageFilter<TInputImage, TInputImage> PatchBasedDenoisingFilter;
	typename PatchBasedDenoisingFilter::Pointer denoisingFilter = PatchBasedDenoisingFilter::New();
	denoisingFilter->SetInput( inputImage );
    denoisingFilter->SetPatchRadius( patchRadius );
    denoisingFilter->SetNumberOfIterations( numberOfIterations );
	denoisingFilter->SetReleaseDataFlag( true );
    
	
	itkTryCatch(denoisingFilter->Update(), "Error: PatchBasedDenoisingImageFilterWrapper Update Function.");
	
	ImageWrapper *outputWrapper = new ImageWrapper();
	outputWrapper->SetImage<TInputImage>( denoisingFilter->GetOutput() );
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< PatchBasedDenoisingImageFilterWrapper<Image2Float> > PatchBasedDenoisingImageFilterWrapperImage2Float;
static ProcessObjectProxy< PatchBasedDenoisingImageFilterWrapper<Image3Float> > PatchBasedDenoisingImageFilterWrapperImage3Float;
static ProcessObjectProxy< PatchBasedDenoisingImageFilterWrapper<Image2UShort> > PatchBasedDenoisingImageFilterWrapperImage2UShort;
static ProcessObjectProxy< PatchBasedDenoisingImageFilterWrapper<Image3UShort> > PatchBasedDenoisingImageFilterWrapperImage3UShort;

} // namespace XPIWIT

