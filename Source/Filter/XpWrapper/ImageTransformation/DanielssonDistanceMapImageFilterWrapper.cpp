/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "DanielssonDistanceMapImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkDanielssonDistanceMapImageFilter.h"
#include "itkSliceBySliceDanielssonDistanceMapImageFilter.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
DanielssonDistanceMapImageFilterWrapper< TInputImage >::DanielssonDistanceMapImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = DanielssonDistanceMapImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "This filter computes the distance map of the input image as an approximation with pixel accuracy to the Euclidean distance.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "BinaryInput", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Set if input is binary." );
	processObjectSettings->AddSetting( "UseSquareDistance", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Set to use the square of the distance." );
	processObjectSettings->AddSetting( "UseImageSpacing", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Set to use the image spacing." );
	processObjectSettings->AddSetting( "SliceBySlice", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, the distance map is calcualted for each slice individually.");

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
DanielssonDistanceMapImageFilterWrapper< TInputImage >::~DanielssonDistanceMapImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void DanielssonDistanceMapImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const bool useBinaryInput = processObjectSettings->GetSettingValue( "BinaryInput" ).toInt() > 0;
	const bool useSquareDistance = processObjectSettings->GetSettingValue( "UseSquareDistance" ).toInt() > 0;
	const bool useImageSpacing = processObjectSettings->GetSettingValue( "UseImageSpacing" ).toInt() > 0;
	const bool sliceBySlice = processObjectSettings->GetSettingValue("SliceBySlice").toInt() > 0;
	const bool maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	
	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();

	// define the distance map image
	typename TInputImage::Pointer distanceMapImage;
	
	if (sliceBySlice == false)
	{
		// setup the filter
		typedef itk::DanielssonDistanceMapImageFilter<TInputImage, TInputImage> FilterType;
		typename FilterType::Pointer filter = FilterType::New();
		filter->SetInput(inputImage);
		filter->SetReleaseDataFlag(true);
		filter->SetNumberOfWorkUnits(maxThreads);
		filter->SetInputIsBinary(useBinaryInput);
		filter->SetSquaredDistance(useSquareDistance);
		filter->SetUseImageSpacing(useImageSpacing);
		itkTryCatch(filter->Update(), "Error: DanielssonDistanceMapImageFilterWrapper Update Function.");
		distanceMapImage = filter->GetOutput();
	}
	else
	{
		// setup the filter
		typedef itk::SliceBySliceDanielssonDistanceMapImageFilter<TInputImage, TInputImage> FilterType;
		typename FilterType::Pointer filter = FilterType::New();
		filter->SetInput(inputImage);
		filter->SetReleaseDataFlag(true);
		filter->SetNumberOfWorkUnits(maxThreads);
		filter->SetInputIsBinary(useBinaryInput);
		filter->SetSquaredDistance(useSquareDistance);
		filter->SetUseImageSpacing(useImageSpacing);
		itkTryCatch(filter->Update(), "Error: DanielssonDistanceMapImageFilterWrapper Update Function.");
		distanceMapImage = filter->GetOutput();
	}
	
	ImageWrapper *outputWrapper = new ImageWrapper();
	outputWrapper->SetImage<TInputImage>( distanceMapImage );
	outputWrapper->SetRescaleFlag(false);
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< DanielssonDistanceMapImageFilterWrapper<Image2Float> > DanielssonDistanceMapImageFilterWrapperImage2Float;
static ProcessObjectProxy< DanielssonDistanceMapImageFilterWrapper<Image3Float> > DanielssonDistanceMapImageFilterWrapperImage3Float;
static ProcessObjectProxy< DanielssonDistanceMapImageFilterWrapper<Image2UShort> > DanielssonDistanceMapImageFilterWrapperImage2UShort;
static ProcessObjectProxy< DanielssonDistanceMapImageFilterWrapper<Image3UShort> > DanielssonDistanceMapImageFilterWrapperImage3UShort;

} // namespace XPIWIT

