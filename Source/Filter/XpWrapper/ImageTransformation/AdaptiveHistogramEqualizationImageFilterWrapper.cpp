/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "AdaptiveHistogramEqualizationImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkAdaptiveHistogramEqualizationImageFilter.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
AdaptiveHistogramEqualizationImageFilterWrapper< TInputImage >::AdaptiveHistogramEqualizationImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = AdaptiveHistogramEqualizationImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Rescales the image using local histogram information for intensity equalization.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "Alpha", "0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Alpha parameter: 0 for histogram equalization, 1 for unsharp mask." );
	processObjectSettings->AddSetting( "Beta", "1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Beta parameter: 0 for unsharp mask and 1 for pass through." );
	processObjectSettings->AddSetting( "Radius", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Set to radius to use for statistical calculations." );
	processObjectSettings->AddSetting( "UseLookupTable", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled lookup tables are used for intensity mapping." );

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
AdaptiveHistogramEqualizationImageFilterWrapper< TInputImage >::~AdaptiveHistogramEqualizationImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void AdaptiveHistogramEqualizationImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const float alpha = processObjectSettings->GetSettingValue( "Alpha" ).toInt();
	const float beta = processObjectSettings->GetSettingValue( "Beta" ).toInt();
	const bool useLookupTable = processObjectSettings->GetSettingValue( "UseLookupTable" ).toInt() > 0;
	const int radius = processObjectSettings->GetSettingValue( "Radius" ).toInt();
	
	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();

	typedef itk::AdaptiveHistogramEqualizationImageFilter<TInputImage> HistogramEqualizationFilterType;
	typename HistogramEqualizationFilterType::Pointer histogramEqualizationFilter = HistogramEqualizationFilterType::New();
	histogramEqualizationFilter->SetInput( inputImage );
	histogramEqualizationFilter->SetReleaseDataFlag( true );
	histogramEqualizationFilter->SetAlpha( alpha );
    histogramEqualizationFilter->SetBeta( beta );
	histogramEqualizationFilter->SetRadius( radius );
	histogramEqualizationFilter->SetUseLookupTable( useLookupTable );

	itkTryCatch(histogramEqualizationFilter->Update(), "Error: AdaptiveHistogramEqualizationImageFilterWrapper Update Function.");
	
	ImageWrapper *outputWrapper = new ImageWrapper();
	outputWrapper->SetImage<TInputImage>( histogramEqualizationFilter->GetOutput() );
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< AdaptiveHistogramEqualizationImageFilterWrapper<Image2Float> > AdaptiveHistogramEqualizationImageFilterWrapperImage2Float;
static ProcessObjectProxy< AdaptiveHistogramEqualizationImageFilterWrapper<Image3Float> > AdaptiveHistogramEqualizationImageFilterWrapperImage3Float;
static ProcessObjectProxy< AdaptiveHistogramEqualizationImageFilterWrapper<Image2UShort> > AdaptiveHistogramEqualizationImageFilterWrapperImage2UShort;
static ProcessObjectProxy< AdaptiveHistogramEqualizationImageFilterWrapper<Image3UShort> > AdaptiveHistogramEqualizationImageFilterWrapperImage3UShort;

} // namespace XPIWIT

