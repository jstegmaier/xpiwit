/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "AdditiveGaussianNoiseImageFilterWrapper.h"
#include "../../../Filter/Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "../../ITKCustom/itkAdditiveGaussianNoiseImageFilter.h"


namespace XPIWIT
{

// the default constructor
template< class TInputImage >
AdditiveGaussianNoiseImageFilterWrapper<TInputImage>::AdditiveGaussianNoiseImageFilterWrapper() : ProcessObjectBase()
{
    // set the widget type
    this->mName = AdditiveGaussianNoiseImageFilterWrapper<TInputImage>::GetName();
    this->mDescription = "Add additive Gaussian noise to an image.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings to the filter
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Mean", "0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Mean of the Gaussian noise." );
    processObjectSettings->AddSetting( "StandardDeviation", "1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Standard deviation of the Gaussian noise." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
AdditiveGaussianNoiseImageFilterWrapper<TInputImage>::~AdditiveGaussianNoiseImageFilterWrapper()
{
}


// update the filter
template< class TInputImage >
void AdditiveGaussianNoiseImageFilterWrapper<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const float mean = processObjectSettings->GetSettingValue( "Mean" ).toFloat();
	const float standardDeviation = processObjectSettings->GetSettingValue( "StandardDeviation" ).toFloat();
    const int filterMask3D = processObjectSettings->GetSettingValue( "FilterMask3D" ).toInt() > 0;
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

    // set the parameters
    typedef itk::AdditiveGaussianNoiseImageFilter<TInputImage, TInputImage> NoiseFilterType;
    typename NoiseFilterType::Pointer filter = NoiseFilterType::New();
    filter->SetReleaseDataFlag( releaseDataFlag );
    filter->SetInput( inputImage );
    filter->SetNumberOfWorkUnits( maxThreads );
	filter->SetMean( mean );
	filter->SetStandardDeviation( standardDeviation );

    // update the filter
    itkTryCatch( filter->Update(), "Error: AdditiveGaussianNoiseImageFilter Update Function." );
    ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( filter->GetOutput() );
    mOutputImages.append( outputImage );

    // update the process update widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< AdditiveGaussianNoiseImageFilterWrapper<Image2Float> > AdditiveGaussianNoiseImageFilterWrapperImage2Float;
static ProcessObjectProxy< AdditiveGaussianNoiseImageFilterWrapper<Image3Float> > AdditiveGaussianNoiseImageFilterWrapperImage3Float;
static ProcessObjectProxy< AdditiveGaussianNoiseImageFilterWrapper<Image2UShort> > AdditiveGaussianNoiseImageFilterWrapperImage2UShort;
static ProcessObjectProxy< AdditiveGaussianNoiseImageFilterWrapper<Image3UShort> > AdditiveGaussianNoiseImageFilterWrapperImage3UShort;

} // namespace XPIWIT
