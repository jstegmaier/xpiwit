/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "SpeckleNoiseImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "../../ITKCustom/itkSpeckleNoiseImageFilter.h"


namespace XPIWIT
{

// the default constructor
template< class TInputImage >
SpeckleNoiseImageFilterWrapper<TInputImage>::SpeckleNoiseImageFilterWrapper() : ProcessObjectBase()
{
    // set the widget type
    this->mName = SpeckleNoiseImageFilterWrapper<TInputImage>::GetName();
    this->mDescription = "Adds speckle noise to the input image. ";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings to the filter
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "StandardDeviation", "1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Standard deviation of the speckle noise to produce." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
SpeckleNoiseImageFilterWrapper<TInputImage>::~SpeckleNoiseImageFilterWrapper()
{
}


// update the filter
template< class TInputImage >
void SpeckleNoiseImageFilterWrapper<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const float standardDeviation = processObjectSettings->GetSettingValue( "StandardDeviation" ).toFloat();
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

    // set the parameters
    typedef itk::SpeckleNoiseImageFilter<TInputImage, TInputImage> NoiseFilterType;
    typename NoiseFilterType::Pointer filter = NoiseFilterType::New();

    filter->SetReleaseDataFlag( releaseDataFlag );
    filter->SetInput( inputImage );
    filter->SetNumberOfWorkUnits( maxThreads );
	filter->SetStandardDeviation( standardDeviation );

    // update the filter
    itkTryCatch( filter->Update(), "Error: SpeckleNoiseImageFilter Update Function." );
    ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( filter->GetOutput() );
    mOutputImages.append( outputImage );

    // update the process update widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< SpeckleNoiseImageFilterWrapper<Image2Float> > SpeckleNoiseImageFilterWrapperImage2Float;
static ProcessObjectProxy< SpeckleNoiseImageFilterWrapper<Image3Float> > SpeckleNoiseImageFilterWrapperImage3Float;
static ProcessObjectProxy< SpeckleNoiseImageFilterWrapper<Image2UShort> > SpeckleNoiseImageFilterWrapperImage2UShort;
static ProcessObjectProxy< SpeckleNoiseImageFilterWrapper<Image3UShort> > SpeckleNoiseImageFilterWrapperImage3UShort;

} // namespace XPIWIT
