/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "SliceBySliceExtractRegionPropsImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "../../ITKCustom/itkSliceBySliceExtractRegionPropsImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkNumericTraits.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
SliceBySliceExtractRegionPropsImageFilterWrapper< TInputImage >::SliceBySliceExtractRegionPropsImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = SliceBySliceExtractRegionPropsImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Rescales the intensity of an image separately for each slice.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(3);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);

	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(1);
	this->mObjectType->AppendMetaOutputType("SliceBySliceRegionProps");

	// add settings
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "DebugOutput", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, the debug output is written to the log file." );
	processObjectSettings->AddSetting( "CalculateOrientedBoundingBox", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, oriented bounding box will be calculated." );
	processObjectSettings->AddSetting( "CalculateOrientedIntensityRegions", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, oriented intensity regions will be calculated." );
	processObjectSettings->AddSetting( "CalculateOrientedLabelRegions", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, oriented label regions will be calculated." );
	processObjectSettings->AddSetting( "CalculatePixelIndices", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, pixel indices will be calculated." );
	processObjectSettings->AddSetting( "BinaryInput", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, binary input is initially transformed to a label image using connected components." );
	processObjectSettings->AddSetting( "FullyConnected", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, 8-neighborhood is used instead of 4 neighborhood for 2D images." );
	processObjectSettings->AddSetting( "InputForegroundValue", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The input foreground intensity value, usually set to 1." );
	processObjectSettings->AddSetting( "OutputBackgroundValue", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The output background intensity value, usually set to 0." );
	processObjectSettings->AddSetting( "MinSlice", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "If positive, only the slices larger than this number are processed." );
	processObjectSettings->AddSetting( "MaxSlice", "100000", ProcessObjectSetting::SETTINGVALUETYPE_INT, "If positive, only the slices smaller than this number are processed." );
	processObjectSettings->AddSetting( "MinimumSeedArea", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "If seeds are smaller than this value, no labeling is performed." );
	processObjectSettings->AddSetting( "MaximumVolume", "-1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "If seeds are larger than this value, no labeling is performed." );

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
SliceBySliceExtractRegionPropsImageFilterWrapper< TInputImage >::~SliceBySliceExtractRegionPropsImageFilterWrapper()
{
}


// tue update function
template < class TInputImage >
void SliceBySliceExtractRegionPropsImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const float inputForegroundValue = processObjectSettings->GetSettingValue( "InputForegroundValue" ).toFloat();
	const float outputBackgroundValue = processObjectSettings->GetSettingValue( "OutputBackgroundValue" ).toFloat();
	const unsigned int minSlice = processObjectSettings->GetSettingValue( "MinSlice" ).toInt();
	const unsigned int maxSlice = processObjectSettings->GetSettingValue( "MaxSlice" ).toInt();
	const unsigned int minimumSeedArea = processObjectSettings->GetSettingValue( "MinimumSeedArea" ).toInt();
	const unsigned int maximumVolume = processObjectSettings->GetSettingValue( "MaximumVolume" ).toInt();
	const bool calculateOrientedBoundingBox = processObjectSettings->GetSettingValue( "CalculateOrientedBoundingBox" ).toInt() > 0;
	const bool calculateOrientedIntensityRegions = processObjectSettings->GetSettingValue( "CalculateOrientedIntensityRegions" ).toInt() > 0;
	const bool calculateOrientedLabelRegions = processObjectSettings->GetSettingValue( "CalculateOrientedLabelRegions" ).toInt() > 0;
	const bool calculatePixelIndices = processObjectSettings->GetSettingValue( "CalculatePixelIndices" ).toInt() > 0;
	const bool binaryInput = processObjectSettings->GetSettingValue( "BinaryInput" ).toInt() > 0;
	const bool fullyConnected = processObjectSettings->GetSettingValue( "FullyConnected" ).toInt() > 0;
	const bool debugOutput = processObjectSettings->GetSettingValue( "DebugOutput" ).toInt() > 0;

	// Create the meta data Object
	MetaDataFilter* metaOutput = this->mMetaOutputs.at(0);
    metaOutput->mIsMultiDimensional = true;
	metaOutput->mPostfix = "SliceBySliceRegionProps";
	
	// get images
	typename TInputImage::Pointer labelImage = mInputImages.at(0)->template GetImage<TInputImage>();
	typename TInputImage::Pointer intensityImage = mInputImages.at(1)->template GetImage<TInputImage>();
	typename TInputImage::Pointer seedImage = mInputImages.at(2)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();

	typedef itk::SliceBySliceExtractRegionPropsImageFilter<TInputImage, TInputImage> RegionPropsFilterType;
	typename RegionPropsFilterType::Pointer regionPropsFilter = RegionPropsFilterType::New();
	regionPropsFilter->SetCalculateOrientedBoundingBox( calculateOrientedBoundingBox );
	regionPropsFilter->SetCalculateOrientedIntensityRegions( calculateOrientedIntensityRegions );
	regionPropsFilter->SetCalculateOrientedLabelRegions( calculateOrientedLabelRegions );
	regionPropsFilter->SetCalculatePixelIndices( calculatePixelIndices );
	regionPropsFilter->SetBinaryInput( binaryInput );
	regionPropsFilter->SetFullyConnected( fullyConnected );
	regionPropsFilter->SetNumberOfWorkUnits( maxThreads );
	regionPropsFilter->SetMinSlice( minSlice );
	regionPropsFilter->SetMaxSlice( maxSlice );
	regionPropsFilter->SetMinimumSeedArea( minimumSeedArea );
	regionPropsFilter->SetMaximumVolume( maximumVolume );
	regionPropsFilter->SetInput( labelImage );
	regionPropsFilter->SetIntensityImage( intensityImage );
	regionPropsFilter->SetSeedImage( seedImage );
	regionPropsFilter->SetDebugOutput( debugOutput );
    regionPropsFilter->SetOutputMetaFilter( this->mMetaOutputs.at(0) );
	regionPropsFilter->SetReleaseDataFlag( true );

	itkTryCatch(regionPropsFilter->Update(), "Error: SliceBySliceExtractRegionPropsImageFilterWrapper Update Function.");
	
	ImageWrapper *outputWrapper = new ImageWrapper();
	outputWrapper->SetImage<TInputImage>( regionPropsFilter->GetOutput() );
	outputWrapper->SetRescaleFlag( false );
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< SliceBySliceExtractRegionPropsImageFilterWrapper<Image2Float> > SliceBySliceExtractRegionPropsImageFilterWrapperImage2Float;
static ProcessObjectProxy< SliceBySliceExtractRegionPropsImageFilterWrapper<Image3Float> > SliceBySliceExtractRegionPropsImageFilterWrapperImage3Float;
static ProcessObjectProxy< SliceBySliceExtractRegionPropsImageFilterWrapper<Image2UShort> > SliceBySliceExtractRegionPropsImageFilterWrapperImage2UShort;
static ProcessObjectProxy< SliceBySliceExtractRegionPropsImageFilterWrapper<Image3UShort> > SliceBySliceExtractRegionPropsImageFilterWrapperImage3UShort;

} // namespace XPIWIT

