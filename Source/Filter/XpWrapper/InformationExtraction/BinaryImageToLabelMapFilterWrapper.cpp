/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// namespace header
#include "BinaryImageToLabelMapFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkBinaryImageToLabelMapFilter.h"
#include "itkLabelMapToLabelImageFilter.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
BinaryImageToLabelMapFilterWrapper< TInputImage >::BinaryImageToLabelMapFilterWrapper() : ProcessObjectBase()
{
	this->mName = BinaryImageToLabelMapFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Transforms binary image to a label map and creates the label image";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "FullyConnected", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If turned on, 8/26 neighborhood is used, else 4/6 neighborhood is used for 2D/3D images, respectively." );
	processObjectSettings->AddSetting( "InputForegroundValue", "1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The input foreground value. Usually this should be set to 1." );
	processObjectSettings->AddSetting( "OutputBackgroundValue", "0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The output background value. Usually this should be set to 0." );

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
BinaryImageToLabelMapFilterWrapper< TInputImage >::~BinaryImageToLabelMapFilterWrapper()
{
}


// the update function
template < class TInputImage >
void BinaryImageToLabelMapFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const bool fullyConnected = processObjectSettings->GetSettingValue( "FullyConnected" ).toInt() > 0;
	const float foregroundValue = processObjectSettings->GetSettingValue( "InputForegroundValue" ).toFloat();
	const float backgroundValue = processObjectSettings->GetSettingValue( "OutputBackgroundValue" ).toFloat();
	
	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();
	
	// setup the filter
	typedef itk::BinaryImageToLabelMapFilter< TInputImage > BinaryImageToLabelMapFilterType;
	typename BinaryImageToLabelMapFilterType::Pointer binaryImageToLabelMapFilter = BinaryImageToLabelMapFilterType::New();
	binaryImageToLabelMapFilter->SetInput( inputImage );
	binaryImageToLabelMapFilter->SetReleaseDataFlag( true );
	binaryImageToLabelMapFilter->SetInputForegroundValue( foregroundValue );
	binaryImageToLabelMapFilter->SetOutputBackgroundValue( backgroundValue );
	binaryImageToLabelMapFilter->SetFullyConnected( fullyConnected );
	itkTryCatch( binaryImageToLabelMapFilter->Update(), "Error: BinaryImageToLabelMapFilter update.");
 
	// convert the label mapt into a label image
	typedef itk::Image< unsigned short, TInputImage::ImageDimension >  LabelType;
	typedef itk::LabelMapToLabelImageFilter< typename BinaryImageToLabelMapFilterType::OutputImageType, LabelType > LabelMapToLabelImageFilterType;
	typename LabelMapToLabelImageFilterType::Pointer labelMapToLabelImageFilter = LabelMapToLabelImageFilterType::New();
	labelMapToLabelImageFilter->SetInput( binaryImageToLabelMapFilter->GetOutput() );
	labelMapToLabelImageFilter->SetReleaseDataFlag( true );
	itkTryCatch( labelMapToLabelImageFilter->Update(), "Error: LabelMapToLabelImageFilter update.");
	
	// set the output
	ImageWrapper* outputWrapper = new ImageWrapper();
	outputWrapper->SetImage< LabelType >( labelMapToLabelImageFilter->GetOutput() );
	outputWrapper->SetRescaleFlag( false );
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< BinaryImageToLabelMapFilterWrapper<Image2Float> > BinaryImageToLabelMapFilterWrapperImage2Float;
static ProcessObjectProxy< BinaryImageToLabelMapFilterWrapper<Image3Float> > BinaryImageToLabelMapFilterWrapperImage3Float;
static ProcessObjectProxy< BinaryImageToLabelMapFilterWrapper<Image2UShort> > BinaryImageToLabelMapFilterWrapperImage2UShort;
static ProcessObjectProxy< BinaryImageToLabelMapFilterWrapper<Image3UShort> > BinaryImageToLabelMapFilterWrapperImage3UShort;

} // namespace XPIWIT

