/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// namespace header
#include "LabelStatisticsImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkImageRegionIterator.h"
#include "itkLabelStatisticsImageFilter.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
LabelStatisticsImageFilterWrapper< TInputImage >::LabelStatisticsImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = LabelStatisticsImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Extract statistical information of labeled image. First input grayscale image, second labeled image.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(2);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(0);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(1);
	this->mObjectType->AppendMetaOutputType("StatisticalProperties");

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
LabelStatisticsImageFilterWrapper< TInputImage >::~LabelStatisticsImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void LabelStatisticsImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();

	// get images
	typedef itk::Image< unsigned short, TInputImage::ImageDimension >  LabelType;
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	typename LabelType::Pointer labelImage = mInputImages.at(1)->template GetImage<LabelType>();

	// start processing
	ProcessObjectBase::StartTimer();

	// setup the filter
	typedef itk::LabelStatisticsImageFilter< TInputImage, LabelType > LabelStatisticsImageFilterType;
	typename LabelStatisticsImageFilterType::Pointer labelStatisticsImageFilter = LabelStatisticsImageFilterType::New();
	labelStatisticsImageFilter->SetLabelInput( labelImage );
	labelStatisticsImageFilter->SetInput( inputImage );
	itkTryCatch( labelStatisticsImageFilter->Update() , "Error: LabelStatisticsImageFilterWrapper Update Function.");

	// create meta object
	// Create the meta data Object
	MetaDataFilter* metaOutput = this->mMetaOutputs.at(0);
	metaOutput->mIsMultiDimensional = true;
	metaOutput->mPostfix = "RegionProps";

	QStringList metaDescription;                    QStringList metaType;
	metaDescription << "id";						metaType << "int";
	metaDescription << "min";						metaType << "float";
	metaDescription << "max";						metaType << "float";
	metaDescription << "median";					metaType << "float";
	metaDescription << "mean";						metaType << "float";
	metaDescription << "sigma";						metaType << "float";
	metaDescription << "variance";					metaType << "float";
	metaDescription << "sum";						metaType << "float";
	metaDescription << "count";						metaType << "int";
		
	// write to meta object
	metaOutput->mTitle = metaDescription;
	metaOutput->mType = metaType;

	typedef typename LabelStatisticsImageFilterType::ValidLabelValuesContainerType ValidLabelValuesType;
	typedef typename LabelStatisticsImageFilterType::LabelPixelType                LabelPixelType;
	unsigned int currentKeyPointID = 0;

	for( typename ValidLabelValuesType::const_iterator vIt=labelStatisticsImageFilter->GetValidLabelValues().begin();
		vIt != labelStatisticsImageFilter->GetValidLabelValues().end();
		++vIt )
	{
		++currentKeyPointID;
		if ( labelStatisticsImageFilter->HasLabel(*vIt) )
		{
			LabelPixelType labelValue = *vIt;

			QList<float> currentData;
			currentData << currentKeyPointID;		
			currentData << labelStatisticsImageFilter->GetMinimum( labelValue );
			currentData << labelStatisticsImageFilter->GetMaximum( labelValue );		
			currentData << labelStatisticsImageFilter->GetMedian( labelValue );		
			currentData << labelStatisticsImageFilter->GetMean( labelValue );		
			currentData << labelStatisticsImageFilter->GetSigma( labelValue );		
			currentData << labelStatisticsImageFilter->GetVariance( labelValue );		
			currentData << labelStatisticsImageFilter->GetSum( labelValue );		
			currentData << labelStatisticsImageFilter->GetCount( labelValue );		

			metaOutput->mData.append( currentData );
		}
	}

	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< LabelStatisticsImageFilterWrapper<Image2Float> > LabelStatisticsImageFilterWrapperImage2Float;
static ProcessObjectProxy< LabelStatisticsImageFilterWrapper<Image3Float> > LabelStatisticsImageFilterWrapperImage3Float;
static ProcessObjectProxy< LabelStatisticsImageFilterWrapper<Image2UShort> > LabelStatisticsImageFilterWrapperImage2UShort;
static ProcessObjectProxy< LabelStatisticsImageFilterWrapper<Image3UShort> > LabelStatisticsImageFilterWrapperImage3UShort;

} // namespace XPIWIT

