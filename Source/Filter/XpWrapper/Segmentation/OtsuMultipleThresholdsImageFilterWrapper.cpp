/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// namespace header
#include "OtsuMultipleThresholdsImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkOtsuMultipleThresholdsImageFilter.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
OtsuMultipleThresholdsImageFilterWrapper< TInputImage >::OtsuMultipleThresholdsImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = OtsuMultipleThresholdsImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Threshold an image using multiple Otsu Thresholds.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "HistogramBins", "20", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Number of histogram bins." );
	processObjectSettings->AddSetting( "Thresholds", "2", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Number of thresholds." );
	processObjectSettings->AddSetting( "LabelOffset", "0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Set the label offset." );

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
OtsuMultipleThresholdsImageFilterWrapper< TInputImage >::~OtsuMultipleThresholdsImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void OtsuMultipleThresholdsImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const int numberHistogramBins = processObjectSettings->GetSettingValue( "HistogramBins" ).toInt();
	const int numberThresholds = processObjectSettings->GetSettingValue( "Thresholds" ).toInt();
	const float labelOffset = processObjectSettings->GetSettingValue( "LabelOffset" ).toDouble();
	
	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();
	
	// setup the filter
	typedef itk::OtsuMultipleThresholdsImageFilter<TInputImage, TInputImage> FilterType;
	typename FilterType::Pointer filter = FilterType::New();
	filter->SetInput( inputImage );
	filter->SetReleaseDataFlag( true );

	filter->SetNumberOfHistogramBins( numberHistogramBins );
	filter->SetNumberOfThresholds( numberThresholds );
	filter->SetLabelOffset( labelOffset );

	itkTryCatch(filter->Update(), "Error: OtsuMultipleThresholdsImageFilter Update Function.");
	
	ImageWrapper *outputImage = new ImageWrapper();
	outputImage->SetImage<TInputImage>( filter->GetOutput() );
	mOutputImages.append( outputImage );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< OtsuMultipleThresholdsImageFilterWrapper<Image2Float> > OtsuMultipleThresholdsImageFilterWrapperImage2Float;
static ProcessObjectProxy< OtsuMultipleThresholdsImageFilterWrapper<Image3Float> > OtsuMultipleThresholdsImageFilterWrapperImage3Float;
static ProcessObjectProxy< OtsuMultipleThresholdsImageFilterWrapper<Image2UShort> > OtsuMultipleThresholdsImageFilterWrapperImage2UShort;
static ProcessObjectProxy< OtsuMultipleThresholdsImageFilterWrapper<Image3UShort> > OtsuMultipleThresholdsImageFilterWrapperImage3UShort;

} // namespace XPIWIT

