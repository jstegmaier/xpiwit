/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "BinaryThresholdImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkBinaryThresholdImageFilter.h"


namespace XPIWIT
{

// the default constructor
template< class TInputImage >
BinaryThresholdImageFilterWrapper<TInputImage>::BinaryThresholdImageFilterWrapper() : ProcessObjectBase()
{
    // set the widget type
    this->mName = BinaryThresholdImageFilterWrapper<TInputImage>::GetName();
    this->mDescription = "Creates a binary image ";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "LowerThreshold", "0.03", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Defines the lower threshold" );
    processObjectSettings->AddSetting( "UpperThreshold", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Defines the upper threshold" );
    processObjectSettings->AddSetting( "OutsideValue", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Defines the value outside of the thresholds" );
    processObjectSettings->AddSetting( "InsideValue", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Defines the value within the thresholds" );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
BinaryThresholdImageFilterWrapper<TInputImage>::~BinaryThresholdImageFilterWrapper()
{
}


// the update function
template< class TInputImage >
void BinaryThresholdImageFilterWrapper<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

    // adjust filter settings
    typedef itk::BinaryThresholdImageFilter<TInputImage, TInputImage> BinaryThresholdFilter;
    typename BinaryThresholdFilter::Pointer filter = BinaryThresholdFilter::New();
    filter->SetInput( inputImage );
    filter->SetNumberOfWorkUnits( maxThreads );
    filter->SetLowerThreshold( processObjectSettings->GetSettingValue( "LowerThreshold" ).toDouble() );
    filter->SetUpperThreshold( processObjectSettings->GetSettingValue( "UpperThreshold" ).toDouble() );
    filter->SetOutsideValue( processObjectSettings->GetSettingValue( "OutsideValue" ).toDouble() );
    filter->SetInsideValue( processObjectSettings->GetSettingValue( "InsideValue" ).toDouble() );

    // update the filter
    itkTryCatch( filter->Update(), "Error: Updating BinaryThresholdImageFilter failed." );
    
	ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( filter->GetOutput() );
	//outputImage->SetRescaleFlag( false );
    mOutputImages.append( outputImage );

    // update the process widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< BinaryThresholdImageFilterWrapper<Image2Float> > BinaryThresholdImageFilterWrapperImage2Float;
static ProcessObjectProxy< BinaryThresholdImageFilterWrapper<Image3Float> > BinaryThresholdImageFilterWrapperImage3Float;
static ProcessObjectProxy< BinaryThresholdImageFilterWrapper<Image2UShort> > BinaryThresholdImageFilterWrapperImage2UShort;
static ProcessObjectProxy< BinaryThresholdImageFilterWrapper<Image3UShort> > BinaryThresholdImageFilterWrapperImage3UShort;

} // namespace XPIWIT
