/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "SliceBySliceOtsuThresholdImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "../../ITKCustom/itkSliceBySliceOtsuThresholdImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkNumericTraits.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
SliceBySliceOtsuThresholdImageFilterWrapper< TInputImage >::SliceBySliceOtsuThresholdImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = SliceBySliceOtsuThresholdImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Applies Otsu adaptive threshold to an image separately for each slice.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "NumberOfThresholds", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Sets the number of thresholds to iteratively apply. Defaults to standard Otsu." );
    processObjectSettings->AddSetting( "ValleyEmphasis", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Enables or disables the valley emphasis." );

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
SliceBySliceOtsuThresholdImageFilterWrapper< TInputImage >::~SliceBySliceOtsuThresholdImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void SliceBySliceOtsuThresholdImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const int numberOfThresholds = processObjectSettings->GetSettingValue( "NumberOfThresholds" ).toInt();
	const bool valleyEmphasis = processObjectSettings->GetSettingValue( "ValleyEmphasis" ).toInt() > 0;
	
	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();

	typedef itk::SliceBySliceOtsuThresholdImageFilter<TInputImage, TInputImage> OtsuFilterType;
	typename OtsuFilterType::Pointer otsuFilter = OtsuFilterType::New();
	otsuFilter->SetInput( inputImage );
    otsuFilter->SetNumberOfThresholds( numberOfThresholds );
    otsuFilter->SetValleyEmphasis( valleyEmphasis );
	otsuFilter->SetReleaseDataFlag( true );
    otsuFilter->SetNumberOfWorkUnits( maxThreads );

	itkTryCatch(otsuFilter->Update(), "Error: SliceBySliceAdjustIntensityImageFilterWrapper Update Function.");
	
	ImageWrapper *outputWrapper = new ImageWrapper();
	outputWrapper->SetImage<TInputImage>( otsuFilter->GetOutput() );
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< SliceBySliceOtsuThresholdImageFilterWrapper<Image2Float> > SliceBySliceOtsuThresholdImageFilterWrapperImage2Float;
static ProcessObjectProxy< SliceBySliceOtsuThresholdImageFilterWrapper<Image3Float> > SliceBySliceOtsuThresholdImageFilterWrapperImage3Float;
static ProcessObjectProxy< SliceBySliceOtsuThresholdImageFilterWrapper<Image2UShort> > SliceBySliceOtsuThresholdImageFilterWrapperImage2UShort;
static ProcessObjectProxy< SliceBySliceOtsuThresholdImageFilterWrapper<Image3UShort> > SliceBySliceOtsuThresholdImageFilterWrapperImage3UShort;


} // namespace XPIWIT

