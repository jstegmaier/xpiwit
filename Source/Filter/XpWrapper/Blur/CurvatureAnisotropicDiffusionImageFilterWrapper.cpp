/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "CurvatureAnisotropicDiffusionImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkCurvatureAnisotropicDiffusionImageFilter.h"


namespace XPIWIT
{

template < class TInputImage >
CurvatureAnisotropicDiffusionImageFilterWrapper< TInputImage >::CurvatureAnisotropicDiffusionImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = CurvatureAnisotropicDiffusionImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "This filter performs anisotropic diffusion on a scalar image using the modified curvature diffusion equation (MCDE).";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "TimeStep", "0.0625", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Sets the time step to be used for each iteration. Should be <= 0.5/(2^N)), i.e. smaller than 0.125 for 2D and smaller than 0.0625 for 3D." );
	processObjectSettings->AddSetting( "NumIterations", "5", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The number of iterations. Generally, the more iterations, the more diffused the output." );
	processObjectSettings->AddSetting( "ConductanceParameter", "3", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Sensitivity of the conductance term. Lower values preserve image features stronger. Typical values 0.5-2.0." );

	// initialize the widget
	ProcessObjectBase::Init();
}


// the default destructor
template < class TInputImage >
CurvatureAnisotropicDiffusionImageFilterWrapper< TInputImage >::~CurvatureAnisotropicDiffusionImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void CurvatureAnisotropicDiffusionImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const int numIterations = processObjectSettings->GetSettingValue( "NumIterations" ).toInt();
	const float timeStep = processObjectSettings->GetSettingValue( "TimeStep" ).toFloat();
	const float conductance = processObjectSettings->GetSettingValue( "ConductanceParameter" ).toFloat();
	
	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
		
	// start processing
	ProcessObjectBase::StartTimer();
	
	// setup the filter
	typedef itk::CurvatureAnisotropicDiffusionImageFilter<TInputImage, TInputImage> FilterType;
	typename FilterType::Pointer filter = FilterType::New();
	filter->SetInput( inputImage );
	filter->SetNumberOfIterations( numIterations );
	filter->SetTimeStep( timeStep );
	filter->SetConductanceParameter( conductance );
	filter->SetNumberOfWorkUnits( maxThreads );
	filter->SetReleaseDataFlag( true );
	
	itkTryCatch(filter->Update(), "Error: CurvatureAnisotropicDiffusionImageFilterWrapper Update Function.");
	
	ImageWrapper *outputWrapper = new ImageWrapper();
	outputWrapper->SetImage<TInputImage>( filter->GetOutput() );
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< CurvatureAnisotropicDiffusionImageFilterWrapper<Image2Float> > CurvatureAnisotropicDiffusionImageFilterWrapperImage2Float;
static ProcessObjectProxy< CurvatureAnisotropicDiffusionImageFilterWrapper<Image3Float> > CurvatureAnisotropicDiffusionImageFilterWrapperImage3Float;
//static ProcessObjectProxy< CurvatureAnisotropicDiffusionImageFilterWrapper<Image2UShort> > CurvatureAnisotropicDiffusionImageFilterWrapperImage2UShort;
//static ProcessObjectProxy< CurvatureAnisotropicDiffusionImageFilterWrapper<Image3UShort> > CurvatureAnisotropicDiffusionImageFilterWrapperImage3UShort;

} // namespace XPIWIT

