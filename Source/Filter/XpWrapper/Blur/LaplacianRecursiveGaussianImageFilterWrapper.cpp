/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "LaplacianRecursiveGaussianImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"

// itk header
#include "itkLaplacianRecursiveGaussianImageFilter.h"


namespace XPIWIT
{

// the default constructor
template <class TInputImage>
LaplacianRecursiveGaussianImageFilterWrapper<TInputImage>::LaplacianRecursiveGaussianImageFilterWrapper() : ProcessObjectBase()
{
    // set the widget type
    this->mName = LaplacianRecursiveGaussianImageFilterWrapper<TInputImage>::GetName();
    this->mDescription = "Laplacian of Gaussian Filter. ";
    this->mDescription += "The input image will be processed with the derived gaussian kernel.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Sigma", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Sigam value of the gaussian kernel." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template <class TInputImage>
LaplacianRecursiveGaussianImageFilterWrapper<TInputImage>::~LaplacianRecursiveGaussianImageFilterWrapper()
{
}


// the update function
template <class TInputImage>
void LaplacianRecursiveGaussianImageFilterWrapper<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

    // create the LoG filter and adjust settings
    typedef itk::LaplacianRecursiveGaussianImageFilter<TInputImage, TInputImage> LoGFilter;
    typename LoGFilter::Pointer filter = LoGFilter::New();
    filter->SetInput( inputImage );
    filter->SetNumberOfWorkUnits( maxThreads );
    filter->SetSigma( processObjectSettings->GetSettingValue( "Sigma" ).toDouble() );
    filter->SetReleaseDataFlag( processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0 );

    // update the pipeline by calling update for the most downstream filter
    itkTryCatch( filter->Update(), "Error: Updating LaplacianRecursiveGaussianImageFilter failed!" );
    ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( filter->GetOutput() );
    mOutputImages.append( outputImage );

    // update the parent
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< LaplacianRecursiveGaussianImageFilterWrapper<Image2Float> > LaplacianRecursiveGaussianImageFilterWrapperImage2Float;
static ProcessObjectProxy< LaplacianRecursiveGaussianImageFilterWrapper<Image3Float> > LaplacianRecursiveGaussianImageFilterWrapperImage3Float;
static ProcessObjectProxy< LaplacianRecursiveGaussianImageFilterWrapper<Image2UShort> > LaplacianRecursiveGaussianImageFilterWrapperImage2UShort;
static ProcessObjectProxy< LaplacianRecursiveGaussianImageFilterWrapper<Image3UShort> > LaplacianRecursiveGaussianImageFilterWrapperImage3UShort;


} // namespace XPIWIT
