/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

#ifdef XPIWIT_USE_GPU_OPENCL

// project header
#include "GPUDiscreteGaussianImageFilterWrapper.h"
#include "itkGPUDiscreteGaussianImageFilter.h"
#include "itkCastImageFilter.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// qt header

// system header

namespace XPIWIT {

// the default constructor
template< class TInputImage >
GPUDiscreteGaussianImageFilterWrapper<TInputImage>::GPUDiscreteGaussianImageFilterWrapper() : ProcessObjectBase()
{
    // set the widget type
    this->mName = GPUDiscreteGaussianImageFilterWrapper<TInputImage>::GetName();
    this->mDescription = "Discrete Gaussian Filter. ";
    this->mDescription += "Filters the input with a Gaussian kernel.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings to the filter
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Variance", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Variance of the filter kernel." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
GPUDiscreteGaussianImageFilterWrapper<TInputImage>::~GPUDiscreteGaussianImageFilterWrapper()
{
}


// update the filter
template< class TInputImage >
void GPUDiscreteGaussianImageFilterWrapper<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int variance = processObjectSettings->GetSettingValue( "Variance" ).toInt();
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;
    
    std::cout << "Alrighty, I'm using " << itk::MultiThreader::GetGlobalDefaultNumberOfThreads() << " threads." << std::endl;

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

	///////////////////// PLACE YOUR CODE HERE /////////////////////////////

    typedef itk::GPUImage<typename TInputImage::PixelType, TInputImage::ImageDimension> GPUImageType;
    typedef itk::CastImageFilter<TInputImage, GPUImageType> GPUCastImageFilterType;
    typename GPUCastImageFilterType::Pointer castImageFilter = GPUCastImageFilterType::New();
    castImageFilter->SetInput( inputImage );
    
    typedef itk::GPUDiscreteGaussianImageFilter<GPUImageType, GPUImageType> GPUDiscreteGaussianImageFilterType;
    typename GPUDiscreteGaussianImageFilterType::Pointer gaussianImageFilter = GPUDiscreteGaussianImageFilterType::New();
    gaussianImageFilter->SetInput( castImageFilter->GetOutput() );
    gaussianImageFilter->SetVariance( variance );
    itkTryCatch( gaussianImageFilter->Update(), "ERROR: Failed to execute update function of the mean image filter!" );
    gaussianImageFilter->GetOutput()->UpdateBuffers();
    
	///////////////////// PLACE YOUR CODE HERE /////////////////////////////
	
    // update the filter
    ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( gaussianImageFilter->GetOutput() );
    mOutputImages.append( outputImage );

    // update the process update widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< GPUDiscreteGaussianImageFilterWrapper<Image2Float> > GPUDiscreteGaussianImageFilterWrapperImage2Float;
static ProcessObjectProxy< GPUDiscreteGaussianImageFilterWrapper<Image3Float> > GPUDiscreteGaussianImageFilterWrapperImage3Float;
static ProcessObjectProxy< GPUDiscreteGaussianImageFilterWrapper<Image2UShort> > GPUDiscreteGaussianImageFilterWrapperImage2UShort;
static ProcessObjectProxy< GPUDiscreteGaussianImageFilterWrapper<Image3UShort> > GPUDiscreteGaussianImageFilterWrapperImage3UShort;

} // namespace XPIWIT

#endif