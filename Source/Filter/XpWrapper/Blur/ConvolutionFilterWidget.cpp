/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "ConvolutionFilterWidget.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkFFTConvolutionImageFilter.h"

// system header
#include <float.h>

// XPIWIT namespace
namespace XPIWIT 
{

// the default constructor
template< class TInputImage >
ConvolutionFilterWidget<TInputImage>::ConvolutionFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = ConvolutionFilterWidget<TInputImage>::GetName();
    this->mDescription = "Preforms a 3D convolution with an arbitrary convolution kernel (has to be 3D. Physical spacing is assumed to be equal to input image).";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(2);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings to the filter
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Normalize", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, the kernel is normalized to have a sum of 1." );
	
    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
ConvolutionFilterWidget<TInputImage>::~ConvolutionFilterWidget()
{
}


// update the filter
template< class TInputImage >
void ConvolutionFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const bool normalizeKernel = processObjectSettings->GetSettingValue( "Normalize" ).toInt() > 0;
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	
	// get images
    typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	typename TInputImage::Pointer kernelImage = mInputImages.at(1)->template GetImage<TInputImage>();

	// setup the convolution filter
	typedef itk::FFTConvolutionImageFilter<TInputImage> ConvolutionImageFilterType;
	typename ConvolutionImageFilterType::Pointer convolutionFilter = ConvolutionImageFilterType::New();
	convolutionFilter->SetInput( inputImage );
	convolutionFilter->SetKernelImage( kernelImage );
	convolutionFilter->SetNumberOfWorkUnits( maxThreads );
	if (normalizeKernel == true)
		convolutionFilter->NormalizeOn();

	// update the convolution filter
	itkTryCatch( convolutionFilter->Update(), "Exception Caught: Updating the convolution filter." );

    // set the output image
    ImageWrapper *outputWrapper = new ImageWrapper();
    outputWrapper->SetImage<TInputImage>( convolutionFilter->GetOutput() );
    mOutputImages.append( outputWrapper );

    // update the process update widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< ConvolutionFilterWidget<Image2Float> > ConvolutionFilterWidgetImage2Float;
static ProcessObjectProxy< ConvolutionFilterWidget<Image3Float> > ConvolutionFilterWidgetImage3Float;
static ProcessObjectProxy< ConvolutionFilterWidget<Image2UShort> > ConvolutionFilterWidgetImage2UShort;
static ProcessObjectProxy< ConvolutionFilterWidget<Image3UShort> > ConvolutionFilterWidgetImage3UShort;

} // namespace XPIWIT
