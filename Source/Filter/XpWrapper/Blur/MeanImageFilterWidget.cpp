/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. Hbner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. Hbner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "MeanImageFilterWidget.h"
#include "../../../Core/Utilities/Logger.h"
#include "../../../Filter/Base/Management/ImageWrapper.h"

// itk header
#include "itkMeanImageFilter.h"

// XPIWIT namespace
namespace XPIWIT 
{

// the default constructor
template <class TInputImage>
MeanImageFilterWidget<TInputImage>::MeanImageFilterWidget() : ProcessObjectBase()
{
    // set the widget type
	this->mName = MeanImageFilterWidget<TInputImage>::GetName();
    this->mDescription = "Box filter using the mean value of a region specified by the provided radius.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);

    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);

    this->mObjectType->SetNumberMetaInputs(0);

    this->mObjectType->SetNumberMetaOutputs(0);

    
    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "Radius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The radius of the region to use for mean filtering." );


    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template <class TInputImage>
MeanImageFilterWidget<TInputImage>::~MeanImageFilterWidget()
{
}


// the update function
template <class TInputImage>
void MeanImageFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    const bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;
	const int Radius = processObjectSettings->GetSettingValue( "Radius" ).toInt();

    
    // get the input images
	typename TInputImage::Pointer inputImage0 = mInputImages.at(0)->template GetImage<TInputImage>();

    typedef itk::MeanImageFilter<TInputImage, TInputImage> MeanFilterType;
    typename MeanFilterType::Pointer meanFilter = MeanFilterType::New();
    meanFilter->SetRadius( Radius );
    meanFilter->SetInput( inputImage0 );
    
    // update the pipeline by calling update for the most downstream filter
    // TODO: add a call to the update function of your filter (e.g. myfilter->Update() ).
    itkTryCatch( meanFilter->Update(), "Failed to update myfilter" );

	// save output
	ImageWrapper *outputImage1 = new ImageWrapper();
outputImage1->SetImage<TInputImage>( meanFilter->GetOutput() ); // <- TODO: place your filter output here. (e.g. myfilter->GetOutput() )
mOutputImages.append( outputImage1 );


    // update the base class update function
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
// Note: If your filter only supports one or the other data type, remove the respective lines
static ProcessObjectProxy< MeanImageFilterWidget<Image2Float> > MeanImageFilterWidgetImage2Float;
static ProcessObjectProxy< MeanImageFilterWidget<Image3Float> > MeanImageFilterWidgetImage3Float;
static ProcessObjectProxy< MeanImageFilterWidget<Image2UShort> > MeanImageFilterWidgetImage2UShort;
static ProcessObjectProxy< MeanImageFilterWidget<Image3UShort> > MeanImageFilterWidgetImage3UShort;

} // namespace XPIWIT
