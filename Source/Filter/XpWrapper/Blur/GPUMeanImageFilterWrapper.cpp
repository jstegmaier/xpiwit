/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */


// project header
#ifdef XPIWIT_USE_GPU_DEV
#include "GPUMeanImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"
#include "../CUDADev/GPUMeanImageFilter.h"

// qt header

// system header

namespace XPIWIT {

// the default constructor
template< class TInputImage >
GPUMeanImageFilterWrapper<TInputImage>::GPUMeanImageFilterWrapper() : ProcessObjectBase()
{
    // set the widget type
    this->mName = GPUMeanImageFilterWrapper<TInputImage>::GetName();
    this->mDescription = "Mean Filter. ";
    this->mDescription += "Filters the input with a mean kernel.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings to the filter
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Radius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Radius of the filter kernel (manhattan distance)." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
GPUMeanImageFilterWrapper<TInputImage>::~GPUMeanImageFilterWrapper()
{
}

// update the filter
template< class TInputImage >
void GPUMeanImageFilterWrapper<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int radius = processObjectSettings->GetSettingValue( "Radius" ).toInt();
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;
    
    std::cout << "Alrighty, I'm using " << itk::MultiThreader::GetGlobalDefaultNumberOfThreads() << " threads." << std::endl;

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
	//read image dimensions
	TInputImage::RegionType region = inputImage->GetLargestPossibleRegion();
	TInputImage::SizeType imSize = region.GetSize();

	int dims[3];

	for (int ii = 0; ii < TInputImage::ImageDimension; ii++)
		dims[ii] = imSize[ii];
	if (TInputImage::ImageDimension < 3)
		dims[2] = 1;

	//get pointer to image data within ITK class
	TInputImage::PixelType* img = inputImage->GetBufferPointer();


	GPUMeanImageFilter<TInputImage::PixelType> meanFilter;
	meanFilter.Process(img, dims, radius);

	///////////////////// PLACE YOUR CODE HERE /////////////////////////////

	/* // OPENCL CODE!
    typedef itk::GPUImage<typename TInputImage::PixelType, TInputImage::ImageDimension> GPUImageType;
    typedef itk::CastImageFilter<TInputImage, GPUImageType> GPUCastImageFilterType;
    typename GPUCastImageFilterType::Pointer castImageFilter = GPUCastImageFilterType::New();
    castImageFilter->SetInput( inputImage );
    
    typedef itk::GPUMeanImageFilter<GPUImageType, GPUImageType> GPUMeanImageFilterType;
    typename GPUMeanImageFilterType::Pointer meanImageFilter = GPUMeanImageFilterType::New();
    meanImageFilter->SetInput( castImageFilter->GetOutput() );
    meanImageFilter->SetRadius( radius );
    itkTryCatch( meanImageFilter->Update(), "ERROR: Failed to execute update function of the mean image filter!" );
    meanImageFilter->GetOutput()->UpdateBuffers();
    */
	///////////////////// PLACE YOUR CODE HERE /////////////////////////////
	
    // update the filter
    ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( inputImage );
    mOutputImages.append( outputImage );

    // update the process update widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< GPUMeanImageFilterWrapper<Image2Float> > GPUMeanImageFilterWrapperImage2Float;
static ProcessObjectProxy< GPUMeanImageFilterWrapper<Image3Float> > GPUMeanImageFilterWrapperImage3Float;
static ProcessObjectProxy< GPUMeanImageFilterWrapper<Image2UShort> > GPUMeanImageFilterWrapperImage2UShort;
static ProcessObjectProxy< GPUMeanImageFilterWrapper<Image3UShort> > GPUMeanImageFilterWrapperImage3UShort;

} // namespace XPIWIT

#endif