/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "BinaryContourImageFilterWrapper.h"
#include "../../../Filter/Base/Management/ImageWrapper.h"
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkBinaryContourImageFilter.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
BinaryContourImageFilterWrapper< TInputImage >::BinaryContourImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = BinaryContourImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Labels the pixels on the border of the objects in a binary image. ";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

	// initialize the widget
	ProcessObjectBase::Init();
}


// the default destructor
template < class TInputImage >
BinaryContourImageFilterWrapper< TInputImage >::~BinaryContourImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void BinaryContourImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	
	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
	// start processing
	ProcessObjectBase::StartTimer();
	
	// setup the filter
	typedef itk::BinaryContourImageFilter <TInputImage, TInputImage> FilterType;
	typename FilterType::Pointer filter = FilterType::New();
	filter->SetInput( inputImage );
	filter->SetForegroundValue(1);
	filter->SetBackgroundValue(0);
	filter->SetReleaseDataFlag( true );
	
	itkTryCatch(filter->Update(), "Error: BinaryContourImageFilterWrapper Update Function.");
	
	ImageWrapper *outputWrapper = new ImageWrapper();
	outputWrapper->SetImage<TInputImage>( filter->GetOutput() );
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< BinaryContourImageFilterWrapper<Image2Float> > BinaryContourImageFilterWrapperImage2Float;
static ProcessObjectProxy< BinaryContourImageFilterWrapper<Image3Float> > BinaryContourImageFilterWrapperImage3Float;
static ProcessObjectProxy< BinaryContourImageFilterWrapper<Image2UShort> > BinaryContourImageFilterWrapperImage2UShort;
static ProcessObjectProxy< BinaryContourImageFilterWrapper<Image3UShort> > BinaryContourImageFilterWrapperImage3UShort;

} // namespace XPIWIT

