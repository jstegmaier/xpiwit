/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// namespace header
#include "JunctionDetectionFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

namespace XPIWIT
{
	
// the default constructor
template < class TInputImage >
JunctionDetectionFilterWrapper< TInputImage >::JunctionDetectionFilterWrapper() : ProcessObjectBase()
{
	this->mName = JunctionDetectionFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Uses the connected components of a hollow sphere to identify the junctions of a binary image.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "InnerRadius", "2.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The radius of the inner bounding sphere." );
	processObjectSettings->AddSetting( "OuterRadius", "3.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The radius of the outer bounding sphere." );
	processObjectSettings->AddSetting( "MinNumberOfPixel", "16.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The minimum number of pixels to be considered as a connected component." );
	
	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
JunctionDetectionFilterWrapper< TInputImage >::~JunctionDetectionFilterWrapper()
{
}


// the update function
template < class TInputImage >
void JunctionDetectionFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const float innerRadius = processObjectSettings->GetSettingValue( "InnerRadius" ).toFloat();
	const float outerRadius = processObjectSettings->GetSettingValue( "OuterRadius" ).toFloat();
	const float minNumberOfPixel = processObjectSettings->GetSettingValue( "MinNumberOfPixel" ).toFloat();

	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
		
	// start processing
	ProcessObjectBase::StartTimer();
	
	/*
	// setup the filter
	typedef itk::JunctionDetectionFilter<TInputImage> JunctionDetectionFilterType;
	typename JunctionDetectionFilterType::Pointer junctionDetectionFilter = JunctionDetectionFilterType::New();
	junctionDetectionFilter->SetInput( inputImage );
	junctionDetectionFilter->SetNumberOfWorkUnits( maxThreads );
	junctionDetectionFilter->SetReleaseDataFlag( true );
	junctionDetectionFilter->SetInnerRadius( innerRadius );
	junctionDetectionFilter->SetOuterRadius( outerRadius );
	junctionDetectionFilter->SetMinNumberOfPixel( minNumberOfPixel );
	itkTryCatch(junctionDetectionFilter->Update(), "Error: JunctionDetectionFilterWrapper Update Function.");
	*/
	
	ImageWrapper *outputImage = new ImageWrapper();
	outputImage->SetImage<TInputImage>( inputImage );
	mOutputImages.append( outputImage );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}
    
// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< JunctionDetectionFilterWrapper<Image2Float> > JunctionDetectionFilterWrapperImage2Float;
static ProcessObjectProxy< JunctionDetectionFilterWrapper<Image3Float> > JunctionDetectionFilterWrapperImage3Float;
static ProcessObjectProxy< JunctionDetectionFilterWrapper<Image2UShort> > JunctionDetectionFilterWrapperImage2UShort;
static ProcessObjectProxy< JunctionDetectionFilterWrapper<Image3UShort> > JunctionDetectionFilterWrapperImage3UShort;

} // namespace XPIWIT

