/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// namespace header
#include "CannyEdgeDetectionImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkCannyEdgeDetectionImageFilter.h"


namespace XPIWIT
{

// the defacult constructor	
template < class TInputImage >
CannyEdgeDetectionImageFilterWrapper< TInputImage >::CannyEdgeDetectionImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = CannyEdgeDetectionImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Canny edge detection.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "Variance", "1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Variance of the gaussian kernel." );
	processObjectSettings->AddSetting( "MaximumError", "0.01", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The maximum error of the gaussian kernel." );
	processObjectSettings->AddSetting( "LowerThreshold", "0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Upper threshold." );
	processObjectSettings->AddSetting( "UpperThreshold", "0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Upper threshold." );

	// initialize the widget
	ProcessObjectBase::Init();
}


// the default constructor
template < class TInputImage >
CannyEdgeDetectionImageFilterWrapper< TInputImage >::~CannyEdgeDetectionImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void CannyEdgeDetectionImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const float variance = processObjectSettings->GetSettingValue( "Variance" ).toDouble();
	const float maximumError = processObjectSettings->GetSettingValue( "MaximumError" ).toDouble();
	const float lowerThreshold = processObjectSettings->GetSettingValue( "LowerThreshold" ).toDouble();
	const float upperThreshold = processObjectSettings->GetSettingValue( "UpperThreshold" ).toDouble();

	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
	
	// start processing
	ProcessObjectBase::StartTimer();
	
	// setup the filter
	typedef itk::CannyEdgeDetectionImageFilter<TInputImage, TInputImage> FilterType;
	typename FilterType::Pointer filter = FilterType::New();
	filter->SetInput( inputImage );
	filter->SetVariance( variance );
	filter->SetMaximumError( maximumError );
	filter->SetLowerThreshold( lowerThreshold );
	filter->SetUpperThreshold( upperThreshold );
	itkTryCatch(filter->Update(), "Error: CannyEdgeDetectionImageFilterWrapper Update Function.");
	
	ImageWrapper *outputImage = new ImageWrapper();
	outputImage->SetImage<TInputImage>( filter->GetOutput() );
	mOutputImages.append( outputImage );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

// explicit template instantiations to register the filter in the factory
static ProcessObjectProxy< CannyEdgeDetectionImageFilterWrapper<Image2Float> > CannyEdgeDetectionImageFilterWrapperImage2Float;
static ProcessObjectProxy< CannyEdgeDetectionImageFilterWrapper<Image3Float> > CannyEdgeDetectionImageFilterWrapperImage3Float;
//static ProcessObjectProxy< CannyEdgeDetectionImageFilterWrapper<Image2UShort> > CannyEdgeDetectionImageFilterWrapperImage2UShort;
//static ProcessObjectProxy< CannyEdgeDetectionImageFilterWrapper<Image3UShort> > CannyEdgeDetectionImageFilterWrapperImage3UShort;

} // namespace XPIWIT

