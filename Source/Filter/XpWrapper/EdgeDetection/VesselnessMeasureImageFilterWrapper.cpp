/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// namespace header
#include "VesselnessMeasureImageFilterWrapper.h"
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkHessian3DToVesselnessMeasureImageFilter.h"
#include "itkHessianRecursiveGaussianImageFilter.h"


namespace XPIWIT
{

// the default constructor
template < class TInputImage >
VesselnessMeasureImageFilterWrapper< TInputImage >::VesselnessMeasureImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = VesselnessMeasureImageFilterWrapper<TInputImage>::GetName();
	this->mDescription = "Uses the hessian eigenvalues to enhance vessel like structures in the image.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "Sigma", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Variance used by the Hessian calculation." );
	processObjectSettings->AddSetting( "Alpha1", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Alpha1 parameter for the vesselness filter." );
	processObjectSettings->AddSetting( "Alpha2", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Alpha2 parameter for the vesselness filter." );
	
	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TInputImage >
VesselnessMeasureImageFilterWrapper< TInputImage >::~VesselnessMeasureImageFilterWrapper()
{
}


// the update function
template < class TInputImage >
void VesselnessMeasureImageFilterWrapper< TInputImage >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const float sigma = processObjectSettings->GetSettingValue( "Sigma" ).toInt();
	const float alpha1 = processObjectSettings->GetSettingValue( "Alpha1" ).toInt();
	const float alpha2 = processObjectSettings->GetSettingValue( "Alpha2" ).toInt();
	
	// get images
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
		
	// start processing
	ProcessObjectBase::StartTimer();
	
	// setup the filter
	typedef itk::HessianRecursiveGaussianImageFilter<TInputImage> HessianFilterType;
	typedef itk::Hessian3DToVesselnessMeasureImageFilter<float> VesselnessMeasureFilterType;

	typename HessianFilterType::Pointer hessianFilter = HessianFilterType::New();
	hessianFilter->SetInput( inputImage );
	hessianFilter->SetReleaseDataFlag( true );
	hessianFilter->SetSigma( sigma );
	
	typename VesselnessMeasureFilterType::Pointer vesselnessFilter = VesselnessMeasureFilterType::New();
	vesselnessFilter->SetInput( hessianFilter->GetOutput() );
	vesselnessFilter->SetAlpha1( alpha1 );
	vesselnessFilter->SetAlpha2( alpha2 );
	itkTryCatch(vesselnessFilter->Update(), "Error: VesselnessMeasureImageFilterWrapper Update Function.");
	
	ImageWrapper *outputImage = new ImageWrapper();
	outputImage->SetImage<TInputImage>( vesselnessFilter->GetOutput() );
	mOutputImages.append( outputImage );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}
    
// explicit template instantiations to register the filter in the factory
//static ProcessObjectProxy< VesselnessMeasureImageFilterWrapper<Image2Float> > VesselnessMeasureImageFilterWrapperImage2Float;
static ProcessObjectProxy< VesselnessMeasureImageFilterWrapper<Image3Float> > VesselnessMeasureImageFilterWrapperImage3Float;
//static ProcessObjectProxy< VesselnessMeasureImageFilterWrapper<Image2UShort> > VesselnessMeasureImageFilterWrapperImage2UShort;
//static ProcessObjectProxy< VesselnessMeasureImageFilterWrapper<Image3UShort> > VesselnessMeasureImageFilterWrapperImage3UShort;

} // namespace XPIWIT

