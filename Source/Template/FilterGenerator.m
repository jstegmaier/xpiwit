%%%%%%%% PLEASE ADJUST THE PARAMETERS TO FIT YOUR NEW FILTER REQUIREMENTS
%%%%%%%% AND EXECUTE THE MATLAB SCRIPT. THE FILTER TEMPLATE CONTAINING YOUR
%%%%%%%% SPECIFICATIONS WILL BE PLACED DIRECTLY NEXT TO THIS FILE. ADAPT 
%%%%%%%% THE REMAINING PARTS OF THE SOURCE THAT ARE MARKED WITH ''TODO'' 
%%%%%%%% AND SUBSEQUENTLY COPY BOTH THE HEADER AND THE SOURCE FILE TO ONE 
%%%%%%%% OF THE FOLDERS XpExtended or XpWrapper AND RECOMPILE XPIWIT. 
%%%%%%%% AFTER RECOMPILATION, THE NEW FILTER SHOULD BE AVAILABLE IN THE
%%%%%%%% NEW EXECUTABLE. TO UPDATE THE FILTERLIST OF THE GUI, SIMPLY DELETE 
%%%%%%%% THE myfilters.xml IN THE XPIWITGUI EXECUTABLE PATH AND RE-RUN 
%%%%%%%% THE GUI AGAIN.

%% specify the name of the filter. This name is used as a file name and as class name.
filterName = 'GradientVectorFlowTrackingImageFilter';

%% briefly describe your filter. This information will serve as tooltip and description in the XML
filterDescription = 'Two or three channel filter that traces the gradient flow.';

%% specify the number of image and meta input and outputs
numImageInputs = 4;
numMetaInputs = 0;
numImageOutputs = 1;
numMetaOutputs = 0;

%% specify the identifiers for meta input. The list needs to contain as many entries as there are inputs.
metaInputTypes = {'', ''};

%% specify the identifiers for meta output. The list needs to contain as many entries as there are outputs.
metaOutputTypes = { '' };

%% arbitrary names of the parameters that are later on specified in the XML
parameterNames = {'NumIterations', 'IgnoreBackground'}; 

%% parameter types (possible values are SETTINGVALUETYPE_DOUBLE, SETTINGVALUETYPE_INT, SETTINGVALUETYPE_BOOLEAN, SETTINGVALUETYPE_STRING)
parameterTypes = {'SETTINGVALUETYPE_INT', 'SETTINGVALUETYPE_BOOLEAN'};

%% parameter default values (used if not specified in the XML)
parameterDefaultValues = {'40', '1'};

%% the description of each parameter (appears in the XML and the GUI to guide the user)
parameterDescriptions = {'The number of iterations (should be slightly larger than the radius of the biggest object).', 'If enabled, background pixels are ignored.'};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% DO NOT CHANGE ANYTHING BELOW HERE %%%%%%%%%%

%% generate the parameter definitions
parameterDefinition = '';
for i=1:max(size(parameterNames))
    parameterDefinition = sprintf('%s\tprocessObjectSettings->AddSetting( "%s", "%s", ProcessObjectSetting::%s, "%s" );\n', parameterDefinition, parameterNames{i}, parameterDefaultValues{i}, parameterTypes{i}, parameterDescriptions{i} );
end

%% generate the parameter retrieval
parameterRetrieval = '';
for i=1:max(size(parameterNames))
    
    if (i>1)
        insertTab = '\t';
    else
        insertTab = '';
    end
    
    if (strcmp(parameterTypes{i}, 'SETTINGVALUETYPE_BOOL'))
        parameterRetrieval = sprintf('%s\tconst bool %s = processObjectSettings->GetSettingValue( "%s" ).toInt() > 0;\n', parameterRetrieval, parameterNames{i}, parameterNames{i} );
    elseif (strcmp(parameterTypes{i}, 'SETTINGVALUETYPE_INT'))
        parameterRetrieval = sprintf('%s\tconst int %s = processObjectSettings->GetSettingValue( "%s" ).toInt();\n', parameterRetrieval, parameterNames{i}, parameterNames{i} );
    elseif (strcmp(parameterTypes{i}, 'SETTINGVALUETYPE_DOUBLE'))
        parameterRetrieval = sprintf('%s\tconst float %s = processObjectSettings->GetSettingValue( "%s" ).toFloat();\n', parameterRetrieval, parameterNames{i}, parameterNames{i} );
    elseif (strcmp(parameterTypes{i}, 'SETTINGVALUETYPE_STRING'))
        parameterRetrieval = sprintf('%s\tconst QString %s = processObjectSettings->GetSettingValue( "%s" );\n', parameterRetrieval, parameterNames{i}, parameterNames{i} );
    else
       disp('Invalid parameter type! Please correct the parameter type specifier.'); 
    end
end

inputImageDefinition = sprintf('this->mObjectType->SetNumberImageInputs(%i);\n', numImageInputs);
for i=1:numImageInputs
    inputImageDefinition = sprintf('%s\tthis->mObjectType->AppendImageInputType(1);\n', inputImageDefinition);
end

outputImageDefinition = sprintf('this->mObjectType->SetNumberImageOutputs(%i);\n', numImageOutputs);
for i=1:numImageOutputs
    outputImageDefinition = sprintf('%s\tthis->mObjectType->AppendImageOutputType(1);\n', outputImageDefinition);
end

metaInputDefinition = sprintf('this->mObjectType->SetNumberMetaInputs(%i);\n', numMetaOutputs);
for i=1:numMetaInputs
    metaInputDefinition = sprintf('%s\tthis->mObjectType->AppendMetaInputType("%s");\n', metaInputDefinition, metaInputTypes{i});
end

metaOutputDefinition = sprintf('this->mObjectType->SetNumberMetaOutputs(%i);\n', numMetaOutputs);
for i=1:numMetaInputs
    metaOutputDefinition = sprintf('%s\tthis->mObjectType->AppendMetaOutputType("%s");\n', metaOutputDefinition, metaInputTypes{i});
end

getInputImages = '';
for i=1:numImageInputs
    getInputImages = sprintf('%s\ttypename TInputImage::Pointer inputImage%i = mInputImages.at(%i)->template GetImage<TInputImage>();\n', getInputImages, i-1, i-1);
end

getInputMeta = '';
for i=1:numMetaInputs
    getInputMeta = sprintf('%s\tMetaDataFilter* inputMeta%i = this->mMetaInputs.at(%i);\n', getInputMeta, i-1, i-1);
end

outputImages = '';
for i=1:numImageOutputs
    outputImages = sprintf('%s\tImageWrapper *outputImage%i = new ImageWrapper();\noutputImage%i->SetImage<TInputImage>( ... ); // <- TODO: place your filter output here. (e.g. myfilter->GetOutput() )\nmOutputImages.append( outputImage%i );\n', outputImages, i, i, i);
end
    
%% replace the keys 
inputImagesKey = '%INPUTIMAGEDEFINITION%';
outputImagesKey = '%OUTPUTIMAGEDEFINITION%';
inputMetaKey = '%INPUTMETADEFINITION%';
outputMetaKey = '%OUTPUTMETADEFINITION%';
getInputImagesKey = '%GETINPUTIMAGES%';
getInputMetaKey = '%GETINPUTMETA%';
filterNameKey = '%FILTERNAME%';
capitalFilterNameKey = '%CAPITALFILTERNAME%';
descriptionKey = '%DESCRIPTION%';
defineXMLParametersKey = '%DEFINEXMLPARAMETERS%';
retrieveXMLParametersKey = '%RETRIEVEXMLPARAMETERS%';

%% open the file handles
headerTemplate = fopen('Source/FilterTemplate.h', 'rb');
sourceTemplate = fopen('Source/FilterTemplate.cpp', 'rb');
newFilterHeader = fopen(sprintf('%sWidget.h', filterName), 'wb');
newFilterSource = fopen(sprintf('%sWidget.cpp', filterName), 'wb');

%% replace the keys in the header
currentLine = fgetl(headerTemplate);
while (ischar(currentLine))
    
    currentLine = strrep(currentLine, '%CAPITALFILTERNAME%', [upper(filterName) '_H']);
    currentLine = strrep(currentLine, '%FILTERNAME%', filterName);
    currentLine = strrep(currentLine, '%DESCRIPTION%', filterDescription);
 
    %% write the current line
    fprintf(newFilterHeader, '%s\n', currentLine);
    
    currentLine = fgetl(headerTemplate);
end

%% replace the keys in the source
%% replace the keys in the header
currentLine = fgetl(sourceTemplate);
while (ischar(currentLine))
    
    currentLine = strrep(currentLine, '%INPUTIMAGEDEFINITION%', inputImageDefinition);
    currentLine = strrep(currentLine, '%FILTERNAME%', filterName);
    currentLine = strrep(currentLine, '%DESCRIPTION%', filterDescription);
    currentLine = strrep(currentLine, '%OUTPUTIMAGEDEFINITION%', outputImageDefinition);
    currentLine = strrep(currentLine, '%INPUTMETADEFINITION%', metaInputDefinition);
    currentLine = strrep(currentLine, '%OUTPUTMETADEFINITION%', metaOutputDefinition);
    currentLine = strrep(currentLine, '%GETINPUTIMAGES%', getInputImages);
    currentLine = strrep(currentLine, '%GETINPUTMETA%', getInputMeta);
    currentLine = strrep(currentLine, '%DEFINEXMLPARAMETERS%', parameterDefinition);
    currentLine = strrep(currentLine, '%RETRIEVEXMLPARAMETERS%', parameterRetrieval);
    currentLine = strrep(currentLine, '%OUTPUTIMAGES%', outputImages);
    
 
    %% write the current line
    fprintf(newFilterSource, '%s\n', currentLine);
    
    currentLine = fgetl(sourceTemplate);
end

%% close the file handles
fclose(headerTemplate);
fclose(sourceTemplate);
fclose(newFilterHeader);
fclose(newFilterSource);

%% display success message
disp(['Filter template for ' filterName ' was successfully generated in the path ' pwd, '.']);
disp('Copy the files to Source/Filters/, adapt the remaining lines marked with TODO and recompile XPIWIT to include the new filter.');