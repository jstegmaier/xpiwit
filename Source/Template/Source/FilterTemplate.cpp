/**
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit.
 * Copyright (C) 2015 A. Bartschat, E. H�bner, M. Reischl, R. Mikut and J. Stegmaier
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the following publication
 *
 * A. Bartschat, E. H�bner, M. Reischl, R. Mikut, J. Stegmaier,
 * XPIWIT - An XML Pipeline Wrapper for the Insight Toolkit. 2015.
 *
 */

// project header
#include "%FILTERNAME%Widget.h"
#include "../../../Core/Utilities/Logger.h"
#include "../../../Filter/Base/Management/ImageWrapper.h"

// itk header
// TODO: ENTER YOUR REQUIRED HEADERS HERE!

// XPIWIT namespace
namespace XPIWIT 
{

// the default constructor
template <class TInputImage>
%FILTERNAME%Widget<TInputImage>::%FILTERNAME%Widget() : ProcessObjectBase()
{
    // set the widget type
	this->mName = %FILTERNAME%Widget<TInputImage>::GetName();
    this->mDescription = "%DESCRIPTION%";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    %INPUTIMAGEDEFINITION%
    %OUTPUTIMAGEDEFINITION%
    %INPUTMETADEFINITION%
    %OUTPUTMETADEFINITION%
    
    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
%DEFINEXMLPARAMETERS%

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template <class TInputImage>
%FILTERNAME%Widget<TInputImage>::~%FILTERNAME%Widget()
{
}


// the update function
template <class TInputImage>
void %FILTERNAME%Widget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    const bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;
%RETRIEVEXMLPARAMETERS%
    
    // get the input images
%GETINPUTIMAGES%

    // get the meta input
%GETINPUTMETA%
    
    ////////// TODO: DEFINE YOUR CODE HERE! ////////////
    // e.g., specify and update an itk filter and use the above-mentioned input images
    ////////////////////////////////////////////////////

    // update the pipeline by calling update for the most downstream filter
    // TODO: add a call to the update function of your filter (e.g. myfilter->Update() ).
    itkTryCatch( ..., "Failed to update myfilter" );

	// save output
%OUTPUTIMAGES%

    // update the base class update function
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

// explicit template instantiations to register the filter in the factory
// Note: If your filter only supports one or the other data type, remove the respective lines
static ProcessObjectProxy< %FILTERNAME%Widget<Image2Float> > %FILTERNAME%WidgetImage2Float;
static ProcessObjectProxy< %FILTERNAME%Widget<Image3Float> > %FILTERNAME%WidgetImage3Float;
static ProcessObjectProxy< %FILTERNAME%Widget<Image2UShort> > %FILTERNAME%WidgetImage2UShort;
static ProcessObjectProxy< %FILTERNAME%Widget<Image3UShort> > %FILTERNAME%WidgetImage3UShort;

} // namespace XPIWIT